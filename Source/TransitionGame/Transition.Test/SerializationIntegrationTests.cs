﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using ProtoBuf;
using ProtoBuf.Meta;
using Transition._____Temp.StarGuard;
using Transition._____Temp.StarGuard.Entities;
using Transition._____Temp.StarGuard.EntityBlueprints;
using StarGuardPlayer = Transition._____Temp.StarGuard.Entities.Player;

namespace Transition.Test 
{
    /// <summary>
    /// Responsible for testing serialization / deserialization
    /// </summary>
    /// <remarks>
    /// This forms the basis of our rewinding and replays and MUST be 100% ROBUST. We are allowed to change the game design to facilitate this testing
    /// but we will not got down a TDD rabbit hole. We must also be aware of any performance issues that could arise due to the approach taken here (Interfaces / Garbage)
    /// </remarks>
    [TestFixture, RequiresSTA, Category(TestCategory.IntegrationTests)]  
    public class SerializationIntegrationTests
    {
        static RuntimeTypeModel _serializer;

        EntityFactory _entityFactory;

        [TestFixtureSetUp]
        public void Initialise()
        {
            TestHelper.Initialise();
            TransitionGame.Instance.DataManager.LoadContent();

            _entityFactory = TestHelper.InitialiseEntityFactory();
            _serializer = ProtobufConfiguration.InitialiseProtobuf();
        }

        [TestCase]
        public void TestSaveAndLoadEmptyWorld()
        {
            var ms = new MemoryStream();
            var world = new World();
            _serializer.SerializeWithLengthPrefix(ms, world, typeof(World), PrefixStyle.Base128, 1);

            ms.Position = 0;
            var clonedWorld = _serializer.DeserializeWithLengthPrefix(ms, null, typeof(World), PrefixStyle.Base128, 1);

            ms.Dispose();
            
            var referenceTypes = new List<Type> { typeof(EntityBlueprint), typeof(BaseGame) };
            var sv = new ObjectComparer(referenceTypes);
            var equivalent = sv.Equivalent(world, clonedWorld);

            Assert.True(equivalent);
        }

        [TestCase]
        public void TestSaveAndLoadWorldContents()
        {
            var world = CreatePopulatedWorld();

            var ms = SerializeWorld(world);

            BeforeDeserializeWorld();
            var clonedWorld = DeserializeWorld(ms, rewinding: false);

            ms.Dispose();

            CompareWorlds(world, clonedWorld);
        }

        [TestCase]
        public void TestRewindWorld()
        {
            var world = CreatePopulatedWorld();

            var ms = SerializeWorld(world);

            var random = new System.Random();
            var updates = random.Next(1, 60);
            for (var i = 0; i < updates; i++)
                world.Update(false);

            TransitionGame.Instance.World.BeforeRewind();
            world = DeserializeWorld(ms, rewinding: false);

            //REWIND------
            BeforeDeserializeWorld();
            var clonedWorld = DeserializeWorld(ms, rewinding: true);
            //------------

            //TransitionGame.Instance.World.IsRewinding = true;

            ms.Dispose();

            CompareWorlds(world, clonedWorld);
        }

        private World CreatePopulatedWorld()
        {
            TransitionGame.Instance.World = new World();

            var world = TransitionGame.Instance.World;

            world.Add(EntityKeys.Player, _entityFactory.Create<StarGuardPlayer>("Ship1"));
            world.Add(EntityKeys.Camera, _entityFactory.Create<CameraProxy>());
            world.Add(_entityFactory.Create<Weapon>());
            world.Add(_entityFactory.Create<Bullet>());
            world.Add(_entityFactory.Create<EnemyBullet>("TrooperBullet"));
            world.Add(_entityFactory.Create<Zomboid>());
            world.Add(_entityFactory.Create<Trooper>());
            world.Add(_entityFactory.Create<Flag>());
            world.Add(_entityFactory.Create<ParticleSystem>());
            world.Add(_entityFactory.Create<Sound>());
            world.Add(_entityFactory.Create<Mine>());
            world.Add(_entityFactory.Create<Stalactite>());
            world.Add(_entityFactory.Create<Turret>());
            world.Add(_entityFactory.Create<TripleGun>());
            world.Add(_entityFactory.Create<Flier>());
            world.Add(_entityFactory.Create<Rhino>());
            world.Add(_entityFactory.Create<Octopus>());
            world.Add(_entityFactory.Create<ExplosiveBlock>());
            world.Add(_entityFactory.Create<EliteTrooper>());
            world.Add(_entityFactory.Create<DestructibleBlock>());
            world.Add(_entityFactory.Create<EliteJumper>());
            world.Add(_entityFactory.Create<ScoreLabel>());
            return world;
        }

        static MemoryStream SerializeWorld(World world)
        {
            var ms = new MemoryStream();
            _serializer.SerializeWithLengthPrefix(ms, world, typeof(World), PrefixStyle.Base128, 1);

            return ms;
        }

        static void BeforeDeserializeWorld()
        {
            var entityFactory = GameServices.Instance.Get<EntityFactory>();

            entityFactory.ReleaseAllEntities();
            TransitionGame.Instance.SpritePool.DeallocateAllSprites();
        }

        static World DeserializeWorld(Stream ms, bool rewinding)
        {
            ms.Position = 0;
            var clonedWorld = (World)_serializer.DeserializeWithLengthPrefix(ms, null, typeof(World), PrefixStyle.Base128, 1);

            clonedWorld.StateLoaded(rewinding: rewinding);

            return clonedWorld;
        }

        static void CompareWorlds(World originalWorld, World clonedWorld)
        {
            var referenceTypes = new List<Type> { typeof(EntityBlueprint), typeof(EnemyBlueprintSurrogate), typeof(BaseGame) };
            var sv = new ObjectComparer(referenceTypes);
            var equivalent = sv.Equivalent(originalWorld, clonedWorld);

            System.Diagnostics.Debug.WriteLine("Testing worlds are equivalent");
            Assert.True(equivalent);

            System.Diagnostics.Debug.WriteLine("Testing world contents...");
            var i = 0;
            foreach (var entity in originalWorld.Entities)
            {
                System.Diagnostics.Debug.WriteLine("{0}: entity is a {1}", i, entity.GetType());
                var entityEquivalent = sv.Equivalent(entity.Value, clonedWorld[i]);
                Assert.True(entityEquivalent);

                i++;
            }
        }
    }
}
