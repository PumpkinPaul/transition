//TODO: Transition - might need a 3rd colour here as using theme colours directly
using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// A control that displays a list of images representing options.
    /// </summary>
    public class TextStripControl : MenuControl
    {
        public BitmapFont Font = Theme.Font;
        public Vector2 FontSize = Vector2.Zero;
        Alignment _alignment = Alignment.CentreLeft;
        public int MaxPulseTicks = 25;
        public float TextSpacer = 1.0f;
        public Color SelectedItemTint;

        /// <summary></summary>
        readonly List<string> _texts = new List<string>();
        readonly List<bool> _enables = new List<bool>();
        readonly List<string> _helpText = new List<string>();
        readonly List<int> _pulses = new List<int>();

        /// <summary></summary>
        int _selectedIndex = -1;
        private int _hoverIndex = -1;
        int _mouseDownHoverIndex = -1;

        int _pulseInc = 1;

        public override bool AutoTabIndex { get { return true; } }

        /// <summary>
        /// Initialises a new instance of an ImageListControl. 
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public TextStripControl(BaseGame game) : base(game) 
        { 
            Tint = Theme.StandardColor;
            SelectedItemTint = Theme.LabelValueColor;
        }
 
        public override void Load(XElement element)
        {
            base.Load(element);

            ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            _alignment = element.GetAttributeEnum("alignment", _alignment);
            MaxPulseTicks = element.GetAttributeInt32("maxPulseTicks", MaxPulseTicks);
            TextSpacer = element.GetAttributeSingle("textSpacer", TextSpacer);
            SelectedItemTint = element.GetAttributeColor("selectedItemTint", SelectedItemTint);

            //Content! Might be items
            var selectedIndex = -1;
            foreach (var itemElement in element.Elements("item"))
            {
                selectedIndex++;
                var text = itemElement.GetAttributeString("text");
                if (text == null)
                    continue;

                var helpText = itemElement.GetAttributeString("helpText");

                AddText(text, helpText ?? string.Empty);

                var selected = itemElement.GetAttributeBool("selected", false);
                if (selected)
                    SelectedIndex = selectedIndex;
            }
        }

        /// Gets the text to display for the left and right dpad / stick action.
        public override string LeftRightButtonText
        {
            get { return "change section"; }
        }

        /// <summary>
        /// Gets the number of options in the list.
        /// </summary>
        public int Count
        {
            get { return _texts.Count; }
        }

        /// <summary>
        /// Gets or sets the index of the selected item.
        /// </summary>
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { _selectedIndex = value; }
        }

        /// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
        public override Rect Bounds
        {
            get 
            { 
                if (_texts.Count == 0)
                    return Rect.Zero;

                var bounds = Font.GetBounds(Position, FontSize, _texts[0], _alignment);

                for(var i = 1; i < _texts.Count; i++)
                {
                    bounds.Right += TextSpacer + Font.GetTextLength(_texts[i], FontSize.X);
                }

                return bounds;
            }
        }

        /// <summary>
        /// Tells a control to increase its value.
        /// </summary>
        protected override bool OnIncreaseValue()
        {
            if (_selectedIndex >= _texts.Count - 1)
                return false;

            ++_selectedIndex;
            
            return true;
        }

        /// <summary>
        /// Tells a control to decrease its value.
        /// </summary>
        protected override bool OnDecreaseValue()
        {
            if (_selectedIndex <= 0)
                return false;

            --_selectedIndex;
            
            return true;
        }

        protected override void OnMouseDown()
        {
            SetHoverIndex();

            _mouseDownHoverIndex = _hoverIndex;
        }

        protected override void OnMouseMove() 
        {
            SetHoverIndex();
        }

        void SetHoverIndex()
        {
            var bounds = Bounds;

            //Find the mouse relative to the control.
            //We offset by half the spacer to make the calculation nice 'n' easy. We won't get out of bounds bumf because of the first
            //contains check above.
            var mouseX = Game.MenuManager.MouseWorldPosition;

            var left = bounds.Left;
            var right = left;
            for (var i = 0; i < _texts.Count; i++)
            {
                right += Font.GetTextLength(_texts[i], FontSize.X) + (TextSpacer * 0.5f);

                if (mouseX.X >= left && mouseX.X <= right)
                {
                    _hoverIndex = i;
                    return;
                }

                left = right;
                right += (TextSpacer * 0.5f);
            }
        }

        /// <summary>
        /// .
        /// </summary>
        protected override bool OnClick()
        {
            if (Game.InputMethodManager.InputMethod.SupportsMouse)
            {
                if (_hoverIndex != -1 && _hoverIndex != _mouseDownHoverIndex)
                    return false;

                _selectedIndex = _hoverIndex;
            }

            return true;
        }

        protected override void OnMouseLeave()
        {
            _hoverIndex = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        public void AddText(string text)
        {
            _texts.Add(text);
            _helpText.Add(string.Empty);
            _enables.Add(true);
            _pulses.Add(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="helpText"></param>
        public void AddText(string text, string helpText)
        {
            _texts.Add(text);
            _enables.Add(true);
            _helpText.Add(helpText);
            _pulses.Add(0);
        }

        /// <summary>
        /// Clears all the options from the list.
        /// </summary>
        public void ClearItems()
        {
            _selectedIndex = -1;
            _texts.Clear();
            _enables.Clear();
            _helpText.Clear();
            _pulses.Clear();
        }

        /// <summary>
        /// Allows a derived control to update itself.
        /// </summary>
        protected override void OnUpdate()
        {
            if (Game.InputMethodManager.InputMethod.SupportsMouse == false)
            {
                _hoverIndex = -1;
                _mouseDownHoverIndex = -1;
            }

            for (var i = 0; i < _pulses.Count; i++)
            {
                if (_selectedIndex != i ||  Game.MenuManager.FocusControl != this)
                {
                    //Elegantly reduce the size of the non active image back to normal
                    if (_pulses[i] > 0)
                        _pulses[i]--;
                }
                else
                {
                    //Pulse the selected image
                    _pulses[i] += _pulseInc;

                    if (_pulses[i] > MaxPulseTicks)
                        _pulseInc = -_pulseInc;
                    else if (_pulses[i] < 0)
                    {
                        _pulseInc = -_pulseInc;  
                        _pulses[i] = 0;
                    }
                }
            }

            if (Game.InputMethodManager.InputMethod.SupportsMouse == false)
                _hoverIndex = -1;
            
            if (_hoverIndex != -1)
                HelpText = _helpText[_hoverIndex];
            else if (_selectedIndex != -1)
                HelpText = _helpText[SelectedIndex];
        }
        
        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
        {
            var position = Position;

            //Determine the colour of each item!

            //Selected items can be either their default color or the focused color
            Color selectedTint;
            
            if (IsFocusControl)
                selectedTint = Theme.Current.GetColor(Theme.Colors.Focus) * FocusValue;
            else
                selectedTint = Tint;

            //Normal items are always the normal color
            var normalTint = Tint;

            //Hover items are either the hover color or their default color
            var hoverTint = IsHoverControl ? Theme.Current.GetColor(Theme.Colors.Hover) : normalTint;

            normalTint *= Overlay.Opacity;
            selectedTint *= Overlay.Opacity;
            hoverTint *= Overlay.Opacity;

            for (var i = 0; i < _texts.Count; ++i)
            {
                //Pick the correct color (determined above)
                var tint = i == _selectedIndex ? selectedTint : i == _hoverIndex ? hoverTint : normalTint;

                Game.Renderer.DrawString(Font, position, FontSize, _texts[i], _alignment, tint, RenderStyle, Layer);

                position.X += Font.GetTextLength(_texts[0], FontSize.X) + TextSpacer;
            }
        }
    }
}
