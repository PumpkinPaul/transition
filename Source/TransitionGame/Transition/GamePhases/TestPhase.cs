//using Transition._____Temp.RewindBinaryFormatter;

using Transition._____Temp;
using Transition._____Temp.RewindBinaryFormatter;
using Transition._____Temp.StarGuard;
using Transition._____Temp.Svg;
using Transition._____Temp.TestPhaseBlank;

namespace Transition.GamePhases
{
    /// <summary>
    /// Handles the testing out various concepts.
    /// </summary>
    public class TestPhase : GamePhase
    {
        GamePhase _testPhase;

        public TestPhase(BaseGame game) : base(game) { }

        protected override void OnCreate()
        {
            Game.GamePhaseManager.Register(new TestPhaseRewindBinaryFormatter(Game));
            Game.GamePhaseManager.Register(new TestPhaseStarGuard(Game));
            Game.GamePhaseManager.Register(new TestPhaseBlank(Game));
            Game.GamePhaseManager.Register(new TestPhaseSvg(Game));

            //_testPhase = new TestPhaseRewind(Game);
            //Game.ChangePhase(typeof(TestPhaseStarGuard));
            //Game.ChangePhase(typeof(TestPhaseSvg));
            Game.ChangePhase(typeof(TestPhaseBlank));
        }

        protected override void OnUpdate()
        {
            _testPhase.Update();
        }

        protected override void OnRender()
        {
            _testPhase.Render();
        }
    }
}