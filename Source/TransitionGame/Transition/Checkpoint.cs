using System.Xml.Serialization;

namespace Transition
{
    /// <summary>
    /// Represents data required to start a game at a particluar place.
    /// </summary>
    public class Checkpoint : IReadableCheckpoint
    {
        public Checkpoint() { }

        public Checkpoint(IReadableCheckpoint source)
        {
            ShipName = source.ShipName;
            Difficulty = source.Difficulty;
            Level = source.Level;
            Score = source.Score;
            Health = source.Health;
            RagePower = source.RagePower;
        }

        public string ShipName { get; set; }
        public Difficulty Difficulty { get; set; }
        public int Level { get; set; }
        public int Score { get; set; }
        public int Health { get; set; }
        public int RagePower { get; set; }
    }
}
