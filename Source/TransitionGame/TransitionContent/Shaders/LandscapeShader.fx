texture baseTexture;
float4x4 viewProjection;

sampler baseSampler = sampler_state
{
    Texture = < baseTexture >;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    ADDRESSU = CLAMP;
    ADDRESSV = CLAMP;
};

struct VS_INPUT
{
    #if SM4
		float4 Position: SV_POSITION;
	#else
		float4 Position: POSITION;
	#endif
    float2 TextureCoords: TEXCOORD0;
    float4 Color: COLOR;
};

struct VertexToPixel
{
    #if SM4
		float4 Position: SV_POSITION;
	#else
		float4 Position: POSITION;
	#endif
    float2 TextureCoords: TEXCOORD0;
    float4 Color        : COLOR;
};

struct PixelToFrame 
{
	float4 Color		: COLOR;
};

VertexToPixel VertexShaderFunction(VS_INPUT input)
{
    VertexToPixel output = (VertexToPixel)0;
    
	output.Position = mul(input.Position, viewProjection);
	output.TextureCoords = input.TextureCoords;
    output.Color = input.Color;
	
    return output;    
}

PixelToFrame PixelShaderFunction(VertexToPixel input)
{
    PixelToFrame output = (PixelToFrame)0;
       
    float4 texColor = tex2D(baseSampler, input.TextureCoords);
    
    output.Color.r = mul(input.Color.r, texColor.r);
    output.Color.g = mul(input.Color.g, texColor.g);
    output.Color.b = mul(input.Color.b, texColor.b);
    output.Color.a = mul(input.Color.a, texColor.a);
    
    return output;
}

technique Landscape
{
    pass Pass0
    {        
        #if SM4
		VertexShader = compile vs_4_0_level_9_1 VertexShaderFunction();
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
		#elif SM3
		VertexShader = compile vs_3_0 VertexShaderFunction();
		PixelShader = compile ps_3_0 PixelShaderFunction();
		#else
		VertexShader = compile vs_2_0 VertexShaderFunction();
		PixelShader = compile ps_2_0 PixelShaderFunction();
		#endif
    }
}