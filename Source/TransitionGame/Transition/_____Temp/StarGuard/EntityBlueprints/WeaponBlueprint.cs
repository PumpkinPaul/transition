using System.Collections.Generic;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
    /// <summary>
    /// Configuration data for weapons.
    /// </summary>
    public class WeaponBlueprint
    {
        public string Name;
        public string Description;
        public string BulletName;
        public readonly List<WeaponUpgrade> FireRates = new List<WeaponUpgrade>();
    }

    public class WeaponUpgrade
    {
        public int FireRate;
        public int Cost;
    }
}
