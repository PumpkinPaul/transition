using Microsoft.Xna.Framework;
using Transition.Cameras;
using Transition.Rendering;

namespace Transition.LevelEditing
{
    /// <summary>
    /// 
    /// </summary>
    public class DesignGrid
    {
        public bool Active = true;

        readonly float[] _steps = { 0.25f, 0.5f, 1.0f, 2.0f, 3.0f, 4.0f };

        public int MaxSteps { get { return _steps.Length - 1; } }
        public float Step { get { return _steps[_stepId]; } }

        int _stepId = 3;
        public int StepId
        { 
            get { return _stepId; }
            set { _stepId = (int)MathHelper.Clamp(value, 0, MaxSteps); }
        }

        public float LineThickness = 0.00175f;
        public float Left = -100.0f;
        public float Right = 100.0f;
        public float Top = 50.0f;
        public float Bottom = -50.0f;
        public Color Color = new Color(16, 16, 16, 16);

        public void Draw(Renderer geometryBatch, Camera camera)
        {
            if (Active == false) 
                return;

            var step = _steps[_stepId];
            var gridLineThickness = LineThickness * -camera.Position.Z;

            //left
            for (var i = 0.0f; i >= Left; i -= step)
            {
                geometryBatch.DrawLine(new Vector2(i, Top), new Vector2(i, Bottom), Color, TransparentRenderStyle.Instance, gridLineThickness);
            }

            //right
            for (var i = 0.0f; i <= Right; i += step)
            {
                geometryBatch.DrawLine(new Vector2(i, Top), new Vector2(i, Bottom), Color, TransparentRenderStyle.Instance, gridLineThickness);
            }

            //top
            for (var i = 0.0f; i <= Top; i += step)
            {
                geometryBatch.DrawLine(new Vector2(Left, i), new Vector2(Right, i), Color, TransparentRenderStyle.Instance, gridLineThickness);
            }

            //bottom
            for (var i = 0.0f; i >= Bottom; i -= step)
            {
                geometryBatch.DrawLine(new Vector2(Left, i), new Vector2(Right, i), Color, TransparentRenderStyle.Instance, gridLineThickness);
            }
        }
    }
}
