using System;
using System.IO;
using System.Linq;
using Krypton;
using Nuclex.Input;
using Transition.Achievements;
using Transition.Actors;
using Transition.GameDebugTools;
using Transition.GamePhases;
using Transition.Input;
using Transition.LevelEditing;
using Transition.Menus;
using Transition.PostProcess;
using Transition.Storage;
using Microsoft.Xna.Framework;
using Transition.CourseSystem;
using Transition.Gearset;
using Transition._____Temp.StarGuard;
using Microsoft.Xna.Framework.Graphics;
using Krypton.Lights;

namespace Transition
{
    public partial class TransitionGame : BaseGame
    {
        //TODO: Transition - look to remove this is possible.
        public static TransitionGame Instance { get; private set; }

        /// <summary>High score tables.</summary>
        private readonly ScoreTable[] _scoreTable = new ScoreTable[MaxDifficultyLevels];

        public const int MaxDifficultyLevels = 3;
        				
        //Manager type things - some of these may well get replaced by the ContentManager
        public readonly CollisionMapManager CollisionMapManager;
        public readonly Hud Hud;
        public Scene Scene { get; private set; }
 
        public LevelManager LevelManager { get; private set; }

        /// <summary>Represents a rectangle describing the physical bounds of the visible world - ie the bit of the world visible on screen.</summary>
        public Rect VisibleWorldBounds { get; private set; }

        public LevelEditor LevelEditor { get; private set; }
        Course ActiveCourse => LevelEditor.Course;

        readonly Type _gameplayPhase = typeof(TestPhaseStarGuard); //typeof(PlayGamePhase);

        public KryptonEngine Krypton { get; private set; }
        Texture2D _lightTexture;

        readonly GaussianBlurProcessor _sceneBlurProcessor;
        public VhsRewindProcessor VhsRewindProcessor { get; }

        public GlobalStats GlobalStats { get; private set; }

        /// <summary>
        /// Initialises a new instance of a TransitionGame.
        /// </summary>
        public TransitionGame() : base("DisasterIncorporated")
        {
            Instance = this;
 
            //Game Components / Elements
            CollisionMapManager = new CollisionMapManager();
            Hud = new Hud(this);

            LevelEditor = new LevelEditor(this);

            // Create Krypton
            Krypton = new KryptonEngine(this, "Content/Effects/KryptonEffect");

            _sceneBlurProcessor = new GaussianBlurProcessor(this);
            VhsRewindProcessor = new VhsRewindProcessor(this) { Active = false };
        }

        protected override IGameInput CreateGameInput()
        {
            return new GameInput();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            GamePhaseManager.Register(new TestPhaseStarGuard(this));

            InitialiseCourse();
            RegisterMenus();
            RegisterAchievements();
            RegisterActors();
            RegisterColliders();
            Hud.Initialise();
            StorageManager.ShowStorageGuide();

            Krypton.Initialize();
            //Krypton.AmbientColor = new Color(0, 79, 134);
        }

        /// <summary>
        /// Creates all menus used throughout the game.
        /// </summary>
        private void RegisterMenus()
        {
            MenuManager.RegisterMenu(new ControlsMenu(this, "Controls"));
            MenuManager.RegisterMenu(new CreditsMenu(this, "Credits"));
            MenuManager.RegisterMenu(new GameOverMenu(this, "Game Over"));
            MenuManager.RegisterMenu(new HeroesMenu(this, "Scores"));
            MenuManager.RegisterMenu(new HowToPlayMenu(this, "How To Play"));
            MenuManager.RegisterMenu(new HelpAndOptionsMenu(this, "Help and Options"));
            MenuManager.RegisterMenu(new IntroMenu(this, string.Empty));
            MenuManager.RegisterMenu(new MedalsMenu(this, "Medals"));
            MenuManager.RegisterMenu(new MessageBoxMenu(this, string.Empty));
            MenuManager.RegisterMenu(new PauseMenu(this, "Pause"));
            MenuManager.RegisterMenu(new ProfilesMenu(this, "Profiles"));
            MenuManager.RegisterMenu(new SettingsMenu(this, "Settings"));
            MenuManager.RegisterMenu(new ShipSelectionMenu(this, "Select Ship"));
            MenuManager.RegisterMenu(new TextEntryMenu(this, "Enter Text"));
            MenuManager.RegisterMenu(new ViewDemoMenu(this, string.Empty));
            MenuManager.RegisterMenu(new FileListMenu(this, "Files"));
            MenuManager.RegisterMenu(new TestMenu(this, "Test Menu"));
            
            var mainMenu = new MainMenu(this, "Main Menu");
            MenuManager.RegisterMenu(mainMenu);

            var shopMenu = new ShopMenu(this, "Shop");
            shopMenu.AfterClosed += CloseShop;
            MenuManager.RegisterMenu(shopMenu);        
            
            var levelEditorMenu = new LevelEditorMenu(this, "Level Editor");
            MenuManager.RegisterMenu(levelEditorMenu);      

            var newGameMenu = new NewGameMenu(this, "New Game");
            newGameMenu.StartGame += StartGame;
            MenuManager.RegisterMenu(newGameMenu);

            var resumeGameMenu = new ResumeCheckpointMenu(this, "Resume Game");
            resumeGameMenu.StartGame += StartGame;
            MenuManager.RegisterMenu(resumeGameMenu);

            var levelCompleteMenu = new LevelCompleteMenu(this, "Level Complete");
            levelCompleteMenu.StartGame += StartGame;
            MenuManager.RegisterMenu(levelCompleteMenu);

            GS.AddQuickAction("Editor", ()=> LevelEditor.ToggleVisibility());  
        }

        private void RegisterAchievements()
        {
            AchievementsManager.Register(new LevelCompletedAchievement("{57B74F16-55C8-4B8D-8916-363A82B27191}", 1));
            AchievementsManager.Register(new LevelCompletedAchievement("{9BB35212-0948-4294-9322-BD96FC3F37E9}", 3));
        }

        private void RegisterActors()
        {
            ActorFactory.RegisterFactory(typeof(BlastEffect), 64);
            ActorFactory.RegisterFactory(typeof(HomingBullet), 64);
            ActorFactory.RegisterFactory(typeof(LevelCompleteLabel), 4);
            ActorFactory.RegisterFactory(typeof(ParticleSystem), 256);
            ActorFactory.RegisterFactory(typeof(RippleEffect), 32);
            ActorFactory.RegisterFactory(typeof(SceneLabel), 16);
            ActorFactory.RegisterFactory(typeof(ScoreBonusLabel), 64);
            ActorFactory.RegisterFactory(typeof(Ship), 4);
            ActorFactory.RegisterFactory(typeof(ShipInvulnerabilityEffect), 4);
            ActorFactory.RegisterFactory(typeof(SimpleTrailEffect), 256);
            ActorFactory.RegisterFactory(typeof(StandardAlienBullet), 256);
            ActorFactory.RegisterFactory(typeof(StandardBullet), 256);
            ActorFactory.RegisterFactory(typeof(StandardWeapon), 4);
            ActorFactory.RegisterFactory(typeof(Animated), 64);
            ActorFactory.RegisterFactory(typeof(Mine), 24);
        }

        private void RegisterColliders()
        {
            CollisionMapManager.Register(ActorClassification.Bullet, ActorClassification.Alien);
        
            CollisionMapManager.Register(ActorClassification.Ship, ActorClassification.EnemyBullet);
        }

        protected override void OnReloadScripts()
        {
            Hud?.ConfigureOverlay();

            if (GameMap != null)
                GameMap = GameMap.Load(GameMap.Path);
        }

        void InitialiseCourse()
        {
            //Not using courses for this game
            //var filepath = Path.Combine(Platform.ResourcesPath, "Courses", "Course1.course");

            //LevelEditor.InitialiseCourse(Course.Load(filepath));
            
            //GS.Inspect("Level Editor", LevelEditor, true);
        }

        protected override void OnRegisterDebugCommands(IDebugCommandHost host) 
        {    
            host.RegisterCommand("medal", "Grant a medal", (debugHost, command, arguments) =>
            {
                if (arguments.Count != 1)
                    return;

                Hud.AchievementGained(arguments[0]);
            });

            host.RegisterCommand("notify", "Show Notification", (debugHost, command, arguments) =>
            {
                if (arguments.Count != 1)
                    return;

                Hud.AddNotification(arguments[0]);
            });

            host.RegisterCommand("immune", "Make ship immune", (debugHost, command, arguments) =>
            {
                if (arguments.Count != 1)
                    return;
                    
                if (ActivePlayer?.Ship == null)
                    return;

                int ticks;
                if (int.TryParse(arguments[0], out ticks) == false)
                    return;

                ActivePlayer.Ship.MakeInvulnerableWithEffect(ticks);
            });

            host.RegisterCommand("bloom", "Set bloom", (debugHost, command, arguments) =>
            {
                if (arguments.Count != 1)
                    return;

                int id;
                if (int.TryParse(arguments[0], out id) == false)
                    return;

                try
                {
                    BloomProcessor.Settings = BloomProcessor.BloomSettings.PresetSettings[id];
                }
                catch (IndexOutOfRangeException) { }
            });
        }

        protected override void OnReturnToIntro()
        {
            DestroyPlaySession();
            Scene.KillParticles();

            ChangePhase(typeof(IntroMenuPhase));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void LoadContent()
        {
			base.LoadContent();

            //These components are probably game specific - don't see much point in trying to shoehorn some abstract bases into BaseGame.
            //Of course, I reserve the right to totally change my mind later ;-)

            //Actors and particles
            Scene = new Scene(this);
            Scene.CreateParticles(1000);

            //Game logic - spawns attack waves
            LevelManager = new LevelManager(this, Scene);
            LevelManager.LoadContent();

            // Create a new simple point light texture to use for the lights
            _lightTexture = LightTextureBuilder.CreatePointLight(GraphicsDevice, 512);

            // Create some lights and hulls
            CreateLights(_lightTexture);
            
            _sceneBlurProcessor.LoadContent();
            VhsRewindProcessor.LoadContent();
        }

        private void CreateLights(Texture2D texture)
        {
            var playerLight = new Light2D()
            {
                Texture = _lightTexture,
                Range = 4,
                Color = new Color(0.585f, 0.59f, 0.599f, 0.0f),// * 0.75f,
                Intensity = 0.75f,
                Angle = MathHelper.TwoPi,
                X = 0,
                Y = 0
            };

            Krypton.Lights.Add(playerLight);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void UnloadContent()
        {
            base.UnloadContent();

            Content.Unload();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void BeginPlaySession()
        {
            MenuManager.CloseAllMenus();

            DestroyPlaySession();

            DemoRecorder.Stop();

            InitialisePlaySession(InitialCheckpoint);

            SoundManager.StopMusic(immediate: false);
        }

        public override void ContinuePlaySession()
        {
            //Get the level the player was last playing and dig out the checkpoint for it.
            const int minCheckpointId = 0;
            var maxCheckpointId = ActivePlayer?.Profile?.Checkpoints?.Count() - 1 ?? minCheckpointId;

            var checkpointId =  (int)MathHelper.Clamp(LevelManager.LevelNumber - 1, minCheckpointId, maxCheckpointId);

            var checkpoint = ActivePlayer?.Profile?.GetCheckpoint(checkpointId) ?? InitialCheckpoint;

            InitialisePlaySession(checkpoint);
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitialisePlaySession(IReadableCheckpoint checkpoint)
		{
            GlobalStats = new GlobalStats();
            InitialisePlayers(checkpoint);
            LevelManager.InitialisePlaySession(checkpoint);
            Scene.InitialisePlaySession(checkpoint);
            Hud.InitialisePlaySession();
            
            //Only show the HUD if WE are actually playing, not the demo / replay system
            if (DemoRecorder.IsPlaying == false)
                FadeIn();

            ChangePhase(_gameplayPhase);

            ((TestPhaseStarGuard)GamePhaseManager.ActivePhase).InitialisePlaySession(checkpoint);
        }

		/// <summary>
        /// The current play session has ended - either naturally (lost all lives, etc) or via a quit from the pause menu.
        /// </summary>
		public override void EndPlaySession()
		{
            //Any achievements gained this play?
            if (ActivePlayer.Profile.RequiresSave)
                ActivePlayer.Profile.Save(this);

            PlayerCount = 1;

            //Kill the HUD if we are actually playing the game (not the demo playing)
            if (DemoRecorder.IsPlaying == false)
            {
                SoundManager.PlayMusic();
                SoundManager.MusicVolume = GameOptions.MusicVolume;
                FadeOut();
            }

            //Add the IntroMenu and MainMenu
            MenuManager.AddDefaultMenus();

			DemoFinished();
		}

        /// <summary>
        /// 
        /// </summary>
        public void DemoFinished()
        {
		    MaybeSaveReplay();

		    DestroyPlaySession();

            if (GameOptions.ReplayPlaybackMode == ReplayPlaybackMode.Background || GameOptions.ReplayPlaybackMode == ReplayPlaybackMode.OnDemand && MenuManager.TopMenu is ViewDemoMenu && MenuManager.TopMenu.IsClosing == false)
            {
                //We are beginning a game loaded from a demo.
                //var playSessionState = DemoRecorder.Play();
                //InitialisePlaySession(playSessionState);

                DemoRecorder.Play();
                InitialisePlaySession(InitialCheckpoint);
            }
            else
            {    
                DemoRecorder.Stop();
                ChangePhase(typeof(MenuGamePhase));
            }
        }

        public void MaybeSaveReplay()
        {
            //Save the current play session as a demo?
            if (DemoRecorder.IsRecording)
            {
                DemoRecorder.Stop();
                DemoRecorder.Save("demo.bin");
            }
        }
		
		/// <summary>
        /// 
        /// </summary>
		public void DestroyPlaySession()
		{
            Scene.DestroyPlaySession();
            LevelManager.DestroyPlaySession();
            ScreenFlashManager.ClearColor = Theme.Current.GetColor(Theme.Colors.Background);
		}

        public void OpenShop()
        {
            IsPaused = true;
            FadeOut();
            MenuManager.AddMenu(typeof(ShopMenu));
        }

        public void CloseShop(object sender, EventArgs e)
        {
            IsPaused = false;
            FadeIn();
            LevelManager.BeginNextLevel();
            ActivePlayer.Ship.MakeInvulnerableWithEffect(Ship.MaxInvulnerableTicks);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnFadeIn()
        {
            Hud.FadeIn();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnFadeOut()
        {
            Hud.FadeOut();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void OnUpdate(GameTime gameTime)
        {                      
            if (GetGameInput() == false)
                return;

            if (IsPaused == false)
            {
                TestForPauseAction();

                //Update all of the game systems
                CameraManager.Update();
                LevelManager.Update();
                Scene.Update(gameTime);
                LevelEditor.Update();
            }

            Hud.Update();
        }

        /// <summary>
        /// Tests to see if the player(s) paused the game.
        /// </summary>
        private void TestForPauseAction()
        {
            //Should we pause?
            if (IsPaused || MenuManager.HasMenu) 
                return;

            if (ActivePlayer.GameInput.Pause)
                Pause((int)ActivePlayer.PlayerIndex);
        }
					
		/// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void OnDraw(GameTime gameTime)
        {
            CameraManager.Render();

            //Make sure we clear the backbuffer *after* Krypton is done pre-rendering
            GraphicsDevice.Clear(ScreenFlashManager.CurrentColor);

            CameraManager.Camera = GuiCamera;

            GamePhaseManager.Render();

            CameraManager.Camera = GameCamera;

            SetVisibleWorldBounds();

            LevelManager.Draw();

            if (LevelEditor.Visible)
                CameraManager.Camera = LevelEditor.Camera;

            //if (ActiveCourse != null)
            //    ActiveCourse.Draw();

            CameraManager.Camera = GameCamera;
		    Scene.Draw();

		    if (MenuManager.HasMenu == false)
		        _sceneBlurProcessor.BlurAmount = 0.0f;
            else if (MenuManager.Menus.Count() > 1)
                _sceneBlurProcessor.BlurAmount = 1.0f;
            else
                _sceneBlurProcessor.BlurAmount = MathHelper.SmoothStep(0.0f, 1.0f, MenuManager.TopMenu?.Opacity ?? 0.0f);

            PostProcess.DrawPostProcessEffect(_sceneBlurProcessor);

            PostProcess.DrawPostProcessEffect(VhsRewindProcessor);

            LevelEditor.Draw();

            CameraManager.Camera = GuiCamera;
            Hud.Draw();
            MenuManager.Draw();
        }

        private void SetVisibleWorldBounds()
        {
            //TODO: Transition
            //Set the game world / screen bounds - this helps us map window coords to game world coords
            var topLeft = GraphicsDevice.Viewport.Unproject(new Vector3(0, 0, 0), ProjectionMatrix, ViewMatrix, Matrix.Identity);
            var bottomRight = GraphicsDevice.Viewport.Unproject(new Vector3(FullscreenViewport.Bounds.Width, FullscreenViewport.Bounds.Height, 0), ProjectionMatrix, ViewMatrix, Matrix.Identity);

            var zoom = -CameraManager.Zoom;
            VisibleWorldBounds = new Rect(topLeft.Y*zoom, topLeft.X*zoom, bottomRight.Y*zoom, bottomRight.X*zoom);
        }

        /// <summary>
        /// Gets the score table (for the default Points Battle game mode).
        /// </summary>
        public ScoreTable GetScoreTable(Difficulty difficulty)
        {
            return _scoreTable[(int)difficulty];
        }
				
        /// <summary>
        /// Resets the players to their initial state.
        /// </summary>
        private void InitialisePlayers(IReadableCheckpoint checkpoint)
        {
            for (var i = 0; i < PlayerCount; ++i)
            {
                var player = Players[i];
                player.Initialise();
                
                //Create a ship for the player
                var ship = (Ship)ActorFactory.Create(typeof(Ship), checkpoint.ShipName);
                ship.Player = player;
				ship.PlayerId = i;
                
                player.Ship = ship;

                //Add the ship to the scene
                //Scene.AddActor(ship);
            }
        }

		/// <summary>
        /// Gets input from the player(s) and turns it into game actions
        /// </summary>
        private bool GetGameInput()
        {            
   //         var gameInput = new GameInput();
			
			//if (DemoRecorder.IsPlaying)
			//{
   //             if (!IsPaused)
   //             {
			//	    //Get input from the demo recorder
   //                 if (DemoRecorder.Finished)
   //                     DemoFinished();

                    
   //             }
			//}
			//else
			//{
   //             gameInput = 
			//}
		
            ActivePlayer.GameInput = InputMethodManager.InputMethod.GatherGameInput(ActivePlayer.PlayerIndex, ActivePlayer.Profile);
            return true;
        }
		
		/// <summary>
        /// Gets a player by index
        /// </summary>
        public Player GetPlayer(int index)
        {				
            return Players[index];
        }
		
        /// <summary>
        /// Handler for the StorageDeviceSelected event. 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The data for the event.</param>
        protected override void OnStorageManagerStorageDeviceSelected(object sender, StorageDeviceActionEventArgs e)
        {
            if (e.DialogAction == DialogAction.Select)
            {
                var gameOptions = GameOptions;

                DemoRecorder.LoadFromStorage(e.StorageContainer);
                PlayerProfileManager.LoadFromStorage(e.StorageContainer);
                GameOptions = GameOptions.LoadFromStorage(e.StorageContainer);

                //This could do with tidying up - base ScoreTable class with inherited children - quick and dirty for now - yuk.
                _scoreTable[(int)Difficulty.Easy] = ScoreTable.LoadFromStorage(e.StorageContainer, "ScoresEasy.xml");
                _scoreTable[(int)Difficulty.Normal] = ScoreTable.LoadFromStorage(e.StorageContainer, "ScoresMedium.xml");
                _scoreTable[(int)Difficulty.Hard] = ScoreTable.LoadFromStorage(e.StorageContainer, "ScoresHard.xml");

                SoundManager.MusicVolume = GameOptions.MusicVolume;
                SoundManager.SfxVolume = GameOptions.SfxVolume;

                ViewportZoom = GameOptions.ScreenSize;

                #if WINDOWS || WINDOWS_STOREAPP || LINUX || MONOMAC
                    Graphics.PreferredBackBufferWidth = GameOptions.Width;
                    Graphics.PreferredBackBufferHeight = GameOptions.Height;

                    if (gameOptions.FullScreen != GameOptions.FullScreen)
                    {
                        SettingsMenu.ToggleFullscreen = true;
                        Graphics.ToggleFullScreen();
                    }
    //#if !MONOMAC
                    Graphics.ApplyChanges();
    //#endif
                    ResolutionsChanged();
                #endif
                
                CrtEffectChanged();
                BloomChanged();
                VibrationChanged();
            }

            //Always play the music
            //SoundManager.PlayMusic("Transition");
        }

        void StartGame(object sender, EventArgs e)
        {
            //You could add further game setup menus here...
            //MenuManager.AddMenu(MenuManager.GetMenu(typeof(ShipSelectionMenu)));

            //...or somewhere to configure more settings...
            //GameOptions.PlayerCount = 1;
            //GameOptions.Player1Difficulty = Difficulty.Normal;
            //PlayerCount = GameOptions.PlayerCount;
			//ActivePlayer.Difficulty = GameOptions.Player1Difficulty;
            ActivePlayer.PlayerIndex = (ExtendedPlayerIndex)MenuManager.PrimaryControllerId;
            
			BeginPlaySession();
        }
    }
}
