using System.Collections.Generic;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
	/// <summary>
	/// Configuration data for ships.
	/// </summary>
    public class ShipBlueprint
    {
        public string Name;
        public TextureInfo MenuTextureInfo;
        public string Classification;
        public string Description;
        public float AccelerationX;
        public float AccelerationY;
        public float MaxVelocityX;
        public float MaxVelocityY;
        public float FrictionX;
        public float FrictionY;
        public string WeaponName;
        public RageType RageType;
        public string Nuke;
        
        public readonly List<ParticleEffect> NukeEffects = new List<ParticleEffect>();

        public string WalkLeftAnimation;
        public string WalkRightAnimation;
        public string FaceLeftAnimation;
        public string FaceRightAnimation;
    }
}
