using System;
using Microsoft.Xna.Framework;
using Transition.Steering;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public class HomingBullet : Bullet
    {
        private const int TrailPointCount = 50;

        private int _deadTicks;

        private enum Phase
        {
            Normal,
            Targeting,
            Homing,
            Dead
        }

        private Phase _phase;

        int _targetTicks;
        float _speed;
        private readonly TrailEffect _trailEffect;

        /// <summary>
        /// Creates a new instance of a StandardBullet.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public HomingBullet(TransitionGame game) : base(game) 
        {
            _trailEffect = new TrailEffect(_game, TrailPointCount);
            _trailEffect.Initialise("TrailEffect");
        }

        protected override void OnInitialise(string name) 
        {
            base.OnInitialise(name);

            _phase = Phase.Normal;
            _targetTicks = 0;
            _speed = 0.0f;
            _deadTicks = 0;

            _horizontalConstrainBehaviour = ConstrainBehaviour.None;
            _verticalConstrainBehaviour = ConstrainBehaviour.None;

            //_trailEffect is reused in OnAddedToScene.
        }

        /// <summary>
        /// Occurs when the actor is added to the scene.
        /// </summary>
        protected override void OnAddedToScene()
        {
            _game.PlaySound("StandardBulletSpawn");

            base.OnAddedToScene();

            var tint1 = Tint;
            var hsl = ColorHelper.RgbToHsl(tint1);
            hsl.H += RandomHelper.Random.GetFloat(-16f, 16f);
            hsl.L += RandomHelper.Random.GetFloat(-0.1f, 0.2f);

            tint1 = ColorHelper.HslToRgb(hsl);

            _trailEffect.Setup(tint1, 0.1f, 0.01f, true, null);
            
            _speed = RandomHelper.DeterministicRandom.GetFloat(BulletData.SpeedMin, BulletData.SpeedMax);
        }

        protected override void OnUpdate(GameTime gameTime) 
        {
            if (DeactivateMe)
            {
                _phase = Phase.Dead;
                Deactivate();
            }

            switch(_phase)
            {
                case Phase.Normal:
	                _targetTicks++;

	                if (_targetTicks > 0)
                    {
		                _phase = Phase.Targeting;
                        _targetTicks = 0;
	                }
                    break;

                case Phase.Targeting:
	                Target = Scene.FindNearestUntargettedAlien(Position, 0);
	                if (Target != null) 
                    {
		                _phase = Phase.Homing;
	                }

                    break;

                case Phase.Homing:
                    Velocity += SteerLibrary.Seek(this, Target.Position, 0.01f);
                    VectorHelper.Truncate(ref Velocity, _speed);
                    _direction = (float)Math.Atan2(Velocity.Y, Velocity.X);
                    _rotation = Direction;
                    break;

                //Custom death handling to process the trail destruction 
                case Phase.Dead:
                    _deadTicks++;
                    if (_deadTicks == TrailPointCount)
                        _active = false;
                    break;
            }
           
            base.OnUpdate(gameTime);

            _trailEffect.Position = Position;
            _trailEffect.Update(gameTime);
        }

        protected override void OnPostUpdate() 
        {
            _trailEffect.PostUpdate();
        }

        protected override void OnDraw(bool isOnscreen) 
        {   
            _trailEffect.DrawEffect();
        }

        protected override void OnDeactivate() 
        {
            base.OnDeactivate();

            //Don't 'die' immediately - we have a potentially long trail that we'd like to elegantly remove a segment at a time.
            _active = true;
            Visible = false;
            _phase = Phase.Dead;
            this._collidable = false;
            Velocity = Vector2.Zero;
            _trailEffect.Finish();
        }
    }
}
