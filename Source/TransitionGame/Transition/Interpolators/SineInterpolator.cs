using System;
using System.Runtime.Serialization;

namespace Transition
{
    [Serializable]
    public sealed class SineInterpolator : InterpolatorBase, ISerializable
    {
	    public static readonly SineInterpolator Instance = new SineInterpolator();

	    /**
	     * CosineInterpolator can only be accessed as a singleton.
	     */
	    public SineInterpolator() : base() { }
		 
	    /**
	     * Sine interpolation.
	     */
	    //@Override
	    public override float Interpolate(float a, float b, float ratio) 
        {
		    if (a == b) 
			    return a;
		    
		    if (ratio < 0.0f) 
			    ratio = 0.0f;
		    else if (ratio > 1.0f) 
			    ratio = 1.0f;
		    
		    ratio = 1.0f - ratio;
		    var f = Math.Sqrt(1.0 - ratio * ratio);
		    return (float) (a * (1.0 - f) + b * f);
	    }

	    public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.SetType(typeof(SingletonSerializationHelper));
        }

        [Serializable]
        private sealed class SingletonSerializationHelper : IObjectReference 
        {
            //GetRealObject is called after this object is deserialized.
            public Object GetRealObject(StreamingContext context) 
            {
                // When deserialiing this object, return a reference to the Singleton object instead.
                return Instance;
            }
        }
    }
}
