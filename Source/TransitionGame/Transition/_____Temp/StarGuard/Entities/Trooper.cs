﻿using System;
using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Extensions;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// trooper enemy (shoots lasers, moves)
    /// </summary>
    [ProtoContract]
    public class Trooper : Enemy
    {
        public enum Phase
        {
            Uninitialised,
            Walking,
            Crouching,
            Aiming,
            Shooting,
            Rising
        }

        const float MaxSpeedY = 0.35f;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        Phase _phase;

        [ProtoMember(2)]
        int _countdownTicks;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here
        float _walkSpeed;
        private float _chanceOfCrouching;
        private float _chanceOfRepeatShooting;

        //----------------------------------------------------------------------------------------------------

        public Trooper(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _phase = Phase.Uninitialised;
            _countdownTicks = 0;

            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-01.walk-right.animation")
                : Game.AnimationManager.Get("enemy-01.walk-left.animation"));

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() 
        {
            RandomHelper.FastRandom.Reinitialise(Seed);
            _walkSpeed = RandomHelper.FastRandom.NextFloat(0.0085f, 0.0125f);
            _chanceOfCrouching = RandomHelper.FastRandom.NextFloat(0.3f, 0.4f);
            _chanceOfRepeatShooting = RandomHelper.FastRandom.NextFloat(0.45f, 0.75f);
        }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnAddedToWorld(World world)
        {
            BeginWalkingPhase();
        }

        protected override void OnUpdate(bool wasRewinding) 
        {
            switch(_phase)
            {
                case Phase.Walking:
                    UpdateWalkingPhase();
                    break;

                case Phase.Crouching:
                    UpdateCrouchingPhase();
                    break;

                case Phase.Aiming:
                    UpdateAimingPhase();
                    break;

                case Phase.Shooting:
                    UpdateShootingPhase();
                    break;

                case Phase.Rising:
                    UpdateRisingPhase();
                    break;
            }

            Velocity.Y += TestPhaseStarGuard.Gravity;

            Velocity.X = MathHelper.Clamp(Velocity.X, -_walkSpeed, _walkSpeed);
            Velocity.Y = MathHelper.Clamp(Velocity.Y, -MaxSpeedY, MaxSpeedY);

            Game.GameMap.HandleCollisions(this);
        }

        void BeginWalkingPhase()
        {
            Sprites[0].Event = 0;

            _countdownTicks = RandomHelper.Random.GetInt(60, 120);

            if (_phase != Phase.Walking)
            {
                var previousVelocityX = Velocity.X;
                if (RandomHelper.Random.GetFloat() <= 0.5)
                    Velocity.X = _walkSpeed;
                else
                    Velocity.X = -_walkSpeed;

                if (previousVelocityX.FloatEquals(Velocity.X) == false)
                    IsFacingRight = Velocity.X > 0;

                Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-01.walk-right.animation")
                    : Game.AnimationManager.Get("enemy-01.walk-left.animation"));
            }

            _phase = Phase.Walking;
        }

        void BeginCrouchingPhase()
        {
            _phase = Phase.Crouching;
            Sprites[0].Event = 0;

            var player = Game.World.Get<Player>(EntityKeys.Player);
            IsFacingRight = player.Position.X > Position.X;

            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-01.crouch-right.animation")
                : Game.AnimationManager.Get("enemy-01.crouch-left.animation"));

            Velocity.X = 0.0f;
        }

        void BeginAimingPhase()
        {
            _phase = Phase.Aiming;
            Sprites[0].Event = 0;
            
            _countdownTicks = RandomHelper.Random.GetInt(60, 120);
        }

        void BeginShootingPhase()
        {
            _phase = Phase.Shooting;
            Sprites[0].Event = 0;

            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-01.shoot-right.animation")
                : Game.AnimationManager.Get("enemy-01.shoot-left.animation"));            

            var bullet = Game.EntityFactory.Create(typeof(EnemyBullet), "TrooperBullet");
            if (bullet != null)
            {
                bullet.Position = Position;
                var speed = IsFacingRight ? 0.15f : -0.15f;
                bullet.Velocity = new Vector2(speed, 0.0f);
                Game.World.Add(bullet);
            }
        }

        void BeginRisingPhase()
        {
            _phase = Phase.Rising;
            Sprites[0].Event = 0;

            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-01.rise-right.animation")
                : Game.AnimationManager.Get("enemy-01.rise-left.animation"));
        }

        void UpdateWalkingPhase()
        {
            //Stay on platforms?
            var directionSign = Math.Sign(Velocity.X);
            var lookAheadOffset = new Vector2(directionSign * GameMap.TileSize / 5.0f, 0.0f);
            var coords = Game.GameMap.GetTileCoordinates(Position + lookAheadOffset);
            var tileCollision = Game.GameMap.GetTileCollision(coords.X, coords.Y - 1);
            if (tileCollision == GameMap.TileCollision.Passable)
                TurnAround();
        
            _countdownTicks--;
            if (_countdownTicks > 0)
                return;

            //Countdown expired - do something!

            //chance we'll stop and shoot
            if (RandomHelper.Random.GetFloat() <= _chanceOfCrouching)
            {
                BeginCrouchingPhase();
            }
            else
            {
                //Reset the walking phase
                BeginWalkingPhase();
            }
        }

        void UpdateCrouchingPhase()
        {
            if (Sprites[0].Event == 1)
                BeginAimingPhase();
        }

        void UpdateAimingPhase()
        {
            _countdownTicks--;
            if (_countdownTicks > 0)
                return;

            //Countdown expired - do something!
            if (IsPlayerInRange)
                BeginShootingPhase();
            else
                BeginWalkingPhase();
        }

        void UpdateShootingPhase()
        {
            if (Sprites[0].Event != 1)
                return;

            //Reached the end of the shooting animation - we can shoot again or rise
            if (RandomHelper.Random.GetFloat() <= _chanceOfRepeatShooting)
                //BeginShootingPhase();
                BeginAimingPhase();
            else
                BeginRisingPhase();
        }

        void UpdateRisingPhase()
        {
            if (Sprites[0].Event == 1)
                BeginWalkingPhase();
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            if ((entityCollisionEdge & CollisionEdge.Left) != 0 || (entityCollisionEdge & CollisionEdge.Right) != 0)
                TurnAround();
        }

        void TurnAround()
        {
            IsFacingRight = !IsFacingRight;

            if (IsFacingRight)
            {
                Velocity.X = _walkSpeed;
                Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-01.walk-right.animation"));
            }
            else
            {
                Velocity.X = -_walkSpeed;
                Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-01.walk-left.animation"));
            }
        }
    }
}