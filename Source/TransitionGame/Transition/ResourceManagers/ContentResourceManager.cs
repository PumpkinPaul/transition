using System;
using Microsoft.Xna.Framework.Content;

namespace Transition.ResourceManagers
{
    /// <summary>
    /// A base class for resource managers.
    /// </summary>
    public abstract class ContentResourceManager : ResourceManager
    {
        protected readonly ContentManager ContentManager;

        protected ContentResourceManager(ContentManager contentManager)
        {
            if (contentManager == null)
                throw new ArgumentNullException(nameof(contentManager));

            ContentManager = contentManager;
        }
    }
}
