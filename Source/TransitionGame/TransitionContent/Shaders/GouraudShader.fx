float4x4 ViewProjection;

struct VertexToPixel
{
	#if SM4
		float4 Position: SV_POSITION;
	#else
		float4 Position: POSITION;
	#endif
    float4 Color        : COLOR0;
};

struct PixelToFrame 
{
	float4 Color		: COLOR0;
};

#if SM4
	VertexToPixel VertexShaderFunction(float4 inPos : SV_POSITION, float4 inColor : COLOR0)
#else
	VertexToPixel VertexShaderFunction(float4 inPos : POSITION, float4 inColor : COLOR0)
#endif
{
    VertexToPixel output = (VertexToPixel)0;
    
	output.Position = mul(inPos, ViewProjection);
    output.Color.rgba = inColor.rgba;
	
    return output;    
}

PixelToFrame PixelShaderFunction(VertexToPixel input)
{
    PixelToFrame output = (PixelToFrame)0;
    
    output.Color = input.Color;
    
    return output;
}

technique Gouraud
{
    pass Pass0
    {        
        #if SM4
		VertexShader = compile vs_4_0_level_9_1 VertexShaderFunction();
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
		#elif SM3
		VertexShader = compile vs_3_0 VertexShaderFunction();
		PixelShader = compile ps_3_0 PixelShaderFunction();
		#else
		VertexShader = compile vs_2_0 VertexShaderFunction();
		PixelShader = compile ps_2_0 PixelShaderFunction();
		#endif
    }
}