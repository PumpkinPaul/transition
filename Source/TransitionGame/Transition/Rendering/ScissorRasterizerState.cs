using Microsoft.Xna.Framework.Graphics;

namespace Transition.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public class ScissorRasterState : RasterizerState
    {
        public static ScissorRasterState Instance = new ScissorRasterState();

        public ScissorRasterState()
        {
            ScissorTestEnable = true;
        }
    }
}
