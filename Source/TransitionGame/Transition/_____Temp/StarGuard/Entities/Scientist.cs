﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Extensions;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Base for all scientists
    /// </summary>
    [ProtoContract]
    public class Scientist : Entity
    {
        public enum Phase
        {
            Thinking,
            Walking
        }

        const float MaxSpeedY = 0.35f;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        Phase _phase;

        [ProtoMember(2)]
        int _countdownTicks;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here
        float _walkSpeed;
        float _chanceOfThinking;// = 0.3f;

        //----------------------------------------------------------------------------------------------------

        public Scientist(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.Scientist;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _phase = Phase.Thinking;
            _countdownTicks = 0;

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized()
        {
            RandomHelper.FastRandom.Reinitialise(Seed);
            _walkSpeed = RandomHelper.FastRandom.NextFloat(0.0085f, 0.0125f);
            _chanceOfThinking = RandomHelper.FastRandom.NextFloat(0.25f, 0.35f);
        }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization()
        {
            InitialiseNonSerialized();
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            switch (_phase)
            {
                case Phase.Thinking:
                    UpdateThinkingPhase();
                    break;

                case Phase.Walking:
                    UpdateWalkingPhase();
                    break;
            }

            Velocity.Y += TestPhaseStarGuard.Gravity;

            Velocity.X = MathHelper.Clamp(Velocity.X, -_walkSpeed, _walkSpeed);
            Velocity.Y = MathHelper.Clamp(Velocity.Y, -MaxSpeedY, MaxSpeedY);

            Game.GameMap.HandleCollisions(this);
        }


        void BeginThinkingPhase()
        {
            //Stop
            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("scientist-01.face-right.animation")
                : Game.AnimationManager.Get("scientist-01.face-left.animation"));

            _phase = Phase.Thinking;
            _countdownTicks = RandomHelper.Random.GetInt(60, 120);
            Velocity.X = 0.0f;
        }

        void BeginWalkingPhase()
        {
            Sprites[0].Event = 0;

            _countdownTicks = RandomHelper.Random.GetInt(60, 120);

            if (_phase != Phase.Walking)
            {
                var previousVelocityX = Velocity.X;
                if (RandomHelper.Random.GetFloat() <= 0.5)
                    Velocity.X = _walkSpeed;
                else
                    Velocity.X = -_walkSpeed;

                if (previousVelocityX.FloatEquals(Velocity.X) == false)
                    IsFacingRight = Velocity.X > 0;

                Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("scientist-01.walk-right.animation")
                    : Game.AnimationManager.Get("scientist-01.walk-left.animation"));
            }

            _phase = Phase.Walking;
        }

        void UpdateThinkingPhase()
        {
            _countdownTicks--;
            if (_countdownTicks > 0)
                return;

            //Countdown expired - do something!
            BeginWalkingPhase();
        }

        void UpdateWalkingPhase()
        {
            //Stay on platforms?
            var directionSign = Math.Sign(Velocity.X);
            var lookAheadOffset = new Vector2(directionSign * GameMap.TileSize / 5.0f, 0.0f);
            var coords = Game.GameMap.GetTileCoordinates(Position + lookAheadOffset);
            var tileCollision = Game.GameMap.GetTileCollision(coords.X, coords.Y - 1);
            if (tileCollision == GameMap.TileCollision.Passable)
                TurnAround();

            _countdownTicks--;
            if (_countdownTicks > 0)
                return;

            //Countdown expired - do something!

            //Only a 30% chance we'll stop and think
            if (RandomHelper.Random.GetFloat() <= _chanceOfThinking)
            {
                BeginThinkingPhase();
            }
            else
            {
                //Reset the walking phase
                BeginWalkingPhase();
            }
        }

        protected override void OnCollisionWithEntity(Entity collider, float time)
        {
            Debug.Assert(collider.Classification == EntityClassification.Player, "Expecting a player here");

            CreateParticleSystem("ScientistCollect");
            Sound.Play(Game, "ScientistCollect", Position);

            Deactivate();
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            if ((entityCollisionEdge & CollisionEdge.Left) != 0 || (entityCollisionEdge & CollisionEdge.Right) != 0)
                TurnAround();
        }

        void TurnAround()
        {
            IsFacingRight = !IsFacingRight;

            if (IsFacingRight)
            {
                Velocity.X = _walkSpeed;
                Sprites[0].SetAppearance(Game.AnimationManager.Get("scientist-01.walk-right.animation"));
            }
            else
            {
                Velocity.X = -_walkSpeed;
                Sprites[0].SetAppearance(Game.AnimationManager.Get("scientist-01.walk-left.animation"));
            }
        }
    }
}