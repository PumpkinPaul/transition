#include "CommonVertex.inc"

const float blurSize = 1.0 / 512.0;
const float intensity = 0.35;
const float grainstrength = 64.0;

float iGlobalTime = 0.0;

//math:
float sum(float3 c)
{
	return (c.x + c.y + c.z);
}


float random(float2 p)
{
	float2 r = float2(
        23.14069263277926, // e^pi (Gelfond's constant)
         2.665144142690225 // 2^sqrt(2) (Gelfond�Schneider constant)
    );

	return frac(cos(fmod(12345678.0, 256.0 * dot(p, r))));
}

float onOff(float a, float b, float c)
{
	return step(c, sin(iGlobalTime + a * cos(iGlobalTime * b)));
}


float4 getVideo(float2 uv)
{
    float time = iGlobalTime * 0.0001;
	float2 look = uv;
    float window = 1.0 / (1.0 + 20.0 * (look.y - fmod(time / 4.0, 1.0)) * (look.y - fmod(time / 4.0, 1.0)));
    look.x = look.x + sin(look.y * 10.0 + time) / 100.0 * onOff(4.0, 4.0, 0.3) * (1.0 + cos(time * 80.0)) * window;
    float vShift = 0.4 * onOff(2.0, 3.0, 0.9) * (sin(time) * sin(time * 20.0) + (0.5 + 0.1 * sin(time * 200.0) * cos(time)));
    vShift *= 0.005;
	look.y = fmod(look.y + vShift, 1.0);
	float4 video = float4(tex2D(baseSampler, look));
	//float4 video = float4(tex2D(baseSampler, uv));

	return video;
}

float grain(float2 uv, float strength)
{
	float x = (uv.x + 4.0) * (uv.y + 4.0) * (iGlobalTime * 10.0);
	float g = fmod((fmod(x, 13.0) + 1.0) * (fmod(x, 123.0) + 1.0), 0.01) - 0.005;
	return 1.0 - g * strength;
}

struct PS_OUT
{
   float4 Color:   COLOR;
};

float4 PixelShaderFunction(VS_OUT input) : COLOR0
{
	float4 fragColor;
    float2 uv = input.TextureCoords;
    
	float rand = random(iGlobalTime);

    //----------------------------------------------------------------------------------------------------
	//Old TV style jumpiness
	//float4 color = float4(getVideo(uv));
	float4 color = float4(tex2D(baseSampler, uv));

	//----------------------------------------------------------------------------------------------------
	//Grey scale for monotone image
	float greyscale = dot(color.rgb, float3(0.3, 0.59, 0.11));
	color.rgb = greyscale;

	//----------------------------------------------------------------------------------------------------
	//Grain / interferance
	float4 g = grain(uv, grainstrength);
	color *= g;

	//----------------------------------------------------------------------------------------------------
	//Flickey image
	float flicker = max(1.0, rand * 1.5);
	color.rgb *= flicker;

	//----------------------------------------------------------------------------------------------------
	//Simple scanlines 
	//float scanlines = 0.875 + (0.125 * sin(uv.y * 800.0));
	//color *= scanlines;

	//----------------------------------------------------------------------------------------------------
    float shadowspeed = 0.00005;
    float shadowrange = 0.35;
    float shadowheight = 50.0;
    
    float shadow = 1.0 - shadowrange + (shadowrange * sin((uv.y + (iGlobalTime * -shadowspeed)) * shadowheight));
    color *= shadow;
	
	//Tint the image form the source colour
	color.rgb *= (2 * input.Colour * input.Colour.a);
	
	return color;
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.
        VertexShader = compile vs_3_0 VertexShaderFunction();
        //PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}
