using System;

using Microsoft.Xna.Framework;

namespace Transition.Steering
{
    /// <summary>
    /// A class to allow homing style behaviour for objects.
    /// </summary>
    public static class HomingSystem
    {
        /// <summary>
        /// Turn to face a target.
        /// </summary>
        /// <param name="position">The position of the object that requires turning.</param>
        /// <param name="target">The position of the target.</param>
        /// <param name="angle">Out: The direction angle (in radians) between the object and target.</param>
        /// <param name="maxAngle">The maximum angle (in radians) the object can turn.</param>
        public static void FaceTarget(Vector2 position, Vector2 target, ref float angle, float maxAngle)
	    {
		    //Get the angle required to face the target
		    var angleToTarget = 0.0f;
            GetAngleToTarget(position, target, ref angleToTarget);

		    //Calculate the difference between the spinner angle and the target angle and fix
		    //e.g.  > +PI becomes some negative -PI
            var fixedDifference = FixRadianAngle(angle - angleToTarget);

		    //Get the turning step size 
            if (maxAngle > Math.Abs(fixedDifference))
                angle = angleToTarget;
		    else 
            {
			    //Determine the direction of the turn (clockwise or anti-cockwise)
                if (fixedDifference < 0)
                    angle = (angle + maxAngle); //anti-clockwise
			    else
                    angle = (angle - maxAngle); //clockwise
		    }

		    //Fix the angle 
            angle = FixRadianAngle(angle);
	    }

        /// <summary>
        /// Drunkly turn to face a target.
        /// </summary>
        /// <param name="position">The position of the object that requires turning.</param>
        /// <param name="target">The position of the target.</param>
        /// <param name="angle">Out: The direction angle (in radians) between the object and target.</param>
        /// <param name="maxAngle">The maximum angle (in radians) the object can turn.</param>
        /// <remarks>The object may overshoot the target (intended, as if the object is drunk)</remarks>
	    public static void FaceTargetDrunken(Vector2 position, Vector2 target, ref float angle, float maxAngle)
	    {
		    //Get the angle required to face the target
		    var angleToTarget = 0.0f;
            GetAngleToTarget(position, target, ref angleToTarget);

		    //Calculate the difference between the spinner angle and the target angle and fix
		    //e.g.  > +PI becomes some negative -PI
            var fixedDifference = FixRadianAngle(angle - angleToTarget);

		    //Determine the direction of the turn (clockwise or anti-cockwise)
            if (fixedDifference < 0)
                angle = (angle + maxAngle); //anti-clockwise
		    else
                angle = (angle - maxAngle); //clockwise

		    //Fix the angle 
            angle = FixRadianAngle(angle);
	    }

		/// <summary>
		/// Returns the direction to the player's ship
		/// </summary>
        public static bool FacePlayerShip(TransitionGame game, Vector2 position, ref float direction, float maxAngle)
        {
			var shipPosition = GetShipPosition(game);
			FaceTarget(position, shipPosition, ref direction, maxAngle);
			
			return true;
        }

        /// <summary>
		/// Returns the direction to the player's ship as if the actor is drunk.
		/// </summary>
        public static bool FacePlayerShip(TransitionGame game, Vector2 position, ref float direction, float maxAngle, Vector2 targetOffset)
        {
			var shipPosition = GetShipPosition(game);
            shipPosition += targetOffset;
			FaceTarget(position, shipPosition, ref direction, maxAngle);
						
            return true;
        }

		/// <summary>
		/// Returns the direction to the player's ship as if the actor is drunk.
		/// </summary>
        public static bool FacePlayerShipDrunken(TransitionGame game, Vector2 position, ref float direction, float maxAngle)
        {
			var shipPosition = GetShipPosition(game);
			FaceTargetDrunken(position, shipPosition, ref direction, maxAngle);
						
            return true;
        }

        /// <summary>
		/// Returns the direction to the player's ship as if the actor is drunk.
		/// </summary>
        public static bool FacePlayerShipDrunken(TransitionGame game, Vector2 position, ref float direction, float maxAngle, Vector2 targetOffset)
        {			
			var shipPosition = GetShipPosition(game);
            shipPosition += targetOffset;
			FaceTargetDrunken(position, shipPosition, ref direction, maxAngle);
						
            return true;
        }

        public static Vector2 GetShipPosition(TransitionGame game)
        {
            return game.ActivePlayer.Ship.Position;
        }

        /// <summary>
        /// Gets the angle (in radians) between two objects.
        /// </summary>
        /// <param name="position">The position of the object that requires turning.</param>
        /// <param name="target">The position of the target.</param>
        /// <param name="angle">Out: The direction angle (in radians) between the object and target.</param>
	    public static void GetAngleToTarget(Vector2 position, Vector2 target, ref float angle)
	    {
		    //Get the angle of the target
            angle = (float)Math.Atan2(target.Y - position.Y, target.X - position.X);		
	    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static float FixRadianAngle(float angle)
        {
            //Handle invalid radian angle and fix it, valid range -PI > angle <= PI
            if (angle <= (float)-Math.PI)
                angle = (float)Math.PI + ((float)Math.PI + angle);
            else if (angle > Math.PI)
                angle = (float)-Math.PI - ((float)Math.PI - angle);

            return angle;
        }
    }
}
