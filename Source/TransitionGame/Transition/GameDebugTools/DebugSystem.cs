#region File Description
//-----------------------------------------------------------------------------
// DebugSystem.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

/*
 * To get started with the GameDebugTools, go to your main game class, override the Initialize method and add the
 * following line of code:
 * 
 * GameDebugTools.DebugSystem.Initialize(this, "MyFont");
 * 
 * where "MyFont" is the name a SpriteFont in your content project. This method will initialize all of the debug
 * tools and add the necessary components to your game. To begin instrumenting your game, add the following line of 
 * code to the top of your Update method:
 *
 * GameDebugTools.//DebugSystem.Instance.TimeRuler.StartFrame()
 * 
 * Once you have that in place, you can add markers throughout your game by surrounding your code with BeginMark and
 * EndMark calls of the TimeRuler. For example:
 * 
 * GameDebugTools.//////Gearset.GS.BeginMark("SomeCode", Color.Blue);
 * // Your code goes here
 * GameDebugTools.//////Gearset.GS.EndMark("SomeCode");
 * 
 * Then you can display these results by setting the Visible property of the TimeRuler to true. This will give you a
 * visual display you can use to profile your game for optimizations.
 *
 * The GameDebugTools also come with an FpsCounter and a DebugCommandUI, which allows you to type commands at runtime
 * and toggle the various displays as well as registering your own commands that enable you to alter your game without
 * having to restart.
 */
using Microsoft.Xna.Framework;
using PerformanceMeasuring.GameDebugTools;

namespace Transition.GameDebugTools
{
    /// <summary>
    /// DebugSystem is a helper class that streamlines the creation of the various GameDebug
    /// pieces. While games are free to add only the pieces they care about, DebugSystem allows
    /// games to quickly create and add all the components by calling the Initialize method.
    /// </summary>
    public class DebugSystem
    {
        private static DebugSystem singletonInstance;

        /// <summary>
        /// Gets the singleton instance of the debug system. You must call Initialize
        /// to create the instance.
        /// </summary>
        public static DebugSystem Instance
        {
            get { return singletonInstance; }
        }

        /// <summary>
        /// Gets the DebugManager for the system.
        /// </summary>
        public DebugManager DebugManager { get; private set; }

        /// <summary>
        /// 
        /// Gets the DebugCommandUI for the system.
        /// </summary>
        public DebugCommandUI DebugCommandUI { get; private set; }

        /// <summary>
        /// Gets the FpsCounter for the system.
        /// </summary>
        public GCCounter GCCounter { get; private set; }

        /// <summary>
        /// Gets the TimeRuler for the system.
        /// </summary>
        //public TimeRuler TimeRuler { get; private set; }

        /// <summary>
        /// Gets the OutputWindow for the system.
        /// </summary>
        public OutputWindow OutputWindow { get; private set; }

        /// <summary>
        /// Initializes the DebugSystem and adds all components to the game's Components collection.
        /// </summary>
        /// <param name="game">The game using the DebugSystem.</param>
        /// <param name="debugFont">The font to use by the DebugSystem.</param>
        /// <returns>The DebugSystem for the game to use.</returns>
        public static DebugSystem Initialize(Game game, string debugFont)
        {
            // if the singleton exists, return that; we don't want two systems being created for a game
            if (singletonInstance != null)
                return singletonInstance;

            // Create the system
            singletonInstance = new DebugSystem();

            // Create all of the system components
            singletonInstance.DebugManager = new DebugManager(game, debugFont);
            singletonInstance.DebugCommandUI = new DebugCommandUI(game);
            singletonInstance.GCCounter = new GCCounter(game);
            singletonInstance.OutputWindow = new OutputWindow(game);

#if PROFILE || DEBUG
            game.Components.Add(singletonInstance.DebugManager);
            game.Components.Add(singletonInstance.GCCounter);
            game.Components.Add(singletonInstance.OutputWindow);
            game.Components.Add(singletonInstance.DebugCommandUI);
#endif
            
            //Register 'fps' command if debug command is registered as a service.
            var host = game.Services.GetService(typeof(IDebugCommandHost)) as IDebugCommandHost;

            if (host != null)
            {
                host.RegisterCommand("debug", "Toggle Debug Systems [on/off]", (commandHost, command, arguments) => {
                    
                    //Get the visibility of all our systems - if any are visible we'll set the default to true.
                    var visible = singletonInstance.GCCounter.Visible || singletonInstance.OutputWindow.Visible;
                    
                    if (arguments.Count == 0)
                        visible = !visible;

                    foreach (string arg in arguments)
                    {
                        switch (arg.ToLower())
                        {
                            case "on":
                                visible = true;
                                break;
                            case "off":
                                visible = false;
                                break;
                        }
                    }

                    singletonInstance.DebugManager.Visible = visible;
                    singletonInstance.GCCounter.Visible = visible;
                    singletonInstance.OutputWindow.Visible = visible;
                });
            }

            #if PROFILE || DEBUG
                singletonInstance.GCCounter.Visible = true;
                singletonInstance.OutputWindow.Visible = true;

                singletonInstance.OutputWindow.Bounds.X = game.GraphicsDevice.Viewport.TitleSafeArea.X + 8;
                singletonInstance.OutputWindow.Bounds.Width = (int)(game.GraphicsDevice.Viewport.TitleSafeArea.Width * 0.65f);
            #endif

            return singletonInstance;
        }

        // Private constructor; games should use Initialize
        private DebugSystem() { }
    }
}
