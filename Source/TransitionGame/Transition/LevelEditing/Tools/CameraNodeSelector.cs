using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class CameraNodeSelector : Selector
    {
        public CameraNodeSelector(SelectionTool selectionTool) : base(selectionTool) {}

        public override int SelectionCount { get { return LevelEditorState.SelectedTileIds.Count; } } 

        public override SelectionTool.Feature MouseJustDownOverCanvas()
        {
            //Determine if we should move current selection or begin a new drag selection operation
            var selectionBounds = LevelEditor.GetNodeSelectionBounds();

            return selectionBounds.Contains(LevelEditor.WorldPosition) ? SelectionTool.Feature.Move : SelectionTool.Feature.Drag;
        }

        public override void Moving(Vector2 delta)
        {
            //Move all the selected nodes
            foreach (var nodeId in LevelEditorState.SelectedNodeIds)
            {
                if (nodeId > 0)
                    LevelEditor.Course.CameraPath.Vertices[nodeId - 1] += delta;

                LevelEditor.Course.CameraPath.Vertices[nodeId] += delta;

                if (nodeId < LevelEditor.Course.CameraPath.Vertices.Count - 1)
                    LevelEditor.Course.CameraPath.Vertices[nodeId + 1] += delta;
            }   
        }

        public override bool DragCompleted(bool isShiftDown, Rect selectionRect)
        {
            var oldSelectedNodeIds = new List<int>(LevelEditorState.SelectedNodeIds);

            if (isShiftDown == false)
                LevelEditorState.SelectedNodeIds.Clear();

            //Select all the things in the drag area
            for (var index = 0; index < LevelEditor.Course.CameraPath.Vertices.Count; index++)
            {
                if (index % 3 != 0)
                    continue;

                var node = LevelEditor.Course.CameraPath.Vertices[index];

                if (selectionRect.Contains(node) == false)
                    continue;

                if (LevelEditorState.SelectedNodeIds.Contains(index) == false)
                    LevelEditorState.SelectedNodeIds.Add(index);
            }

            var firstNotSecond = oldSelectedNodeIds.Except(LevelEditorState.SelectedNodeIds).ToList();
            var secondNotFirst = LevelEditorState.SelectedNodeIds.Except(oldSelectedNodeIds).ToList();

            return firstNotSecond.Count + secondNotFirst.Count > 0;
        }
    }
}
