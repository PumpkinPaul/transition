using Microsoft.Xna.Framework.Input;

namespace Transition.Input
{   
    /// <summary>
    /// Mapping between XINPUT and DirectInput gamepads.
    /// </summary>
    /// <remarks>
    /// We want a consistent approach to gamepad input from XINPUT and DirectInput. We'll specify the Buttons of the XBox360 gamepad as properties.
    /// Create instances of the class and set the values for the mappings according to the specific gamepads utilised.
    /// </remarks>
    /// <example>
    /// The game might want to perform some action when the 'B' button is pressed on the Xbox 360 gamepad. A player doesn't have a 360 pad but they have a PS one!
    /// So, they map the Playstation gamepad's CIRCLE button to B. Now the CIRCLE button may have been signed 9 by the driver software.
    /// 
    /// e.g.
    /// public int B = 9;
    /// ...Game Action -> 'B' -> Circle -> button 9
    /// </example>
    public class GamePadMapping
    {
        //Default button ids to the Xbox360 gamepad...

        public int A;
        public int B = 1;
        public int X = 2;
        public int Y = 3;
        public int LeftShoulder = 4;
        public int RightShoulder = 5;
        public int Back = 6;
        public int Start = 7;
        public int LeftStick = 8;
        public int RightStick = 9;
        public int BigButton = 10;

        //Analogue sticks
        public int LeftStickXAxis = (int)Nuclex.Input.Devices.ExtendedAxes.X;
        public int LeftStickYAxis = (int)Nuclex.Input.Devices.ExtendedAxes.Y;
        public bool LeftStickXAxisInvert;
        public bool LeftStickYAxisInvert;
        public GamePadDeadZone LeftStickDeadZone = GamePadDeadZone.IndependentAxes;

        public int RightStickXAxis = (int)Nuclex.Input.Devices.ExtendedAxes.RotationX;
        public int RightStickYAxis = (int)Nuclex.Input.Devices.ExtendedAxes.RotationY;
        public bool RightStickXAxisInvert;
        public bool RightStickYAxisInvert;
        public GamePadDeadZone RightStickDeadZone = GamePadDeadZone.IndependentAxes;
    }
}