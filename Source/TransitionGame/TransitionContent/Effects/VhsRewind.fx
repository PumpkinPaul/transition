//This effect utilises techniques found in the following shaders...
//https://www.shadertoy.com/view/ldjGzV
//https://www.shadertoy.com/view/Mds3zn

texture noiseTexture;
float time = 0.0f;

sampler TextureSampler : register(s0);
sampler NoiseSampler = sampler_state
{
    Texture = <noiseTexture>;
    
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;  
    AddressU = Wrap;
    AddressV = Wrap;  
};

float4x4 MatrixTransform;

void SpriteVertexShader(inout float4 color : COLOR0, inout float2 texCoord : TEXCOORD0, inout float4 position : SV_Position)
{
    position = mul(position, MatrixTransform);
}

float noise(float2 p)
{
    float sample = tex2D(NoiseSampler, float2(1.0, 2.0 * cos(time)) * time * 8.0 + p * 1.0).x;
    sample *= sample;
    return sample;
}

float onOff(float a, float b, float c)
{
    return step(c, sin(time + a * cos(time * b)));
}
  
float ramp(float y, float start, float end)
{
    float inside = step(start, y) - step(end, y);
    float fact = (y - start) / (end - start) * inside;
    return (1.0 - fact) * inside;
}

float stripes(float2 uv)
{
    float noi = noise(uv * float2(0.5, 1.0) + float2(1.0, 3.0));
    return ramp(fmod(uv.y * 2.0 + time / 2.0 + sin(time + sin(time * 0.63)), 1.0), 0.5, 0.6) * noi;
}

float3 chromaticAberation(float2 uv)
{
    float amount = 0.0;
	
    amount = (1.0 + sin(time * 6.0)) * 0.5;
    amount *= 1.0 + sin(time * 16.0) * 0.5;
    amount *= 1.0 + sin(time * 19.0) * 0.5;
    amount *= 1.0 + sin(time * 27.0) * 0.5;
    amount = pow(amount, 3.0);

    amount *= 0.0015;
	
    float3 col;
    col.r = tex2D(TextureSampler, float2(uv.x + amount, uv.y)).r;
    col.g = tex2D(TextureSampler, uv).g;
    col.b = tex2D(TextureSampler, float2(uv.x - amount, uv.y)).b;
	
    col *= (1.0 - amount * 0.5);
    
    return col;
}

float3 getVideo(float2 uv)
{
    float2 look = uv;
    float window = 1.0 / (1.0 + 20.0 * (look.y - fmod(time / 4.0, 1.0)) * (look.y - fmod(time / 4.0, 1.0)));
    window *= 0.25;
    look.x = look.x + sin(look.y * 10.0 + time) / 21.0 * onOff(4.0, 4.0, 0.3) * (1.0 + cos(time * 80.0)) * window;
    //float vShift = 0.4 * onOff(2.0, 3.0, 0.9) * (sin(time) * sin(time * 20.0) + (0.5 + 0.1 * sin(time * 200.0) * cos(time)));
    float vShift = 0.14 * onOff(2.0, 3.0, 0.9) * (sin(time) * sin(time * 2.0) + (0.15 + 0.1 * sin(time * 400.0) * cos(time)));
    look.y = fmod(look.y + vShift, 1.0);
    //float3 video = tex2D(TextureSampler, look).xyz;

    //float3 video = tex2D(TextureSampler, uv).xyz;
    //return video;

    //return chromaticAberation(uv);
    return chromaticAberation(look);
}
 
float2 fishEye(float2 uv)
{
    uv -= float2(0.5, 0.5);
    uv = uv * 1.2 * (1.0 / 1.2 + 2.0 * uv.x * uv.x * uv.y * uv.y);
    uv += float2(0.5, 0.5);
    return uv;
}

float4 PixelShaderFunction(float2 texCoord  : TEXCOORD0) : COLOR0
{		
    float2 uv = texCoord;
	
    uv = fishEye(uv);
    float3 video = getVideo(uv);
    float vigAmt = 3.0 + 0.3 * sin(time + 5.0 * cos(time * 5.0));
    float vignette = (1.0 - vigAmt * (uv.y - 0.5) * (uv.y - 0.5)) * (1.0 - vigAmt * (uv.x - 0.5) * (uv.x - 0.5));
	
    video += stripes(uv) / 2.0;
    video += noise(uv * 2.0) / 8.0;
    video *= vignette;
	//video *= (12.0 + fmod(uv.y * 30.0 + time, 1.0)) / 13.0;
		
    return float4(video, 1.0);
}

technique VhsRewind
{
	pass P0
	{
		#if SM4
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
		#elif SM2
		PixelShader = compile ps_2_0 PixelShaderFunction();
		#else
		PixelShader = compile ps_3_0 PixelShaderFunction();
		#endif

        VertexShader = compile vs_3_0 SpriteVertexShader();
    }
}