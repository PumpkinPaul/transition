//using System;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;

//namespace Transition
//{
//    /// <summary>
//    /// Represents a layer of scenery in the game world.
//    /// </summary>
//    /// <remarks>
//    /// Landscapes can consist of a number of different layers allowing for parallax scrolling
//    /// and are drawn using triangle strips.
//    /// </remarks>
//    public class WaterLandscapeLayer : LandscapeLayer
//    {
//        private const int PointCount = 200;

//        private const float WaterWidth = 40.0f;

//        //Spring constant for forces applied by adjacent points
//        private const float SpringConstant = 0.0005f;

//        //Sprint constant for force applied to baseline
//        private const float SpringConstantBaseline = 0.0005f;

//        //Damping to apply to speed changes
//        private const float Damping = 0.97f;

//        //Number of iterations of point-influences-point to do on wave per step
//        //(this makes the waves animate faster)
//        private const int Iterations = 3;
        
//        private readonly float _yPosition;

//        private class WaterPoint
//        {
//            public float X;
//            public float Y;
//            public float SpeedX;
//            public float SpeedY;
//            public float Mass; 
//        }

//        private readonly WaterPoint[] _waterPoints = new WaterPoint[PointCount];

//        //A phase difference to apply to each sine
//        private int _offset;

//        private const int NumBackgroundWaves = 7;
//        private const float BackgroundWaveMaxHeight = 0.075f;//0.125f;//0.075f;
//        private const float BackgroundWaveCompression = 2f;
//        //Amounts by which a particular sine is offset
//        private readonly float[] _sineOffsets = new float[NumBackgroundWaves];
//        //Amounts by which a particular sine is amplified
//        private readonly float[] _sineAmplitudes = new float[NumBackgroundWaves];
//        //Amounts by which a particular sine is stretched
//        private readonly float[] _sineStretches = new float[NumBackgroundWaves];
//        //Amounts by which a particular sine's offset is multiplied
//        private readonly float[] _offsetStretches = new float[NumBackgroundWaves];

//        private readonly Geometry _geometry;
//        private readonly TextureInfo _textureInfo;
//        private Color _tint;

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="game"></param>
//        /// <param name="landscape"></param>
//        /// <param name="layerId"></param>
//        /// <param name="forceMod"></param>
//        /// <param name="worldBounds"></param>
//        /// <param name="yPosition"></param>
//        public WaterLandscapeLayer(TransitionGame game, Landscape landscape, int layerId, Rect worldBounds, float forceMod, float yPosition) : base(game, landscape, layerId, worldBounds, forceMod)
//        {
//            _yPosition = yPosition;

//            _textureInfo = Game.TextureManager.GetTextureInfo("Water");
//            _geometry = new Geometry();
//            _geometry.TextureId = _textureInfo.TextureId;
//            //_geometry.TextureId = -1;
//            _geometry.RenderStyle = OpaqueRenderStyle.Instance;
          
//            _tint = ColourScheme.Colours[0].BaseColour.ToColor();
//            var hsl = ColourHelper.RgbToHsl(_tint);
//            hsl.H += RandomHelper.GetFloat(-8f, 8f);
//            //hsl.S += RandomHelper.GetFloat(-0.1f, 0.1f);
//            hsl.L += RandomHelper.GetFloat(-0.35f, -0.25f);
//            hsl.L = 0.6f;
                
//            _tint = ColourHelper.HslToRgb(hsl);
//            //_tint.A = 255;

//            CreateIndexData();

//            InitialiseWavePoints();
//            InitialiseBackgroundWaves();
//        }

//        /// <summary>
//        /// Initialises the array that tells us which elements from the render points array to draw (and in which order)
//        /// </summary>
//        private void CreateIndexData()
//        {
//            //Populate the array with references to indices in the vertex buffer
//            _geometry.Indices = new ushort[(PointCount - 1) * 6];
//            var vertex = 0;
//            for (var i = 0; i < _geometry.Indices.Length; i += 3)
//            {
//                if (i % 2 == 0) //even, so anticlockwise
//                {
//                    _geometry.Indices[i] = (ushort)(vertex);
//                    _geometry.Indices[i + 1] = (ushort)(vertex + 1);
//                    _geometry.Indices[i + 2] = (ushort)(vertex + 2);
//                }
//                else //odd, therefore clockwise
//                {
//                    _geometry.Indices[i] = (ushort)(vertex + 2);
//                    _geometry.Indices[i + 1] = (ushort)(vertex + 1);
//                    _geometry.Indices[i + 2] = (ushort)(vertex + 3);

//                    vertex += 2;
//                }
//            }
//        }

//        /// <summary>
//        /// Make points to go on the wave
//        /// </summary>
//        private void InitialiseWavePoints()
//        {
//            float startX = -20;
//            for(var i = 0; i < PointCount; i++)
//            {
//                //This represents a point on the wave
//                var newPoint = new WaterPoint {
//                    X    = startX + (i / (float)PointCount * WaterWidth),
//                    Y    = _yPosition,
//                    SpeedX = 0,
//                    SpeedY = 0,
//                    Mass = 1.0f
//                };
//                _waterPoints[i] = newPoint;
//            }
//        }

//        private void InitialiseBackgroundWaves()
//        {
//            //Set each sine's values to a reasonable random value
//            for(var i = 0; i < NumBackgroundWaves; i++)
//            {
//                _sineOffsets[i] = 0.00099999991f * (-1 + 2 * RandomHelper.GetFloat());
//                _sineAmplitudes[i] = RandomHelper.GetFloat() * BackgroundWaveMaxHeight;
//                _sineStretches[i] = RandomHelper.GetFloat() * BackgroundWaveCompression;
//                _offsetStretches[i] =  RandomHelper.GetFloat() * BackgroundWaveCompression;
//            }
//        }

//        /// <summary>
//        /// This function sums together the sines generated above, given an input value x
//        /// </summary>
//        /// <param name="x">.</param>
//        private float OverlapSines(float x)
//        {
//            var result = 0.0f;
//            for (var i = 0; i < NumBackgroundWaves; i++)
//                result += _sineOffsets[i] + _sineAmplitudes[i] * (float)Math.Sin(x * _sineStretches[i] + (0.125f * _offset) * _offsetStretches[i]);
    
//            return result * 0.7f;
//        }

//        /// <summary>
//        /// Update the landscape layer
//        /// </summary>
//        /// <param name="force">The force to apply to the world.</param>
//        public override void OnUpdate(float force)
//        {
//            _offset++;
//            UpdateWavePoints();

//            return;
//            for (var i = 0; i < PointCount; i++)
//            {
//                var p = _waterPoints[i];
//                p.X += force;

//                if (force < 0) 
//                {
//                    if (p.X < WorldBounds.Left)
//                        p.X += WorldSize;
//                } 
//                else if (force > 0)
//                {
//                    if (p.X > WorldBounds.Right)
//                        p.X -= WorldSize;
//                }
//            }
//        }

//        /// <summary>
//        /// Update the positions of each wave point
//        /// </summary>
//        private void UpdateWavePoints()
//        {
//            for (var i = 0; i < Iterations; i++)
//            {
//                for (var n = 0; n < PointCount; n++)
//                {
//                    var p = _waterPoints[n];
//                    //force to apply to this point
//                    var force = 0.0f;

//                    //forces caused by the point immediately to the left or the right
//                    float forceFromLeft;
//                    float forceFromRight;

//                    if (n == 0)
//                    {
//                        //wrap to left-to-right
//                        forceFromLeft = SpringConstant * _waterPoints[PointCount - 1].Y - p.Y;
//                    }
//                    else
//                    {
//                        //normally
//                        forceFromLeft = SpringConstant * _waterPoints[n-1].Y - p.Y;
//                    }

//                    if (n == PointCount - 1)
//                    {
//                        //wrap to right-to-left
//                        forceFromRight = SpringConstant * _waterPoints[0].Y - p.Y;
//                    }
//                    else
//                    {
//                        //normally
//                        forceFromRight = SpringConstant * _waterPoints[n + 1].Y - p.Y;
//                    }
                    
//                    //Also apply force toward the baseline
//                    var forceToBaseline = SpringConstantBaseline * _yPosition - p.Y;

//                    //Sum up forces
//                    force += forceFromLeft;
//                    force += forceFromRight;
//                    force += forceToBaseline;

//                    //Calculate acceleration
//                    var acceleration = force / p.Mass;

//                    //Apply acceleration (with damping)
//                    p.SpeedY = Damping * p.SpeedY + acceleration;

//                    //Apply speed
//                    p.Y += p.SpeedY;
//                }
//            }
//        }
					
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="viewMatrix"></param>
//        /// <param name="projectionMatrix"></param>
//        public override void Draw(Matrix viewMatrix, Matrix projectionMatrix)
//        {
//            //for(var i = 0; i < PointCount; i++)
//            //{
//            //    var p = _waterPoints[i];
//            //    var y = _yPosition + p.Y + OverlapSines(p.X);
//            //    var q = new QuadStruct(new Vector2(p.X, y), new Vector2(0.15f, 0.15f));
//            //    Game.GeometryBatch.DrawSprite(ref q, spriteImage.TextureId, spriteImage.SourceRect, tint.ToColor(), TransparentRenderStyle.Instance);
//            //}

//            var tex = new Vector2(0.0f, 0.0f);

//            _geometry.Vertices.Clear();
//            for(var i = 0; i < PointCount; i++)
//            {
//                var p = _waterPoints[i];
//                var y = _yPosition + p.Y + OverlapSines(p.X);

//                _geometry.Vertices.Add(new  VertexPositionColorTexture(new Vector3(p.X, _yPosition - 3.0f, 0.0f), _tint, new Vector2(0, _textureInfo.SourceRect.Bottom)));
//                _geometry.Vertices.Add(new  VertexPositionColorTexture(new Vector3(p.X, y, 0.0f), _tint, new Vector2(0, _textureInfo.SourceRect.Top)));
//            }

//            Game.GeometryBatch.DrawGeometry(_geometry);
//        }
//    }
//}
