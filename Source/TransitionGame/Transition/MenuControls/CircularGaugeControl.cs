using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.Actors;
using Transition.Rendering;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represent a circular gaugecontrol.
    /// </summary>
    public class CircularGaugeControl : MenuControl
    {			
        public float Value;
        
		/// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;
		
		/// <summary>The size of the font.</summary>
        public Vector2 FontSize;
				
		/// <summary>The text to render.</summary>
        public string Text;

        readonly List<Geometry<VertexPositionColorTextureFloat>> _geometry = new List<Geometry<VertexPositionColorTextureFloat>>();
        int _geometryId;

        public override bool AutoTabIndex { get { return true; } }

        /// <summary>
        /// Initialises a new instance of a TextButtonControl.
        /// </summary>
		/// <param name="game">A reference to the game.</param>
        public CircularGaugeControl(BaseGame game) : base(game)
		{
            Font = Theme.Font;
            FontSize = Theme.FontSize;

            _geometry.Add(CreateGeometry());
		}

        Geometry<VertexPositionColorTextureFloat> CreateGeometry()
        {
            return new Geometry<VertexPositionColorTextureFloat>
            {
                Indices = Renderer.QuadIndices,
                Layer = 0,
                TextureId = Game.TextureManager.GetTextureId("RingMask"),
                RenderStyle = CircleGaugeRenderStyle.Instance
            };
        }

        public override void Load(XElement element)
        {
            base.Load(element);
            
            Font = ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            Text = element.GetAttributeString("text");
            Value = element.GetAttributeSingle("value");
        }

        protected override bool OnIncreaseValue() 
        {
            Value += 0.05f;
            return true;
        }

        protected override bool OnDecreaseValue() 
        {
            Value -= 0.05f;
            return true;
        }
		
        public override Rect Bounds
		{
			get 
            {
                var bounds = Rect.Zero;// Font.GetBounds(Position, FontSize, Text, Alignment);

                return bounds;
            }
		}
	
		/// <summary>
        /// Allows a derived control to draw itself.
		/// </summary>
        int _lastDrawId = int.MinValue;
		protected override void OnDraw()
		{
            var tint = EffectiveTint;

            Game.Renderer.DrawNumber(Font, Position + new Vector2(0, 0.2f), FontSize * 1.25f, (int)(Value * 100), Alignment.Centre, tint, TransparentRenderStyle.Instance, Layer);
            Game.Renderer.DrawString(Font, Position + new Vector2(0, -0.6f), FontSize * 0.6f, Text, Alignment.Centre, tint, TransparentRenderStyle.Instance, Layer);

            var bounds = Font.GetBounds(Position + new Vector2(0, 0.2f), FontSize * 1.25f, ((int)(Value * 100)).ToString(), Alignment.Centre);
            //Game.Renderer.DrawString(Font, Position + new Vector2(0.7f, 0.55f), FontSize * 0.6f, "%", Alignment.TopLeft, tint, TransparentRenderStyle.Instance);
            Game.Renderer.DrawString(Font, new Vector2(bounds.Right + 0.1f, Position.Y + 0.45f), FontSize * 0.6f, "%", Alignment.CentreLeft, tint, TransparentRenderStyle.Instance, Layer);

            //Enable RepeaterControl support by ensuring multiple instance of THIS VERY CONTROL use unique geometry.
            var geometry = GetGeometry();
            geometry.Layer = Layer;

            // 1-----3
            // |\    |
            // |  \  |
            // |    \|
            // 0-----2
            var q = new Quad(Position, new Vector2(3.5f), 0);
            geometry.Vertices.Clear();
            geometry.Vertices.Add(new VertexPositionColorTextureFloat(q.BottomLeft, tint, new Vector2(0,0), Value));
            geometry.Vertices.Add(new VertexPositionColorTextureFloat(q.TopLeft, tint, new Vector2(0, 1), Value));
            geometry.Vertices.Add(new VertexPositionColorTextureFloat(q.BottomRight, tint, new Vector2(1, 0), Value));
            geometry.Vertices.Add(new VertexPositionColorTextureFloat(q.TopRight, tint, new Vector2(1, 1), Value));
            
            Game.Renderer.DrawGeometry(geometry);

            _lastDrawId = Game.DrawId;
            _geometryId++;
		}

        Geometry<VertexPositionColorTextureFloat> GetGeometry()
        {
            if (_lastDrawId != Game.DrawId)
            {
                _geometryId = 0;
            }
            else if (_geometryId >= _geometry.Count)
            {
                var geometry = CreateGeometry();

                _geometry.Add(geometry);
            }

            return _geometry[_geometryId];
        }
    }
}
