using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.PostProcess
{
    /// <summary>
    ///   Manages the scanlines effect.
    /// </summary>
    public class BloomProcessor : BaseProcessor
    {
        /// <summary>
        ///   The display settings the bloom should use.
        /// </summary>
        public BloomSettings Settings = BloomSettings.PresetSettings[0];

        /// <summary>
        /// </summary>
        private Effect _bloomCombineEffect;

        /// <summary>
        /// </summary>
        private Effect _bloomExtractEffect;

        /// <summary>
        /// </summary>
        private Effect _gaussianBlurEffect;

        /// <summary>
        /// </summary>
        private RenderTarget2D _renderTarget1;

        /// <summary>
        /// </summary>
        private RenderTarget2D _renderTarget2;

        /// <summary>
        /// </summary>
        private int _sampleCount;

        /// <summary>
        /// </summary>
        private Vector2[] _sampleOffsets;

        /// <summary>
        /// </summary>
        private float[] _sampleWeights;

        /// <summary>
        ///   Initialises a new instance of a <see cref = "BloomProcessor">BloomProcessor</see>.
        /// </summary>
        /// <param name = "game">A reference to the game.</param>
        public BloomProcessor(BaseGame game) : base(game)
        {
            Settings = BloomSettings.PresetSettings[7];
        }

        /// <summary>
        ///   Occurs when a derived effect needs to load content.
        /// </summary>
        protected override void OnLoadContent()
        {
            _bloomExtractEffect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/BloomExtract"));
            _bloomCombineEffect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/BloomCombine"));
            _gaussianBlurEffect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/GaussianBlur"));

            _sampleCount = _gaussianBlurEffect.Parameters["SampleWeights"].Elements.Count;

            //Create temporary arrays for computing our filter settings.
            _sampleWeights = new float[_sampleCount];
            _sampleOffsets = new Vector2[_sampleCount];

            ResolutionChanged();
        }

        /// <summary>
        ///   Occurs when a derived effect needs to load content.
        /// </summary>
        protected override void OnUnloadContent()
        {
            _bloomExtractEffect.Dispose();
            _bloomCombineEffect.Dispose();
            _gaussianBlurEffect.Dispose();

            _renderTarget1.Dispose();
            _renderTarget2.Dispose();
        }

        public override void ResolutionChanged()
        {
            //Look up the resolution and format of our main backbuffer.
            var pp = Game.GraphicsDevice.PresentationParameters;

            var width = pp.BackBufferWidth;
            var height = pp.BackBufferHeight;

            var format = pp.BackBufferFormat;

            //Create two rendertargets for the bloom processing. These are half the size of the backbuffer, in order to minimize fillrate costs. 
            //Reducing the resolution in this way doesn't hurt quality, because we are going to be blurring the bloom images in any case.
            width /= 2;
            height /= 2;

            _renderTarget1 = new RenderTarget2D(Game.GraphicsDevice, width, height, false, format, DepthFormat.None);
            _renderTarget2 = new RenderTarget2D(Game.GraphicsDevice, width, height, false, format, DepthFormat.None);
        }

        /// <summary>
        ///   Draws the post processing effect.
        /// </summary>
        /// <param name = "spriteBatch"></param>
        /// <param name = "deltaTime">Time passed since the last frame.</param>
        /// <param name = "texture">The texture to draw.</param>
        /// <param name = "renderTarget"></param>
        protected override bool OnDraw(SpriteBatch spriteBatch, float deltaTime, Texture2D texture, RenderTarget2D renderTarget)
        {
            Game.GraphicsDevice.SamplerStates[1] = SamplerState.PointClamp;

            //Pass 1: draw the scene into rendertarget 1, using a shader that extracts only the brightest parts of the image.
            _bloomExtractEffect.Parameters["BloomThreshold"].SetValue(Settings.BloomThreshold);
            DrawFullscreenQuad(spriteBatch, texture, _renderTarget1, _bloomExtractEffect);

            //Pass 2: draw from rendertarget 1 into rendertarget 2, using a shader to apply a horizontal gaussian blur filter.
            SetBlurEffectParameters(1.0f / _renderTarget1.Width, 0);
            DrawFullscreenQuad(spriteBatch, _renderTarget1, _renderTarget2, _gaussianBlurEffect);

            //Pass 3: draw from rendertarget 2 back into rendertarget 1, using a shader to apply a vertical gaussian blur filter.
            SetBlurEffectParameters(0, 1.0f / _renderTarget1.Height);
            DrawFullscreenQuad(spriteBatch, _renderTarget2, _renderTarget1, _gaussianBlurEffect);

            //Pass 4: draw both rendertarget 1 and the original scene image back into the main backbuffer, using a shader that
            //combines them to produce the final bloomed result.
            Game.GraphicsDevice.SetRenderTarget(renderTarget);

            var parameters = _bloomCombineEffect.Parameters;

            parameters["BloomIntensity"].SetValue(Settings.BloomIntensity);
            parameters["BaseIntensity"].SetValue(Settings.BaseIntensity);
            parameters["BloomSaturation"].SetValue(Settings.BloomSaturation);
            parameters["BaseSaturation"].SetValue(Settings.BaseSaturation);

            Game.GraphicsDevice.Textures[1] = texture;

            var viewport = Game.GraphicsDevice.Viewport;

            DrawFullscreenQuad(spriteBatch, _renderTarget1, viewport.Width, viewport.Height, _bloomCombineEffect);

            return true;
        }

        /// <summary>
        ///   Helper for drawing a texture into a rendertarget, using
        ///   a custom shader to apply postprocessing effects.
        /// </summary>
        /// <param name = "spriteBatch"></param>
        /// <param name = "texture">The texture to draw.</param>
        /// <param name = "renderTarget">The render target used to capture the result of the draw.</param>
        /// <param name = "effect">The effect to use to draw the texture.</param>
        private void DrawFullscreenQuad(SpriteBatch spriteBatch, Texture2D texture, RenderTarget2D renderTarget, Effect effect)
        {
            Game.GraphicsDevice.SetRenderTarget(renderTarget);

            DrawFullscreenQuad(spriteBatch, texture, renderTarget.Width, renderTarget.Height, effect);
        }

        /// <summary>
        ///   Helper for drawing a texture into the current rendertarget,
        ///   using a custom shader to apply postprocessing effects.
        /// </summary>
        /// <param name = "spriteBatch"></param>
        /// <param name = "texture">The tecture to draw.</param>
        /// <param name = "width">The width of the destination rectangle.</param>
        /// <param name = "height">The height of the destination rectangle.</param>
        /// <param name = "effect">The effect to use to draw the texture.</param>
        private static void DrawFullscreenQuad(SpriteBatch spriteBatch, Texture2D texture, int width, int height, Effect effect)
        {
            spriteBatch.Begin(0, BlendState.Opaque, null, null, null, effect);
            spriteBatch.Draw(texture, new Rectangle(0, 0, width, height), Color.White);
            spriteBatch.End();
        }

        /// <summary>
        ///   Computes sample weightings and texture coordinate offsets
        ///   for one pass of a separable gaussian blur filter.
        /// </summary>
        /// <param name = "dx">The number of pixels to offset the texture sample on the x-axis.</param>
        /// <param name = "dy">The number of pixels to offset the texture sample on the y-axis.</param>
        private void SetBlurEffectParameters(float dx, float dy)
        {
            //Look up the sample weight and offset effect parameters.
            var weightsParameter = _gaussianBlurEffect.Parameters["SampleWeights"];
            var offsetsParameter = _gaussianBlurEffect.Parameters["SampleOffsets"];

            //The first sample always has a zero offset.
            _sampleWeights[0] = ComputeGaussian(0);
            _sampleOffsets[0] = new Vector2(0);

            //Maintain a sum of all the weighting values.
            var totalWeights = _sampleWeights[0];

            //Add pairs of additional sample taps, positioned
            //along a line in both directions from the center.
            for (var i = 0; i < _sampleCount / 2; i++)
            {
                //Store weights for the positive and negative taps.
                var weight = ComputeGaussian(i + 1);

                _sampleWeights[i * 2 + 1] = weight;
                _sampleWeights[i * 2 + 2] = weight;

                totalWeights += weight * 2;

                //To get the maximum amount of blurring from a limited number of pixel shader samples, we take advantage of the bilinear filtering
                //hardware inside the texture fetch unit. If we position our texture coordinates exactly halfway between two texels, the filtering unit
                //will average them for us, giving two samples for the price of one. This allows us to step in units of two texels per sample, rather
                //than just one at a time. The 1.5 offset kicks things off by positioning us nicely in between two texels.
                var sampleOffset = i * 2 + 1.5f;

                var delta = new Vector2(dx, dy) * sampleOffset;

                //Store texture coordinate offsets for the positive and negative taps.
                _sampleOffsets[i * 2 + 1] = delta;
                _sampleOffsets[i * 2 + 2] = -delta;
            }

            //Normalize the list of sample weightings, so they will always sum to one.
            for (var i = 0; i < _sampleWeights.Length; i++)
            {
                _sampleWeights[i] /= totalWeights;
            }

            //Tell the effect about our new filter settings.
            weightsParameter.SetValue(_sampleWeights);
            offsetsParameter.SetValue(_sampleOffsets);
        }

        /// <summary>
        ///   Evaluates a single point on the gaussian falloff curve.
        ///   Used for setting up the blur filter weightings.
        /// </summary>
        /// <param name = "n"></param>
        private float ComputeGaussian(float n)
        {
            var theta = Settings.BlurAmount;

            return (float)((1.0 / Math.Sqrt(2 * Math.PI * theta)) * Math.Exp(-(n * n) / (2 * theta * theta)));
        }

        #region Nested type: BloomSettings

        /// <summary>
        ///   Class holds all the settings used to tweak the bloom effect.
        /// </summary>
        public class BloomSettings
        {
            /// <summary>
            ///   Table of preset bloom settings, used by the sample program.
            /// </summary>
            public static BloomSettings[] PresetSettings =
                {
                    //                Name           Thresh  Blur Bloom  Base  BloomSat BaseSat
                    new BloomSettings("OFF", 0.0f, 0, 0.0f, 0, 0, 0),
                    new BloomSettings("SUBTLE", 0.5f, 2, 1, 1, 1, 1),
                    new BloomSettings("MEDIUM", 0.25f, 4, 1.25f, 1, 1, 1),
                    new BloomSettings("SOFT", 0, 3, 1, 1, 1, 1),
                    new BloomSettings("HIGH", 0.05f, 4, 1.5f, 1, 1.5f, 1.5f),
                    new BloomSettings("DESATURATED", 0.5f, 8, 2, 1, 0, 1),
                    new BloomSettings("SATURATED", 0.25f, 4, 2, 1, 1.5f, 0.5f),
                    new BloomSettings("ENDING", 0.25f, 1f, 1f, 1f, 1f, 1f),
                    new BloomSettings("LANDSCAPE", 0.5f, 4f, 2f, 1.5f, 1.5f, 1.5f)
                };

            public readonly float BaseIntensity;

            public readonly float BaseSaturation;

            /// <summary>
            ///   Controls the amount of the bloom and base images that will be mixed into the final scene. Range 0 to 1.
            /// </summary>
            public readonly float BloomIntensity;

            /// <summary>
            ///   Independently control the color saturation of the bloom and base images. Zero is totally desaturated, 1.0 leaves saturation
            ///   unchanged, while higher values increase the saturation level.
            /// </summary>
            public readonly float BloomSaturation;

            /// <summary>
            ///   Controls how bright a pixel needs to be before it will bloom. Zero makes everything bloom equally, while higher values select
            ///   only brighter colors. Somewhere between 0.25 and 0.5 is good.
            /// </summary>
            public readonly float BloomThreshold;

            /// <summary>
            ///   Controls how much blurring is applied to the bloom image. The typical range is from 1 up to 10 or so.
            /// </summary>
            public readonly float BlurAmount;

            /// <summary>
            ///   Name of a preset bloom setting, for display to the user.
            /// </summary>
            public readonly string Name;

            /// <summary>
            ///   Initilises a new instance of a <see cref = "BloomSettings">BloomSettings</see>.
            /// </summary>
            public BloomSettings(string name, float bloomThreshold, float blurAmount, float bloomIntensity, float baseIntensity, float bloomSaturation, float baseSaturation)
            {
                Name = name;
                BloomThreshold = bloomThreshold;
                BlurAmount = blurAmount;
                BloomIntensity = bloomIntensity;
                BaseIntensity = baseIntensity;
                BloomSaturation = bloomSaturation;
                BaseSaturation = baseSaturation;
            }
        }

        #endregion
    }
}