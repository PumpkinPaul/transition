using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Transition
{
    public class CrashDebugGame : Game
    {
        private SpriteBatch _spriteBatch;
        private SpriteFont _font;
        private readonly Exception _exception;

        public CrashDebugGame(Exception exception)
        {
            _exception = exception;
            var graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
        }

        protected override void LoadContent()
        {
            _font = Content.Load<SpriteFont>("SpriteFonts\\Font");
            _spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)// || GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed)
                Exit();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            _spriteBatch.Begin();
            _spriteBatch.DrawString(_font, "**** Error ****", new Vector2(100f, 100f), Color.White);
            _spriteBatch.DrawString(_font, "Press Back to Exit", new Vector2(100f, 120f), Color.White);
            _spriteBatch.DrawString(_font, $"Exception: {_exception.Message}", new Vector2(100f, 140f), Color.Red);
            _spriteBatch.DrawString(_font, $"Stack Trace:\n{_exception.StackTrace}", new Vector2(100f, 160f), Color.White);
            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
