
texture scanlineTexture;
float repeatY = 1.0f;
float value = 1.0f;

sampler TextureSampler;

sampler ScanlineSampler = sampler_state
{
    Texture = <scanlineTexture>;
    
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;  
    AddressU = Wrap;
    AddressV = Wrap;  
};

void VS(float4 inPosition : Position0, float2 inTex : TexCoord0, 
		out float4 oPosition : Position0, out float2 oTex : TexCoord0)
{
	oTex = inTex;
	oPosition = inPosition; 
}

float4 PixelShaderFunction(float2 texCoord  : TEXCOORD0) : COLOR0
{		
  	float4 colour = tex2D(TextureSampler, texCoord);
  	texCoord.y *= repeatY;
  	float4 alpha = tex2D(ScanlineSampler, texCoord);
  		
	colour.rgb *= lerp(1.0, alpha.a, value);
		
  	return colour;
}

technique Scanlines
{
	pass P0
	{
		#if SM4
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
		#elif SM3
		PixelShader = compile ps_3_0 PixelShaderFunction();
		#else
		PixelShader = compile ps_2_0 PixelShaderFunction();
		#endif
	}
}