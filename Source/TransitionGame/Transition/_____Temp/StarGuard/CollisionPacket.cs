using Transition._____Temp.StarGuard.Entities;

namespace Transition._____Temp.StarGuard
{
    public struct CollisionPacket
    {
        public Entity Entity1;
        public Entity Entity2;
        public float Time;

        public CollisionPacket(Entity entity1, Entity entity2, float time)
        {
            Entity1 = entity1;
            Entity2 = entity2;
            Time = time;
        }
    }
}
