﻿using System;
using Microsoft.Xna.Framework;
using Transition.Rendering;

namespace Transition._____Temp.RewindBinaryFormatter
{
    [Serializable]
    public class EnemyEntity : Entity
    {
        public float Velocity = -0.15f;

        public override void Update(bool wasRewinding)
        {
            Position.Y += Velocity;

            if (Position.Y < -10.0f)
            {
                Position.Y = -10.0f;
                Velocity = -Velocity;
            }
            else if (Position.Y > 10.0f)
            {
                Position.Y = 10.0f;
                Velocity = -Velocity;
            }
        }

        public override void Draw()
        {
            var q = new Quad(Position, Scale);
            var textureInfo = TransitionGame.Instance.TextureManager.GetTextureInfo("Pixel");
            TransitionGame.Instance.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, Color.Red, TransparentRenderStyle.Instance);
        }
    }
}