using System;
using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Represents a rectangle in 2d space.
    /// </summary>
    /// <remarks>
    /// Use this class in a perspective type world where 
    /// 
    ///                     +Y  
    ///                      |
    ///                      |
    ///                      |
    ///                      |0,0
    ///         -X --------------------- +X
    ///                      |
    ///                      |
    ///                      |
    ///                      |
    ///                     -Y
    /// The x,y position of the box relates to the left, bottom properties. Top is y + height.
    /// Hopefully this will make working in a 2d world with origin at lower left much easier than trying to retrofit the XNA Rectangle class where bottom > top.
    /// </remarks>
    public struct Box2
    {
        public static readonly Box2 Empty = new Box2();

		/// <summary>.</summary>
		public float X; //Left
        public float Y; //Bottom
        public float Width;
        public float Height;

        public float Top => Y + Height;
        public float Left => X;
        public float Bottom => Y;
        public float Right => X + Width;

        /// <summary>
        /// Creates a new instance of a Box2.
        /// </summary>
        public Box2(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Creates a new instance of a Box2.
        /// </summary>
        public Box2(Vector2 bottomLeft, float width, float height)
        {
            X = bottomLeft.X;
            Y = bottomLeft.Y;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Creates a new instance of a Box2.
        /// </summary>
        public Box2(Vector2 bottomLeft, Vector2 size)
        {
            X = bottomLeft.X;
            Y = bottomLeft.Y;
            Width = size.X;
            Height = size.Y;
        }

        /// <summary>
        /// Tests vectors for equality.
        /// </summary>
        /// <param name="value1">Source vector.</param><param name="value2">Source vector.</param>
        public static bool operator ==(Box2 value1, Box2 value2)
        {
            if (value1.X == value2.X && value1.Y == value2.Y && value1.Width == value2.Width && value1.Height == value2.Height)
                return true;

            return false;
        }

        /// <summary>
        /// Tests vectors for inequality.
        /// </summary>
        /// <param name="value1">Vector to compare.</param><param name="value2">Vector to compare.</param>
        public static bool operator !=(Box2 value1, Box2 value2)
        {
            if (value1.X == value2.X && value1.Y == value2.Y && value1.Width == value2.Width && value1.Height == value2.Height)
                return false;

            return true;
        }

        /// <summary>
        /// Determines whether the specified Object is equal to the Box2.
        /// </summary>
        /// <param name="other">The Object to compare with the current Box2.</param>
        public bool Equals(Box2 other)
        {
            if (X == other.X && Y == other.Y && Width == other.Width && Height == other.Height)
                return true;

            return false;
        }

        /// <summary>
        /// Returns a value that indicates whether the current instance is equal to a specified object.
        /// </summary>
        /// <param name="obj">Object to make the comparison with.</param>
        public override bool Equals(object obj)
        {
            var flag = false;
            if (obj is Box2)
                flag = Equals((Box2)obj);

            return flag;
        }

        /// <summary>
        /// Gets the hash code of the box object.
        /// </summary>
        public override int GetHashCode()
        {
            return X.GetHashCode() + Y.GetHashCode();
        }

        /// <summary>
        /// .
        /// </summary>
        public bool Contains(Vector2 position)
		{
            return (position.X >= Left && position.X <= Right && position.Y <= Top && position.Y >= Bottom);
		}

        public bool Overlaps(Box2 other)
        {
            return Left < other.Right && Right > other.Left && Top > other.Bottom && Bottom < other.Top;
        }

        public bool Overlaps(ref Box2 other)
        {
            return Left < other.Right && other.Right > other.Left && other.Top > other.Bottom && other.Bottom < other.Top;
        }

        public void Union(Box2 other)
        {
            X = Math.Min(X, other.X);
            Y = Math.Min(Y, other.Y);
            Width = Math.Max(Right, other.Right) - X;
            Height = Math.Max(Top, other.Top) - Y;
        }

        public Vector2 GetIntersectionDepth(Box2 rectB)
        {
            // Calculate half sizes.
            var halfWidthA = Width / 2.0f;
            var halfHeightA = Height / 2.0f;
            var halfWidthB = rectB.Width / 2.0f;
            var halfHeightB = rectB.Height / 2.0f;

            // Calculate centers.
            var centerA = new Vector2(X + halfWidthA, Y + halfHeightA);
            var centerB = new Vector2(rectB.X + halfWidthB, rectB.Y + halfHeightB);

            // Calculate current and minimum-non-intersecting distances between centers.
            var distanceX = centerA.X - centerB.X;
            var distanceY = centerA.Y - centerB.Y;
            var minDistanceX = halfWidthA + halfWidthB;
            var minDistanceY = halfHeightA + halfHeightB;

            // If we are not intersecting at all, return (0, 0).
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
                return Vector2.Zero;

            // Calculate and return intersection depths.
            var depthX = distanceX > 0 ? minDistanceX - distanceX : -minDistanceX - distanceX;
            var depthY = distanceY > 0 ? minDistanceY - distanceY : -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }

        public override string ToString()
        {
            return $"X:{X} Y:{Y} Width:{Width} Height:{Height}";
        }
    }
}
