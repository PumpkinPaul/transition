using System.Collections.Generic;
using System.Linq;

namespace Transition.LevelEditing
{
    /// <summary>
    /// Represents level state - used for loading, saving and undo
    /// </summary>
    public class LevelEditorState
    {
        public List<int> SelectedTileIds { get; private set; }
        public List<int> SelectedNodeIds { get; private set; }
        public List<int> SelectedLabelIds { get; private set; }

        public CourseSystem.Course Course { get; set; }

        public bool HasSelection { get { return SelectedTileIds.Count > 0 || SelectedLabelIds.Count > 0 || SelectedNodeIds.Count > 0; } }
        public int SelectionCount { get { return SelectedTileIds.Count + SelectedLabelIds.Count + SelectedNodeIds.Count; } }

        public LevelEditorState()
        {
            SelectedTileIds = new List<int>();
            SelectedNodeIds = new List<int>();
            SelectedLabelIds = new List<int>();
        }

        public void SelectAll()
        {
            ClearSelection();
            SelectedTileIds.AddRange(Enumerable.Range(0, Course.Tiles.Count).Select(i => i));
            SelectedLabelIds.AddRange(Enumerable.Range(0, Course.Labels.Count).Select(i => i));
            SelectedNodeIds.AddRange(Enumerable.Range(0, Course.CameraPath.Vertices.Count).Select(i => i));
        }

        public void ClearSelection()
        {
            SelectedTileIds.Clear();
            SelectedNodeIds.Clear();
            SelectedLabelIds.Clear();
        }
    }
}
