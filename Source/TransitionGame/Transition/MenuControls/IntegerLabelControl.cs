using System.Text;
using System.Xml.Linq;
using Transition.Extensions;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a label control for displaying integers.
    /// </summary>
    public class IntegerLabelControl : LabelControl
    {		
        int _value;
        
        /// <summary>The text to render.</summary>
        public int Value 
        {
            get { return _value; }
            set 
            {
                _stringBuilder.Length = 0;
                _stringBuilder.AppendNumber(value);
                _value = value;
            }
        }

        private readonly StringBuilder _stringBuilder = new StringBuilder();

		/// <summary>
		/// Initialises a new instance of a Label control.
		/// </summary>
		/// <param name="game">A reference to the game.</param>
        public IntegerLabelControl(BaseGame game) : base(game) { }

        public override void Load(XElement element)
        {
            base.Load(element);

            Value = element.GetAttributeInt32("value");
        }

        public override Rect Bounds
		{
			get { return Font.GetBounds(Position, FontSize, _stringBuilder, Alignment); }
		}

        /// <summary>
        /// Allows a derived control to draw itself.
		/// </summary>
		protected override void OnDraw()
		{
            var tint = Tint;
            tint *= Overlay.Opacity;

            Game.Renderer.DrawNumber(Font, Position, FontSize, Value, Alignment, tint, RenderStyle, Layer);
		}
    }
}
