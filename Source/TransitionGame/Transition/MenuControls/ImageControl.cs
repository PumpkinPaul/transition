using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>Represents a control for displaying images.</summary>
    public class ImageControl : MenuControl
    {
        /// <summary>The angle of rotation around the z-axis (in radians).</summary>
        public float Rotation;

        /// <summary>The size of the image in world units.</summary>
        public Vector2 Size;

        /// <summary>The id of the texture to use for drawing.</summary>
        public TextureInfo TextureInfo;

        /// <summary>Initialises a new instance of an ImageControl.</summary>
        /// <param name = "game">A reference to the game.</param>
        public ImageControl(BaseGame game) : base(game) 
        {
            Size = Vector2.One;
            Tint = Color.White;
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            TextureInfo = ParseTexture(element, "textureInfo", TextureInfo);
            Size = element.GetAttributeVector2("size");
            Rotation = element.GetAttributeSingle("rotation", Rotation);
        }

        /// <summary>Gets the control bounds (a rectangle defining the control's limits).</summary>
        public override Rect Bounds => new Rect(Position - (Size / 2.0f), Size);

        /// <summary>Allows a derived control to draw itself.</summary>
        protected override void OnDraw()
        {
            var tint = Tint;
            tint *= Overlay.Opacity;

            var q = new Quad(Position, Size, Rotation);
            Game.Renderer.DrawSprite(ref q, TextureInfo.TextureId, TextureInfo.SourceRect, tint, RenderStyle, Layer);
        }
    }
}