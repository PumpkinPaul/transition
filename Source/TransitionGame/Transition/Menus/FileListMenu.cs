using System.Collections.Generic;
using System.IO;
using System.Linq;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents a menu for displaying a list of files
    /// </summary>
    public class FileListMenu : Menu
    {
        public string FileName 
        {
            get { return _fileListBox.SelectedText; }
        }

        //Controls...
        private ListBoxControl _fileListBox; 

        TextButtonControl _okButton;
        TextButtonControl _cancelButton;

        /// <summary>
        /// Initialises a new instance of a FileListMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public FileListMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            _fileListBox = GetControl<ListBoxControl>("fileListBox");

            _okButton = GetControl<TextButtonControl>("okButton");
            _okButton.Click += (sender, e) => Close();

            _cancelButton = GetControl<TextButtonControl>("cancelButton");
            _cancelButton.Click += (sender, e) => BackPressed();
        }

        public void SetFiles(IEnumerable<string> items)
        {
            _fileListBox.ClearOptions();
            foreach(var item in items)
                _fileListBox.AddOption(item);

            if (items.Any())
                _fileListBox.SelectedIndex = 0;
        }
    }
}
