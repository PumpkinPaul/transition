using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public class ParticleSystemTemplate
    {
        public string Name;
        public Texture2D Texture;
        public int TextureId;
        public TextureInfo TextureInfo;
        public IParticleEmitter Emitter;

        public float LifetimeMin;
        public float LifetimeMax;

        public float GravityStartMinX;
        public float GravityStartMaxX;
        public float GravityStartMinY;
        public float GravityStartMaxY;

        public float GravityEndMinX;
        public float GravityEndMaxX;
        public float GravityEndMinY;
        public float GravityEndMaxY;

        public float EmissionCone = MathHelper.TwoPi;
        public float MaxEmissionRate;
        public float MinEmissionRate;
        public float EmissionResidue;

        public float SizeMinX;
        public float SizeMinY;
        public float SizeMaxX;
        public float SizeMaxY;

        public float LinearSpeedMin;
        public float LinearSpeedMax;
        public float LinearDamping = 1.0f;

        public float AngularSpeedMin;
        public float AngularSpeedMax;
        public float AngularDamping = 1.0f;

        //public List<ParticleValueBand> ValueBands = new List<ParticleValueBand>();

        public List<ColorBand> ColorBands = new List<ColorBand>();
        public List<SizeBand> SizeBands = new List<SizeBand>();
        public List<ForceBand> ForceBands = new List<ForceBand>();

        public bool SymetricalParticles;
        public bool DirectionalParticles;

        public bool InheritTint;

        public bool BoundsTop;
        public bool BoundsLeft;
        public bool BoundsBottom;
        public bool BoundsRight;
        public bool Bounded;
    }
}
