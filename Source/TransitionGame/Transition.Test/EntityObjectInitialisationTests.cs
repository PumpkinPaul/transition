﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Xna.Framework.Audio;
using NUnit.Framework;
using Transition.Animations;
using Transition.ResourceManagers;
using Transition._____Temp.StarGuard;
using Transition._____Temp.StarGuard.Entities;
using Transition._____Temp.StarGuard.EntityBlueprints;
using StarGuardPlayer = Transition._____Temp.StarGuard.Entities.Player;

namespace Transition.Test 
{    
    [TestFixture, RequiresSTA, Category(TestCategory.IntegrationTests)]  
    public class EntityInitialisationTests 
    {
        const string DefaultNameForType = null;

        EntityFactory _entityFactory;

        static readonly Random Random = new Random();

        [TestFixtureSetUp]
        public void Initialise()
        {
            TestHelper.Initialise();
            TransitionGame.Instance.DataManager.LoadContent();

            _entityFactory = TestHelper.InitialiseEntityFactory();
        }
        
        [TestCase]
        public void TestPlayerInitialization()
        {
            const string name = "Ship1";

            var ignoredTypes = new HashSet<Type> { typeof(Weapon) };

            var manglers = new Dictionary<Type, Func<object>>();

            AddEnumMangler(typeof(StarGuardPlayer.Phase), manglers);
            AddActorDataMangler<ShipBlueprint>(manglers);

            var object1 = _entityFactory.Create<StarGuardPlayer>(name);
            var object2 = _entityFactory.Create<StarGuardPlayer>(name);

            TestInitialization(object1, object2, ignoredTypes, manglers, name);
        }

        [TestCase]
        public void TestScientistInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            AddEnumMangler(typeof(Scientist.Phase), manglers);
            
            var object1 = _entityFactory.Create<Scientist>();
            var object2 = _entityFactory.Create<Scientist>();

            var randomAtrtributes = new List<string> { "_walkSpeed", "_chanceOfThinking" };

            TestInitialization(object1, object2, null, manglers, DefaultNameForType, randomAtrtributes);
        }

        [TestCase]
        public void TestCameraProxyInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            AddEnumMangler(typeof(CameraProxy.Phase), manglers);

            var object1 = _entityFactory.Create<CameraProxy>();
            var object2 = _entityFactory.Create<CameraProxy>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestFlagInitialization()
        {
            var object1 = _entityFactory.Create<Flag>();
            var object2 = _entityFactory.Create<Flag>();

            TestInitialization(object1, object2, null, null, DefaultNameForType);
        }

        [TestCase]
        public void TestBulletInitialization()
        {
            var object1 = _entityFactory.Create<Bullet>();
            var object2 = _entityFactory.Create<Bullet>();

            TestInitialization(object1, object2, null, null, DefaultNameForType);
        }

        [TestCase]
        public void TestEnemyBulletInitialization()
        {
            const string name = "TrooperBullet";

            var object1 = _entityFactory.Create<EnemyBullet>(name);
            var object2 = _entityFactory.Create<EnemyBullet>(name);

            TestInitialization(object1, object2, null, null, name);
        }

        [TestCase]
        public void TestTrooperInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            AddEnumMangler(typeof(Trooper.Phase), manglers);

            var object1 = _entityFactory.Create<Trooper>();
            var object2 = _entityFactory.Create<Trooper>();

            var randomAtrtributes = new List<string> { "_walkSpeed", "_chanceOfCrouching", "_chanceOfRepeatShooting" };

            TestInitialization(object1, object2, null, manglers, DefaultNameForType, randomAtrtributes);
        }

        [TestCase]
        public void TestZomboidInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            AddEnumMangler(typeof(Zomboid.Phase), manglers);

            var object1 = _entityFactory.Create<Zomboid>();
            var object2 = _entityFactory.Create<Zomboid>();

            var randomAtrtributes = new List<string> { "_walkSpeed", "_runSpeed", "_maxSpeedX", "_jumpSpeed", "_chanceOfThinking", "_runAndTurnThreshold" };

            TestInitialization(object1, object2, null, manglers, DefaultNameForType, randomAtrtributes);
        }

        [TestCase]
        public void TestWeaponInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            AddEnumMangler(typeof(Weapon.Phase), manglers);

            var object1 = _entityFactory.Create<Weapon>();
            var object2 = _entityFactory.Create<Weapon>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestParticleSystemInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<ParticleSystem>();
            var object2 = _entityFactory.Create<ParticleSystem>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestSoundInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var ignoredTypes = new HashSet<Type> { typeof(DynamicSoundEffectInstance) };

            var object1 = _entityFactory.Create<Sound>();
            var object2 = _entityFactory.Create<Sound>();

            TestInitialization(object1, object2, ignoredTypes, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestMovingBlockInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<MovingBlock>();
            var object2 = _entityFactory.Create<MovingBlock>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestMineInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<Mine>();
            var object2 = _entityFactory.Create<Mine>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestStalactiteInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();
            AddEnumMangler(typeof(Stalactite.Phase), manglers);

            var object1 = _entityFactory.Create<Stalactite>();
            var object2 = _entityFactory.Create<Stalactite>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestTurretInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<Turret>();
            var object2 = _entityFactory.Create<Turret>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestTripleGunInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<TripleGun>();
            var object2 = _entityFactory.Create<TripleGun>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestFlierInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<Flier>();
            var object2 = _entityFactory.Create<Flier>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestRhinoInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();
            AddEnumMangler(typeof(Rhino.Phase), manglers);

            var object1 = _entityFactory.Create<Rhino>();
            var object2 = _entityFactory.Create<Rhino>();

            var randomAtrtributes = new List<string> { "_walkSpeed", "_chargeSpeed", "_maxSpeedX", "_jumpSpeed", "_chanceOfThinking", "_runAndTurnThreshold" };

            TestInitialization(object1, object2, null, manglers, DefaultNameForType, randomAtrtributes);
        }

        [TestCase]
        public void TestOctopusInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<Octopus>();
            var object2 = _entityFactory.Create<Octopus>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestExplosiveBlockInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();
            AddEnumMangler(typeof(ExplosiveBlock.Phase), manglers);

            var object1 = _entityFactory.Create<ExplosiveBlock>();
            var object2 = _entityFactory.Create<ExplosiveBlock>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestEliteTrooperInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();
            AddEnumMangler(typeof(EliteTrooper.Phase), manglers);

            var object1 = _entityFactory.Create<EliteTrooper>();
            var object2 = _entityFactory.Create<EliteTrooper>();

            var randomAtrtributes = new List<string> { "_walkSpeed", "_chanceOfCrouching", "_chanceOfRepeatShooting" };

            TestInitialization(object1, object2, null, manglers, DefaultNameForType, randomAtrtributes);
        }

        [TestCase]
        public void TestDestructibleBlockInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<DestructibleBlock>();
            var object2 = _entityFactory.Create<DestructibleBlock>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        [TestCase]
        public void TestEliteJumperInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<EliteJumper>();
            var object2 = _entityFactory.Create<EliteJumper>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        //[TestCase]
        //public void TestLaserInitialization()
        //{
        //    var manglers = new Dictionary<Type, Func<object>>();

        //    var object1 = _entityFactory.Create<Laser>();
        //    var object2 = _entityFactory.Create<Laser>();

        //    TestInitialization(object1, object2, null, manglers);
        //}

        [TestCase]
        public void TestScoreLabelInitialization()
        {
            var manglers = new Dictionary<Type, Func<object>>();

            var object1 = _entityFactory.Create<ScoreLabel>();
            var object2 = _entityFactory.Create<ScoreLabel>();

            TestInitialization(object1, object2, null, manglers, DefaultNameForType);
        }

        static void TestInitialization<T>(T object1, T object2, HashSet<Type> ignoredTypes, Dictionary<Type, Func<object>> manglers, string name, List<string> randomAtrtributes = null) where T : Entity
        {
            var referenceTypes = new List<Type> { typeof(TransitionGame), typeof(EntityBlueprint) };
            var objectComparer = new ObjectComparer(referenceTypes);

            var ignore = new HashSet<Type> { typeof(TransitionGame) };

            manglers = manglers ?? new Dictionary<Type, Func<object>>();

            AddActorDataMangler<EntityBlueprint>(manglers);
            AddActorDataMangler<EnemyBlueprint>(manglers);
            AddActorDataMangler<EnemyBulletBlueprint>(manglers);
            AddListTMangler<Sprite>(manglers);

            if (ignoredTypes != null)
            {
                foreach (var type in ignoredTypes)
                    ignore.Add(type);
            }
            
            var objectMangler = new ObjectMangler(ignore, manglers);
            
            for (var i = 0; i < 5; i++)
            {
                objectMangler.Mangle(object2);
                object2.Initialise(name ?? typeof(T).Name);

                objectComparer.ResetComparisons();
                var equivalent = objectComparer.Equivalent(object1, object2, randomAtrtributes);
                Assert.True(equivalent, "Expected objects of " + typeof(T) + " to be equivalent");
            }
        }

        static void AddEnumMangler(Type type, IDictionary<Type, Func<object>> manglers)
        {
            var random = new Random();

            var phaseValues = Enum.GetValues(type);
            manglers.Add(type, () => random.GetInt(0, phaseValues.Length - 1));
        }

        private static void AddListTMangler<T>(IDictionary<Type, Func<object>> manglers)
        {
            manglers.Add(typeof(List<T>), () => new List<T> {default(T)});
        }

        private static void AddActorDataMangler<T>(IDictionary<Type, Func<object>> manglers)
        {
            const BindingFlags bindingFlags = BindingFlags.NonPublic | BindingFlags.Instance;

            var type = typeof (T);

            //Get a field of the appropriate 'ActorData' type;
            var fields = typeof(EntityBlueprintManager).GetFields(bindingFlags);
            var field = fields.FirstOrDefault(f => f.FieldType == typeof(Dictionary<string, T>));

            var dictionary = field?.GetValue(TransitionGame.Instance.EntityBlueprints) as Dictionary<string, T>;
            if (dictionary == null)
                throw new InvalidCastException($"dictonary not found for type: {type.Name}");

            var index = Random.GetInt(0, dictionary.Count - 1);
            manglers.Add(type, () => dictionary.Values.ElementAt(index));
        }
    }
}
