using Microsoft.Xna.Framework;

namespace Transition.ActorDatas
{
    /// <summary>
    /// Configuration for simple trail effects.
    /// </summary>
    public class SimpleTrailEffectData
    {
        public Color HeadColor;
        public Color TailColor;
        public float HeadWidth;
        public float TailWidth;
        public float MaxLength;
    }
}
