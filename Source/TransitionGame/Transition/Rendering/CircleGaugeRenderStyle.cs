using Microsoft.Xna.Framework.Graphics;
using Transition.ResourceManagers;

namespace Transition.Rendering
{
    /// <summary>
    /// A style to render 'hit' effects.
    /// </summary>
    public class CircleGaugeRenderStyle : RenderStyle
    {
        public static CircleGaugeRenderStyle Instance;

        public override RenderStyleType RenderStyleId => RenderStyleType.CircleGauge;

        protected override void OnRegister(EffectManager effectManager)
        {
            Instance = this;
            Effect = effectManager.GetEffect("CircularGauge");
        }

        public override void Setup(GraphicsDevice graphicsDevice)
        {
            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            graphicsDevice.Textures[2] = TransitionGame.Instance.TextureManager.GetTexture("ConicalGradient");
        }
    }
}
