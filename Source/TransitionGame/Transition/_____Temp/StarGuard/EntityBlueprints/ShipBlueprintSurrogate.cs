using ProtoBuf;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
	/// <summary>
	/// Configuration data surrogate for serialization.
	/// </summary>
    [ProtoContract]
    public class ShipBlueprintSurrogate
    {
        [ProtoMember(1)]
        public string Name;

        static readonly ShipBlueprintSurrogate Instance = new ShipBlueprintSurrogate();

        public static implicit operator ShipBlueprintSurrogate(ShipBlueprint value)
        {
            if (value == null)
                return null;

            Instance.Name = value.Name;
            return Instance;
        }

        public static implicit operator ShipBlueprint(ShipBlueprintSurrogate value)
        {
            if (value == null)
                return null;

            return TransitionGame.Instance.EntityBlueprints.ShipBlueprint(value.Name);
        }
    }
}
