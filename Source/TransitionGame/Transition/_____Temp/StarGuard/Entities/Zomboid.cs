﻿using System;
using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Extensions;
using Transition.Util;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// zomboid enemy (walks, reanimates)
    /// </summary>
    [ProtoContract]
    public class Zomboid : Enemy
    {
        const int MaxResurectionTicks = 60;

        const float MaxSpeedY = 0.35f;
        const float DyingYSpeed = 0.15f;

        public enum Phase
        {
            Uninitialised,
            Thinking,
            Walking,
            Running,
            Jumping,
            Dying,
            Dead,
            Resurecting,
        }

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        Phase _phase;

        [ProtoMember(2)]
        int _countdownTicks;

        [ProtoMember(3)]
        bool _isZombie;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here
        float _walkSpeed;// = 0.015f;
        float _runSpeed;// = 0.0395f;
        float _maxSpeedX;
        float _jumpSpeed;// = 0.275f;
        float _chanceOfThinking;// = 0.3f;
        float _runAndTurnThreshold;// = 3.0f;

        //----------------------------------------------------------------------------------------------------

        public Zomboid(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _phase = Phase.Uninitialised;
            _countdownTicks = 0;
            _isZombie = false;
           
            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() 
        { 
            RandomHelper.FastRandom.Reinitialise(Seed);
            _walkSpeed = RandomHelper.FastRandom.NextFloat(0.01f, 0.0175f);
            _runSpeed = RandomHelper.FastRandom.NextFloat(0.035f, 0.045f);
            _jumpSpeed = RandomHelper.FastRandom.NextFloat(0.275f, 0.285f);
            _chanceOfThinking = RandomHelper.FastRandom.NextFloat(0.25f, 0.35f);
            _runAndTurnThreshold = RandomHelper.FastRandom.Next(1, 4);

            _maxSpeedX = _runSpeed * 2;
        }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnAddedToWorld(World world)
        {
            BeginThinkingPhase();
        }

        bool CanSeePlayer
        {
            get
            {
                var bounds = GetBoundingBox();

                var player = Game.World.Get<Player>(EntityKeys.Player);
                var playerBounds = player.GetBoundingBox();

                return bounds.Y.FloatEquals(playerBounds.Y, 0.1f);
            }
        }

        void BeginThinkingPhase()
        {
            //Stop
            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-02.face-right.animation")
                : Game.AnimationManager.Get("enemy-02.face-left.animation"));

            _phase = Phase.Thinking;
            _countdownTicks = RandomHelper.Random.GetInt(60, 120);
            Velocity.X = 0.0f;
        }

        void BeginWalkingPhase()
        {
            _countdownTicks = RandomHelper.Random.GetInt(60, 120);

            if (_phase != Phase.Walking)
            {
                var previousVelocityX = Velocity.X;
                if (RandomHelper.Random.GetFloat() <= 0.5)
                    Velocity.X = _walkSpeed;
                else
                    Velocity.X = -_walkSpeed;

                if (previousVelocityX.FloatEquals(Velocity.X) == false)
                    IsFacingRight = Velocity.X > 0;

                Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-02.walk-right.animation")
                    : Game.AnimationManager.Get("enemy-02.walk-left.animation"));
            }

            _phase = Phase.Walking;
        }

        void TurnAround()
        {
            IsFacingRight = !IsFacingRight;
        }

        void BeginRunningPhase()
        {
            _phase = Phase.Running;

            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-02.run-right.animation")
                : Game.AnimationManager.Get("enemy-02.run-left.animation"));
        }

        void BeginDyingPhase(float force)
        {
            _countdownTicks = 60;
            HitTicks = _countdownTicks;
            _phase = Phase.Dying;
            if (OnGround)
                Velocity.Y += DyingYSpeed;
            Velocity.X += force;
            Velocity.X = MathHelper.Clamp(Velocity.X, -_runSpeed, _runSpeed);
            Collidable = false;

            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-02.face-right.animation")
                : Game.AnimationManager.Get("enemy-02.face-left.animation"));

            CreateScoreLabel();
        }

        void BeginDeadPhase()
        {
            _countdownTicks = RandomHelper.Random.GetInt(180, 300);
            HitTicks = _countdownTicks;
            _phase = Phase.Dead;
        }

        void BeginResurectingPhase()
        {
            _countdownTicks = MaxResurectionTicks;
            HitTicks = _countdownTicks;
            _isZombie = true;
            _phase = Phase.Resurecting;
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            switch (_phase)
            {
                case Phase.Thinking:
                    UpdateThinkingPhase();
                    break;

                case Phase.Walking:
                    UpdateWalkingPhase();
                    break;

                case Phase.Running:
                    UpdateRunningPhase();
                    break;

                case Phase.Jumping:
                    UpdateJumpingPhase();
                    break;

                case Phase.Dying:
                    UpdateDyingPhase();
                    break;

                case Phase.Dead:
                    UpdateDeadPhase();
                    break;

                case Phase.Resurecting:
                    UpdateResurectingPhase();
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(_phase), $"Unknown _phase: {_phase}");
            }

            Velocity.Y += TestPhaseStarGuard.Gravity;

            Velocity.X = MathHelper.Clamp(Velocity.X, -_maxSpeedX, _maxSpeedX);
            Velocity.Y = MathHelper.Clamp(Velocity.Y, -MaxSpeedY, MaxSpeedY);

            Game.GameMap.HandleCollisions(this);
        }

        void UpdateThinkingPhase()
        {
            if (CanSeePlayer)
            {
                BeginRunningPhase();
                return;
            }

            _countdownTicks--;
            if (_countdownTicks <= 0)
            {
                BeginWalkingPhase();
            }
        }

        void UpdateWalkingPhase()
        {
            if (CanSeePlayer)
            {
                BeginRunningPhase();
                return;
            }

            _countdownTicks--;
            if (_countdownTicks > 0)
                return;

            //Countdown expired - do something!

            //Only a 30% chance we'll stop and think
            if (RandomHelper.Random.GetFloat() <= _chanceOfThinking)
            {
                BeginThinkingPhase();
            }
            else
            {
                //Reset the walking phase
                BeginWalkingPhase();
            }
        }

        void UpdateRunningPhase()
        {
            var player = Game.World.Get<Player>(EntityKeys.Player);

            var previousVelocity = Velocity;

            //Determine if the zomboid should turn around 
            //+ve player is to right of zomboid
            //-ve player is to left of zomboid
            var positionDelta = player.Position.X - Position.X; 
            var turnThreshold = 0.0f;
            if ((IsFacingLeft && positionDelta > 0) || (IsFacingRight && positionDelta < 0))
                turnThreshold = _runAndTurnThreshold;

            if (player.Position.X - turnThreshold > Position.X)
            {
                Velocity.X = _runSpeed;
                IsFacingRight = true;
            }
            else if (player.Position.X + turnThreshold < Position.X)
            {
                Velocity.X = -_runSpeed;
                IsFacingRight = false;
            }

            if (previousVelocity.X.FloatEquals(Velocity.X) == false)
            {
                Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-02.run-right.animation")
                    : Game.AnimationManager.Get("enemy-02.run-left.animation"));
            }

            //We should jump if we are blocked by a wall in the direction we are travelling
            var directionSign = Math.Sign(Velocity.X);
            var lookAheadOffset = new Vector2(directionSign * GameMap.TileSize / 5.0f, 0.0f);
            var tileCollision = Game.GameMap.GetTileCollision(Position + new Vector2(directionSign * CollisionBounds.Width / 2, 0) + lookAheadOffset);
            if (tileCollision == GameMap.TileCollision.Impassable)
            {
                Jump();
            }

            //Jumping off platforms!
            var myFloor = Game.GameMap.GetTileCoordinates(Position);
            myFloor.Y--;

            var targetCoords = myFloor;
            if (Velocity.X > 0)
            {
                targetCoords.X++;
                if (Game.GameMap.GetTileCollision(myFloor) == GameMap.TileCollision.Passable && Game.GameMap.GetTileCollision(targetCoords) == GameMap.TileCollision.Passable)
                {
                    Jump();
                }
            }
            else if (Velocity.X < 0)
            {
                targetCoords.X--;
                if (Game.GameMap.GetTileCollision(myFloor) == GameMap.TileCollision.Passable && Game.GameMap.GetTileCollision(targetCoords) == GameMap.TileCollision.Passable)
                {
                    Jump();
                }
            }
        }

        void UpdateJumpingPhase()
        {
            if (OnGround)
            {
                //Imediately target the player on landing!
                var player = Game.World.Get<Player>(EntityKeys.Player);
                if (player.Position.X > Position.X && IsFacingLeft)
                    TurnAround();
                else if(player.Position.X < Position.X && IsFacingRight)
                    TurnAround();

                BeginRunningPhase();
            }
        }

        void UpdateDyingPhase()
        {
            if (OnGround)
            {
                Velocity.X *= 0.9f;
            }

            _countdownTicks--;
            if (_countdownTicks <= 0)
            {
                BeginDeadPhase();
            }
        }

        void UpdateDeadPhase()
        {
            if (OnGround)
            {
                Velocity.X *= 0.9f;
            }

            _countdownTicks--;
            if (_countdownTicks <= 0)
                BeginResurectingPhase();
        }

        void UpdateResurectingPhase()
        {
            _countdownTicks--;
            Visible = Math2.SquareWave(MaxResurectionTicks - _countdownTicks / (float)MaxResurectionTicks, 16U) > 0;
            if (_countdownTicks <= 0)
            {
                Collidable = true;
                Visible = true;
                Health = Blueprint.InitialHealth;
                BeginRunningPhase();
            }
        }

        void Jump()
        {
            _phase = Phase.Jumping;
            Velocity.Y += _jumpSpeed;

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-02.face-right.animation")
                    : Game.AnimationManager.Get("enemy-02.face-left.animation"));
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            if ((entityCollisionEdge & CollisionEdge.Left) != 0)
            {
                Velocity.X = BounceSpeed;
                if (_phase == Phase.Walking)
                {
                    IsFacingRight = true;
                    Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-02.walk-right.animation"));
                }
            }
            else if ((entityCollisionEdge & CollisionEdge.Right) != 0)
            {
                Velocity.X = -BounceSpeed;
                if (_phase == Phase.Walking)
                {
                    IsFacingRight = false;
                    Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-02.walk-left.animation"));
                }
            }
        }

        float BounceSpeed
        {
            get 
            {
                switch (_phase)
                {
                    case Phase.Walking: 
                        return _walkSpeed;

                    case Phase.Jumping:
                        return _runSpeed * 2;

                    default:
                        return 0.0f;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>true if we should be killed / deactivated; false otherwise.</returns>
        protected override bool OnKilled(Entity collider)
        {
            //Death is not final - we are a zomboid afterall
            if (_isZombie == false)
            {
                BeginDyingPhase(collider.Velocity.X);
                return false;
            }
            
            return true;
        }
    }
}