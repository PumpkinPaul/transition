{
  "InputKeys": {
    "Up": 38,
    "Down": 40,
    "Left": 37,
    "Right": 39,
    "Fire": 88,
    "Pause": 80,
    "UpKeyHint": "Up",
    "DownKeyHint": "Down",
    "LeftKeyHint": "Left",
    "RightKeyHint": "Right",
    "SelectKeyHint": "X",
    "FireKeyHint": "X",
    "PauseKeyHint": "P"
  },
  "Checkpoints": [],
  "FileName": "48B61DD8-0AE7-4d4c-9190-EF7FD87F8939",
  "Name": "Player 3",
  "Guid": "48B61DD8-0AE7-4d4c-9190-EF7FD87F8939",
  "ProfileType": 0,
  "SelectedShipName": "Ship1",
  "CompletedAchievements": [],
  "RequiresSave": false
}