using System;
using System.Collections.Generic;

namespace Transition
{
    /// <summary>
    /// A pool of items.
    /// </summary>
    public class Pool<T>
    {
        readonly Stack<T> _availableItems;
        readonly List<T> _allocatedItems;

        readonly bool _allowGrowing;

        readonly Func<T> _create;
        readonly Action<T> _init;

        public Pool(bool allowGrowing, int size, Func<T> create, Action<T> init)
        {
            if (create == null)
                throw new ArgumentNullException(nameof(create));

            _allowGrowing = allowGrowing;

            _create = create;
            _init = init;

            _availableItems = new Stack<T>(size);
            _allocatedItems = new List<T>(size);

            for (var i = 0; i < size; i++)
                _availableItems.Push(_create());

        }

        public T Allocate()
        {
            if (_availableItems.Count == 0)
            {
                if (_allowGrowing)
                    _availableItems.Push(_create());
                else
                    return default(T);
            }

            var item = _availableItems.Pop();

            _allocatedItems.Add(item);

            _init?.Invoke(item);

            return item;
        }

        public void Deallocate(T item)
        {
            _allocatedItems.Remove(item);
            _availableItems.Push(item);
        }

        public void DeallocateAllItems()
        {
            foreach (var item in _allocatedItems)
                _availableItems.Push(item);

            _allocatedItems.Clear();
        }
    }
}
