﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PerformanceMeasuring.GameDebugTools;

namespace Transition.GameDebugTools
{
    public struct Line
    {
        public Color Color;
        public string Text;
    }
 
    /// <summary>
    ///  Output Window class for Debug purpose.
    /// </summary>
    public class OutputWindow : DrawableGameComponent
    {
        /// <summary>
        /// Maximum lines that shows in Debug command window.
        /// </summary>
        public int MaxLineCount = 10;
 
        // Current command line string and cursor position.
        private readonly Queue<Line> _lines = new Queue<Line>();
 
        public Rectangle Bounds;
 
        // Selecting command history index.
        private DebugManager _debugManager;
 
        /// <summary>
        /// Constructor
        /// </summary>
        public OutputWindow(Game game) : base(game)
        {
            //Add this instance as a service.
            Game.Services.AddService(typeof(OutputWindow), this);
 
            //Draw the command UI on top of everything
            DrawOrder = int.MaxValue;

            #if PROFILE
                MaxLineCount = 5;
            #endif
        }
 
        /// <summary>
        /// Initialize component
        /// </summary>
        public override void Initialize()
        {
            _debugManager = Game.Services.GetService(typeof(DebugManager)) as DebugManager;
 
            if (_debugManager == null)
                throw new InvalidOperationException("Coudn't find DebugManager.");
 
            // Add "tr" command if DebugCommandHost is registered.
            var host = Game.Services.GetService(typeof(IDebugCommandHost)) as IDebugCommandHost;
 
            if (host != null)
            {
                host.RegisterCommand("output", "OutputWindow", CommandExecute);
                Visible = true;
            }
 
            base.Initialize();
 
            // Compute command window size and draw.
            float w = GraphicsDevice.Viewport.TitleSafeArea.Width;
            float h = GraphicsDevice.Viewport.TitleSafeArea.Height;
            const float topMargin = 32.0f;
            float leftMargin = w * 0.025f;
 
            Bounds = new Rectangle();
            Bounds.X = (int) leftMargin;
            Bounds.Y = (int) topMargin;
            Bounds.Width = (int) (w - Bounds.X - Bounds.X);
            Bounds.Height = (MaxLineCount * _debugManager.DebugFont.LineSpacing);
        }
 
        public void Echo(string text)
        {
            Echo(new Line{ Text = text, Color = Color.White });
        }
 
        public void Echo(string text, Color color)
        {
            Echo(new Line{ Text = text, Color = color });
        }
 
        public void Echo(Line line)
        {
            lock (_lines) {
                _lines.Enqueue(line);
                while (_lines.Count > MaxLineCount)
                    _lines.Dequeue();
            }
        }
 
        /// <summary>
        /// 'tr' command execution.
        /// </summary>
        public void CommandExecute(IDebugCommandHost host, string command, IList<string> arguments)
        {
            if (arguments.Count == 0)
                Visible = !Visible;
 
            var subArgSeparator = new[] { ':' };
            foreach (var orgArg in arguments)
            {
                var arg = orgArg.ToLower();
                var subargs = arg.Split(subArgSeparator);
                switch (subargs[0])
                {
                    case "on":
                        Visible = true;
                        break;
 
                    case "off":
                        Visible = false;
                        break;
 
                    case "cls":
                        ClearOutput();
                        break;
 
                    case "/?":
                    case "--help":
                        host.Echo("output");
                        host.Echo("output [on|off|cls]");
                        host.Echo("Options:");
                        host.Echo("       on     Display.");
                        host.Echo("       off    Hide.");
                        host.Echo("       cls    Clear.");
                        break;
                }
            }
        }
 
        /// <summary>
        /// Reset marker log.
        /// </summary>
        [Conditional("PROFILE")]
        public void ClearOutput()
        {
#if PROFILE
            lock (this)
            {
                _lines.Clear();
            }
#endif
        }
 
        public override void Draw(GameTime gameTime)
        {
            var font = _debugManager.DebugFont;
            var spriteBatch = _debugManager.SpriteBatch;
            var whiteTexture = _debugManager.WhiteTexture;
 
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null);
 
            spriteBatch.Draw(whiteTexture, Bounds, DebugSystem.Instance.DebugManager.OverlayColor);
 
            // Draw each lines.
            var pos = new Vector2(Bounds.X, Bounds.Y);
            lock(_lines) {
                foreach (var line in _lines)
                {
                    spriteBatch.DrawString(font, line.Text, pos, line.Color);
                    pos.Y += font.LineSpacing;
                }
            }
 
            spriteBatch.End();
        }
    }
}