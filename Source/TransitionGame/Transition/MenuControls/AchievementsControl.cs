//TODO: Transition

using System;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// A control for displaying the achivements for a profile.
    /// </summary>
    public class AchievementsControl : MenuControl
    {
        /// <summary>The x positions of the images and text.</summary>
        /// <remarks>The first 3 items are for the images and the last one is for the description of the achievement.</remarks>
        //protected readonly static float[] ColumnPositions = new float[] { 0.0f, 1.0f, 2.0f, 3.0f };
        private readonly static float[] ColumnPositions = { 0.0f, 2.0f, 4.0f, 6.0f };

        /// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;

        /// <summary>The size of the font.</summary>
		public Vector2 FontSize;

        /// <summary>The colour tint to apply to achievements that are still locked.</summary>
        public Color LockedTint;

        /// <summary>The profile whose achievements to display.</summary>
        public PlayerProfile Profile;

        Quad _quadStruct;

        readonly int _lockedMedalTextureId;
        readonly int _bronzeMedalTextureId;
        readonly int _silverMedalTextureId;
        readonly int _goldMedalTextureId;

		/// <summary>
        /// Initialises a new instance of an AchievementsControl.
		/// </summary>
		/// <param name="game">A reference to the game.</param>
        public AchievementsControl(BaseGame game) : base(game)
		{
            _lockedMedalTextureId = Game.TextureManager.GetTextureId("LockedMedal");
            _bronzeMedalTextureId = Game.TextureManager.GetTextureId("BronzeMedal");
            _silverMedalTextureId = Game.TextureManager.GetTextureId("SilverMedal");
            _goldMedalTextureId = Game.TextureManager.GetTextureId("GoldMedal");

            PageSize = 5;
		}

        /// <summary>
        /// Gets or sets the index of the current page of medals to show.
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Gets or sets the number of medal tasks to show at a time.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Allows a derived control to draw itself.
		/// </summary>
		protected override void OnDraw()
		{
            var lockedTint = LockedTint;
            var medalTint = Color.White;
            
            lockedTint *= Overlay.Opacity;
            medalTint  *= Overlay.Opacity;

            var position = Position;

            var firstIndex = PageIndex * PageSize;
            var lastIndex = Math.Min(firstIndex + PageSize, Profile.CompletedAchievements.Count);

            for (var i = firstIndex; i < lastIndex; ++i)
            {
                var achievement = Profile.CompletedAchievements[i];

                //Draw an icon for each difficulty level
                for (var difficultyId = 0; difficultyId < 3; ++difficultyId)
                {
                    //_quadStruct = new QuadStruct(position + new Vector2(AchievementsControl.ColumnPositions[i], 0.0f), new Vector2(1.3f), 0.0f);
                    _quadStruct = new Quad(position + new Vector2(ColumnPositions[difficultyId], 0.0f), new Vector2(2.0f), 0.0f);

                    //Work out which medals are unlocked based on the difficulty played
                    var textureId = _lockedMedalTextureId;

                    if ((int)achievement.Difficulty >= difficultyId)
                    {
                        switch (difficultyId)
                        {
                            case 0:
                                textureId = _bronzeMedalTextureId;
                                break;

                            case 1:
                                textureId = _silverMedalTextureId;
                                break;

                            case 2: 
                                textureId = _goldMedalTextureId;
                                break;

                        }
                    }

                    Game.Renderer.DrawSprite(ref _quadStruct, textureId, Rect.TextureRect, medalTint, RenderStyle, Layer);
                }

                //Draw the description for the achievement
                position.X = Position.X + ColumnPositions[3];

                var tint = lockedTint;

                Game.Renderer.DrawString(Font, position, FontSize, achievement.Guid, Alignment.CentreLeft, tint, RenderStyle, Layer);

                position.X = Position.X;
                position.Y -= FontSize.Y * 1.2f;
            }
		}
    }
}
