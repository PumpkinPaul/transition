﻿using System.Reflection;
using NUnit.Framework;
using Transition._____Temp.StarGuard;
using Transition._____Temp.StarGuard.Entities;
using StarGuardPlayer = Transition._____Temp.StarGuard.Entities.Player;

namespace Transition.Test 
{    
    public static class TestHelper 
    {
        public static readonly Random Random = new Random();

        [TestFixtureSetUp]
        public static void Initialise()
        {
            var game = new TransitionGame();
            //game.ExternalInitialization();

            GameServices.Instance = new GameServices();
            GameServices.Instance.Add<TransitionGame>(TransitionGame.Instance);

            game.World = new World();
            game.NewCollisionMapManager = new _____Temp.StarGuard.CollisionMapManager();

            game.GameMap = GameMap.Load(null);
            GameServices.Instance.Add<GameMap>(game.GameMap);

            game.EntityBlueprints.LoadContent();
            
            var methodInfo = typeof(BaseGame).GetMethod("CreatePlayers", BindingFlags.NonPublic | BindingFlags.Instance);
            methodInfo.Invoke(TransitionGame.Instance, null);
        }

        public static EntityFactory InitialiseEntityFactory()
        {
            var entityFactory = new EntityFactory(TransitionGame.Instance);
            entityFactory.RegisterFactory(typeof(StarGuardPlayer), 2);
            entityFactory.RegisterFactory(typeof(CameraProxy), 2);
            entityFactory.RegisterFactory(typeof(Weapon), 2);
            entityFactory.RegisterFactory(typeof(Bullet), 16);
            entityFactory.RegisterFactory(typeof(EnemyBullet), 32);
            entityFactory.RegisterFactory(typeof(Zomboid), 32);
            entityFactory.RegisterFactory(typeof(Trooper), 32);
            entityFactory.RegisterFactory(typeof(Flag), 16);
            entityFactory.RegisterFactory(typeof(ParticleSystem), 128);
            entityFactory.RegisterFactory(typeof(Sound), 32);
            entityFactory.RegisterFactory(typeof(Scientist), 8);
            entityFactory.RegisterFactory(typeof(MovingBlock), 32);
            entityFactory.RegisterFactory(typeof(Mine), 32);
            entityFactory.RegisterFactory(typeof(Stalactite), 32);
            entityFactory.RegisterFactory(typeof(Turret), 32);
            entityFactory.RegisterFactory(typeof(TripleGun), 32);
            entityFactory.RegisterFactory(typeof(Flier), 32);
            entityFactory.RegisterFactory(typeof(Rhino), 16);
            entityFactory.RegisterFactory(typeof(Octopus), 32);
            entityFactory.RegisterFactory(typeof(ExplosiveBlock), 32);
            entityFactory.RegisterFactory(typeof(EliteTrooper), 32);
            entityFactory.RegisterFactory(typeof(DestructibleBlock), 64);
            entityFactory.RegisterFactory(typeof(EliteJumper), 32);
            entityFactory.RegisterFactory(typeof(ScoreLabel), 32);
            //entityFactory.RegisterFactory(typeof(Laser), 32);

            GameServices.Instance.Add<EntityFactory>(entityFactory);

            return entityFactory;
        }
    }
}
