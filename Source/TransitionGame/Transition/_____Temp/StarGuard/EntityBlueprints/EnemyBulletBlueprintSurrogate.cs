using ProtoBuf;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
	/// <summary>
	/// Configuration data surrogate for serialization.
	/// </summary>
    [ProtoContract]
    public class EnemyBulletBlueprintSurrogate
    {
        [ProtoMember(1)]
        public string Name;

        static readonly EnemyBulletBlueprintSurrogate Instance = new EnemyBulletBlueprintSurrogate();

        public static implicit operator EnemyBulletBlueprintSurrogate(EnemyBulletBlueprint value)
        {
            if (value == null)
                return null;

            Instance.Name = value.Name;
            return Instance;
        }

        public static implicit operator EnemyBulletBlueprint(EnemyBulletBlueprintSurrogate value)
        {
            return TransitionGame.Instance.EntityBlueprints.EnemyBulletBlueprint(value.Name);
        }
    }
}
