using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.Rendering;
using Transition.Steering;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control for displaying images on the intro menu.
    /// </summary>
    public class IntroImageControl : MenuControl
    {
        /// <summary>The angle of rotation around the z-axis (in radians).</summary>
        public float Rotation;

        /// <summary>The size of the image.</summary>
        public Vector2 Size;

        /// <summary>The id of the texture to use for drawing.</summary>
        public TextureInfo TextureInfo;

        /// <summary>The id of the texture to use for drawing.</summary>
        public TextureInfo ShadowTextureInfo;

        /// <summary>The colour of the shadow tint.</summary>
        public Color ShadowTint;

        float _direction;
        float _ticks = 400;
        Vector2 _target;
        int _maxTicks = 400;
        int _ticksMin = 30;
        int _ticksMax = 400;
        float _speed = 0.05f;
        float _minSpeed = 0.04f;
        float _maxSpeed = 0.15f;
        float _scale = 1.0f;
        float _previousScale = 1.0f;
        float _minScale = 0.7f;
        float _maxScale = 1.35f;
        float _angle;
        float _minAngle = 0.01f;
        float _maxAngle = 0.05f;

        /// <summary>
        /// Initialises a new instance of an ImageControl.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public IntroImageControl(BaseGame game) : base(game)
        {
            _speed = RandomHelper.Random.GetFloat(_minSpeed, _maxSpeed);
        }

        public override void Load(XElement element)
        {
            base.Load(element);
            
            Size = element.GetAttributeVector2("size");
            TextureInfo = ParseTexture(element, "textureInfo", TextureInfo);
            ShadowTextureInfo = ParseTexture(element, "textureInfo", ShadowTextureInfo);

            ShadowTint = element.GetAttributeColor("shadowTint", ShadowTint);
        }

        /// <summary>
        /// Allows a control to update itself.
        /// </summary>
        protected override void OnUpdate()
        {
            _ticks++;

            if (_ticks >= _maxTicks)
            {
                _maxTicks = RandomHelper.Random.GetInt(_ticksMin, _ticksMax);
                _ticks = 0;
                _target = RandomHelper.Random.GetVector2(-21.0f, 21.0f, -11.0f, 11.0f);

                _previousScale = _scale;
                _scale = RandomHelper.Random.GetFloat(_minScale, _maxScale);

                _angle = RandomHelper.Random.GetFloat(_minAngle, _maxAngle);
            }

            HomingSystem.FaceTarget(Position, _target, ref _direction, _angle);
            Position += VectorHelper.Polar(_speed, _direction);
        }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
        {
            var tint = Tint;
            tint *= Overlay.Opacity;

            var shadowTint = ShadowTint;
            shadowTint *= Overlay.Opacity;

            var shadowPosition = Position + (Position * 0.15f);

            Rotation = MathHelper.Lerp(MathHelper.PiOver4, -MathHelper.PiOver4, (Position.X + 25.0f) / 50.0f);

            var q = new Quad(shadowPosition, 1.15f * Size * MathHelper.Lerp(_previousScale, _scale, (float)_ticks / (float)_maxTicks), Rotation);
            Game.Renderer.DrawSprite(ref q, ShadowTextureInfo.TextureId, ShadowTextureInfo.SourceRect, shadowTint, RenderStyle, Layer);

            q = new Quad(Position, Size * MathHelper.Lerp(_previousScale, _scale, (float)_ticks / (float)_maxTicks), Rotation);
            Game.Renderer.DrawSprite(ref q, TextureInfo.TextureId, TextureInfo.SourceRect, tint, RenderStyle, Layer + 1);
        }
    }
}
