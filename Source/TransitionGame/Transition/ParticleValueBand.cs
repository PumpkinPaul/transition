using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public class ParticleValueBand
	{	
		public Vector4 ColourMin;
        public Vector4 ColourMax;
		
		public float SizeMinX;
		public float SizeMinY;
		public float SizeMaxX;
		public float SizeMaxY;

		public float WobbleMinX;
		public float WobbleMinY;
		public float WobbleMaxX;
		public float WobbleMaxY;
		
		public float SpinMin;
		public float SpinMax;
	}

    public class ColorBand
	{	
		public Vector4 ColourMin;
        public Vector4 ColourMax;
        public List<Color> Colors;
	}

    public class SizeBand
	{	
		public float SizeMinX;
		public float SizeMinY;
		public float SizeMaxX;
		public float SizeMaxY;
	}

    public class ForceBand
	{	
		public float Angle;
		public float Min;
		public float Max;
	}
}
