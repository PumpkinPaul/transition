# README #

XNA / MonoGame source code for the game Transition by Pumpkin Games

### What is this repository for? ###

* Contains all 'engine' and 'game' source code

### What do I need? ###

* Microsoft XNA Game Studio 4.0 Refresh for Windows
    * http://www.microsoft.com/en-gb/download/details.aspx?id=27599

* MonoGame for Mac and Linux
    * http://www.monogame.net/

### Who do I talk to? ###

* Paul Cunningham - paul.cunningham@pumpkin-games.net
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)