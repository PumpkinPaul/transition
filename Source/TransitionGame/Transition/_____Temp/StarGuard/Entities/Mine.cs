﻿using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Mine enemy
    /// </summary>
    [ProtoContract]
    public class Mine : Enemy
    {
        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public Mine(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        public override void PlayRicochet()
        {
            Sound.Play(Game, "Mine", Position);
        }
    }
}