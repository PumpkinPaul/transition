using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using Krypton;
using Krypton.Lights;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Extensions;
using Transition.Rendering;
using Transition._____Temp.StarGuard.Entities;

namespace Transition._____Temp.StarGuard
{
    public class GameMap
    {
        /// <summary>
        /// Controls the collision detection and response behavior of a tile.
        /// </summary>
        public enum TileCollision
        {
            /// <summary>A passable tile is one which does not hinder player motion at all.</summary>
            Passable = 0,

            /// <summary>An impassable tile is one which does not allow the player to move through it at all. It is completely solid.</summary>
            Impassable = 1,

            /// <summary>
            /// A platform tile is one which behaves like a passable tile except when the  player is above it. A player can jump up through a platform as 
            /// well as move past it to the left and right, but can not fall down through the top of it.
            /// </summary>
            Platform = 2,
        }

        static class TileType
        {
            public const int Empty = 255;
            public const int Orange = 0;
            public const int Wall = 1;
            
            public const int DestructibleBlock = 5;
            public const int DestructibleBlockHiddenDiamond = 6;
            //public const int Diamond = 7;
    
            public const int ExplosiveBlockNotRespawning = 10;
            public const int ExplosiveBlockRespawning = 11;

            public const int TransparentSolid = 96;

            public const int Bridge1 = 112;
            public const int Bridge2 = 113;
            public const int Bridge3 = 114;
            public const int Bridge4 = 115;
            public const int Bridge5 = 116;
            public const int Bridge6 = 117;
            public const int Bridge7 = 118;
            public const int Bridge8 = 119;
            public const int Bridge9 = 120;
            public const int Bridge10 = 121;
            public const int Bridge11 = 122;
            public const int Bridge12 = 123;
            public const int Bridge13 = 124;
            public const int Bridge14 = 125;
            public const int Bridge15 = 126;
            public const int Bridge16 = 127;

            // control
            //public const int BlueBlinkingExit = 4;
            //public const int LiftExit = 8;
            //public const int AmbushTrigger = 208;
            //public const int TextTrigger = 209;

            // enemies
            //public const int FlierEnemyFliesUpAndDownShootsBullets = 12;
            public const int TrooperEnemyShootsLasersMoves = 173; 
            //public const int ZomboidEnemyWalksReanimates = 174;
            //public const int JumperEnemy = 15;
            //public const int BerzerkerEnemyTtoughRunsQuickly = 16;
            //public const int EliteTrooperEnemyShootsEnergyBalls = 17;
            //public const int EliteJumperEnemy = 18;
            //public const int OctopusEnemyFliesUpAndDownShootsOnTurn = 19;
            //public const int ManyTrooperEnemies = 37;
            //public const int StationaryTrooperEnemy = 61;
            //public const int LaserGun1 = 57;
            //public const int LaserGun2 = 58;
            //public const int LaserGun3 = 59;
            //public const int TrippleBallWallGunGreenDoesntAimIndestructible = 26;
            //public const int TrippleBallWallGunRedAims = 27;

            // triggered ambushes
            //public const int TriggeredFlierEnemyInvisibleBefore = 36;
            //public const int ManyTriggeredZomboidEnemiesLargeRedBlob = 38;
            //public const int ManyTriggeredJumperEnemiesRedBlob = 39;
            //public const int TriggeredZomboidEnemyRedBlob = 40;
            //public const int TriggeredEliteTrooperEnemyRedBlob = 41;
            //public const int ManyTriggeredEliteJumperEnemiesInvisibleBefore = 42;

            // hazards
            //public const int Mine = 24;
            //public const int Stalactite = 25;
            //public const int LaserHazard1Horizontal = 30;
            //public const int LaserHazard1Vertical = 31;
            //public const int LaserHazard2Horizontal = 32;
            //public const int LaserHazard2Vertical = 33;
            //public const int LaserHazard3Horizontal = 34;
            //public const int LaserHazard3Vertical = 35;
            //public const int MovingBlockStartsMovingRight = 51;
            //public const int MovingBlockStartsMovingUp = 52;
            //public const int MovingBlockStartsMovingLeft = 53;
            //public const int MovingBlockStartsMovingDown = 54;
            //public const int FillCeilingToRightOrWallBelowWithGuns = 56;

            // friendlies
            //public const int FriendlyManFacingRight = 22;
            //public const int FriendlyManFacingLeft = 23; 
            //public const int FriendlyLaserWallGun = 28;

            // decoration
            //public const int Text = 122;
            //public const int SmallRedBlob = 43;
            //public const int SmallGreenBlob1 = 44;
            //public const int SmallGreenBlob2 = 45;
            //public const int DestroyedGreenWallGun = 46;
            //public const int DestroyedRedWallGun = 47;

            // boss
            //public const int TriggeredEndBoss = 60;

            // duplicates
            //public const int LavaDuplicate = 9;
            //public const int MovingBlockStartsMovingRightDuplicate = 63;
            //public const int MovingBlockStartsMovingUpDuplicate = 64;
            //public const int MovingBlockStartsMovingLeftDuplicate = 65;
            //public const int MovingBlockStartsMovingDownDuplicate = 66;
        }

        static class ObjectType
        {
            //Enemies
            public const int Zomboid = 0;
            public const int Trooper = 1;
            public const int Scientist = 2;

            public const int MovingBlockRight = 3;
            public const int MovingBlockUp = 4;
            public const int MovingBlockLeft = 5;
            public const int MovingBlockDown = 6;
            public const int Mine = 7;
            public const int Stalactite = 8;
            public const int Turret = 9;
            public const int TripleGun = 10;
            public const int Flier = 11;
            public const int Rhino = 12;
            public const int Octopus = 13;
            public const int ExplosiveBlock = 14;
            public const int EliteTrooper = 15;
            public const int DestructibleBlock = 16;
            public const int EliteJumper = 17;
            public const int Laser = 18;

            //Items
            public const int PlayerEntryLeft = 48;
            public const int PlayerEntryRight = 48000;
            public const int PlayerExit = 49;
            public const int Flag = 50;

            //Environment
            public const int Lava = 163;
        }

        //Map file has data starting from top, left - we convert it so that...

        //MapData starts at bottom, left and positive y = up (so MapData[0] is bottom left)
        // 
        //                        MapWidth, MapHeight
        //  ----------------------
        //  |                    |
        //  |                    |
        //  |                    |
        //  |                    |
        //  ----------------------
        //0,0

        //Therefore to check the tile above the player (x, y + 1)

        //Also drawing starts at bottom left (0,0) and draws each column for each row

        //The TMX format is 1 based - we convert it to 0 based on load too.

        class DrawingTile
        {
            public int TileId;
            public int TextureId;
            public Quad Quad;
        }

        class EntityDefinition
        {
            public Type Type;
            public string Name;
            public Vector2 Position;
            //public Color Tint;
            public Action<Entity> Action;
            public Alignment Alignment = Alignment.Centre;
        }

        public Color BackgroundColor { get; private set; }
        public int TileWidth;
        public int TileHeight;
        readonly int _mapWidth;
        readonly int _mapHeight;

        readonly int[] _mapData;

        public int TextureId;

        public const float TileSize = 1f;
        private readonly Vector2 _worldLocation = new Vector2(0.0f, 0.0f);

        public Vector2 StartPosition { get; private set; }

        readonly List<Box2> _exits = new List<Box2>();

        DrawingTile[] _tiles;
        List<EntityDefinition> Entities { get; set; }

        readonly Lava _lava;

        public string Path { get; }

        public static GameMap Load(string path)
        {
            var gameMap = new GameMap(path);
            return gameMap;
        }

        GameMap(string path)
        {
            if (path == null)
                return;

            Path = path;

            Entities = new List<EntityDefinition>();

            _lava = new Lava();

            //Load the map with data
            var document = XDocument.Load(path);
            var mapElement = document.Root;

            if (mapElement == null)
                throw new InvalidOperationException("Map file is missing the <map> element.");

            _mapWidth = mapElement.GetAttributeInt32("width");
            _mapHeight = mapElement.GetAttributeInt32("height");

            BackgroundColor = mapElement.GetAttributeColor("backgroundcolor");

            _mapData = new int[_mapWidth * _mapHeight];

            foreach (var layerElement in mapElement.Elements("layer"))
            {
                var tileY = _mapHeight - 1;
                var tileX = 0;

                var dataElement = layerElement.Element("data");
                if (dataElement == null)
                    throw new InvalidOperationException("<map> element is missing the <data> element.");

                var lightTexture = LightTextureBuilder.CreatePointLight(TransitionGame.Instance.GraphicsDevice, 512);

                foreach (var tileElement in dataElement.Elements("tile"))
                {
                    var mapId = (_mapWidth * tileY) + tileX;

                    switch (layerElement.GetAttributeString("name"))
                    {
                        case "Tiles":
                            var tileType = tileElement.GetAttributeInt32("gid") - 1;
                            if (tileType < 0)
                                tileType = TileType.Empty;

                            //Put the value into the map;
                            _mapData[mapId] = tileType;
                            break;

                        case "Objects":
                            var objectType = tileElement.GetAttributeInt32("gid") - 1;
                            if (objectType < 0)
                                break;

                            //Object tiles start at 257
                            objectType -= 256;
                            ProcessObject(objectType, tileY, tileX);
                            break;

                        case "Lights":
                            var lightId = tileElement.GetAttributeInt32("gid") - 1;
                            if (lightId < 0)
                                break;

                            var p = GetTileLocation(tileX, tileY);
                            var light = new Light2D()
                            {
                                Texture = lightTexture,
                                Range = 4,
                                Color = new Color(150, 180, 200),
                                Intensity = 0.8f,
                                Angle = MathHelper.TwoPi,
                                X = p.X,
                                Y = p.Y
                            };

                            TransitionGame.Instance.Krypton.Lights.Add(light);
                            break;

                        default:
                            throw new InvalidOperationException("Unknown layer name: " + layerElement.GetAttributeString("name"));
                    }

                    tileX++;
                    if (tileX != _mapWidth)
                        continue;

                    tileX = 0;
                    tileY--;
                }
            }

            //'Process' the map
            //Turns all those dumb tiles into something gamey (e.g. obstacle, mine generator)
            //ProcessEntities();
            ProcessTiles();

            if (TransitionGame.Instance.Krypton.Enabled)
                GenerateShadowCasters();

            _lava.Initialise();
        }

        public void RefreshMapData()
        {

        }

        void ProcessTiles()
        {
            _tiles = new DrawingTile[_mapWidth * _mapHeight];

            const int startColId = 0;
            var endColId = _mapWidth - 1;

            var y = _worldLocation.Y;
            var i = 0;
            for (var r = 0; r < _mapHeight; r++)
            {
                var x = _worldLocation.X;

                for (var c = startColId; c <= endColId; c++)
                {
                    var quad = new Quad(y + TileSize, x, y, x + TileSize);
                    var tileId = _mapData[(r * _mapWidth) + c];

                    _tiles[i] = new DrawingTile
                    {
                        TileId = tileId,
                        TextureId = TransitionGame.Instance.TextureManager.GetTextureId("StarGuard"),
                        Quad = quad
                    };

                    x += TileSize;
                    i++;
                }

                y += TileSize;
            }
        }

        void ProcessObject(int objectId, int r, int c)
        {
            switch (objectId)
            {
                case ObjectType.PlayerEntryRight:
                case ObjectType.PlayerEntryLeft:
                    StartPosition = GetTileLocation(c, r);
                    break;

                case ObjectType.PlayerExit:
                    var exitLocationBottomLeft = GetTileLocation(c, r);
                    _exits.Add(new Box2(exitLocationBottomLeft, TileSize, TileSize));
                    break;

                case ObjectType.Zomboid:
                    //if (c < 160)
                    //    return;
                    Entities.Add(new EntityDefinition { Type = typeof(Zomboid), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.Trooper:
                    //if (c < 140)
                    //    return;
                    Entities.Add(new EntityDefinition { Type = typeof(Trooper), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.Scientist:
                    //if (c < 140)
                    //    return;
                    Entities.Add(new EntityDefinition { Type = typeof(Scientist), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.Flag:
                    Entities.Add(new EntityDefinition { Type = typeof(Flag), Position = GetTileLocation(c, r) });

                    //var lightTexture = LightTextureBuilder.CreatePointLight(TransitionGame.Instance.GraphicsDevice, 512);

                    //var p = GetTileLocation(c, r);
                    //var light2 = new Light2D()
                    //{
                    //    Texture = lightTexture,
                    //    Range = 8,
                    //    Color = new Color(255, 230, 172),
                    //    Intensity = 0.85f,
                    //    Angle = MathHelper.TwoPi,
                    //    X = p.X,
                    //    Y = p.Y
                    //};

                    //TransitionGame.Instance.Krypton.Lights.Add(light2);

                    break;
                
                //case TileType.Text:
                //    Entities.Add(new EntityDefinition { Type = typeof(Text), Position = GetTileLocation(c, r, Alignment.TopCentre) });
                //    break;

                case ObjectType.Lava:
                    _lava.AddTile(c, r);
                    break;

                case ObjectType.MovingBlockUp:
                    Entities.Add(new EntityDefinition { Type = typeof(MovingBlock), Position = GetTileLocation(c, r), Action = entity => entity.Velocity = new Vector2(0, MovingBlock.SpeedY)});
                    break;
                case ObjectType.MovingBlockDown:
                    Entities.Add(new EntityDefinition { Type = typeof(MovingBlock), Position = GetTileLocation(c, r), Action = entity => entity.Velocity = new Vector2(0, -MovingBlock.SpeedY)});
                    break;
                case ObjectType.MovingBlockLeft:
                    Entities.Add(new EntityDefinition { Type = typeof(MovingBlock), Position = GetTileLocation(c, r), Action = entity => entity.Velocity = new Vector2(-MovingBlock.SpeedX,0)});
                    break;
                case ObjectType.MovingBlockRight:
                    Entities.Add(new EntityDefinition { Type = typeof(MovingBlock), Position = GetTileLocation(c, r), Action = entity => entity.Velocity = new Vector2(MovingBlock.SpeedX, 0)});
                    break;

                case ObjectType.Mine:
                    Entities.Add(new EntityDefinition { Type = typeof(Mine), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.Stalactite:
                    Entities.Add(new EntityDefinition { Type = typeof(Stalactite), Position = GetTileLocation(c, r), Alignment = Alignment.TopCentre });
                    break;

                case ObjectType.Turret:
                    Entities.Add(new EntityDefinition { Type = typeof(Turret), Position = GetTileLocation(c, r)});
                    break;

                case ObjectType.TripleGun:
                    Entities.Add(new EntityDefinition { Type = typeof(TripleGun), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.Flier:
                    Entities.Add(new EntityDefinition { Type = typeof(Flier), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.Rhino:
                    Entities.Add(new EntityDefinition { Type = typeof(Rhino), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.Octopus:
                    Entities.Add(new EntityDefinition { Type = typeof(Octopus), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.ExplosiveBlock:
                    Entities.Add(new EntityDefinition { Type = typeof(ExplosiveBlock), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.EliteTrooper:
                    Entities.Add(new EntityDefinition { Type = typeof(EliteTrooper), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.DestructibleBlock:
                    Entities.Add(new EntityDefinition { Type = typeof(DestructibleBlock), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.EliteJumper:
                    Entities.Add(new EntityDefinition { Type = typeof(EliteJumper), Position = GetTileLocation(c, r) });
                    break;

                case ObjectType.Laser:
                    Entities.Add(new EntityDefinition { Type = typeof(Laser), Position = GetTileLocation(c, r) });
                    break;
            }
        }

        static bool IsPlatform(int tileId)
        {
            return (tileId >= TileType.Bridge1 && tileId <= TileType.Bridge16);
        }

        static bool IsSolid(int tileId)
        {
            return (tileId > 0 && tileId < TileType.Bridge1);
        }

        public void Initialise()
        {
            foreach(var entityDefinition in Entities)
            {
                var entity = TransitionGame.Instance.EntityFactory.Create(entityDefinition.Type, entityDefinition.Name);
                if (entity == null) 
                    continue;

                switch (entityDefinition.Alignment)
                {
                    case Alignment.Centre:
                        var name = entityDefinition.Name ?? entityDefinition.Type.Name;
                        var bounds = TransitionGame.Instance.EntityBlueprints.EntityBlueprint(name).CollisionBounds;
                        //entity.Position = entityDefinition.Position - (0.5f * new Vector2(0, TileSize - entity.CollisionBounds.Height));
                        entity.Position = new Vector2(entityDefinition.Position.X, entityDefinition.Position.Y - (TileSize / 2.0f) - bounds.Y);
                        break;

                    case Alignment.TopCentre:
                        entity.Position = entityDefinition.Position + (0.5f * new Vector2(0, TileSize - entity.CollisionBounds.Height));
                        break;
                }
                entityDefinition.Action?.Invoke(entity);
                
                TransitionGame.Instance.World.Add(entity);
            }
        }

        public void SetTileTransparentSolid(Vector2 position)
        {
            var coord = GetTileCoordinates(position);
            SetTileId(coord, TileType.TransparentSolid);
        }

        public void SetTileEmpty(Vector2 position)
        {
            var coord = GetTileCoordinates(position);
            SetTileId(coord, TileType.Empty);
        }

        void SetTileId(Point point, int tileType)
        {
            // Prevent escaping past the level ends.
            if (point.X < 0 || point.X >= _mapWidth || point.Y < 0 || point.Y >= _mapHeight)
                return;

            _mapData[point.Y * _mapWidth + point.X] = tileType;
        }

        /// <summary>
        /// Gets the location of a tile (using an optional origin)
        /// </summary>
        /// <param name="colId">The column in the map.</param>
        /// <param name="rowId">The row in the map</param>
        /// <param name="alignment">Optionally specify the default origin of the tile.</param>
        /// <returns></returns>
        Vector2 GetTileLocation(int colId, int rowId, Alignment alignment = Alignment.Centre)
        {
            var bottomLeft = new Vector2(_worldLocation.X + (colId * TileSize), _worldLocation.Y + (rowId * TileSize));
            const float halfWidth = TileSize * 0.5f;
            const float halfHeight = halfWidth;
            switch (alignment)
            {
                case Alignment.TopLeft:
                    return bottomLeft + new Vector2(0, TileSize);

                case Alignment.TopCentre:
                    return bottomLeft + new Vector2(halfWidth, TileSize);

                case Alignment.Centre:
                    return bottomLeft + new Vector2(halfWidth, halfHeight);

                case Alignment.BottomCentre:
                    return bottomLeft + new Vector2(halfWidth, 0);

                //case Alignment.BottomLeft:
                default:
                    return bottomLeft;
            }
        }

        /// <summary>
        /// Gets the tile coordinates (colId, rowId) from a positin in worldSpace)
        /// </summary>
        public Point GetTileCoordinates(Vector2 worldPosition)
        {
            var coordinates = new Point
            {
                X = (int)((worldPosition.X - _worldLocation.X) / TileSize),
                Y = (int)((worldPosition.Y - _worldLocation.Y) / TileSize)
            };
            return coordinates;
        }

        /// <summary>
        /// Gets the bounding rectangle of a tile in world space from tile coords.
        /// </summary>        
        public Box2 GetWorldBounds(Vector2 worldPosition)
        {
            var point = GetTileCoordinates(worldPosition);
            return GetWorldBounds(point.X, point.Y);
        }

        /// <summary>
        /// Gets the bounding rectangle of a tile in world space from tile coords.
        /// </summary>        
        public Box2 GetWorldBounds(int colId, int rowId)
        {
            var bottomLeft = GetTileLocation(colId, rowId, Alignment.BottomLeft);
            return new Box2(bottomLeft, (int)(TileSize), (int)TileSize);
        }

        /// <summary>
        /// Gets the collision mode of the tile at a particular position (world location).
        /// </summary>
        public TileCollision GetTileCollision(Vector2 worldPosition)
        {
            var coords = GetTileCoordinates(worldPosition);
            return GetTileCollision(coords.X, coords.Y);
        }

        /// <summary>
        /// Gets the collision mode of the tile at a particular position (world location).
        /// </summary>
        public TileCollision GetTileCollision(Point coords)
        {
            return GetTileCollision(coords.X, coords.Y);
        }

        /// <summary>
        /// Gets the collision mode of the tile at a particular location (column and row).
        /// This method handles tiles outside of the levels boundries by making it impossible to escape past the left or right edges, but allowing things
        /// to jump beyond the top of the level and fall off the bottom.
        /// </summary>
        public TileCollision GetTileCollision(int colId, int rowId)
        {
            // Prevent escaping past the level ends.
            if (colId < 0 || colId >= _mapWidth)
                return TileCollision.Impassable;

            // Allow jumping past the level top and falling through the bottom.
            if (rowId < 0 || rowId >= _mapHeight)
                return TileCollision.Passable;

            var tileId = _mapData[rowId * _mapWidth + colId];

            if (IsSolid(tileId))
                return TileCollision.Impassable;

            if (IsPlatform(tileId)) 
                return TileCollision.Platform;
                
            return TileCollision.Passable;
        }

        public bool IsAtExit(Vector2 position)
        {
            foreach (var exit in _exits)
            {
                if (exit.Contains(position))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Detects and resolves all collisions between the player and his neighboring
        /// tiles. When a collision is detected, the player is pushed away along one
        /// axis to prevent overlapping. There is some special logic for the Y axis to
        /// handle platforms which behave differently depending on direction of movement.
        /// </summary>
        public void HandleCollisions(Entity entity, bool respectPlatforms = true)
        {
            const float collisionResponseThreshold = 0.0f;//0.00001f;

            //Represents the edges of the ENTITY that collide with the game map
            //      Top
            //   ---------
            // L |       | R
            // e |       | i
            // f |       | g
            // t |       | h
            //   |       | t
            //   ---------
            //     Bottom
            var entityCollisionEdge = CollisionEdge.None;

            //Reset flag to search for ground collision - it will get set in this routine if we land on a platform.
            entity.OnGround = false;

            //Set the target location of the player - this would be the position if no collisions happen this frame.
            var targetBounds = entity.GetBoundingBox(entity.Position + entity.Velocity);
            var startBounds = entity.GetBoundingBox(entity.Position);

            //Get the tiles that the player would intersect this frame.
            var leftTile = (int)Math.Floor(targetBounds.Left / TileSize);
            var rightTile = (int)Math.Ceiling(targetBounds.Right / TileSize) - 1;
            var topTile = (int)Math.Ceiling(targetBounds.Top / TileSize) - 1;
            var bottomTile = (int)Math.Floor(targetBounds.Bottom / TileSize);

            //var tile1X = entity.Velocity.X > 0 ? leftTile : rightTile;
            //var tile2X = entity.Velocity.X <= 0 ? rightTile : leftTile;

            //var tile1Y = entity.Velocity.Y > 0 ? bottomTile : topTile;
            //var tile2Y = entity.Velocity.Y <= 0 ? topTile : bottomTile;

            //For each potentially colliding tile...
            var checkTiles = true;
            for (var y = topTile; y >= bottomTile && checkTiles; --y)
            {
                for (var x = leftTile; x <= rightTile && checkTiles; ++x)
                {
                    //If the current tile is not solid then there's no edges to test.
                    var tileCollision = GetTileCollision(x, y);
                    if (tileCollision == TileCollision.Passable || (tileCollision == TileCollision.Platform && (entity.Velocity.Y >= 0 || respectPlatforms == false)))
                        continue;

                    //Get the bounds of the current tile
                    var tileBounds = GetWorldBounds(x, y);

                    //Now calculate the collision depth - current tile against target player location...
                    var depth = targetBounds.GetIntersectionDepth(tileBounds);
                    if (depth == Vector2.Zero)
                        continue;

                    //Determine which edges to test against. An edge test will be required if...
                    //1) Velocity is travelling in the correct direction. No point in checking a left edge if we are travelling left (we'd be interested in right edges in that case) 
                    //2) The target location is not already passed the edge.
                    //3) The 'next' tile is passable - if it's not passable if would be an 'inner' edge (e.g. an edge along 2 solid tiles)
                    var topEdge = startBounds.Bottom >= tileBounds.Top && entity.Velocity.Y < 0 && tileBounds.Top < targetBounds.Top && (GetTileCollision(x, y + 1) == TileCollision.Passable || (respectPlatforms && GetTileCollision(x, y + 1) == TileCollision.Platform));
                    var leftEdge = tileCollision == TileCollision.Impassable && entity.Velocity.X > 0 && tileBounds.Left > targetBounds.Left && (GetTileCollision(x - 1, y) != TileCollision.Impassable);
                    var rightEdge = tileCollision == TileCollision.Impassable && entity.Velocity.X < 0 && tileBounds.Right < targetBounds.Right && (GetTileCollision(x + 1, y) != TileCollision.Impassable);
                    var bottomEdge = entity.Velocity.Y > 0 && tileBounds.Bottom > targetBounds.Bottom && GetTileCollision(x, y - 1) == TileCollision.Passable;

                    if (!leftEdge && !rightEdge)
                        depth.X = float.MaxValue;

                    if (!topEdge && !bottomEdge)
                        depth.Y = float.MaxValue;

                    var absDepthX = Math.Abs(depth.X);
                    var absDepthY = Math.Abs(depth.Y);

                    //Edge case were the intersection is equal along both X and Y axes
                    if (absDepthY < absDepthX == false && absDepthY > absDepthX == false)
                    {
                        //Test the velocity, we'll 'project' out of the axis with the smallest velocity
                        if (entity.Velocity.Y < entity.Velocity.X)
                            absDepthY -= float.Epsilon;
                        else
                            absDepthX -= float.Epsilon;
                    }

                    if (topEdge && targetBounds.Bottom < tileBounds.Top && absDepthY < absDepthX)
                    {
                        entityCollisionEdge |= CollisionEdge.Bottom;

                        entity.OnGround = true;
                        entity.Velocity.Y += (tileBounds.Top - targetBounds.Bottom) + collisionResponseThreshold;
                        targetBounds = entity.GetBoundingBox(entity.Position + entity.Velocity);
                        checkTiles = false;
                    }
                    else if (bottomEdge && targetBounds.Top > tileBounds.Bottom && absDepthY < absDepthX)
                    {
                        entityCollisionEdge |= CollisionEdge.Top;

                        entity.Velocity.Y -= (targetBounds.Top - tileBounds.Bottom) - collisionResponseThreshold;
                        targetBounds = entity.GetBoundingBox(entity.Position + entity.Velocity);
                    }

                    //Going right
                    if (leftEdge && targetBounds.Right > tileBounds.Left && absDepthX < absDepthY)
                    {
                        entityCollisionEdge |= CollisionEdge.Right;

                        entity.Velocity.X -= (targetBounds.Right - tileBounds.Left) - collisionResponseThreshold;
                        targetBounds = entity.GetBoundingBox(entity.Position + entity.Velocity);
                    }
                    //Going left
                    else if (rightEdge && targetBounds.Left < tileBounds.Right && absDepthX < absDepthY)
                    {
                        entityCollisionEdge |= CollisionEdge.Left;

                        entity.Velocity.X += (tileBounds.Right - targetBounds.Left) + collisionResponseThreshold;
                        targetBounds = entity.GetBoundingBox(entity.Position + entity.Velocity);
                    }
                }
            }

            if (entityCollisionEdge != CollisionEdge.None)
                entity.OnCollisionWithMap(entityCollisionEdge);
        }

        public bool CollidesWithHazard(Entities.Player player)
        {
            return _lava != null && _lava.Contains(player.GetBoundingBox());
        }

        public void Update()
        {
            _lava.Update();
        }

        public void Draw()
        {
            //Temp attempt at draw character
            var game = TransitionGame.Instance;

            game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);

            //Only draw visible tiles
            var cameraBounds = game.CameraManager.Camera.Bounds;

            var startColId = (int)Math.Max((cameraBounds.Left - _worldLocation.X) / TileSize, 0);
            var endColId = (int)Math.Min(startColId + Math.Ceiling(cameraBounds.Width / TileSize) + 1, _mapWidth - 1);
            var startRowId = (int)Math.Max((cameraBounds.Bottom + _worldLocation.Y) / TileSize, 0);
            var endRowId = (int)Math.Min(startRowId + Math.Ceiling(cameraBounds.Height / TileSize) + 1, _mapHeight - 1);

            for (var r = startRowId; r < endRowId; r++)
            {
                for (var c = startColId; c < endColId; c++)
                {
                    var tile = _tiles[r * _mapWidth + c];

                    //game.Renderer.DrawSprite(ref tile.Quad, tile.TextureId, GetTextureRect(tile.TileId), tile.Tint * tile.Opacity, PointClampRenderStyle.Instance);
                    game.Renderer.DrawSprite(ref tile.Quad, tile.TextureId, GetTextureRect(tile.TileId), Color.White, TransparentRenderStyle.Instance);

                    //DEBUGGING INFO
                    //Uncomment this to see all the collideable edges for the map.
                    //DrawDebugCollisionTiles(c, r, game, tile);
                }
            }

            game.Renderer.End();

            _lava.Draw();
        }

        private void DrawDebugCollisionTiles(int c, int r, TransitionGame game, DrawingTile tile)
        {
            var solid = GetTileCollision(c, r) == TileCollision.Impassable;

            var topEdge = solid && GetTileCollision(c, r + 1) == TileCollision.Passable;
            var leftEdge = solid && GetTileCollision(c - 1, r) == TileCollision.Passable;
            var rightEdge = solid && GetTileCollision(c + 1, r) == TileCollision.Passable;
            var bottomEdge = solid && GetTileCollision(c, r - 1) == TileCollision.Passable;

            var textInfo = game.TextureManager.GetTextureInfo("Pixel");
            if (topEdge)
            {
                var q = tile.Quad;
                q.Bottom(tile.Quad.TopLeft.Y - 0.1f);
                game.Renderer.DrawSprite(ref q, textInfo.TextureId, textInfo.SourceRect, Color.Red,
                    PointClampRenderStyle.Instance);
            }

            if (bottomEdge)
            {
                var q = tile.Quad;
                q.Top(tile.Quad.BottomLeft.Y + 0.1f);
                game.Renderer.DrawSprite(ref q, textInfo.TextureId, textInfo.SourceRect, Color.Purple, PointClampRenderStyle.Instance);
            }

            if (leftEdge)
            {
                var q = tile.Quad;
                q.Right(tile.Quad.TopLeft.X + 0.1f);
                game.Renderer.DrawSprite(ref q, textInfo.TextureId, textInfo.SourceRect, Color.Green, PointClampRenderStyle.Instance);
            }

            if (rightEdge)
            {
                var q = tile.Quad;
                q.Left(tile.Quad.TopRight.X - 0.1f);
                game.Renderer.DrawSprite(ref q, textInfo.TextureId, textInfo.SourceRect, Color.Yellow, PointClampRenderStyle.Instance);
            }

            {
                var q = tile.Quad;
                q.Bottom(tile.Quad.TopLeft.Y - 0.01f);
                game.Renderer.DrawSprite(ref q, textInfo.TextureId, textInfo.SourceRect, Color.Black, 
                    PointClampRenderStyle.Instance);

                q = tile.Quad;
                q.Top(tile.Quad.BottomLeft.Y + 0.01f);
                game.Renderer.DrawSprite(ref q, textInfo.TextureId, textInfo.SourceRect, Color.Black, PointClampRenderStyle.Instance);

                q = tile.Quad;
                q.Right(tile.Quad.TopLeft.X + 0.01f);
                game.Renderer.DrawSprite(ref q, textInfo.TextureId, textInfo.SourceRect, Color.Black, PointClampRenderStyle.Instance);

                q = tile.Quad;
                q.Left(tile.Quad.TopRight.X - 0.01f);
                game.Renderer.DrawSprite(ref q, textInfo.TextureId, textInfo.SourceRect, Color.Black, PointClampRenderStyle.Instance);
            }

            game.Renderer.DrawString(Theme.Current.GetFont(Theme.Fonts.Default),
                new Vector2(tile.Quad.BottomLeft.X, tile.Quad.BottomLeft.Y), new Vector2(0.3f), c + "," + r, Alignment.BottomLeft, Color.Black, TransparentRenderStyle.Instance);
        }

        void GenerateShadowCasters()
        {
            var shadowCasters = new List<Box2>();

            var y = _worldLocation.Y;
            for (var r = 0; r < _mapHeight - 1; r++)
            {
                var x = _worldLocation.X;

                var canMerge = false;

                for (var c = 0; c < _mapWidth - 1; c++)
                {
                    var solid = GetTileCollision(c, r) == TileCollision.Impassable;

                    if (!solid)
                    {
                        canMerge = false;

                        //Can we now merge this caster with any previous ones?
                        var b = shadowCasters[shadowCasters.Count - 1];
                        for (var i = 0; i < shadowCasters.Count - 1; i++)
                        {
                            var b2 = shadowCasters[i];
                            if (b.Width.FloatEquals(b2.Width) && b.Left.FloatEquals(b2.Left) && b.Bottom.FloatEquals(b2.Top))
                            {
                                b2.Height += b.Height;
                                shadowCasters[i] = b2;
                                shadowCasters.Remove(b);
                                break;
                            }

                        }
                    }
                    else
                    {
                        //Only edges need to be shadow casters
                        var isEdge = c > 0 && GetTileCollision(c - 1, r) != TileCollision.Impassable;
                        isEdge = isEdge || (c < _mapWidth - 1 && GetTileCollision(c + 1, r) != TileCollision.Impassable);
                        isEdge = isEdge || (r > 0 && GetTileCollision(c, r - 1) != TileCollision.Impassable);
                        isEdge = isEdge || (r < _mapHeight - 1 && GetTileCollision(c, r + 1) != TileCollision.Impassable);

                        if (isEdge || canMerge)
                        {
                            if (canMerge)
                            {
                                var b = shadowCasters[shadowCasters.Count - 1];
                                b.Width += TileSize;
                                shadowCasters[shadowCasters.Count - 1] = b;
                            }
                            else
                            {
                                var b1 = new Box2(x, y, TileSize, TileSize);
                                shadowCasters.Add(b1);
                                canMerge = true;
                            }
                        }
                    }

                    x += TileSize;
                }

                y += TileSize;
            }

            //shrink the casters, checking from right to left.
            for (var i = 0; i < shadowCasters.Count; i++)
            {
                var b = shadowCasters[i];
                var bw = (int)Math.Round(b.Width);
                var bx = (int)Math.Round(b.X);
                var by = (int)Math.Round(b.Y);

                if (GetTileCollision(bx + bw, by) != TileCollision.Impassable)
                    continue;

                for (var width = bw - 1; width > 0; width--)
                {
                    var c = bx + width;
                    if (GetTileCollision(c, by + 1) == TileCollision.Impassable && GetTileCollision(c, by - 1) == TileCollision.Impassable)
                    {
                        b.Width--;
                    }
                    else
                        break;
                }

                shadowCasters[i] = b;
            }

            //merge the casters, checking from bottom to top.
            for (var i = 0; i < shadowCasters.Count; i++)
            {
                var b = shadowCasters[i];
                var bw = (int)Math.Round(b.Width);
                var bx = (int)Math.Round(b.X);
                var by = (int)Math.Round(b.Y);
                
                for (var j = i + 1; j < shadowCasters.Count; j++)
                {
                    var bh = (int)Math.Round(b.Height);

                    //Can these casters be merged
                    var b2 = shadowCasters[j];
                    var bw2 = (int)Math.Round(b2.Width);
                    var bx2 = (int)Math.Round(b2.X);
                    var by2 = (int)Math.Round(b2.Y);

                    if (bx2 == bx && bw2 == bw && by + bh == by2)
                    {
                        b.Height += b2.Height;
                        b2.Height = 0;
                        shadowCasters[j] = b2;
                    }
                }

                shadowCasters[i] = b;
            }

            //Remove zero height casters
            for (var i = shadowCasters.Count - 1; i >= 0; i--)
            {
                if (shadowCasters[i].Height.FloatEquals(0))
                    shadowCasters.RemoveAt(i);
            }

            //Draw debug information.
            //var ii = 0;
            //var textureInfo = TransitionGame.Instance.TextureManager.GetTextureInfo("Pixel");
            //foreach (var caster in shadowCasters)
            //{
            //    if (caster.Height.FloatEquals(0) == false)
            //    {
            //        var q = new Quad(caster.Top, caster.Left, caster.Bottom, caster.Right);
            //        var color = new Color(rand.GetFloat(), rand.GetFloat(), rand.GetFloat())*0.85f;
            //        //TransitionGame.Instance.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, Color.Black, AlphaBlendingRenderStyle.Instance, 100002);
            //        TransitionGame.Instance.Renderer.DrawBox(caster, color, AlphaBlendingRenderStyle.Instance, 0.04f, 100000);

            //        TransitionGame.Instance.Renderer.DrawString(Theme.Current.GetFont(Theme.Fonts.Default), new Vector2(q.BottomLeft.X + 0.1f, q.BottomLeft.Y + 0.1f), new Vector2(0.4f), ii.ToString(), Alignment.BottomLeft, Color.Black, TransparentRenderStyle.Instance, 100001);
            //    }
            //    ii++;
            //}

            //Add them to Krypton!
            TransitionGame.Instance.Krypton.Hulls.Clear();
            foreach (var caster in shadowCasters)
            {
                var hull = ShadowHull.CreateRectangle(Vector2.One);
                hull.Position.X = caster.X + (caster.Width * 0.5f);
                hull.Position.Y = caster.Y + (caster.Height * 0.5f);
                hull.Scale.X = caster.Width;
                hull.Scale.Y = caster.Height;

                TransitionGame.Instance.Krypton.Hulls.Add(hull);
            }
        }

        static Rect GetTextureRect(int tileId)
        {
            //TODO - this needs to get dimensions from Texture / TextureInfo
            const float tileWidth = 64 / 1024.0f;
            const float tileHeight = 64 / 1024.0f;

            const float pixelOffset = 1 / 1024.0f;

            var left = (tileId % 16) * tileWidth;
            var top = (tileId / 16) * tileHeight;

            var right = left + tileWidth;
            var bottom = top + tileHeight;

            left += pixelOffset; 
            right -= pixelOffset;
            top += pixelOffset;
            bottom -= pixelOffset;

            return new Rect(top, left, bottom, right);
        }
    }
}
