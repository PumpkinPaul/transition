using System;
using Microsoft.Xna.Framework;
using Transition.Extensions;
using Transition.Input;
using Transition.Menus;

namespace Transition
{
    /// <summary>
    /// A mouse pointer for menus.
    /// </summary>
    public class MousePointer
    {				
		/// <summary>.</summary>
		protected BaseGame Game;
		
		/// <summary>.</summary>
		protected MenuManager MenuManager;
										
		/// <summary>
		/// Creates a new instance of a MenuControl.
		/// </summary>
		public MousePointer(BaseGame game, MenuManager menuManager)
		{
			Game = game;
			MenuManager = menuManager;
		}
        		
		/// <summary>
		/// Gets or the mouse pointer's position in window coordinnates.
		/// </summary>
		public Vector2 WindowPosition { get; private set; }

        /// <summary>
		/// Gets or the mouse pointer's position in sumo world units.
		/// </summary>
        public Vector2 WorldPosition { get; private set; }

		/// <summary>
		/// .
		/// </summary>
		public void Update()
		{
            //if (MouseHelper.XMovement == 0 && MouseHelper.YMovement == 0)
             //   return;

            WindowPosition = new Vector2(MouseHelper.MousePos.X, MouseHelper.MousePos.Y);

            //Barrel Distortion will throw mouspointer out so let's do simiar to the shader to modify the mouse position
            if (Game.BarrelDistortionProcessor.Enabled)
            {
                //Modify the WindowPosition by the Barrel Distortion
                var texCoord = new Vector2(WindowPosition.X / Game.GraphicsDevice.Viewport.Width, WindowPosition.Y / Game.GraphicsDevice.Viewport.Height);
                var tex = texCoord - new Vector2(0.5f);

                var imageAspect = Game.GraphicsDevice.Viewport.Width / (float)Game.GraphicsDevice.Viewport.Height;

                const float k = 0.0f; // lens distortion coefficient
                var kcube = Game.BarrelDistortionProcessor.CrtStrength;  // cubic distortion value
        
                var r2 = imageAspect * imageAspect * tex.X * tex.X + tex.Y * tex.Y;   
		     
                float f;
 
                //only compute the cubic distortion if necessary 
                if (kcube.FloatEquals(0.0f)) f = 1 + r2 * k;
                else f = 1 + r2 * (k + kcube * (float)Math.Sqrt(r2));

                //Bulge!
                f = f * Game.BarrelDistortionProcessor.Bulge;
        
                // get the right pixel for the current position
                var x = f * tex.X + 0.5f;
                var y = f * tex.Y + 0.5f;

                //x 0...1 range i.e. the new texCoord

                x *= Game.GraphicsDevice.Viewport.Width;
                y *= Game.GraphicsDevice.Viewport.Height;


                WindowPosition = new Vector2(x, y);
            }

            var p = Game.RenderViewport.Unproject(new Vector3(WindowPosition.X, WindowPosition.Y, 0), Game.ProjectionMatrix, Game.ViewMatrix, Matrix.Identity);
  
            var zoom = -Game.CameraManager.Zoom;
            WorldPosition = new Vector2(p.X * zoom, p.Y * zoom);
		}
    }
}
