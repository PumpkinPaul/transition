using System.IO;

namespace Transition.Input
{
    /// <summary>
    /// The action the GameControls perform plus any deterministic data that cannot reliably be derived from the controls 
    /// </summary>
    public struct GameInput : IGameInput
    {
        public bool Left;
        public bool Right;

        public bool LeftJustPressed;
        public bool RightJustPressed;

        public bool Fire;
        public bool FireJustPressed;

        public bool Jump;
        public bool JumpJustPressed;

        public bool Rewind;
        public bool RewindJustPressed;

        public bool Pause;

        //Some old Paladin controls - left in the engine but probably won't be used.
        public bool AltFire;
        public bool Rage;
        public bool RageJustPressed;

        public void Read(BinaryReader reader)
        {
            throw new System.NotImplementedException();
        }

        public void Write(BinaryWriter writer)
        {
            throw new System.NotImplementedException();
        }
    }
}