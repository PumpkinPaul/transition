using Microsoft.Xna.Framework;
using Transition.MenuControls;
using Transition.Menus;
using Transition.Rendering;
using Transition._____Temp.StarGuard;

namespace Transition
{
    /// <summary>
    /// Represents a HUD.
    /// </summary>
    public class Hud : Overlay
    {
        private const float TextYPosition = 9.2f;
        
        static private readonly IHudState FadeInState = new HudFadeInState();
        static private readonly IHudState FadeOutState = new HudFadeOutState();
        static private readonly IHudState NormalState = new HudNormalState();
        static private readonly IHudState HiddenState = new HudHiddenState();

        /// <summary>A reference to the hud's current state.</summary>
        private IHudState _state;

        //Controls
        AchievementPopupControl _achievementPopup;
        NotificationPopupControl _notificationPopup;
        NotificationPopupControl _landNowPopup;

        CircularGaugeControl _levelTimer;
        CircularGaugeControl _rewindFrames;

        private TextLabelControl _recordLabel;
        private ImageControl _recordIcon;
        private TextLabelControl _rewindLabel;
        private ImageControl _rewindIcon;

        /// <summary>The colour of the hud.</summary>
        private Color _tint;

        public bool DrawProgress;
        public float ProgressValue;

        protected override bool ShouldDraw => !(Game.DemoRecorder.IsPlaying || _state == Hud.HiddenState);

        /// <summary>
        /// Creates a new instance of a Hud.
        /// </summary>
        /// <param name="game"></param>
        public Hud(TransitionGame game) : base(game) { }

        protected override void OnConfigureMenu()
        {
            CreateControls();
            _levelTimer = GetControl<CircularGaugeControl>("levelTimer");
            _rewindFrames = GetControl<CircularGaugeControl>("rewindFrames");

            _recordLabel = GetControl<TextLabelControl>("recordLabel");
            _recordIcon = GetControl<ImageControl>("recordIcon");
            _rewindLabel = GetControl<TextLabelControl>("rewindLabel");
            _rewindIcon = GetControl<ImageControl>("rewindIcon");
        }

        /// <summary>
        /// Creates the controls.
        /// </summary>
        private void CreateControls()
        { 
            const float achievementPopupY = -8.0f;
            _achievementPopup = new AchievementPopupControl(Game);
            _achievementPopup.TextureInfo = Game.TextureManager.GetTextureInfo("Medal");
            _achievementPopup.Position = new Vector2(20.0f, achievementPopupY);
            _achievementPopup.TargetPosition = new Vector2(0.0f, achievementPopupY);
            _achievementPopup.EndPosition = new Vector2(-20.0f, achievementPopupY);

            _achievementPopup.Position = new Vector2(0.0f, -11);
            _achievementPopup.TargetPosition = new Vector2(0.0f, achievementPopupY);
            _achievementPopup.EndPosition = new Vector2(0.0f, -11);

            _achievementPopup.BackgroundTint = Color.White * 0.25f;
            _achievementPopup.TitleTint = Color.White;
            _achievementPopup.Tint = Theme.StandardColor;
            _achievementPopup.Alignment = Alignment.Centre;
            _achievementPopup.FontSize = Theme.FontSize * 0.8f;
            _achievementPopup.TitleFontSize = Theme.FontSize * 0.8f;
            _achievementPopup.ImageScale = _achievementPopup.TitleFontSize * 0.9f;

            const float notificationPopupY = 7.2f;
            _notificationPopup = new NotificationPopupControl(Game);
            _notificationPopup.TextureInfo = Game.TextureManager.GetTextureInfo("RageIcon");
            _notificationPopup.Position = new Vector2(-30.0f, notificationPopupY);
            _notificationPopup.TargetPosition = new Vector2(0.0f, notificationPopupY);
            _notificationPopup.EndPosition = new Vector2(30.0f, notificationPopupY);

            _notificationPopup.BackgroundTint = Theme.StandardColor * 0.25f;
            _notificationPopup.Tint = _tint;
            _notificationPopup.Alignment = Alignment.TopCentre;
            _notificationPopup.FontSize = Theme.FontSize * 2f;
            _notificationPopup.ImageScale = _notificationPopup.FontSize * 0.75f;

            const float landNowPopupY = -11.0f;
            _landNowPopup = new NotificationPopupControl(Game);
            _landNowPopup.TextureInfo = Game.TextureManager.GetTextureInfo("ShopNuke");
            _landNowPopup.Position = new Vector2(0.0f, landNowPopupY);
            _landNowPopup.TargetPosition = new Vector2(0.0f, achievementPopupY);
            _landNowPopup.EndPosition = new Vector2(0.0f, landNowPopupY);
            _landNowPopup.BackgroundTint = Theme.StandardColor * 0.25f;
            _landNowPopup.Tint = Theme.Base1;
            _landNowPopup.Alignment = Alignment.TopCentre;
            _landNowPopup.FontSize = Theme.FontSize * 1.15f;
            _landNowPopup.ImageScale = _landNowPopup.FontSize * 0.75f;
            _landNowPopup.Pulse = true;
            _landNowPopup.BackgroundTint = Theme.Base3;

            AddControl(_achievementPopup, -1);
            AddControl(_notificationPopup, -1);
            AddControl(_landNowPopup, -1);
        }

        /// <summary>
        /// Initialises the HUD.
        /// </summary>
        public void Initialise()
        {
            _state = Hud.HiddenState;

            _tint = Color.White;// Theme.Base2;

            CreateControls();

            Opacity = 0.0f;

            Register();
        }

        public void InitialisePlaySession()
        {
            _achievementPopup.Reset();
            _notificationPopup.Reset();
        }

        /// <summary>
        /// Fades the Hud in.
        /// </summary>
        public void FadeIn()
        {
            _state = Hud.FadeInState;
            StartTransition("fadeIn");
        }

        /// <summary>
        /// Fades the Hud out.
        /// </summary>
        public void FadeOut()
        {
            _state = Hud.FadeOutState;
        }

        public void SetTint(Color tint)
        {
            _tint = tint;
            _achievementPopup.Tint = tint;
            _notificationPopup.Tint = tint;
            _landNowPopup.Tint = tint * 1.2f;
            _landNowPopup.BackgroundTint = tint * 0.5f;
        }

        /// <summary>
        /// Updates the Hud each tick.
        /// </summary>
        protected override void OnUpdateMustCallBase()
        {
            base.OnUpdateMustCallBase();

            _state.Update(this);
        }

        public void StartLandNow()
        {
            _landNowPopup.MaxVisibleTicks = NotificationPopupControl.StayVisible;
            _landNowPopup.AddMessage("land now");
        }

        public void StopLandNow()
        {
            _landNowPopup.Reset();
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddNotification(string text)
        {
            _notificationPopup.AddMessage(text);
        }

        /// <summary>
        /// 
        /// </summary>
        public void AchievementGained(string achievementText)
        {
            _achievementPopup.FadeIn(achievementText);
        }
      
        protected override void OnPreDraw() 
        {
            Game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);
        }

        /// <summary>
        /// Draws the Hud.
        /// </summary>
        protected override void OnDraw()
        {
            var renderState = TransparentRenderStyle.Instance;
            var colour = _tint;
            colour *= Opacity;
            var hudColour = colour;
            var fontScale = new Vector2(2.0f);
      
            var scoreLabel = (IntegerLabelControl)ControlMap["scoreLabel"];
            scoreLabel.Value = Game.ActivePlayer.Score;

            var levelLabel = (TextLabelControl)ControlMap["levelLabel"];
            levelLabel.Text = ((TransitionGame)Game).LevelManager.Level.Name;

            var phase = TransitionGame.Instance?.GamePhaseManager?.ActivePhase as TestPhaseStarGuard;
            if (phase != null)
            {
                _levelTimer.Value = 1.0f - phase.LevelTimeNormalized;
                _rewindFrames.Value = 0.01f * phase.RewindableFrameCount;

                if (phase.IsRewinding)
                {
                    _recordLabel.Visible = false;
                    _recordIcon.Visible = false;
                    _rewindLabel.Visible = true;
                    _rewindIcon.Visible = true;
                }
                else
                {
                    _recordLabel.Visible = true;
                    _recordIcon.Visible = true;
                    _rewindLabel.Visible = false;
                    _rewindIcon.Visible = false;
                }
            }

            //Game.GeometryBatch.DrawNumber(Theme.Font, new Vector2(17.0f, Hud.TextYPosition), fontScale, Game.ActivePlayer.Score, Alignment.CentreRight, hudColour, renderState);
            //Game.GeometryBatch.DrawString(Theme.AltFont, new Vector2(0.0f, Hud.TextYPosition), fontScale, ((TransitionGame)Game).LevelManager.Level.Name, Alignment.Centre, hudColour, renderState);

            //Lives
            //var x = -17.0f;
            //var size = fontScale;
            //var shipTextureInfo = Game.TextureManager.GetTextureInfo("George.Left.01");
            //for (var i = 0; i < Game.ActivePlayer.Lives; i++)
            //{
            //    var pos = new Vector2(x, Hud.TextYPosition - size.Y / 2.0f);
            //    var rect = new Rect(pos, size);
            //    var q = new Quad(rect);
            //    Game.Renderer.DrawSprite(ref q, shipTextureInfo.TextureId, shipTextureInfo.SourceRect, hudColour, renderState);
            //    x += size.X;
            //}

            //'Progress' bar indicating various things - e.g. time to initialise shop!
            if (DrawProgress)
            {
                //Progress bar...
                var barTextureInfo = Game.TextureManager.GetTextureInfo("Pixel");
                const float width = 10.0f;
                var tint2 = _tint;
                tint2 *= MenuControl.InactiveAlphaModifier;
		
                var rect = new Rect(-2.0f, -(width / 2.0f), -2.5f, (width / 2.0f));
                var q = new Quad(rect);
                Game.Renderer.DrawSprite(ref q, barTextureInfo.TextureId, barTextureInfo.SourceRect, tint2, renderState);

                tint2 *= Opacity;
                var length = ProgressValue * 10.0f;
                q.Right(rect.Left + length);
                Game.Renderer.DrawSprite(ref q, barTextureInfo.TextureId, barTextureInfo.SourceRect, tint2, renderState);
            }
        }

        protected override void OnPostDraw() 
        { 
            Game.Renderer.End();
        }

        /// <summary>
        /// Base class for the Hud's state machine.
        /// </summary>
        public interface IHudState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="hud">The hud to update.</param>
            void Update(Hud hud);
        }

        /// <summary>
        /// Hud is fading in.
        /// </summary>
        public struct HudFadeInState : IHudState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="hud">The hud to update.</param>
            public void Update(Hud hud)
            {
                hud.Opacity += 0.05f;
                if (hud.Opacity >= 1.0f)
                {
                    hud.Opacity = 1.0f;
                    hud._state = Hud.NormalState;
                }
            }
        }

        /// <summary>
        /// Hud is fading out.
        /// </summary>
        public struct HudFadeOutState : IHudState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="hud">The hud to update.</param>
            public void Update(Hud hud)
            {
                hud.Opacity -= 0.05f;
                if (hud.Opacity <= 0.0f)
                {
                    hud.Opacity = 0.0f;
                    hud._state = Hud.HiddenState;
                }
            }
        }

        /// <summary>
        /// Normal state of the hud.
        /// </summary>
        public struct HudNormalState : IHudState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="hud">The hud to update.</param>
            public void Update(Hud hud) { }
        }

        /// <summary>
        /// Hidden state for the hud.
        /// </summary>
        public struct HudHiddenState : IHudState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="hud">The hud to update.</param>
            public void Update(Hud hud) { }
        }
    }
}
