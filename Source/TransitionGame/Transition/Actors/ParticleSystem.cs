using Microsoft.Xna.Framework;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public class ParticleSystem : Actor
    {
        /// <summary></summary>
        ParticleSystemTemplate _template;

        /// <summary></summary>
        float _lifetime;

        /// <summary></summary>
        float _emissionResidue;

        /// <summary></summary>
        bool _stopped;
        
        /// <summary>
        /// Creates a new instance of a ParticleSystem.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public ParticleSystem(TransitionGame game) : base (game) { }
        
        /// <summary>
        /// Gets the type of actor.
        /// </summary>
        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.ParticleSystem; }
        }

        /// <summary>
        /// Sets the template to use.
        /// </summary>
        public ParticleSystemTemplate Template
        {
            get { return _template; }
            set { _template = value; }
        }

        /// <summary>
        /// Sets how long this particle system lives for. 
        /// </summary>
        public float Lifetime
        {
            set { _lifetime = value; }
        }
        
        /// <summary>
        /// Causes the particle system to start emitting particles.
        /// </summary>
	    public void Start()
	    {
		    _stopped = false;
	    }

	    /// <summary>
        /// Causes the particle system to stop emitting particles.
        /// </summary>
	    public void Stop()
	    {
		    _stopped = true;
	    }
        
        protected override void OnInitialise(string name)
        {
            base.OnInitialise(name);

            _lifetime = 0.0f;
            _stopped = false;
        }
        /// <summary>
        /// 
        /// </summary>
	    protected override void OnAddedToScene()
	    {
		    _emissionResidue = _template.EmissionResidue;
	    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void OnUpdate(GameTime gameTime)
	    {
  			if (_stopped) 
                return;

            var dt = (float)_game.TargetElapsedTime.TotalMilliseconds / 1000.0f;

			if (_lifetime > Actor.EternalLife) 
            {			
				//Test to see if the particle system has expired
				if(_lifetime < 0.0f) 
                {
					Deactivate();
					return;
				}
				_lifetime -= dt;
			}

            //TODO: Transition - IsOffScreen
            //if (Position.X < -21.0f && Position.X > 21.0f) return;

			//Calculate how many particles we should create from emission_rate and time elapsed taking the previous frame's residue into account.
            var rate = _template.MaxEmissionRate == _template.MinEmissionRate ? _template.MaxEmissionRate : RandomHelper.Random.GetFloat(_template.MinEmissionRate, _template.MaxEmissionRate);
            
			var tempRequired = (rate * dt) + _emissionResidue;

            if (tempRequired <= 0)
                return;

			var particlesRequired = (uint)(tempRequired);

			//Remember the difference between what we wanted to create and how many we created.
			_emissionResidue = tempRequired - particlesRequired;

	        uint i = 0;
			uint particlesCreated = 0;
			var maxParticles = _scene.MaxParticles;
			var particle = _scene.NextParticle();

	        while (i < maxParticles && particlesCreated < particlesRequired)
	        {
				if (!particle.Active) 
				{
					++particlesCreated;
                    InitialiseParticle(particle, particlesCreated, particlesRequired);
				}

				//Next particle in the system
				particle = _scene.NextParticle();
                ++i;
			}
		}

	    /// <summary>
	    /// 
	    /// </summary>
	    /// <param name="particle"></param>
	    /// <param name="created"></param>
	    /// <param name="required"></param>
        protected void InitialiseParticle(Particle particle, uint created, uint required)
		{
			var ratio = created / (float)required;
            var emitterPosition = _template.Emitter.GetPosition();
            var particlePosition = Vector2.Lerp(_previousPosition, Position, ratio) + emitterPosition;
            particle.Position = particlePosition;

            //Determine if the initial velocity should be cone based
            if (_template.EmissionCone == MathHelper.TwoPi)
            {
                //No, 360 degree emmision
                particle.Direction = _template.Emitter.GetDirection(ref Position, ref particlePosition);
            }
            else
            {
                //Yes, so the actual direction will be based on the cone angle and the direction of the system.
                var direction = RandomHelper.Random.GetFloat(-_template.EmissionCone, _template.EmissionCone);
                direction += _direction;
                particle.Direction = direction;
            }

            particle.Reset(this);
		}
	}
}
