﻿using Microsoft.Xna.Framework;
using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Elite Jumper enemy
    /// </summary>
    [ProtoContract]
    public class EliteJumper : Enemy
    {
        static bool _jumpLeft = false;

        const int MaxCountdownTicks = 60;
        public const float JumpSpeed = 0.1f;
        const float JumpAngle = MathHelper.TwoPi / 18.0f;

        public const float MaxSpeedY = 0.35f;
        public const float MaxSpeedX = JumpSpeed * 2;
        
        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        int _countdownTicks;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public EliteJumper(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _countdownTicks = MaxCountdownTicks;

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-14.face-right.animation")
                    : Game.AnimationManager.Get("enemy-14.face-left.animation"));

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnAddedToWorld(World world)
        {
            base.OnAddedToWorld(world);

            _jumpLeft = !_jumpLeft;

            IsFacingRight = !_jumpLeft;

            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-14.face-right.animation")
                : Game.AnimationManager.Get("enemy-14.face-left.animation"));
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            Velocity.Y += TestPhaseStarGuard.Gravity * 0.1f;

            if (OnGround)
            {
                if (_countdownTicks == MaxCountdownTicks)
                {
                    Sprites[0].SetAppearance(IsFacingRight
                        ? Game.AnimationManager.Get("enemy-14.face-right.animation")
                        : Game.AnimationManager.Get("enemy-14.face-left.animation"));
                }

                Velocity.X = 0;
                _countdownTicks--;
                if (_countdownTicks <= 0)
                {
                    _countdownTicks = MaxCountdownTicks;
                    Jump();
                }
            }

            Velocity.X = MathHelper.Clamp(Velocity.X, -MaxSpeedX, MaxSpeedX);
            Velocity.Y = MathHelper.Clamp(Velocity.Y, -MaxSpeedY, MaxSpeedY);

            Game.GameMap.HandleCollisions(this);
        }

        void Jump()
        {
            IsFacingRight = !IsFacingRight;

            //_phase = Phase.Jumping;
            
            Velocity = VectorHelper.Polar(JumpSpeed, MathHelper.PiOver2 - (IsFacingRight ?  JumpAngle : -JumpAngle));

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-14.jump-right.animation")
                    : Game.AnimationManager.Get("enemy-14.jump-left.animation"));
        }

        void Shoot()
        {
            var player = Game.World.Get<Player>(EntityKeys.Player);

            const float speed = 0.075f;
            
            //Left and right bullets...
            CreateBullet(VectorHelper.Polar(speed, 0.0f));
            CreateBullet(VectorHelper.Polar(speed, MathHelper.Pi));

            var playerTileCoords = Game.GameMap.GetTileCoordinates(player.Position);
            var tileCoords = Game.GameMap.GetTileCoordinates(Position);

            //...bullets that go up or down depending on player y position.
            if (playerTileCoords.Y < tileCoords.Y)
            {
                //Player below, shoot down
                CreateBullet(VectorHelper.Polar(speed, -MathHelper.PiOver4));
                CreateBullet(VectorHelper.Polar(speed, -MathHelper.PiOver2));
                CreateBullet(VectorHelper.Polar(speed, -MathHelper.PiOver2 - MathHelper.PiOver4));
            }
            else
            {
                //Shoot up
                CreateBullet(VectorHelper.Polar(speed, MathHelper.PiOver4));
                CreateBullet(VectorHelper.Polar(speed, MathHelper.PiOver2));
                CreateBullet(VectorHelper.Polar(speed, MathHelper.PiOver2 + MathHelper.PiOver4));
            }
        }

        void CreateBullet(Vector2 velocity)
        { 
            var bullet = Game.EntityFactory.Create(typeof(EnemyBullet), "EliteJumperBullet");
            if (bullet != null)
            {
                bullet.Position = Position;
                bullet.Velocity = velocity;
                Game.World.Add(bullet);
            }
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            //Position += Velocity;

            switch (entityCollisionEdge)
            {
                case CollisionEdge.Left:
                    //Velocity.X = -Velocity.X;
                    OnGround = true;
                    break;

                case CollisionEdge.Right:
                    //Velocity.X = -Velocity.X;
                    OnGround = true;
                    break;
            }
        }

        protected override bool OnKilled(Entity collider)
        {
            Shoot();

            return true;
        }
    }
}