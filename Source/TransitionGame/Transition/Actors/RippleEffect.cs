using Microsoft.Xna.Framework;
using Transition.Rendering;

namespace Transition.Actors
{
    /// <summary>
    /// Represents a thing to manage the ripples
    /// </summary>
    public class RippleEffect : SpecialEffect
    {
        int _maxTicks;
        int _ticks;
        int _textureId;
        float _strength;
        float _scaleDelta;

        //readonly Geometry _geometry = new Geometry();

        /// <summary>
        /// Creates a new instance of a GodRaysEffect.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public RippleEffect(TransitionGame game) : base(game)
        {
            //_geometry.RenderStyle = TransparentRenderStyle.Instance;
            //_geometry.TextureId = -1;
            //_geometry.Indices = _indices;
            //_geometry.IndexCount = _indices.Length;
        }

        protected override void OnInitialise(string name) 
        {
            base.OnInitialise(name);

            _ticks = 0;
            _maxTicks = 90;
            _strength = 1.0f;
            Scale = Vector2.Zero;
        }

        public void Setup(int textureId, int maxTicks, float scaleDelta)
        {
            _textureId = textureId;
            _maxTicks = maxTicks;
            _scaleDelta = scaleDelta;
        }

        protected override void OnUpdate(GameTime gameTime)
        {   
            if (_ticks == _maxTicks)
            {
                _ticks = 0;
                Deactivate();
                return;
            }
            _strength = 1 - ((float)_ticks / _maxTicks);

            _ticks++;
            Scale += new Vector2(_scaleDelta);
        }

        protected override void OnDraw(bool isOnscreen) 
        {
            //if (isOnscreen == false)
            //    return;

            var quad = new Quad(Position, Scale);
            _game.RippleDistortionProcessor.DrawSprite(ref quad, _textureId, Rect.TextureRect, Color.White * _strength, RippleRenderStyle.Instance);

        }
    }
}
