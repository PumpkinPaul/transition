using System;

namespace Transition.GamePhases
{
    /// <summary>
    /// Handles the end of a game.
    /// </summary>
    public class GameOverPhase : GamePhase
    {
        public event EventHandler NotifyGameOver;
        
        const int NotifyTicks = 120;
        const int ChangeStateTicks = 300;

        int _ticks;

        public GameOverPhase(BaseGame game) : base(game) { }

        protected override void OnCreate()
        {
            _ticks = 0;
        }

        /// <summary>
		///
		/// </summary>
        protected override void OnUpdate()
		{
            _ticks++;

            if (_ticks == NotifyTicks)
            {
                OnNotifyGameOver(EventArgs.Empty);
                Game.SoundManager.PlaySound("GameOver");
            }

            if (_ticks == ChangeStateTicks)
                Game.ChangePhase(typeof(PostGameOverPhase));
		}

        protected void OnNotifyGameOver(EventArgs e)
        {
            var handler = NotifyGameOver;
            if (handler != null)
                handler(this, e);
        }
    }
}

