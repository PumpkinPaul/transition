using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Transition.CourseSystem;

namespace Transition.LevelEditing
{
    /// <summary>
    /// Represents the clipboard for selectable items (tiles, points, labels, etc)
    /// </summary>
    public class Clipboard
    {
        readonly LevelEditor _levelEditor;

        private List<Tile> Tiles { get; set; }
        private List<Label> Labels { get; set; }

        //Note nodes can not be cut, copied or pasted
        bool HasSelection(LevelEditorState levelEditorState)
        {
            return levelEditorState.SelectedTileIds.Count > 0 || levelEditorState.SelectedLabelIds.Count > 0;
        }

        public Clipboard(LevelEditor levelEditor)
        {
            _levelEditor = levelEditor;
            Tiles = new List<Tile>();
            Labels = new List<Label>();
        }

        public void Cut(LevelEditorState levelEditorState)
        {
            if (HasSelection(levelEditorState))
            {
                CloneSelectedItemsToClipboard();
                _levelEditor.DeleteSelectedCourseItems();
                _levelEditor.SaveEditorState("Cut items");
            }
        }

        public void Copy(LevelEditorState levelEditorState)
        {
            if (HasSelection(levelEditorState))
            {
                CloneSelectedItemsToClipboard();
            }
        }

        public void Paste(LevelEditorState levelEditorState)
        {
            if (PasteItemsFromClipboard(levelEditorState) > 0)
                _levelEditor.SaveEditorState("Paste items");
        }

        void CloneSelectedItemsToClipboard()
        {
            Tiles.Clear();
            Tiles.AddRange(CloneTiles(_levelEditor.GetSelectedTiles()));

            Labels.Clear();
            Labels.AddRange(CloneLabels(_levelEditor.GetSelectedLabels()));
        }

        static IEnumerable<Tile> CloneTiles(IEnumerable<Tile> tiles)
        {
            foreach (var tile in tiles)
            {
                var xmlSerializer = new XmlSerializer(typeof(Tile));
                string data;
                using (var textWriter = new StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, tile);
                    data = textWriter.ToString();
                }

                using (var textReader = new StringReader(data))
                {
                    var clone = (Tile)xmlSerializer.Deserialize(textReader);
                    clone.Position.X += 3;
                    clone.Position.Y += 3;
                    yield return clone;
                }
            }
        }

        static IEnumerable<Label> CloneLabels(IEnumerable<Label> labels)
        {
            foreach (var label in labels)
            {
                var xmlSerializer = new XmlSerializer(typeof(Label));
                string data;
                using (var textWriter = new StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, label);
                    data = textWriter.ToString();
                }

                using (var textReader = new StringReader(data))
                {
                    var clone = (Label)xmlSerializer.Deserialize(textReader);
                    clone.Position.X += 3;
                    clone.Position.Y += 3;
                    yield return clone;
                }
            }
        }

        int PasteItemsFromClipboard(LevelEditorState levelEditorState)
        {
            //Begin the paster operation
            if (Tiles.Count > 0)
                PasteTiles(levelEditorState);

            if (Labels.Count > 0)
                PasteLabels(levelEditorState);

            return Tiles.Count + Labels.Count;
        }

        void PasteTiles(LevelEditorState levelEditorState)
        {
            //Starting index for the 
            var startIndex = levelEditorState.Course.Tiles.Count;

            var clones = CloneTiles(Tiles);

            levelEditorState.Course.Tiles.AddRange(clones);

            levelEditorState.SelectedTileIds.Clear();

            for (var i = 0; i < Tiles.Count; i++)
                levelEditorState.SelectedTileIds.Add(i + startIndex);
        }

        void PasteLabels(LevelEditorState levelEditorState)
        {
            //Starting index for the 
            var startIndex = levelEditorState.Course.Labels.Count;

            var clones = CloneLabels(Labels);

            levelEditorState.Course.Labels.AddRange(clones);

            levelEditorState.SelectedLabelIds.Clear();

            for (var i = 0; i < Labels.Count; i++)
                levelEditorState.SelectedLabelIds.Add(i + startIndex);
        }
    }
}
