using System;
using Microsoft.Xna.Framework;

namespace Transition.Actors
{
    public class StandardAlienBullet : EnemyBullet
    {
        private const float Threshold = 20.0f;

        /// <summary>
        /// Creates a new instance of an AlienBullet
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public StandardAlienBullet(TransitionGame game) : base(game) { }

        /// <summary>
        /// Gets the type of actor.
        /// </summary>
        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.EnemyBullet; }
        }

        protected override void OnInitialise(string name)
        {
            base.OnInitialise(name);

            BlastTextureInfo = _game.TextureManager.GetTextureInfo("BlastEffect2pxFade");			
        }

        protected override void OnUpdate(GameTime gameTime) 
        {
            Pulsate(0.2f, 0.2f);

            if (Math.Abs(_game.ActivePlayer.Ship.Position.X - Position.X) < Threshold)
                return;
            
            Expire();
            Deactivate();
        }
    }
}
