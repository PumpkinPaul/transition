using Microsoft.Xna.Framework.Graphics;
using Transition.Rendering;
using Transition.ResourceManagers;

namespace Transition
{
    /// <summary>
    /// A style to render 'hit' effects.
    /// </summary>
    public class FlashRenderStyle : RenderStyle
    {
        public static FlashRenderStyle Instance;

        public override RenderStyleType RenderStyleId => RenderStyleType.Flash;

        protected override void OnRegister(EffectManager effectManager)
        {
            Instance = this;
            Effect = effectManager.GetEffect("FlashEffect");
        }

        public override void Setup(GraphicsDevice graphicsDevice)
        {
            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
        }
    }
}
