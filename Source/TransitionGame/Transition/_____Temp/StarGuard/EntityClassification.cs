﻿namespace Transition._____Temp.StarGuard
{
    /// <summary>
    /// The 'class' of entity
    /// </summary>
    /// <remarks>Allows for broad categorisation of things.</remarks>
    public enum EntityClassification
    {
        Player,
        Weapon,
        Bullet,
        CameraProxy,
        Enemy,
        EnemyBullet,
        Text,
        Flag,
        ParticleSystem,
        Sound,
        Scientist,
        ExplosiveBlock,
        DestructibleBlock,
        //SpecialEffect,
        Label,
        //Animated,
    }
}