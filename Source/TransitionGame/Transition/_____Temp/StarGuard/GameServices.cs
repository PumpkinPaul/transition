﻿using System;
using System.Collections.Generic;

namespace Transition._____Temp.StarGuard
{
    public interface IGameServices
    {
        void Add<T>(object o);
        T Get<T>();
    }

    public class GameServices : IGameServices
    {
        public static IGameServices Instance { get; set; }

        readonly Dictionary<Type, object> _services = new Dictionary<Type, object>();

        public void Add<T>(object o)
        {
            _services[typeof(T)] =  o;
        }

        public T Get<T>()
        {
            return (T)_services[typeof(T)];
        }
    }
}