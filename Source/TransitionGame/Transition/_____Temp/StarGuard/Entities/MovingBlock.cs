﻿using Microsoft.Xna.Framework;
using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Moving Block enemy
    /// </summary>
    [ProtoContract]
    public class MovingBlock : Enemy
    {
        public const float SpeedX = 0.06f;
        public const float SpeedY = 0.06f;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public MovingBlock(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-04.face-right.animation")
                    : Game.AnimationManager.Get("enemy-04.face-left.animation"));

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnUpdate(bool wasRewinding) 
        {
            Game.GameMap.HandleCollisions(this, respectPlatforms: false);
        }
        
        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            Position += Velocity;

            var coords = Game.GameMap.GetTileCoordinates(Position);

            switch (entityCollisionEdge)
            {
                case CollisionEdge.Left:
                    Velocity.X = 0;
                    var tileAbove = new Point(coords.X, coords.Y + 1);
                    if (Game.GameMap.GetTileCollision(tileAbove) != GameMap.TileCollision.Impassable)
                    {
                        Velocity.Y = SpeedY;
                        IsFacingRight = false;
                    }
                    else
                    {
                        Velocity.Y = -SpeedY;
                        IsFacingRight = true;
                    }
                    
                    break;

                case CollisionEdge.Right:
                    Velocity.X = 0;
                    var tileBelow = new Point(coords.X, coords.Y - 1);
                    if (Game.GameMap.GetTileCollision(tileBelow) != GameMap.TileCollision.Impassable)
                    {
                        Velocity.Y = -SpeedY;
                        IsFacingRight = true;
                    }
                    else
                    {
                        Velocity.Y = SpeedY;
                        IsFacingRight = false;
                    }
                    
                    break;

                case CollisionEdge.Top:
                    var tileRight = new Point(coords.X + 1, coords.Y);
                    if (Game.GameMap.GetTileCollision(tileRight) != GameMap.TileCollision.Impassable)
                    {
                        Velocity.X = SpeedX;
                        IsFacingRight = true;
                    }
                    else
                    {
                        Velocity.X = -SpeedX;
                        IsFacingRight = false;
                    }
                    
                    Velocity.Y = 0;
                    
                    break;

                case CollisionEdge.Bottom:
                    var tileLeft = new Point(coords.X - 1, coords.Y);
                    if (Game.GameMap.GetTileCollision(tileLeft) != GameMap.TileCollision.Impassable)
                    {
                        Velocity.X = -SpeedX;
                        IsFacingRight = false;
                    }
                    else
                    {
                        Velocity.X = SpeedX;
                        IsFacingRight = true;
                    }

                    Velocity.Y = 0;
                    break;
            }

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-04.face-right.animation")
                    : Game.AnimationManager.Get("enemy-04.face-left.animation"));
        }
    }
}