using System;
using System.Collections.Generic;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
    /// <summary>
    /// Configuration data for bullets.
    /// </summary>
    public class BulletBlueprint
    {
        public Type EntityClass;
        public string Name;
        public float SpeedMin;
        public float SpeedMax;
        public float Friction;
        public int LifeTicks;
        public float Spread;
        public bool FastMoving;
        public List<BulletUpgrade> Damage;
    }

    public class BulletUpgrade
    {
        public int Damage;
        public int Cost;
    }
}
