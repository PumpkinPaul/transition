using System;
using System.Collections.Generic;

namespace Transition.Actors
{
    /// <summary>
    /// Stores precreated actor objects in pools and allows the game to retrieve instances of them.
    /// </summary>
    public class ActorFactory
    {
        private class Factory
        {
            private readonly TransitionGame _game;
            private readonly int _maxItems;
            private int _currentId = -1;

            private readonly List<Actor> _pool;

            public string DefaultResourceName { get; private set; }

            public Factory(Type actorType, TransitionGame game, int maxItems, string defaultResourceName)
            {
                _game = game;
                _maxItems = maxItems;
                DefaultResourceName = defaultResourceName;

                _pool = new List<Actor>(_maxItems);

                for (var i = 0; i < _pool.Capacity; ++i)
                {
                    #if WINDOWS_STOREAPP
                        _pool.Add((T)typeof(T).GetTypeInfo().DeclaredConstructors.FirstOrDefault(info => info.IsPublic).Invoke(new object[] { this._game }));
                    #else
                        var constructor = actorType.GetConstructor(new [] { typeof(TransitionGame) });
                        if (constructor == null)
                            throw new InvalidOperationException("The object does not contain an appropriate constructor: e.g. MyClass(TransitionGame game)");
                        
                        _pool.Add((Actor)constructor.Invoke(new object[] { _game }));
                    #endif
                }
            }

            public Actor GetActor()
            {
                var attempts = 0;

                do
                {
                    ++_currentId;
                    if (_currentId >= _pool.Count)
                        _currentId = 0;

                    if (_pool[_currentId].Active == false && _pool[_currentId].Scene == null)
                        return _pool[_currentId];

                    ++attempts;

                } while (attempts <= _pool.Count);

                return null;
            }
        }

        private readonly TransitionGame _game;

        private readonly Dictionary<Type, Factory> _factories = new Dictionary<Type, Factory>();

        public ActorFactory(TransitionGame game)
        {
            _game = game;
        }

        public void RegisterFactory(Type actorType, int capacity) 
        {
            _factories.Add(actorType, new Factory(actorType, _game, capacity, actorType.Name));
        }

        public Actor Create(Type actorType, string key = null)
        {
            if (_factories.ContainsKey(actorType) == false)
                throw new Exception("Unhandled actor class passed in ActorFactory.Create: " + actorType);

            var factory = _factories[actorType];

            var name = key ?? factory.DefaultResourceName;

            var actor = factory.GetActor();

            if (actor != null)
                actor.Initialise(name);

            return actor;
        }
    }
}
