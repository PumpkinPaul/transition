using System.Collections.Generic;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public class CollisionMapManager
    {
        /// <summary></summary>
        private readonly Dictionary<int, int> _collisionMap = new Dictionary<int, int>();

        public void Register(ActorClassification actorClassification1, ActorClassification actorClassification2)
        {
            var key = (int)actorClassification1;

            //Add the item if it doesn't extsts - update it if it does
            if (!_collisionMap.ContainsKey(key))
                _collisionMap.Add(key, (1 << (int)actorClassification2));
            else
            {
                var value = _collisionMap[key];
                value = value | (1 << (int)actorClassification2);
                _collisionMap[key] = value;
            }
        }

        public bool TestTypesCollide(ActorClassification actorClassification1, ActorClassification actorClassification2)
        {
            var key1 = (int)actorClassification1;

            if (_collisionMap.ContainsKey(key1))
                return (_collisionMap[key1] & 1 << (int)actorClassification2) > 0;

            var key2 = (int)actorClassification2;

            if (_collisionMap.ContainsKey(key2))
                return (_collisionMap[key2] & 1 << (int)actorClassification1) > 0;

            return false;
        }
    }
}
