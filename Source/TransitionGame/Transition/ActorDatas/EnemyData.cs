namespace Transition.ActorDatas
{
    /// <summary>
    /// Configuration data for the game's enemies.
    /// </summary>
    public class EnemyData
    {
		/// <summary>The amount of points the player receives for killing the alien.</summary>
        public int ScoreBonus;
		
        /// <summary>The amount of cash the player receives for killing the alien.</summary>
        public int MoneyBonus;

		/// <summary>The speed the actor can move one tick</summary>
        public readonly float[] Speed = new float[TransitionGame.MaxDifficultyLevels];

        public float RageModifier = 1.0f;
    }
}
