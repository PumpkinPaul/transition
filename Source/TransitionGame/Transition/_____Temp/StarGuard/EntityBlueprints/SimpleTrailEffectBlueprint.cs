using Microsoft.Xna.Framework;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
    /// <summary>
    /// Configuration for simple trail effects.
    /// </summary>
    public class SimpleTrailEffectBlueprint
    {
        public Color HeadColor;
        public Color TailColor;
        public float HeadWidth;
        public float TailWidth;
        public float MaxLength;
    }
}
