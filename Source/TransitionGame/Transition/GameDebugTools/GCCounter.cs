﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Extensions;

namespace Transition.GameDebugTools
{
    /// <summary>
    /// Component for GC measure and draw.
    /// </summary>
    public class GCCounter : DrawableGameComponent
    {
        const int MaxGraphFrames = 60;
        const int MaxGraphBytes = 50 * 1024;
        const int AllocationBarWidth = 3;

        /// <summary>Gets current GC</summary>
        public float Collections { get; private set; }

        /// <summary>Gets/Sets GC sample duration.</summary>
        public TimeSpan SampleSpan { get; set; }

        // Reference for debug manager.
        private DebugManager _debugManager;

        // Stopwatch for GC measuring.
        private Stopwatch _stopwatch;

        WeakReference _gcTracker = new WeakReference(new object());

        // stringBuilder for GC counter draw.
        private readonly StringBuilder _stringBuilder = new StringBuilder(128);
        readonly StringBuilder _tempBuilder = new StringBuilder(32);

        private readonly Queue<long> _tickBytes = new Queue<long>(MaxGraphFrames);

        const int OneMegabyte = 1024 * 1024;
        
        long _previousMemory;
        long _baseMemory;
        long _registeredTickMemory;

        public long TotalMemoryBytes;
        public long TickMemoryBytes;

        public long TotalMemoryK { get { return TotalMemoryBytes / 1024; } }
        public long TickMemoryK { get { return TickMemoryBytes / 1024; } }

        readonly Color _panelColor = DebugSystem.Instance.DebugManager.OverlayColor;//Color.White * 0.15f; //new Color(0, 0, 0, 128)

        public GCCounter(Game game) : base(game)
        {
            SampleSpan = TimeSpan.FromSeconds(1);
        }

        public override void Initialize()
        {
            // Get debug manager from game service.
            _debugManager = Game.Services.GetService(typeof(DebugManager)) as DebugManager;

            if (_debugManager == null)
                throw new InvalidOperationException("DebugManager is not registered.");

            // Register 'GC' command if debug command is registered as a service.
            var host = Game.Services.GetService(typeof(IDebugCommandHost)) as IDebugCommandHost;

            if (host != null)
            {
                host.RegisterCommand("GC", "Garbage Collection Counter [on|off|collect]", CommandExecute);
                Visible = true;
            }

            // Initialize parameters.
            Collections = 0;
            _stopwatch = Stopwatch.StartNew();
            _stringBuilder.Length = 0;

            TotalMemoryBytes = GC.GetTotalMemory(false);
            _previousMemory = TickMemoryBytes;

            for (var i = 0; i < MaxGraphFrames; i++)
                _tickBytes.Enqueue(0);

            base.Initialize();
        }

        /// <summary>
        /// GC command implementation.
        /// </summary>
        private void CommandExecute(IDebugCommandHost host, string command, IList<string> arguments)
        {
            if (arguments.Count == 0)
                Visible = !Visible;

            foreach (var arg in arguments)
            {
                switch (arg.ToLower())
                {
                    case "on":
                        Visible = true;
                        break;
                    case "off":
                        Visible = false;
                        break;
#if WINDOWS || WINDOWS_STOREAPP || LINUX || MONOMAC
                    case "collect":
                        for(var i = 0; i < GC.MaxGeneration; ++i)
                            GC.Collect(0, GCCollectionMode.Forced);
                        break;
#endif
                    case "/?":
                    case "--help":
                        host.Echo("gc [on|off|collect]");
                        host.Echo("Options:");
                        host.Echo("       on     Display GC.");
                        host.Echo("       off    Hide GC.");
                        host.Echo("       collect  forces collection of all generations.");
                        break;
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (Visible == false)
                return;

            TotalMemoryBytes = GC.GetTotalMemory(false);
            TickMemoryBytes = TotalMemoryBytes - _previousMemory;
            _registeredTickMemory += TickMemoryBytes;
            _baseMemory += TickMemoryBytes;
            _previousMemory = TotalMemoryBytes;

            if (_tickBytes.Count == MaxGraphFrames)
                _tickBytes.Dequeue();

            _tickBytes.Enqueue(TickMemoryBytes);

            if (!_gcTracker.IsAlive)
            {
                Gearset.GS.Log("Garbage", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:fffffff") + " : Garbage Collection");
                _gcTracker = new WeakReference(new object());
                TickMemoryBytes = 0;
                _registeredTickMemory = 0;
                _baseMemory = 0;
            }

            if (_stopwatch.Elapsed <= SampleSpan)
                return;
        
            //Need to know if > 1mb as that is when we get a collection on the xbox
            if (_baseMemory > OneMegabyte) 
            {
                //XBox collection
#if WINDOWS || WINDOWS_STOREAPP || LINUX || MONOMAC
                Gearset.GS.Log("Garbage", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:fffffff") + " : XBOX360 Garbage Collection");
#endif
                _baseMemory = 0;
            }

            _stopwatch.Reset();
            _stopwatch.Start();

            // Update draw string.
            _stringBuilder.Length = 0;

            _stringBuilder.Append("Total Memory: ");
            _stringBuilder.Append("\n");
            _stringBuilder.AppendNumber(TotalMemoryBytes);
            _stringBuilder.Append("B");
            _stringBuilder.Append("\n");
            _stringBuilder.AppendNumber(TotalMemoryBytes / 1024.0f);
            _stringBuilder.Append("KB");
            _stringBuilder.Append("\n");
            _stringBuilder.AppendNumber(TotalMemoryBytes / 1024.0f / 1024.0f);
            _stringBuilder.Append("MB");
            _stringBuilder.Append("\n");
            _stringBuilder.Append("Tick Memory: ");
            _stringBuilder.Append("\n");
            _stringBuilder.AppendNumber(_registeredTickMemory / 1024.0f);
            _stringBuilder.Append("KB");
            _stringBuilder.Append("\n");

            var total = 0;
#if WINDOWS || WINDOWS_STOREAPP || LINUX || MONOMAC
            
            for(var i = 0; i < GC.MaxGeneration; ++i)
                total += GC.CollectionCount(i);
#else
            total = gcTrackerCount;
#endif         
            _stringBuilder.Append("GC Collections: ");
            _stringBuilder.Append("\n");
            _stringBuilder.Append("Total: ");
            _stringBuilder.AppendNumber(total);
            _stringBuilder.Append("\n");

#if WINDOWS || WINDOWS_STOREAPP
            for(var i = 0; i < GC.MaxGeneration; ++i)
            {
                _stringBuilder.Append("Gen ");
                _stringBuilder.AppendNumber(i);
                _stringBuilder.Append(": ");
                _stringBuilder.AppendNumber(GC.CollectionCount(i));
                _stringBuilder.Append("\n");
            }
#endif
    
            _stringBuilder.Append("Graph Scale: ");
            _stringBuilder.Append("\n");
            _stringBuilder.AppendNumber(MaxGraphBytes);
            _stringBuilder.Append("B");

            _registeredTickMemory = 0;
        }

        public override void Draw(GameTime gameTime)
        {
            var spriteBatch = _debugManager.SpriteBatch;
            var font = _debugManager.DebugFont;

            // Compute size of border area.
            var size = font.MeasureString(_stringBuilder);
            size.X = Math.Max(size.X, MaxGraphFrames * AllocationBarWidth);
            var rc = new Rectangle(0, 0, (int)(size.X) + 0, (int)(size.Y) + 0);

            var layout = new Layout(spriteBatch.GraphicsDevice.Viewport);
            rc = layout.Place(rc, 0.01f, 0.01f, Align.TopRight);

            // Place GC string in border area.
            size = font.MeasureString(_stringBuilder);
            layout.ClientArea = rc;
            var pos = layout.Place(size, 0.03f, 0, Align.CenterLeft);

            //Draw
            spriteBatch.Begin();

            //Draw bg and output text
            spriteBatch.Draw(_debugManager.WhiteTexture, rc, _panelColor);
            spriteBatch.DrawString(font, _stringBuilder, pos, Color.White);

            //Draw a bar that represent how far along we are from an Xbox GC (GCs every 1MB allocated)
            DrawXbox360AllocationsBar(rc, spriteBatch);

            //Draw a graph of the allocation memory for the last x many ticks
            DrawGraph(rc, spriteBatch); 

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void DrawXbox360AllocationsBar(Rectangle rc, SpriteBatch spriteBatch)
        {
            var gcRatio = MathHelper.Clamp(_baseMemory/(float) OneMegabyte, 0.0f, 1.0f);
            var barBg = new Rectangle(rc.X, rc.Y + rc.Height + 3, rc.Width, 10);
            var bar = new Rectangle(barBg.X, barBg.Y, (int) (rc.Width*gcRatio), 10);

            var ratioColor = Color.Green;
            if (gcRatio >= 0.8f)
                ratioColor = Color.Red;
            else if (gcRatio >= 0.5f)
                ratioColor = Color.Orange;

            spriteBatch.Draw(_debugManager.WhiteTexture, barBg, _panelColor);
            spriteBatch.Draw(_debugManager.WhiteTexture, bar, ratioColor);
        }

        private void DrawGraph(Rectangle rc, SpriteBatch spriteBatch)
        {
            var graphBounds = new Rectangle(rc.X + rc.Width - MaxGraphFrames * AllocationBarWidth, rc.Y + rc.Height + 3 + 10 + 3, MaxGraphFrames * AllocationBarWidth, 50);
            spriteBatch.Draw(_debugManager.WhiteTexture, graphBounds, _panelColor);

            _tempBuilder.Length = 0;
            _tempBuilder.AppendNumber(0);
            _tempBuilder.Append(" Bytes");
            
            var x = rc.X + rc.Width - (_tickBytes.Count*AllocationBarWidth);
            foreach (var bytes in _tickBytes)
            {
                if (bytes > 0)
                {
                    var clampedBarHeight = Math.Min(bytes, MaxGraphBytes);
                    var ratioBarHeight = clampedBarHeight/(float) MaxGraphBytes;
                    var scaledBarHeight = (int)(graphBounds.Height * ratioBarHeight);
                    var tickBar = new Rectangle(x, graphBounds.Y + graphBounds.Height - scaledBarHeight, AllocationBarWidth, scaledBarHeight);

                    var ratioColor = Color.Green;
                    if (ratioBarHeight >= 0.8f)
                        ratioColor = Color.Red;
                    else if (ratioBarHeight >= 0.5f)
                        ratioColor = Color.Orange;

                    spriteBatch.Draw(_debugManager.WhiteTexture, tickBar, ratioColor);
                }
                x += AllocationBarWidth;
            }
        }
    }
}
