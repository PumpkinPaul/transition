using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Transition.ActorDatas;
using Transition.Animations;
using Transition.Rendering;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Actor
    {
        public const int InfiniteHealth = -1;
        public const int MaximumDamage = int.MaxValue;
        public const int EternalLife = -1;

        /// <summary></summary>
        protected TransitionGame _game;

        /// <summary>Configuration data for the actor.</summary>
        protected ActorData _actorData;

        protected List<Sprite> Sprites = new List<Sprite>();

        /// <summary>The actors position in the world.</summary>
        public Vector2 Position;

        public bool Visible;

        /// <summary>The actors size in the world.</summary>
        protected Vector2 _scale = new Vector2(1.0f);

        /// <summary>The direction (angle in radians) the actor is 'facing'.</summary>
        protected float _direction; 

        /// <summary>The angle in radians the actor needs to be rotated around the z axis.</summary>
        protected float _rotation;

        public Vector2 PreviousOffset;

        /// <summary>The actors position in the world the previous update.</summary>
        protected Vector2 _previousPosition;

        /// <summary>The actors size in the world the previous update.</summary>
        protected Vector2 _previousScale;

        /// <summary>The angle in radians the actor was rotated around the z axis the previous update.</summary>
        protected float _previousRotation;

        /// <summary>The direction (angle in radians) the actor was 'facing' the previous update.</summary>
        protected float _previousDirection; 

        /// <summary>The actors velocity</summary>
		/// <remarks>Not resetting velocity each tick will allow phyics style behaviour like bouncing, gravity etc.
		/// Scene force is not added to velocity and is added to position directly in Actor.Move()</remarks>
        public Vector2 Velocity;

        /// <summary>The actors offset position (from their owner).</summary>
        protected Vector2 _offset; 

        /// <summary>The scene the actor belongs to.</summary>
        protected Scene _scene;

		/// <summary>.</summary>
        public Color Tint = Color.White;

        /// <summary>Whether the actor should be updated</summary>
        /// <remarks>Inactive actors get removed when they have been drawn for the last time.</remarks>
        protected bool _active;

        /// <summary>Whether a collidable acotr is currently collidable.</summary>
        protected bool _collidable;

        /// <summary>The number of game ticks the actor lives for.</summary>
        /// <remarks>Set to EternalLife to never have the actor age.</remarks>
        protected int _lifeTicks = Actor.EternalLife;

        /// <summary>The amount of health the actor has.</summary>
        protected int _health;
        protected int _cachedHealth;

        /// <summary>A reference to the actor who owns this actor.</summary>
        protected Actor _owner;

        /// <summary>The type of ownership behaviour performed by the actor.</summary>
        protected Ownership _ownership = Ownership.None;

        /// The type of behaviour to perform when an actor leaves the play area
        protected ConstrainBehaviour _horizontalConstrainBehaviour = ConstrainBehaviour.Bounce;
        protected ConstrainBehaviour _verticalConstrainBehaviour = ConstrainBehaviour.Bounce;

        protected Vector2 _constrainPadding;

        public virtual bool IsNukeable { get { return false; } }
		
        /// <summary></summary>
        protected List<Vector2> _forces = new List<Vector2>();
        
        /// <summary></summary>
        private float _spawningScale;

        /// <summary></summary>
        private bool _spawned;

        /// <summary>Id of the player who 'owns' the actor.</summary>
        /// <remarks>Allows us to tag actors (bullets for example so the correct player gets the score bonus).</remarks>
		protected int _playerId = -1;

        public int _hitTicks;

        /// <summary>Phase degrees for pulsating..</summary>
        protected float _pulse;

        public int Layer;

        private SimpleTrailEffect _simpleTrailEffect;

        protected float _collisionRadius;

        /// <summary>
        /// Creates a new instance of an Actor object
        /// </summary>
        /// <param name="game">A reference to the game</param>
        protected Actor(TransitionGame game)
        {
            _game = game;

            Layer = Layers.Actor;
        }

        /// <summary>
        /// Gets the type of actor
        /// </summary>
        public abstract ActorClassification ActorClassification { get; }
        
        /// <summary>
        /// Gets the configuration data for the actor. 
        /// </summary>
        public ActorData ActorData 
        {
            get { return _actorData; }
        }

        protected virtual void OnWarped() { }

        /// <summary>
        /// Gets a bitmask for testing collisions with.
        /// </summary>
        public virtual int CollisionMask
        {
            get { return 0; }
        }

        /// <summary>
        /// Gets whether the actor is active 
        /// </summary>
        public bool Active
        {
            get { return _active; }
        }

        /// <summary>
        /// Gets or sets whether the actor has fully spawned.
        /// </summary>
        public bool Spawned
        {
            get { return _spawned; }
            set { _spawned = value; }
        }

        /// <summary>
        /// Gets whether a collidable actor is currently collidable.
        /// </summary>
        public bool Collidable
        {
            get { return _collidable; }
            set { _collidable = value; }
        }

        /// <summary>The actor's current size when spawning.</summary>
        public float SpawningScale
        {
            get { return _spawningScale; }
            set 
			{ 
				_spawningScale = value; 
				
				//Do the scale too (I think)
				Scale = new Vector2(value);
			}
        }

        /// <summary>
        /// The number of game ticks the actor lives for.
        /// </summary>
        /// <remarks>Set to EternalLife to never have the actor age.</remarks>
        public int LifeTicks
        {
            get { return _lifeTicks; }
            set { _lifeTicks = value; }
        }

        /// <summary>A reference to the actor who owns this actor.</summary>
        public Actor Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }

        /// <summary>
        /// The type of ownership behaviour performed by the actor.
        /// </summary>
        public Ownership Ownership
        {
            get { return _ownership; }
            set { _ownership = value; }
        }

		/// <summary>
		/// Gets the type of behaviour to perform when an actor leaves the left / right of the play area
		/// </summary>
		public ConstrainBehaviour HorizontalConstrainBehaviour 
		{
            get { return _horizontalConstrainBehaviour; }
		}

        /// <summary>
		/// Gets the type of behaviour to perform when an actor leaves the top / bottom of the play area
		/// </summary>
		public ConstrainBehaviour VerticalConstrainBehaviour 
		{
            get { return _verticalConstrainBehaviour; }
		}
		
        /// <summary>
		/// Gets the distance from the world bounds we apply testing for out of bounds!
		/// </summary>
		public Vector2 ConstrainPadding
		{
            get { return _constrainPadding; }
		}

        /// <summary>
        /// Gets or sets the direction (angle in radians) the actor is 'facing'
        /// </summary>
        public float Direction
        {
            get { return _direction; }
            set { _direction = value; }
        }

        /// <summary>
        /// Gets or sets the direction (angle in radians) the actor is 'facing'
        /// </summary>
        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }
		
		/// <summary>
        /// Gets or sets the actors size in the world.
        /// </summary>
        public Vector2 Scale
        {
            get { return _scale; }
            set { _scale = value; }
        }
		
        /// <summary>
        /// Gets or sets the actors offset position (from their owner).
        /// </summary>
        public Vector2 Offset
        {
            get { return _offset; }
            set { _offset = value; }
        }

        /// <summary>
        /// Sets the scene the actor belongs to.
        /// </summary>
        public Scene Scene
        {
            get { return _scene; }
            set { _scene = value; }
        }

        /// <summary>
        /// Gets the size of the collision radius.
        /// </summary>
        public float CollisionRadius
        {
            get { return _scale.X * _collisionRadius; }
        }
		
		/// <summary>
        /// Gets or sets the Id of the 'owning' player.
        /// </summary>
		public int PlayerId
		{
			get { return _playerId; }
			set { _playerId = value; }
		}

        public int Health { get { return _health; } }

        public void CacheHealth()
        {
            _cachedHealth = _health;
            _health = InfiniteHealth;
        }

        public void ResetHealthFromCache()
        {
            _health = _cachedHealth;
        }

        public void Flash()
        {
            _hitTicks = 8;
        }

        public void Initialise(string name)
        {
            _actorData = _game.DataManager.ActorData(name);

            Position = Vector2.Zero;
            _rotation = 0.0f;  
            _direction = 0.0f;      
            Velocity = Vector2.Zero;
            _offset = Vector2.Zero; 
            _scene = null;
            Tint = Color.White; 
            _lifeTicks = Actor.EternalLife;
            _health = _actorData.InitialHealth;
            _cachedHealth = 0;
            _owner = null;
            _ownership = Ownership.None;
        
            _forces.Clear();
		    _playerId = -1;
            _pulse = 0.0f;
            _hitTicks = 0;
            Visible = true;

            _simpleTrailEffect = null;

            PreviousOffset = _offset;
            _previousPosition = Position;

            _previousRotation = 0;
            _previousDirection = 0;

            DealocateSprites();

            if (_actorData.Layers != null)
            {
                var layers = _actorData.Layers.Length;
                for(var i = 0; i < layers; i++)
                {
                    var sprite = _game.SpritePool.Allocate();
                    if (sprite == null)
                        break;

                    sprite.SetAppearance(_actorData.Layers[i].Appearance);
                    sprite.Layer = _actorData.Layers[i].DrawLayer;
                    Sprites.Add(sprite);
                }
            }

            _collisionRadius = _actorData.CollisionRadius;

            OnInitialise(name);
        }

        protected virtual void OnInitialise(string name) { }

        /// <summary>
        /// Adds a force to the actor
        /// </summary>
        /// <param name="force">The direction and length of the force</param>
        public void AddForce(Vector2 force)
        {
            _forces.Add(force);
        }

        /// <summary>
        /// Called when an actor is added to the scene.
        /// </summary>
        public void AddedToScene()
        {
            if (_game.GameOptions.TrailEffects && GetType() != typeof(Ship))
                CreateParticleSystems(_actorData.TrailEffects, Ownership.AutoDeactivate);

            if (_actorData.SpawnScaleIncrement <= 0.0f) 
            {
		        _spawningScale = 1.0f;
		        _spawned = true;
		        _collidable = true;
	        }
	        else 
            {
		        _spawningScale = 0.0f;
		        _scale = Vector2.Zero;
		        _spawned = false;
		        _collidable = false;
	        }

            _active = true;
            SavePhysicalState();

            CreateParticleSystems(_actorData.SpawnEffects, Ownership.FlyTheNest);

            if (_actorData.SimpleTrail != null)
            {
                var tint = Tint;
                _simpleTrailEffect = (SimpleTrailEffect)_game.ActorFactory.Create(typeof(SimpleTrailEffect));
                if (_simpleTrailEffect != null)
                {
                    _simpleTrailEffect.Owner = this;
                    _simpleTrailEffect.Ownership =  Ownership.FlyTheNest;       
                    _simpleTrailEffect.Setup(tint, tint * 0.0f, _actorData.SimpleTrail.HeadWidth, _actorData.SimpleTrail.TailWidth, _actorData.SimpleTrail.MaxLength);
                    _simpleTrailEffect.TailPosition = Position;
                    _simpleTrailEffect.Direction = Direction;
                    _scene.AddActor(_simpleTrailEffect);
                }
            }

            OnAddedToScene();
        }

        /// <summary>
        /// Occurs when thwe actor is added to a scene.
        /// </summary>
        protected virtual void OnAddedToScene() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            HandleOwnership();

            if (_active == false)
                return;

            Age();

            foreach(var sprite in Sprites)
            {
                if (sprite != null)
                    sprite.Update();
            }

            //SavePhysicalState();
            OnUpdate(gameTime);
            SavePhysicalState();
            ApplyForces();         

            //If still active and it's a collider add to the collision list
            if (_active && (CollisionMask > 0) && _collidable && IsOnScreen)
                _scene.AddCollider(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        protected virtual void OnUpdate(GameTime gameTime) { }

        /// <summary>
        /// Processing for actors 'owned' by another actor.
        /// </summary>
        private void HandleOwnership()
        {
            if (_owner == null)
                return;

            if (_owner.Active)
            {
                Position = _owner.Position + _offset;
                Velocity = _owner.Velocity;
            }

            //Is our owner deactivating?
            if (_owner.Active == false && _ownership == Ownership.AutoDeactivate)
            {
                _owner = null;
                Deactivate();
            }
        }

        /// <summary>
        /// Calculates the actor's total velocity by adding up all the forces acting on it.
        /// </summary>
        private void ApplyForces()
        {
            foreach (Vector2 force in _forces)
                Velocity += force;

            _forces.Clear();
        }

        /// <summary>
        /// Age the actor.
        /// </summary>
        private void Age()
        {
            if (_hitTicks > 0)
                --_hitTicks;

            if (_lifeTicks == Actor.EternalLife)
                return;

            --_lifeTicks;
            if (_lifeTicks >= 0)
                return;

            //Actor has expired due to old age
            Expire(); 
        }

        /// <summary>
        /// 
        /// </summary>
	    void SavePhysicalState()
	    {
            PreviousOffset = Offset;
            _previousPosition = Position;
            _previousRotation = _rotation;
            _previousScale = _scale;
            _previousDirection = _direction;
	    }

        /// <summary>
        /// Allow all actors to do something after the Update PHASE (not just update, it's update, apply forces, move, constrain, etc).
        /// </summary>
        public void PostUpdate()
        {
            OnPostUpdate();
        }

        protected virtual void OnPostUpdate() { }

        /// <summary>
        /// Called when another actor collides with us.
        /// </summary>
        /// <param name="collider">The actor colliding with us.</param>
        /// <param name="time">The normaised time (0..1) the collision occurred.</param>
        public void Collide(Actor collider, float time)
        {
            if (_active && collider.Active && _collidable && collider.Collidable)
            {
                OnCollision(collider, time);
                if (_active && collider.Active)
                    collider.OnCollision(this, time);
            }
        }

        /// <summary>
        /// Register a collision hit with an actor.
        /// </summary>
        /// <param name="actor">The actor we hit.</param>
        /// <param name="damage">The amount of damage to apply to the hit actor.</param>
        public void RegisterHit(Actor actor, int damage)
        {
            if (_health == Actor.InfiniteHealth)
                return;

            _health -= damage;
            if (_health <= 0)
            {
                _health = 0;

                Explode(); //pretty particle effects
                Deactivate(); //flag actor for removal from scene
            }
            else
                _hitTicks = 8;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Move()
        {
            if (_active == false)
                return;

            if (_ownership != Ownership.None && _owner.Active)
                Position = _owner.Position + _offset;
            else
                Position += Velocity;

            OnMove();
        }
		
		/// <summary>
        /// Called when an actor is constrained to world bounds.
        /// </summary>
        public void Constrained(bool constrainedTop, bool constrainedLeft, bool constrainedBottom, bool constrainedRight)
        {
			OnConstrained(constrainedTop, constrainedLeft, constrainedBottom, constrainedRight);
        }
        
        public bool IsOnScreen 
        {
            get 
            {    
                //TODO: Transition
                return true;
                //var size = Scale * _actorData.BaseSize;
                //var halfWidth = size.X * 0.5f;

                //if (Position.X + halfWidth < _game.VisibleWorldBounds.Left || Position.X - halfWidth > _game.VisibleWorldBounds.Right) 
                //    return false;

                //return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Draw()
        {
            var isOnScreen = IsOnScreen;
            if (isOnScreen)
            {
                foreach(var sprite in Sprites)
                {
                    TextureInfo textureInfo = null;
                    var layer = Layer;

                    if (sprite != null)
                    {
                        if (sprite.Visible == false)
                            continue;

                        textureInfo = sprite.TextureInfo;
                        layer = sprite.Layer;
                    }
                
                    if (textureInfo != null && Visible)
                    {
                        RenderStyle renderStyle = TransparentRenderStyle.Instance;
                        //RenderStyle renderStyle = PointFilterRenderStyle.Instance;
                        var tint = Tint;

                        if (_hitTicks > 0)
                        {
                            tint = Color.White;
                            renderStyle = FlashRenderStyle.Instance;
                        }

                        var q = new Quad(Position, Scale * _actorData.BaseSize, Rotation);
                        _game.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, tint, renderStyle, layer);
                    }
                }
            }

            if (_simpleTrailEffect != null)
                _simpleTrailEffect.DrawEffect(); 

            OnDraw(isOnScreen);
        }

		/// <summary>
        /// Called when an actor has been destroyed.
        /// </summary>
        public void Explode()
        {
            CreateParticleSystems(_actorData.ExplosionEffects, Ownership.FlyTheNest);

            if (!string.IsNullOrEmpty(_actorData.ExplosionSfx))
                _game.PlaySound(_actorData.ExplosionSfx);

            OnExplode();
        }

        /// <summary>
        /// Actor no longer plays an active part in scene and will be flagged for removal.
        /// </summary>
        public void Deactivate()
        {
            _active = false;
            _simpleTrailEffect = null;

            OnDeactivate();  
        }

        /// <summary>
        /// Called when an actor dies of 'old age'
        /// </summary>
        protected void Expire()
        {
            CreateParticleSystems(_actorData.ExpiredEffects, Ownership.FlyTheNest);
            Deactivate();
            OnExpire();
        }

        /// <summary>
        /// Actor was removed from the scene.
        /// </summary>
        public void RemovedFromScene()
        {
            Scene = null;
            _active = false;

            DealocateSprites();

            OnRemovedFromScene();
        }

        private void DealocateSprites()
        {
            foreach (var sprite in Sprites)
                _game.SpritePool.Deallocate(sprite);
            
            Sprites.Clear();
        }

        /// <summary>
        /// Weapon has expired;
        /// </summary>
        public void ExpireWeapon(Weapon weapon)
        {
            OnExpireWeapon(weapon);
        }

        /// <summary>
        /// Creates a list of particle system
        /// </summary>
        protected void CreateParticleSystems(List<ParticleEffect> effects, Ownership ownership)
        {
            foreach (var particleEffect in effects)
                CreateParticleSystem(particleEffect, ownership);
        }

        /// <summary>
        /// Creates a particle system
        /// </summary>
        /// <param name="particleEffect"></param>
        /// <param name="ownership"></param>
        /// <returns></returns>
        protected ParticleSystem CreateParticleSystem(ParticleEffect particleEffect, Ownership ownership)
        {
            var particleSystem = (ParticleSystem)(_game.ActorFactory.Create(typeof(ParticleSystem)));

            if (particleSystem == null)
                return null;

            particleSystem.PlayerId = _playerId;
            particleSystem.Template = particleEffect.Template;
            particleSystem.Tint = Tint;// new Colour(Tint.B, Tint.R, Tint.G, Tint.A);
            //particleSystem.Tint = ColourScheme.Colours[0].BaseColour;
            particleSystem.Lifetime = particleEffect.Lifetime;
            particleSystem.Position = Position;
            particleSystem.Offset = particleEffect.Offset;
            particleSystem.Ownership = ownership;
            
            if (ownership != Ownership.None)
                particleSystem.Owner = this;

            _scene.AddActor(particleSystem);

            return particleSystem;
        }

        /// <summary>
        /// Pulsate the antimatter.
        /// </summary>
        protected void Pulsate(float frequency, float amplitude)
        {
            float modifier = 1.0f + ((float)Math.Sin(_pulse) * amplitude);

            _pulse += frequency;
            if (_pulse > 360.0f)
                _pulse -= 360.0f;

            Scale = Vector2.One * modifier;
        }

        /// <summary>
        /// Called after the actor has been moved.
        /// </summary>
        protected virtual void OnMove() { }

		/// <summary>
        /// Called after an actor was constrained in the Y axis
        /// </summary>
        protected virtual void OnConstrained(bool constrainedTop, bool constrainedLeft, bool constrainedBottom, bool constrainedRight) { }
		
        /// <summary>
        /// Occurs when the actor collides with another actor.
        /// </summary>
        /// <param name="actor">The actor colliding.</param>
        /// <param name="time">The normalised time (0..1) of the collision.</param>
        protected virtual void OnCollision(Actor actor, float time) { }
        		
		/// <summary>
        /// 
        /// </summary>
        protected virtual void OnExpireWeapon(Weapon weapon) { }

        /// <summary>
        /// Called when an actor dies of 'old age'
        /// </summary>
        protected virtual void OnExpire()
        {
		    Deactivate();
        }

        /// <summary>
        /// Called when an actor has been flagged for removal from the scene.
        /// </summary>
        protected virtual void OnDeactivate() { }

        /// <summary>
        /// Occurs when an actor is removed from a scene.
        /// </summary>
        protected virtual void OnRemovedFromScene() { }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnExplode() { }
        
        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnDraw(bool isOnscreen) { }
    }
}
