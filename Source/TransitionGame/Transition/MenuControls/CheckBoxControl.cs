using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control for displaying boolean responses.
    /// </summary>
    public class CheckBoxControl : MenuControl
    {			
		/// <summary>The size of the image.</summary>
		public Vector2 Size;

        /// <summary>The id of the texture to use for drawing.</summary>
        public TextureInfo OnTextureInfo;
        public TextureInfo OffTextureInfo;

        public bool Value;

        public override bool AutoTabIndex { get { return true; } }

        /// <summary>
        /// Initialises a new instance of an ImageControl.
        /// </summary>
		/// <param name="game">A reference to the game.</param>
		public CheckBoxControl(BaseGame game) : base(game) 
        { 
            OnTextureInfo = Game.TextureManager.GetTextureInfo("CheckBoxOn");
            OffTextureInfo = Game.TextureManager.GetTextureInfo("CheckBoxOff");

            Size = new Vector2(1.0f);
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            Size = element.GetAttributeVector2("size", Size);
            OnTextureInfo = ParseTexture(element, "onTextureInfo", OnTextureInfo);
            OffTextureInfo = ParseTexture(element, "offTextureInfo", OffTextureInfo);
            Value = element.GetAttributeBool("value", Value);
        }
        /// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
		public override Rect Bounds
		{
            get { return new Rect(Position - (Size / 2.0f), Size); }
		}

        protected override bool OnClick() {
            
            Value = !Value;

            return true;
        }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
		{
            var tint = EffectiveTint;
            
            var q = new Quad(Position, Size, 0);

            if (Value)
                Game.Renderer.DrawSprite(ref q, OnTextureInfo.TextureId, OnTextureInfo.SourceRect, tint, RenderStyle, Layer);
            else
                Game.Renderer.DrawSprite(ref q, OffTextureInfo.TextureId, OffTextureInfo.SourceRect, tint, RenderStyle, Layer);
		}
    }
}
