using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.Rendering
{
    /// <summary>
    /// Represents the data needed by the GFX card to render some arbitrary geometry!.
    /// </summary>
    /// <remarks>
    /// Rendering of these thngs is restricted to TRIANGLE_LIST for simplicity.
    /// 
    /// The sprites are rendered like so using GL_TRIANGLES
    /// 1-----3
    /// |\    |
    /// |  \  |
    /// |    \|
    /// 0-----2
    /// 
    /// Therefore the indices would be { 0, 1, 2, 2, 1, 3 }
    /// </remarks>
    public abstract class Geometry
    {
        public Type VertexType { get; protected set; }

        /// <summary>The z-order of the geometry - think broad layers like 'background', 'aliens', 'effects'.</summary>
        public int Layer;

        /// <summary>The order the geom was added to the render queue - stops flickering of sprite drawing order.</summary>
        public int Rank;

        public int TextureId;
        public RenderStyle RenderStyle;

        //Geometry definitions (vertices and indices)
        //public readonly List<VertexPositionColorTexture> Vertices = new List<VertexPositionColorTexture>(4);
        public ushort[] Indices { get; set; }

        //Allows us to specify the number of indices to actually use (we might have a BIG array but only want to use part of it)
        public int? IndexCount { get; set; }

        public abstract int VertexCount { get; }
    }

    public class Geometry<T> : Geometry where T : IVertexType
    {
        public readonly List<T> Vertices;

        public override int VertexCount => Vertices.Count;

        public Geometry(int size = 0)
        {
            VertexType = typeof(T);
            Vertices = new List<T>(size);
        }
    }
}