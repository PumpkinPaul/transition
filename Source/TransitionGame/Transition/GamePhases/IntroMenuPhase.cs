namespace Transition.GamePhases
{
    /// <summary>
    /// Responsible for the IntroMenu phase.
    /// </summary>
    public class IntroMenuPhase : GamePhase 
    { 
        public IntroMenuPhase(BaseGame game) : base(game) { }
    }
}

