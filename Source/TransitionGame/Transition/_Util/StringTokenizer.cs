/*
 * Copyright (c) 2003-onwards Shaven Puppy Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of 'Shaven Puppy' nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System.Collections.Generic;

namespace Transition.Util
{
    /// <summary>
	/// A String Tokenizer that accepts Strings as source and delimiter. Only 1 delimiter is supported (either String or char[]).
	/// </summary>
	public class StringTokenizer
	{
        private static readonly char[] DefaultDelims = {' ', '\t', '\n', '\r', '\f' };
		private int _currIndex;
		private int _numTokens;
		private readonly IList<string> _tokens;
        private char[] _delimiter;
        private readonly bool _returnDelims;
		
        /// <summary>
		/// Constructor for StringTokenizer Class.
		/// </summary>
		/// <param name="source">The Source String.</param>
		/// <param name="delimiter">The Delimiter String. If a 0 length delimiter is given, " " (space) is used by default.</param>
        /// <param name="returnDelims">Whether the delims should be considered tokens.</param>
		public StringTokenizer(string source, char[] delimiter, bool returnDelims) 
		{
			_tokens = new List<string>(10);
			Source = source;
			_delimiter = delimiter;
            _returnDelims = returnDelims;

			if(delimiter.Length == 0)
				_delimiter = DefaultDelims;
			
			Tokenize();
		}

        /// <summary>
		/// Constructor for StringTokenizer Class.
		/// </summary>
		/// <param name="source">The Source String.</param>
		/// <param name="delimiter">The Delimiter String. If a 0 length delimiter is given, " " (space) is used by default.</param>
        /// <param name="returnDelims">Whether the delims should be considered tokens.</param>
		public StringTokenizer(string source, string delimiter, bool returnDelims) : this(source, delimiter.ToCharArray(), returnDelims) { }

        /// <summary>
		/// Constructor for StringTokenizer Class.
		/// </summary>
		/// <param name="source">The Source String.</param>
		/// <param name="delimiter">The Delimiter String. If a 0 length delimiter is given, " " (space) is used by default.</param>
		public StringTokenizer(string source, string delimiter) : this(source, delimiter.ToCharArray(), false) { }

		/// <summary>
		/// Constructor for StringTokenizer Class.
		/// </summary>
		/// <param name="source">The Source String.</param>
		/// <param name="delimiter">The Delimiter String as a char[]. This is converted into a single String and
		/// expects Unicode encoded chars.</param>
		public StringTokenizer(string source, char[] delimiter) : this(source, delimiter, false) { }

		/// <summary>
		/// Constructor for StringTokenizer Class.  The default delimiter of " " (space) is used.
		/// </summary>
		/// <param name="source">The Source String.</param>
		public StringTokenizer(string source) : this(source, DefaultDelims, false) { }

		/// <summary>
		/// Empty Constructor.  Will create an empty StringTokenizer with no source, no delimiter, and no tokens.
		/// If you want to use this StringTokenizer you will have to call the NewSource(string s) method.  You may
		/// optionally call the NewDelim(string d) or NewDelim(char[] d) methods if you don't with to use the default
		/// delimiter of " " (space).
		/// </summary>
		public StringTokenizer() : this("", DefaultDelims) { }

        /// <summary>
        /// 
        /// </summary>
		private void Tokenize()
		{
            _numTokens = 0;
            _tokens.Clear();
            _currIndex = 0;

            if (string.IsNullOrEmpty(Source))
                return;

            var index = 0;
            var temp = Source;
            while (index >= 0)
            {
                const int startIndex = 0;
                index = temp.IndexOfAny(_delimiter, startIndex);
                if (index <= -1)
                    continue;
                
                //Delimeter at start
                if (index == 0)
                {
                    if (_returnDelims)
                        _tokens.Add(temp.Substring(0, 1));
                        
                    temp = temp.Substring(1);//, source.Length - 1);
                }
                else
                {
                    _tokens.Add(temp.Substring(0, index));
                    temp = temp.Substring(index);//, source.Length - 1);

                    if (_returnDelims)
                        _tokens.Add(temp.Substring(0 , 1));

                    temp = temp.Substring(1);//, source.Length - 1);
                }
            }

            //Might have a bit left over.
            if (temp.Length > 0)
                _tokens.Add(temp);

            _numTokens = _tokens.Count;
        }

		/// <summary>
		/// Method to add or change this Instance's Source string.  The delimiter will
		/// remain the same (either default of " " (space) or whatever you constructed 
		/// this StringTokenizer with or added with NewDelim(string d) or NewDelim(char[] d) ).
		/// </summary>
		/// <param name="newSrc">The new Source String.</param>
		public void NewSource(string newSrc)
		{
			Source = newSrc;
			Tokenize();
		}

		/// <summary>
		/// Method to add or change this Instance's Delimiter string.  The source string
		/// will remain the same (either empty if you used Empty Constructor, or the 
		/// previous value of source from the call to a parameterized constructor or
		/// NewSource(string s)).
		/// </summary>
		/// <param name="newDel">The new Delimiter String.</param>
		public void NewDelim(char[] newDel)
		{
		    _delimiter = newDel.Length == 0 ? DefaultDelims : newDel;

		    Tokenize();
		}

        /// <summary>
		/// Method to add or change this Instance's Delimiter string.  The source string
		/// will remain the same (either empty if you used Empty Constructor, or the 
		/// previous value of source from the call to a parameterized constructor or
		/// NewSource(string s)).
		/// </summary>
		/// <param name="newDel">The new Delimiter as a char[]. This is converted into a single String and
		/// expects Unicode encoded chars.</param>
		public void NewDelim(string newDel)
        {
            _delimiter = string.IsNullOrEmpty(newDel) ? DefaultDelims : newDel.ToCharArray();

            Tokenize();
        }

        /// <summary>
		/// Method to get the number of tokens in this StringTokenizer.
		/// </summary>
		/// <returns>The number of Tokens in the internal ArrayList.</returns>
		public int CountTokens()
		{
			return _numTokens;
		}

		/// <summary>
		/// Method to probe for more tokens.
		/// </summary>
		/// <returns>true if there are more tokens; false otherwise.</returns>
		public bool HasMoreTokens()
		{
			if(_currIndex <= (_tokens.Count-1))
				return true;
			
			return false;
		}

        public bool HasMoreElements()
        {
            return HasMoreTokens();
        }

		/// <summary>
		/// Method to get the next (string)token of this StringTokenizer.
		/// </summary>
		/// <returns>A string representing the next token; null if no tokens or no more tokens.</returns>
		public string NextToken()
		{
		    if(_currIndex <= (_tokens.Count-1))
			{
				var retString = _tokens[_currIndex];
				_currIndex ++;
				return retString;
			}
			
			return null;
		}

        /// <summary>
        /// Gets the Source string of this Stringtokenizer.
        /// </summary>
        /// <returns>A string representing the current Source.</returns>
        public string Source { get; private set; }

        /// <summary>
		/// Gets the Delimiter string of this StringTokenizer.
		/// </summary>
		/// <returns>A string representing the current Delimiter.</returns>
		public string Delim
		{
			get { return _delimiter.ToString(); }
		}
	}

}
