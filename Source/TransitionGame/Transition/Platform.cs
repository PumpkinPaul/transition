using System.IO;
#if WINDOWS_STOREAPP
	using Windows.ApplicationModel;
#endif

namespace Transition
{
    /// <summary>
    /// Responsible for encapsulating platform specific details
    /// </summary>
    public static class Platform
    {
		public static string Root
        {
            get 
            {
				#if PSM
				    return "./Application";
				#else
				    return "";
				#endif
            }
        }
		
		public static string ContentFolderName
        {
            get 
            {
				#if WINDOWS_STOREAPP
                	return "ContentWindows8";
				#elif LINUX
				    return "ContentLinux";
				#elif MONOMAC
				    return "ContentOSX";
				#elif PSM
				    return "ContentPSM";
				#else
				    return "Content";
				#endif
            }
        }
		
	    public static string ContentPath
        {
            get 
            {
            	return Path.Combine(Root, ContentFolderName);
			}
		}

		public static string ResourcesFolderName
		{
			get 
			{
				#if WINDOWS_STOREAPP
				return "ContentWindows8";
				#elif LINUX
				return "Resources";
				#elif MONOMAC
				return "Resources";
				#elif PSM
				return "ContentPSM";
				#else
				return "Content";
				#endif
			}
		}

		public static string ResourcesPath
		{
			get 
			{
				return Path.Combine(Root, ResourcesFolderName);
			}
		}
    }
}
