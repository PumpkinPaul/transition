﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition._____Temp.StarGuard.Entities;

namespace Transition._____Temp.StarGuard
{
    [ProtoContract]
    public class World
    {
        const string DefaultKey = null;

        //An ordered list of entities where we can also look up an object by a key - an exception is thrown if a duplicate key is added.
        [ProtoMember(1)]
        readonly HashList<string, Entity> _entities = new HashList<string, Entity>(16, EqualityComparer<string>.Default, DefaultKey);

        [ProtoMember(2)]
        public bool IsRewinding;

        [ProtoMember(3)]
        public int FrameId { get; private set; }

        readonly List<Entity>_potentialColliders = new List<Entity>();
        readonly List<CollisionPacket>_actualColliders = new List<CollisionPacket>();

        public Entity this[int index] => _entities[index];
        public Entity this[string key] => _entities[key];

        public Entity Get(int index) { return _entities[index]; }
        public Entity Get(string key) { return _entities[key]; }

        public T Get<T>(int index) where T : Entity { return (T)_entities[index]; }
        public T Get<T>(string key) where T : Entity { return (T)_entities[key]; }

        public void Add(Entity entity)
        {
            _entities.Add(DefaultKey, entity);

            entity.AddedToWorld(this);
        }

        public void Add(string name, Entity entity)
        {
            _entities.Add(name, entity);

            entity.AddedToWorld(this);
        }

        public bool Remove(string key)
        {
            return _entities.Remove(key);
        }

        public void Remove(int index)
        {
            _entities.RemoveAt(index);
        }

        public void AddGameMapCollider(Entity entity) 
        {

        }

        public void AddEntityCollider(Entity entity) 
        {
            _potentialColliders.Add(entity);
        }

        public void BeforeRewind()
        {
            foreach (var entity in _entities.List)
                entity.Value.BeforeRewind();
        }

        public void StateLoaded(bool rewinding)
        {
            foreach (var entity in _entities.List)
                entity.Value.StateLoaded(this, rewinding);
        }

        public void Update(bool wasRewinding)
        {
            FrameId++;

            RemoveInactiveEntities();
            UpdateEntities(wasRewinding);
            TestForWorldCollisions();
            TestForCollisions();
            ProcessCollisions();
            MoveEntities();
            PostUpdateEntities();
        }

        void RemoveInactiveEntities()
        {
            foreach (var entity in _entities.List)
            {
                if (entity.Value.Active == false)
                    entity.Value.RemovedFromWorld(this);
            }

            for (var i = _entities.List.Count - 1; i >= 0; --i)
            {
                if (_entities[i].Active == false)
                    _entities.RemoveAt(i);
            }
        }

        void UpdateEntities(bool wasRewinding)
        {
            for (var index = 0; index < _entities.List.Count; index++)
            {
                var entity = _entities[index];
                entity.Update(wasRewinding);
            }
        }

        void TestForWorldCollisions()
        {

        }

        void TestForCollisions()
        {
            for (var i = 0; i < _potentialColliders.Count; ++i)
            {
                for (var j = i + 1; j < _potentialColliders.Count; ++j)
                {
                    var entity1 = _potentialColliders[i];
                    var entity2 = _potentialColliders[j];

                    if (TransitionGame.Instance.NewCollisionMapManager.TestClassificationsCollide(entity1.Classification, entity2.Classification) == false)
                        continue;
                    
                    float time;
                    float time2;

                    var worldBounds1 = entity1.GetBoundingBox();
                    var worldBounds2 = entity2.GetBoundingBox();
                    var centreOfGravity1 = new Vector2(worldBounds1.Left + ((worldBounds1.Right - worldBounds1.Left) / 2.0f), worldBounds1.Bottom + ((worldBounds1.Top - worldBounds1.Bottom) / 2.0f));
                    var centreOfGravity2 = new Vector2(worldBounds2.Left + ((worldBounds2.Right - worldBounds2.Left) / 2.0f), worldBounds2.Bottom + ((worldBounds2.Top - worldBounds2.Bottom) / 2.0f));
                    //if (Collisions.TestSweptCircleCircle(entity1.Position, entity1.Velocity, entity1.CollisionBounds.Width / 2.0f, entity2.Position, entity2.Velocity, entity2.CollisionBounds.Width / 2.0f, out time))
                    if (Collisions.TestAABBSweep(new Vector2(entity1.CollisionBounds.Width, entity1.CollisionBounds.Height) * 0.5f, centreOfGravity1, entity1.Velocity, new Vector2(entity2.CollisionBounds.Width, entity2.CollisionBounds.Height) * 0.5f, centreOfGravity2, entity2.Velocity, out time, out time2))
                        _actualColliders.Add(new CollisionPacket(entity1, entity2, time));
                }
            }

            _potentialColliders.Clear();
        }

        void ProcessCollisions()
        {
            //Sort collisions based on time.
            _actualColliders.Sort(CollisionPacketComparer.Default);

            //Process each collison
            foreach (var packet in _actualColliders)
                packet.Entity1.Collide(packet.Entity2, packet.Time);

            _actualColliders.Clear();
        }

        void MoveEntities()
        {
            foreach (var entity in _entities.List)
                entity.Value.Move();
        }

        void PostUpdateEntities()
        {
            foreach (var entity in _entities.List)
                entity.Value.PostUpdate();
        }
        
        public void Draw()
        {
            foreach (var entity in _entities.List)
                entity.Value.Draw();
        }

        public IEnumerable<KeyValuePair<string,Entity>> Entities => _entities.List;

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        private void AfterDeserialization() { }   
    }
}