using Microsoft.Xna.Framework;

namespace Transition.CourseSystem
{
    public class Tile 
    {
        public Color Color { get; set; }
        public Vector2 Position;
        public float Rotation { get; set; }
        public Vector2 Scale { get; set; }
        public string Texture { get; set; }
    }
}
