using System.Collections.Generic;

namespace Transition
{
    /// <summary>
    /// .
    /// </summary>
    public class LevelEvent
    {
        public int Ticks;

        public List<LevelAction> Actions = new List<LevelAction>();
    }
}