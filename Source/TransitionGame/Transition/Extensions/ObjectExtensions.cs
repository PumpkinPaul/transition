using System.Linq;

namespace Transition.Extensions
{
    public static class ObjectExtensions
    {
        public static bool In<T>(this T o, params T[] items)
        {
            return items.Contains(o);
        }
    }
}
