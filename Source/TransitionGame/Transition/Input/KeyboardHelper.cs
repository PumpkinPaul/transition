using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace Transition.Input
{
    /// <summary>
    /// Input helper class, captures all keyboard input and provides some nice helper methods and properties.
    /// Will also keep track of the last frame states for comparison if a key was just pressed this frame, but not already in the last frame.
    /// </summary>
    public static class KeyboardHelper
    {
        /// <summary>
        /// Keyboard state, set every frame in the Update method.  Note: KeyboardState is a class and not a struct, we have to initialize it here, else we might run into trouble when
        /// accessing any keyboardState data before BaseGame.Update() is called. We can also NOT use the last state because everytime we call Keyboard.GetState() the old state is 
        /// useless (see XNA help for more information, section Input). We store our own array of keys from the last frame for comparing stuff.
        /// </summary>
        private static KeyboardState _keyboardState = Microsoft.Xna.Framework.Input.Keyboard.GetState();
        private static KeyboardState _keyboardStateLastFrame;

        private static readonly Keys[] ValidKeys = GetAllValidKeys();

        //A list of pressed keys per frame - use this as a replacement for KeyboardState.GetPressedKeys() to stop garbage being generated each frame.
        private static readonly List<Keys> PressedKeys = new List<Keys>(16);

        /// <summary>Returns all entries in the XNA Keys enumeration</summary>
        /// <returns>All entries in the keys enumeration</returns>
        private static Keys[] GetAllValidKeys() 
        {
            #if XBOX360 || WINDOWS_PHONE
                FieldInfo[] fieldInfos = typeof(Keys).GetFields(BindingFlags.Public | BindingFlags.Static);

                //Create an array to hold the enumeration values and copy them over from the fields we just retrieved
                var values = new Keys[fieldInfos.Length];
                for (int index = 0; index < fieldInfos.Length; ++index) 
                    values[index] = (Keys)fieldInfos[index].GetValue(null);
          
                return values;
            #else
                return (Keys[])Enum.GetValues(typeof(Keys));
            #endif
        }

        /// <summary>
        /// Update, called from BaseGame.Update().
        /// Will catch all new states for keyboard, mouse and the gamepad.
        /// </summary>
        internal static void Update()
        {
            // Handle keyboard input
            _keyboardStateLastFrame = _keyboardState;
            _keyboardState = Microsoft.Xna.Framework.Input.Keyboard.GetState();
        }

        /// <summary>
        /// Keyboard
        /// </summary>
        /// <returns>Keyboard state</returns>
        public static KeyboardState Keyboard
        {
            get { return _keyboardState; }
        }
        
        public static IEnumerable<Keys> GetPressedKeys()
        {
            PressedKeys.Clear();

            // Check all keys for changes between the two provided states
            foreach (var key in ValidKeys)
            {
                var previousState = _keyboardStateLastFrame[key];
                var currentState = _keyboardState[key];

                // If this key changed state, report it
                if (previousState == currentState) 
                    continue;

                if (currentState == KeyState.Down) 
                {
                    PressedKeys.Add(key);
                }
            }

            return PressedKeys;
        }

        public static bool IsKeyPressed(Keys key)
        {
            return _keyboardState.IsKeyDown(key);
        }

        /// <summary>
        /// Keyboard key just pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsKeyJustPressed(Keys key)
        {
            return _keyboardState.IsKeyDown(key) && _keyboardStateLastFrame.IsKeyUp(key);
        }

        /// <summary>
        /// Keyboard key just pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsKeyJustReleased(Keys key)
        {
            return _keyboardState.IsKeyUp(key) && _keyboardStateLastFrame.IsKeyDown(key);
        }

        /// <summary>
        /// Keyboard space just pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsSpaceJustPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Space) && _keyboardStateLastFrame.IsKeyUp(Keys.Space); }
        }

        /// <summary>
        /// Keyboard F1 just pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsF1JustPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.F1) && _keyboardStateLastFrame.IsKeyUp(Keys.F1); }
        }

        /// <summary>
        /// Keyboard escape just pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsEscapeJustPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Escape) && _keyboardStateLastFrame.IsKeyUp(Keys.Escape); }
        }

        /// <summary>
        /// Keyboard left just pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsLeftJustPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Left) && _keyboardStateLastFrame.IsKeyUp(Keys.Left); }
        }

        /// <summary>
        /// Keyboard right just pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsRightJustPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Right) && _keyboardStateLastFrame.IsKeyUp(Keys.Right); }
        }

        /// <summary>
        /// Keyboard up just pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsUpJustPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Up) && _keyboardStateLastFrame.IsKeyUp(Keys.Up); } 
        }

        /// <summary>
        /// Keyboard down just pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsDownJustPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Down) && _keyboardStateLastFrame.IsKeyUp(Keys.Down); }
        }

        /// <summary>
        /// Keyboard left pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsLeftPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Left); }
        }

        /// <summary>
        /// Keyboard right pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsRightPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Right); }
        }

        /// <summary>
        /// Keyboard up pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsUpPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Up); }
        }

        /// <summary>
        /// Keyboard down pressed
        /// </summary>
        /// <returns>Bool</returns>
        public static bool IsDownPressed
        {
            get { return _keyboardState.IsKeyDown(Keys.Down); }
        }
    }
}
