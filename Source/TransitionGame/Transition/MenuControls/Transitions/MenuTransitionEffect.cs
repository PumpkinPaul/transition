namespace Transition.MenuControls.Transitions
{
    /// <summary>
    /// A base class for transitioning things.
    /// </summary>
    public abstract class MenuTransitionEffect
    {
        public enum TransitionType
        {
            Relative,
            Absolute
        }

        protected readonly MenuControl MenuControl;

        public string Name { get; set; }
        
        bool _enabled;

        protected readonly int MaxTicks;
        int _ticks = -1;

        protected readonly int MaxDelay;
        int _delayTicks = -1;

        protected readonly TransitionType Type;
        protected readonly InterpolationMethod InterpolationMethod;

        protected MenuTransitionEffect(MenuControl menuControl, string name, int delay, int ticks, InterpolationMethod interpolationMethod, TransitionType type)
        {
            MenuControl = menuControl;
            Name = name;
            MaxDelay = delay;
            MaxTicks = ticks;
            Type = type;
            InterpolationMethod = interpolationMethod;
        }

        public void Start()
        {
            _ticks = 0;
            _delayTicks = 0;
            _enabled = true;

            OnStart();
        }

        protected virtual void OnStart() { }

        public void Stop()
        {
            _enabled = false;

            OnStop();
        }

        protected virtual void OnStop() { }

        public void Update()
        {
            if (_enabled == false)
                return;

            if (MaxDelay != 0)
            {
                if (_delayTicks < MaxDelay)
                {
                    _delayTicks++;
                    return;
                }
            }

            float delta;

            //Is the transition independant? Does it use its own timer?
            if (MaxTicks != 0)
            {
                //We have a timer so test to see if it has fired!
                if (_ticks > MaxTicks)
                {
                    OnCompleted();

                    _enabled = false;
                    return;
                }

                _ticks++;
                delta = (float)_ticks / MaxTicks;
            }
            else
                delta = MenuControl.Overlay.Opacity;
            
            OnUpdate(delta);
        }

        protected virtual void OnUpdate(float delta) { }
        protected virtual void OnCompleted() { }
    }
}
