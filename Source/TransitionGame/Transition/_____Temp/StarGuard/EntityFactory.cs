﻿using System;
using System.Collections.Generic;
using Transition._____Temp.StarGuard.Entities;

namespace Transition._____Temp.StarGuard
{
    public class EntityFactory
    {
        private class Factory
        {
            readonly EntityFactory _parent;
            readonly TransitionGame _game;
            readonly Type _type;
            readonly bool _canGrow;

            readonly Stack<Entity> _availableEntities;
            readonly HashSet<Entity> _usedEntities;

            public string DefaultResourceName { get; private set; }

            public Factory(EntityFactory parent, Type type, TransitionGame game, int initialCapacity, bool canGrow, string defaultResourceName)
            {
                _parent = parent;
                _game = game;
                _type = type;
                _canGrow = canGrow;
                DefaultResourceName = defaultResourceName;

                _availableEntities = new Stack<Entity>(initialCapacity);
                for (var i = 0; i < initialCapacity; ++i)
                    _availableEntities.Push(CreateNewEntity());

                //Allocate capacity for the used entities but make sure we clear the references
                _usedEntities = new HashSet<Entity>(_availableEntities);
                _usedEntities.Clear(); 
            }

            Entity CreateNewEntity()
            {
                #if WINDOWS_STOREAPP
                        return _type.DeclaredConstructors.FirstOrDefault(info => info.IsPublic).Invoke(new object[] { this._game });
                #else
                    var constructor = _type.GetConstructor(new [] { typeof(TransitionGame) });
                    if (constructor == null)
                        throw new InvalidOperationException("The object does not contain an appropriate constructor: e.g. MyClass(TransitionGame game)");
                        
                    return (Entity)constructor.Invoke(new object[] { _game });
                #endif
            }

            public Entity Get()
            {
                if (_parent.IsPoolable == false)
                    return CreateNewEntity();

                if (_availableEntities.Count == 0) 
                {
                    if (_canGrow) 
                        _availableEntities.Push(CreateNewEntity());
                    else 
                        return null;
                }
                                   
                var entity = _availableEntities.Pop();
                
                _usedEntities.Add(entity);

                return entity;
            }

            public void Release(Entity entity)
            {
                _usedEntities.Remove(entity);
                _availableEntities.Push(entity);
            }

            public void ReleaseAllEntities()
            {
                foreach (var entity in _usedEntities)
                    _availableEntities.Push(entity);
                
                _usedEntities.Clear();
            }
        }

        static EntityFactory _instance;

        readonly TransitionGame _game;

        readonly Dictionary<Type, Factory> _factories = new Dictionary<Type, Factory>();

        public bool IsPoolable { get; set; } 

        public EntityFactory(TransitionGame game)
        {
            _game = game;
            IsPoolable = true;
            _instance = this;
        }

        public void RegisterFactory(Type type, int initialCapacity, bool canGrow = true) 
        {
            _factories.Add(type, new Factory(this, type, _game, initialCapacity, canGrow, type.Name));
        }

        public Entity Create(Type type, string key = null)
        {
            var instance = (Entity)GetEntity(type);

            instance?.Initialise(key ?? _factories[type].DefaultResourceName);

            return instance;
        }

        public T Create<T>(string key = null) where T : Entity
        {
            var type = typeof(T);
            var instance = (T)GetEntity(type);

            instance?.Initialise(key ?? _factories[type].DefaultResourceName);

            return instance;
        }

        public void Release(Entity entity)
        {
            var type = entity.GetType();
            _factories[type].Release(entity);
        }

        public static object GetEntity(Type type)
        {
            if (_instance._factories.ContainsKey(type) == false)
                throw new Exception("Unhandled Entity class passed in EntityFactory.Create: " + type);

            return _instance._factories[type].Get();
        }

        public void ReleaseAllEntities()
        {
            foreach(var factory in _factories.Values)
                factory.ReleaseAllEntities();
        }
    }
}