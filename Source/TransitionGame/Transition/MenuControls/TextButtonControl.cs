using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represent a text button control.
    /// </summary>
    public class TextButtonControl : MenuControl
    {			
        /// <summary>Where the text is rendered in relation to the control's position.</summary>
        public Alignment Alignment;

		/// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;
		
		/// <summary>The size of the font.</summary>
        public Vector2 FontSize;

        public Vector2 MinimumSize;
				
		/// <summary>The text to render.</summary>
        public string Text;		

        public bool CanIncreaseDecrease;

        public override bool AutoTabIndex { get { return true; } }

        /// <summary>
        /// Initialises a new instance of a TextButtonControl.
        /// </summary>
		/// <param name="game">A reference to the game.</param>
        public TextButtonControl(BaseGame game) : base(game)
		{
            Font = Theme.Font;
            FontSize = Theme.FontSize * 2.0f;
            Alignment = Alignment.Centre;
		}

        public override void Load(XElement element)
        {
            base.Load(element);
            
            Alignment = element.GetAttributeEnum("alignment", Alignment);
            Font = ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            Text = element.GetAttributeString("text");
        }
		
        public override Rect Bounds
		{
			get 
            {
                var bounds = Font.GetBounds(Position, FontSize, Text, Alignment);

                if (MinimumSize == Vector2.Zero) 
                    return bounds; 
                
                if (bounds.Width < MinimumSize.X)
                {
                    switch (Alignment)
                    {
                        case Alignment.TopLeft:
                        case Alignment.CentreLeft:
                        case Alignment.BottomLeft:
                            bounds.Right = bounds.Left + MinimumSize.X;
                            break;

                        case Alignment.TopCentre:
                        case Alignment.Centre:
                        case Alignment.BottomCentre:
                            var halfDx = (MinimumSize.X - bounds.Width) / 2.0f;
                            bounds.Left -= halfDx;
                            bounds.Right += halfDx;
                            break;

                        case Alignment.TopRight:
                        case Alignment.CentreRight:
                        case Alignment.BottomRight:
                            bounds.Left = bounds.Right - MinimumSize.X;
                            break;
                    }   
                }

                if (bounds.Height < MinimumSize.Y)
                {
                    switch (Alignment)
                    {
                        case Alignment.TopLeft:
                        case Alignment.TopCentre:
                        case Alignment.TopRight:
                            bounds.Top = bounds.Bottom + MinimumSize.Y;
                            break;

                        case Alignment.CentreLeft:
                        case Alignment.Centre:
                        case Alignment.CentreRight:
                            var halfDy = (MinimumSize.Y - bounds.Height) / 2.0f;
                            bounds.Top += halfDy;
                            bounds.Bottom -= halfDy;
                            break;

                        case Alignment.BottomLeft:
                        case Alignment.BottomCentre:
                        case Alignment.BottomRight:
                            bounds.Bottom = bounds.Top - MinimumSize.Y;
                            break;
                    }
                }

                return bounds;
            }
		}

        /// <summary>
        /// Occurs when a control should increase its value.
        /// </summary>
        /// <returns>.</returns>
        protected override bool OnIncreaseValue()
        {
            return CanIncreaseDecrease;
        }

        /// <summary>
        /// Occurs when a control should decrease its value.
        /// </summary>
        /// <returns>.</returns>
        protected override bool OnDecreaseValue()
        {
            return CanIncreaseDecrease;
        }
		
		/// <summary>
        /// Allows a derived control to draw itself.
		/// </summary>
		protected override void OnDraw()
		{
            var tint = EffectiveTint;

            Game.Renderer.DrawString(Font, Position, FontSize, Text, Alignment, tint, RenderStyle, Layer);
		}
    }
}
