//using System;

//#if WINDOWS
//using System.Net;
//using System.IO;
//using System.Text;
//#endif

//using Microsoft.Xna.Framework;

//namespace Transition
//{
//#if WINDOWS
//    /// <summary>
//    /// This class stores the State of the request.
//    /// </summary>
//    public class RequestState
//    {
//        private const int BufferSize = 1024;
     
//        public StringBuilder requestData;
//        public byte[] BufferRead;
//        public HttpWebRequest request;
//        public HttpWebResponse response;
//        public Stream streamResponse;
        
//        public RequestState()
//        {
//            BufferRead = new byte[RequestState.BufferSize];
//            requestData = new StringBuilder("");
//            request = null;
//            streamResponse = null;
//        }
//    }
//#endif

//    /// <summary>
//    /// Manages web requests
//    /// </summary>
//    public class WebManager
//    {
//        #if WINDOWS
//            private HttpWebRequest _webRequest;

//            ///<summary>A flag that indicates whether the web manager is busy.<summary>
//            private bool _busy = false;

//            private int _timer = 0;
//        #endif
        
//        private readonly TransitionGame _game;
//        private readonly int _timeout = 7500;
//        private string _lastOnlineResponse = string.Empty;
//        private const int BufferSize = 1024;
        
//        /// <summary>
//        /// Creates a new instance of a WebManager.
//        /// </summary>
//        public WebManager(BaseGame game)
//        {
//            LastResult = false;
//            _game = game;
//            _timeout = _game.GameOptions.OnlineTimeout;
//        }
		
//        /// <summary>
//        /// Gets whether the web manager is busy (commnicating with the web).
//        /// </summary>
//        public bool Busy
//        {
//            get
//            {
//                #if WINDOWS
//                    return _busy;
//                #else
//                    return false;
//                #endif
//            }
//        }

//        /// <summary>
//        /// Gets the result of the last web action.
//        /// </summary>
//        public bool LastResult { get; private set; }

//        /// <summary>
//        /// Gets the response of the last web action.
//        /// </summary>
//        public string LastResponse
//        {
//            get { return _lastOnlineResponse; }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public void Update()
//        {
//            #if WINDOWS
//                if (Busy)
//                {
//                    //Update the timer so we can handle timeout
//                    _timer += _game.TargetElapsedTime.Milliseconds;

//                    if (_timer >= _timeout)
//                    {
//                        _webRequest.Abort();
//                        _timer = 0;
//                        _lastOnlineResponse = "the request timed out";
//                        LastResult = false;
//                        EndRequest();
//                    }
//                }
//            #endif
//        }

//        /// <summary>
//        /// Creates an online profile.
//        /// </summary>
//        /// <param name="profile">The profile to create.</param>
//        /// <remarks>Writes the data to an online database.</remarks>
//        public bool CreateOnlineProfile(PlayerProfile profile)
//        {
//            #if WINDOWS
//                BeginRequest();

//                var url = new StringBuilder();
//                url.Append("http://www.pumpkin-games.net/addprofile.php");
//                url.Append("?guid=");
//                url.Append(profile.Guid);
//                url.Append("&name=");
//                url.Append(profile.Name);

//                _webRequest = (HttpWebRequest)WebRequest.Create(url.ToString());

//                //Create an instance of the RequestState and assign the previous myHttpWebRequest object to it's request field.  
//                var requestState = new RequestState();
//                requestState.request = _webRequest;

//                // Start the asynchronous request.
//                var result = _webRequest.BeginGetResponse(RespCallback, requestState);
//            #endif

//            return true;
//        }

//        /// <summary>
//        /// Writes the score to the online high score table.
//        /// </summary>
//        /// <param name="scoreItem">Score item containing the data to write.</param>
//        /// <remarks>Writes the data to an online database.</remarks>
//        public bool AddOnlineScores(ScoreItem scoreItem)
//        {
//            #if WINDOWS
//                return AddOnlineScore(scoreItem.Score, scoreItem.Level, scoreItem.Ticks, scoreItem.Name, scoreItem.Key);
//            #else
//                return true;
//            #endif
//        }
		
//        /// <summary>
//        /// Writes the score to the online high score table.
//        /// </summary>
//        /// <param name="scoreItem">Score item containing the data to write.</param>
//        /// <remarks>Writes the data to an online database.</remarks>
//        public bool AddOnlineScores(ScoreItemTimeAttack scoreItem)
//        {
//            #if WINDOWS
//                return AddOnlineScore(scoreItem.Score, scoreItem.Level, scoreItem.Ticks, scoreItem.Name, scoreItem.Key);
//            #else
//                return true;
//            #endif
//        }

//        #if WINDOWS
//            /// <summary>
//            /// Writes the score to the online high score table.
//            /// </summary>
//            /// <remarks>Writes the data to an online database.</remarks>
//            private bool AddOnlineScore(int score, int level, int time, string name, string key)
//            {
//                BeginRequest();

//                var url = new StringBuilder();
//                url.Append("http://www.pumpkin-games.net/addscore.php");
//                url.Append("?game=Transition");
//                url.Append("&game_mode=");
//                url.Append((int)_game.GameMode);
//                url.Append("&score=");
//                url.Append(score);
//                url.Append("&level=");
//                url.Append(level);
//                url.Append("&difficulty=");
//                url.Append((int)_game.ActivePlayer.Difficulty);
//                url.Append("&name=");
//                url.Append(name);
//                url.Append("&seconds=");
//                url.Append(time);
//                url.Append("&key=");
//                url.Append(key);
//                url.Append("&score_table=0");
//                url.Append("&mission_pak=0");
//                url.Append("&platform=windows");

//                _webRequest = (HttpWebRequest)WebRequest.Create(url.ToString());

//                //Create an instance of the RequestState and assign the previous myHttpWebRequest object to it's request field.  
//                var requestState = new RequestState();
//                requestState.request = _webRequest;

//                // Start the asynchronous request.
//                var result = _webRequest.BeginGetResponse(RespCallback, requestState);

//                return true;
//            }

//            /// <summary>
//            /// Gets the online scores table.
//            /// </summary>
//            /// <param name="difficulty"></param>
//            /// <param name="page">The page of data to get.</param>
//            /// <param name="gameMode"></param>
//            public bool GetOnlineScores(GameMode gameMode, Difficulty difficulty, int page)
//            {
//                BeginRequest();

//                var url = new StringBuilder();
//                url.Append("http://www.pumpkin-games.net/hiscores.php");
//                url.Append("?game=Transition");
//                url.Append("&game_mode=");
//                url.Append((int)gameMode);
//                url.Append("&difficulty=");
//                url.Append((int)difficulty);
//                url.Append("&score_table=0");
//                url.Append("&mission_pak=0");
//                url.Append("&page_entries=5");
//                url.Append("&page=");
//                url.Append(page);

//                _webRequest = (HttpWebRequest)WebRequest.Create(url.ToString()); 
            
//                //Create an instance of the RequestState and assign the previous myHttpWebRequest object to it's request field.  
//                var requestState = new RequestState();
//                requestState.request = _webRequest;

//                // Start the asynchronous request.
//                var result = _webRequest.BeginGetResponse(RespCallback, requestState);

//                return true;
//            }

//            /// <summary>
//            /// 
//            /// </summary>
//            /// <param name="asynchronousResult"></param>
//            private void RespCallback(IAsyncResult asynchronousResult)
//            {  
//                // State of request is asynchronous.
//                var myRequestState=(RequestState) asynchronousResult.AsyncState;
//                var  myHttpWebRequest=myRequestState.request;
//                try
//                {
//                    myRequestState.response = (HttpWebResponse)myHttpWebRequest.EndGetResponse(asynchronousResult);

//                    // Read the response into a Stream object.
//                    var responseStream = myRequestState.response.GetResponseStream();
//                    myRequestState.streamResponse = responseStream;

//                    // Begin the Reading of the contents of the HTML page and print it to the console.
//                    var asynchronousInputRead = responseStream.BeginRead(myRequestState.BufferRead, 0, WebManager.BufferSize, ReadCallBack, myRequestState);

//                }
//                catch (Exception ex)
//                {     
//                    LastResult = false;
//                    _lastOnlineResponse = ex.Message;
           
//                    EndRequest();
//                }
//            }
    
//            /// <summary>
//            /// 
//            /// </summary>
//            /// <param name="asyncResult"></param>
//            private void ReadCallBack(IAsyncResult asyncResult)
//            {
//                RequestState myRequestState = null;
//                Stream responseStream = null;

//                try
//                {
//                    myRequestState = (RequestState)asyncResult.AsyncState;
//                    responseStream = myRequestState.streamResponse;
//                    var read = responseStream.EndRead(asyncResult);
//                    // Read the HTML page and then print it to the console.
//                    if (read > 0)
//                    {
//                        myRequestState.requestData.Append(Encoding.ASCII.GetString(myRequestState.BufferRead, 0, read));
//                        var asynchronousResult = responseStream.BeginRead(myRequestState.BufferRead, 0, WebManager.BufferSize, ReadCallBack, myRequestState);
//                    }
//                    else
//                    {
//                        var result = myRequestState.requestData.ToString();
//                        if (result == string.Empty)
//                        {
//                            LastResult = true;
//                            _lastOnlineResponse = result;
//                        }
//                        else if (result.Substring(0, 3) == "OK:")
//                        {
//                            LastResult = true;
//                            _lastOnlineResponse = result.Substring(3);
//                        }
//                        else
//                            _lastOnlineResponse = result;

//                        responseStream.Close();
//                        myRequestState.response.Close();

//                        EndRequest();

//                    }
//                }
//                catch (Exception ex)
//                {
//                    _lastOnlineResponse = ex.Message;

//                    responseStream.Close();
//                    myRequestState.response.Close();

//                    EndRequest();
//                }
//            }

//            /// <summary>
//            /// Deletes an online profile.
//            /// </summary>
//            /// <param name="profile">The profile to delete.</param>
//            /// <remarks>Writes the data to an online database.</remarks>
//            public bool DeleteOnlineProfile(PlayerProfile profile)
//            {
//                return true;
//            }

//            /// <summary>
//            /// Initialises a new web requests.
//            /// </summary>
//            private void BeginRequest()
//            {
//                _game.MenuManager.Busy("Accessing internet.");
//                _lastOnlineResponse = string.Empty;
//                LastResult = false;
//                _timer = 0;
//                _busy = true;
//            }

//                   /// <summary>
//            /// Ends a web request.
//            /// </summary>
//            private void EndRequest()
//            {
//                _game.MenuManager.Ready();
//                _busy = false;
//            }
//        #endif
//    }
//}
