using System.Collections.Generic;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
    /// <summary>
    /// Configuration data for enemy bullets.
    /// </summary>
    public class EnemyBulletBlueprint
    {
        public string Name;
        public bool Visible;
        public bool Explode;

		/// <summary>A lit of spawn sounds.</summary>
        public readonly List<string> Sounds = new List<string>();
    }
}
