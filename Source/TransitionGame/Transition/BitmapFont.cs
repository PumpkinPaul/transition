using System;
using System.Text;
using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Bitmap font.
    /// </summary>
    public class BitmapFont
    {
        public float Spacing = 1.0f / 32.0f;

        readonly bool _fixedWidth;
        readonly int _textureId = -1;
        public readonly float LineHeight = 1.0f;
        public readonly int MinChar;
        public readonly int MaxChar;
        float _heightScale = 1.0f;

        /// <summary>.</summary>
        BitmapCharacterMetrics[] _charMetrics;

        Vector2 _tiles;
        
        public BitmapFont(int textureId, Vector2 tiles, bool fixedWidth, float heightScale, BitmapCharacterMetrics[] charMetrics, float lineHeight, int minChar, int maxChar)
        {
            _tiles = tiles;
            _fixedWidth = fixedWidth;
            _textureId = textureId;
            LineHeight = lineHeight;
            MinChar = minChar;
            MaxChar = maxChar;
            _heightScale = heightScale;

            GenerateCharacterMetrics(charMetrics);
        }

        /// <summary>
        /// 
        /// </summary>
        public BitmapCharacterMetrics[] CharMetrics
        {
            get { return _charMetrics; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TextureId
        {
            get { return _textureId; }
        }

        /// <summary>
        /// .
        /// </summary>
        private void GenerateCharacterMetrics(BitmapCharacterMetrics[] charMetrics)
        {   
            _charMetrics = charMetrics;
                       
            if (_tiles == Vector2.Zero)
                return;

            var chars = (int)(_tiles.X * _tiles.Y);

            var texX = 1.0f / _tiles.X;
            var texY = 1.0f / _tiles.Y;

            for (var loop = 0; loop < chars; ++loop)
            {
                var cx = (loop % _tiles.X) / _tiles.X; // Holds our X character coord
                var cy = (int)(loop / _tiles.X) / _tiles.Y; // Holds our Y character coord

                _charMetrics[loop].Rect.Left = cx;
                _charMetrics[loop].Rect.Top = cy;
                _charMetrics[loop].Rect.Bottom = cy + texY;
                _charMetrics[loop].Rect.Right = cx + texX;
            }
        }

        /// <summary>
        /// Gets a rectangle bounding the text.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <param name="stringBuilder"></param>
        /// <param name="alignment"></param>
        /// <returns></returns>
        public Rect GetBounds(Vector2 position, Vector2 size, StringBuilder stringBuilder, Alignment alignment)
        {
            if (stringBuilder == null)
                return Rect.TextureRect;

            if (stringBuilder.Length == 0)
                return Rect.TextureRect;

            var textLength = GetTextLength(stringBuilder, size.X);

            return GetBoundsInternal(position, size, textLength, alignment);
        }

        /// <summary>
        /// Gets a rectangle bounding the text.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <param name="text"></param>
        /// <param name="alignment"></param>
        /// <returns></returns>
        public Rect GetBounds(Vector2 position, Vector2 size, string text, Alignment alignment)
        {
            if (string.IsNullOrEmpty(text))
                return Rect.TextureRect;

            var textLength = GetTextLength(text, size.X);

            return GetBoundsInternal(position, size, textLength, alignment);
        }

        private Rect GetBoundsInternal(Vector2 position, Vector2 size, float textLength, Alignment alignment)
        {
            //Calculate the bounding rect for the text based on the length, its position and the alignment.
            var x = position.X;
            var y = position.Y;

            size.Y *= _heightScale;

            switch (alignment)
            {
                case Alignment.TopLeft:
                    return new Rect(y, x, y - size.Y, x + textLength);

                case Alignment.TopCentre:
                    return new Rect(y, x - (textLength / 2.0f), y - size.Y, x + (textLength / 2.0f));

                case Alignment.TopRight:
                    return new Rect(y, x - textLength, y - size.Y, x);

                case Alignment.Centre:
                    return new Rect(y + (size.Y / 2.0f), x - (textLength / 2.0f), y - (size.Y / 2.0f), x + (textLength / 2.0f));

                case Alignment.CentreRight:
                    return new Rect(y + (size.Y / 2.0f), x - textLength, y - (size.Y / 2.0f), x);

                case Alignment.BottomLeft:
                    return new Rect(y + size.Y, x, y, x + textLength);

                case Alignment.BottomCentre:
                    return new Rect(y + size.Y, x - (textLength / 2.0f), y, x + (textLength / 2.0f));

                case Alignment.BottomRight:
                    return new Rect(y + size.Y, x - textLength, y, x);

                //case Alignment.CentreLeft:
                default:
                    return new Rect(y + (size.Y / 2.0f), x, y - (size.Y / 2.0f), x + textLength);
            }
        }

        /// <summary>
        /// Gets the length of the text at the specified size.
        /// </summary>
        /// <param name="text">The text to measure.</param>
        /// <param name="sizeX">The size of the text.</param>
        /// <param name="chars"></param>
        /// <returns>The length of the text.</returns>
        public float GetTextLength(string text, float sizeX, int chars)
        {
            var length = 0.0f;

            chars = Math.Min(text.Length, chars);

            if (_fixedWidth)
                length = (chars + (chars * Spacing));
            else
            {
                for (var index = 0; index < chars; index++)
                {
                    var t = text[index];
                    var charId = ConvertCharToIndex(t);
                   
                    length += (_charMetrics[charId]).Width + Spacing;
                }
            }

            if (length > 0.0f)
                length *= sizeX;

            return length;
        }

        /// <summary>
        /// Gets the length of the text at the specified size.
        /// </summary>
        /// <param name="text">The text to measure.</param>
        /// <param name="sizeX">The size of the text.</param>
        /// <returns>The length of the text.</returns>
        public float GetTextLength(string text, float sizeX)
        {
            var length = 0.0f;

            if (_fixedWidth)
                length = text.Length;
            else
            {
                foreach (var t in text)
                {
                    var charId = ConvertCharToIndex(t);
                    
                    length += (_charMetrics[charId]).Width;
                }
            }

            if (length > 0.0f)
                length = (length + ((text.Length - 1)* Spacing)) * sizeX;

            return length;
        }

        /// <summary>
        /// Gets the length of the text at the specified size.
        /// </summary>
        /// <param name="text">The text to measure.</param>
        /// <param name="sizeX">The size of the text.</param>
        /// <returns>The length of the text.</returns>
        public float GetTextLength(StringBuilder text, float sizeX)
        {
            var length = 0.0f;

            if (_fixedWidth)
                length = text.Length;
            else
            {
                for (var i = 0; i < text.Length; ++i)
                {
                    var charId = ConvertCharToIndex(text[i]);
                    
                    length += (_charMetrics[charId]).Width;
                }
            }

            if (length > 0.0f)
                length = (length + ((text.Length - 1) * Spacing)) * sizeX;

            return length;
        }

        public int ConvertCharToIndex(char ch)
        {
            if (ch < MinChar || ch > MaxChar)
                ch = (char)MinChar;

            return ch - MinChar;
        }
    }
}
