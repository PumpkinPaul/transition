using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.ResourceManagers;

namespace Transition.Rendering
{
    /// <summary>
    /// A style to render 'wobbly sine wave' effects.
    /// </summary>
    public class WobblySineWaveRenderStyle : RenderStyle
    {
        public static WobblySineWaveRenderStyle Instance;

        public override RenderStyleType RenderStyleId => RenderStyleType.WobblySineWave;

        protected override void OnRegister(EffectManager effectManager)
        {
            Instance = this;
            Effect = effectManager.GetEffect("WobblySineWave");
        }

        public override void Setup(GraphicsDevice graphicsDevice)
        {
            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            //var projection = Matrix.CreateOrthographicOffCenter(0, graphicsDevice.Viewport.Width, graphicsDevice.Viewport.Height, 0, 0, 1);
            //var halfPixelOffset = Matrix.CreateTranslation(-0.5f, -0.5f, 0);
            //Effect.Parameters["MatrixTransform"].SetValue(halfPixelOffset * projection);
            Effect.Parameters["time"].SetValue(TransitionGame.Instance.GameTimeInSeconds);
        }
    }
}
