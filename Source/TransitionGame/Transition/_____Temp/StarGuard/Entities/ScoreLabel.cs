﻿using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Rendering;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// In world notification of score bonuses
    /// </summary>
    [ProtoContract]
    public class ScoreLabel : Label
    {
        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        public int Score;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public ScoreLabel(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            Score = 0;

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization()
        {
            InitialiseNonSerialized();
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            base.OnUpdate(wasRewinding);

            Velocity.Y -= 0.005f;
        }
        
        protected override void OnDraw()
        {
            base.OnDraw();

            Game.Renderer.DrawNumber(Theme.Current.GetFont(Theme.Fonts.Default), Position, Vector2.One, Score, Alignment.Centre, Color.White, AlphaBlendingRenderStyle.Instance);
        }
    }
}