namespace Transition.Cameras
{
    /// <summary>
    /// Data for a camera shake.
    /// </summary>
    public class CameraShake
    {
        #region Fields
		public float Strength;
		public int Duration;
		public float StrengthDelta;
        #endregion

        #region Constructors
		/// <summary>
		/// Creates a new instance of a CameraShake.
		/// </summary>
		public CameraShake(float strength, int duration, float strengthDelta)
		{
			this.Strength = strength;
			this.Duration = duration;
            this.StrengthDelta = strengthDelta;
		}
        #endregion

        #region Properties
        #endregion

        #region Base Overrides
        #endregion

        #region Methods
        #endregion

        #region State Machine
        #endregion
    }
}
