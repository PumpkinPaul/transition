﻿using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Nuclex.Input;
using ProtoBuf;
using Transition.Input;
using Transition._____Temp.StarGuard.EntityBlueprints;

namespace Transition._____Temp.StarGuard.Entities
{
    [ProtoContract]
    public class Player : Entity
    {
        public enum Phase
        {
            Normal,
            Flag
        }

        const float MaxSpeedX = 0.12f;
        const float MaxSpeedY = 0.35f;

        const float GroundFrictionDamping = 0.5f;
        const float AirFrictionDamping = 0.5f;

        const float Acceleration = MaxSpeedX;
        const float JumpSpeed = 0.4f;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1, AsReference = true)]
        Weapon _weapon;

        [ProtoMember(2)]
        Vector2 _checkpoint;

        [ProtoMember(3)]
        Phase _phase;

        [ProtoMember(4)]
        ShipBlueprint _shipBlueprint;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public Player(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.Player;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _shipBlueprint = Game.EntityBlueprints.ShipBlueprint(name);

            _weapon = null;
            _checkpoint = Vector2.Zero;
            _phase = Phase.Normal;

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization()
        {
            InitialiseNonSerialized();
        }   

        protected override void OnAddedToWorld(World world) 
        { 
            //This change here is purely to enable testing
            var gameMap = GameServices.Instance.Get<GameMap>();
            _checkpoint = gameMap.StartPosition;

            var entityManager = GameServices.Instance.Get<EntityFactory>();
            _weapon = (Weapon)entityManager.Create(typeof(Weapon));

            _weapon.Position = Position;
            world.Add(_weapon);
        }
        
        public void SetPhaseNormal()
        {
            _phase = Phase.Normal;
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            switch (_phase)
            {
                case Phase.Normal:
                    UpdatePhaseNormal();
                    break;

                case Phase.Flag:
                    break;
            }
        }

        void UpdatePhaseNormal()
        {
            var gameInput = Game.ActivePlayer.GameInput;

            if (gameInput.Left)
            {
                if (gameInput.LeftJustPressed)
                    Sprites[0].SetAppearance(Game.AnimationManager.Get(_shipBlueprint.WalkLeftAnimation));

                Velocity.X -= Acceleration;
                Direction = MathHelper.Pi;
            }
            else if (gameInput.Right)
            {
                if (gameInput.RightJustPressed)
                    Sprites[0].SetAppearance(Game.AnimationManager.Get(_shipBlueprint.WalkRightAnimation));

                Velocity.X += Acceleration;
                Direction = 0.0f;
            }
            else
            {
                if (OnGround)
                    Velocity.X *= GroundFrictionDamping;
                else
                    Velocity.X *= AirFrictionDamping;
            }

            Velocity.Y += TestPhaseStarGuard.Gravity;

            if (OnGround && gameInput.JumpJustPressed)
                Velocity.Y += JumpSpeed;

            Velocity.X = MathHelper.Clamp(Velocity.X, -MaxSpeedX, MaxSpeedX);
            Velocity.Y = MathHelper.Clamp(Velocity.Y, -MaxSpeedY, MaxSpeedY);

            //TODO: StarGuard - nested Entitys? / Owners?
            _weapon.Position = Position - new Vector2(0, 0.125f);
            _weapon.Direction = Direction;

            if (gameInput.Fire)
            {
                var fired = _weapon.Fire();
                if (fired)
                    Sound.Play(Game, "Shoot", Position);
            }

            if (Game.GameMap.CollidesWithHazard(this))
                CollisionWithNastie();

            Game.GameMap.HandleCollisions(this);

            //Determine the correct animation based on movement speed and direction facing
            if (IsFacingRight)
            {
                if (IsMovingLeft)
                {
                    Sprites[0].SetAppearance(Game.AnimationManager.Get(_shipBlueprint.WalkLeftAnimation));
                    IsFacingRight = false;
                }
                else if (IsMovingRight == false)
                    Sprites[0].SetAppearance(Game.AnimationManager.Get(_shipBlueprint.FaceRightAnimation));
            }
            else
            {
                if (IsMovingRight)
                {
                    Sprites[0].SetAppearance(Game.AnimationManager.Get(_shipBlueprint.WalkRightAnimation));
                    IsFacingRight = true;
                }
                else if (IsMovingLeft == false)
                    Sprites[0].SetAppearance(Game.AnimationManager.Get(_shipBlueprint.FaceLeftAnimation));
            }
        }

        protected override void OnCollisionWithEntity(Entity collider, float time)
        {
            switch (collider.Classification)
            {
                case EntityClassification.Enemy:
                case EntityClassification.EnemyBullet:
                    CollisionWithNastie();
                    break;

                case EntityClassification.Flag:
                    var bottom = collider.Position.Y + collider.CollisionBounds.Bottom;
                    var checkpoint = new Vector2(collider.Position.X, bottom - CollisionBounds.Y);

                    if (_checkpoint != checkpoint)
                    {
                        _checkpoint = checkpoint;
                        var sfx = Game.EntityFactory.Create<Sound>();
                        if (sfx != null)
                        {
                            sfx.Position = Position;
                            sfx.Name = "Checkpoint";
                            sfx.Play();
                            Game.World.Add(sfx);
                        }
                    }
                    break;

                case EntityClassification.DestructibleBlock:
                    var colisionEdge = Position.X > collider.Position.X ? CollisionEdge.Left : CollisionEdge.Right;
                    if (colisionEdge == CollisionEdge.Left && Velocity.X < 0.0f || colisionEdge == CollisionEdge.Right && Velocity.X > 0.0f)
                    {
                        Velocity.X *= time;
                        Position.X += Velocity.X;
                        Velocity.X = 0.0f;// Vector2.Zero;
                    }
                    break;
            }
        }

        void CollisionWithNastie()
        {
            Position = _checkpoint;
            Velocity = Vector2.Zero;
            Game.World.Get<CameraProxy>(EntityKeys.Camera).MoveTo(Position, 30);
            _phase = Phase.Flag;

            Sound.Play(Game, "PlayerHit", Position);

            //Update the stats monkey - are these Checkpoint things?
            TransitionGame.Instance.GlobalStats.PlayerDied();
        }
    }
}