using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a label control.
    /// </summary>
    public class LineControl : MenuControl
    {
        public Vector2 Start;
        public Vector2 End;
        public float Thickness = 0.0175f;
        
        /// <summary>
        /// Initialises a new instance of a LineControl control.
		/// </summary>
		/// <param name="game">A reference to the game.</param>
        public LineControl(BaseGame game) : base(game) { }

        public override void Load(XElement element)
        {
            base.Load(element);
            
            Start = element.GetAttributeVector2("start");
            End = element.GetAttributeVector2("end");
            Thickness = element.GetAttributeSingle("thickness", 0.0175f);
        }

        public override Rect Bounds
		{
            get { return new Rect(MathHelper.Max(Start.Y, End.Y), MathHelper.Min(Start.X, End.X), MathHelper.Min(Start.Y, End.Y), MathHelper.Max(Start.X, End.X)); }
		}

        /// <summary>
        /// Allows a derived control to draw itself.
		/// </summary>
		protected override void OnDraw()
		{
            var tint = Tint;
            tint *= Overlay.Opacity;

            Game.Renderer.DrawLine(Start + Position, End + Position, tint, RenderStyle, Thickness, Layer);
		}
    }
}
