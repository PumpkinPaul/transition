using Microsoft.Xna.Framework;

namespace Transition
{
    public class Random
    { 
        private const int DefaultSeed = 12345;

        /// <summary>A reference to the current random number generator value</summary>
        private int _value = DefaultSeed & 0xFFFF;

        /// <summary>
        /// Sets the seed to use for the current random number generation mode.
        /// </summary>
        public int Seed
        {
            set { _value = value & 0xFFFF; }
        }

        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public void Reset()
        {
            _value = DefaultSeed & 0xFFFF;
        }

        private void Next()
        {
            _value = ((_value * 43) + 1509) & 0xFFFF;
        }

        public float GetFloat()
        {
            Next();
            return (_value) / 65535.0f;
        }

        public float GetFloat(float upperBound)
        {
            Next();
            return upperBound * (_value) / 65535.0f;
        }

        public float GetFloat(float lowerBound, float upperBound)
        {
            return lowerBound + GetFloat(upperBound - lowerBound);
        }

        /// <summary>
        /// A 32-bit signed integer greater than or equal to zero and less than MaxValue.
        /// </summary>	
        public int GetInt()
        {
            Next();
            return int.MaxValue * _value / 65535;
        }

        public int GetInt(int upperBound)
        {
            Next();
            return upperBound * _value / 65535;
        }

        public int GetInt(int lowerBound, int upperBound)
        {
            return lowerBound + GetInt(upperBound - lowerBound + 1);
        }

        public bool GetBool()
        {
            return GetFloat(1.0f) > 0.5f;
        }

        public Vector2 GetVector(Vector2 v1, Vector2 v2)
        {
            return new Vector2(GetFloat(v1.X, v2.X), GetFloat(v1.Y, v2.Y));
        }

        public Vector2 GetVector(float x1, float x2, float y1, float y2)
        {
            return new Vector2(GetFloat(x1, x2), GetFloat(y1, y2));
        }

        public Color GetColor(Color lower, Color upper)
        {
            return new Color(GetFloat(lower.R, upper.R), GetFloat(lower.G, upper.G), GetFloat(lower.B, upper.B), GetFloat(lower.A, upper.A));
        }

        public Vector2 GetVector2(float lower, float upper)
        {
            return new Vector2(GetFloat(lower, upper), GetFloat(lower, upper));
        }

        public Vector2 GetVector2(float lowerX, float upperX, float lowerY, float upperY)
        {
            return new Vector2(GetFloat(lowerX, upperX), GetFloat(lowerY, upperY));
        }

        public Vector4 GetVector4(float lowerX, float upperX, float lowerY, float upperY, float lowerZ, float upperZ, float lowerW, float upperW)
        {
            return new Vector4(GetFloat(lowerX, upperX), GetFloat(lowerY, upperY), GetFloat(lowerZ, upperZ), GetFloat(lowerW, upperW));
        }

        public Vector4 GetVector4(Vector4 lower, Vector4 upper)
        {
            return new Vector4(GetFloat(lower.X, upper.X), GetFloat(lower.Y, upper.Y), GetFloat(lower.Z, upper.Z), GetFloat(lower.W, upper.W));
        }

        public Vector4 GetVector4(ref Vector4 lower, ref Vector4 upper)
        {
            return new Vector4(GetFloat(lower.X, upper.X), GetFloat(lower.Y, upper.Y), GetFloat(lower.Z, upper.Z), GetFloat(lower.W, upper.W));
        }
    }
}

