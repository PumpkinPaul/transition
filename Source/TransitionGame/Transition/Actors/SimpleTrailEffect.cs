using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Rendering;

namespace Transition.Actors
{
    /// <summary>
    /// Represents a 'simple' trail effect - just a quad
    /// </summary>
    public class SimpleTrailEffect : SpecialEffect
    {
        readonly Geometry<VertexPositionColorTexture> _geometry = new Geometry<VertexPositionColorTexture>();

        Color _headColor;
        Color _tailColor;
        float _headWidth;
        float _tailWidth;
        float _headHalfWidth;
        float _tailHalfWidth;
        float _maxLength;

        public Vector2 TailPosition;

        /// <summary>
        /// Creates a new instance of a GodRaysEffect.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public SimpleTrailEffect(TransitionGame game) : base(game)
        {
            _geometry.RenderStyle = TransparentRenderStyle.Instance;
            _geometry.TextureId = -1;
            _geometry.Indices = Renderer.QuadIndices;
        }

        protected override void OnInitialise(string name) 
        {
            base.OnInitialise(name);

            //properties are initialised in Setup() so no need to do anything here (yet?)
        }

        public void Setup(Color headColor, Color tailColor, float headWidth, float tailWidth, float maxLength)
        {
            _headColor = headColor;
            _tailColor = tailColor;
            _headWidth = headWidth;
            _tailWidth = tailWidth;
            _maxLength = maxLength;

            _headHalfWidth = _headWidth / 2.0f;
            _tailHalfWidth = _tailWidth / 2.0f;
        }

        protected override void OnUpdate(GameTime gameTime) 
        {
            if (Owner.Active == false)
            {
                Deactivate();
            }

            var d = Position - TailPosition;
            if (d.Length() > _maxLength)
            {
                d.Normalize();
                d *= _maxLength;
                TailPosition = Position - d;
            }
        }
        
        protected override void OnPostUpdate()
        {   

        }

        protected override void OnDraw(bool isOnscreen) 
        {
            DrawEffectInternal();
        }

        public void DrawEffect() 
        {
            //DrawEffectInternal();
        }

        public void DrawEffectInternal() 
        {
            var normal = Direction + MathHelper.PiOver2;

            var p1 = new Vector3(Position + VectorHelper.Polar(_headHalfWidth, normal), 0.0f);
            var p2 = new Vector3(Position + VectorHelper.Polar(-_headHalfWidth, normal), 0.0f);
            var p3 = new Vector3(TailPosition + VectorHelper.Polar(_tailHalfWidth, normal), 0.0f);
            var p4 = new Vector3(TailPosition + VectorHelper.Polar(-_tailHalfWidth, normal), 0.0f);

            //TODO: Transition - frustum culling
            ////If all of the positions are outside the view then we don;t need to draw the effect
            //if (_game.VisibleWorldBounds.Contains(p1) == false && _game.VisibleWorldBounds.Contains(p2) == false && _game.VisibleWorldBounds.Contains(p3) == false && _game.VisibleWorldBounds.Contains(p4) == false)
            //    return;
            
            _geometry.Layer = Layer;
            _geometry.Vertices.Clear();            

            // The sprites are rendered like so using GL_TRIANGLES
            // 2-----0
            // |\    |
            // |  \  |
            // |    \|
            // 3-----1
     
            _geometry.Vertices.Add(new VertexPositionColorTexture(p1, _headColor, Vector2.Zero));
            _geometry.Vertices.Add(new VertexPositionColorTexture(p2, _headColor, Vector2.Zero));
            _geometry.Vertices.Add(new VertexPositionColorTexture(p3, _tailColor, Vector2.One));
            _geometry.Vertices.Add(new VertexPositionColorTexture(p4, _tailColor, Vector2.One));

            _game.Renderer.DrawGeometry(_geometry);
        }
    }
}
