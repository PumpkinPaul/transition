using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Emits particles from a single point.
    /// </summary>
    public class ParticlePointEmitter : IParticleEmitter
    {
	    /// <summary>
        /// 
        /// </summary>
		/// <param name="data">The data required for configuring the emitter.</param>
        public void SetData(string data)
	    {
		    //nar
	    }
		
        /// <summary>
        /// Gets the position of the next particle to be emitted.
        /// </summary>
        /// <returns>Returns a Vector2 defining the position of the particle.</returns>
        public Vector2 GetPosition()
        {
            return Vector2.Zero;
        }

        public float GetDirection(ref Vector2 emitterPosition, ref Vector2 particlePosition)
        {
            return RandomHelper.Random.GetFloat(MathHelper.TwoPi);
        }
    }
}
