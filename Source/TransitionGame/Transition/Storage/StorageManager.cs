using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using KiloWatt.Runtime.Support;
using Microsoft.Xna.Framework.Storage;
using Newtonsoft.Json;

namespace Transition.Storage
{
    public class StorageManager
    {
        public enum Format
        {
            Binary,
            XmlSerializer,
            Json
        }

        private readonly BaseGame _game;

        private struct SaveInfo 
        {
            public object Data { get; set; }
            public string Filename { get; set; }
            public Format Format { get; set; }
        }

        private readonly Queue<SaveInfo> _saveQueue = new Queue<SaveInfo>();

        private StorageContainer _storageContainer;
        
        /// <summary>
        /// Delegate backing the StorageDeviceSelected event.
        /// </summary>
        private EventHandler<StorageDeviceActionEventArgs> _storageDeviceActionEvent;

        /// <summary>
        /// Lock for StorageDeviceSelected delegate access.
        /// </summary>
        readonly object _storageDeviceActionLock = new object();

        /// <summary>
        /// Occurs when a storage device is selected.
        /// </summary>
        public event EventHandler<StorageDeviceActionEventArgs> StorageDeviceAction
        {
            add { lock (_storageDeviceActionLock) { _storageDeviceActionEvent += value; } }
            remove { lock (_storageDeviceActionLock) { _storageDeviceActionEvent -= value; } }
        }

        /// <summary>A collection of serializers.</summary>
        private readonly Dictionary<string, XmlSerializer> _xmlSerializers = new Dictionary<string, XmlSerializer>();

        /// <summary>A reference to the storage device.</summary>
        private StorageDevice _storageDevice;

        /// <summary>The result of the stroage guide selection.</summary>
        private IAsyncResult _storageGuideResult;

        /// <summary>Whether to show the storage device selection guide.</summary>
        private bool _showStorageGuide = true;

        /// <summary>Whether the storage device selection guide has been requested.</summary>
        private bool _storageGuideRequested;

        /// <summary>Whether saving is enabled.</summary>
        public bool StorageEnabled { get; private set; }

        public string ContainerName { get; private set; }

        public StorageManager(BaseGame game, string gameName)
        {
            _game = game;
            ContainerName = gameName + " Saved Data";
        }

        /// <summary>
        /// Updates the storage manager.
        /// </summary>
        public void Update()
        {
            if (_storageContainer != null && _storageContainer.IsDisposed)
                _storageContainer = null;    
            
            //Get a storage device?
            if (GetStorageDevice())
                NotifyStorageDeviceAction();

            MaybeSaveDataInternal();
         }

        /// <summary>
        /// Shows the storage guide. 
        /// </summary>
        public void ShowStorageGuide()
        {
            _showStorageGuide = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filename"></param>
        /// <returns></returns>
        public T Load<T>(string filename) {
            
            var obj = default(T);

            try
            {
                if (StorageEnabled == false)
                    return obj;

                if (_storageDevice == null)
                    return obj;

                if (_storageDevice.IsConnected == false)
                    return obj;

                using (var storageContainer = OpenStorageContainer(_storageDevice))
                    obj = Load<T>(storageContainer, filename); 
            }
            catch (Exception ex)
            {
                //Catch the error and swallow it! Nom nom.
                Trace.WriteLine(ex);
            }

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storageContainer"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public T Load<T>(StorageContainer storageContainer, string filename) {
            
            var obj = default(T);

            try
            {
                if (StorageEnabled == false)
                    return obj;

                if (_storageDevice == null)
                    return obj;

                if (_storageDevice.IsConnected == false)
                    return obj;

                if (storageContainer.FileExists(filename) == false)
                    return obj;

                using (var fileStream = storageContainer.OpenFile(filename, FileMode.Open))
                {
                    var x = GetXmlSerializer(typeof(T));
                    obj = (T)x.Deserialize(fileStream);
                }
            }
            catch (Exception ex)
            {
                //Catch the error and swallow it! Nom nom.
                Trace.WriteLine(ex);
            }

            return obj;
        }

        public T DeserializeObject<T>(string data)
        {
            using (var tr = new StringReader(data))
            {
                var x = GetXmlSerializer(typeof(T));
                return (T)x.Deserialize(tr);
            }
        }

        /// <summary>
        /// Saves the data to the storage device.
        /// </summary>
        /// <param name="data">The object to save.</param>
        /// <param name="filename">Name of the file to save.</param>
        /// <param name="format">Determines whether the data should be serialized as xml</param>
        public void Save(object data, string filename, Format format)
        {
            try
            {
                if (StorageEnabled == false)
                    return;

                if (_storageDevice == null)
                    return;

                if (_storageDevice.IsConnected == false)
                    return;

                //Push a new packet onto the queue
                lock(_saveQueue)
                    _saveQueue.Enqueue(new SaveInfo { Data = data, Filename = filename, Format = format });
                
            }
            catch (Exception ex)
            {
                //Catch the error and swallow it! Nom nom.
                Trace.WriteLine(ex);
            }
        }

        private void MaybeSaveDataInternal() {
            
            try
            {
                if (StorageEnabled == false) 
                    return;

                if (_storageDevice == null)
                    return;

                bool saveRequired;
                
                lock(_saveQueue) 
                {
                    saveRequired = _saveQueue.Count > 0; 
                }

                if (saveRequired == false)
                    return;

                #if !MONOMAC
                    //This causes LOADS of garbage so moved to here so it's not checked every frame - only when there's something to save.
                    if (_storageDevice.IsConnected == false)
                        return;
                #endif

                //Sync the cached file system (asynchronously)
                _game.ThreadPoolComponent.AddTask(ThreadTarget.Core1Thread3, SaveQueuedData, null, null);
            }
            catch (Exception ex)
            {
                //Catch the error and swallow it! Nom nom.
                Trace.WriteLine(ex);
            }
        }

        /// <summary>
        /// Gets a storage device.
        /// </summary>
        /// <returns>True if a stroage device was selected.</returns>
        private bool GetStorageDevice()
        {
            try
            {
                //Being showing the storage device selection guide if it is required.
                ShowDeviceSelector();

                //Monitor guide for a selection.
                return MonitorDeviceSelection();
            }
            catch (Exception ex)
            {
                //Catch the error and swallow it! Nom nom.
                Trace.WriteLine(ex);
            
                _storageDevice = null;
                _showStorageGuide = false;
                _storageGuideRequested = false;
                StorageEnabled = false;

                return false;
            }
        }

        /// <summary>
        /// Shows the storage device selector guide if required.
        /// </summary>
        private void ShowDeviceSelector()
        {
            //Begin showing the storage guide asynchornously.
            if (!_showStorageGuide)
                return;

            _storageGuideResult = StorageDevice.BeginShowSelector(null, null);

            _storageGuideRequested = true;
            _showStorageGuide = false;
        }

        /// <summary>
        /// Monitors the storage device guide for a selection. 
        /// </summary>
        /// <returns>True if storage guide dialog is dismissed (select or back); otherwise false.</returns>
        private bool MonitorDeviceSelection()
        {
            var guideDismissed = false;

            //Monitor for a device selection
            if (_storageGuideRequested && _storageGuideResult.IsCompleted)
            {
                _storageDevice = StorageDevice.EndShowSelector(_storageGuideResult);
#if !MONOMAC
				if (_storageDevice != null && _storageDevice.IsConnected)
#else	
				if (_storageDevice != null)
#endif
                    StorageEnabled = true;

				guideDismissed = true;

                _storageGuideRequested = false;
            }

            return guideDismissed;
        }

        /// <summary>
        /// Notifies interested parties of a storage device selection / cancellation.
        /// </summary>
        private void NotifyStorageDeviceAction()
        {
            if (StorageEnabled)
            {
                using (var storageContainer = OpenStorageContainer(_storageDevice))
                    OnStorageDeviceSelected(new StorageDeviceActionEventArgs(storageContainer, DialogAction.Select));
            }
            else
                //Raise the event so interested parties know the device selection screen was cancelled.
                OnStorageDeviceSelected(new StorageDeviceActionEventArgs(null, DialogAction.Back));
        }

        /// <summary>
        /// Gets the xml serializer for the passed type.
        /// </summary>
        /// <param name="type">The type of xml serializer.</param>
        /// <returns>An xml serializer.</returns>
        /// <remarks>Caches the serializer in a dictionary.</remarks>
        XmlSerializer GetXmlSerializer(Type type)
        {
            if (_xmlSerializers.ContainsKey(type.Name) == false)
                _xmlSerializers[type.Name] = new XmlSerializer(type);

            return _xmlSerializers[type.Name];
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void SaveQueuedData()
        {
            try
            {
                SaveInfo info;
                lock (_saveQueue)
                {
                    if (_saveQueue.Count == 0)
                        return;

                    info = _saveQueue.Dequeue();
                }

                var type = info.Data.GetType();

                using (var storageContainer = OpenStorageContainer(_storageDevice))
                {
                    if (storageContainer.FileExists(info.Filename))
                        storageContainer.DeleteFile(info.Filename);

                    using (var fileStream = storageContainer.OpenFile(info.Filename, FileMode.Create))
                    {
                        switch (info.Format)
                        {
                            case Format.XmlSerializer:
                                var x = GetXmlSerializer(type);
                                x.Serialize(fileStream, info.Data);
                                break;

                            case Format.Json:
                                string json = JsonConvert.SerializeObject(info.Data, Formatting.Indented);

                                using (var streamWriter = new StreamWriter(fileStream))
                                {
                                    var serializer = new JsonSerializer();
                                    serializer.Formatting = Formatting.Indented;
                                    serializer.Serialize(streamWriter, info.Data);
                                }

                                break;

                            case Format.Binary:
                                var data = info.Data as byte[];
                                if (data != null)
                                    fileStream.Write(data, 0, data.Length);
                                break;

                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Catch the error and swallow it! Nom nom.
                Trace.WriteLine(ex);
            }
        }

        public StorageContainer OpenStorageContainer()
        {
            return OpenStorageContainer(_storageDevice);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="storageDevice"></param>
        /// <returns></returns>
        private StorageContainer OpenStorageContainer(StorageDevice storageDevice)
        {
            //Got one already?
            if (_storageContainer != null && _storageContainer.IsDisposed == false)
                return _storageContainer;

            //Open a storage container.
            var result = storageDevice.BeginOpenContainer(ContainerName, null, null);

            //Wait for the WaitHandle to become signaled.
#if !WINDOWS_PHONE
            result.AsyncWaitHandle.WaitOne();
#endif

            _storageContainer = storageDevice.EndOpenContainer(result);

            //Close the wait handle.
#if !(WINDOWS_PHONE || WINDOWS_STOREAPP)
            result.AsyncWaitHandle.Close();
#endif

            return _storageContainer;
        }

        /// <summary>
        /// Occurs when a storage device is selected.
        /// </summary>
        /// <param name="e">A StorageDeviceActionEventArgs containing the data for the event.</param>
        protected internal void OnStorageDeviceSelected(StorageDeviceActionEventArgs e)
        {
            //Now raise the event to any subscribers
            EventHandler<StorageDeviceActionEventArgs> handler;

            lock (_storageDeviceActionLock)
                handler = _storageDeviceActionEvent;

            handler?.Invoke(this, e);
        }
    }
}
