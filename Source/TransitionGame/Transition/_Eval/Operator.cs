/*
 * Copyright 2008  Reg Whitton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace Transition.Eval 
{
	internal abstract class Operator 
    {
		/**
		 * End of string reached.
		 */
		internal static Operator End = new OperatorEnd(-1, 0, null, null, null);
        
        internal class OperatorEnd : Operator {
	
            internal OperatorEnd(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				throw new InvalidOperationException("END is a dummy operation");
			}
		};
	
		/**
		 * condition ? (expression if true) : (expression if false)
		 */
		public static Operator Ternary = new OperatorTernary(0, 3, "?", null, null);
	    internal class OperatorTernary : Operator {
            
            internal OperatorTernary(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }

			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return (value1 != 0) ? value2 : value3;
			}
		};
		/**
		 * &amp;&amp;
		 */
		public static Operator And = new OperatorAnd(0, 2, "&&", Type.Boolean, Type.Boolean);
        
        internal class OperatorAnd : Operator {
	
            internal OperatorAnd(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1 != 0 && value2 != 0 ? 1 : 0;
			}
		};
		/**
		 * ||
		 */
		public static Operator Or = new OperatorOr(0, 2, "||", Type.Boolean, Type.Boolean);
        
        internal class OperatorOr : Operator {
	
            internal OperatorOr(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1 != 0 || value2 != 0 ? 1 : 0;
			}
		};
		/**
		 * &gt;
		 */
		public static Operator GreaterThan = new OperatorGt(1, 2, ">", Type.Boolean, Type.Arithmetic);
        
        internal class OperatorGt : Operator {
	
            internal OperatorGt(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1.CompareTo(value2) > 0 ? 1 : 0;
			}
		};
		/**
		 * &gt;=
		 */
		public static Operator GreaterThanOrEqual = new OperatorGe(1, 2, ">=", Type.Boolean, Type.Arithmetic);
        
        internal class OperatorGe : Operator {
	
            internal OperatorGe(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1.CompareTo(value2) >= 0 ? 1 : 0;
			}
		};
		/**
		 * &lt;
		 */
		public static Operator LessThan = new OperatorLt(1, 2, "<", Type.Boolean, Type.Arithmetic);
        
        internal class OperatorLt : Operator {
	
            internal OperatorLt(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1.CompareTo(value2) < 0 ? 1 : 0;
			}
		};
		/**
		 * &lt;=
		 */
		public static Operator LessThanOrEqual = new OperatorLe(1, 2, "<=", Type.Boolean, Type.Arithmetic);
        
        internal class OperatorLe : Operator {
	
            internal OperatorLe(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1.CompareTo(value2) <= 0 ? 1 : 0;
			}
		};
		/**
		 * ==
		 */
		public static Operator Equal = new OperatorEq(1, 2, "==", Type.Boolean, Type.Arithmetic);
        
        internal class OperatorEq : Operator {
	
            internal OperatorEq(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1.CompareTo(value2) == 0 ? 1 : 0;
			}
		};
		/**
		 * != or &lt;&gt;
		 */
		public static Operator NotEqual = new OperatorNe(1, 2, "!=", Type.Boolean, Type.Arithmetic);
        
        internal class OperatorNe : Operator {
	
            internal OperatorNe(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1.CompareTo(value2) != 0 ? 1 : 0;
			}
		};
		/**
		 * +
		 */
		public static Operator Add = new OperatorAdd(2, 2, "+", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorAdd : Operator {
	
            internal OperatorAdd(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1 + value2;
			}
		};
		/**
		 * -
		 */
		public static Operator Subtract = new OperatorSub(2, 2, "-", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorSub : Operator {
	
            internal OperatorSub(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1 - value2;
			}
		};
		/**
		 * /
		 */
		public static Operator Divide = new OperatorDiv(3, 2, "/", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorDiv : Operator {
	
            internal OperatorDiv(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1 / value2;
			}
		};
		/**
		 * %
		 */
		public static Operator Remainder = new OperatorRemainder(3, 2, "%", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorRemainder : Operator {
	
            internal OperatorRemainder(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1 % value2;
			}
		};
		/**
		 * *
		 */
		public static Operator Multiply = new OperatorMul(3, 2, "*", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorMul : Operator {
	
            internal OperatorMul(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1 * value2;
			}
		};
		/**
		 * -negate
		 */
		public static Operator Negate = new OperatorNeg(4, 1, "-", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorNeg : Operator {
	
            internal OperatorNeg(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return -value1;
			}
		};
		/**
		 * +plus
		 */
		public static Operator Plus = new OperatorPlus(4, 1, "+", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorPlus : Operator {
	
            internal OperatorPlus(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1;
			}
		};
		/**
		 * abs
		 */
		public static Operator Absolute = new OperatorAbs(4, 1, " abs ", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorAbs : Operator {
	
            internal OperatorAbs(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return Math.Abs(value1);
			}
		};
		/**
		 * pow
		 */
		public static Operator Power = new OperatorPow(4, 2, " pow ", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorPow : Operator {
	
            internal OperatorPow(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				try {
					return (decimal)Math.Pow((double)value1, (double)value2);
				} catch (ArithmeticException ae) {
					throw new InvalidOperationException("pow argument: " + ae.Message);
				}
			}
		};
		/**
		 * int
		 */
		public static Operator Int = new OperatorInt(4, 1, "int ", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorInt : Operator {
	
            internal OperatorInt(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return new decimal((int)value1);
			}
		};
		/**
		 * No operation - used internally when expression contains only a reference to a variable.
		 */
		public static Operator NoOperation = new OperatorNop(4, 1, "", Type.Arithmetic, Type.Arithmetic);
        
        internal class OperatorNop : Operator {
	
            internal OperatorNop(int precedence, int numberOfOperands,String stringx,Type resultType, Type operandType) : base(precedence, numberOfOperands, stringx, resultType, operandType) { }
			//@Override
			public override decimal Perform(decimal value1, decimal value2, decimal value3) {
				return value1;
			}
		};
	
		internal int Precedence;
		internal int NumberOfOperands;
		internal String Stringx;
		internal Type ResultType;
		internal Type OperandType;
	
		protected Operator(int precedence, int numberOfOperands, String stringx, Type resultType, Type operandType) 
        {
			Precedence = precedence;
			NumberOfOperands = numberOfOperands;
			Stringx = stringx;
			ResultType = resultType;
			OperandType = operandType;
		}
	
		public abstract decimal Perform(decimal value1, decimal value2, decimal value3);
	}
}

