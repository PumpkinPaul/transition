using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Emits particles from a circluar shape.
    /// </summary>
    public class ParticleCircleEmitter : IParticleEmitter
    {
        private float _radius;

        /// <summary>
        /// Sets the radii of the emitter rings.
        /// </summary>
		/// <param name="data">The data required for configuring the emitter.</param>
        public void SetData(string data)
	    {
		    _radius = ConversionHelper.ToSingle(data);
	    }
		
        /// <summary>
        /// Gets the position of the next particle to be emitted
        /// </summary>
        /// <returns>Returns a Vector2 defining the position of the particle.</returns>
        public Vector2 GetPosition()
        {
            return VectorHelper.Polar(RandomHelper.Random.GetFloat(0, _radius), RandomHelper.Random.GetFloat(MathHelper.TwoPi));
        }
        
        public float GetDirection(ref Vector2 emitterPosition, ref Vector2 particlePosition)
        {
            return RandomHelper.Random.GetFloat(MathHelper.TwoPi);
        }
    }
}