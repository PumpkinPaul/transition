using System;
using System.Collections;
using System.Collections.Generic;

namespace Transition._____Temp.StarGuard
{
	/// <summary>
	/// Represents a generic collection of key/value pairs that are ordered independently of the key and value.
	/// </summary>
	/// <typeparam name="TKey">The type of the keys in the dictionary</typeparam>
	/// <typeparam name="TValue">The type of the values in the dictionary</typeparam>
	public class HashList<TKey, TValue> : IHashList<TKey, TValue>
	{
	    readonly TKey _defaultKey;

		const int DefaultInitialCapacity = 0;

	    readonly Dictionary<TKey, TValue> _dictionary;
	    readonly List<KeyValuePair<TKey, TValue>> _list;
		readonly IEqualityComparer<TKey> _comparer;
		
		readonly int _initialCapacity;

		/// <summary>
		/// Initializes a new instance of the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> class.
		/// </summary>
		public HashList() : this(DefaultInitialCapacity, null, default(TKey)) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> class using the specified initial capacity.
		/// </summary>
		/// <param name="capacity">The initial number of elements that the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> can contain.</param>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="capacity"/> is less than 0</exception>
		public HashList(int capacity) : this(capacity, null, default(TKey)) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> class using the specified comparer.
		/// </summary>
		/// <param name="comparer">The <see cref="IEqualityComparer{TKey}">IEqualityComparer&lt;TKey&gt;</see> to use when comparing keys, or <null/> to use the default <see cref="EqualityComparer{TKey}">EqualityComparer&lt;TKey&gt;</see> for the type of the key.</param>
		public HashList(IEqualityComparer<TKey> comparer) : this(DefaultInitialCapacity, comparer, default(TKey)) { }

	    /// <summary>
	    /// Initializes a new instance of the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> class using the specified initial capacity and comparer.
	    /// </summary>
	    /// <param name="capacity">The initial number of elements that the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection can contain.</param>
	    /// <param name="comparer">The <see cref="IEqualityComparer{TKey}">IEqualityComparer&lt;TKey&gt;</see> to use when comparing keys, or <null/> to use the default <see cref="EqualityComparer{TKey}">EqualityComparer&lt;TKey&gt;</see> for the type of the key.</param>
	    /// <param name="defaultKey"></param>
	    /// <exception cref="ArgumentOutOfRangeException"><paramref name="capacity"/> is less than 0</exception>
	    public HashList(int capacity, IEqualityComparer<TKey> comparer, TKey defaultKey)
		{
			if(0 > capacity)
				throw new ArgumentOutOfRangeException(nameof(capacity), "'capacity' must be non-negative");

			_initialCapacity = capacity;
			_comparer = comparer;
            _defaultKey = defaultKey;

            _dictionary = new Dictionary<TKey, TValue>(_initialCapacity, _comparer);
            _list = new List<KeyValuePair<TKey, TValue>>(_initialCapacity); 
		}

		/// <summary>
		/// Gets the dictionary object that stores the keys and values
		/// </summary>
		/// <value>The dictionary object that stores the keys and values for the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see></value>
		/// <remarks>Accessing this property will create the dictionary object if necessary</remarks>
		Dictionary<TKey, TValue> Dictionary
		{
			get { return _dictionary; }
		}

		/// <summary>
		/// Gets the list object that stores the key/value pairs.
		/// </summary>
		/// <value>The list object that stores the key/value pairs for the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see></value>
		/// <remarks>Accessing this property will create the list object if necessary.</remarks>
		public List<KeyValuePair<TKey, TValue>> List
		{
			get { return _list; }
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey,TValue>>.GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		/// <summary>
		/// Inserts a new entry into the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection with the specified key and value at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which the element should be inserted.</param>
		/// <param name="key">The key of the entry to add.</param>
		/// <param name="value">The value of the entry to add. The value can be <null/> if the type of the values in the dictionary is a reference type.</param>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than 0.<br/>
		/// -or-<br/>
		/// <paramref name="index"/> is greater than <see cref="Count"/>.</exception>
		/// <exception cref="ArgumentNullException"><paramref name="key"/> is <null/>.</exception>
		/// <exception cref="ArgumentException">An element with the same key already exists in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>.</exception>
		public void Insert(int index, TKey key, TValue value)
		{
			if(index > Count || index < 0)
				throw new ArgumentOutOfRangeException(nameof(index));

			Add(key, value);
			_list.Insert(index, new KeyValuePair<TKey, TValue>(key, value));
		}

		/// <summary>
		/// Removes the entry at the specified index from the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection.
		/// </summary>
		/// <param name="index">The zero-based index of the entry to remove.</param>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than 0.<br/>
		/// -or-<br/>
		/// index is equal to or greater than <see cref="Count"/>.</exception>
		public void RemoveAt(int index)
		{
			if(index >= Count || index < 0)
				throw new ArgumentOutOfRangeException(nameof(index), "'index' must be non-negative and less than the size of the collection");

			var key = _list[index].Key;

			_list.RemoveAt(index);

            if(!EqualityComparer<TKey>.Default.Equals(key, _defaultKey))
			    Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets or sets the value at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the value to get or set.</param>
		/// <value>The value of the item at the specified index.</value>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than 0.<br/>
		/// -or-<br/>
		/// index is equal to or greater than <see cref="Count"/>.</exception>
		public TValue this[int index]
		{
			get { return _list[index].Value; }
			set
			{
				if(index >= Count || index < 0)
					throw new ArgumentOutOfRangeException(nameof(index), "'index' must be non-negative and less than the size of the collection");

				var key = _list[index].Key;

				_list[index] = new KeyValuePair<TKey, TValue>(key, value);

                if(!EqualityComparer<TKey>.Default.Equals(key, _defaultKey))
				    Dictionary[key] = value;
			}
		}

		/// <summary>
		/// Adds an entry with the specified key and value into the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection with the lowest available index.
		/// </summary>
		/// <param name="key">The key of the entry to add.</param>
		/// <param name="value">The value of the entry to add. This value can be <null/>.</param>
		/// <remarks>A key cannot be <null/>, but a value can be.
		/// <para>You can also use the <see cref="P:HashList{TKey,TValue}.Item(TKey)"/> property to add new elements by setting the value of a key that does not exist in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection; however, if the specified key already exists in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>, setting the <see cref="P:HashList{TKey,TValue}.Item(TKey)"/> property overwrites the old value. In contrast, the <see cref="M:Add"/> method does not modify existing elements.</para></remarks>
		/// <exception cref="ArgumentNullException"><paramref name="key"/> is <null/></exception>
		/// <exception cref="ArgumentException">An element with the same key already exists in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see></exception>
		void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
		{
			Add(key, value);
		}

		/// <summary>
		/// Adds an entry with the specified key and value into the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection with the lowest available index.
		/// </summary>
		/// <param name="key">The key of the entry to add.</param>
		/// <param name="value">The value of the entry to add. This value can be <null/>.</param>
		/// <returns>The index of the newly added entry</returns>
		/// <remarks>A key cannot be <null/>, but a value can be.
		/// <para>You can also use the <see cref="P:HashList{TKey,TValue}.Item(TKey)"/> property to add new elements by setting the value of a key that does not exist in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection; however, if the specified key already exists in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>, setting the <see cref="P:HashList{TKey,TValue}.Item(TKey)"/> property overwrites the old value. In contrast, the <see cref="M:Add"/> method does not modify existing elements.</para></remarks>
		/// <exception cref="ArgumentNullException"><paramref name="key"/> is <null/></exception>
		/// <exception cref="ArgumentException">An element with the same key already exists in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see></exception>
		public int Add(TKey key, TValue value)
		{            
            if(!EqualityComparer<TKey>.Default.Equals(key, _defaultKey))
			    Dictionary.Add(key, value);

			_list.Add(new KeyValuePair<TKey,TValue>(key, value));
			return Count - 1;
		}

		/// <summary>
		/// Removes all elements from the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection.
		/// </summary>
		/// <remarks>The capacity is not changed as a result of calling this method.</remarks>
		public void Clear()
		{
			_dictionary.Clear();
			_list.Clear();
		}

		/// <summary>
		/// Determines whether the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection contains a specific key.
		/// </summary>
		/// <param name="key">The key to locate in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection.</param>
		/// <returns><see langword="true"/> if the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection contains an element with the specified key; otherwise, <see langword="false"/>.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="key"/> is <null/></exception>
		public bool ContainsKey(TKey key)
		{
			return _dictionary.ContainsKey(key);
		}

		/// <summary>
		/// Gets a value indicating whether the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection is read-only.
		/// </summary>
		/// <value><see langword="true"/> if the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> is read-only; otherwise, <see langword="false"/>. The default is <see langword="false"/>.</value>
		/// <remarks>
		/// A collection that is read-only does not allow the addition, removal, or modification of elements after the collection is created.
		/// <para>A collection that is read-only is simply a collection with a wrapper that prevents modification of the collection; therefore, if changes are made to the underlying collection, the read-only collection reflects those changes.</para>
		/// </remarks>
		public bool IsReadOnly
		{
			get { return false; }
		}

		/// <summary>
		/// Returns the zero-based index of the specified key in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>
		/// </summary>
		/// <param name="key">The key to locate in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see></param>
		/// <returns>The zero-based index of <paramref name="key"/>, if <paramref name="ley"/> is found in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>; otherwise, -1</returns>
		/// <remarks>This method performs a linear search; therefore it has a cost of O(n) at worst.</remarks>
		public int IndexOfKey(TKey key)
		{
			if(EqualityComparer<TKey>.Default.Equals(key, _defaultKey))
				return -1;

			for(var index = 0; index < List.Count; index++)
			{
				var entry = _list[index];
				var next = entry.Key;

                if(EqualityComparer<TKey>.Default.Equals(next, _defaultKey))
                    continue;

				if(null != _comparer)
				{
					if(_comparer.Equals(next, key))
						return index;
				}
				else if(next.Equals(key))
					return index;
			}

			return -1;
		}

		/// <summary>
		/// Removes the entry with the specified key from the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection.
		/// </summary>
		/// <param name="key">The key of the entry to remove</param>
		/// <returns><see langword="true"/> if the key was found and the corresponding element was removed; otherwise, <see langword="false"/></returns>
		public bool Remove(TKey key)
		{
		    if(EqualityComparer<TKey>.Default.Equals(key, _defaultKey))
				return false;

			var index = IndexOfKey(key);
		    if (index < 0) return false;
		    if (!_dictionary.Remove(key)) 
                return false;

		    _list.RemoveAt(index);
		    return true;
		}

		/// <summary>
		/// Gets or sets the value with the specified key.
		/// </summary>
		/// <param name="key">The key of the value to get or set.</param>
		/// <value>The value associated with the specified key. If the specified key is not found, attempting to get it returns <null/>, and attempting to set it creates a new element using the specified key.</value>
		public TValue this[TKey key]
		{
			get { return _dictionary[key]; }
			set
			{
				if(_dictionary.ContainsKey(key))
				{
					_dictionary[key] = value;
					_list[IndexOfKey(key)] = new KeyValuePair<TKey, TValue>(key, value);
				}
				else
				{
					Add(key, value);
				}
			}
		}

		/// <summary>
		/// Gets the number of key/values pairs contained in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection.
		/// </summary>
		/// <value>The number of key/value pairs contained in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> collection.</value>
		public int Count
		{
			get { return _list.Count; }
		}

		/// <summary>
		/// Gets an <see cref="T:System.Collections.Generic.ICollection{TKey}">ICollection&lt;TKey&gt;</see> object containing the keys in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>.
		/// </summary>
		/// <value>An <see cref="T:System.Collections.Generic.ICollection{TKey}">ICollection&lt;TKey&gt;</see> object containing the keys in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>.</value>
		/// <remarks>The returned <see cref="T:System.Collections.Generic.ICollection{TKey}">ICollection&lt;TKey&gt;</see> object is not a static copy; instead, the collection refers back to the keys in the original <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>. Therefore, changes to the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> continue to be reflected in the key collection.</remarks>
		public ICollection<TKey> Keys
		{
			get { return _dictionary.Keys; }
		}

		/// <summary>
		/// Gets the value associated with the specified key.
		/// </summary>
		/// <param name="key">The key of the value to get.</param>
		/// <param name="value">When this method returns, contains the value associated with the specified key, if the key is found; otherwise, the default value for the type of <paramref name="value"/>. This parameter can be passed uninitialized.</param>
		/// <returns><see langword="true"/> if the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> contains an element with the specified key; otherwise, <see langword="false"/>.</returns>
		public bool TryGetValue(TKey key, out TValue value)
		{
			return _dictionary.TryGetValue(key, out value);
		}

		/// <summary>
		/// Gets an <see cref="T:ICollection{TValue}">ICollection&lt;TValue&gt;</see> object containing the values in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>.
		/// </summary>
		/// <value>An <see cref="T:ICollection{TValue}">ICollection&lt;TValue&gt;</see> object containing the values in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>.</value>
		/// <remarks>The returned <see cref="T:ICollection{TValue}">ICollection&lt;TKey&gt;</see> object is not a static copy; instead, the collection refers back to the values in the original <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>. Therefore, changes to the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> continue to be reflected in the value collection.</remarks>
		public ICollection<TValue> Values
		{
			get { return _dictionary.Values; }
		}

		/// <summary>
		/// Adds the specified value to the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> with the specified key.
		/// </summary>
		/// <param name="item">The <see cref="T:KeyValuePair{TKey,TValue}">KeyValuePair&lt;TKey,TValue&gt;</see> structure representing the key and value to add to the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>.</param>
		void ICollection<KeyValuePair<TKey,TValue>>.Add(KeyValuePair<TKey, TValue> item)
		{
			Add(item.Key, item.Value);
		}

		/// <summary>
		/// Determines whether the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> contains a specific key and value.
		/// </summary>
		/// <param name="item">The <see cref="T:KeyValuePair{TKey,TValue}">KeyValuePair&lt;TKey,TValue&gt;</see> structure to locate in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>.</param>
        /// <returns><see langword="true"/> if <paramref name="item{TKey,TValue}"/> is found in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>; otherwise, <see langword="false"/>.</returns>
		bool ICollection<KeyValuePair<TKey,TValue>>.Contains(KeyValuePair<TKey, TValue> item)
		{
			return ((ICollection<KeyValuePair<TKey,TValue>>)_dictionary).Contains(item);
		}

		/// <summary>
		/// Copies the elements of the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see> to an array of type <see cref="T:KeyValuePair`2>"/>, starting at the specified index.
		/// </summary>
		/// <param name="array">The one-dimensional array of type <see cref="T:KeyValuePair{TKey,TValue}">KeyValuePair&lt;TKey,TValue&gt;</see> that is the destination of the <see cref="T:KeyValuePair{TKey,TValue}">KeyValuePair&lt;TKey,TValue&gt;</see> elements copied from the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>. The array must have zero-based indexing.</param>
		/// <param name="arrayIndex">The zero-based index in <paramref name="array"/> at which copying begins.</param>
		void ICollection<KeyValuePair<TKey,TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			((ICollection<KeyValuePair<TKey,TValue>>)_dictionary).CopyTo(array, arrayIndex);
		}

		/// <summary>
		/// Removes a key and value from the dictionary.
		/// </summary>
		/// <param name="item">The <see cref="T:KeyValuePair{TKey,TValue}">KeyValuePair&lt;TKey,TValue&gt;</see> structure representing the key and value to remove from the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>.</param>
        /// <returns><see langword="true"/> if the key and value represented by <paramref name="item"/> is successfully found and removed; otherwise, <see langword="false"/>. This method returns <see langword="false"/> if <paramref name="item"/> is not found in the <see cref="HashList{TKey,TValue}">HashList&lt;TKey,TValue&gt;</see>.</returns>
		bool ICollection<KeyValuePair<TKey,TValue>>.Remove(KeyValuePair<TKey, TValue> item)
		{
			return Remove(item.Key);
		}
	}
}
