using System;
using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Emits particles from a ring shape.
    /// </summary>
    public class ParticleRingEmitter : IParticleEmitter
    {
		/// <summary>The radius of the circle that defines the inner part of the ring</summary>
        private float _innerRadius;
		
		/// <summary>The radius of the circle that defines the outer part of the ring</summary>
        private float _outerRadius;

        bool _tangentialDirection;

        /// <summary>
        /// Sets the radii of the emitter rings.
        /// </summary>
		/// <param name="data">The data required for configuring the emitter.</param>
        public void SetData(string data)
	    {
			var radii = data.Split(',');
		    _innerRadius = ConversionHelper.ToSingle(radii[0].Trim());
		    _outerRadius = ConversionHelper.ToSingle(radii[1].Trim());

            if (radii.Length > 2)
                _tangentialDirection = ConversionHelper.ToBool(radii[2].Trim());
	    }

        /// <summary>
        /// Gets the position of the next particle to be emitted
        /// </summary>
        /// <returns>Returns a Vector2 defining the position of the particle.</returns>
        public Vector2 GetPosition()
        {
            return VectorHelper.Polar(RandomHelper.Random.GetFloat(_innerRadius, _outerRadius), RandomHelper.Random.GetFloat(MathHelper.TwoPi));
        }
        
        public float GetDirection(ref Vector2 emitterPosition, ref Vector2 particlePosition)
        {
            if (_tangentialDirection)
                return (float)Math.Atan2(particlePosition.Y - emitterPosition.Y, particlePosition.X - emitterPosition.X);

            return RandomHelper.Random.GetFloat(MathHelper.TwoPi);
        }
    }
}