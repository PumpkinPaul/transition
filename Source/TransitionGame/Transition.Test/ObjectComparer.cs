﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Transition._____Temp.StarGuard;

namespace Transition.Test 
{
    public class ObjectComparer
    {
        readonly IList<Type> _referenceTypes;
        readonly Dictionary<KeyRefPair<object, object>, bool> _comparisions = new Dictionary<KeyRefPair<object, object>, bool>();

        readonly StringBuilder _stringBuilder = new StringBuilder();
        int _nesting;

        public ObjectComparer(IList<Type> referenceTypes)
        {
            _referenceTypes = referenceTypes;
        }

        public void ResetComparisons()
        {
            _comparisions.Clear();
        }

        public bool Equivalent<T>(T source, T clone, List<string> randomAtrtributes = null)
        {
            try
            {
                Log("ObjectComparer.Equivalent(object source, object clone)");

                _nesting++;

                var sourceType = TypeName(source);
                var cloneType = TypeName(clone);
                Log("source type is " + sourceType.Name + " clone type is " + cloneType.Name);

                if (sourceType != cloneType)
                    return false;

                if (source == null && clone == null) {
                    Log("source and clone are both null");
                    return true;
                }

                if (source == null || clone == null) {
                    Log(source == null ? "source is null" : "clone is null");
                    return false;
                }

                var key = new KeyRefPair<object, object>(source, clone);
                if (_comparisions.ContainsKey(key))
                    return _comparisions[key];

                _comparisions.Add(key, false);
                
                const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

                var fields = sourceType.GetFields(bindingFlags);
                foreach(var fieldInfo in fields)
                {
                    //Is it a property set from a random seed?
                    if (randomAtrtributes != null)
                    {
                        if (randomAtrtributes.Contains(fieldInfo.Name))
                        {
                            Log($"Skipping field: {fieldInfo.Name} as it is generated randomly");
                            _comparisions[key] = true;
                            return true;
                        }
                    }

                    Log("Testing " + fieldInfo.FieldType.Name + " " + fieldInfo.Name);
                    var sourceFieldValue = fieldInfo.GetValue(source);
                    var cloneFieldValue = fieldInfo.GetValue(clone);            

                    if (_referenceTypes != null)
                    {
                        foreach(var type in _referenceTypes)
                        {
                            if (!fieldInfo.FieldType.IsSubclassOf(type))
                                continue;
                        
                            //Must be a reference to the same object
                            var b = ReferenceEquals(sourceFieldValue, cloneFieldValue);
                            _comparisions[key] = b;

                            if (!b)
                            {
                                Log("ReferenceEquals is false for " + fieldInfo.FieldType.Name + " / " + type.Name);
                                return false;
                            }
   
                            break;
                        }
                    }
                            
                    if (fieldInfo.FieldType.IsValueType)
                    {
                        if (sourceFieldValue.Equals(cloneFieldValue) == false)
                        {
                            Log("ValueType Equals is false, expected " + sourceFieldValue + " but found " + cloneFieldValue);
                            _comparisions[key] = false;
                            return false;
                        }
                    }
                    else
                    {
                        if (Equivalent(sourceFieldValue, cloneFieldValue) == false)
                        {
                            Log("Equivalent is false");
                            _comparisions[key] = false;
                            return false;
                        }
                    }

                    Log(fieldInfo.FieldType.Name + " " + fieldInfo.Name + " members are equivalent");
                }

                _comparisions[key] = true;
                return true;
            }
            finally
            {
                _nesting--;
            }
        }

        Type TypeName<T>(T obj)
        {
            return typeof(T);
        }
        
        void Log(string message)
        {
            _stringBuilder.Length = 0;
            for (var i = 0; i < _nesting; i++)
                _stringBuilder.Append("    ");

            _stringBuilder.Append(message);
            System.Diagnostics.Debug.WriteLine(_stringBuilder.ToString());
        }
    }
}