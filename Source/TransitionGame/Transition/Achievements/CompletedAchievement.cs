namespace Transition.Achievements
{
    /// <summary>
    /// Represents information required to log an achievement as completed for a profile
    /// </summary>
    public class CompletedAchievement
    {	
        public string Guid;
        public Difficulty Difficulty;				
        
        // Require this constructor for serialization.
        // ReSharper disable UnusedMember.Local
        private CompletedAchievement() { }
        // ReSharper restore UnusedMember.Local

        public CompletedAchievement(string guid, Difficulty difficulty)
        {
            Guid = guid;
            Difficulty = difficulty;
        }
    }
}

