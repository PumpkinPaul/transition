using Microsoft.Xna.Framework;

namespace Transition.CourseSystem
{
    public class Label 
    {
        public Vector2 Position;
        public string Text { get; set; }
        public float Rotation { get; set; }
        public Vector2 FontSize { get; set; }
        public Color Color { get; set; }
        public Alignment Alignment { get; set; }
    }
}
