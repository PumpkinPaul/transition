using System;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.Input;

namespace Transition.MenuControls
{
    /// <summary>
    /// A sliding bar control.
    /// </summary>
    public class SliderControl : MenuControl
    {
        public event EventHandler ValueChanged;

        public enum FillMode
        {
            LeftToRight,
            RightToLeft
        }

        public int MinValue = 0;
        public int MaxValue = 100;

        /// <summary></summary>
		int _value;	
		
		/// <summary></summary>
        public int Step = 1;

        readonly TextureInfo _barTextureInfo;

        public FillMode Fill;

        /// <summary>Gets or sets the size of the control.</summary>
        public Vector2 Size = new Vector2(5.0f, 1.0f);

        public override bool AutoTabIndex { get { return true; } }

        public bool IsDragging { get; private set;}

		/// <summary>
		/// .
		/// </summary>
        public SliderControl(BaseGame game) : base(game)
		{
            AButtonText = null;

            _barTextureInfo = Game.TextureManager.GetTextureInfo("Pixel");
            Fill = FillMode.LeftToRight;
		}

        public override void Load(XElement element)
        {
            base.Load(element);

            Fill = element.GetAttributeEnum("fill", Fill);
            Size = element.GetAttributeVector2("size", Size);
            MinValue = element.GetAttributeInt32("minValue", MinValue);
            MaxValue = element.GetAttributeInt32("maxValue", MaxValue);
            Step = element.GetAttributeInt32("step", Step);
        }

		/// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
		public override Rect Bounds
		{
			get 
			{
                var halfSize = Size / 2.0f;

                switch (Fill)
                {
                    case FillMode.LeftToRight:
                        return new Rect(Position.Y + halfSize.Y, Position.X, Position.Y - halfSize.Y, Position.X + Size.X);

                    //case FillMode.RightToLeft:
                    default:
                        return new Rect(Position.Y + halfSize.Y, Position.X - Size.X, Position.Y - halfSize.Y, Position.X);
                }
			}
		}

        /// <summary>Gets the standard value of the control from a normalised input</summary>
        /// <param name="normalizedValue"></param>
        /// <returns></returns>
        int GetValueFromNormalizedValue(float normalizedValue)
        {
            var range = MaxValue - MinValue;
            return MinValue + (int)Math.Round((normalizedValue * range) / Step) * Step;

        }

        /// <summary>Gets the standard value of the control from a normalised input</summary>
        /// <param name="normalizedValue"></param>
        /// <returns></returns>
        public void SetValueFromNormalizedValue(float normalizedValue)
        {
            Value = GetValueFromNormalizedValue(normalizedValue);
        }

        /// <summary>Gets the value of the control as a normalized value 0.0f...value...1.0f</summary>
        public float NormalizedValue
        {
            get
            {
                var range = MaxValue - MinValue;
                var filled = _value - MinValue;
                return (filled / (float)range);
            }
        }

 
        /// <summary>Gets or sets the standard value of the control (in the range MinValue...value...MaxValue)</summary>
		public int Value
		{
			get { return _value; }
			set 
            {
                var oldValue = _value; 
                _value = (int)MathHelper.Clamp(value, MinValue, MaxValue);

                if (oldValue != _value)
                {
                    PerformValueChanged();

                    if (_value > oldValue)
                        RaiseIncreaseValue();
                    else if (_value < oldValue)
                        RaiseDecreaseValue();
                }
            }
		}

        public override string LeftRightButtonText
        {
            get { return "change value"; }
        }

        protected override void OnMouseDown()
        {
            IsDragging = true;
            SetValueFromMousePosition();
        }

		protected override void OnMouseMove()
		{
            if (MouseHelper.IsLeftButtonDown == false)
                return;

            SetValueFromMousePosition();
        }

        protected override void OnMouseUp()
        {
            IsDragging = false;
        }

        void SetValueFromMousePosition()
        {
			var clientX = Game.MenuManager.MouseWorldPosition.X - Position.X;	
            var normalizedValue = MathHelper.Clamp(clientX / Size.X, 0.0f, 1.0f);

            if (normalizedValue < 0.05f)
                normalizedValue = 0;

            Value = GetValueFromNormalizedValue(normalizedValue);
		}

		protected override bool OnIncreaseValue()
		{
            if (_value >= MaxValue)
                return false;
		    
		    Value += Step;
		    return true;
		}
		
		protected override bool OnDecreaseValue()
		{
            if (_value <= MinValue)
                return false;

            Value -= Step;
            return true;
		}

        protected override void OnUpdate()
        {
            AButtonText = string.Empty;

            if (Game.InputMethodManager.InputMethod.SupportsMouse == false)
                return;

            if (Bounds.Contains(Game.MenuManager.MouseWorldPosition))
            {
                AButtonText = "Select";
            }
        }
		
		/// <summary>
		/// .
		/// </summary>
        protected override void OnDraw()
		{
            var tint = EffectiveTint;

            //Draw the background of the slider full size
            var rect = Bounds;
            var q = new Quad(rect);
            //Game.GeometryBatch.DrawSprite(ref q, _barTextureInfo.TextureId, _barTextureInfo.SourceRect, tint * InactiveAlphaModifier, RenderStyle);

            Game.Renderer.DrawBox(rect, tint, RenderStyle, 0.05f, Layer);

            //Draw the 'value' bar to the required length
            var length = NormalizedValue * Size.X;

            switch (Fill)
            {
                case FillMode.LeftToRight:
                    q.Right(rect.Left + length);
                    break;

                case FillMode.RightToLeft:
                    q.Left(rect.Right - length);
                    break;
            }

            Game.Renderer.DrawSprite(ref q, _barTextureInfo.TextureId, _barTextureInfo.SourceRect, tint, RenderStyle, Layer);
		}

        public void PerformValueChanged()
        {
            LogProcess("PerformValueChanged");

            //Allow subclasses to perform actions before raising the event
            if (OnValueChanged() == false)
                return;

            //Now raise the event to any subscribers
            RaiseValueChanged();
        }

        protected void RaiseValueChanged()
        {
            var handler = ValueChanged;

            if (handler != null)
            {
                LogEvent("ValueChanged");
                handler(this, EventArgs.Empty);
            }
        }
    }
}
