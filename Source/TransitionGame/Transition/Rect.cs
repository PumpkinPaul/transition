using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Represents a rectangle in 2d space.
    /// </summary>
    /// <remarks>
    /// Use this class in a perspective type world where 
    /// 
    ///                     +Y  
    ///                      |
    ///                      |
    ///                      |
    ///                      |0,0
    ///         -X --------------------- +X
    ///                      |
    ///                      |
    ///                      |
    ///                      |
    ///                     -Y
    /// 
    /// Hopefully this will make working in a 2d world much easier than trying to retrofit the XNA Rectangle class where bottom > top.
    /// </remarks>
    public struct Rect
    {
        public static readonly Rect Zero = new Rect();

        public static readonly Rect TextureRect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);

        public static readonly Rect InvTextureRect = new Rect(1.0f, 0.0f, 0.0f, 1.0f);

		/// <summary>.</summary>
		public float Top;
        public float Left;
        public float Bottom;
        public float Right;

        public float Width { get { return Right - Left; } }
        public float Height { get { return Top - Bottom; } }
        		
        /// <summary>
        /// Creates a new instance of a Rect.
        /// </summary>
        public Rect(float top, float left, float bottom, float right)
        {
            Top = top;
            Left = left;
            Bottom = bottom;
            Right = right;
        }

        /// <summary>
        /// Creates a new instance of a Rect.
        /// </summary>
        public Rect(Vector2 bottomLeft, Vector2 size)
        {
            Bottom = bottomLeft.Y;
            Top = Bottom + size.Y;
            Left = bottomLeft.X;
            Right = Left + size.X;
        }

        /// <summary>
        /// Creates a new instance of a Rect.
        /// </summary>
        public Rect(Vector2 position, Vector2 size, Alignment alignment)
        {
            var half = size / 2.0f;

            switch (alignment)
            {
                case Alignment.TopLeft:
                    Top = position.Y;
                    Left = position.X;
                    Bottom = position.X;
                    Right = Left + size.X;
                    break;

                case Alignment.CentreLeft:
                    Top = position.Y + half.Y;
                    Left = position.X;
                    Bottom = position.Y - half.Y;
                    Right = Left + size.X;
                    break;

                case Alignment.BottomLeft:
                    Bottom = position.Y;
                    Left = position.X;
                    Top = Bottom + size.Y;
                    Right = Left + size.X;
                    break;

                case Alignment.TopCentre:
                    Top = position.Y;
                    Right = position.X + half.X;
                    Bottom = Top - size.Y;
                    Left = Right - size.X;
                    break;

                case Alignment.Centre:
                    Top = position.Y + half.Y;
                    Right = position.X + half.X;
                    Bottom = position.Y - half.Y;
                    Left = position.X - half.X;
                    break;

                case Alignment.BottomCentre:
                    Bottom = position.Y;
                    Right = position.X + half.X;
                    Top = Bottom + size.Y;
                    Left = Right - size.X;
                    break;

                case Alignment.TopRight:
                    Top = position.Y;
                    Right = position.X;
                    Bottom = Top - size.Y;
                    Left = Right - size.X;
                    break;

                case Alignment.CentreRight:
                    Top = position.Y + half.Y;
                    Right = position.X;
                    Bottom = position.Y - half.Y;
                    Left = Right - size.X;
                    break;
                
                case Alignment.BottomRight:
                default:
                    Bottom = position.Y;
                    Right = position.X;
                    Top = Bottom + size.Y;
                    Left = Right - size.X;
                    
                    break;
            }   
        }
		
		/// <summary>
		/// .
		/// </summary>
		public bool Contains(Vector2 point)
		{
            return (point.X >= Left && point.X <= Right && point.Y <= Top && point.Y >= Bottom);
		}

        public bool Contains(Vector2 point, float rotation)
        {
            var rotationMatrix = Matrix.CreateRotationZ(-rotation);

            //difference vector from rotation center to mouse
            var localPoint = point - new Vector2(Left + ( Width / 2.0f), Bottom + (Height / 2.0f));

            //now rotate the point
            var rotatedPoint = Vector2.Transform(localPoint, rotationMatrix);

            return rotatedPoint.X > -Width / 2 && rotatedPoint.X < Width / 2 && rotatedPoint.Y > -Height / 2 && rotatedPoint.Y < Height / 2;
        }

        public bool Contains(Vector3 point)
		{
            return (point.X >= Left && point.X <= Right && point.Y <= Top && point.Y >= Bottom);
		}

        /// <summary>
        /// .
        /// </summary>
        public bool Contains(Obb2 obb)
        {
            return Contains(obb.Min) && Contains(obb.Max);
        }

        public void Inflate(float amount)
        {
            Top += amount;
            Bottom -= amount;
            Left -= amount;
            Right += amount;
        }

        public Rectangle ToRectangle()
        {
            return new Rectangle((int)Left, (int)Top, (int)Width, (int)Height);
        }
    }
}
