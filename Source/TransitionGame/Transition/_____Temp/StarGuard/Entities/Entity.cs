﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using ProtoBuf;
using Transition.ActorDatas;
using Transition.Animations;
using Transition.Gearset;
using Transition.Rendering;
using Transition._____Temp.StarGuard.EntityBlueprints;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary></summary>
    /// <remarks>
    /// ProtobufNet Notes.
    /// BeforeDeserialization / AfterDeserialization / etc do not need to be virtual - the methods will get called at each level of the inheritance heirarchy.
    /// </remarks>>
    [ProtoContract]
    public abstract class Entity
    {
        const int ActiveFlag = 1;
        const int CollidableFlag = 2;
        const int VisibleFlag = 4;
        const int IsFacingRightFlag = 8;
        const int OnGroundFlag = 16;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        protected EntityBlueprint Blueprint;

        [ProtoMember(2)]
        public Vector2 Position;

        [ProtoMember(3)]
        public Vector2 Velocity;

        [ProtoMember(4)]
        public float Direction;

        [ProtoMember(5)]
        int _flags;

        [ProtoMember(6)]
        protected readonly List<Sprite> Sprites = new List<Sprite>();

        [ProtoMember(7)]
        protected int HitTicks;

        [ProtoMember(8)]
        public int Seed { get; private set; }

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here
        protected TransitionGame Game;

        public Box2 CollisionBounds;
        //----------------------------------------------------------------------------------------------------

        public bool Active
        {
            get { return (_flags & ActiveFlag) == ActiveFlag; }
            private set { if (value) _flags |= ActiveFlag; else _flags &= ~ActiveFlag; }
        }

        public bool Collidable
        {
            get { return (_flags & CollidableFlag) == CollidableFlag; }
            protected set { if (value) _flags |= CollidableFlag; else _flags &= ~CollidableFlag; }
        }

        public bool Visible
        {
            get { return (_flags & VisibleFlag) == VisibleFlag; }
            set { if (value) _flags |= VisibleFlag; else _flags &= ~VisibleFlag; }
        }

        public bool IsFacingRight
        {
            get { return (_flags & IsFacingRightFlag) == IsFacingRightFlag; }
            set { if (value) _flags |= IsFacingRightFlag; else _flags &= ~IsFacingRightFlag; }
        }

        public bool OnGround
        {
            get { return (_flags & OnGroundFlag) == OnGroundFlag; }
            set { if (value) _flags |= OnGroundFlag; else _flags &= ~OnGroundFlag; }
        }

        public bool IsFacingLeft => !IsFacingRight;
        public bool IsMovingRight => Velocity.X > 0;
        public bool IsMovingLeft => Velocity.X < 0;

        protected Entity(TransitionGame game) 
        {
            Game = game;
        }

        public abstract EntityClassification Classification { get; }
        
        public virtual void Initialise(string name)
        {
            Position = Vector2.Zero;
            Velocity = Vector2.Zero;
            Direction = 0.0f;
            CollisionBounds = Box2.Empty;

            _flags = 0;
            Active = false;
            Visible = true;
            Collidable = false;
            IsFacingRight = true;
            OnGround = false;

            HitTicks = 0;
            Seed = RandomHelper.FastRandom.Next();

            Blueprint = Game.EntityBlueprints.EntityBlueprint(name);

            InitialiseSprites();

            InitialiseNonSerialized();
        }

        void InitialiseSprites()
        {
            Sprites.Clear();

            if (Blueprint?.Layers == null) 
                return;

            var layers = Blueprint.Layers.Length;
            for (var i = 0; i < layers; i++)
            {
                var sprite = Game.SpritePool.Allocate();
                if (sprite == null)
                    break;

                sprite.SetAppearance(Blueprint.Layers[i].Appearance);
                sprite.Layer = Blueprint.Layers[i].DrawLayer;
                Sprites.Add(sprite);
            }
        }

        private void InitialiseNonSerialized()
        {
            CollisionBounds = Blueprint.CollisionBounds;
        }

        [ProtoBeforeDeserialization]
        // ReSharper disable once UnusedMember.Global
        public void BeforeDeserialization()
        {
            Sprites.Clear();
        }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        private void AfterDeserialization()
        {
            //Game = TransitionGame.Instance;
            Game = GameServices.Instance.Get<TransitionGame>();

            InitialiseNonSerialized();

            if (Blueprint == null)
                Blueprint = null;
        }

        /// <summary>
        /// Gets a rectangle which bounds this object in world space.
        /// </summary>
        public Box2 GetBoundingBox()
        {
            var x = Position.X + CollisionBounds.X;
            var y = Position.Y + CollisionBounds.Y;

            return new Box2(x, y, CollisionBounds.Width, CollisionBounds.Height);
        }

        /// <summary>
        /// Gets a rectangle which bounds this object in world space.
        /// </summary>
        public Box2 GetBoundingBox(Vector2 position)
        {
            var x = position.X + CollisionBounds.X;
            var y = position.Y + CollisionBounds.Y;

            return new Box2(x, y, CollisionBounds.Width, CollisionBounds.Height);
        }

        public bool IsOnScreen => true;

        public void AddedToWorld(World world)
        {
            Active = true;
            Collidable = true;

            OnAddedToWorld(world);
        }

        protected virtual void OnAddedToWorld(World world) { }

        public void RemovedFromWorld(World world)
        {
            DealocateSprites();

            Game.EntityFactory.Release(this);
            OnRemovedFromWorld(world);
        }

        private void DealocateSprites()
        {
            foreach (var sprite in Sprites)
                Game.SpritePool.Deallocate(sprite);

            Sprites.Clear();
        }

        protected virtual void OnRemovedFromWorld(World world) { }
         
        public void Update(bool wasRewinding) 
        {
            if (HitTicks > 0)
                --HitTicks;

            foreach (var sprite in Sprites)
                sprite.Update();
            
            OnUpdate(wasRewinding);

            //If still active and it's a collider add to the collision list
            if (Active && Collidable)
            {
                if (Game.NewCollisionMapManager.IsMapCollider(Classification))
                    Game.World.AddGameMapCollider(this);

                if (Game.NewCollisionMapManager.IsEntityCollider(Classification))
                    Game.World.AddEntityCollider(this);
            }
        }

        protected virtual void OnUpdate(bool wasRewinding) { }

        /// <summary>
        /// Called when another Entity collides with us.
        /// </summary>
        /// <param name="collider">The Entity colliding with us.</param>
        /// <param name="time">The normaised time (0..1) the collision occurred.</param>
        public void Collide(Entity collider, float time)
        {
            if (Active && collider.Active && Collidable && collider.Collidable)
            {
                OnCollisionWithEntity(collider, time);
                //if (Active && collider.Active)
                    collider.OnCollisionWithEntity(this, time);
            }
        }

        protected virtual void OnCollisionWithEntity(Entity collider, float time) { }

        public virtual void OnCollisionWithMap(CollisionEdge entityCollisionEdge) { }

        public void Move() 
        {
            if (Math.Abs(Velocity.X) < 0.001f)
                Velocity.X = 0.0f;

            if (Math.Abs(Velocity.Y) < 0.001f)
                Velocity.Y = 0.0f;

            Position += Velocity;
        }

        public virtual void PostUpdate() { }

        public void Draw()
        {
            if (Visible == false)
                return;

            if (IsOnScreen)
            {
                foreach (var sprite in Sprites)
                {
                    TextureInfo textureInfo = null;
                    var layer = 0;// Layer;

                    if (sprite != null)
                    {
                        if (sprite.Visible == false)
                            continue;

                        textureInfo = sprite.TextureInfo;
                        layer = sprite.Layer;
                    }

                    if (textureInfo == null)
                        continue;

                    RenderStyle renderStyle = TransparentRenderStyle.Instance;
                    if (HitTicks > 0)
                        renderStyle = FlashRenderStyle.Instance;

                    var q = new Quad(Position, Blueprint.BaseSize);//, Direction);
                    Game.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, Color.White, renderStyle, layer);
                }
            }

            OnDraw();

            DrawDebugBounds();
        }

        [Conditional("USE_GEARSET")]
        void DrawDebugBounds()
        {
            var bounds = GetBoundingBox(Position);
            GS.ShowBoxOnce(new Vector3(bounds.Left, bounds.Bottom, 0), new Vector3(bounds.Right, bounds.Top, 0), Color.Red);

            //GS.ShowSphereOnce(new Vector3(Position, 0), bounds.Width * 0.5f, Color.Blue);

            //var renderStyle = TransparentRenderStyle.Instance;
            //var color = Color.Blue;
            //Game.Renderer.DrawLine(new Vector2(bounds.Left, bounds.Top), new Vector2(bounds.Right, bounds.Top), color, renderStyle);
            //Game.Renderer.DrawLine(new Vector2(bounds.Right, bounds.Top), new Vector2(bounds.Right, bounds.Bottom), color, renderStyle);
            //Game.Renderer.DrawLine(new Vector2(bounds.Right, bounds.Bottom), new Vector2(bounds.Left, bounds.Bottom), color, renderStyle);
            //Game.Renderer.DrawLine(new Vector2(bounds.Left, bounds.Bottom), new Vector2(bounds.Left, bounds.Top), color, renderStyle);
        }

        protected virtual void OnDraw() { }

        public virtual void BeforeRewind() { }
        public virtual void StateLoaded(World world, bool rewinding) { }

        public void Deactivate() 
        {
             Debug.Assert(Active);

            Active = false; 
            OnDeactivate();
        }

        protected virtual void OnDeactivate() { }

        protected void CreateParticleSystems(IEnumerable<ParticleEffect> effects)
        {
            foreach (var particleEffect in effects)
                CreateParticleSystem(particleEffect);
        }

        /// <summary>
        /// Creates a particle system
        /// </summary>
        /// <param name="particleEffect"></param>
        /// <returns></returns>
        protected ParticleSystem CreateParticleSystem(ParticleEffect particleEffect)
        {
            return CreateParticleSystem(particleEffect.Template.Name);
        }

        protected ParticleSystem CreateParticleSystem(string templateName)
        {
            var ps = Game.EntityFactory.Create<ParticleSystem>();
            if (ps == null)
                return null;

            ps.Position = Position;
            ps.TemplateName = templateName;
            Game.World.Add(ps);

            return ps;
        }

        public virtual void PlayRicochet()
        {
            Sound.Play(Game, "Ricochet", Position);
        }
    }
}