/*
 * Copyright 2008  Reg Whitton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
#if WINDOWS
using System.Collections;
#else
using Hashtable = System.Collections.Generic.Dictionary<object, object>;
#endif

namespace Transition.Eval 
{
	class Operation 
    {
	    readonly Type _type;
	    readonly Operator _op;
	    readonly Object _operand1;
	    readonly Object _operand2;
	    readonly Object _operand3;
	
		private Operation(Type type, Operator op, Object operand1, Object operand2, Object operand3)
        {
			_type = type;
			_op = op;
			_operand1 = operand1;
			_operand2 = operand2;
			_operand3 = operand3;
		}
	
		internal static Operation NopOperationfactory(Object operand) 
        {
			return new Operation(Operator.NoOperation.ResultType, Operator.NoOperation, operand, null, null);
		}
	
		internal static Object UnaryOperationfactory(Operator op, Object operand) 
        {
			ValidateOperandType(operand, op.OperandType);
	
			/*
			 * If values can already be resolved then return result instead of operation
			 */
			if (operand is decimal) 
				return op.Perform((decimal)operand, 0, 0);
			
			return new Operation(op.ResultType, op, operand, null, null);
		}
	
		internal static Object BinaryOperationfactory(Operator op, Object operand1, Object operand2) 
        {
			ValidateOperandType(operand1, op.OperandType);
			ValidateOperandType(operand2, op.OperandType);
	
			// If values can already be resolved then return result instead of operation
			if (operand1 is decimal && operand2 is decimal) 
				return op.Perform((decimal) operand1, (decimal) operand2, 0);
			
			return new Operation(op.ResultType, op, operand1, operand2, null);
		}
	
		internal static Object TenaryOperationFactory(Operator op, Object operand1, Object operand2, Object operand3) 
        {
			ValidateOperandType(operand1, Type.Boolean);
			ValidateOperandType(operand2, Type.Arithmetic);
			ValidateOperandType(operand3, Type.Arithmetic);
	
			// If values can already be resolved then return result instead of operation
			if (operand1 is decimal) 
				return ((decimal) operand1) != 0 ? operand2 : operand3;
			
			return new Operation(Type.Arithmetic, op, operand1, operand2, operand3);
		}
	
		internal decimal Eval(Hashtable variables) 
        {
			switch (_op.NumberOfOperands) 
            {
				case 3:
					return _op.Perform(EvaluateOperand(_operand1, variables), EvaluateOperand(_operand2, variables), EvaluateOperand(_operand3, variables));
				case 2:
					return _op.Perform(EvaluateOperand(_operand1, variables), EvaluateOperand(_operand2, variables), 0);
				default:
					return _op.Perform(EvaluateOperand(_operand1, variables), 0, 0);
			}
		}
	
		private static decimal EvaluateOperand(Object operand, Hashtable variables) 
        {
			if (operand is Operation) 
				return ((Operation) operand).Eval(variables);
			
            if (operand is String) 
            {
                if (variables != null && variables.ContainsKey(operand))
                {
				    var v = variables[operand];
				    if (!(v is decimal)) {
					    throw new InvalidOperationException("value '"+operand+"' is '"+v+"' which is not a decimal");
				    }

                    return (decimal)v;
                }
				
		        throw new InvalidOperationException("no value for variable \"" + operand + "\"");
			}
			
            return (decimal) operand;
		}
	
		/**
		 * Validate that where operations are combined together that the types are as expected.
		 */
		private static void ValidateOperandType(Object operand, Type type)
        {
			Type operandType;
	
			if (operand is Operation && (operandType = ((Operation) operand)._type) != type) 
				throw new InvalidOperationException("cannot use " + operandType.Name + " operands with " + type.Name + " operators");
		}
	
		public override String ToString() 
        {
			switch (_op.NumberOfOperands) 
            {
				case 3:
					return "(" + _operand1 + _op.Stringx + _operand2 + ":" + _operand3 + ")";
				case 2:
					return "(" + _operand1 + _op.Stringx + _operand2 + ")";
				default:
					return "(" + _op.Stringx + _operand1 + ")";
			}
		}
	}
}

