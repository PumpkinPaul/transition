using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a label control.
    /// </summary>
    public abstract class LabelControl : MenuControl
    {		
        /// <summary>Where the text is rendered in relation to the control's position.</summary>
        public Alignment Alignment;

        /// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;

        /// <summary>The size of the font.</summary>
        public Vector2 FontSize;

        public float Rotation;

		/// <summary>
		/// Initialises a new instance of a Label control.
		/// </summary>
		/// <param name="game">A reference to the game.</param>
		protected LabelControl(BaseGame game) : base(game)
		{
            Font = Theme.Current.GetFont(Theme.Fonts.Default);
            FontSize = Theme.Current.GetFontSize(Theme.FontSizes.Normal);
		}

        public override void Load(XElement element)
        {
            base.Load(element);

            Alignment = element.GetAttributeEnum("alignment", Alignment);
            Font = ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            Rotation = element.GetAttributeSingle("rotation", Rotation);
        }
    }
}
