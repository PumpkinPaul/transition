namespace Transition.Menus
{
    /// <summary>
    /// Represents the credits menu.
    /// </summary>
    public class CreditsMenu : Menu
    {
        /// <summary>
        /// Initialises a new instance of a CreditsMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public CreditsMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu() { }
    }
}
