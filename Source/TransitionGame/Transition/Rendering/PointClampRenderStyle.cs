using Microsoft.Xna.Framework.Graphics;
using Transition.ResourceManagers;

namespace Transition.Rendering
{
    /// <summary>
    /// A style to handle both ADDITIVE and normal ALPHA BLENDING using Point filter
    /// </summary>
    /// <remarks>
    /// NOTE - If we set the alpha of an item to 0 and RGB > 0 we get ADDITIVE blending. Normal pre multiplied alpha gives us normal ALPHA BLENDING.
    /// </remarks>
    public class PointClampRenderStyle : RenderStyle
    {
        public static PointClampRenderStyle Instance;

        public override RenderStyleType RenderStyleId => RenderStyleType.PointClamp;

        protected override void OnRegister(EffectManager effectManager)
        {
            Instance = this;
            Effect = effectManager.GetEffect("BasicEffect");
        }

        public override void Setup(GraphicsDevice graphicsDevice)
        {
            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
        }
    }
}
