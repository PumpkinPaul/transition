using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>Represents a base control that displays hints for controller actions.</summary>
    public class ActionHintControl : MenuControl
    {
        /// <summary>Where the text is rendered in relation to the control's position.</summary>
        /// <remarks>The image will always be with its centre at position.  
        /// BottomCentre, Centre and TopCentre options won't give very pleasant results for this control.</remarks>
        protected readonly Alignment Alignment;

        /// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;

        /// <summary>The size of the font.</summary>
        public Vector2 FontSize;

        /// <summary>The colour to tint the image with.</summary>
        /// <remarks>MenuControl.Tint will tint the text.</remarks>
        public Color ImageTint;

        /// <summary>The angle of rotation around the z-axis (in radians).</summary>
        public float ImageRotation;

        /// <summary>The size of the image.</summary>
        public Vector2 ImageSize;

        /// <summary>The size of the space between the button and the image.</summary>
        public float Spacer;

        /// <summary>The text to render.</summary>
        public string HintText;

        /// <summary>The id of the texture to render for the hint image.</summary>
        public TextureInfo TextureInfo;

        /// <summary>Initialises a new instance of a ButtonHintControl control.</summary>
        /// <param name="game">A reference to the game.</param>
        public ActionHintControl(BaseGame game) : base(game)
        {
            Alignment = Alignment.CentreLeft;
            Font = Theme.Font;
            FontSize = new Vector2(0.75f, 0.75f);
            ImageTint = Color.White;
            ImageSize = new Vector2(1.25f);
            Spacer = 0.2f;
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            ImageTint = element.GetAttributeColor("imageTint", ImageTint);
            ImageRotation = element.GetAttributeSingle("imageRotation", ImageRotation);
            ImageSize = element.GetAttributeVector2("imageSize", ImageSize);
            Spacer = element.GetAttributeSingle("spacer", Spacer);
            HintText = element.GetAttributeString("hintText", HintText);
            TextureInfo = ParseTexture(element, "textureInfo", TextureInfo);
        }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
        {
            DrawImage();
            DrawHintText(GetHintTextPosition());
        }

        /// <summary>
        /// Gets the position to draw the text at.
        /// </summary>
        /// <returns>The position of the text.</returns>
        private Vector2 GetHintTextPosition()
        {
            //Draw the text in relation to the image respecting the alignment property.  For this offset the position of the text
            var position = Position;
            switch (Alignment)
            {
                case Alignment.BottomLeft:
                case Alignment.CentreLeft:
                case Alignment.TopLeft:
                    position.X += (Spacer + (ImageSize.X / 2.0f));
                    break;

                case Alignment.BottomRight:
                case Alignment.CentreRight:
                case Alignment.TopRight:
                    position.X -= (Spacer + (ImageSize.X / 2.0f));
                    break;
            }

            return position;
        }

        /// <summary>
        /// Draw the image.
        /// </summary>
        private void DrawImage()
        {
            var tint = ImageTint;
            tint *= Overlay.Opacity;

            //Draw the image centred on position.
            var q = new Quad(Position, ImageSize, ImageRotation);
            Game.Renderer.DrawSprite(ref q, TextureInfo.TextureId, TextureInfo.SourceRect, tint, RenderStyle, Layer);
        }

        /// <summary>
        /// Draws the text.
        /// </summary>
        /// <param name="position">Where to draw the text.</param>
        private void DrawHintText(Vector2 position)
        {
            var tint = Tint;
            tint *= Overlay.Opacity;

            Game.Renderer.DrawString(Font, position, FontSize, HintText, Alignment, tint, RenderStyle, Layer);
        }
    }
}
