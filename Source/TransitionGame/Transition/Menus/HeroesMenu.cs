using System;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the high scores menu.
    /// </summary>
    public class HeroesMenu : Menu
    {
        //Controls...
        ScoreTableControl _scoreTableControl;
        ListControl _pageList;

        /// <summary>
        /// Initialises a new instance of a HeroesMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public HeroesMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {                  
            _scoreTableControl = GetControl<ScoreTableControl>("scoreTableControl");
            _scoreTableControl.ScoreTable = ((TransitionGame)Game).GetScoreTable(Difficulty.Easy);

            _pageList = (ListControl)ControlMap["pageList"];
            
            var pageCount = (int)Math.Ceiling((double)ScoreTable.MaxScoreItems / ScoreTableControl.PageSize);

            for (var i = 1; i <= pageCount; ++i)
            {
                _pageList.AddOption(i + " of " + pageCount);
            }

            _pageList.SelectedIndex = 0;
            _pageList.IncreaseValue += PageChanged;
            _pageList.DecreaseValue += PageChanged;
        }

        public ScoreTable ScoreTable
        {
            get { return _scoreTableControl.ScoreTable; }
            set { _scoreTableControl.ScoreTable = value; }
        }

        protected override void OnAddedToManager()
        {
            _scoreTableControl.ScoreTable = ((TransitionGame)Game).GetScoreTable(Difficulty.Normal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageChanged(object sender, EventArgs e)
        {
			if (_scoreTableControl.Visible)
				_scoreTableControl.PageIndex = _pageList.SelectedIndex;
				
            RefreshScoreTable();
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshScoreTable()
        {
            _scoreTableControl.ScoreTable = ((TransitionGame)Game).GetScoreTable(Difficulty.Normal);
        }

        /// <summary>
        /// Set the current page based on the current score item
        /// </summary>
        /// ///<remarks>e.g. show the page that the players score apears on (or page 1)</remarks>
        public void SetPage()
        {
	        _pageList.SelectedIndex = _scoreTableControl.SetPageIndex();
				
            RefreshScoreTable();
        }
    }
}
