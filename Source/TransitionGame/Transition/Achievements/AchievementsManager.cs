using System;
using System.Collections.Generic;

namespace Transition.Achievements
{
    /// <summary>
    /// Manages the cameras.
    /// </summary>
    public class AchievementsManager
    {
        readonly List<Achievement> _achievements = new List<Achievement>();

        public void Register(Achievement achievement)
        {
            if (achievement == null)
                throw new ArgumentNullException(nameof(achievement));

            _achievements.Add(achievement);
        }

        public void Update()
        {
            //TODO: Transition
            var game = TransitionGame.Instance;
            var profile = game.ActivePlayer.Profile;
            var difficulty = game.ActivePlayer.Difficulty;

            foreach(var achievement in _achievements)
            {
                var achievement1 = achievement;
                if (profile.CompletedAchievements.Exists(x => x.Guid == achievement1.Guid && x.Difficulty == difficulty))
                    continue;

                if (achievement.IsGainedThisSession(difficulty) == false)
                    continue;
                
                var completedAchievement = new CompletedAchievement(achievement.Guid, difficulty);
                profile.CompletedAchievements.Add(completedAchievement);
                profile.RequiresSave = true;

                game.PlaySound("AchievementUnlocked");
            }
        }
    }
}
