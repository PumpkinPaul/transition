namespace Transition.Animations
{
    /// <summary>
    /// Describes a Sprite's appearance.
    /// </summary>
    public interface IAppearance 
    {
        /// <summary>
        /// Set the image or animation of a sprite.
        /// </summary>
        /// <param name="target"></param>
        /// <returns>True if we need to recurse or false to simply advance the sequence number.</returns>
        bool ToSprite(Sprite target);
    }
}

