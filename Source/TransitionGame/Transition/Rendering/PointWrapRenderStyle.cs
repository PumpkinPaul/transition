using Microsoft.Xna.Framework.Graphics;
using Transition.ResourceManagers;

namespace Transition.Rendering
{
    /// <summary>
    /// A style to draw dotted
    /// </summary>
    /// <remarks>
    /// </remarks>
    public class PointWrapRenderStyle : RenderStyle
    {
        public static PointWrapRenderStyle Instance;

        public override RenderStyleType RenderStyleId => RenderStyleType.PointWrap;

        protected override void OnRegister(EffectManager effectManager)
        {
            Instance = this;
            Effect = effectManager.GetEffect("BasicEffect");
        }

        public override void Setup(GraphicsDevice graphicsDevice)
        {
            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.SamplerStates[0] = SamplerState.PointWrap;
        }
    }
}
