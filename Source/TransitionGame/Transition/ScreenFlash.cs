using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Data for a screen flash.
    /// </summary>
    public class ScreenFlash
    {
		public Color Color;
		public int Ticks;
        public int Duration;

		/// <summary>
		/// Creates a new instance of a ScreenFlash.
		/// </summary>
		public ScreenFlash(Color color, int duration)
		{
			Color = color;
			Ticks = duration;
            Duration = duration;
		}
    }
}
