using Nuclex.Input;
using Transition.Menus;

namespace Transition.Input
{
    /// <summary>
    /// A base class for game input control methods (mouse, gamepad, keys, touch)
    /// </summary>
    public abstract class InputMethod
    {
        /// <summary>The number of ticks the input is held for before registering as a button press / stick or d-pad move.</summary>
        protected const int MaxInputHeldTicks = 15;

        /// <summary>How many ticks the 'previous control' action has been held for.</summary>
        protected int PreviousControlHeldTicks;

        /// <summary>How many ticks the 'next control' action has been held for.</summary>
        protected int NextControlHeldTicks;

        /// <summary>How many ticks the increase value action has been held for.</summary>
        protected int IncreaseValueHeldTicks;

        /// <summary>How many ticks the decrease value action has been held for.</summary>
        protected int DecreaseValueHeldTicks;

        public ExtendedPlayerIndex PlayerIndex { get ; protected set; } 

        public abstract bool SupportsMouse { get; }
        public abstract bool SupportsKeyboard { get; }
        public abstract bool SupportsGamepad { get; }

        public abstract bool SupportsVibration { get; }

        public abstract bool CanChangeInputMethodIndex { get ; set; }

        public abstract bool IsBeingUsed();

        public abstract GameInput GatherGameInput(ExtendedPlayerIndex playerIndex, PlayerProfile playerProfile);
        public abstract MenuInput GatherMenuInput(MenuManager menuManager, ExtendedPlayerIndex playerIndex, PlayerProfile playerProfile);

        protected bool HandleHeldAction(ref MenuInput menuInput, bool actionHeld, bool action, ref int heldTicks)
        {
            if (actionHeld)
			{
				++heldTicks;
				if (heldTicks >= MaxInputHeldTicks)
				{
					heldTicks = 0;
					action = true;
				}
			}
			else
				heldTicks = 0;

            return action;
        }
    }
}
