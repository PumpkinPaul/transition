using System.Text;
using System.Xml.Linq;
using Transition.Extensions;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a label control for displaying floating point values.
    /// </summary>
    public class FloatLabelControl : LabelControl
    {		
        float _value;
        
        /// <summary>The text to render.</summary>
        public float Value 
        {
            get { return _value; }
            set 
            {
                _stringBuilder.Length = 0;
                _stringBuilder.AppendNumber(value);
                _value = value;
            }
        }

        private readonly StringBuilder _stringBuilder = new StringBuilder();

		/// <summary>
		/// Initialises a new instance of a Label control.
		/// </summary>
		/// <param name="game">A reference to the game.</param>
        public FloatLabelControl(BaseGame game) : base(game) { }

        public override void Load(XElement element)
        {
            base.Load(element);

            Value = element.GetAttributeSingle("value");
        }

        public override Rect Bounds
		{
			get { return Font.GetBounds(Position, FontSize, _stringBuilder, Alignment); }
		}

        /// <summary>
        /// Allows a derived control to draw itself.
		/// </summary>
		protected override void OnDraw()
		{
            var tint = Tint;
            tint *= Overlay.Opacity;

            Game.Renderer.DrawNumber(Font, Position, FontSize, Value, Alignment, tint, RenderStyle, Layer);
		}
    }
}
