﻿#region File Description
//-----------------------------------------------------------------------------
// DebugManager.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace Transition.GameDebugTools
{
    /// <summary>
    /// DebugManager class that holds graphics resources for debug
    /// </summary>
    public class DebugManager : DrawableGameComponent
    {
        // the name of the font to load
        private readonly string _debugFont;

        /// <summary>
        /// Gets a sprite batch for debug.
        /// </summary>
        public SpriteBatch SpriteBatch { get; private set; }

        /// <summary>
        /// Gets white texture.
        /// </summary>
        public Texture2D WhiteTexture { get; private set; }

        /// <summary>
        /// Gets SpriteFont for debug.
        /// </summary>
        public SpriteFont DebugFont { get; private set; }

        public Color OverlayColor = new Color(0,0,4,210);// Color.White * 0.15f;

        public DebugManager(Game game, string debugFont) : base(game)
        {
            // Added as a Service.
            Game.Services.AddService(typeof(DebugManager), this);
            this._debugFont = debugFont;

            // This component doesn't need be call neither update nor draw.
            this.Enabled = false;
            this.Visible = false;
        }

        protected override void LoadContent()
        {
            //Load debug content.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            DebugFont = Game.Content.Load<SpriteFont>(_debugFont);

            //Create white texture.
            WhiteTexture = new Texture2D(GraphicsDevice, 1, 1);
            var whitePixels = new [] { Color.White };
            WhiteTexture.SetData(whitePixels);

            base.LoadContent();
        }
    }
}