using System;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the start menu.
    /// </summary>
    public class NewGameMenu : Menu
    {
        private const int DefaultShipHealth = -1;

        public event EventHandler StartGame;

        ListControl _shipList;
        RepeaterControl _statsRepeater;
        RepeaterControl _characterRepeater;

        ListControl _difficultyList;

        bool _loadOptions;

        /// <summary>
        /// Initialises a new instance of a StartMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
        public NewGameMenu(BaseGame game, string title) : base(game, title) { }

        protected override void OnConfigureMenu()
        {
            _statsRepeater = GetControl<RepeaterControl>("statsRepeater");
            _statsRepeater.DataSource = Game.EntityBlueprints.ShipBlueprints();

            _characterRepeater = GetControl<RepeaterControl>("characterRepeater");
            _characterRepeater.DataSource = Game.EntityBlueprints.ShipBlueprints();

            _shipList = GetControl<ListControl>("shipList");
            _shipList.ClearOptions();
            foreach (var shipBlueprint in Game.EntityBlueprints.ShipBlueprints())
                _shipList.AddOption(shipBlueprint.Classification);

            _shipList.SelectedIndex = 0;

            _shipList.IncreaseValue += (sender, args) =>
            {
                _statsRepeater.StartTransition("left");
                _characterRepeater.StartTransition("left");
            };

            _shipList.DecreaseValue += (sender, args) =>
            {
                _statsRepeater.StartTransition("right");
                _characterRepeater.StartTransition("right");
            };

            _shipList.TextButtonClicked += ShipListClick;
            _shipList.Click += ShipListClick;

            _difficultyList = GetControl<ListControl>("difficultyList");
            _difficultyList.AddOption($"Play {Difficulty.Easy}");
            _difficultyList.AddOption($"Play {Difficulty.Normal}");
            _difficultyList.AddOption($"Play {Difficulty.Hard}");
            _difficultyList.SelectedIndex = 1;
            _difficultyList.TextButtonClicked += DifficultyListClick;
            _difficultyList.Click += DifficultyListClick;
        }

        /// <summary>
        /// Occurs when a menu is added to the MenuManager.
        /// </summary>
        protected override void OnAddedToManager()
        {
            if (_loadOptions == false)
            {
                SetControlValuesFromOptions();
                _loadOptions = true;
            }

            _shipList.SetSelectedItemByText(Game.ActivePlayer.Profile.SelectedShipName);
        }

        /// <summary>
        /// Sets the values of the controls form the game options.
        /// </summary>
        private void SetControlValuesFromOptions()
        {
            _difficultyList.SelectedIndex = (int)Game.GameOptions.Player1Difficulty;
        }

        protected override void OnClosed()
        {
            base.OnClosed();

            SaveSelectedShip();
        }

        /// <summary>
        /// Event handler for the play button click.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The data for the event.</param>
        void ShipListClick(object sender, EventArgs e)
        {
            PlayGame();
        }

        /// <summary>
        /// Event handler for the play button click.
        /// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The data for the event.</param>
		void DifficultyListClick(object sender, EventArgs e)
        {
            PlayGame();
        }

        void PlayGame()
        {
            //Brand new game
            Game.GameOptions.Player1Difficulty = (Difficulty)_difficultyList.SelectedIndex;

            Game.InitialCheckpoint = new Checkpoint
            {
                ShipName = SelectedShipName,
                Level = 1,
                RagePower = 1,
                Difficulty = Game.GameOptions.Player1Difficulty,
                Health = DefaultShipHealth
            };

            var handler = StartGame;
            handler?.Invoke(this, EventArgs.Empty);
        }

        string SelectedShipName
        { 
            get
            {
                foreach (var shipBlueprint in Game.EntityBlueprints.ShipBlueprints())
                {
                    if (shipBlueprint.Classification != _shipList.SelectedText)
                        continue;

                    return shipBlueprint.Name;
                }

                throw new InvalidOperationException("Unknown ship:" + _shipList.SelectedText);
            }
        }

        private void SaveSelectedShip()
        {
            var shipName = SelectedShipName;

            if (Game.ActivePlayer.Profile.SelectedShipName == shipName) 
                return;

            Game.ActivePlayer.Profile.RequiresSave = true;
            Game.ActivePlayer.Profile.SelectedShipName = shipName;
            Game.ActivePlayer.Profile.Save(Game);
        }
    }
}
