using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class LabelSelector : Selector
    {
        public LabelSelector(SelectionTool selectionTool) : base(selectionTool) { }

        public override int SelectionCount { get { return LevelEditorState.SelectedLabelIds.Count; } } 

        public override SelectionTool.Feature MouseJustDownOverCanvas()
        {
            //Determine if we should move current selection or begin a new drag selection operation
            var selectionBounds = LevelEditor.GetLabelSelectionBounds();

            return selectionBounds.Contains(LevelEditor.WorldPosition) ? SelectionTool.Feature.Move : SelectionTool.Feature.Drag;
        }
        
        public override void Moving(Vector2 delta)
        {
            //Move all the selected tiles
            foreach (var labelId in LevelEditorState.SelectedLabelIds)
            {
                var tile = LevelEditor.GetLabelByIndex(labelId);

                tile.Position += delta;
            }                
        }
       
        public override bool DragCompleted(bool isShiftDown, Rect selectionBox)
        {
            var oldSelectedTiles = new List<int>(LevelEditorState.SelectedLabelIds);

            //if (KeyboardHelper.IsKeyPressed(Keys.LeftShift) == false)
            if (isShiftDown == false)
                LevelEditorState.SelectedLabelIds.Clear();

            //Select all the things in the drag area
            for (var index = 0; index < LevelEditor.Course.Labels.Count; index++)
            {
                var label = LevelEditor.GetLabelByIndex(index);

                //All corners have to be in selection area
                var labelBounds = LevelEditor.Course.LabelFont.GetBounds(label.Position, label.FontSize, label.Text, label.Alignment);

                var obb2 = new Obb2(labelBounds, label.Position, label.Rotation);

                if (selectionBox.Contains(obb2) == false)
                    continue;

                if (LevelEditorState.SelectedLabelIds.Contains(index) == false)
                    LevelEditorState.SelectedLabelIds.Add(index);
            }

            var firstNotSecond = oldSelectedTiles.Except(LevelEditorState.SelectedLabelIds).ToList();
            var secondNotFirst = LevelEditorState.SelectedLabelIds.Except(oldSelectedTiles).ToList();

            return firstNotSecond.Count + secondNotFirst.Count > 0;
        }

        public override Vector4 GetAcumulatedColorVector()
        {
            //Set the color of the object to the average of all selected
            if (SelectionCount == 0)
                return base.GetAcumulatedColorVector();
            
            var color = Vector4.Zero;
            foreach (var labelId in LevelEditorState.SelectedLabelIds)
            {
                var label = LevelEditor.GetLabelByIndex(labelId);
                color += label.Color.ToVector4();
            }

            return color;
        }
    }
}
