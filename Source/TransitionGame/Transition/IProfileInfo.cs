namespace Transition
{
    /// <summary>
    /// A readonly interface to the ProfileInfo class.
    /// </summary>
    public interface IProfileInfo
    {
        /// <summary>Gets the display name of the profile.</summary>
        /// <remarks>If the profile is signed in gets the gamertag; otherwise gets the built in name (e.g. player 2).</remarks>
        string DisplayName { get; }

        /// <summary>Gets the id of the texture to use for the gamer pic of the profile.</summary>
        /// <remarks>If the profile is signed in gets the gamertag; otherwise gets the default not signed in texture.</remarks>
        TextureInfo GamepicTextureInfo { get; }

        /// <summary>Gets wether the profile is signed in.</summary>
        bool IsSignedIn { get; }
    }
}
