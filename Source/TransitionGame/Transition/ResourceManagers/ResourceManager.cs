using System.IO;
using System.Xml.Linq;
using Transition.Extensions;
using Transition.Util;

namespace Transition.ResourceManagers
{
    /// <summary>
    /// A base class for resource managers.
    /// </summary>
    public abstract class ResourceManager : IResourceManager
    {
        public static string ResourcesFolderName = "Resources";

        public abstract string FolderName { get; }
        public virtual string SearchPattern { get { return "*.xml"; } }

        public void Reload()
        {
            RefreshContent();
            LoadContent();
        }

        public void RefreshContent()
        {
            var gameSourceDirectoryInfo = new DirectoryInfo("./").GetParent(3);

            var scriptSourcePath = Path.Combine(gameSourceDirectoryInfo.FullName, ResourcesFolderName, FolderName);
            RefreshFolder(scriptSourcePath, string.Empty);  
        }

        void RefreshFolder(string folder, string subFolder)
        {
            foreach (var sourceFilename in Directory.GetFiles(folder, SearchPattern))
            {
                var sourceFileInfo = new FileInfo(sourceFilename);
                var destinationFileName = Path.Combine(Platform.ContentPath, FolderName, subFolder, sourceFileInfo.Name);

                File.Copy(sourceFilename, destinationFileName, true);
            }

            foreach(var sub in Directory.GetDirectories(folder))
            {
                var di = new DirectoryInfo(sub);
                var sf = Path.Combine(subFolder, di.Name);
                RefreshFolder(sub, sf);
            } 
        }

        public virtual void LoadContent() { }

        protected void ParseCommonElements(XElement parent)
        {
            foreach (var element in parent.Elements())
            {
                ParseCommonElement(element);
            }
        }

        protected bool ParseCommonElement(XElement element)
        {
            if (element.Name.LocalName == "property")
            {
                var name = element.GetAttributeString("name");
                var value = element.GetAttributeString("value");

                ScriptManager.AddVariable(name, value);
                return true;
            }

            return false;
        }
    }
}
