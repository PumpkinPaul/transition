Add files to this folder to create mappings for Direct Input Gamepads to XInput (Xbox360) Gamepads.
You currently can create mappings for buttons and the two analogue sticks along with axis inversion and deadzone handling.
A maximum of 4 Xbox 360 gamepads and 4 Direct Input gamepads are supported (giving 8 in total)
Mapping files will be loaded in order and matched to corresponding Direct Input gamepads so name them accordingly.
e.g
1.mapping -> first Direct Input gamepad
2.mapping -> second Direct Input gamepad
3.mapping -> third Direct Input gamepad
4.mapping -> fourth Direct Input gamepad

Here's is an exmaple of a mapping from a Playstation 2 game pad recognised as a "PS Vibration Feedback Controller" in Game Controllers...

<?xml version="1.0"?>
<GamePadMapping xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<!-- 
		Mappings for buttons
		--------------------
		Buttons range from 0 to 63. Use -1 if your gamepad has no equivalent Xbox 360 gamepad button. 
	-->

	<A>2</A>
	<B>1</B>
	<X>3</X>
	<Y>0</Y>
	<LeftShoulder>6</LeftShoulder>
	<RightShoulder>7</RightShoulder>
	<Back>8</Back>
	<Start>11</Start>
	<LeftStick>9</LeftStick>
	<RightStick>10</RightStick>
	<BigButton>-1</BigButton>
	
	<-- 
		Mappings for analogue sticks
		----------------------------
		Axis values are binary bitflags ranging from 2^0 to 2^23:
			e.g 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, etc... , up until 8388608

		Some axes are inverted compared to the Xbox 360 pads so spcify 'true' for those
		Custom deadzone processing can be specified using the following enums: None, IndependentAxes, Circular 
	-->
	<LeftStickXAxis>1</LeftStickXAxis>
	<LeftStickYAxis>2</LeftStickYAxis>
	<LeftStickXAxisInvert>false</LeftStickXAxisInvert>
	<LeftStickYAxisInvert>false</LeftStickYAxisInvert>
	<LeftStickDeadZone>IndependentAxes</LeftStickDeadZone>
	
	<RightStickXAxis>16384</RightStickXAxis>
	<RightStickYAxis>4</RightStickYAxis>
	<RightStickXAxisInvert>false</RightStickXAxisInvert>
	<RightStickYAxisInvert>true</RightStickYAxisInvert>
	<RightStickDeadZone>IndependentAxes</RightStickDeadZone>
</GamePadMapping>

