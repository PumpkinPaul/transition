using Transition.Animations;

namespace Transition.Actors
{
    public class Animated : Actor
    {
        /// <summary>
        /// Creates a new instance of an 'animated' actor.
        /// </summary>
        /// <remarks>All actors can be animated but this is just a dumb thing the ccan can control for animations, cut scenes, etc.</remarks>
        /// <param name="game">An instance of the game.</param>
        public Animated(TransitionGame game) : base(game) 
        { 
            _horizontalConstrainBehaviour = ConstrainBehaviour.None;
			_verticalConstrainBehaviour = ConstrainBehaviour.None;
        }

        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.Animated; }
        }

        public void SetAppearance(int layerId, IAppearance appearance)
        {
            Sprites[layerId].SetAppearance(appearance);
        }

        public void SetAnimation(int layerId, Animation animation)
        {
            Sprites[layerId].SetAnimation(animation);
        }
    }
}
