using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Transition._____Temp.StarGuard
{
    public class StarGuardMapToTileMapConvertor
    {
        public static void ConvertAllMaps()
        {
            //CONVERTS THE 'STAR GUARD' XML MAPS TO 'TILED' MAPS.
            const string inputFolder = @"C:\Users\PAUL~1.CUN\AppData\Local\Temp\star_guard_0.97_pc\star_guard_0.97_pc\data\";
            const string outputFolder = @"C:\Users\paul.cunningham\Desktop";

            var converter = new StarGuardMapToTileMapConvertor();
            for (var i = 1; i < 10; i++)
                converter.Convert(Path.Combine(inputFolder, i + ".xml"), Path.Combine(outputFolder, i + ".tmx"));
        }

        int[] _map;


        const string XmlFooter = "  </data>\n </layer>\n</map>\n";

        public void Convert(string inFileName, string outFilename)
        {
            //Load the map with data
            var document = XDocument.Load(inFileName);
            var mapElement = document.Root;

            var mapHeight = mapElement.Elements("tilerow").Count();
            var mapWidth = 0;
            
            foreach (var rowElement in mapElement.Elements("tilerow"))
            {
                var colCount = rowElement.Elements("tilecol").Count();
                if (colCount > mapWidth)
                    mapWidth = colCount;
            }

            string xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<map version=\"1.0\" orientation=\"orthogonal\" renderorder=\"right-down\" width=\"" + mapWidth + "\" height=\"" + mapHeight + "\" tilewidth=\"64\" tileheight=\"64\" backgroundcolor=\"#ff00ff\" nextobjectid=\"1\">\n" +
                " <tileset firstgid=\"1\" name=\"StarGuard\" tilewidth=\"64\" tileheight=\"64\">\n" +
                "  <image source=\"StarGuard.png\" width=\"1024\" height=\"1024\"/>\n" +
                " </tileset>\n" +
                " <layer name=\"Tile Layer 1\" width=\"" + mapWidth + "\" height=\"" + mapHeight + "\">\n" +
                "  <data>\n";

            Debug.WriteLine("Map dimensions {0},{1}", mapWidth, mapHeight);

            _map = new int[mapWidth * mapHeight];

            var tileY = 0;

            foreach (var rowElement in mapElement.Elements("tilerow"))
            {
                var tileX = 0;

                foreach (var colElement in rowElement.Elements("tilecol"))
                {
                    var mapId = (mapWidth * tileY) + tileX;

                    var tileType = colElement.GetValueInt32();

                    if (tileType == 0)
                        tileType = 255;
                    else if (tileType == 1)
                        tileType = 255;
                    else if (tileType == 2)
                        tileType = 2;
                    else
                        tileType += 160;

                    //Put the value into the map;
                    _map[mapId] = tileType;

                    tileX++;
                }

                tileY++;
            }

            var stringBuilder = new StringBuilder();
            stringBuilder.Append(xmlHeader);
            foreach (var tileType in _map)
                stringBuilder.AppendFormat("   <tile gid=\"{0}\"/>\n", tileType);

            stringBuilder.Append(XmlFooter);

            File.WriteAllText(outFilename, stringBuilder.ToString(), Encoding.UTF8);
        }
    }
}
