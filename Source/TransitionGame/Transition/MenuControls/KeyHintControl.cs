using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control that displays hints for keyboard actions.
    /// </summary>
    public class KeyHintControl : ActionHintControl
    {		
        public Vector2 TextFontSize;
        public Vector2 HintFontSize;

        /// <summary>The text to render on the button.</summary>
        public string Text;
        public Color TextTint;
		
		/// <summary>
        /// Initialises a new instance of a ButtonHintControl control.
		/// </summary>
		/// <param name="game">A reference to the game.</param>
        public KeyHintControl(BaseGame game) : base(game)
		{
            HintFontSize = Theme.FontSize * 0.8f;
            TextFontSize = Theme.FontSize * 0.75f;
            ImageSize = new Vector2(2.85f, 1.85f);
            Spacer = -0.1f;
		}

        public override void Load(XElement element)
        {
            base.Load(element);

            TextFontSize = element.GetAttributeVector2("textFontSize", TextFontSize);
            HintFontSize = element.GetAttributeVector2("hintFontSize", HintFontSize);
            Text = element.GetAttributeString("text", Text);
            TextTint = element.GetAttributeColor("textTint", TextTint);
        }

        /// <summary>
        /// Allows a derived control to draw itself.
		/// </summary>
		protected override void OnDraw()
		{
            base.OnDraw();

            DrawText();
        }

        /// <summary>
        /// Draws the hint text.
        /// </summary>
        private void DrawText()
        {
            var tint = TextTint;
            tint *= Overlay.Opacity;

            var position = Position;
            position.Y -= 0.05f;
            Game.Renderer.DrawString(Font, position, TextFontSize, Text, Alignment.Centre, tint, RenderStyle, Layer + 1);
        }
    }
}
