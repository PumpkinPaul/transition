using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Emits particles from a rectangular shape.
    /// </summary>
    public class ParticleRectEmitter : IParticleEmitter
    {
        private float _width;
        private float _height;

		/// <summary>
        /// Sets the radii of the emitter rings.
        /// </summary>
		/// <param name="data">The data required for configuring the emitter.</param>
        public void SetData(string data)
	    {
			var dimensions = data.Split(',');
            _width = ConversionHelper.ToSingle(dimensions[0].Trim());
            _height = ConversionHelper.ToSingle(dimensions[1].Trim());
	    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void SetBounds (float width, float height)
        {
            _width = width;
            _height = height;
        }

        /// <summary>
        /// Gets the position of the next particle to be emitted
        /// </summary>
        /// <returns>Returns a Vector2 defining the position of the particle.</returns>
        public Vector2 GetPosition()
        {
            return RandomHelper.Random.GetVector(-_width / 2.0f, _width / 2.0f, -_height / 2.0f, _height / 2.0f);
        }

        public float GetDirection(ref Vector2 emitterPosition, ref Vector2 particlePosition)
        {
            return RandomHelper.Random.GetFloat(MathHelper.TwoPi);
        }
    }
}