﻿using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Explosive Block enemy
    /// </summary>
    [ProtoContract]
    public class ExplosiveBlock : Entity
    {
        const int MaxCountdownTicks = 60;
        const int MaxHiddenTicks = 120;

        public enum Phase
        {
            Normal,
            Countdown,
            Hidden
        }

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        int _countdownTicks;

        [ProtoMember(2)]
        Phase _phase;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public ExplosiveBlock(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.ExplosiveBlock;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _countdownTicks = 0;
            _phase = Phase.Normal;
            
            Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-12.animation"));

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();

            switch (_phase)
            {
                case Phase.Normal:
                case Phase.Countdown:
                    Game.GameMap.SetTileTransparentSolid(Position);
                    break;

                case Phase.Hidden:
                    Game.GameMap.SetTileEmpty(Position);
                    break;
            }
        }

        protected override void OnAddedToWorld(World world)
        {
            BeginNormalPhase();
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            switch (_phase)
            {
                case Phase.Normal:
                    break;

                case Phase.Countdown:
                    UpdateCountdownPhase();
                    break;

                case Phase.Hidden:
                    UpdateHiddenPhase();
                    break;
            }
        }

        void BeginNormalPhase()
        {
            _phase = Phase.Normal;
            Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-12.animation"));
            Visible = true;
            Game.GameMap.SetTileTransparentSolid(Position);
        }

        void BeginCountDownPhase()
        {
            _phase = Phase.Countdown;
            Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-12.primed.animation"));
            _countdownTicks = MaxCountdownTicks;

            Sound.Play(Game, "ExplosiveBlockPing", Position);
        }

        void BeginHiddenPhase()
        {
            _phase = Phase.Hidden;
            _countdownTicks = MaxHiddenTicks;
            Visible = false;
            Game.GameMap.SetTileEmpty(Position);

            var ps = Game.EntityFactory.Create<ParticleSystem>();
            if (ps != null)
            {
                ps.Position = Position;
                ps.TemplateName = "ExplosiveBlockExplosion";
                Game.World.Add(ps);
            }

            Sound.Play(Game, Blueprint.ExplosionSfx, Position);
        }

        void UpdateCountdownPhase()
        {
            _countdownTicks--;

            if (_countdownTicks > 0)
                return;

            BeginHiddenPhase();
        }

        void UpdateHiddenPhase()
        {
            _countdownTicks--;

            if (_countdownTicks > 0)
                return;

            //Ensure we wouldn't overlap the player!
            var player = Game.World.Get<Player>(EntityKeys.Player);
            if (GetBoundingBox().Overlaps(player.GetBoundingBox()))
            {
                //Overlap - so wait a bit and try again!
                _countdownTicks = 1;
                return;
            }

            BeginNormalPhase();
        }

        protected override void OnCollisionWithEntity(Entity collider, float time)
        {
            switch (collider.Classification)
            {
                case EntityClassification.Player:
                    if (_phase == Phase.Normal)
                        BeginCountDownPhase();
                    break;
            }
        }
    }
}