using Microsoft.Xna.Framework;
using Transition.Actors;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public class LevelCompleteLabel : Label
    {
        public Vector2 StartPosition;
        public Vector2 TargetPosition;
        public Vector2 EndPosition;
        
        /// <summary>
        /// Creates a new instance of a LevelCompleteLabel.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public LevelCompleteLabel(TransitionGame game) : base(game) { }

        protected override void OnUpdate(GameTime gameTime)
        {
            _scale.X += 0.1f;
            _scale.Y += 0.1f;

            base.OnUpdate(gameTime);
        }
    }
}
