using System;

namespace Transition.GamePhases
{
    /// <summary>
    /// Abstract base class for game phases (level complete, game won, etc)
    /// </summary>
    public abstract class GamePhase
    {						
        protected BaseGame Game;
        			
		/// <summary>The number of ticks since the state was created.</summary>
		protected int ElapsedTicks;
		
        protected GamePhase(BaseGame game)
        {
            if (game == null)
                throw new ArgumentNullException(nameof(game));

            Game = game;
        }

		public void Create()
		{
			ElapsedTicks = 0;
			
			OnCreate();
		}		
		
		public void Update()
		{
			ElapsedTicks++;

			OnUpdate();
		}		
		
		public void Render()
		{
			OnRender();
		}		
		
		public void Destroy()
		{
			OnDestroy();
		}

		protected virtual void OnCreate() { }		
		protected virtual void OnUpdate() { }		
		protected virtual void OnRender() { }		
		protected virtual void OnDestroy() { }		
    }
}

