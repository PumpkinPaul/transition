#if !WINDOWS_PHONE
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.PostProcess
{
    /// <summary>
    /// Renders a barrel distprtion / crt bulge / fish eye effect.
    /// </summary>
    public class BarrelDistortionProcessor : BaseProcessor
    {
        public float CrtStrength { get; set; }
        public float Bulge { get; set; }

        private EffectParameter _viewportParameter;
        private EffectParameter _crtStrengthParameter;
        private EffectParameter _bulgeParameter;

        /// <summary>
        /// Initialises a new instance of a <see cref="BarrelDistortionProcessor">BarrelDistortionProcessor</see>.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public BarrelDistortionProcessor(BaseGame game) : base(game) 
        { 
            CrtStrength = 0.05f;
            Bulge = 0.96f;
        }

        /// <summary>
        /// Occurs when a derived effect needs to load content.
        /// </summary>
        protected override void OnLoadContent()
        {
            Effect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/BarrelDistortion"));

            _viewportParameter = Effect.Parameters["Viewport"];
            _crtStrengthParameter = Effect.Parameters["CrtStrength"];
            _bulgeParameter = Effect.Parameters["Bulge"];
        }

        /// <summary>
        /// Occurs when a derived effect needs to load content.
        /// </summary>
        protected override void OnUnloadContent()
        {
            Effect.Dispose();
        }

        /// <summary>
        /// Draws the post processing effect.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="deltaTime">Time passed since the last frame.</param>
        /// <param name="texture">The texture to draw.</param>
        /// <param name="renderTarget"></param>
        protected override bool OnDraw(SpriteBatch spriteBatch, float deltaTime, Texture2D texture, RenderTarget2D renderTarget)
        {
            //Less than == 0 what's the point!
            if (CrtStrength <= 0.0f)
                return false;

            _viewportParameter.SetValue(new Vector2(Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height));
            _crtStrengthParameter.SetValue(CrtStrength);
            _bulgeParameter.SetValue(Bulge);
            
            return base.OnDraw(spriteBatch, deltaTime, texture, renderTarget);
        }
    }   
}
#endif