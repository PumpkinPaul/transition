namespace Transition
{
    /// <summary>
    /// .
    /// </summary>
    public class NoMoreAliensLevelAction : LevelAction
    {
        public override void Execute(LevelManager levelManager)
        {
            levelManager.NoMoreAliens();
        }
    }
}