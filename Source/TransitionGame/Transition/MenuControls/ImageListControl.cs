using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// A control that displays a list of images representing options.
    /// </summary>
    public class ImageListControl : MenuControl
    {
        public Alignment Alignment = Alignment.Centre;
        public FlowStyle FlowStyle = FlowStyle.Horizontal;
        public Color TextTint;
        public Color NormalTint;
        public bool ImageGetsFocusColour = true;
        public bool TextGetsFocusColour = true;
        public bool ImageGetFocusStrobe;
        public bool ShowText;
        public float NormalImageScale = 1.0f;
        public float LargeImageScale = 1.5f;
        public int MaxPulseTicks = 25;
        public float ImageSpacer = 1.0f;
        public Color SelectedItemTint;
        public Vector2 FontSize = Vector2.One;

        /// <summary></summary>
        readonly List<TextureInfo> _images = new List<TextureInfo>();
        readonly List<Color> _colours = new List<Color>();
        readonly List<bool> _enables = new List<bool>();
        readonly List<string> _text = new List<string>();
        readonly List<string> _helpText = new List<string>();
        readonly List<int> _pulses = new List<int>();

        /// <summary></summary>
        int _selectedIndex = -1;
        int _hoverIndex = -1;
        int _mouseDownHoverIndex = -1;

        private int _pulseInc = 1;

        public override bool AutoTabIndex { get { return true; } }

        /// <summary>
        /// Initialises a new instance of an ImageListControl. 
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public ImageListControl(BaseGame game) : base(game) { }
 
        public override void Load(XElement element)
        {
            base.Load(element);

            Alignment = element.GetAttributeEnum("alignment", Alignment);
            TextTint = element.GetAttributeColor("textTint", TextTint);
            ShowText = element.GetAttributeBool("showText", ShowText);
            ImageGetsFocusColour = element.GetAttributeBool("imageGetsFocusColour", ImageGetsFocusColour);
            ImageGetFocusStrobe = element.GetAttributeBool("imageGetFocusStrobe", ImageGetFocusStrobe);
            ImageSpacer = element.GetAttributeSingle("imageSpacer", ImageSpacer);
            LargeImageScale = element.GetAttributeSingle("largeImageScale", LargeImageScale);
            MaxPulseTicks = element.GetAttributeInt32("maxPulseTicks", MaxPulseTicks);
            NormalImageScale = element.GetAttributeSingle("normalImageScale", NormalImageScale);
            FlowStyle = element.GetAttributeEnum("flowStyle", FlowStyle);
            SelectedItemTint = element.GetAttributeColor("selectedItemTint", SelectedItemTint);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            TextGetsFocusColour = element.GetAttributeBool("textGetsFocusColour", TextGetsFocusColour);

            //TextureInfo textureInfo, Color color, string text, string helpText

            //Content! Might be items
            var selectedIndex = -1;
            foreach (var itemElement in element.Elements("item"))
            {
                selectedIndex++;

                var texture = itemElement.GetAttributeString("texture");
                if (texture == null)
                    continue;

                var textureInfo = Game.TextureManager.GetTextureInfo(texture);
                if (textureInfo == null)
                    continue;

                var tint = element.GetAttributeColor("tint", Tint);
                var text = itemElement.GetAttributeString("text");
                var helpText = itemElement.GetAttributeString("helpText");

                AddImage(textureInfo, tint, text ?? string.Empty, helpText ?? string.Empty);

                var selected = itemElement.GetAttributeBool("selected", false);
                if (selected)
                    SelectedIndex = selectedIndex;
            }
        }

        /// Gets the text to display for the left and right dpad / stick action.
        public override string LeftRightButtonText
        {
            get { return "change value"; }
        }

        /// <summary>
        /// Gets the number of options in the list.
        /// </summary>
        public int Count
        {
            get { return _images.Count; }
        }

        /// <summary>
        /// Gets or sets the index of the selected item.
        /// </summary>
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { _selectedIndex = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureInfo"></param>
        public void AddImage(TextureInfo textureInfo)
        {
            AddImage(textureInfo, Color.White, string.Empty); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureInfo"></param>
        /// <param name="helpText"></param>
        public void AddImage(TextureInfo textureInfo, string helpText)
        {
            AddImage(textureInfo, Tint, string.Empty, helpText);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureInfo"></param>
        /// <param name="color"></param>
        /// <param name="helpText"></param>
        public void AddImage(TextureInfo textureInfo, Color color, string helpText)
        {
            AddImage(textureInfo, color, string.Empty, helpText); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureInfo"></param>
        /// <param name="color"></param>
        /// <param name="text"></param>
        /// <param name="helpText"></param>
        public void AddImage(TextureInfo textureInfo, Color color, string text, string helpText)
        {
            _images.Add(textureInfo);
            _colours.Add(color);
            _text.Add(text);
            _helpText.Add(helpText);

            _pulses.Add(0);
            _enables.Add(true);
        }

        /// <summary>
        /// Clears all the options from the list.
        /// </summary>
        public void ClearImages()
        {
            _selectedIndex = -1;
            _images.Clear();
            _colours.Clear();
            _enables.Clear();
            _text.Clear();
            _helpText.Clear();
            _pulses.Clear();
        }

        /// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
        public override Rect Bounds
        {
            get 
            { 
                if (_images.Count == 0)
                    return Rect.Zero;

                var width = 0.0f;
                var height = 0.0f;
                
                switch (FlowStyle)
                {
                    case FlowStyle.Horizontal:
                        width = (_images.Count * NormalImageScale) + ((_images.Count - 1) * ImageSpacer); 
                        height = NormalImageScale;
                        break;

                    case FlowStyle.Vertical:
                        height = (_images.Count * NormalImageScale) + ((_images.Count - 1) * ImageSpacer); 
                        width = NormalImageScale;
                        break;
                }
                

                //Calculate the bounding rect for the text based on the length, its position and the alignment.
                var x = Position.X;
                var y = Position.Y;
            
                switch (Alignment)
                {
                    case Alignment.TopLeft:
                        return new Rect(y, x, y - height, x + width);

                    case Alignment.TopCentre:
                        return new Rect(y, x - (width / 2.0f), y - height, x + (width / 2.0f));

                    case Alignment.TopRight:
                        return new Rect(y, x - width, y - height, x);

                    case Alignment.Centre:
                        return new Rect(y + (height / 2.0f), x - (width / 2.0f), y - (height / 2.0f), x + (width / 2.0f));

                    case Alignment.CentreRight:
                        return new Rect(y + (height / 2.0f), x - width, y - (height / 2.0f), x);

                    case Alignment.BottomLeft:
                        return new Rect(y + height, x, y, x + width);

                    case Alignment.BottomCentre:
                        return new Rect(y + height, x - (width / 2.0f), y, x + (width / 2.0f));

                    case Alignment.BottomRight:
                        return new Rect(y + height, x - width, y, x);

                    case Alignment.CentreLeft:
                        return new Rect(y + (height / 2.0f), x, y - (height / 2.0f), x + width);
                }

                return Rect.Zero;
            }
        }

        /// <summary>
        /// Tells a control to increase its value.
        /// </summary>
        protected override bool OnIncreaseValue()
        {
            if (_selectedIndex >= _images.Count - 1)
                return false;

            ++_selectedIndex;
            
            return true;
        }

        /// <summary>
        /// Tells a control to decrease its value.
        /// </summary>
        protected override bool OnDecreaseValue()
        {
            if (_selectedIndex <= 0)
                return false;

            --_selectedIndex;
            
            return true;
        }

        /// <summary>
        /// .
        /// </summary>
        protected override void OnMouseDown()
        {
            SetHoverIndex();

            _mouseDownHoverIndex = _hoverIndex;
        }

        protected override void OnMouseMove()
        {
            SetHoverIndex();
        }

        void SetHoverIndex() 
        {
            var bounds = Bounds;
            
            //Find the mouse relative to the control.
            //We offset by half the spacer to make the calculation nice 'n' easy. We won't get out of bounds bumf because of the first
            //contains check above.
            
            switch (FlowStyle)
            {
                case FlowStyle.Horizontal:
                    var mouseRelativeH = new Vector2(Game.MenuManager.MouseWorldPosition.X - bounds.Left, Game.MenuManager.MouseWorldPosition.Y - bounds.Top);
                    mouseRelativeH.X -= ImageSpacer / 2.0f;
            
                    var width = ((bounds.Right - bounds.Left) + ImageSpacer);
                    _hoverIndex = (int)((mouseRelativeH.X / width) * _images.Count);
                    break;

                case FlowStyle.Vertical:
                    var mouseRelativeV = new Vector2(Game.MenuManager.MouseWorldPosition.X - bounds.Left, bounds.Top - Game.MenuManager.MouseWorldPosition.Y);
                    mouseRelativeV.Y += ImageSpacer / 2.0f;
            
                    var height = ((bounds.Top - bounds.Bottom) + ImageSpacer);
                    _hoverIndex = (int)((mouseRelativeV.Y / height) * _images.Count);
                    break;
            }     
        }

        /// <summary>
        /// .
        /// </summary>
        protected override bool OnClick()
        {
            if (Game.InputMethodManager.InputMethod.SupportsMouse)
            {
                if (_hoverIndex != -1 && _hoverIndex != _mouseDownHoverIndex)
                    return false;

                _selectedIndex = _hoverIndex;
            }

            return true;
        }

        protected override void OnMouseLeave()
        {
            _hoverIndex = -1;
        }

        /// <summary>
        /// Allows a derived control to update itself.
        /// </summary>
        protected override void OnUpdate()
        {
            if (Game.InputMethodManager.InputMethod.SupportsMouse == false)
            {
                _hoverIndex = -1;
                _mouseDownHoverIndex = -1;
            }

            for (var i = 0; i < _pulses.Count; i++)
            {
                if (_selectedIndex != i ||  Game.MenuManager.FocusControl != this)
                {
                    //Elegantly reduce the size of the non active image back to normal
                    if (_pulses[i] > 0)
                        _pulses[i]--;
                }
                else
                {
                    //Pulse the selected image
                    _pulses[i] += _pulseInc;

                    if (_pulses[i] > MaxPulseTicks)
                        _pulseInc = -_pulseInc;
                    else if (_pulses[i] < 0)
                    {
                        _pulseInc = -_pulseInc;  
                        _pulses[i] = 0;
                    }
                }
            }
            
            if (_hoverIndex != -1)
                HelpText = _helpText[_hoverIndex];
            else if (_selectedIndex != -1)
                HelpText = _helpText[_selectedIndex];
        }
        
        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
        {
            var bounds = Bounds;
            var position = new Vector2(bounds.Left, bounds.Top);

            //Determine the colour of each item!

            //Normal items can be either their default color or the focused color
            Color normalTint;
            if (IsFocusControl && ImageGetsFocusColour)
                normalTint = Theme.Current.GetColor(Theme.Colors.Focus) * FocusValue;
            else
                normalTint = Tint;

            //Selected items are always the selected color
            var selectedTint = SelectedItemTint;

            //Hover items are either the hover color or their default color
            Color hoverTint;
            if (IsHoverControl)
                hoverTint = Theme.Current.GetColor(Theme.Colors.Hover);
            else
                hoverTint = normalTint;

            normalTint *= Overlay.Opacity;
            selectedTint *= Overlay.Opacity;
            hoverTint *= Overlay.Opacity;


            for (var i = 0; i < _images.Count; ++i)
            {
                var scale = MathHelper.SmoothStep(NormalImageScale, LargeImageScale, _pulses[i] / (float)MaxPulseTicks);

                //Pick the correct color (determined above)
                var tint = IsFocusControl && i == _selectedIndex ? selectedTint : i == _hoverIndex ? hoverTint : normalTint;

                var q = new Quad(position + (new Vector2(NormalImageScale, -NormalImageScale) / 2.0f), new Vector2(scale));
                Game.Renderer.DrawSprite(ref q, _images[i].TextureId, _images[i].SourceRect, tint, RenderStyle, Layer);

                if (ShowText)
                {
                    Color textTint;
                    if (IsFocusControl && TextGetsFocusColour)
                        textTint = Theme.Current.GetColor(Theme.Colors.Focus) * FocusValue;
                    else
                        textTint = TextTint;

                    textTint *= Overlay.Opacity;

                    tint = IsFocusControl && i == _selectedIndex ? selectedTint : i == _hoverIndex ? hoverTint : textTint;

                    Game.Renderer.DrawString(Theme.Font, position + new Vector2(NormalImageScale / 2, (-NormalImageScale * 0.475f) - (scale * 0.475f)), new Vector2((scale / NormalImageScale)) * FontSize, _text[i], Alignment.Centre, /*textTint*/ tint, RenderStyle, Layer);
                }

                switch (FlowStyle)
                {
                    case FlowStyle.Horizontal:
                        position.X += NormalImageScale + ImageSpacer;
                        break;

                    case FlowStyle.Vertical:
                        position.Y -= NormalImageScale + ImageSpacer;
                        break;
                }
            }
        }
    }
}
