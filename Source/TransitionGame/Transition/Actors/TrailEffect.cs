using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Rendering;

namespace Transition.Actors
{
    /// <summary>
    /// Represents lightning effects in the scene.
    /// </summary>
    /// p2[0]1----p2[i] --> p2[n]
    ///     |\    |
    ///     |  \  |
    ///     |    \|
    /// p1[0]-----p1[1] --> p1[n]
    public class TrailEffect : SpecialEffect
    {
        static ushort[] _indices;
        readonly Geometry<VertexPositionColorTexture> _geometry = new Geometry<VertexPositionColorTexture>();

        private readonly int _pointCount;
        private readonly int _segments;
        private readonly Vector2[] _positions;
        private readonly Vector2[] _offsets;
        private readonly Vector2[] _directions;
        private readonly Vector2[] _velocities;

        private Color _color;

        private float _widthStart;
        private float _widthEnd;
        private int _ticks;

        private bool _fading;
        private float _alpha;

        int _nextPointId;
        int _trailPointCount;

        TextureInfo _textureInfo;

        /// <summary>
        /// Creates a new instance of a SpecialEffects.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        /// <param name="pointCount"></param>
        public TrailEffect(TransitionGame game, int pointCount) : base(game)
        {
            _pointCount = pointCount;
            _segments = pointCount - 1;

            //Have we got an index buff and is there enough room in there for all these points?
            if (_indices == null || pointCount < _indices.Length)
                CreateIndexBuffer();
            
            _positions = new Vector2[_pointCount];
            _offsets = new Vector2[_pointCount];
            _directions = new Vector2[_pointCount];
            _velocities = new Vector2[_pointCount];

            _geometry.RenderStyle = TransparentRenderStyle.Instance;
            _geometry.TextureId = -1;
            _geometry.Indices = _indices;
            _geometry.IndexCount = _indices.Length;
        }

        private void CreateIndexBuffer()
        {
            //Build the index buffer - 
            _indices = new ushort[(_segments * 6)];// + 6]; 
            
            var vertexPointer = 0;
            for (var i = 0; i < _indices.Length; i += 6) 
            {
                //Triangle 1
                _indices[i] = (ushort)(vertexPointer);
                _indices[i + 1] = (ushort)(vertexPointer + 1);
                _indices[i + 2] = (ushort)(vertexPointer + 2);

                //Triangles 2
                _indices[i + 3] = (ushort)(vertexPointer + 2);
                _indices[i + 4] = (ushort)(vertexPointer + 1);
                _indices[i + 5] = (ushort)(vertexPointer + 3);

                vertexPointer += 2;
            }
        }

        protected override void OnInitialise(string name) 
        {
            base.OnInitialise(name);

            //TODO:
            _active = true;
        }

        public void Setup(Color color, float widthStart, float widthEnd, bool initialise, TextureInfo textureInfo)
        {
            _textureInfo = textureInfo;
            _alpha = 1.0f;
            _ticks = 0;
            _fading = false;

            _color = color;
            _widthStart = widthStart;
            _widthEnd = widthEnd;

            if (initialise)
            {
                _nextPointId = 0;
                _trailPointCount = 0;

                Array.Clear(_positions, 0, _positions.Length);
                Array.Clear(_directions, 0, _directions.Length);
                Array.Clear(_velocities, 0, _velocities.Length);
            }

            if (textureInfo != null)
                _geometry.TextureId = textureInfo.TextureId;

            _geometry.Layer = Layer;
        }

        protected override void OnUpdate(GameTime gameTime)
        {
            if (_fading)
            {
                _ticks++;
                if (_ticks <= 10)
                {
                    _alpha -= 0.1f;
                }
            }

            //Have we moved?
            var moved = false;

            if (Owner == null && Position != _previousPosition)
            {
                //Stash the position...
                //TODO - config the randomness
                _positions[_nextPointId] = Position;// + RandomHelper.GetVector(-0.1f, 0.1f, -0.1f, 0.1f);
                _directions[_nextPointId] = _previousPosition - Position;
                moved = true;
            }
            else if (Owner != null && Owner.Offset != Owner.PreviousOffset)
            {
                _offsets[_nextPointId] = Owner.Offset;
                _directions[_nextPointId] = Owner.PreviousOffset - Owner.Offset;
                moved = true;
            }

            if (moved)
            {
                //...and increase the counter.
                if (_trailPointCount < _pointCount)
                    _trailPointCount++;

                //Increase the pointer in the ring buffer or wrap round
                _nextPointId = GetRingBufferId(_nextPointId + 1);
                _velocities[_nextPointId] = RandomHelper.Random.GetVector(-0.025f, 0.025f, -0.025f, 0.025f);
            }
            else
            {
                //Shrink the trail?
                if (!_fading && _trailPointCount > 0)
                    _trailPointCount--;
            }

            var pointId = GetRingBufferId(_nextPointId - 1);
            for (var i = 0; i < _trailPointCount; i++)
            {
                //_positions[i] += _velocities[i];

                pointId = GetRingBufferId(pointId - 1);
            }
        }

        //TODO: Transition
        //protected override void OnPostUpdate()
        //{
        //    if (Owner == null)
        //    {
        //        var force = _game.Scene.GetForce();

        //        _previousPosition.X += force;

        //        for(var i = 0; i < _positions.Length; i++)
        //            _positions[i].X += force;
        //    }
        //    else
        //    {
        //        for(var i = 0; i < _positions.Length; i++)
        //            _positions[i] = Owner.Position - Owner.Offset + _offsets[i];
        //    }
        //}

        public void DrawEffect() 
        {
            if (_trailPointCount < 2)
                return;

            _geometry.IndexCount = (_trailPointCount - 1) * 6; //Trail can shrink so better refresh the count!
            _geometry.Vertices.Clear();            

            var endColor = _color * 0.0f;
            
            var t1 = _textureInfo == null ? Vector2.Zero : new Vector2(_textureInfo.SourceRect.Left, _textureInfo.SourceRect.Bottom);
            var t2 = _textureInfo == null ? Vector2.Zero : new Vector2(_textureInfo.SourceRect.Right, _textureInfo.SourceRect.Top);

            //Convert the list of points to a quad.
            var pointId = GetRingBufferId(_nextPointId - 1);
            for (var i = 0; i < _trailPointCount; i++) 
            {
                var ratio = i / (float)_trailPointCount;
                var color = Color.Lerp(_color, endColor, ratio);
                color *= _alpha; //fading?
                var dir =  _directions[pointId];
                var n = new Vector2(dir.Y, -dir.X);
                Vector2.Normalize(ref n, out n);

                var width = MathHelper.Lerp(_widthStart, _widthEnd, ratio);
          
                var halfWidth = n * width;
                var p1 = _positions[pointId] + halfWidth;
                var p2 = _positions[pointId] - halfWidth;

                _geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(p1.X, p1.Y, 0), color, t1));
                _geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(p2.X, p2.Y, 0), color, t2));

                pointId = GetRingBufferId(pointId - 1);
            }

            _game.Renderer.DrawGeometry(_geometry);
        }

        private int GetRingBufferId(int id)
        {
            var upperBound = (_pointCount - 1);
            const int lowerBound = 0;

            var range = upperBound - lowerBound + 1;
            id = ((id-lowerBound) % range);

            if (id<0)
                return upperBound + 1 + id;
            
            return lowerBound + id;
        }

        public override void Finish() 
        {
            _fading = true;    
        }
    }
}
