using System;

namespace Transition
{
    /// <summary>
    /// Data for a controller vibration.
    /// </summary>
    public class Vibration
    {
        #region Fields
        public int PlayerId;
		public float Power;
		public int Duration;
        public float PowerDelta;
        #endregion

        #region Constructors
		/// <summary>
		/// Creates a new instance of a Vibration.
		/// </summary>
        public Vibration(int playerId, float power, int duration, float powerDelta)
		{
            this.PlayerId = playerId;
			this.Power = power;
			this.Duration = duration;
            this.PowerDelta = powerDelta;
		}
        #endregion

        #region Properties
        #endregion

        #region Base Overrides
        #endregion

        #region Methods
        #endregion

        #region State Machine
        #endregion
    }
}
