using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.PostProcess
{
    /// <summary>
    ///   Responsible for post processing effects.
    /// </summary>
    public class PostProcessManager
    {
        public delegate void InitializeDelegate();

        public readonly List<BaseProcessor> PostProcessEffects = new List<BaseProcessor>();
        private readonly BaseGame _game;
        public InitializeDelegate Initializer;
        private SpriteBatch _spriteBatch;

        /// <summary>
        /// </summary>
        /// <param name = "game"></param>
        /// <param name = "initializer"></param>
        public PostProcessManager(BaseGame game, InitializeDelegate initializer)
        {
            _game = game;
            Initializer = initializer;
        }

        public RenderTarget2D SceneRenderTarget1 { get; private set; }
        public RenderTarget2D SceneRenderTarget2 { get; private set; }

        public void Initialize()
        {
            if (Initializer != null)
            {
                Initializer();
            }
        }

        public void AddProcessor(BaseProcessor processor)
        {
            PostProcessEffects.Add(processor);
        }

        public void LoadContent()
        {
            _spriteBatch = new SpriteBatch(_game.GraphicsDevice);

            CreateRenderTargets();

            foreach (var processor in PostProcessEffects)
            {
                processor.LoadContent();
            }
        }

        private void CreateRenderTargets()
        {
            var pp = _game.GraphicsDevice.PresentationParameters;
            var format = pp.BackBufferFormat;

            SceneRenderTarget1 = new RenderTarget2D(_game.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, false, format, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);
            SceneRenderTarget2 = new RenderTarget2D(_game.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, false, format, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);
        }

        public void UnloadContent()
        {
            SceneRenderTarget1.Dispose();
            SceneRenderTarget2.Dispose();

            foreach (var processor in PostProcessEffects)
            {
                processor.UnloadContent();
            }
        }

        public void ResolutionChanged()
        {
            CreateRenderTargets();

            foreach(var processor in PostProcessEffects)
            {
                processor.ResolutionChanged();
            }
        }

        /// <summary>
        ///   Called before any rendering is done
        /// </summary>
        public RenderTarget2D PreRender()
        {
            //Set up the graphics device to render to the texture...
            _game.GraphicsDevice.SetRenderTarget(SceneRenderTarget1);
            _game.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;

            foreach(var processor in PostProcessEffects)
            {
                if (processor.Enabled)
                    processor.PreRender();
            }

            return SceneRenderTarget1;
        }

        public void PostRender()
        {
            //Draw all scene post processing effects.
            foreach (var processor in PostProcessEffects)
            {
                DrawPostProcessEffect(processor);
            }

            //Draw the scene - everything that was drawn in the render target will now get drawn to the screen respecting the title safe area and the zoom factor!.
            _game.GraphicsDevice.SetRenderTarget(null);
            _game.GraphicsDevice.Clear(Color.FromNonPremultiplied(0, 0, 0, 0));

            _spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque);
            _spriteBatch.Draw(SceneRenderTarget1, _game.GraphicsDevice.Viewport.Bounds, Color.White);
            _spriteBatch.End();
        }

        public bool DrawPostProcessEffect(BaseProcessor processor)
        {
            if (processor.Enabled == false)
            {
                return false;
            }

            //Draw each post processing effect
            if (processor.Draw(_spriteBatch, 1.0f, SceneRenderTarget1, SceneRenderTarget2) == false)
            {
                return false;
            }

            //Effect rendered - swap the render targets so that the results of the previous post processing effect can be utilised by the next one.
            var swapTexture = SceneRenderTarget1;
            SceneRenderTarget1 = SceneRenderTarget2;
            SceneRenderTarget2 = swapTexture;

            return true;
        }
    }
}