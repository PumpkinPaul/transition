using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Transition.ResourceManagers;

namespace Transition.Rendering
{
    public enum RenderStyleType
    {
        Opaque,
        Transparent,
        Additive,
        AlphaBlend,
        Flash,
        Ripple,
        PointClamp,
        PointWrap,
        CircleGauge,
        Hologram,
        WobblySineWave
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class RenderStyle
    {
        static readonly Dictionary<RenderStyleType, RenderStyle> RenderStyles = new Dictionary<RenderStyleType, RenderStyle>();

        public static RenderStyle Get(string value, RenderStyle defaultRenderStyle)
        {
            if (string.IsNullOrWhiteSpace(value))
                return defaultRenderStyle;

            RenderStyleType result;
            if (Enum.TryParse(value, out result) == false)
                throw new InvalidOperationException($"Unable to parse RenderStyleType: {value}");

            if (RenderStyles.ContainsKey(result))
                return RenderStyles[result];

            throw new InvalidOperationException($"RenderStyleType: {value} has not been registered");
        }

        public Effect Effect { get; protected set; }

        public abstract RenderStyleType RenderStyleId { get; }

        public void Register(EffectManager effectManager)
        {
            RenderStyles[RenderStyleId] = this;

            OnRegister(effectManager);
        }

        protected abstract void OnRegister(EffectManager effectManager);

        public abstract void Setup(GraphicsDevice graphicsDevice);
    }
}
