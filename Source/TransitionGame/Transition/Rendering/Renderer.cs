using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Extensions;

namespace Transition.Rendering
{
    /// <summary>
    /// A class used to draw batches of sprites.
    /// </summary>
    /// <remarks>
    /// GraphicsDevice.RasterizerState = RasterizerState.CullClockwise; //OpenGL  and RasterizerState.CullCounterClockwise; //XNA
    /// </remarks>
    public class Renderer
    {
        public static readonly ushort[] QuadIndices = { 0, 1, 2, 2, 1, 3 };

        public enum SortStrategy
        {
            None,
            LayerRenderStyleTextureRank
        }

        class StateChange
        {
            public IGeometryBuffer GeometryBuffer;
            public RenderStyle RenderStyle;
            public int TextureId;
            public int StartVertex;
            public int StartIndex;
            public int VertexCount;
            public int IndexCount;
        }

        public enum LineStyle
        {
            Dotted,
            Dashed
        }

        //This constant controls how large the vertices buffer is. Larger buffers will require growing less often, which can increase performance. However, having
        //buffer that is unnecessarily large will waste memory.
        private const int MaxGeometry = 4096;

        private readonly BaseGame _game;

        //The device that we will issue draw calls to.
        private readonly GraphicsDevice _graphicsDevice;

        //GeometryBuffers for all the different types of vertex data that we are able to render
        readonly GeometryBuffer<VertexPositionColorTexture> _vertexPositionColorTextureBuffer;
        readonly GeometryBuffer<VertexPositionColorTextureFloat> _vertexPositionColorTextureFloatBuffer;

        readonly Dictionary<Type, IGeometryBuffer> _geometryBuffers;

        //Arrays of stuff for rendering.
        private readonly List<Geometry> _renderables = new List<Geometry>(MaxGeometry); //Sprites and arbitrary geometry to render this frame

        private Geometry<VertexPositionColorTexture>[] _spriteGeometry = new Geometry<VertexPositionColorTexture>[MaxGeometry];

        private readonly StateChange[] _stateChanges = new StateChange[MaxGeometry];

        //Keeps track of how many vertices have been added. Value increases until we run out of space in the buffer, at which time they grow automatically.
        private int _geometryId;

        //hasBegun is flipped to true once Begin is called, and is used to make sure users don't call End before Begin is called.
        private bool _hasBegun;

        private bool _isDisposed;

        private readonly LayerRenderStyleTextureRankSorter _geometryComparer = new LayerRenderStyleTextureRankSorter();

        SortStrategy _sortStrategy;

        private int _stateChangeId;

        private int _order;

        //Used to draw number without garbage.
        readonly StringBuilder _stringBuilder = new StringBuilder(64);

        public readonly TextureInfo PixelTextureInfo;
        readonly int _dottedTextureId;

        public int Count => _geometryId;
        public int DrawCalls { get; private set; }
        public int RenderablesCount { get; private set; }

        public Renderer(BaseGame game)
        {
            _game = game;

            _graphicsDevice = _game.Graphics.GraphicsDevice;

            for(var i = 0; i < _stateChanges.Length; i++)
                _stateChanges[i] = new StateChange();

            for(var i = 0; i < _spriteGeometry.Length; i++)
                _spriteGeometry[i] = new Geometry<VertexPositionColorTexture>();

            _vertexPositionColorTextureFloatBuffer = new GeometryBuffer<VertexPositionColorTextureFloat>(_graphicsDevice);
            _vertexPositionColorTextureBuffer = new GeometryBuffer<VertexPositionColorTexture>(_graphicsDevice);

            _geometryBuffers = new Dictionary<Type, IGeometryBuffer>
            {
                { _vertexPositionColorTextureFloatBuffer.VertexType, _vertexPositionColorTextureFloatBuffer },
                { _vertexPositionColorTextureBuffer.VertexType, _vertexPositionColorTextureBuffer },
            };

            PixelTextureInfo = _game.TextureManager.GetTextureInfo("Pixel");
            _dottedTextureId = _game.TextureManager.GetTextureId("Dotted");
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        void Dispose(bool disposing)
        {
            if (!disposing || _isDisposed) 
                return;

            foreach (var geometryBuffer in _geometryBuffers.Values)
                geometryBuffer?.Dispose();

            _isDisposed = true;
        }

        void EnsureBuffersHaveCapacity(IGeometryBuffer geometryBatch, int newVerticesCount, int newIndicesCount)
        {
            //TODO: Could test be smarter here? are buffers linked?
            if (_geometryId + 1 >= _spriteGeometry.Length || geometryBatch.BufferWillOverflow(newVerticesCount, newIndicesCount))
            {
                //We will overflow the current buffers. Action required!

                if (_sortStrategy == SortStrategy.None)
                    DrawRenderables(); //No sorting so we can just flush here.
                else
                    GrowArrays(geometryBatch); //Grow the arrays and carry on adding to the batch - we need to maintain sort order.
            }
        }

        void GrowArrays(IGeometryBuffer geometryBuffer)
        {
            var startId = _spriteGeometry.Length;
            Array.Resize(ref _spriteGeometry, _spriteGeometry.Length * 2);
            for (var i = startId; i < _spriteGeometry.Length; i++)
                _spriteGeometry[i] = new Geometry<VertexPositionColorTexture>(4);

            geometryBuffer.GrowArrays();
            geometryBuffer.AllocateGraphicsResources();
        }

        public void BeginFrame()
        {
            DrawCalls = 0;
        }

        /// <summary>
        ///Begin is called at the start of rendering each batch of geometry.
        /// </summary>
        public void Begin(SortStrategy sortStrategy)
        {
            if (_hasBegun)
                throw new InvalidOperationException("End must be called before Begin can be called again.");

            _hasBegun = true;
            _sortStrategy = sortStrategy;
            _stateChangeId = 0;
            _order = 0;
        }

        public void DrawSprite(ref Quad quad, int textureId, Rect textureCoords, Color tint, RenderStyle renderStyle, int layer = 0)
        {
            if (_hasBegun == false)
                throw new InvalidOperationException("Begin must be called before Draw can be called.");

            //If there is no space in the arrays, grow them so that we can maintain sort order.
            //We'll default to 4 for SPRITES here. Some things like BlastEffects will have a LOT more but hey ho! Something to look at
            const int verticesPerSprite = 4;
            const int indicesPerSprite = 6;
            EnsureBuffersHaveCapacity(_vertexPositionColorTextureBuffer, verticesPerSprite, indicesPerSprite);
            
            var geometry = _spriteGeometry[_geometryId];
            geometry.Layer = layer;
            geometry.Rank = _order++;

            geometry.Indices = QuadIndices;
            geometry.IndexCount = QuadIndices.Length;

            geometry.Vertices.Clear();
            geometry.Vertices.Add(new VertexPositionColorTexture(quad.BottomLeft, tint, new Vector2(textureCoords.Left, textureCoords.Bottom)));
            geometry.Vertices.Add(new VertexPositionColorTexture(quad.TopLeft, tint, new Vector2(textureCoords.Left, textureCoords.Top)));
            geometry.Vertices.Add(new VertexPositionColorTexture(quad.BottomRight, tint, new Vector2(textureCoords.Right, textureCoords.Bottom)));
            geometry.Vertices.Add(new VertexPositionColorTexture(quad.TopRight, tint, new Vector2(textureCoords.Right, textureCoords.Top)));

            geometry.RenderStyle = renderStyle;
            geometry.TextureId = textureId;

            _vertexPositionColorTextureBuffer.TotalVertices += geometry.Vertices.Count;
            _vertexPositionColorTextureBuffer.TotalIndices += geometry.IndexCount.Value;

            _geometryId++;

            _renderables.Add(geometry);
        }

        // DrawGeometry is called to add another piece of geometry to be rendered. This function can only be called once begin has been called.
        // if there is not enough room in the vertices buffer the array are doubled in size.       
        public void DrawGeometry<T>(Geometry<T> geometry) where T : IVertexType
        {
            if (_hasBegun == false)
                throw new InvalidOperationException("Begin must be called before Draw can be called.");

            var buffer = _geometryBuffers[geometry.VertexType];

            EnsureBuffersHaveCapacity(buffer, geometry.VertexCount, geometry.Indices.Length);

            _renderables.Add(geometry);

            buffer.TotalVertices += geometry.VertexCount;
            buffer.TotalIndices += geometry.IndexCount ?? geometry.Indices.Length;
        }

        public void DrawNumber(BitmapFont font, Vector2 position, Vector2 scale, int number, Alignment alignment, Color tint, RenderStyle renderStyle, int layer = 0)
        {
            _stringBuilder.Length = 0;
            _stringBuilder.AppendNumber(number);
            
            DrawString(font, position, scale, _stringBuilder, alignment, tint, renderStyle, layer);
        }

        public void DrawNumber(BitmapFont font, Vector2 position, Vector2 scale, float number, Alignment alignment, Color tint, RenderStyle renderStyle, int layer = 0)
        {
            _stringBuilder.Length = 0;
            _stringBuilder.AppendNumber(number);

            DrawString(font, position, scale, _stringBuilder, alignment, tint, renderStyle, layer);
        }

        public void DrawString(BitmapFont font, Vector2 position, Vector2 scale, string text, Alignment alignment, Color tint, RenderStyle renderStyle, int layer = 0)
        {
            _stringBuilder.Length = 0;
            _stringBuilder.Append(text);
            
            DrawString(font, position, scale, _stringBuilder, alignment, tint, renderStyle, layer);
        }

        public void DrawString(BitmapFont font, Vector2 position, Vector2 scale, StringBuilder stringBuilder, Alignment alignment, Color tint, RenderStyle renderStyle, int layer = 0)
        {
            DrawString(font, position, scale, 0, stringBuilder, alignment, tint, renderStyle, layer);
        }

        public void DrawString(BitmapFont font, Vector2 position, Vector2 scale, float rotation, string text, Alignment alignment, Color tint, RenderStyle renderStyle, int layer = 0)
        {
            _stringBuilder.Length = 0;
            _stringBuilder.Append(text);
            
            DrawString(font, position, scale, rotation, _stringBuilder, alignment, tint, renderStyle, layer);
        }

        public void DrawString(BitmapFont font, Vector2 position, Vector2 scale, float rotation, StringBuilder stringBuilder, Alignment alignment, Color tint, RenderStyle renderStyle, int layer = 0)
        {
            if (font == null)
                return;

            if (stringBuilder.Length == 0)
                return;

            var rect = font.GetBounds(position, scale, stringBuilder, alignment);

            var charPos = new Vector2();
            charPos.X = rect.Left + (scale.X / 2.0f);
            charPos.Y = rect.Bottom + (rect.Top - rect.Bottom) / 2.0f;

            //Rotated text we assume no rotation initially when calculating the position for each character. Then before rendering we'll rotate
            //around the centre of gravity (the position parameter)
            var rotationMatrix = Matrix.CreateRotationZ(rotation);
           
            for (var c = 0; c < stringBuilder.Length; c++)
            {
                var ch = stringBuilder[c];
                if (ch == '\n')
                {
                    charPos.Y -= (font.LineHeight * scale.Y);
                    charPos.X = rect.Left + (scale.X / 2.0f);
                }
                else
                {
                    var charId = font.ConvertCharToIndex(ch);

                    charPos.X -= font.CharMetrics[charId].PaddingLeft * scale.X;

                    var renderPosition = VectorHelper.RotateAboutOrigin(charPos, position, ref rotationMatrix);

                    var q = new Quad(renderPosition, scale, rotation);
                    DrawSprite(ref q, font.TextureId, font.CharMetrics[charId].Rect, tint, renderStyle, layer);
                    charPos.X += (font.CharMetrics[charId].PaddingLeft + font.CharMetrics[charId].Width + font.Spacing) * scale.X;
                }
            }
        }

        /// <summary>
        /// Draws a styled line
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="color"></param>
        /// <param name="lineStyle"></param>
        /// <param name="thickness"></param>
        /// <param name="layer"></param>
        public void DrawLineStyled(Vector2 start, Vector2 end, Color color, LineStyle lineStyle, float thickness = 0.0175f, int layer = 0)
		{
			var delta = end - start;
            var midPoint = new Vector2(start.X + delta.X * 0.5f, start.Y + delta.Y * 0.5f);

            var lineDelta = end - start;
            var lineLength = lineDelta.Length();

            var textureRect = Rect.TextureRect;

            switch (lineStyle)
            {
                case LineStyle.Dotted:
                    textureRect.Right *= lineLength * 2.5f; //good for dotted
                    break;

                case LineStyle.Dashed:
                    textureRect.Right *= lineLength; //good for dashed
                    break;
            }
            
            var q = new Quad(midPoint, new Vector2(delta.Length(), thickness), (float)Math.Atan2(delta.Y, delta.X));
            DrawSprite(ref q, _dottedTextureId, textureRect, color, PointWrapRenderStyle.Instance, layer);
		}

        /// <summary>
        /// Draws the outline of a box.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="color"></param>
        /// <param name="renderStyle"></param>
        /// <param name="thickness"></param>
        /// <param name="layer"></param>
        public void DrawBox(Rect rect, Color color, RenderStyle renderStyle, float thickness = 0.0175f, int layer = 0)
        {
            DrawLine(new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Top), color, renderStyle, thickness, layer);
            DrawLine(new Vector2(rect.Right, rect.Top), new Vector2(rect.Right, rect.Bottom), color, renderStyle, thickness, layer);
            DrawLine(new Vector2(rect.Right, rect.Bottom), new Vector2(rect.Left, rect.Bottom), color, renderStyle, thickness, layer);
            DrawLine(new Vector2(rect.Left, rect.Bottom), new Vector2(rect.Left, rect.Top), color, renderStyle, thickness, layer);
        }

        /// <summary>
        /// Draws the outline of a box.
        /// </summary>
        /// <param name="box"></param>
        /// <param name="color"></param>
        /// <param name="renderStyle"></param>
        /// <param name="thickness"></param>
        /// <param name="layer"></param>
        public void DrawBox(Box2 box, Color color, RenderStyle renderStyle, float thickness = 0.0175f, int layer = 0)
        {
            DrawLine(new Vector2(box.Left, box.Top), new Vector2(box.Right, box.Top), color, renderStyle, thickness, layer);
            DrawLine(new Vector2(box.Right, box.Top), new Vector2(box.Right, box.Bottom), color, renderStyle, thickness, layer);
            DrawLine(new Vector2(box.Right, box.Bottom), new Vector2(box.Left, box.Bottom), color, renderStyle, thickness, layer);
            DrawLine(new Vector2(box.Left, box.Bottom), new Vector2(box.Left, box.Top), color, renderStyle, thickness, layer);
        }

        /// <summary>
        /// Draws the outline of a box.
        /// </summary>
        /// <param name="top"></param>
        /// <param name="left"></param>
        /// <param name="bottom"></param>
        /// <param name="right"></param>
        /// <param name="color"></param>
        /// <param name="renderStyle"></param>
        /// <param name="thickness"></param>
        /// <param name="layer"></param>
        public void DrawBox(float top, float left, float bottom, float right, Color color, RenderStyle renderStyle, float thickness = 0.0175f, int layer = 0)
        {
            DrawLine(new Vector2(left, top), new Vector2(right, top), color, renderStyle, thickness, layer);
            DrawLine(new Vector2(right, top), new Vector2(right, bottom), color, renderStyle, thickness, layer);
            DrawLine(new Vector2(right, bottom), new Vector2(left, bottom), color, renderStyle, thickness, layer);
            DrawLine(new Vector2(left, bottom), new Vector2(left, top), color, renderStyle, thickness, layer);
        }


        /// <summary>
        /// Draws the outline of a box.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="color"></param>
        /// <param name="lineStyle"></param>
        /// <param name="thickness"></param>
        /// <param name="layer"></param>
        public void DrawBoxStyled(Rect rect, Color color, LineStyle lineStyle, float thickness = 0.0175f, int layer = 0)
        {
            DrawLineStyled(new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Top), color, lineStyle, thickness, layer);
            DrawLineStyled(new Vector2(rect.Right, rect.Top), new Vector2(rect.Right, rect.Bottom), color, lineStyle, thickness, layer);
            DrawLineStyled(new Vector2(rect.Left, rect.Bottom), new Vector2(rect.Right, rect.Bottom), color, lineStyle, thickness, layer);
            DrawLineStyled(new Vector2(rect.Left, rect.Top), new Vector2(rect.Left, rect.Bottom), color, lineStyle, thickness, layer);
        }

        /// <summary>
        /// Draws the outline of a box.
        /// </summary>
        /// <param name="box"></param>
        /// <param name="color"></param>
        /// <param name="lineStyle"></param>
        /// <param name="thickness"></param>
        /// <param name="layer"></param>
        public void DrawBoxStyled(Box2 box, Color color, LineStyle lineStyle, float thickness = 0.0175f, int layer = 0)
        {
            DrawLineStyled(new Vector2(box.Left, box.Top), new Vector2(box.Right, box.Top), color, lineStyle, thickness, layer);
            DrawLineStyled(new Vector2(box.Right, box.Top), new Vector2(box.Right, box.Bottom), color, lineStyle, thickness, layer);
            DrawLineStyled(new Vector2(box.Left, box.Bottom), new Vector2(box.Right, box.Bottom), color, lineStyle, thickness, layer);
            DrawLineStyled(new Vector2(box.Left, box.Top), new Vector2(box.Left, box.Bottom), color, lineStyle, thickness, layer);
        }

        /// <summary>
        /// Draws the outline of a box.
        /// </summary>
        /// <param name="top"></param>
        /// <param name="left"></param>
        /// <param name="bottom"></param>
        /// <param name="right"></param>
        /// <param name="color"></param>
        /// <param name="lineStyle"></param>
        /// <param name="thickness"></param>
        /// <param name="layer"></param>
        public void DrawBoxStyled(float top, float left, float bottom, float right, Color color, LineStyle lineStyle, float thickness = 0.0175f, int layer = 0)
        {
            DrawLineStyled(new Vector2(left, top), new Vector2(right, top), color, lineStyle, thickness, layer);
            DrawLineStyled(new Vector2(right, top), new Vector2(right, bottom), color, lineStyle, thickness, layer);
            DrawLineStyled(new Vector2(left, bottom), new Vector2(right, bottom), color, lineStyle, thickness, layer);
            DrawLineStyled(new Vector2(left, top), new Vector2(left, bottom), color, lineStyle, thickness, layer);
        }

        /// <summary>
        /// Draws a solid line
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="color"></param>
        /// <param name="renderStyle"></param>
        /// <param name="thickness"></param>
        /// <param name="layer"></param>
        public void DrawLine(Vector2 start, Vector2 end, Color color, RenderStyle renderStyle, float thickness = 0.0175f, int layer = 0)
        {
            var delta = end - start;
            var midPoint = new Vector2(start.X + delta.X * 0.5f, start.Y + delta.Y * 0.5f);

            var q = new Quad(midPoint, new Vector2(delta.Length(), thickness), (float)Math.Atan2(delta.Y, delta.X));
            DrawSprite(ref q, PixelTextureInfo.TextureId, PixelTextureInfo.SourceRect, color, renderStyle, layer);
        }

        //End is called once all the quads have been drawn using Draw.
        //it will call Flush to actually submit the draw call to the graphics card, and
        //then tell the basic effect to end.
        public void End()
        {
            if (_hasBegun == false)
                throw new InvalidOperationException("Begin must be called before End can be called.");

            //Draw the batch
            DrawRenderables();

            _hasBegun = false;
        }

        //Called to issue the draw call to the graphics card. Once the draw call is made, positionInBuffer is reset, so that Draw can start over
        //at the beginning. End will call this to draw the primitives that the user requested, and Draw will call this if there is not enough room in the buffer.
        private void DrawRenderables()
        {
            if (_hasBegun == false)
                throw new InvalidOperationException("Begin must be called before Flush can be called.");

            //No work to do?
            if (_renderables.Count == 0)
                return;

            //We probably want to sort by RenderStyle, Texture, then PrimitiveType
            if (_sortStrategy != SortStrategy.None)
                _renderables.Sort(0, _renderables.Count, _geometryComparer);

            _stateChangeId = -1;
            var stateChange = _stateChanges[0];

            foreach (var geometryBuffer in _geometryBuffers.Values)
                geometryBuffer.Reset();
            
            //Transfer the data from the renderable array into the vertex and index arrays ready for rendering.
            for (var r = 0; r < _renderables.Count; ++r)
            {
                var renderable = _renderables[r];

                //State changes - currently by RenderStyle / Texture / PrimitiveType
                if (r == 0 || renderable.VertexType != _renderables[r - 1].VertexType || renderable.RenderStyle != _renderables[r - 1].RenderStyle || renderable.TextureId != _renderables[r - 1].TextureId) 
                {
                    _stateChangeId++;

                    stateChange = _stateChanges[_stateChangeId];

                    stateChange.GeometryBuffer = _geometryBuffers[renderable.VertexType]; 
                    stateChange.RenderStyle = renderable.RenderStyle;
                    stateChange.TextureId = renderable.TextureId;

                    stateChange.StartVertex = stateChange.GeometryBuffer.VertexPointer;
                    stateChange.StartIndex = stateChange.GeometryBuffer.IndexPointer;

                    stateChange.VertexCount = 0;
                    stateChange.IndexCount = 0;
                }

                var indexOffset = stateChange.GeometryBuffer.VertexPointer;

                stateChange.VertexCount += renderable.VertexCount;

                var indexCount = renderable.IndexCount ?? renderable.Indices.Length;
                stateChange.IndexCount += indexCount;

                stateChange.GeometryBuffer.CopyVertices(renderable, stateChange.GeometryBuffer.VertexPointer);
                stateChange.GeometryBuffer.VertexPointer += renderable.VertexCount;

                stateChange.GeometryBuffer.CopyIndices(renderable.Indices, stateChange.GeometryBuffer.IndexPointer, indexCount, indexOffset);
                stateChange.GeometryBuffer.IndexPointer += indexCount;
            }

            const int currentTextureId = int.MinValue;
            RenderStyle currentRenderStyle = null;
            IGeometryBuffer currentGeometryBuffer = null;

            for (var stateChangeId = 0; stateChangeId <= _stateChangeId; ++stateChangeId)
            {
                stateChange = _stateChanges[stateChangeId];

                if (stateChange.RenderStyle != currentRenderStyle)
                {
                    currentRenderStyle = stateChange.RenderStyle;
                    currentRenderStyle.Setup(_game.GraphicsDevice);
                }

                if (stateChange.GeometryBuffer != currentGeometryBuffer)
                {
                    currentGeometryBuffer = stateChange.GeometryBuffer;

                    stateChange.GeometryBuffer.AllocateGraphicsResources();

                    //TODO - this looks handy!
                    //_graphicsDevice.SetVertexBuffers(All stateChange.GeometryBuffer.VertexBuffer);

                    stateChange.GeometryBuffer.SetBuffersToGraphicsDevice();
                    stateChange.GeometryBuffer.SetData();
                }

                if (currentRenderStyle == null)
                    continue;

                if (stateChange.TextureId != currentTextureId)
                {
                    Texture2D currentTexture = null;

                    var textureEnabled = false;
                    if (stateChange.TextureId >= 0)
                    {
                        textureEnabled = true;
                        currentTexture = _game.TextureManager.GetTexture(stateChange.TextureId);
                    }

                    var basicEffect = currentRenderStyle.Effect as BasicEffect;
                    if (basicEffect != null) 
                    {
                        var effect = basicEffect;
                        effect.Texture = currentTexture;
                        effect.TextureEnabled = textureEnabled;
                        effect.View = _game.ViewMatrix;
                        effect.World = _game.WorldMatrix;
                        effect.Projection = _game.ProjectionMatrix;
                    }
                    else 
                    {
                        _graphicsDevice.Textures[0] = currentTexture;
                        currentRenderStyle.Effect.Parameters["UserTexture"].SetValue(currentTexture);                

                        currentRenderStyle.Effect.Parameters["View"].SetValue(_game.ViewMatrix);
                        currentRenderStyle.Effect.Parameters["World"].SetValue(_game.WorldMatrix);
                        currentRenderStyle.Effect.Parameters["Projection"].SetValue(_game.ProjectionMatrix);
                     }
                }

                foreach (var pass in currentRenderStyle.Effect.CurrentTechnique.Passes)
                {
                    pass.Apply();

                    _graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, stateChange.StartVertex, stateChange.VertexCount, stateChange.StartIndex, stateChange.IndexCount / 3);
    
                    DrawCalls++;
                }
            }

            _geometryId = 0;
            foreach (var geometryBuffer in _geometryBuffers.Values)
            {
                geometryBuffer.TotalVertices = 0;
                geometryBuffer.TotalIndices = 0;
            }
            
            RenderablesCount = _renderables.Count;
            _renderables.Clear();
        }
    }

    class LayerRenderStyleTextureRankSorter : IComparer<Geometry>
    {
        public int Compare(Geometry a, Geometry b)
        {
            if (a.Layer > b.Layer)
                return 1;

            if (a.Layer < b.Layer)
                return -1;

            if (a.RenderStyle.RenderStyleId > b.RenderStyle.RenderStyleId)
                return 1;

            if (a.RenderStyle.RenderStyleId < b.RenderStyle.RenderStyleId)
                return -1;

            if (a.TextureId > b.TextureId)
                return 1;

            if (a.TextureId < b.TextureId)
                return -1;

            if (a.Rank > b.Rank)
                return 1;

            if (a.Rank < b.Rank)
                return -1;

            return 0;
        }
    }
}