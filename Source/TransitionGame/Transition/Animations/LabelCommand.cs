using System.Diagnostics;
using System.Xml.Linq;

namespace Transition.Animations
{
    /// <summary>
    /// An Animation specifies a sequence of Commands which control the image displayed by an Animated thing.
    /// </summary>
    public class LabelCommand : Command
    {						
        public string Id;

        /// <summary>
        /// A special command that doesn't actually do anything, but serves as a destination for goto's and such.
	    /// Labels are not actually placed in the command list and so don't count as sequence numbers
        /// </summary>
        /// <param name="target">The target Animated thing</param>
        /// <returns>If the next command should be immediately executed.</returns>
	    public override bool Execute(Sprite target)
        {
            Debug.Assert(false); // We should never execute labels
			return true;
        }

        public override void Load(XElement element) 
        { 
            Id = element.GetAttributeString("id");
        }
    }
}
