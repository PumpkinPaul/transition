using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.Rendering;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control that displays hints for controller actions.
    /// </summary>
    public class LootControl : MenuControl
    {		
        private enum State
        {
            Locked,
            Available,
            SoldOut,
            CantAfford
        }

        private int _cost;
        private string _costDescription;

        private readonly TextureInfo _valueTextureInfo;
        private readonly TextureInfo _pixelTextureInfo;

        private State _state;

        private int _unlockLevel;

        public float Width;

        /// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;

        /// <summary>The size of the font.</summary>
        public Vector2 FontSize;

        public Color CashTint;

        /// <summary>The colour to tint the image with.</summary>
        /// <remarks>MenuControl.Tint will tint the text.</remarks>
        public Color ImageTint;

        /// <summary>The angle of rotation around the z-axis (in radians).</summary>
        public float ImageRotation;

        /// <summary>The size of the image.</summary>
        public Vector2 ImageSize;

        /// <summary>The text to render.</summary>
        public string Description;

        public int CurrentMoney;
        public int CurrentLevelNumber;

        public int UnlockLevel
        {
            get { return _unlockLevel; }
            set 
            { 
                _unlockLevel = value;
                LockedMessage = "locked until level " + _unlockLevel; 
            }
        }

        /// <summary>The id of the texture to render for the hint image.</summary>
        public int Value;

        /// <summary>The value of the maximum allowed.</summary>
        public int MaxValue;

        public bool IsLocked { get { return _state == State.Locked; } }

        public int Cost 
        {
            get { return _cost; }
            set 
            {
                _cost = value;
                _costDescription = "$" + _cost;
            }
        }

        public string LockedMessage { get; private set; }

        /// <summary>The info of the texture to render for the hint image.</summary>
        public TextureInfo IconTextureInfo;	
		
		/// <summary>
        /// Initialises a new instance of a LootControl control.
		/// </summary>
		/// <param name="game">A reference to the game.</param>
        public LootControl(BaseGame game) : base(game)
		{
            Font = Theme.Font;
            FontSize = new Vector2(0.75f, 0.75f);
            CashTint = Color.Green;
            ImageTint = Color.White;
            ImageSize = new Vector2(0.75f);
            Width = 4.0f;
            MaxValue = 1;
            UnlockLevel = 1;

            _valueTextureInfo = Game.TextureManager.GetTextureInfo("ParticleDisc");
            _pixelTextureInfo = Game.TextureManager.GetTextureInfo("Pixel");
		}

        public override void Load(XElement element)
        {
            base.Load(element);
        }

        /// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
		public override Rect Bounds
		{
			get { return new Rect(Position.Y + 0.5f, Position.X - 0.2f, Position.Y - 1.1f - 0.2f, Position.X + Width + 0.2f); }
		}

        /// <summary>
        /// Occurs when a control should increase its value.
        /// </summary>
        /// <returns>.</returns>
        protected override bool OnIncreaseValue()
        {
            return true;
        }

        /// <summary>
        /// Occurs when a control should decrease its value.
        /// </summary>
        /// <returns>.</returns>
        protected override bool OnDecreaseValue()
        {
            return true;
        }

        /// <summary>
        /// Allows a derived control to draw itself.
		/// </summary>
		protected override void OnDraw()
		{
            //Determine the state for rendering
            if (CurrentLevelNumber < UnlockLevel)
                _state = State.Locked;
            else if (CurrentMoney >= Cost)
            {
                //Can afford
                if (Value < MaxValue)
                    _state = State.Available;
                else
                    _state = State.SoldOut;
            }
            else
                _state = State.CantAfford;

            var opacityModifier = _state == State.Available ? 1.0f : 0.25f;

            var tint = Tint;
            tint *= Overlay.Opacity * MenuControl.InactiveAlphaModifier * (_state == State.Available ? 0.995f : 0.3f);
            tint *= FocusValue;
            
            var background = new Quad();
            background.Rect = Bounds;
            Game.Renderer.DrawSprite(ref background, _pixelTextureInfo.TextureId, _pixelTextureInfo.SourceRect, tint, TransparentRenderStyle.Instance);

            tint = Tint;
            tint *= Overlay.Opacity * opacityModifier;
            tint *= FocusValue;
            
            //Description
            Game.Renderer.DrawString(Font, Position, FontSize, Description, Alignment.CentreLeft, tint, RenderStyle);

            //Draw cost of loot
            tint = CashTint;
            tint *= Overlay.Opacity * opacityModifier;
            tint *= FocusValue;

            if (Value == MaxValue)
                Game.Renderer.DrawString(Font, Position + new Vector2(Width, -0.85f), FontSize * 0.9f, "MAXED", Alignment.CentreRight, tint, RenderStyle);
            else
                Game.Renderer.DrawString(Font, Position + new Vector2(Width, -0.85f), FontSize * 0.9f, _costDescription, Alignment.CentreRight, tint, RenderStyle);

            tint = ImageTint;
            tint *= Overlay.Opacity * opacityModifier;
            tint *= FocusValue;
            
            //Draw the image centred on position.
            var q = new Quad(Position + new Vector2(Width - (ImageSize.X / 2.0f), 0), ImageSize, ImageRotation);
            Game.Renderer.DrawSprite(ref q, IconTextureInfo.TextureId, IconTextureInfo.SourceRect, tint, RenderStyle);

            if (_state == State.Locked)
            {
                tint = Color.Gray;
                tint *= Overlay.Opacity * opacityModifier;
                tint *= FocusValue;
                
                Game.Renderer.DrawString(Font, Position + new Vector2(0, -0.8f), FontSize * 0.9f, LockedMessage, Alignment.CentreLeft, tint, RenderStyle);
            }
            else
            {
                if (MaxValue > 0)
                {
                    float blockX = Position.X + 0.2f;
                    float blockY = Position.Y - 0.8f;

                    for(var i = 0; i < MaxValue; i++)
                    {
                        tint = CashTint;
                        tint *= Overlay.Opacity * opacityModifier;

                        if (i >= Value)
                            tint *= 0.25f;

                        tint *= FocusValue;
                        
                        var q1 = new Quad(new Vector2(blockX, blockY), new Vector2(0.4f, 0.4f), 0);
                        Game.Renderer.DrawSprite(ref q1, _valueTextureInfo.TextureId, _valueTextureInfo.SourceRect, tint, TransparentRenderStyle.Instance);
                        blockX += 0.5f;
                    }
                }
            }

            //Draw a bounding rect to highlight the active control
            if (Game.MenuManager.FocusControl == this)
            {
                var bounds = Bounds;
                const float lineWidth = 0.04f;

                tint = Color.White;
                tint *= Overlay.Opacity;

                var corner = new Quad();
                corner.Rect = new Rect(bounds.Top, bounds.Left, bounds.Top - lineWidth, bounds.Right);
                Game.Renderer.DrawSprite(ref corner, _pixelTextureInfo.TextureId, _pixelTextureInfo.SourceRect, tint, TransparentRenderStyle.Instance);

                corner.Rect = new Rect(bounds.Bottom + lineWidth, bounds.Left, bounds.Bottom, bounds.Right);
                Game.Renderer.DrawSprite(ref corner, _pixelTextureInfo.TextureId, _pixelTextureInfo.SourceRect, tint, TransparentRenderStyle.Instance);

                corner.Rect = new Rect(bounds.Top, bounds.Left, bounds.Bottom, bounds.Left + lineWidth);
                Game.Renderer.DrawSprite(ref corner, _pixelTextureInfo.TextureId, _pixelTextureInfo.SourceRect, tint, TransparentRenderStyle.Instance);

                corner.Rect = new Rect(bounds.Top, bounds.Right - lineWidth, bounds.Bottom, bounds.Right);
                Game.Renderer.DrawSprite(ref corner, _pixelTextureInfo.TextureId, _pixelTextureInfo.SourceRect, tint, TransparentRenderStyle.Instance);

            }
        }
    }
}
