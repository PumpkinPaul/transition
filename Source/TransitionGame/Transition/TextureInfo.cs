using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Animations;

namespace Transition
{
    /// <summary>
    /// Represents the definition of an individual image from a sprite sheet
    /// </summary>
    public class TextureInfo : IAppearance
    {
        public int TextureId;
        public Texture2D Texture;
        public string AssetName;
        public string SpriteImageName;
        public Rect SourceRect;
        public Rect FlippedSourceRect;
        public Vector2 Origin;
        public string Style;

        public bool ToSprite(Sprite target) 
        {
            target.TextureInfo = this;
            return false;
        }
    }
}
