using Microsoft.Xna.Framework;
using Transition.Eval;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Selector
    {
        protected SelectionTool SelectionTool { get; private set; }

        protected Selector(SelectionTool selectionTool)
        {
            SelectionTool = selectionTool;
        }

        protected LevelEditor LevelEditor { get { return SelectionTool.Game.LevelEditor; } }
        protected LevelEditorState LevelEditorState { get { return SelectionTool.Game.LevelEditor.LevelEditorState; } }

        public abstract SelectionTool.Feature MouseJustDownOverCanvas();
        public abstract void Moving(Vector2 delta);
        public abstract bool DragCompleted(bool isShiftDown, Rect selectionRect);

        public abstract int SelectionCount { get; }

        public virtual Vector4 GetAcumulatedColorVector() { return Vector4.Zero; }
    }
}
