using Microsoft.Xna.Framework.Input;
using Nuclex.Input;
using Transition.Menus;

namespace Transition.Input
{
    /// <summary>
    /// Represents game input from a keyboard only
    /// </summary>
    /// <remakrks>
    /// This is meant to be a simple control method that spoofs the gamepad.
    /// </remakrks>
    /// <example>
    /// Think of a Uridium
    /// </example>
    public class KeyboardInputMethod : InputMethod
    {
        public override bool SupportsMouse { get { return false; } }
        public override bool SupportsKeyboard { get { return true; } }
        public override bool SupportsGamepad { get { return false; } }

        public override bool SupportsVibration { get { return false; } }

        public override bool CanChangeInputMethodIndex { get  { return false;} set {} }

        public override bool IsBeingUsed() 
        {
            if (KeyboardHelper.Keyboard.IsKeyDown(Keys.Enter))
                return true;

            if (KeyboardHelper.Keyboard.IsKeyDown(Keys.Space))
                return true;

            if (KeyboardHelper.Keyboard.IsKeyDown(Keys.Up))
                return true;

            if (KeyboardHelper.Keyboard.IsKeyDown(Keys.Down))
                return true;

            if (KeyboardHelper.Keyboard.IsKeyDown(Keys.Left))
                return true;

            if (KeyboardHelper.Keyboard.IsKeyDown(Keys.Right))
                return true;

            for (var k = Keys.A; k <= Keys.Z; k++)
            {
                if (KeyboardHelper.IsKeyPressed(k))
                    return true;
            }

            return false;
        }

        public override GameInput GatherGameInput(ExtendedPlayerIndex playerIndex, PlayerProfile playerProfile)
        {
            var gameInput = new GameInput();

            SetGameInputFromKeyboard(ref gameInput, playerProfile);

            return gameInput;
        }

        protected void SetGameInputFromKeyboard(ref GameInput gameInput, PlayerProfile playerProfile)
        {
            if (KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Fire))
                gameInput.Fire = true;
                        
            if (KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Left))
                gameInput.Left = true;
            else if (KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Right))
                gameInput.Right = true;

            if (KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Left))
                gameInput.LeftJustPressed = true;
            else if (KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Right))
                gameInput.RightJustPressed = true;

            if (KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Up))
                gameInput.Jump = true;

            if (KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Up))
                gameInput.JumpJustPressed = true;

            if (KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Down))
                gameInput.Rewind = true;

            if (KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Down))
                gameInput.RewindJustPressed = true;

            if (KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Pause))
                gameInput.Pause = true;
        }

        public override MenuInput GatherMenuInput(MenuManager menuManager, ExtendedPlayerIndex playerIndex, PlayerProfile playerProfile)
        {
            var menuInput = new MenuInput();

            menuInput.SelectPressed = menuInput.SelectPressed || (KeyboardHelper.IsKeyPressed(playerProfile.InputKeys.Fire) || KeyboardHelper.IsKeyPressed(Keys.Enter));
            menuInput.SelectJustPressed = menuInput.SelectJustPressed || (KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Fire) || KeyboardHelper.IsKeyJustPressed(Keys.Enter));
                
	        menuInput.PreviousControlPressed = menuInput.PreviousControlPressed || KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Up);
	        menuInput.NextControlPressed = menuInput.NextControlPressed || KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Down);
	        menuInput.IncreaseValuePressed = menuInput.IncreaseValuePressed || KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Right);
	        menuInput.DecreaseValuePressed = menuInput.DecreaseValuePressed || KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Left);

	        menuInput.XJustPressed = menuInput.XJustPressed || KeyboardHelper.IsKeyJustPressed(Keys.X);
	        menuInput.YJustPressed = menuInput.YJustPressed || KeyboardHelper.IsKeyJustPressed(Keys.Y);
            menuInput.BackJustPressed = menuInput.BackJustPressed || KeyboardHelper.IsKeyJustPressed(Keys.Escape);

            //Keyboard held
            menuInput.PreviousControlPressed = HandleHeldAction(ref menuInput, KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Up), menuInput.PreviousControlPressed, ref PreviousControlHeldTicks);
            menuInput.NextControlPressed = HandleHeldAction(ref menuInput, KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Down),  menuInput.NextControlPressed, ref NextControlHeldTicks);
            menuInput.DecreaseValuePressed = HandleHeldAction(ref menuInput, KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Left), menuInput.DecreaseValuePressed, ref DecreaseValueHeldTicks);
            menuInput.IncreaseValuePressed = HandleHeldAction(ref menuInput, KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Right), menuInput.IncreaseValuePressed, ref IncreaseValueHeldTicks);

            return menuInput;
        }
    }
}
