namespace Transition.Actors
{
    /// <summary>
    /// Represents special effects in the scene.
    /// </summary>
    public abstract class SpecialEffect : Actor
    {
        /// <summary>
        /// Creates a new instance of a Effects.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        protected SpecialEffect(TransitionGame game) : base(game) { }

        /// <summary>
        /// Gets the type of actor.
        /// </summary>
        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.SpecialEffect; }
        }

        public virtual bool IsEffectActive 
        { 
            get { return false; }
        }

        public virtual void Finish() { }
    }
}
