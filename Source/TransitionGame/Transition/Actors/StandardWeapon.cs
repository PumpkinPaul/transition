namespace Transition.Actors
{
    public class StandardWeapon : Weapon
    {
        /// <summary>
        /// Creates a new instance of a StandardWeapon
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public StandardWeapon(TransitionGame game) : base(game) { }

        /// <summary>
        /// Creates the bullet fired by this wepaon
        /// </summary>
        /// <returns>A new instance of a Bullet.</returns>
        protected override Bullet CreateBullet()
        {
            return (Bullet)_game.ActorFactory.Create(BulletClass, BulletName);
        }
    }
}
