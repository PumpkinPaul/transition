using System;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the start menu.
    /// </summary>
    public class ShipSelectionMenu : Menu
    {
        //Controls...
        private ShipSelectionControl _shipSelection;
        private TextButtonControl _playButton;

        //HashSet<string> _profileShips = new HashSet<string>();

		/// <summary>
        /// Initialises a new instance of a StartMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public ShipSelectionMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            var tabIndex = -1;

            _shipSelection = new ShipSelectionControl(Game);
            _shipSelection.Position = Vector2.Zero;

            AddControl(_shipSelection, ++tabIndex);
            _shipSelection.Position = new Vector2(0.0f, 0.0f);
            _shipSelection.HelpText = "Select your ship";
            _shipSelection.Click += PlayButtonClick;
            _shipSelection.SelectedIndex = 0;
            _shipSelection.Initialise(Game.EntityBlueprints.ShipBlueprints());

            const float yPosition = -5.0f;

            _playButton = new TextButtonControl(Game);
            _playButton.Position = new Vector2(0.0f, yPosition);
            _playButton.Text = "Continue";
            _playButton.Click += PlayButtonClick;
            _playButton.HelpText = "Choose ship and continue to the next screen";

            AddControl(_playButton, ++tabIndex);
        }

        protected override void OnAddedToManager() 
        {
            base.OnAddedToManager();

            //When adding the ship selection menu we shall default it to the one specified in the profile. However, we only want to do this once for each profile.
            //if (_profileShips.Contains(Game.Player1.Profile.Name) == false)
            //{
            //    _profileShips.Add(Game.Player1.Profile.Name);
                _shipSelection.SetSelectedItemByText(Game.ActivePlayer.Profile.SelectedShipName);
            //}
        }

        //public void ProfileSignedOut(string profileName)
        //{
        //    if (_profileShips.Contains(profileName))
        //        _profileShips.Remove(profileName);
        //}

        protected override void OnUpdateMustCallBase() 
        {
            base.OnUpdateMustCallBase();

            if (Game.InputMethodManager.InputMethod.SupportsMouse)
            {
                _playButton.Visible = true;
                _shipSelection.HelpText = "Select your ship";
            }
            else
            {
                _playButton.Visible = false;
                MenuManager.SetFocus(_shipSelection, false);
                _shipSelection.HelpText = "Choose ship and continue to the next screen";
            }
        } 

        private void SaveSelectedShip()
        {
            if (Game.ActivePlayer.Profile.SelectedShipName != _shipSelection.SelectedShipName)
            {
                Game.ActivePlayer.Profile.RequiresSave = true;
                Game.ActivePlayer.Profile.SelectedShipName = _shipSelection.SelectedShipName;
                Game.ActivePlayer.Profile.Save(Game);
            }
        }

        protected override bool OnBack() 
        {
            SaveSelectedShip();

            return base.OnBack();
        }

        /// <summary>
        /// Event handler for the play button click.
        /// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The data for the event.</param>
		private void PlayButtonClick(object sender, EventArgs e)
		{
            SaveSelectedShip();

            MenuManager.AddMenu(typeof(NewGameMenu));
		}
    }
}
