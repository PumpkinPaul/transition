float2 Center = 0.5f;
float RippleSize = 0.25f;
float Intensity = 0.25f;

sampler TextureSampler;

#if SM4
	void VS(float4 inPosition : SV_POSITION, float2 inTex : TexCoord0, out float4 oPosition : SV_POSITION, out float2 oTex : TexCoord0)
#else
	void VS(float4 inPosition : Position0, float2 inTex : TexCoord0, out float4 oPosition : Position0, out float2 oTex : TexCoord0)
#endif
{
	oTex = inTex;
	oPosition = inPosition; 
}

float4 PixelShaderFunction(float2 TexCoord  : TEXCOORD0) : COLOR0
{
  	float2 vec = TexCoord - Center.xy;
  	float dist = length(vec);

	vec = normalize(vec);
		
  	float w = 0.0f;
		
  	if (dist < RippleSize)
  	{
		float rippleRadius = (1.0f - dist / RippleSize);
    		w = rippleRadius * Intensity * cos(rippleRadius * 37.853981634);
	}

  	return tex2D(TextureSampler, TexCoord + (vec * w));
}

technique Ripple
{
	pass P0
	{
		#if SM4
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
		#elif SM3
		PixelShader = compile ps_3_0 PixelShaderFunction();
		#else
		PixelShader = compile ps_2_0 PixelShaderFunction();
		#endif
	}
}