using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Transition.Animations;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
	/// <summary>
	/// Configuration data for entitiess.
	/// </summary>
    public class EntityBlueprint
    {
        public class Layer
        {
            public IAppearance Appearance;    
            public int DrawLayer;
        }

        public Type EntityType;
        public string Name;
		public Layer[] Layers;
        public int InitialHealth;
        public Box2 CollisionBounds;
	    public string ExplosionSfx;
        public Vector2 BaseSize;
		public readonly List<ParticleEffect> SpawnEffects = new List<ParticleEffect>();
		public readonly List<ParticleEffect> TrailEffects = new List<ParticleEffect>();
        public readonly List<ParticleEffect> ExpiredEffects = new List<ParticleEffect>();
		public readonly List<ParticleEffect> ExplosionEffects = new List<ParticleEffect>();
    }
}
