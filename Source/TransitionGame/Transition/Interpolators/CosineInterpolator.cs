using System;
using System.Runtime.Serialization;

namespace Transition.Interpolators
{
    [Serializable]
    public sealed class CosineInterpolator : InterpolatorBase, ISerializable 
    {
	    public static readonly CosineInterpolator Instance = new CosineInterpolator();
	   
	    public override float Interpolate(float a, float b, float ratio) 
        {
		    if (a == b) 
			    return a;
		    
		    if (ratio < 0.0f) 
			    ratio = 0.0f;
		    else if (ratio > 1.0f) 
			    ratio = 1.0f;

		    var f = Math.Sqrt(1.0 - ratio * ratio);
		    return (float) (a * f + b * (1.0 - f));
        }

      	public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.SetType(typeof(SingletonSerializationHelper));
        }

        [Serializable]
        private sealed class SingletonSerializationHelper : IObjectReference 
        {
            // This object has no fields (although it could).

            // GetRealObject is called after this object is deserialized.
            public Object GetRealObject(StreamingContext context) 
            {
                // When deserialiing this object, return a reference to 
                // the Singleton object instead.
                return Instance;
            }
        }
    }
}
