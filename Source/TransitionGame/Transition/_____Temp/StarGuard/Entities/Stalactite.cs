﻿using System;
using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Util;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Stalactite enemy
    /// </summary>
    [ProtoContract]
    public class Stalactite : Enemy
    {
        const float FallingSpeed = -0.125f;
        const int MaxResurectionTicks = 60;

        public enum Phase
        {
            Hanging,
            Falling,
            Regeneration
        }

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        Phase _phase;

        [ProtoMember(2)]
        Vector2 _initialPosition;

        [ProtoMember(3)]
        int _countdownTicks;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public Stalactite(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _phase = Phase.Hanging;
            _initialPosition = Vector2.Zero;
            _countdownTicks = 0;

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnAddedToWorld(World world)
        {
            _initialPosition = Position;
        }

        void BeginPhaseRegeneration()
        {
            _phase = Phase.Regeneration;
            Position = _initialPosition;
            Velocity.Y = 0;
            _countdownTicks = MaxResurectionTicks;
            HitTicks = MaxResurectionTicks;
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            switch (_phase)
            {
                case Phase.Hanging:
                    UpdatePhaseHanging();
                    break;

                case Phase.Falling:
                    UpdatePhaseFalling();
                    break;

                case Phase.Regeneration:
                    UpdatePhaseRegeneration();
                    break;
            }
        }

        void UpdatePhaseHanging()
        {
            var player = Game.World.Get<Player>(EntityKeys.Player);

            if (Math.Abs(player.Position.X - Position.X) < GameMap.TileSize && Position.Y > player.Position.Y && Math.Abs(player.Position.Y - Position.Y) < GameMap.TileSize * 4)
            {
                _phase = Phase.Falling;
                Velocity.Y = FallingSpeed;
            }
        }

        void UpdatePhaseFalling()
        {
            Game.GameMap.HandleCollisions(this);
        }

        void UpdatePhaseRegeneration()
        {
            _countdownTicks--;
            Visible = Math2.SquareWave(MaxResurectionTicks - _countdownTicks / (float)MaxResurectionTicks, 16U) > 0;

            if (_countdownTicks > 0)
                return;

            Visible = true;
            _phase = Phase.Hanging;

            Sound.Play(Game, "StalactiteRegenerate", Position);
        }

        protected override void OnCollisionWithEntity(Entity collider, float time)
        {
            if (collider.Classification == EntityClassification.Player)
            {
                CreateSparks(Position.X > collider.Position.X ? CollisionEdge.Left : CollisionEdge.Right);

                BeginPhaseRegeneration();
            }
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            Move();

            CreateSparks(entityCollisionEdge);

            Sound.Play(Game, "StalactiteExplode", Position);

            BeginPhaseRegeneration();
        }

        void CreateSparks(CollisionEdge entityCollisionEdge)
        {
            var ps = Game.EntityFactory.Create<ParticleSystem>();
            if (ps == null)
                return;

            switch (entityCollisionEdge)
            {
                case CollisionEdge.Bottom:
                    ps.Position = Position + new Vector2(0, CollisionBounds.Bottom);
                    break;

                case CollisionEdge.Left:
                    ps.Position = Position + new Vector2(CollisionBounds.Left, 0);
                    break;

                case CollisionEdge.Right:
                    ps.Position = Position + new Vector2(CollisionBounds.Right, 0);
                    break;
            }

            
            ps.TemplateName = "BulletSparks";
            Game.World.Add(ps);
        }
    }
}