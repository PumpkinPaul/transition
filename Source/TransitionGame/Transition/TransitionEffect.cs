using System;
using Transition.MenuControls;

namespace Transition
{
    /// <summary>
    /// A base class for transitioning things.
    /// </summary>
    public class TransitionEffect
    {
        public enum Type
        {
            Relative,
            Absolute
        }

        public string Name { get; set; }
        
        bool _enabled;

        readonly Action _start;
        readonly Action<float> _action;
        readonly Action _completed;

        protected readonly int _maxTicks;
        int _ticks = -1;

        protected readonly int _maxDelay;
        int _delayTicks = -1;

        public TransitionEffect(string name, int delay, int ticks, Action start, Action<float> action, Action completed)
        {
            Name = name;
            _maxDelay = delay;
            _maxTicks = ticks;

            _start = start;
            _action = action;
            _completed = completed;
        }

        public void Start()
        {
            _ticks = 0;
            _delayTicks = 0;
            _enabled = true;

            OnStart();
        }

        protected virtual void OnStart() { }

        public void Stop()
        {
            _enabled = false;

            OnStop();
        }

        protected virtual void OnStop() { }

        public void Update()
        {
            if (_enabled == false)
                return;

            if (_maxDelay != 0)
            {
                if (_delayTicks < _maxDelay)
                {
                    _delayTicks++;
                    return;
                }
            }

            var delta = 0.0f;

            //Is the transition independant? Does it use its own timer?
            if (_maxTicks != 0)
            {
                //We have a timer so test to see if it has fired!
                if (_ticks > _maxTicks)
                {
                    if (_completed != null)
                        _completed();
                    _enabled = false;
                    return;
                }

                _ticks++;
                delta = (float)_ticks / _maxTicks;
            }

            OnUpdate(delta);
        }

        protected virtual void OnUpdate(float delta) { }
    }
}
