﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition._____Temp.StarGuard.EntityBlueprints;

namespace Transition._____Temp.StarGuard.Entities
{
    [ProtoContract]
    public class EnemyBullet : Entity
    {
        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        //[ProtoMember(1)]
        //protected EnemyBulletBlueprint EnemyBulletBlueprint;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public EnemyBullet(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.EnemyBullet;

        public override void Initialise(string name)
        {
            base.Initialise(name);
            
            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            TransitionGame.Instance.GameMap.HandleCollisions(this, respectPlatforms: false);
        }

        protected override void OnCollisionWithEntity(Entity collider, float time) 
        { 
            Debug.Assert(collider.Classification == EntityClassification.Player, "Expecting a player here");

            Deactivate();

            CreateSparks(Position.X > collider.Position.X ? CollisionEdge.Left : CollisionEdge.Right);
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            Deactivate();

            CreateSparks(entityCollisionEdge);

            Sound.Play(Game, "BulletWall", Position);
        }

        void CreateSparks(CollisionEdge entityCollisionEdge)
        { 
            var ps = Game.EntityFactory.Create<ParticleSystem>();
            if (ps == null)
                return;

            switch (entityCollisionEdge)
            {
                case CollisionEdge.Top:
                    ps.Position = Position + new Vector2(0.0f, CollisionBounds.Height * 0.5f);
                    break;

                case CollisionEdge.Left:
                    ps.Position = Position - new Vector2(CollisionBounds.Width * 0.5f, 0.0f);
                    break;

                case CollisionEdge.Bottom:
                    ps.Position = Position - new Vector2(0.0f, CollisionBounds.Height * 0.5f);
                    break;

                case CollisionEdge.Right:
                    ps.Position = Position + new Vector2(CollisionBounds.Width * 0.5f, 0.0f);
                    break;
            }
            
            ps.TemplateName = "BulletSparks";
            Game.World.Add(ps);
        }
    }
}