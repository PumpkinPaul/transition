﻿#if WINDOWS_STOREAPP
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Windows.ApplicationModel;
    using Windows.Storage;
    using Windows.Storage.Search; 

    namespace System.IO
    {
        public class Directory
        {
            public static bool Exists(string path)
            {
                path = Path.Combine(Package.Current.InstalledLocation.Path, path);
                return Task.Run(()=> ExistsInternal(path)).Result;
            }

            async private static Task<bool> ExistsInternal(string path)
            {
                path = Path.Combine(Package.Current.InstalledLocation.Path, path);
                var storageFolder = await StorageFolder.GetFolderFromPathAsync(path);

                return storageFolder != null;
            }

            public static string[] GetFiles(string path, string searchPattern)
            {
                return Task.Run(()=> GetFilesInternal(path, searchPattern)).Result.ToArray();
            }

            async private static Task<string[]> GetFilesInternal(string path, string searchPattern)
            {
                var fullPath = Path.Combine(Package.Current.InstalledLocation.Path, path);
                var fileTypeFilter = new List<string> { FixSearchPattern(searchPattern)};

                var queryOptions = new QueryOptions(CommonFileQuery.DefaultQuery, fileTypeFilter);

                var storageFolder = await StorageFolder.GetFolderFromPathAsync(fullPath);
                var queryResult = storageFolder.CreateFileQueryWithOptions(queryOptions);

                var t = await queryResult.GetFilesAsync().AsTask();
                return (t.Select(sf => Path.Combine(path, sf.Name))).ToArray();
            }

            private static string FixSearchPattern(string searchPattern)
            {
                return searchPattern.Replace("*.", ".");
            }
        }
    }
#endif
