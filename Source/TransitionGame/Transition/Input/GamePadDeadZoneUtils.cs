using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Transition.Input
{
	internal static class GamePadDeadZoneUtils
	{
		private const float LeftStickDeadZoneSize = 7849.0f / 32767.0f;
		private const float RightStickDeadZoneSize = 8689.0f / 32767.0f;
		private const float TriggerDeadZoneSize = 30.0f / 255.0f;

		internal static Vector2 ApplyLeftStickDeadZone(float x, float y, GamePadDeadZone deadZoneMode)
		{
			return ApplyStickDeadZone(x, y, deadZoneMode, LeftStickDeadZoneSize);
		}
		internal static Vector2 ApplyRightStickDeadZone(float x, float y, GamePadDeadZone deadZoneMode)
		{
			return ApplyStickDeadZone(x, y, deadZoneMode, RightStickDeadZoneSize);
		}
		private static Vector2 ApplyStickDeadZone(float x, float y, GamePadDeadZone deadZoneMode, float deadZoneSize)
		{
			Vector2 result;
			switch (deadZoneMode)
			{
			    case GamePadDeadZone.IndependentAxes:
			        result.X = ApplyLinearDeadZone(x, 1.0f, deadZoneSize);
			        result.Y = ApplyLinearDeadZone(y, 1.0f, deadZoneSize);
			        break;

			    case GamePadDeadZone.Circular:
			        {
			            var num = (float)Math.Sqrt((x * x + y * y));
			            var num2 = ApplyLinearDeadZone(num, 1.0f, deadZoneSize);
			            var num3 = (num2 > 0f) ? (num2 / num) : 0f;
			            result.X = MathHelper.Clamp(x * num3, -1f, 1f);
			            result.Y = MathHelper.Clamp(y * num3, -1f, 1f);
			        }
			        break;
			    default:
			        result.X = ApplyLinearDeadZone(x, 1.0f, 0f);
			        result.Y = ApplyLinearDeadZone(y, 1.0f, 0f);
			        break;
			}
			return result;
		}
		internal static float ApplyTriggerDeadZone(float value, GamePadDeadZone deadZoneMode)
		{
			if (deadZoneMode == GamePadDeadZone.None)
			{
				return ApplyLinearDeadZone(value, 1.0f, 0f);
			}

			return ApplyLinearDeadZone(value, 1.0f, TriggerDeadZoneSize);
		}
		private static float ApplyLinearDeadZone(float value, float maxValue, float deadZoneSize)
		{
			if (value < -deadZoneSize)
			{
				value += deadZoneSize;
			}
			else
			{
				if (value <= deadZoneSize)
				{
					return 0f;
				}
				value -= deadZoneSize;
			}
			
            var value2 = value / (maxValue - deadZoneSize);
			return MathHelper.Clamp(value2, -1f, 1f);
		}
	}
}
