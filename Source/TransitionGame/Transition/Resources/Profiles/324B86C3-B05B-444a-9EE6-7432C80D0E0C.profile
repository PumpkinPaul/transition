{
  "InputKeys": {
    "Up": 38,
    "Down": 40,
    "Left": 37,
    "Right": 39,
    "Fire": 88,
    "Pause": 80,
    "UpKeyHint": "Up",
    "DownKeyHint": "Down",
    "LeftKeyHint": "Left",
    "RightKeyHint": "Right",
    "SelectKeyHint": "X",
    "FireKeyHint": "X",
    "PauseKeyHint": "P"
  },
  "Checkpoints": [],
  "FileName": "324B86C3-B05B-444a-9EE6-7432C80D0E0C",
  "Name": "Player 4",
  "Guid": "324B86C3-B05B-444a-9EE6-7432C80D0E0C",
  "ProfileType": 0,
  "SelectedShipName": "Ship1",
  "CompletedAchievements": [],
  "RequiresSave": false
}