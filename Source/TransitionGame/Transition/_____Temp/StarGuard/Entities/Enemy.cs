﻿using System;
using ProtoBuf;
using Transition._____Temp.StarGuard.EntityBlueprints;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Base for all enemies
    /// </summary>
    [ProtoContract]
    public abstract class Enemy : Entity
    {
        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        protected int Health;

        [ProtoMember(2)]
        protected EnemyBlueprint EnemyBlueprint;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        protected Enemy(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.Enemy;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            EnemyBlueprint = Game.EntityBlueprints.EnemyBlueprint(name);

            Health = Blueprint.InitialHealth;
            
            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization()
        {
            InitialiseNonSerialized();
        }

        protected override void OnCollisionWithEntity(Entity collider, float time)
        {
            if (collider.Classification == EntityClassification.Bullet)
            {
                //Bullet will have handled itself
                TakeDamage(collider);
            }
            else if (collider.Classification == EntityClassification.Player)
            {
                //Player will have handled itself
                TakeDamage(collider);
            }
        }

        void TakeDamage(Entity collider)
        {
            if (Blueprint.InitialHealth < 0)
            {
                PlayRicochet();
                return;
            }

            Health--;
            if (Health <= 0)
                Kill(collider);
            else
            {
                HitTicks = 6;

                CreateSparks();
                PlayRicochet();
            }
        }

        void CreateSparks()
        {
            CreateParticleSystem("EnemyHit");
        }

        void Kill(Entity collider)
        {
            Sound.Play(Game, Blueprint.ExplosionSfx, Position);

            if (OnKilled(collider))
            {
                CreateParticleSystems(Blueprint.ExplosionEffects);
                if (EnemyBlueprint.ScoreBonus > 0)
                    CreateScoreLabel();

                Deactivate();
            }
        }

        protected virtual bool OnKilled(Entity collider) 
        {
            return true;
        }

        protected void CreateScoreLabel()
        {
            var scoreLabel = Game.EntityFactory.Create<ScoreLabel>();
            if (scoreLabel != null)
            {
                scoreLabel.Position = Position;
                scoreLabel.Velocity.Y = 0.1f;
                scoreLabel.Score = EnemyBlueprint.ScoreBonus;
                scoreLabel.LifeTicks = 90;
                Game.World.Add(scoreLabel);
            }
        }

        protected bool IsPlayerInRange
        {
            get
            {
                var player = Game.World.Get<Player>(EntityKeys.Player);
                var delta = player.Position - Position;

                return Math.Abs(delta.X) < 11 && Math.Abs(delta.Y) < 6;
            }
        }
    }
}