using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.ResourceManagers;
using ControlsMap = System.Collections.Generic.Dictionary<string, Transition.MenuControls.MenuControl>;

namespace Transition.MenuControls
{
    public class DataBindingInfo
    {
        public string ControlPropertyName;
        public string BindingPropertyName;
    }

    /// <summary>
    /// Represents a label control.
    /// </summary>
    public class RepeatTemplate// : IEnumerable<MenuControl>
    {
        readonly ControlsMap _itemControls = new ControlsMap();
        readonly Dictionary<string, DataBindingInfo> _dataBindingInfoMap = new Dictionary<string, DataBindingInfo>();

        public Vector2 Position;
        public Vector2 Offset; 

        public RepeatTemplate(XElement element, MenuControl parent, BaseGame game)
        {
            var controlParser = new OverlayTemplateManager.ControlParser();

            Position = element.GetAttributeVector2("position", Position);
            Offset = element.GetAttributeVector2("offset", Offset);

            foreach (var nestedElement in element.Elements())
            {
                var control = controlParser.ParseControls("RepeatTemplate", "", _itemControls, nestedElement, "RepeatTemplate.ctor", parent, game);

                //var bindingInfo = control.GetBindingInfo();
                foreach (var attribute in nestedElement.Attributes())
                {
                    if (attribute.Value.StartsWith("{{", StringComparison.Ordinal) == false)
                        continue;

                    var propertyName = attribute.Value.Replace("{{", string.Empty).Replace("}}", string.Empty);
                    var bindingInfo = new DataBindingInfo
                    {
                        ControlPropertyName = attribute.Name.LocalName,
                        BindingPropertyName = propertyName
                    };

                    _dataBindingInfoMap[control.Name] = bindingInfo; 
                }  
            }
        }

        public Dictionary<string, MenuControl>.ValueCollection Controls => _itemControls.Values;

        public void DataBind(MenuControl control, object dataItem) 
        { 
            //Look up any properties that need binding
            if (_dataBindingInfoMap.ContainsKey(control.Name) == false)
                return;

            var bindingInfo = _dataBindingInfoMap[control.Name];

            var dataItemType = dataItem.GetType();
            var controlType = control.GetType();
            
            object value = null;
            var dataItemProperty = dataItemType.GetProperty(bindingInfo.BindingPropertyName);
            if (dataItemProperty != null)
            {
                value = dataItemProperty.GetValue(dataItem, BindingFlags.Default, null, null, null);
            }
            else 
            {
                var dataItemField = dataItemType.GetField(bindingInfo.BindingPropertyName);
                if (dataItemField != null)
                {
                    value = dataItemField.GetValue(dataItem);
                }
            }

            var controlProperty = controlType.GetProperty(bindingInfo.ControlPropertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            if (controlProperty != null)
            {
                controlProperty.SetValue(control, value, BindingFlags.Default, null, null, null);
            }
            else
            {
                var controlField = controlType.GetField(bindingInfo.ControlPropertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                controlField?.SetValue(control, value);
            }   
        }
    }
}