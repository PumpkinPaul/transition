using Microsoft.Xna.Framework.Graphics;
using Transition.Rendering;
using Transition.ResourceManagers;

namespace Transition
{
    /// <summary>
    /// A style to render cheesey hologram effects.
    /// </summary>
    public class HologramRenderStyle : RenderStyle
    {
        public static HologramRenderStyle Instance;

        public override RenderStyleType RenderStyleId => RenderStyleType.Hologram;

        protected override void OnRegister(EffectManager effectManager)
        {
            Instance = this;
            Effect = effectManager.GetEffect("HologramEffect");
        }

        public override void Setup(GraphicsDevice graphicsDevice)
        {
            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            Effect.Parameters["iGlobalTime"].SetValue(TransitionGame.Instance.GameTimeInSeconds);
        }
    }
}
