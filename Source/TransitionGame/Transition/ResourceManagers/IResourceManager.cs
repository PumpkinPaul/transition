namespace Transition.ResourceManagers
{
    /// <summary>
    /// A class to manage effect resources.
    /// </summary>
    public interface IResourceManager
    {
        void LoadContent();
    }
}
