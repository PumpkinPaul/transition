using Microsoft.Xna.Framework;

namespace Transition.Cameras
{
    /// <summary>
    /// Represents a base class for all cameras.
    /// </summary>
    /// 
    ///                    + Y   + Z (zoom in)
    ///                      |   (in to screen)
    ///                      |  /
    ///                      | /
    ///                      |/
    ///         -X --------------------- +X
    ///                     /|
    ///                    / |
    ///                   /  |
    ///    (out of screen)   |
    ///    - Z (zoom out)   - Y
    /// 
    public class Camera
    {
        protected readonly BaseGame Game;

        public float Rotation;
        public Vector3 Position;
        public Matrix View;

        public Camera(BaseGame game)
        {
            Game = game;
            Position.Z = -27.0f;
            View = Matrix.CreateTranslation(Position);
        }

        public virtual void Initialise() { }
        public virtual void Update() { }

        public Rect Bounds
        {
            get 
            { 
                var zoom = -Game.CameraManager.Zoom;

                var topLeft = (Game.RenderViewport.Unproject(new Vector3(0, 0, 0), Game.ProjectionMatrix, Matrix.Identity, Matrix.Identity) * zoom) + Position;
                var bottomRight = (Game.RenderViewport.Unproject(new Vector3(Game.RenderViewport.Bounds.Right, Game.RenderViewport.Bounds.Bottom, 0), Game.ProjectionMatrix, Matrix.Identity, Matrix.Identity) * zoom) + Position;
  
                return new Rect(topLeft.Y, topLeft.X, bottomRight.Y, bottomRight.X);
            }
        }

        public virtual void Render()
        {
            View = GetViewMatrix();
        }

        public Matrix GetViewMatrix()
        {
            return Matrix.CreateTranslation(new Vector3(-Position.X, -Position.Y, Position.Z)) * Matrix.CreateRotationZ(Rotation);
        }
    }
}
