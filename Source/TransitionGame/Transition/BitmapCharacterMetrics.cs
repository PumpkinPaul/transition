using System;

namespace Transition
{
    /// <summary>
    /// Represents information about a character.
    /// </summary>
    public class BitmapCharacterMetrics
    {				
		/// <summary>The amount of padding there is to the left of the character.</summary>
        public float PaddingLeft;
		
		/// <summary>The width of the character.</summary>
        /// <remarks>In the range 0...x...1</remarks>
        public float Width;
			
		/// <summary>.</summary>
        public float PaddingRight;
		
		/// <summary>.</summary>
        public Rect Rect;
		
        /// <summary>
		/// Creates a new instance of a BitmapCharacterMetrics.
		/// </summary>
		public BitmapCharacterMetrics(Rect rect)
		{
			Rect = rect;
		}

        /// <summary>
		/// Creates a new instance of a BitmapCharacterMetrics.
		/// </summary>
		public BitmapCharacterMetrics(float top, float left, float bottom, float right)
		{
			Rect = new Rect(top, left, bottom, right);
            
			this.Width = (right - left) / 26.0f;
            this.PaddingLeft = 0.0f;
			
			this.PaddingRight = 0.0f;
		}

		/// <summary>
		/// Creates a new instance of a BitmapCharacterMetrics.
		/// </summary>
		public BitmapCharacterMetrics(float paddingLeft, float width)
		{
			this.PaddingLeft = paddingLeft;
			this.Width = width;
			
			this.PaddingRight = (1.0f - paddingLeft - width);
		}
    }
}
