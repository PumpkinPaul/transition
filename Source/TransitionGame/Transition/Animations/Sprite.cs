namespace Transition.Animations
{
    /// <summary>
    /// Abstract base class for game phases (level complete, game won, etc)
    /// </summary>
    public class Sprite
    {				
        /// <summary>The Animation if any.</summary>
        Animation _animation;
        private int _frame;
        public float ChildXOffset;
        public float ChildYOffset;
        public int Sequence;
        public int Tick;
        public int Layer;
        public int Event;
        public bool Active;
        public bool Visible;
        public bool Paused;
        public TextureInfo TextureInfo;

        public void Init()
        {
            //TODO: Transition - Sprites
            //allocated = true;
            Reset();
            //owner = newOwner;
            Active = true;
            Visible = true;
            //image = null;
            //flash = false;
            //ox = 0;
            //oy = 0;
            //x = 0;
            //y = 0;
            //color[0].set(ReadableColor2.WHITE);
            //color[1].set(ReadableColor2.WHITE);
            //color[2].set(ReadableColor2.WHITE);
            //color[3].set(ReadableColor2.WHITE);
            //layer = 0;
            //subLayer = 0;
            //xscale = NO_SCALE;
            //yscale = NO_SCALE;
            //alpha = 255;
            //angle = 0;
            //mirrored = false;
            //flipped = false;
            //doChildOffset = false;
            //style = null;
            TextureInfo = null;
        }

        /**
         * Gets the animation.
         * @return Returns a Animation
         */
        public Animation GetAnimation() 
        {
            return _animation;
        }
	
        /**
         * Sets the animation.
         * @param animation The animation to set
         */
        public void SetAnimation(Animation animation, bool rewind = true) 
        {
            //if (animation != null) 
            //    Debug.Assert(animation.isCreated());
            
            _animation = animation;

            if (rewind)
                Rewind();
        }

        public void SetAppearance(IAppearance appearance) 
        {
            if (appearance == null) 
            {
                TextureInfo = null;
                SetAnimation(null);
                Rewind();
            } 
            else
            {
                appearance.ToSprite(this);
            }
        }

        public bool SetFrame(int frame)
        {
            _frame = frame;

            //TODO: Transition - Sprites
            //if (_frameList != null) 
            //    return updateFrame();
            
            return false;
        }

        public void Reset()
        {
            _animation = null;
            //TODO: Transition - Sprites
            //_frameList = null;
            Sequence = 0;
            _frame = 0;
            Tick = 0;
            Event = 0;
            Paused = false;
            ChildXOffset = 0;
            ChildYOffset = 0;
        }

        public void Rewind()
        {
            Sequence = 0;
            Tick = 0;
            Update();
        }

        public void Update()
        {
            // If the sprite is not active, don't do anything.
            // If there's no animation, we don't need to do anything.
            // If we're paused we don't need to do anything.
            if (!Active || _animation == null || Paused) {
                return;
            }
        
            _animation.Animate(this);
        }

        private bool UpdateFrame() 
        {
            //TODO: Transition - Sprites
            //if (_frame < 0 || _frame >= frameList.getNumResources()) 
            //    return false;
            
            //var newAppearance = (IAppearance) frameList.getResource(frame);
            //if ((_animation != null && newAppearance != _animation) || _animation == null) 
            //    return newAppearance.ToSprite(this);
            
            return false;
        }
    }
}

