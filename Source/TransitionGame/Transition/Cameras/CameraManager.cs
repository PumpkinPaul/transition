using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Transition.Cameras
{
    /// <summary>
    /// Manages the cameras.
    /// </summary>
    public class CameraManager
    {
        readonly List<Camera> _cameras = new List<Camera>();
		
		readonly List<CameraShake> _cameraShakes = new List<CameraShake>();
		
		/// <summary>The amount of units to shake the camera.</summary>
		float _currentShake;

        public float Zoom
        {
            get { return Camera.Position.Z; }
        }

        public Matrix ViewMatrix
        {
            get { return Camera.View; }
            set { Camera.View = value; }
        }

        public Camera Camera { get; set; }

        public void AddCamera(Camera camera)
        {
            _cameras.Add(camera);
        }

		/// <summary>
		/// Resets the camera position.
		/// </summary>
		public void Reset()
		{
            ViewMatrix = Matrix.CreateTranslation(new Vector3(0.0f, 0.0f, Zoom));
        }
					
		/// <summary>
		/// Shakes the camera with the desired strength over time.
		/// </summary>
		/// <remarks>Adds a new camera shake to the list.</remarks>
		public void ShakeCamera(float strength, int duration)
		{
            //TODO: Transition
            //if (_game.MenuManager.HasMenu)
            //    return;

			var strengthDelta = strength / duration;
			var shake = new CameraShake(strength, duration, strengthDelta);
			_cameraShakes.Add(shake);
		}
		
		/// <summary>
		/// .
		/// </summary>
		public void Update()
		{
            foreach (var camera in _cameras)
		        camera.Update();
		}
		
		/// <summary>
		/// .
		/// </summary>
		public void UpdateCameraShake()
		{
			//Calculate how much to 'shake' the camera this update.
			_currentShake = 0.0f;

			for (var i = 0; i < _cameraShakes.Count; ++i)
			{
				var shake = _cameraShakes[i];
				//If the current shake has finished remove it from the list
				if (shake.Duration == 0) 
				{
					_cameraShakes.RemoveAt(i);
					--i;
				}
				//Add the shake strength to the final strength - allows us to combine shakes of different strength.
				else 
				{
					--shake.Duration;
					_currentShake += shake.Strength;
					shake.Strength -= shake.StrengthDelta;
				}
			}
		}

        /// <summary>
		/// .
		/// </summary>
		public void Render()
		{
		    foreach (var camera in _cameras)
		        camera.Render();
        }
    }
}
