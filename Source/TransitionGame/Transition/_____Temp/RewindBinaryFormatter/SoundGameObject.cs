﻿using System;
using Microsoft.Xna.Framework.Audio;

namespace Transition._____Temp.RewindBinaryFormatter
{
    [Serializable]
    public class SoundEntity : Entity
    {
        int _position;

        [NonSerialized]
        DynamicSoundEffectInstance _dynamicSoundEffectInstance;

        void DynamicSoundBufferNeeded(object sender, EventArgs e)
        {
            if (_dynamicSoundEffectInstance == null)
                return;

            if (_position + TestPhaseRewindBinaryFormatter._audio.count <= TestPhaseRewindBinaryFormatter._audio.byteArray.Length)
            {
                _dynamicSoundEffectInstance.SubmitBuffer(TestPhaseRewindBinaryFormatter._audio.byteArray, _position, TestPhaseRewindBinaryFormatter._audio.count / 2);
                _dynamicSoundEffectInstance.SubmitBuffer(TestPhaseRewindBinaryFormatter._audio.byteArray, _position + TestPhaseRewindBinaryFormatter._audio.count / 2, TestPhaseRewindBinaryFormatter._audio.count / 2);

                _position += TestPhaseRewindBinaryFormatter._audio.count;
            }

            if (_position + TestPhaseRewindBinaryFormatter._audio.count > TestPhaseRewindBinaryFormatter._audio.byteArray.Length)
            {
               KillSound();
                //position = 0;
            }
        }

        private void KillSound()
        {
            if (_dynamicSoundEffectInstance != null)
            {
                _dynamicSoundEffectInstance.BufferNeeded -= DynamicSoundBufferNeeded;
                _dynamicSoundEffectInstance.Stop(true);
                _dynamicSoundEffectInstance = null;
                //_eventsId--;
            }
        }

        public override void BeforeRewind()
        {
            KillSound();
        }

        public override void StateLoaded(bool rewinding) 
        {
            base.StateLoaded(rewinding);

            if (!rewinding)
                Play();
        }

        public override void Update(bool wasRewinding)
        {
            //Should I play the sound?
            if (wasRewinding)
                Play();
        }

        private void EnsureSource()
        {
            if (_dynamicSoundEffectInstance == null)
            {
                //_dynamicSoundEffectInstance = new DynamicSoundEffectInstance(22050, (AudioChannels)1);
                _dynamicSoundEffectInstance = new DynamicSoundEffectInstance(44100, (AudioChannels)1);
                _dynamicSoundEffectInstance.BufferNeeded += DynamicSoundBufferNeeded;
            }
        }

        public void Play()
        {
            EnsureSource();

            _dynamicSoundEffectInstance.Play();
        }
    }
}