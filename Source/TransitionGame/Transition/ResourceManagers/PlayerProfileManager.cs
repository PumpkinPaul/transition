using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Storage;
using Newtonsoft.Json;
using Nuclex.Input;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Responsible for managing player profiles.
	/// </summary>
    public class PlayerProfileManager
    {
        /// <summary>
        /// Responsible for profile information.
        /// </summary>
        class ProfileInfo : IProfileInfo
        {
            /// <summary>Gets or sets the display name of the profile.</summary>
            public string DisplayName { get; set; }

            /// <summary>Gets or sets the id of the texture to use for the gamerpic of the profile.</summary>
            public TextureInfo GamepicTextureInfo { get; set; }

            /// <summary>Gets or sets whether the profile is signed in.</summary>
            public bool IsSignedIn { get; set; }
        }

	    readonly TextureManager _textureManager;

        readonly string _contentFolder;
        const string StorageFolder = "Profiles";

        const string ProfileFileExtension = "profile";
        #if WINDOWS_STOREAPP
            const string ProfileSearchPattern = "." + ProfileFileExtension;
        #else
            const string ProfileSearchPattern = "*." + ProfileFileExtension;
        #endif
		
		///<summary>A list of player profiles.</summary>
        readonly List<PlayerProfile> _playerProfiles = new List<PlayerProfile>();

        readonly TextureInfo[] _defaultGamerPicTextureInfos = new TextureInfo[4];

        readonly ProfileInfo[] _profileInfos = {
            new ProfileInfo { DisplayName = "Player 1", GamepicTextureInfo = null, IsSignedIn = false},
            new ProfileInfo { DisplayName = "Player 2", GamepicTextureInfo = null, IsSignedIn = false},
            new ProfileInfo { DisplayName = "Player 3", GamepicTextureInfo = null, IsSignedIn = false},
            new ProfileInfo { DisplayName = "Player 4", GamepicTextureInfo = null, IsSignedIn = false}
        };

	    /// <summary>
	    /// Creates a new instance of a PlayerProfileManager.
	    /// </summary>
	    /// <param name="textureManager"></param>
	    public PlayerProfileManager(TextureManager textureManager)
        {
            if (textureManager == null)
                throw new ArgumentNullException(nameof(textureManager));

			_textureManager = textureManager;
			
			_contentFolder = Platform.ResourcesPath;
        }
		
        /// <summary>
        /// Gets the list of player profiles.
        /// </summary>
        public List<PlayerProfile> Profiles => _playerProfiles;

	    /// <summary>
        /// Loads content from title storage.
        /// </summary>
        public void LoadContent()
        {
            LoadProfilesInFolder(_contentFolder);
            SortProfilesIntoAlphabeticalOrder();

            for (var i = 0; i < 4; ++i)
            {
                _defaultGamerPicTextureInfos[i] = _textureManager.GetTextureInfo("DefaultGamerpic" + (i + 1));
                _profileInfos[i].GamepicTextureInfo = _defaultGamerPicTextureInfos[i];
            }
        }

        public void LoadFromStorage(StorageContainer storageContainer)
        {
            if (string.IsNullOrEmpty(StorageFolder) == false && storageContainer.DirectoryExists(StorageFolder) == false)
            {
                storageContainer.CreateDirectory(StorageFolder);
                return;
            }

            var filenames = storageContainer.GetFileNames(Path.Combine(StorageFolder, ProfileSearchPattern));

			if (filenames !=  null)
			{
	            foreach (var filename in filenames)
	            {
	                using (var stream = storageContainer.OpenFile(filename, FileMode.Open, FileAccess.Read))
	                {
	                    var playerProfile = LoadProfile(stream);
	
	                    if (playerProfile != null)
	                        AddProfileIfNotExists(playerProfile);
	                }
	            }
			}
        }

        /// <summary>
        /// Loads profile data into the manager.
        /// </summary>
        /// <param name="folder">Location to load from.</param>
        public void LoadProfilesInFolder(string folder)
        {
            var path = Path.Combine(folder, StorageFolder);

            if (Directory.Exists(path) == false)
                return;
            
            foreach (var filename in Directory.GetFiles(path, ProfileSearchPattern))
            {
                var name = filename;
                #if PSM
	                name = filename.Replace("./Application/", "");
                #endif

                using (var stream = TitleContainer.OpenStream(name))
                {
                    var playerProfile = LoadProfile(stream);

                    if (playerProfile != null)
                        AddProfileIfNotExists(playerProfile);
                }
            }
        }
        
        /// <summary>
        /// Loads a player profile.
        /// </summary>
        /// <param name="stream">Stream to load profile from.</param>
        /// <returns>A player profile.</returns>
        private static PlayerProfile LoadProfile(Stream stream)
        {
            using (var file = new StreamReader(stream))
            {
                var serializer = new JsonSerializer();
                return (PlayerProfile)serializer.Deserialize(file, typeof(PlayerProfile));
            }
        }

        /// <summary>
        /// Adds a <see cref="PlayerProfile">PlayerProfile</see> if it doesn't already exist.
        /// </summary>
        /// <param name="playerProfile">The <see cref="PlayerProfile">PlayerProfile</see> to add.</param>
        private void AddProfileIfNotExists(PlayerProfile playerProfile)
        {
            for (var i = 0; i < _playerProfiles.Count; ++i)
            {
                if (_playerProfiles[i].Name != playerProfile.Name)
                    continue;
                
                //Replace the current profile.
                _playerProfiles[i] = playerProfile;
                return;
            }

            //Doesn't already exist so add it.
            _playerProfiles.Add(playerProfile);
        }

        /// <summary>
        /// Sorts the player profiles into alphabetical order.
        /// </summary>
        private void SortProfilesIntoAlphabeticalOrder()
        {
            _playerProfiles.Sort((a, b) => string.Compare(a.Name, b.Name, StringComparison.Ordinal));
        }

        /// <summary>
        /// Gets the info associated with a player index (gamepad)
        /// </summary>
        /// <param name="playerIndex">The index of the player.</param>
        /// <returns>The info associated with a gamepad.</returns>
        public IProfileInfo GetProfileInfo(ExtendedPlayerIndex playerIndex)
        {
            return _profileInfos[0]; //Always get first profile - always player 1 (for now? May make a profile management section - maybe)
        }

        /// <summary>
        /// Gets the profile with a player index (gamepad)
        /// </summary>
        /// <param name="name">The name of the profile.</param>
        /// <returns>A profile.</returns>
        public PlayerProfile GetProfile(string name)
        {
            foreach (var profile in _playerProfiles)
            {
                if (profile.Name == name)
                    return profile;
            }

            return null;
        }

        /// <summary>
        /// Creates a new player profile.
        /// </summary>
        /// <param name="name">The name of the profile.</param>
        public PlayerProfile CreateProfile(string name)
        {
            var playerProfile = new PlayerProfile();
            
            playerProfile.Guid = Guid.NewGuid().ToString("N");
            playerProfile.Name = name;
            playerProfile.FileName = Path.Combine(StorageFolder, playerProfile.Guid + "." + ProfileFileExtension);
            playerProfile.RequiresSave = false;
            playerProfile.ProfileType = ProfileType.Local;

            AddProfileIfNotExists(playerProfile);

            return playerProfile;
        }
    }
}
