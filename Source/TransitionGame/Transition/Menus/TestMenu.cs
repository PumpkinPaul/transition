using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>Represents a menu for larking about innit.
    /// </summary>
    public class TestMenu : Menu
    {
        public TestMenu(BaseGame game, string title) : base(game, title) { }

        protected override void OnConfigureMenu()
        {
            //var label = GetControl<PanelControl>("panel2");

            //var leftButton = GetControl<TextButtonControl>("leftButton");
            //leftButton.Click += (sender, e) => label.StartTransition("left");

            //var rightButton = GetControl<TextButtonControl>("rightButton");
            //rightButton.Click += (sender, e) => { label.StartTransition("right"); };
        }
    }
}
