using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Transition.ActorDatas;
using Transition.Rendering;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Manages actor configuration data.
	/// </summary>
    public class DataManager : ResourceManager
    {
        public override string FolderName => "Actors";

	    const string Separator = ",";
        readonly char[] _seperatorArray = Separator.ToCharArray();

        readonly string _namespace;

        readonly TextureManager _textureManager;
        readonly AnimationManager _animationManager;
        readonly ParticleTemplateManager _particleTemplateManager;

	    readonly Dictionary<string, ActorData> _actorDictionary = new Dictionary<string, ActorData>();
        readonly Dictionary<string, EnemyData> _enemyDictionary = new Dictionary<string, EnemyData>();
        readonly Dictionary<string, EnemyBulletData> _enemyBulletDictionary = new Dictionary<string, EnemyBulletData>();
	    readonly Dictionary<string, BulletData> _bulletDictionary = new Dictionary<string, BulletData>();
        readonly Dictionary<string, ShipData> _shipDictionary = new Dictionary<string, ShipData>();
	    readonly Dictionary<string, WeaponData> _weaponDictionary = new Dictionary<string, WeaponData>();

		/// <summary>
        /// Creates a new instance of a DataManager.
        /// </summary>
        public DataManager(string @namespace, TextureManager textureManager, AnimationManager animationManager, ParticleTemplateManager particleTemplateManager)
        {
			if (textureManager == null)
                throw new ArgumentNullException(nameof(textureManager));

            if (animationManager == null)
                throw new ArgumentNullException(nameof(animationManager));

            if (particleTemplateManager == null)
                throw new ArgumentNullException(nameof(particleTemplateManager));

            _namespace = @namespace;

            _textureManager = textureManager;
            _animationManager = animationManager;
            _particleTemplateManager = particleTemplateManager;
	    }

        Type GetActorType(string className)
        {
            return Type.GetType(_namespace + className);
        }

        /// <summary>
        /// Loads all actor configuration data into the data manager.
        /// </summary>
        public override void LoadContent()
        {
            var folder = Path.Combine(Platform.ResourcesPath, FolderName);

            foreach (var filename in Directory.GetFiles(folder, "*.xml"))
                LoadActors(filename);
        }

        private void LoadActors(string filename)
        {
            var xdocument = XDocument.Load(filename);
			
            if (xdocument.Root == null)
                throw new XmlException("Missing Root element in " + filename);

			//Read in all of the actors...
            foreach (var actorElement in xdocument.Root.Elements("actor"))
            {
                //Specified in file but not utilised as of yet - maybe use for validation of elements later?
                var actorClass = actorElement.GetAttributeString("class");
                var type = GetActorType(actorClass);

                var name = actorElement.GetAttributeString("name");

                foreach (var element in actorElement.Elements())
                {
                    switch (element.Name.LocalName)
                    {
                        case "actorData":
                            ParseActorData(type, name, element);
                            break;

                        case "enemyBulletData":
                            ParseEnemyBulletData(type, name, element);
                            break;

                        case "enemyData":
                            ParseEnemyData(type, name, element);
                            break;

                        case "bulletData":
                            ParseBulletData(type, name, element);
                            break;

                        case "shipData":
                            ParseShipData(type, name, element);
                            break;

                        case "weaponData":
                            ParseWeaponData(type, name, element);
                            break;
                    }
                }
            }
        }

        private void ParseActorData(Type actorType, string name, XElement actorDataElement)
	    {
            //Can override name in sub elements
	        name = name ?? actorDataElement.GetAttributeString("name");

	        var actorData = ActorData(name) ?? new ActorData();

            actorData.Name = name;
            actorData.ActorType = actorType;

            //<layers>
            //    <layer appearance="StandardAlienBullet" />
            //</layers>

            var layersElement = actorDataElement.Element("layers");
            if (layersElement != null)
            {
                var layers = layersElement.Elements("layer").ToArray();
                actorData.Layers = new ActorData.Layer[layers.Length];
                var layerId = 0;
                foreach(var layerElement in layers)
                {
                    var layer = new ActorData.Layer();
                    actorData.Layers[layerId] = layer;
                    var appearance = layerElement.GetAttributeString("appearance");
	                if (string.IsNullOrEmpty(appearance) == false)
                    {
                        if (appearance.EndsWith(".animation", StringComparison.Ordinal))
	                        layer.Appearance = _animationManager.Get(appearance);
                        else
                            layer.Appearance = _textureManager.GetTextureInfo(appearance);
                    }

                    layer.DrawLayer = layerElement.GetAttributeInt32("layer", Layers.Actor);

                    layerId++;
                }
            }

	        actorData.InitialHealth = actorDataElement.GetAttributeInt32("health");
	        actorData.CollisionRadius = actorDataElement.GetAttributeSingle("collisionRadius");
	        actorData.SpawnScaleIncrement = actorDataElement.GetAttributeSingle("spawnScale");
	        actorData.ExplosionSfx = actorDataElement.GetAttributeString("explosionFx");
	        actorData.BaseSize = actorDataElement.GetAttributeVector2("baseSize");

	        AddParticleSystemsOfType(actorDataElement, ParticleSystemType.Expired, actorData.ExpiredEffects);
	        AddParticleSystemsOfType(actorDataElement, ParticleSystemType.Explosion, actorData.ExplosionEffects);
	        AddParticleSystemsOfType(actorDataElement, ParticleSystemType.Spawn, actorData.SpawnEffects);
	        AddParticleSystemsOfType(actorDataElement, ParticleSystemType.Trail, actorData.TrailEffects);

	        var simpleTrailElement = actorDataElement.Element("simpleTrail");
	        if (simpleTrailElement != null)
	        {
	            actorData.SimpleTrail = new SimpleTrailEffectData
	            {
	                HeadColor = simpleTrailElement.GetAttributeColor("headColor"),
	                TailColor = simpleTrailElement.GetAttributeColor("tailColor"),
	                HeadWidth = simpleTrailElement.GetAttributeSingle("headWidth"),
	                TailWidth = simpleTrailElement.GetAttributeSingle("tailWidth"),
	                MaxLength = simpleTrailElement.GetAttributeSingle("maxLength")
	            };
	        }

	        _actorDictionary[name] = actorData;
	    }

        /// <summary>
        /// Load the alien configuration data.
        /// </summary>
        private void ParseEnemyBulletData(Type actorType, string name, XElement alienBulletDataElement)
        {
            //Can override name in sub elements
            name = name ?? alienBulletDataElement.GetAttributeString("name");

            var enemyBulletData = EnemyBulletData(name) ?? new EnemyBulletData();
            enemyBulletData.Name = name;

            enemyBulletData.Visible = alienBulletDataElement.GetAttributeBool("visible");
            enemyBulletData.Explode = alienBulletDataElement.GetAttributeBool("explode");

            var soundsElement = alienBulletDataElement.Element("spawnSounds");
            if (soundsElement != null)
            {
                foreach (var itemElement in soundsElement.Elements("item"))
                    enemyBulletData.Sounds.Add(itemElement.Value);

            }

            _enemyBulletDictionary[name] = enemyBulletData;
        }

        private void ParseEnemyData(Type actorType, string name, XElement enemyDataElement)
	    {
            //Can override name in sub elements
            name = name ?? enemyDataElement.GetAttributeString("name");

            var enemyData = EnemyData(name) ?? new EnemyData();

	        enemyData.ScoreBonus = enemyDataElement.GetAttributeInt32("scoreBonus");
	        enemyData.MoneyBonus = enemyDataElement.GetAttributeInt32("moneyBonus");
	        enemyData.RageModifier = enemyDataElement.GetAttributeSingle("rageModifier", 1.0f);

	        var speedElement = enemyDataElement.Element("speed");
	        ParseDifficultyElementSingle(speedElement, enemyData.Speed);

            _enemyDictionary[name] = enemyData;
	    }
		
	    private void ParseBulletData(Type actorType, string name, XElement bulletDataElement)
	    {
            //Can override name in sub elements
            name = name ?? bulletDataElement.GetAttributeString("name");

	        var bulletData = BulletData(name) ?? new BulletData();

	        bulletData.ActorClass = actorType;
	        bulletData.Name = name;

	        var speed = bulletDataElement.GetAttributeString("speed");
	        if (speed.Contains(Separator))
	        {
	            var components = speed.Split(_seperatorArray, StringSplitOptions.RemoveEmptyEntries);
	            bulletData.SpeedMin = ConversionHelper.ToSingle(components[0].Trim());
	            bulletData.SpeedMax = ConversionHelper.ToSingle(components[1].Trim());
	        }
	        else
	        {
	            bulletData.SpeedMin = bulletDataElement.GetAttributeSingle("speed");
	            bulletData.SpeedMax = bulletData.SpeedMin;
	        }

	        bulletData.Friction = bulletDataElement.GetAttributeSingle("friction");
	        bulletData.Spread = bulletDataElement.GetAttributeSingle("spread");
	        bulletData.LifeTicks = bulletDataElement.GetAttributeInt32("lifeTicks");
	        bulletData.FastMoving = bulletDataElement.GetAttributeBool("fastMoving");
	        bulletData.Damage = new List<BulletUpgrade>();
	        var damagesElement = bulletDataElement.Element("damages");
	        if (damagesElement != null)
	        {
	            foreach (var itemElement in damagesElement.Elements("item"))
	            {
	                var bulletUpgrade = new BulletUpgrade
	                {
	                    Damage = itemElement.GetAttributeInt32("damage"),
	                    Cost = itemElement.GetAttributeInt32("cost")
	                };
	                bulletData.Damage.Add(bulletUpgrade);
	            }
	        }

	        _bulletDictionary[name] = bulletData;
	    }

        private void ParseShipData(Type actorType, string name, XElement shipDataElement)
	    {
            name = name ?? shipDataElement.GetAttributeString("name");

	        var shipData = ShipData(name) ?? new ShipData();
	        shipData.Name = name;

	        var menuTextureAsset = shipDataElement.GetAttributeString("menuTexture");
            if (string.IsNullOrEmpty(menuTextureAsset) == false)
	            shipData.MenuTextureInfo = _textureManager.GetTextureInfo(menuTextureAsset);

	        shipData.Classification = shipDataElement.GetAttributeString("classification");
	        shipData.Description = shipDataElement.GetAttributeString("description");
	        shipData.AccelerationX = shipDataElement.GetAttributeSingle("accelerationX");
	        shipData.AccelerationY = shipDataElement.GetAttributeSingle("accelerationY");
	        shipData.MaxVelocityX = shipDataElement.GetAttributeSingle("maxVelocityX");
	        shipData.MaxVelocityY = shipDataElement.GetAttributeSingle("maxVelocityY");
	        shipData.FrictionX = shipDataElement.GetAttributeSingle("frictionX");
	        shipData.FrictionY = shipDataElement.GetAttributeSingle("frictionY");
	        shipData.WeaponName = shipDataElement.GetAttributeString("weaponName");
	        shipData.Nuke = shipDataElement.GetAttributeString("nuke");
	        shipData.RageType = shipDataElement.GetAttributeEnum<RageType>("rage");
            
            shipData.WalkLeftAnimation= shipDataElement.GetAttributeString("walkLeftAnimation");
            shipData.WalkRightAnimation = shipDataElement.GetAttributeString("walkRightAnimation");
            shipData.FaceLeftAnimation = shipDataElement.GetAttributeString("faceLeftAnimation");
            shipData.FaceRightAnimation = shipDataElement.GetAttributeString("faceRightAnimation");

	        AddParticleSystemsOfType(shipDataElement, ParticleSystemType.Explosion, shipData.NukeEffects);

	        _shipDictionary[name] = shipData;
	    }

        private void ParseWeaponData(Type actorType, string name, XElement weaponDataElement)
	    {
	        var weaponData = WeaponData(name) ?? new WeaponData();

	        weaponData.Description = weaponDataElement.GetAttributeString("description");
	        weaponData.BulletName = weaponDataElement.GetAttributeString("bulletName");

	        var fireRatesElement = weaponDataElement.Element("fireRates");
	        if (fireRatesElement != null)
	        {
	            foreach (var itemElement in fireRatesElement.Elements("item"))
	            {
	                var weaponUpgrade = new WeaponUpgrade
	                {
	                    FireRate = itemElement.GetAttributeInt32("fireRate"),
	                    Cost = itemElement.GetAttributeInt32("cost")
	                };
	                weaponData.FireRates.Add(weaponUpgrade);
	            }
	        }

	        _weaponDictionary[name] = weaponData;
	    }

	    /// <summary>
		/// Gets ActorData for a class of actor.
		/// </summary>
        public ActorData ActorData(string name)
        {
            return _actorDictionary.ContainsKey(name) ? _actorDictionary[name] : null;
        }

		/// <summary>
		/// Gets EnemyData for a class of actor.
		/// </summary>
        public EnemyData EnemyData(string name)
		{
            return _enemyDictionary.ContainsKey(name) ? _enemyDictionary[name] : null;
		}

        /// <summary>
		/// Gets AlienBulletData for a class of alien.
		/// </summary>
        public EnemyBulletData EnemyBulletData(string name)
		{
		    return _enemyBulletDictionary.ContainsKey(name) ? _enemyBulletDictionary[name] : null;
		}

	    /// <summary>
		/// Gets BulletData for a class of actor.
		/// </summary>
        public BulletData BulletData(string name)
	    {
	        return _bulletDictionary.ContainsKey(name) ? _bulletDictionary[name] : null;
	    }

	    /// <summary>
		/// Gets ShipData for a class of actor.
		/// </summary>
        public ShipData ShipData(string name)
	    {
	        return _shipDictionary.ContainsKey(name) ? _shipDictionary[name] : null;
	    }

        /// <summary>
		/// Gets ShipData for a class of actor.
		/// </summary>
        public IEnumerable<ShipData> ShipDatasxxx()
	    {
	        return _shipDictionary.Values;
	    }

		/// <summary>
		/// Gets WeaponData for a class of actor.
		/// </summary>
        public WeaponData WeaponData(string name)
		{
		    return _weaponDictionary.ContainsKey(name) ? _weaponDictionary[name] : null;
		}

        private void AddParticleSystemsOfType(XElement parentElement, ParticleSystemType requiredType, IList<ParticleEffect> particleEffects)
        {
            var particleSystemsElement = parentElement.Elements("particleSystems");

            //Read in all the data...
            foreach (var particleSystemElement in particleSystemsElement.Elements("particleSystem"))
            { 
                var systemType = particleSystemElement.GetAttributeEnum<ParticleSystemType>("type");
                if (requiredType != systemType)
                    continue;

                particleEffects.Add(ParseParticleSystem(particleSystemElement));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private ParticleEffect ParseParticleSystem(XElement particleSystemElement)
        {
            //Read in all the data...
            var name = particleSystemElement.GetAttributeString("name");

            var particleEffect = new ParticleEffect
            {
                Type = particleSystemElement.GetAttributeEnum<ParticleSystemType>("type"),
                Template = _particleTemplateManager.GetTemplate(name),
                Delay = particleSystemElement.GetAttributeSingle("delay"),
                Lifetime = particleSystemElement.GetAttributeSingle("lifetime"),
                Offset = particleSystemElement.GetAttributeVector2("offset")
            };

            return particleEffect;
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ParseDifficultyElementInt32(XElement element, IList<int> array)
        {
            if (array.Count != TransitionGame.MaxDifficultyLevels)
                throw new XmlException("Incorrect difficulty array length for element:" + element.Name);

            if (element == null)
                return;

            array[(int)Difficulty.Easy] = element.GetAttributeInt32("easy");
            array[(int)Difficulty.Normal] = element.GetAttributeInt32("medium");
            array[(int)Difficulty.Hard] = element.GetAttributeInt32("hard");
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ParseDifficultyElementSingle(XElement element, IList<float> array)
        {
            if (array.Count != TransitionGame.MaxDifficultyLevels)
                throw new XmlException("Incorrect difficulty array length for element:" + element.Name);

            if (element == null)
                return;

            array[(int)Difficulty.Easy] = element.GetAttributeSingle("easy");
            array[(int)Difficulty.Normal] = element.GetAttributeSingle("medium");
            array[(int)Difficulty.Hard] = element.GetAttributeSingle("hard");
        }
    }
}
