using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Transition.Actors;
using Transition.Rendering;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public class Scene
    {       
        /// <summary>The game.</summary>
        private readonly TransitionGame _game;

        /// <summary>The list of actors in the scene.</summary>
        private readonly List<Actor> _actors = new List<Actor>(1024);

        /// <summary>The list of particles in the scene.</summary>
        private readonly List<Particle> _particles = new List<Particle>(4096);
        private int _startParticleId = -1;

        /// <summary>A list of potential collidable actors this update.</summary>
        private readonly List<Actor> _potentialColliders = new List<Actor>(256);

        /// <summary>A list of collided actors this update.</summary>
        private readonly List<CollisionPacket> _actualColliders = new List<CollisionPacket>(16);

        readonly CollisionPacketComparer _collisionPacketComparer = new CollisionPacketComparer();

        private int ActiveParticleCount { get; set; }
        private int ActorCount { get; set; }

        /// <summary>
        /// Creates a new instance of a Scene.
        /// </summary>
        /// <param name="game">The game.</param>
        public Scene(TransitionGame game)
        {
            _game = game;
        }

        /// <summary>
        /// Gets the maximum number of particles in the scene.
        /// </summary>
        public int MaxParticles => _particles.Count;

        public void InitialisePlaySession(IReadableCheckpoint checkpoint)
        {

        }

        public void DestroyPlaySession()
        {
            RemoveAllActors();
            KillParticles();
        }

		/// <summary>
        /// Creates the particles used for the particle system.
        /// </summary>
		public void CreateParticles(int count)
		{
			_particles.Clear();
			
			for(var i = 0; i < count; ++i)
				_particles.Add(new Particle(_game));
		}
		
		/// <summary>
        /// Gets the next availab;e particle.
        /// </summary>
        public Particle NextParticle()
        {
            ++_startParticleId;
            if (_startParticleId >= _particles.Count)
                _startParticleId = 0;

            return _particles[_startParticleId];

        }

        /// <summary>
        /// Adds an actor to the scene.
        /// </summary>
        /// <param name="actor">The actor to add to the scene.</param>
        public void AddActor(Actor actor)
        {			
            _actors.Add(actor);

            actor.Scene = this;			
            actor.AddedToScene();
        }
				
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            RemoveInactiveActors();
            UpdateActors(gameTime);
            UpdateParticles(gameTime);
            TestForCollisions();
            ProcessCollisions();
            MoveActors();
            PostUpdateActors();

            ActorCount = _actors.Count;
        }
		
        /// <summary>
        /// Updates all actors in the scene.
        /// </summary>
        /// <param name="gameTime"></param>
        private void UpdateActors(GameTime gameTime)
        {
            for (var i = 0; i < _actors.Count; ++i)
                _actors[i].Update(gameTime);
        }

        /// <summary>
        /// Allow all actors to do something after the Update PHASE (not just update, it's update, apply forces, move, constrain, etc).
        /// </summary>
        private void PostUpdateActors()
        {
            for (var i = 0; i < _actors.Count; ++i)
            {
                if (_actors[i].Active)
                    _actors[i].PostUpdate();
            }

        }

        private void UpdateParticles(GameTime gameTime)
        {
            ActiveParticleCount = 0;

            var timeDelta = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000;

            foreach (var particle in _particles)
            {
                if (particle.Active == false)
                    continue;

                ActiveParticleCount++;
                particle.Update(timeDelta, 0.0f);
            }
        }

        /// <summary>
        /// Adds an actor to the list of collidable actors.
        /// </summary>
        /// <param name="collider"></param>
        public void AddCollider(Actor collider)
        {
            _potentialColliders.Add(collider);
        }

        /// <summary>
        /// 
        /// </summary>
        void TestForCollisions()
        {
            for (var i = 0; i < _potentialColliders.Count; ++i)
            {
                for (var j = i + 1; j < _potentialColliders.Count; ++j)
                {
                    var actor1 = _potentialColliders[i];
                    var actor2 = _potentialColliders[j];

                    ////Handle a hits b and b hits a check as we only add colliders one way
                    if ((_game.CollisionMapManager.TestTypesCollide(actor1.ActorClassification, actor2.ActorClassification)) == false)
                        continue;

                    float time;
                    if (Collisions.TestSweptCircleCircle(actor1.Position, actor1.Velocity, actor1.CollisionRadius, actor2.Position, actor2.Velocity, actor2.CollisionRadius, out time))
                        _actualColliders.Add(new CollisionPacket(actor1, actor2, time));
                }
            }

            _potentialColliders.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        void ProcessCollisions()
        {
            //Sort collisions based on time.
            _actualColliders.Sort(_collisionPacketComparer);//(cp1, cp2) => Comparer<float>.Default.Compare(cp1.Time, cp2.Time));

            //Process each collison
            foreach (var packet in _actualColliders)
                packet.Actor1.Collide(packet.Actor2, packet.Time);

            _actualColliders.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        void RemoveInactiveActors()
        {
            foreach (var actor in _actors)
            {
                if (actor.Active == false)
                    actor.RemovedFromScene();
            }

            for (var i = _actors.Count - 1; i >= 0 ; --i)
            {
                if (_actors[i].Active == false)
                    _actors.RemoveAt(i);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void RemoveAllActors()
        {
            for(var i = 0; i < _actors.Count; ++i)
            {
                var actor = _actors[i];
                actor.RemovedFromScene();
            }

            _actors.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        public void ExplodeActorsOfType(Type type)
        {
            for (var i = 0; i < _actors.Count; ++i)
            {
                var actor = _actors[i];
                if (actor.GetType() != type)
                    continue;
                
                actor.Explode();
                actor.Deactivate();
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public void ExplodeActorsOfClassification(ActorClassification classification)
        {
            for (var i = 0; i < _actors.Count; ++i)
            {
                var actor = _actors[i];
                if (actor.ActorClassification != classification)
                    continue;
                
                actor.Explode();
                actor.Deactivate();
            }
        }

        public void SmartBomb()
        {
            for (var i = 0; i < _actors.Count; ++i)
            {
                var actor = _actors[i];

                if (actor.IsNukeable == false)
                    continue;

                if (actor.IsOnScreen == false)
                    continue;

                actor.Explode();
                actor.Deactivate();
            }
        }

        public Alien FindNearestUntargettedAlien(Vector2 position, int dir)
        {
	        Alien nearestAlien = null;
	        var nearestDistance = 99999.0f;

	        foreach (var actor in _actors)
	        {
	            if (!actor.Active || actor.ActorClassification != ActorClassification.Alien || !actor.Collidable || !actor.IsOnScreen || actor.Health == Actor.InfiniteHealth)
	                continue;

	            if (((Alien) actor).IsTargetted) 
                    continue;

	            if (dir != 0) 
	            {
	                var sy = position.Y;
	                var ay = actor.Position.Y;

	                if (dir < 0 && sy < ay)
	                    continue;
	                if (dir > 0 && sy > ay)
	                    continue;
	            }

	            var d = (actor.Position - position).Length();
	            if (nearestAlien != null && d >= nearestDistance)   
                    continue;

	            nearestAlien = (Alien)actor;
	            nearestDistance = d;
	        }

            return nearestAlien;
        }
		
        /// <summary>
        /// 
        /// </summary>
        private void MoveActors()
        {
            for (var i = 0; i < _actors.Count; ++i)
            {
                _actors[i].Move();
                ConstrainActor(_actors[i]);
            }
        }

		/// <summary>
        /// Draws all particles in the scene.
        /// </summary>
		public void KillParticles()
		{
			foreach (var particle in _particles)
			{
				if (particle.Active)
					particle.Kill();
			}
		}
		
        /// <summary>
        /// Constrains an actor to a universe using clamping behaviour.
        /// </summary>
        /// <param name="actor">The actor to constrain.</param>
        /// <returns>True if the actor had left the Universe and had to be constrained; false otherwise</returns>
        private void ConstrainActor(Actor actor)
        {
            var constrainedLeft = false;
            var constrainedRight = false;
            var constrainedTop = false;
            var constrainedBottom = false;

            var rect = _game.LevelManager.Bounds;
            rect.Top -= (actor.CollisionRadius + actor.ConstrainPadding.Y);
            rect.Left += (actor.CollisionRadius + actor.ConstrainPadding.X);
            rect.Bottom += (actor.CollisionRadius + actor.ConstrainPadding.Y);
            rect.Right -= (actor.CollisionRadius + actor.ConstrainPadding.X);

            if (actor.HorizontalConstrainBehaviour != ConstrainBehaviour.None)
            {
                if (actor.Position.X < rect.Left)
                {
                    actor.Position = new Vector2(rect.Left, actor.Position.Y);
                    constrainedLeft = true;
                }
                else if (actor.Position.X > rect.Right)
                {
                    actor.Position = new Vector2(rect.Right, actor.Position.Y);
                    constrainedRight = true;
                }

                if (constrainedLeft || constrainedRight)
                {
                    switch (actor.HorizontalConstrainBehaviour)
                    {
                        case ConstrainBehaviour.Deactivate:
                            actor.Deactivate();
                            break;

                        case ConstrainBehaviour.Bounce:
                            actor.Velocity.X = -actor.Velocity.X;
                            actor.Direction += MathHelper.PiOver2;
                            break;

                        case ConstrainBehaviour.Clamp:
                            actor.Velocity.X = 0.0f;
                            break;
                    }
                }
            }

            if (actor.VerticalConstrainBehaviour != ConstrainBehaviour.None)
            {
                if (actor.Position.Y > rect.Top)
                {
                    actor.Position = new Vector2(actor.Position.X, rect.Top);
                    constrainedTop = true;
                }
                else if (actor.Position.Y < rect.Bottom)
                {
                    actor.Position = new Vector2(actor.Position.X, rect.Bottom);
                    constrainedBottom = true;
                }

                if (constrainedTop || constrainedBottom)
                {
                    switch (actor.VerticalConstrainBehaviour)
                    {
                        case ConstrainBehaviour.Deactivate:
                            if (actor.Active)
                                actor.Deactivate();
                            break;

                        case ConstrainBehaviour.Bounce:
                            actor.Velocity.Y = -actor.Velocity.Y;
                            actor.Direction += MathHelper.PiOver2;
                            break;

                        case ConstrainBehaviour.Clamp:
                            actor.Velocity.Y = 0.0f;
                            break;
                    }
                }
            }
                
            if (constrainedLeft || constrainedRight || constrainedTop || constrainedBottom)
                actor.Constrained(constrainedTop, constrainedLeft, constrainedBottom, constrainedRight);        
        }

        /// <summary>
        /// Creates a ScoreBonusLabel in the scene whena a player is hit.
        /// </summary>
        /// <param name="position">The position to create the label</param>
        public void CreateGotYouLabel(Vector2 position)
	    {
		    var label = (ScoreBonusLabel)_game.ActorFactory.Create(typeof(ScoreBonusLabel));
            if (label == null)
                return;

            label.Tint = Color.White;
            label.Tint.A = 0;
            label.EndTint = label.Tint * 0.0f;
            label.LifeTicks = 400;
            //label.Position = position;
            if (_game.ActivePlayer.Ship.Health > 1)
            {
                label.SetText(_game.ActivePlayer.Ship.Health);
                label.Append(" lives remaining");
                label.Scale = new Vector2(1.75f);
            }
            else if (_game.ActivePlayer.Ship.Health == 1)
            {
                label.SetText("1 life remaining");
                label.Scale = new Vector2(1.75f);
            }
            else
            {
                label.SetText("gOT yOU");
                label.Scale = new Vector2(2.0f);
            }

            label.Owner = _game.ActivePlayer.Ship;
            label.Ownership = Ownership.FlyTheNest;
            label.Offset = new Vector2(0, 4.25f);
            label.BitmapFont = Theme.AltFont;

            var dy = RandomHelper.Random.GetFloat(0.0125f, 0.025f);
            const float dx = 0.0f;//RandomHelper.Random.GetFloat(label.Position.X * 0.01f, label.Position.X * 0.015f);

            if (position.Y < 0.0f)
                dy = -dy;

            label.Velocity = new Vector2(dx, dy);
            label.Acceleration = new Vector2(0, label.Velocity.Y * -0.015f);
		    AddActor(label);

            label.Scale = new Vector2(3.0f);

		    AddActor(label);

            label.Layer = Layers.ActorForegroundEffect;
        }

        /// <summary>
        /// Creates a ScoreBonusLabel in the scene.
        /// </summary>
        /// <param name="position">The position to create the label</param>
        /// <param name="scoreBonus">The value of the label.</param>
        public void CreateScoreBonusLabel(Vector2 position, int scoreBonus)
	    {
		    var label = (ScoreBonusLabel)_game.ActorFactory.Create(typeof(ScoreBonusLabel));
            if (label == null)
                return;

            var dy = RandomHelper.Random.GetFloat(0.025f, 0.05f);

            label.Tint = Color.White;
            label.Tint.A = 0;
            label.EndTint = label.Tint * 0.0f;
            label.LifeTicks = 90;
            label.Scale = new Vector2(1.5f);
            label.Position = position;
            label.SetText(scoreBonus);
            label.BitmapFont = Theme.AltFont;

            const float dx = 0.0f;

            if (position.Y < 0.0f)
                dy = -dy;

            label.Velocity = new Vector2(dx, dy);
            label.Acceleration = new Vector2(0, label.Velocity.Y * -0.015f);

		    AddActor(label);

            label.Layer = Layers.ActorForegroundEffect;
        }

        /// <summary>
        /// Draws the scene.
        /// </summary>
        public void Draw()
        {
            Gearset.GS.BeginMark("Scene:DrawActorsAndParticles();", Color.Green);
            DrawActorsAndParticles();
            Gearset.GS.EndMark("Scene:DrawActorsAndParticles();");
        }

        /// <summary>
        /// Draws all particles in the scene.
        /// </summary>
        void DrawActorsAndParticles()
        {
            DrawActors();
            DrawParticles();
        }

        /// <summary>
        /// Draws all particles in the scene.
        /// </summary>
        void DrawActors()
        {
            Gearset.GS.BeginMark("DrawActors", Color.Red);
            
            _game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);

            foreach (var actor in _actors)
                actor.Draw(); 
            
            _game.Renderer.End();

            Gearset.GS.EndMark("DrawActors");
        }

        /// <summary>
        /// Draws all particles in the scene.
        /// </summary>
        void DrawParticles()
        {
            Gearset.GS.BeginMark("DrawParticles", Color.Blue);

            _game.Renderer.Begin(Renderer.SortStrategy.None);

            foreach (var particle in _particles)
            {
                if (particle.Active && particle.IsOnScreen)
                    particle.Draw();
            }

            _game.Renderer.End();

            Gearset.GS.EndMark("DrawParticles");
        }
    }

    class CollisionPacketComparer : IComparer<CollisionPacket>
    {
        public int Compare(CollisionPacket a, CollisionPacket b)
        {
            if (a.Time > b.Time)
                return 1;

            if (a.Time < b.Time)
                return -1;

            return 0;
        }
    }
}
