using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Transition.Rendering;
using Transition._____Temp.StarGuard;
using Transition._____Temp.StarGuard.EntityBlueprints;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Manages entity configuration data.
	/// </summary>
    public class EntityBlueprintManager : ResourceManager
    {
        public override string FolderName => "Entities";

	    const string Separator = ",";
        readonly char[] _seperatorArray = Separator.ToCharArray();

        readonly string _namespace;

        readonly TextureManager _textureManager;
        readonly AnimationManager _animationManager;
        readonly ParticleTemplateManager _particleTemplateManager;

	    readonly Dictionary<string, EntityBlueprint> _entityDictionary = new Dictionary<string, EntityBlueprint>();
        readonly Dictionary<string, EnemyBlueprint> _enemyDictionary = new Dictionary<string, EnemyBlueprint>();
        readonly Dictionary<string, EnemyBulletBlueprint> _enemyBulletDictionary = new Dictionary<string, EnemyBulletBlueprint>();
	    readonly Dictionary<string, BulletBlueprint> _bulletDictionary = new Dictionary<string, BulletBlueprint>();
        readonly Dictionary<string, ShipBlueprint> _shipDictionary = new Dictionary<string, ShipBlueprint>();
	    readonly Dictionary<string, WeaponBlueprint> _weaponDictionary = new Dictionary<string, WeaponBlueprint>();

		/// <summary>
        /// Creates a new instance of a EntityBlueprints.
        /// </summary>
        public EntityBlueprintManager(string @namespace, TextureManager textureManager, AnimationManager animationManager, ParticleTemplateManager particleTemplateManager)
        {
			if (textureManager == null)
                throw new ArgumentNullException(nameof(textureManager));

            if (animationManager == null)
                throw new ArgumentNullException(nameof(animationManager));

            if (particleTemplateManager == null)
                throw new ArgumentNullException(nameof(particleTemplateManager));

            _namespace = @namespace;

            _textureManager = textureManager;
            _animationManager = animationManager;
            _particleTemplateManager = particleTemplateManager;
	    }

        Type GetEntityType(string className)
        {
            return Type.GetType(_namespace + className);
        }

        /// <summary>
        /// Loads all entity configuration data into the data manager.
        /// </summary>
        public override void LoadContent()
        {
            var folder = Path.Combine(Platform.ResourcesPath, FolderName);

            foreach (var filename in Directory.GetFiles(folder, "*.xml"))
                LoadEntities(filename);
        }

        private void LoadEntities(string filename)
        {
            var xdocument = XDocument.Load(filename);
			
            if (xdocument.Root == null)
                throw new XmlException("Missing Root element in " + filename);

			//Read in all of the entities...
            foreach (var blueprintElement in xdocument.Root.Elements("blueprint"))
            {
                //Specified in file but not utilised as of yet - maybe use for validation of elements later?
                var entityClass = blueprintElement.GetAttributeString("class");
                var type = GetEntityType(entityClass);

                var name = blueprintElement.GetAttributeString("name");

                foreach (var element in blueprintElement.Elements())
                {
                    switch (element.Name.LocalName)
                    {
                        case "entity":
                            ParseEntityBlueprint(type, name, element);
                            break;

                        case "enemyBullet":
                            ParseEnemyBulletBlueprint(type, name, element);
                            break;

                        case "enemy":
                            ParseEnemyBlueprint(type, name, element);
                            break;

                        case "bullet":
                            ParseBulletBlueprint(type, name, element);
                            break;

                        case "ship":
                            ParseShipBlueprint(type, name, element);
                            break;

                        case "weapon":
                            ParseWeaponBlueprint(type, name, element);
                            break;
                    }
                }
            }
        }

        private void ParseEntityBlueprint(Type entityType, string name, XElement entityBlueprintElement)
	    {
            //Can override name in sub elements
	        name = name ?? entityBlueprintElement.GetAttributeString("name");

	        var entityBlueprint = EntityBlueprint(name) ?? new EntityBlueprint();

            entityBlueprint.Name = name;
            entityBlueprint.EntityType = entityType;

            //<layers>
            //    <layer appearance="StandardAlienBullet" />
            //</layers>

            var layersElement = entityBlueprintElement.Element("layers");
            if (layersElement != null)
            {
                var layers = layersElement.Elements("layer").ToArray();
                entityBlueprint.Layers = new EntityBlueprint.Layer[layers.Length];
                var layerId = 0;
                foreach(var layerElement in layers)
                {
                    var layer = new EntityBlueprint.Layer();
                    entityBlueprint.Layers[layerId] = layer;
                    var appearance = layerElement.GetAttributeString("appearance");
	                if (string.IsNullOrEmpty(appearance) == false)
                    {
                        if (appearance.EndsWith(".animation", StringComparison.Ordinal))
	                        layer.Appearance = _animationManager.Get(appearance);
                        else
                            layer.Appearance = _textureManager.GetTextureInfo(appearance);
                    }

                    layer.DrawLayer = layerElement.GetAttributeInt32("layer", Layers.Actor);

                    layerId++;
                }
            }

	        entityBlueprint.InitialHealth = entityBlueprintElement.GetAttributeInt32("health");
	        entityBlueprint.CollisionBounds = entityBlueprintElement.GetAttributeBox2("collisionBounds");
	        entityBlueprint.BaseSize = entityBlueprintElement.GetAttributeVector2("baseSize");
            entityBlueprint.ExplosionSfx = entityBlueprintElement.GetAttributeString("explosionSfx");

	        AddParticleSystemsOfType(entityBlueprintElement, ParticleSystemType.Expired, entityBlueprint.ExpiredEffects);
	        AddParticleSystemsOfType(entityBlueprintElement, ParticleSystemType.Explosion, entityBlueprint.ExplosionEffects);
	        AddParticleSystemsOfType(entityBlueprintElement, ParticleSystemType.Spawn, entityBlueprint.SpawnEffects);
	        AddParticleSystemsOfType(entityBlueprintElement, ParticleSystemType.Trail, entityBlueprint.TrailEffects);

	        _entityDictionary[name] = entityBlueprint;
	    }

        /// <summary>
        /// Load the alien configuration data.
        /// </summary>
        private void ParseEnemyBulletBlueprint(Type entityType, string name, XElement alienBulletBlueprintElement)
        {
            //Can override name in sub elements
            name = name ?? alienBulletBlueprintElement.GetAttributeString("name");

            var enemyBulletBlueprint = EnemyBulletBlueprint(name) ?? new EnemyBulletBlueprint();
            enemyBulletBlueprint.Name = name;

            enemyBulletBlueprint.Visible = alienBulletBlueprintElement.GetAttributeBool("visible");
            enemyBulletBlueprint.Explode = alienBulletBlueprintElement.GetAttributeBool("explode");

            var soundsElement = alienBulletBlueprintElement.Element("spawnSounds");
            if (soundsElement != null)
            {
                foreach (var itemElement in soundsElement.Elements("item"))
                    enemyBulletBlueprint.Sounds.Add(itemElement.Value);
            }

            _enemyBulletDictionary[name] = enemyBulletBlueprint;
        }

        private void ParseEnemyBlueprint(Type entityType, string name, XElement enemyBlueprintElement)
	    {
            //Can override name in sub elements
            name = name ?? enemyBlueprintElement.GetAttributeString("name");

            var enemyBlueprint = EnemyBlueprint(name) ?? new EnemyBlueprint();
            enemyBlueprint.Name = name;

            enemyBlueprint.ScoreBonus = enemyBlueprintElement.GetAttributeInt32("scoreBonus");
	        enemyBlueprint.MoneyBonus = enemyBlueprintElement.GetAttributeInt32("moneyBonus");
	        enemyBlueprint.RageModifier = enemyBlueprintElement.GetAttributeSingle("rageModifier", 1.0f);

	        var speedElement = enemyBlueprintElement.Element("speed");
	        ParseDifficultyElementSingle(speedElement, enemyBlueprint.Speed);

            _enemyDictionary[name] = enemyBlueprint;
	    }
		
	    private void ParseBulletBlueprint(Type entityType, string name, XElement bulletBlueprintElement)
	    {
            //Can override name in sub elements
            name = name ?? bulletBlueprintElement.GetAttributeString("name");

	        var bulletBlueprint = BulletBlueprint(name) ?? new BulletBlueprint();

	        bulletBlueprint.EntityClass = entityType;
	        bulletBlueprint.Name = name;

	        var speed = bulletBlueprintElement.GetAttributeString("speed");
	        if (speed.Contains(Separator))
	        {
	            var components = speed.Split(_seperatorArray, StringSplitOptions.RemoveEmptyEntries);
	            bulletBlueprint.SpeedMin = ConversionHelper.ToSingle(components[0].Trim());
	            bulletBlueprint.SpeedMax = ConversionHelper.ToSingle(components[1].Trim());
	        }
	        else
	        {
	            bulletBlueprint.SpeedMin = bulletBlueprintElement.GetAttributeSingle("speed");
	            bulletBlueprint.SpeedMax = bulletBlueprint.SpeedMin;
	        }

	        bulletBlueprint.Friction = bulletBlueprintElement.GetAttributeSingle("friction");
	        bulletBlueprint.Spread = bulletBlueprintElement.GetAttributeSingle("spread");
	        bulletBlueprint.LifeTicks = bulletBlueprintElement.GetAttributeInt32("lifeTicks");
	        bulletBlueprint.FastMoving = bulletBlueprintElement.GetAttributeBool("fastMoving");
	        bulletBlueprint.Damage = new List<BulletUpgrade>();
	        var damagesElement = bulletBlueprintElement.Element("damages");
	        if (damagesElement != null)
	        {
	            foreach (var itemElement in damagesElement.Elements("item"))
	            {
	                var bulletUpgrade = new BulletUpgrade
	                {
	                    Damage = itemElement.GetAttributeInt32("damage"),
	                    Cost = itemElement.GetAttributeInt32("cost")
	                };
	                bulletBlueprint.Damage.Add(bulletUpgrade);
	            }
	        }

	        _bulletDictionary[name] = bulletBlueprint;
	    }

        private void ParseShipBlueprint(Type entityType, string name, XElement shipBlueprintElement)
	    {
            name = name ?? shipBlueprintElement.GetAttributeString("name");

	        var shipBlueprint = ShipBlueprint(name) ?? new ShipBlueprint();
	        shipBlueprint.Name = name;

	        var menuTextureAsset = shipBlueprintElement.GetAttributeString("menuTexture");
            if (string.IsNullOrEmpty(menuTextureAsset) == false)
	            shipBlueprint.MenuTextureInfo = _textureManager.GetTextureInfo(menuTextureAsset);

	        shipBlueprint.Classification = shipBlueprintElement.GetAttributeString("classification");
	        shipBlueprint.Description = shipBlueprintElement.GetAttributeString("description");
	        shipBlueprint.AccelerationX = shipBlueprintElement.GetAttributeSingle("accelerationX");
	        shipBlueprint.AccelerationY = shipBlueprintElement.GetAttributeSingle("accelerationY");
	        shipBlueprint.MaxVelocityX = shipBlueprintElement.GetAttributeSingle("maxVelocityX");
	        shipBlueprint.MaxVelocityY = shipBlueprintElement.GetAttributeSingle("maxVelocityY");
	        shipBlueprint.FrictionX = shipBlueprintElement.GetAttributeSingle("frictionX");
	        shipBlueprint.FrictionY = shipBlueprintElement.GetAttributeSingle("frictionY");
	        shipBlueprint.WeaponName = shipBlueprintElement.GetAttributeString("weaponName");
	        shipBlueprint.Nuke = shipBlueprintElement.GetAttributeString("nuke");
	        shipBlueprint.RageType = shipBlueprintElement.GetAttributeEnum<RageType>("rage");
            
            shipBlueprint.WalkLeftAnimation= shipBlueprintElement.GetAttributeString("walkLeftAnimation");
            shipBlueprint.WalkRightAnimation = shipBlueprintElement.GetAttributeString("walkRightAnimation");
            shipBlueprint.FaceLeftAnimation = shipBlueprintElement.GetAttributeString("faceLeftAnimation");
            shipBlueprint.FaceRightAnimation = shipBlueprintElement.GetAttributeString("faceRightAnimation");

	        AddParticleSystemsOfType(shipBlueprintElement, ParticleSystemType.Explosion, shipBlueprint.NukeEffects);

	        _shipDictionary[name] = shipBlueprint;
	    }

        private void ParseWeaponBlueprint(Type entityType, string name, XElement weaponBlueprintElement)
	    {
	        var weaponBlueprint = WeaponBlueprint(name) ?? new WeaponBlueprint();

	        weaponBlueprint.Description = weaponBlueprintElement.GetAttributeString("description");
	        weaponBlueprint.BulletName = weaponBlueprintElement.GetAttributeString("bulletName");

	        var fireRatesElement = weaponBlueprintElement.Element("fireRates");
	        if (fireRatesElement != null)
	        {
	            foreach (var itemElement in fireRatesElement.Elements("item"))
	            {
	                var weaponUpgrade = new WeaponUpgrade
	                {
	                    FireRate = itemElement.GetAttributeInt32("fireRate"),
	                    Cost = itemElement.GetAttributeInt32("cost")
	                };
	                weaponBlueprint.FireRates.Add(weaponUpgrade);
	            }
	        }

	        _weaponDictionary[name] = weaponBlueprint;
	    }

	    /// <summary>
		/// Gets EntityBlueprint for a class of entity.
		/// </summary>
        public EntityBlueprint EntityBlueprint(string name)
        {
            return _entityDictionary.ContainsKey(name) ? _entityDictionary[name] : null;
        }

		/// <summary>
		/// Gets EnemyBlueprint for a class of entity.
		/// </summary>
        public EnemyBlueprint EnemyBlueprint(string name)
		{
            return _enemyDictionary.ContainsKey(name) ? _enemyDictionary[name] : null;
		}

        /// <summary>
		/// Gets AlienBulletBlueprint for a class of alien.
		/// </summary>
        public EnemyBulletBlueprint EnemyBulletBlueprint(string name)
		{
		    return _enemyBulletDictionary.ContainsKey(name) ? _enemyBulletDictionary[name] : null;
		}

	    /// <summary>
		/// Gets BulletBlueprint for a class of entity.
		/// </summary>
        public BulletBlueprint BulletBlueprint(string name)
	    {
	        return _bulletDictionary.ContainsKey(name) ? _bulletDictionary[name] : null;
	    }

	    /// <summary>
		/// Gets ShipBlueprint for a class of entity.
		/// </summary>
        public ShipBlueprint ShipBlueprint(string name)
	    {
	        return _shipDictionary.ContainsKey(name) ? _shipDictionary[name] : null;
	    }

        /// <summary>
		/// Gets ShipBlueprint for a class of entity.
		/// </summary>
        public IEnumerable<ShipBlueprint> ShipBlueprints()
	    {
	        return _shipDictionary.Values;
	    }

		/// <summary>
		/// Gets WeaponBlueprint for a class of entity.
		/// </summary>
        public WeaponBlueprint WeaponBlueprint(string name)
		{
		    return _weaponDictionary.ContainsKey(name) ? _weaponDictionary[name] : null;
		}

        private void AddParticleSystemsOfType(XElement parentElement, ParticleSystemType requiredType, IList<ParticleEffect> particleEffects)
        {
            var particleSystemsElement = parentElement.Elements("particleSystems");

            //Read in all the data...
            foreach (var particleSystemElement in particleSystemsElement.Elements("particleSystem"))
            { 
                var systemType = particleSystemElement.GetAttributeEnum<ParticleSystemType>("type");
                if (requiredType != systemType)
                    continue;

                particleEffects.Add(ParseParticleSystem(particleSystemElement));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private ParticleEffect ParseParticleSystem(XElement particleSystemElement)
        {
            //Read in all the data...
            var name = particleSystemElement.GetAttributeString("name");

            var particleEffect = new ParticleEffect
            {
                Type = particleSystemElement.GetAttributeEnum<ParticleSystemType>("type"),
                Template = _particleTemplateManager.GetTemplate(name),
                Delay = particleSystemElement.GetAttributeSingle("delay"),
                Lifetime = particleSystemElement.GetAttributeSingle("lifetime"),
                Offset = particleSystemElement.GetAttributeVector2("offset")
            };

            return particleEffect;
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ParseDifficultyElementInt32(XElement element, IList<int> array)
        {
            if (array.Count != TransitionGame.MaxDifficultyLevels)
                throw new XmlException("Incorrect difficulty array length for element:" + element.Name);

            if (element == null)
                return;

            array[(int)Difficulty.Easy] = element.GetAttributeInt32("easy");
            array[(int)Difficulty.Normal] = element.GetAttributeInt32("medium");
            array[(int)Difficulty.Hard] = element.GetAttributeInt32("hard");
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ParseDifficultyElementSingle(XElement element, IList<float> array)
        {
            if (array.Count != TransitionGame.MaxDifficultyLevels)
                throw new XmlException("Incorrect difficulty array length for element:" + element.Name);

            if (element == null)
                return;

            array[(int)Difficulty.Easy] = element.GetAttributeSingle("easy");
            array[(int)Difficulty.Normal] = element.GetAttributeSingle("medium");
            array[(int)Difficulty.Hard] = element.GetAttributeSingle("hard");
        }
    }
}
