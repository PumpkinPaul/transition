using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Manages particle template configuration data.
	/// </summary>
    public class ParticleTemplateManager : ResourceManager
    {
        public override string FolderName { get { return "Particles"; } }

        readonly TextureManager _textureManager;

	    static readonly char[] Separator = { ',' };

	    readonly Dictionary<string, ParticleSystemTemplate> _templates = new Dictionary<string, ParticleSystemTemplate>();

		/// <summary>
        /// Creates a new instance of a ParticleTemplateManager.
        /// </summary>
        public ParticleTemplateManager(TextureManager textureManager) 
        {
            if (textureManager == null)
                throw new ArgumentNullException(nameof(textureManager));

            _textureManager = textureManager;
        }
        
        /// <summary>
        /// Loads all actor configuration data into the data manager.
        /// </summary>
        public override void LoadContent()
        {
            var folder = Directory.GetFiles(Path.Combine(Platform.ResourcesPath, "Particles"), "*.xml");

            foreach (var filename in folder)
                LoadTemplateData(filename);
        }

		/// <summary>
        /// Load the template configuration data.
        /// </summary>
        private void LoadTemplateData(string filename)
        {
            var xdocument = XDocument.Load(filename);

            if (xdocument.Root == null)
                throw new XmlException($"The file {filename} is not a valid XML file - No root element");

            var particleElement = xdocument.Root;
            var templateName = particleElement.GetAttributeString("name");
   		
			var template = _templates.ContainsKey(templateName) ? _templates[templateName] : new ParticleSystemTemplate();
            template.Name = templateName;

            ParseAppearance(particleElement, template);
            ParseEmission(particleElement, template);
            ParseLifeElement(particleElement, template);
            ParseLinearSpeedElement(particleElement, template);
            ParseAngularSpeedElement(particleElement, template);
            ParseBoundsElement(particleElement, template);

            ParseColorsElement(particleElement, template);
            ParseSizesElement(particleElement, template);
            //TODO
            //ParseForcesElement(particleElement, template);
	
			_templates[templateName] = template;         
        }

	    private void ParseAppearance(XContainer particleElement, ParticleSystemTemplate template)
	    {
	        var appearanceElement = particleElement.Element("appearance");

	        var assetName = appearanceElement.GetAttributeString("image");
	        template.TextureInfo = _textureManager.GetTextureInfo(assetName);
	        template.Texture = _textureManager.GetTexture(template.TextureInfo.AssetName);
	        template.TextureId = _textureManager.GetTextureId(template.TextureInfo.AssetName);

	        template.DirectionalParticles = appearanceElement.GetAttributeBool("directional");
	    }

        private static void ParseEmission(XContainer particleElement, ParticleSystemTemplate template)
        {
            var emissionElement = particleElement.Element("emission");

            var emitterType = emissionElement.GetAttributeEnum<ParticleEmitterType>("type");
            template.Emitter = ParticleEmitterFactory.Create(emitterType);
            template.Emitter.SetData(emissionElement.GetAttributeString("data"));

            var emissionRate = emissionElement.GetAttributeString("rate");
            if (emissionRate.Contains(","))
            {
                var data = emissionRate.Split(Separator, StringSplitOptions.RemoveEmptyEntries);
                template.MinEmissionRate = ConversionHelper.ToSingle(data[0]);
                template.MaxEmissionRate = ConversionHelper.ToSingle(data[1]);
            }
            else
            {
                template.MaxEmissionRate = ConversionHelper.ToSingle(emissionRate);
                template.MinEmissionRate = template.MaxEmissionRate;
            }
            template.EmissionResidue = emissionElement.GetAttributeSingle("residue");
        }

        private static void ParseLifeElement(XContainer particleElement, ParticleSystemTemplate template)
        {
            var lifeElement = particleElement.Element("life");
            template.LifetimeMin = lifeElement.GetAttributeSingle("min");
            template.LifetimeMax = lifeElement.GetAttributeSingle("max");
        }

        private static void ParseLinearSpeedElement(XContainer particleElement, ParticleSystemTemplate template)
        {
            var linearSpeedElement = particleElement.Element("linearSpeed");
            template.LinearSpeedMin = linearSpeedElement.GetAttributeSingle("min");
            template.LinearSpeedMax = linearSpeedElement.GetAttributeSingle("max");
            template.LinearDamping = linearSpeedElement.GetAttributeSingle("damping", 1.0f);
        }

        private static void ParseAngularSpeedElement(XContainer particleElement, ParticleSystemTemplate template)
        {
            var angularSpeedElement = particleElement.Element("angularSpeed");
            template.AngularSpeedMin = angularSpeedElement.GetAttributeSingle("min");
            template.AngularSpeedMax = angularSpeedElement.GetAttributeSingle("max");
            template.AngularDamping = angularSpeedElement.GetAttributeSingle("damping", 1.0f);
        }

        //TODO - make into force and remove this
        //private static void ParseGravityElement(XContainer particleElement, ParticleSystemTemplate template)
        //{
        //    var gravityElement = particleElement.Element("gravity");

        //    var startXY = gravityElement.GetAttributeString("startXY");
        //    if (startXY.Contains(","))
        //    {
        //        var data = startXY.Split(Separator, StringSplitOptions.RemoveEmptyEntries);
        //        template.GravityStartMinX = ConversionHelper.ToSingle(data[0]);
        //        template.GravityStartMinY = ConversionHelper.ToSingle(data[1]);
        //    }
        //    else
        //    {
        //        template.GravityStartMinX = ConversionHelper.ToSingle(startXY);
        //        template.GravityStartMinY = template.MaxEmissionRate;
        //    }

        //    var gravityStartMin = gravityElement.GetAttributeVector2("startMin");
        //    var gravityStartMax = gravityElement.GetAttributeVector2("startMax");
        //    var gravityEndMin = gravityElement.GetAttributeVector2("endMin");
        //    var gravityEndMax = gravityElement.GetAttributeVector2("endMax");

        //    template.GravityStartMinX = gravityStartMin.X;
        //    template.GravityStartMinY = gravityStartMin.Y;
        //    template.GravityStartMaxX = gravityStartMax.X;
        //    template.GravityStartMaxY = gravityStartMax.Y;
        //    template.GravityEndMinX = gravityEndMin.X;
        //    template.GravityEndMinY = gravityEndMin.Y;
        //    template.GravityEndMaxX = gravityEndMax.X;
        //    template.GravityEndMaxY = gravityEndMax.Y;
        //}

        private static void ParseBoundsElement(XContainer particleElement, ParticleSystemTemplate template)
        {
            var boundsElement = particleElement.Element("bounds");
            if (boundsElement != null)
            {
                template.BoundsTop = boundsElement.GetAttributeBool("top");
                template.BoundsLeft = boundsElement.GetAttributeBool("left");
                template.BoundsBottom = boundsElement.GetAttributeBool("bottom");
                template.BoundsRight = boundsElement.GetAttributeBool("right");
            }

            template.Bounded = template.BoundsTop || template.BoundsLeft || template.BoundsBottom || template.BoundsRight;
        }

        private static void ParseColorsElement(XContainer particleElement, ParticleSystemTemplate template)
        {
            template.ColorBands.Clear();

            var colorsElement = particleElement.Element("colors");

            if (colorsElement == null)
                return;

            template.InheritTint = colorsElement.GetAttributeBool("inheritTint");

            //Read in all the data for each band...
            foreach (var colorElement in colorsElement.Elements("color"))
            {
                var band = new ColorBand
                {
                    ColourMin = colorElement.GetAttributeVector4("minRGBA"),
                    ColourMax = colorElement.GetAttributeVector4("maxRGBA")
                };

                foreach (var itemElement in colorElement.Elements("item"))
                {
                    if (band.Colors == null)
                        band.Colors = new List<Color>();

                    band.Colors.Add(itemElement.GetAttributeColor("color"));
                }

                template.ColorBands.Add(band);
            }
        }

        private static void ParseSizesElement(XContainer particleElement, ParticleSystemTemplate template)
        {
            template.SizeBands.Clear();

            var sizesElement = particleElement.Element("sizes");

            if (sizesElement == null)
                return;

            template.SymetricalParticles = sizesElement.GetAttributeBool("symetrical");
   
            //Read in all the data for each band...
            foreach (var sizeElement in sizesElement.Elements("size"))
            {
                var min = sizeElement.GetAttributeVector2("minXY");
                var max = sizeElement.GetAttributeVector2("maxXY");
                var band = new SizeBand
                {
                    SizeMinX = min.X,
                    SizeMinY = min.Y,
                    SizeMaxX = max.X,
                    SizeMaxY = max.Y
                };

                template.SizeBands.Add(band);
            }
        }

        private static void ParseForcesElement(XContainer particleElement, ParticleSystemTemplate template)
        {

        }

	    /// <summary>
        /// Gets a ParticleSystemTemplate for a key.
		/// </summary>
        public ParticleSystemTemplate GetTemplate(string name)
        {
            if (_templates.ContainsKey(name))
                return _templates[name];
            
            return null;
        }
    }
}
