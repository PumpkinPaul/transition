using Microsoft.Xna.Framework.Graphics;
using Transition.ResourceManagers;

namespace Transition.Rendering
{
    /// <summary>
    /// An style to handle ADDITIVE blending
    /// </summary>
    public class AdditiveRenderStyle : RenderStyle
    {
	    public static AdditiveRenderStyle Instance;

        public override RenderStyleType RenderStyleId => RenderStyleType.Additive;

        protected override void OnRegister(EffectManager effectManager)
        {
            Instance = this;
            Effect = effectManager.GetEffect("BasicEffect");
        }

        public override void Setup(GraphicsDevice graphicsDevice)
        {
            graphicsDevice.BlendState = BlendState.Additive;
            graphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
        }
    }
}
