namespace Transition
{
    /// <summary>
    /// .
    /// </summary>
    public abstract class LevelAction
    {
        public LevelActionType Type;

        public abstract void Execute(LevelManager levelManager);
    }
}