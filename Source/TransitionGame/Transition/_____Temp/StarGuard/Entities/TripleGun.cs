﻿using Microsoft.Xna.Framework;
using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// TripleGun enemy
    /// </summary>
    [ProtoContract]
    public class TripleGun : Enemy
    {
        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public TripleGun(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnAddedToWorld(World world)
        {
            //Check tiles to left and right to determine which way to face
            var coords = Game.GameMap.GetTileCoordinates(Position);

            var tileToLeft = new Point(coords.X - 1, coords.Y);
            if (Game.GameMap.GetTileCollision(tileToLeft) == GameMap.TileCollision.Impassable)
            {
                //Solid tile to left
                IsFacingRight = true;
            }
            else
            {
                IsFacingRight = false;
            }

            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-08.face-right.animation")
                : Game.AnimationManager.Get("enemy-08.face-left.animation"));

            Collidable = false;
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            if (IsPlayerInRange == false)
                return;

            if (Sprites[0].Event == 1)
                Shoot();
        }

        void Shoot()
        {   var player = Game.World.Get<Player>(EntityKeys.Player);

            if (IsFacingRight && player.Position.X > Position.X || IsFacingLeft && player.Position.X < Position.X)
            {

                for (var i = 0; i < 3; i++)
                {
                    var offset = new Vector2(RandomHelper.FastRandom.NextFloat(-2.0f, 2.0f));
                    var speed = RandomHelper.FastRandom.NextFloat(0.075f, 0.1f);
                    var angle = (float)VectorHelper.GetAngle(Position, player.Position + offset);
                    var velocity = VectorHelper.Polar(speed, angle);
                    CreateBullet(velocity);
                }
            }
        }

        void CreateBullet(Vector2 velocity)
        {
            var bullet = Game.EntityFactory.Create(typeof(EnemyBullet), "TripleGunBullet");
            if (bullet != null)
            {
                bullet.Position = Position;
                bullet.Velocity = velocity;
                Game.World.Add(bullet);
            }
        }
    }
}