sampler backbuffer : register(s0);
sampler distortion : register(s1);

float2 distortionPixels;

/*
	Final distortion application fragment shader, for applying the distortion maps to a buffer to get the end result
	requires: backbuffer, distortion, distortionPixels
*/
float4 final_glfrag(float2 gl_TexCoord : TEXCOORD0) : COLOR0
{
	float2 offset = tex2D(distortion, gl_TexCoord).rg; 
	offset *= distortionPixels; 
	return tex2D(backbuffer, gl_TexCoord + offset);
}

technique Technique1
{
    pass Pass1 
	{ 
		#if SM4
		PixelShader = compile ps_4_0_level_9_1 final_glfrag();
		#elif SM3
		PixelShader = compile ps_3_0 final_glfrag();
		#else
		PixelShader = compile ps_2_0 final_glfrag();
		#endif
	}
}

