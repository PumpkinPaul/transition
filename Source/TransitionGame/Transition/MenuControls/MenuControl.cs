#define MENU_CONTROL_PROCESS_LOGGING
#define MENU_CONTROL_EVENT_LOGGING

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.MenuControls.Transitions;
using Transition.Menus;
using Transition.Rendering;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents the base class for overlay controls.
    /// </summary>
    public abstract class MenuControl
    {
        public bool DisableLogging;
        public static bool Monkeying = false;

        public const float InactiveAlphaModifier = 0.25f;

        public event EventHandler GotFocus;
        public event EventHandler LostFocus;
        public event EventHandler Click;
        public event EventHandler IncreaseValue;
        public event EventHandler DecreaseValue;
        public event EventHandler MouseEnter;
        public event EventHandler MouseDown;
        public event EventHandler MouseMove;
        public event EventHandler MouseUp;
        public event EventHandler MouseLeave;
        
        /// <summary>A reference to the game object.</summary>
        protected readonly BaseGame Game;

		/// <summary>A reference to the control's parent menu.</summary>
		public Overlay Overlay;

        public string Name;

        public string CachedAButtonText;
        protected string ActiveAButtonText;

        public virtual bool AutoTabIndex => false;

        /// <summary>The text to display for the A button action.</summary>
        public string AButtonText
        {
            get { return ActiveAButtonText; }
            set 
            {
                CachedAButtonText = value;
                ActiveAButtonText = value;
            } 
        }

        /// <summary>Whether the control can be selected with the A button.</summary>
        public bool AToSelect = true;

        protected string CachedHelpText;
        protected string ActiveHelpText;

        /// <summary></summary>
        public string HelpText 
        {
            get { return ActiveHelpText; }
            set 
            {
                CachedHelpText = value;
                ActiveHelpText = value;
            } 
        } 

        /// <summary>The position of the control relative to its parent if it has one; otherwise world coordinates.</summary>
        public Vector2 Position
        {
            get { return Parent != null ? Parent.Position + _position : _position; }
            set { _position = value; }
        }

        Vector2 _position;

        public MenuControl Parent;

        /// <summary>How to render the control.</summary>
        public RenderStyle RenderStyle = TransparentRenderStyle.Instance;

        /// <summary>Whether the control can be selected with the start button.</summary>
        public bool StartToSelect;

        /// <summary>The order the controls are 'tabbed' to.</summary>
        public int TabIndex = Overlay.UnspecifiedTabIndex;

        /// <summary>Tint applied to the control's appearance.</summary>
        public Color Tint;

        public float Alpha
        {
            get { return Parent?.Alpha + _alpha ?? _alpha; }
            set { _alpha = value; }
        }

        float _alpha = 1.0f;
        public float AnchorAlpha = 1.0f;
        
        /// <summary>Whether the control is visible.</summary>
        bool _visible = true;

        /// <summary>The visibility of the control with respect to its parent if it has one; otherwise my visibility.</summary>
        public bool Visible
        {
            get { return Parent != null ? Parent.Visible && _visible : _visible; }
            set 
            { 
                _visible = value; 
            }
        }

        //Used for pulsing the active control
        protected float FocusValue = 1.0f;
        protected float FocusValueInc = 0.0f;//0.056f; //Set this to have the Focused control 'flash'

        protected bool IsFocusControl => Game.MenuManager.FocusControl == this;
        protected bool IsHoverControl => Game.MenuManager.HoverControl == this;

        public Vector2 AnchorPosition;

        public int Layer = 0;

        Dictionary<string, MenuTransitionEffect> _interpolations = new Dictionary<string, MenuTransitionEffect>();

        protected readonly List<MenuControl> Children = new List<MenuControl>(); 
        public void AddChild(MenuControl control)
        {
            Children.Add(control);
        }

		/// <summary>
        /// Initialises a new instance of a MenuControl.
		/// </summary>
		/// <param name="game">A reference to the game.</param>
		protected MenuControl(BaseGame game)
		{
			Game = game;
            AButtonText = "select";
            Tint = Theme.StandardColor;
        }

        public virtual void Load(XElement element)
        {
            Name = element.GetAttributeString("name");
            Position = element.GetAttributeVector2("position");

            AButtonText = element.GetAttributeString("aButtonText", AButtonText);
            AToSelect = element.GetAttributeBool("aToSelect", AToSelect);
            HelpText = element.GetAttributeString("helpText");
            RenderStyle = RenderStyle.Get(element.GetAttributeString("renderStyle"), TransparentRenderStyle.Instance);
            StartToSelect = element.GetAttributeBool("startToSelect", StartToSelect);
            TabIndex = element.GetAttributeInt32("tabIndex", TabIndex);
            Tint = element.GetAttributeColor("tint", Tint);
            Visible = element.GetAttributeBool("visible", Visible);
            Layer = element.GetAttributeInt32("layer", Layer);
            DisableLogging = element.GetAttributeBool("disableLogging", DisableLogging);
            
            //Content! 
            //Might be transitions
            //<transition name="showing" attribute="position" position="-15.0,-2" easing="EaseBothSoft" />
            foreach (var transitionElement in element.Elements("transition"))
            {
                var name = transitionElement.GetAttributeString("name");
                var attribute = transitionElement.GetAttributeString("attribute");
                
                var easing = transitionElement.GetAttributeEnum("easing", InterpolationMode.EaseBothMedium);

                var type = transitionElement.GetAttributeEnum("type", MenuTransitionEffect.TransitionType.Absolute);
                var ticks = transitionElement.GetAttributeInt32("ticks");
                var delay = transitionElement.GetAttributeInt32("delay");

                //if (attribute != "position")
                //    throw new NotImplementedException("Can only transition Position attributes at present");

                if (_interpolations == null)
                    _interpolations = new Dictionary<string, MenuTransitionEffect>();

                switch(attribute)
                {
                    case "position":
                        _interpolations[name] = new PositionTransitionEffect(
                            this,
                            Name + "." + name,
                            delay,
                            ticks,
                            Interpolate.GetMethod(easing),
                            type,
                            transitionElement.GetAttributeVector2("position"));
                        break;

                    case "alpha":
                        _interpolations[name] = new AlphaTransitionEffect(
                            this,
                            Name + "." + name,
                            delay,
                            ticks,
                            Interpolate.GetMethod(easing),
                            type,
                            transitionElement.GetAttributeSingle("alpha"));
                        break;
                }
            }
        }

        public void StartTransition(string name)
        {
            if (_interpolations.ContainsKey(name))
                _interpolations[name].Start();
        }

        public void StopTransition(string name)
        {
            if (_interpolations.ContainsKey(name))
                _interpolations[name].Stop();
        }

        protected BitmapFont ParseFont(XElement element, BitmapFont defaultValue)
        {
            var fontName = element.GetAttributeString("font");
            return string.IsNullOrWhiteSpace(fontName) == false ? Game.FontManager.GetBitmapFont(fontName) : defaultValue;
        }

        protected TextureInfo ParseTexture(XElement element, string name, TextureInfo defaultValue)
        {
            var textureInfo = element.GetAttributeString(name);
            return string.IsNullOrEmpty(textureInfo) == false ? Game.TextureManager.GetTextureInfo(textureInfo) : defaultValue;
        }
        
        /// <summary>
        /// Gets whether the control captures a request to move to the next or previous menu item.
        /// </summary>
        public bool CapturesPreviousNextControlRequest { get; protected set; }

        /// Gets the text to display for the left and right dpad / stick action.
        public virtual string LeftRightButtonText => null;

        /// Gets the text to display for the up and down dpad / stick action.
        public virtual string UpDownButtonText => "move selection";

        /// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
        public virtual Rect Bounds => new Rect();

        public Color EffectiveTint
        {
            get
            {
                Color tint;
                if (IsFocusControl)
                    tint = Theme.Current.GetColor(Theme.Colors.Focus) * FocusValue;
                else if (IsHoverControl)
                    tint = Theme.Current.GetColor(Theme.Colors.Hover);
                else
                    tint = Tint;

                tint *= Overlay.Opacity;

                return tint;
            }
        }

        /// <summary>
        /// Occurs when the player has selected the previous contol but the current control captures the input.
        /// </summary>
        /// <returns>.</returns>
        public bool CapturePreviousControl()
        {
            var capturedPreviousControl = OnCapturePreviousControl();
            if (capturedPreviousControl)
                Game.PlaySound("MenuItemChange");

            return capturedPreviousControl;
        }

        /// <summary>
        /// Occurs when the player has selected the next contol but the current control captures the input.
        /// </summary>
        /// <returns>.</returns>
        public bool CaptureNextControl()
        {
            var capturedNextControl = OnCaptureNextControl();
            if (capturedNextControl)
                Game.PlaySound("MenuItemChange");

            return capturedNextControl;
        }

        /// <summary>
        /// Updates the control.
        /// </summary>
        public void Update()
        {

            foreach (var interpolation in _interpolations.Values)
                interpolation.Update();

            //Common updating for all controls.
            PulseControl();

            //Allow derived controls to update themselves.
            OnUpdate();
        }

        /// <summary>
        /// Pulses the control if it has the focus (hover control)
        /// </summary>
        public void PulseControl()
        {
            //If this control has the focus pulse it
            if (IsFocusControl)
            {
                FocusValue += FocusValueInc;
                //if (_activeValue > 1.05f || _activeValue < 0.95f)
                if (FocusValue > 1.95f || FocusValue < 0.5f)
                    FocusValueInc = -FocusValueInc;
            }
            //otherwise gracefully return to normal scale
            else
            {
                if (FocusValue < 1.0)
                    FocusValue += 0.05f;
                else if (FocusValue > 1.0f)
                    FocusValue -= 0.05f;
            }
        }

        /// <summary>
        /// Draws the control.
        /// </summary>
        public void Draw()
        {
            if (Visible == false)
                return;

            OnDraw();

            if (Monkeying)
            {
                var color = Color.Red * Overlay.Opacity;
                Game.Renderer.DrawBoxStyled(Bounds, color, Renderer.LineStyle.Dotted);
            }
        }

        /// <summary>
        /// Occurs when a control should capture an attempt to move to the previous control in the tab order.
        /// </summary>
        /// <returns>.</returns>
        protected virtual bool OnCapturePreviousControl()
        {
            return false;
        }

        /// <summary>
        /// Occurs when a control should capture an attempt to move to the next control in the tab order.
        /// </summary>
        /// <returns>.</returns>
        protected virtual bool OnCaptureNextControl()
        {
            return false;
        }

        /// <summary>
        /// Allows a derived control to update itself.
        /// </summary>
        protected virtual void OnUpdate() { }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected virtual void OnDraw() { }

        /// <summary>
        /// Tells a control it was clicked (i.e. the active control is selected).
        /// </summary>
        /// <returns>.</returns>
        public void PerformGotFocus()
        {
            LogProcess("PerformGotFocus");

            OnGotFocus();
            
            //Now raise the event to any subscribers
            RaiseGotFocus();
        }

        /// <summary>
        /// Tells a control it was clicked (i.e. the active control is selected).
        /// </summary>
        /// <returns>.</returns>
        public void PerformLostFocus()
        {
            LogProcess("PerformLostFocus");

            OnLostFocus();
            
            //Now raise the event to any subscribers
            RaiseLostFocus();
        }

        /// <summary>
        /// Tells a control it was clicked (i.e. the active control is selected).
        /// </summary>
        /// <returns>.</returns>
        public void PerformClick()
        {
            LogProcess("PerformClick");

            //Derived controls can stop the event being raised   
            if (OnClick() == false)
                return;

            Game.PlaySound("MenuItemSelect");
            
            //Now raise the event to any subscribers
            RaiseClick();
        }

        /// <summary>
        /// Tells a control to increase its value.
		/// </summary>
        /// <returns>.</returns>
		public void PerformIncreaseValue()
		{
            LogProcess("PerformIncreaseValue");

            //Derived controls can stop the event being raised
            if (OnIncreaseValue() == false)
                return;

            Game.PlaySound("MenuItemChange");
        	
            //Now raise the event to any subscribers
			RaiseIncreaseValue();
		}

        /// <summary>
        /// Tells a control to increase its value.
		/// </summary>
        /// <returns>.</returns>
		public void PerformDecreaseValue()
		{
            LogProcess("PerformDecreaseValue");

            //Derived controls can stop the event being raised
            if (OnDecreaseValue() == false)
                return;

            Game.PlaySound("MenuItemChange");

            //Now raise the event to any subscribers
            RaiseDecreaseValue();
		}

        /// <summary>
		/// .
		/// </summary>
		public void PerformMouseEnter()
        {
            LogProcess("PerformMouseEnter");

            //Allow subclasses to perform actions before raising the event
			OnMouseEnter();

            //Now raise the event to any subscribers
            RaiseMouseEnter();
        }

        /// <summary>
		/// .
		/// </summary>
		public void PerformMouseDown()
        {
            LogProcess("PerformMouseDown");

            //Allow subclasses to perform actions before raising the event
			OnMouseDown();

            //Now raise the event to any subscribers
            RaiseMouseDown();
        }

        /// <summary>
		/// .
		/// </summary>
		public void PerformMouseMove()
        {
            LogProcess("PerformMouseMove");

            //Allow subclasses to perform actions before raising the event
			OnMouseMove();

            //Now raise the event to any subscribers
            RaiseMouseMove();
        }

        /// <summary>
		/// .
		/// </summary>
		public void PerformMouseUp()
        {
            LogProcess("PerformMouseUp");

            //Allow subclasses to perform actions before raising the event
			OnMouseUp();

            //Now raise the event to any subscribers
            RaiseMouseUp();
        }

        /// <summary>
		/// .
		/// </summary>
		public void PerformMouseLeave()
        {
            LogProcess("PerformMouseLeave");

            //Allow subclasses to perform actions before raising the event
			OnMouseLeave();

            //Now raise the event to any subscribers
            RaiseMouseLeave();
        }

        protected void RaiseGotFocus()
        {
            var handler = GotFocus;
            
            if (handler != null)
            {
                LogEvent("GotFocus");
                handler(this, EventArgs.Empty);
            }
        }

        protected void RaiseLostFocus()
        {
            var handler = LostFocus;

            if (handler != null)
            {
                LogEvent("LostFocus");
                handler(this, EventArgs.Empty);
            }
        }

        protected void RaiseClick()
        {
            var handler = Click;

            if (handler != null)
            {
                LogEvent("Click");
                handler(this, EventArgs.Empty);
            }
        }

        protected void RaiseIncreaseValue()
        {
            var handler = IncreaseValue;

            if (handler != null)
            {
                LogEvent("IncreaseValue");
                handler(this, EventArgs.Empty);
            }
        }

        protected void RaiseDecreaseValue()
        {
            var handler = DecreaseValue;

            if (handler != null)
            {
                LogEvent("DecreaseValue");
                handler(this, EventArgs.Empty);
            }
        }

        protected void RaiseMouseEnter()
        {
            var handler = MouseEnter;

            if (handler != null)
            {
                LogEvent("MouseEnter");
                handler(this, EventArgs.Empty);
            }
        }

        protected void RaiseMouseDown()
        {
            var handler = MouseDown;

            if (handler != null)
            {
                LogEvent("MouseDown");
                handler(this, EventArgs.Empty);
            }
        }

        protected void RaiseMouseMove()
        {
            var handler = MouseMove;

            if (handler != null)
            {
                LogEvent("MouseMove");
                handler(this, EventArgs.Empty);
            }
        }

        protected void RaiseMouseUp()
        {
            var handler = MouseUp;

            if (handler != null)
            {
                LogEvent("MouseUp");
                handler(this, EventArgs.Empty);
            }
        }

        protected void RaiseMouseLeave()
        {
            var handler = MouseLeave;

            if (handler != null)
            {
                LogEvent("MouseLeave");
                handler(this, EventArgs.Empty);
            }
        }

        //Derviced controls can override these methods to cusomise behaviour
        protected virtual void OnGotFocus() { }
        protected virtual void OnLostFocus() { }
        protected virtual bool OnClick() { return true; }
        protected virtual bool OnIncreaseValue() { return false; }
        protected virtual bool OnDecreaseValue() { return false; }
		protected virtual void OnMouseEnter() { }
		protected virtual void OnMouseDown() { }
		protected virtual void OnMouseMove() { }
		protected virtual void OnMouseUp() { }
		protected virtual void OnMouseLeave() { }
        protected virtual bool OnValueChanged() { return true; }

        [Conditional("MENU_CONTROL_PROCESS_LOGGING")]
        protected void LogProcess(string message)
        {
            if (DisableLogging)
                return;

            Gearset.GS.Action(() => Gearset.GS.Log("Menu Control Process", (Name ?? "[Unknown]") + "_" + message));
        }

        [Conditional("MENU_CONTROL_EVENT_LOGGING")]
        protected void LogEvent(string message)
        {
            if (DisableLogging)
                return;

            Gearset.GS.Action(()=>Gearset.GS.Log("Menu Control Event", (Name ?? "[Unknown]") + "_" + message));
        }
    }
}
