using System;

using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Helper class for Vecotrs
    /// </summary>
    public static class VectorHelper
    {
		public static Vector2 Polar(float length, float angle)
		{
			return new Vector2(length * (float)Math.Cos(angle), length * (float)Math.Sin(angle));
		}

        public static void Truncate(ref Vector2 source, float maxLength)
        {
            var length = source.Length();
            if (length > maxLength)
            {
                source.Normalize();
                source *= maxLength;
            }
        }

        public static Vector2 RotateAboutOrigin(Vector2 point, Vector2 origin, float rotation)
        {
            return Vector2.Transform(point - origin, Matrix.CreateRotationZ(rotation)) + origin;
        }

        public static Vector2 RotateAboutOrigin(Vector2 point, Vector2 origin, ref Matrix rotation)
        {
            return Vector2.Transform(point - origin, rotation) + origin;
        }

        public static double GetAngle(Vector2 a, Vector2 b)
        {
            return Math.Atan2(b.Y - a.Y, b.X - a.X);
        }
    }
}
