using System.Diagnostics;
using System.Xml.Linq;

namespace Transition.Animations
{
    /// <summary>
    /// An animation command that simply changes the sequence number to a different, random one, picked from a list.
    /// </summary>
    public class GotoCommand : Command
    {			
        /// <summary>The sequence number we go to.</summary>
		int _newSequenceNumber;
	
        /// <summary>... or label.</summary>
		string _id;
        			
	    public override bool Execute(Sprite target)
        {
            if (_id != null) 
            {
				var seq = target.GetAnimation().GetLabel(_id);
				if (seq == -1) 
                {
					// Skip and ignore
					Trace.WriteLine("Warning: " + target.GetAnimation() + " missing label "+ _id);
				} else 
                {
					target.Sequence = seq;
				}
			} 
            else 
            {
				target.Sequence = _newSequenceNumber;
			}

			return true; // Always go immediately to the next command
        }

        public override void Load(XElement element) 
        {
			_id = element.GetAttributeString("id");
            
            if (_id == null)
                _newSequenceNumber = element.GetAttributeInt32("seq");
			
        }
    }
}
