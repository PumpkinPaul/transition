using System;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Manages game map data.
	/// </summary>
    public class GameMapManager : ResourceManager
    {
        public override string FolderName { get { return "GameMaps"; } }
        public override string SearchPattern { get { return "*.tmx"; } }

        readonly BaseGame _game;

		/// <summary>
        /// Creates a new instance of a OverlaysManager.
        /// </summary>
        public GameMapManager(BaseGame game)
        {
            if (game == null)
                throw new ArgumentNullException(nameof(game));

            _game = game;
        }
    }
}
