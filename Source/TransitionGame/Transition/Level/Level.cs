using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// A definition of a level
    /// </summary>
    public class Level
    {   
        public string Name;
        public Color Tint;
        public List<LevelEvent> Events = new List<LevelEvent>();
    }
}