using Microsoft.Xna.Framework.Graphics;

namespace Transition.Rendering
{
    public interface IGeometryBuffer
    {
        bool BufferWillOverflow(int newVerticesCount, int newIndicesCount);
        void GrowArrays();
        void AllocateGraphicsResources();

        void Reset();

        void SetBuffersToGraphicsDevice();
        void SetData();
        void CopyIndices(ushort[] indices, int indexPointer, int indexCount, int indexOffset);
        void CopyVertices(Geometry vertices, int count);

        int VertexPointer { get; set; }
        int IndexPointer { get; set; }

        int TotalVertices { get; set; }
        int TotalIndices { get; set; }

        DynamicVertexBuffer VertexBuffer { get; }

        void Dispose();
    }
}