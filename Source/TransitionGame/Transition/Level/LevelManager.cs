using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.Menus;
using Transition.ResourceManagers;

#if PROFILE
    using Microsoft.Xna.Framework.Input;
    using Transition.Input;
#endif

namespace Transition
{
    /// <summary>
    /// Manages levels transitions, actor spawning, etc
    /// </summary>
    public class LevelManager : ResourceManager
    {
        public override string FolderName => "Levels";

        enum Phase
        {
            Paused,
            Playing,
            LevelCompleted,
            DisplayLevelStats,
            GameComplete
        }

        Phase _phase;
        int _ticks;

        private readonly List<Level> _levels = new List<Level>();
        
		/// <summary>A reference to the game.</summary>
		private readonly TransitionGame _game;
		
		/// <summary>The universe we need to manage.</summary>
        private readonly Scene _scene;
		
		/// <summary>A rect signifying the level boundaries.</summary>
		public Rect Bounds { get; private set; }
		
        /// <summary>
        /// Gets or sets the number of te level currently being played for a particular universe.
        /// </summary>
        public int LevelNumber { get; private set; }

        public Level Level { get; private set; }

        public bool IsPlaying => _phase == Phase.Playing;

        /// <summary>
		/// Creates a new instance of a LevelManager.
		/// </summary>
		public LevelManager(TransitionGame game, Scene scene)
		{
			_game = game;
            _scene = scene;

            Bounds = new Rect(11.0f, -100, -11f, 100.0f);
		}

        /// <summary>
        /// Load the actor configuration data.
        /// </summary>
        public override void LoadContent()
        {
            const string filename = "levels.xml";

            var folder = Path.Combine(Platform.ResourcesPath, FolderName);

            var xdocument = XDocument.Load(Path.Combine(folder, filename));
			
            if (xdocument.Root == null)
                throw new XmlException("Missing Root element in " + filename);

			//Read in all the data...
            foreach(var levelElement in xdocument.Root.Elements("level"))
            {
                var level = new Level
                {
                    Name = levelElement.GetAttributeString("name"),
                    Tint = levelElement.GetAttributeColor("tint")
                };

                _levels.Add(level);

                foreach(var eventElement in levelElement.Elements("event"))
                {
                    var levelEvent = new LevelEvent {Ticks = eventElement.GetAttributeInt32("ticks")};


                    foreach(var actionElement in eventElement.Elements("action"))
                    {
                        var actionType = actionElement.GetAttributeEnum<LevelActionType>("type");
                        switch(actionType)
                        {
                            case LevelActionType.SpawnActor:
                                var spawnActor = new SpawnActorLevelAction {
                                    Type = actionType,
                                    Name = actionElement.GetAttributeString("name"),
                                    Count = actionElement.GetAttributeInt32("count"),
                                    LocationType = actionElement.GetAttributeEnum<LevelActionLocationType>("locationType"),
                                    Location = actionElement.GetAttributeVector2("location")
                                };

                                levelEvent.Actions.Add(spawnActor);

                                break;

                            case LevelActionType.NoMoreAliens:
                                var noMoreAliens = new NoMoreAliensLevelAction();
                                noMoreAliens.Type = actionType;
                                levelEvent.Actions.Add(noMoreAliens);
                                break;
                        }
                    }

                    level.Events.Add(levelEvent);
                }
            }
        }

        public void InitialisePlaySession(IReadableCheckpoint checkpoint)
        {
            LevelNumber = checkpoint.Level;
            InitialiseLevel();
        }

        /// <summary>
        /// Initialise the levels.
        /// </summary>
        public void InitialiseLevel()
        {
            ResetLevel();
            
            Level = _levels[LevelNumber - 1];
        }

        void ResetLevel()
        {
            _scene.RemoveAllActors();
            
            NextPhase(Phase.Playing);
        }

        public void DestroyPlaySession()
        {
            NextPhase(Phase.Paused);
        }

        public void BeginNextLevel()
        {
            LevelNumber++;
            InitialiseLevel();
        }
		
        //bool IsLevelComplete => false; // _scene.AlienCount <= 0 && _allAliensSpawned && _game.ActivePlayer.Ship.Health > 0; }
        bool IsLevelComplete => TransitionGame.Instance.GameMap.IsAtExit(TransitionGame.Instance.Player.Position);

        /// <summary>
		/// .
		/// </summary>
		public void Update()
		{
            //RandomHelper.DeterministicRandom.Log("LevelManager.Update()");

            _ticks++;

		    switch (_phase)
		    {
                case Phase.Paused:
                    break;

		        case Phase.Playing:
                    _game.Hud.DrawProgress = false;
		            SpawnActors();
		            
                    if (IsLevelComplete)
                        BegnLevelCompleted();
                
                    break;

                case Phase.LevelCompleted:
		            if (_ticks == 60)
		                BeginLevelStats();

                    break;

                case Phase.DisplayLevelStats:
                    //Wait here while menu is shown - that will be responsible for kicking off the call to begin the next level.
		            break;

                case Phase.GameComplete:
                    break;
		    }
        }

        void NextPhase(Phase phase)
        {
            _ticks = 0;
            _phase = phase;
        }

        public void Draw() 
        {
            
        }

        private void BegnLevelCompleted()
        {
            NextPhase(Phase.LevelCompleted);
        }

        private void BeginLevelStats()
        {
            NextPhase(Phase.DisplayLevelStats);
            TransitionGame.Instance.MenuManager.AddMenu(typeof(LevelCompleteMenu));
        }

        public void NoMoreAliens()
        {
            //_allAliensSpawned = true;
        }
		
		/// <summary>
		/// Spawns actors.
		/// </summary>
		private void SpawnActors()
		{
            var levelId = LevelNumber - 1;
		    if (levelId >= _levels.Count) {
		        return;
		    }

		    foreach(var levelEvent in Level.Events) {
		        //Are we ready to do this event?
		        if (levelEvent.Ticks != _ticks) {
		            continue;
		        }

		        //Do all actions for this event!
		        foreach (var action in levelEvent.Actions) {
		            action.Execute(this);
		        }
		    }
		}

        public void SpawnActor(SpawnActorLevelAction spawnActor)
        {
            var upper = Bounds.Top - 3.0f;
            var lower = Bounds.Bottom + 3.0f;

			for(var i = 0; i < spawnActor.Count; ++i)
			{
                var actorData = TransitionGame.Instance.DataManager.ActorData(spawnActor.Name);
                var actor = _game.ActorFactory.Create(actorData.ActorType, spawnActor.Name);
			    if (actor == null)
			    {
			        continue;
			    }

                if (spawnActor.LocationType == LevelActionLocationType.Random)
                    actor.Position = new Vector2(RandomHelper.DeterministicRandom.GetFloat(Bounds.Left, Bounds.Right), RandomHelper.DeterministicRandom.GetFloat(lower, upper));
                else
                    actor.Position = spawnActor.Location;
			    
                actor.Tint = Color.White;

    	        _game.Scene.AddActor(actor);
			}
        }
    }
}
