using System;
using System.Collections.Generic;

namespace Transition.GamePhases
{
    /// <summary>
    /// Manages trhe game's distinct phases - Splash, Intro, MainMenu, Play, etc.
    /// </summary>
    public class GamePhaseManager
    {
        public GamePhase ActivePhase { get; private set; }
        readonly Dictionary<Type, GamePhase> _gamePhases = new Dictionary<Type, GamePhase>();

		public void Register(GamePhase gamePhase)
        {
            if (gamePhase == null)
                throw new ArgumentNullException(nameof(gamePhase));

            var key = gamePhase.GetType();

            _gamePhases[key] = gamePhase;
        }

        /// <summary>
        /// Changes the current game phase.
        /// </summary>
        public void ChangePhase(Type type)
        {
            ActivePhase?.Destroy();

            var newPhase = Get(type);

			ActivePhase = newPhase;
			ActivePhase.Create();
		}

        GamePhase Get(Type key)
        {
            return _gamePhases[key];
        }

        public void Update()
        {
            ActivePhase.Update();
        }

        public void Render()
        {
            ActivePhase.Render();
        }
    }
}

