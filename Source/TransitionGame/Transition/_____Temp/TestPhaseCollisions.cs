using System;
using Microsoft.Xna.Framework;
using Transition.GamePhases;
using Transition.Input;
using Transition.LevelEditing;
using Transition.Menus;
using Transition.Rendering;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace Transition._____Temp
{
    /// <summary>
    /// Test playground
    /// </summary>
    public class TestPhaseCollisions : GamePhase
    {
        Vector2 _boxACentre;
        Vector2 _boxAExtents;
        Vector2 _boxAVelocity;

        Vector2 _boxBCentre;
        Vector2 _boxBExtents;
        Vector2 _boxBVelocity;

        bool _initialised;

        readonly DesignGrid _designGrid = new DesignGrid();

        enum SelectedBox
        {
            BoxA,
            BoxB,
        }

        SelectedBox _selectedBox = SelectedBox.BoxA;
        Vector2 _anchorPosition;
        bool _dragging;
        bool _velocitying;
        bool _sizing;
        float _sizeBoxSize = 0.5f;

        public TestPhaseCollisions(BaseGame game) : base(game) { }

        protected override void OnCreate()
        {
            Game.ScreenFlashManager.ClearColor = Color.White;
            Game.ScreenFlashManager.ClearColor = new Color(32, 32, 32);

            var mouseAndKeys = new MouseAndKeysInputMethod();
            Game.InputMethodManager.AddInputMethod(mouseAndKeys);
            Game.InputMethodManager.InputMethod = mouseAndKeys;

            Game.MenuManager.CloseAllMenus();
            _designGrid.Top = 12;
            _designGrid.Left = -20;
            _designGrid.Bottom = -12;
            _designGrid.Right = 20;
            _designGrid.StepId = 2;

            Reset();
        }

        void Reset()
        {
            _boxACentre = new Vector2(-5);
            _boxAExtents = new Vector2(2, 1);
            _boxAVelocity = new Vector2(3,1);

            _boxBCentre = new Vector2(5);
            _boxBExtents = new Vector2(6, 2);
            _boxBVelocity = new Vector2(0);
        }

        public Vector2 WorldPosition => Game.MenuManager.MouseWorldPosition;

        internal Vector2 ObjectPosition
        {
            get
            {
                //Depends on grid snapping!

                if (_designGrid.Active == false || KeyboardHelper.IsKeyPressed(Keys.LeftControl))
                    return WorldPosition;

                var halfStep = _designGrid.Step / 2.0f;
                var wp = WorldPosition + new Vector2(Math.Sign(WorldPosition.X) * halfStep, Math.Sign(WorldPosition.Y) * halfStep);

                return new Vector2(wp.X - wp.X % _designGrid.Step, wp.Y - wp.Y % _designGrid.Step);
            }
        }

        Box2 IsMouseOverCorner(Box2 box)
        {
            var bottomLeft = new Box2(box.Left - _sizeBoxSize, box.Y - _sizeBoxSize, _sizeBoxSize, _sizeBoxSize);
            if (bottomLeft.Contains(WorldPosition))
                return bottomLeft;

            var topLeft = new Box2(box.Left - _sizeBoxSize, box.Top, _sizeBoxSize, _sizeBoxSize);
            if (topLeft.Contains(WorldPosition))
                return topLeft;

            var bottomRight = new Box2(box.Right, box.Y - _sizeBoxSize, _sizeBoxSize, _sizeBoxSize);
            if (bottomRight.Contains(WorldPosition))
                return bottomRight;

            var topRight = new Box2(box.Right, box.Top, _sizeBoxSize, _sizeBoxSize);
            if (topRight.Contains(WorldPosition))
                return topRight;

            return Box2.Empty;
        }

        protected override void OnUpdate()
        {
            if (!_initialised)
            {
                //Game.MenuManager.AddMenu(typeof(CollisionMenu));
                _initialised = true;
            }

            if (KeyboardHelper.IsKeyJustReleased(Keys.Space))
                Reset();

            if (KeyboardHelper.IsKeyJustReleased(Keys.T) && KeyboardHelper.IsKeyPressed(Keys.LeftControl))
                Game.MenuManager.AddMenu(typeof(TestMenu));

            var boxA = new Box2(_boxACentre - _boxAExtents, _boxAExtents * 2);
            var boxB = new Box2(_boxBCentre - _boxBExtents, _boxBExtents * 2);

            var boxAVelocity = new Box2(_boxACentre + _boxAVelocity - _boxAExtents, _boxAExtents * 2);
            var boxBVelocity = new Box2(_boxBCentre + _boxBVelocity - _boxBExtents, _boxBExtents * 2);

            var boxASizer = IsMouseOverCorner(boxA);
            var boxBSizer = IsMouseOverCorner(boxB);

            if (MouseHelper.IsLeftButtonJustDown && boxASizer != Box2.Empty)
            {
                _selectedBox = SelectedBox.BoxA;
                _anchorPosition = ObjectPosition;
                _sizing = true;
            }
            if (MouseHelper.IsLeftButtonJustDown && boxBSizer != Box2.Empty)
            {
                _selectedBox = SelectedBox.BoxB;
                _anchorPosition = ObjectPosition;
                _sizing = true;
            }
            else if (MouseHelper.IsLeftButtonJustDown && boxA.Contains(Game.MenuManager.MouseWorldPosition) && KeyboardHelper.IsKeyPressed(Keys.V) == false)
            {
                _selectedBox = SelectedBox.BoxA;
                _anchorPosition = ObjectPosition;
                _dragging = true;
            }
            else if (MouseHelper.IsLeftButtonJustDown && boxB.Contains(Game.MenuManager.MouseWorldPosition) && KeyboardHelper.IsKeyPressed(Keys.V) == false)
            {
                _selectedBox = SelectedBox.BoxB;
                _anchorPosition = ObjectPosition;
                _dragging = true;
            }
            else if (MouseHelper.IsLeftButtonJustDown && boxAVelocity.Contains(Game.MenuManager.MouseWorldPosition))
            {
                _selectedBox = SelectedBox.BoxA;
                _anchorPosition = ObjectPosition;
                _velocitying = true;
            }
            else if (MouseHelper.IsLeftButtonJustDown && boxBVelocity.Contains(Game.MenuManager.MouseWorldPosition))
            {
                _selectedBox = SelectedBox.BoxB;
                _anchorPosition = ObjectPosition;
                _velocitying = true;
            }

            if (MouseHelper.IsLeftButtonDown && _dragging)
            {
                if (_selectedBox == SelectedBox.BoxA)
                {
                    _boxACentre += ObjectPosition - _anchorPosition;
                    _anchorPosition = ObjectPosition;
                }    
                else if(_selectedBox == SelectedBox.BoxB)
                {
                    _boxBCentre += ObjectPosition - _anchorPosition;
                    _anchorPosition = ObjectPosition;
                }
            }
            else if (MouseHelper.IsLeftButtonDown && _velocitying)
            {
                if (_selectedBox == SelectedBox.BoxA)
                {
                    _boxAVelocity += ObjectPosition - _anchorPosition;
                    _anchorPosition = ObjectPosition;
                }
                else if (_selectedBox == SelectedBox.BoxB)
                {
                    _boxBVelocity += ObjectPosition - _anchorPosition;
                    _anchorPosition = ObjectPosition;
                }
            }
            else if (MouseHelper.IsLeftButtonDown && _sizing)
            {
                if (_selectedBox == SelectedBox.BoxA)
                {
                    var delta = (ObjectPosition - _anchorPosition) / 2.0f;

                    _boxACentre += delta;

                    if (WorldPosition.X < _boxACentre.X)
                        delta.X = -delta.X;

                    if (WorldPosition.Y < _boxACentre.Y)
                        delta.Y = -delta.Y;

                    _boxAExtents += delta;
                   
                    _anchorPosition = ObjectPosition;
                }
                else if (_selectedBox == SelectedBox.BoxB)
                {
                    var delta = (ObjectPosition - _anchorPosition) / 2.0f;

                    _boxBCentre += delta;

                    if (WorldPosition.X < _boxBCentre.X)
                        delta.X = -delta.X;

                    if (WorldPosition.Y < _boxBCentre.Y)
                        delta.Y = -delta.Y;

                    _boxBExtents += delta;

                    _anchorPosition = ObjectPosition;
                }
            }

            if (MouseHelper.IsLeftButtonJustUp)
            {
                if (_dragging)
                {

                    _dragging = false;
                }
                else if (_sizing)
                {

                    _sizing = false;
                }
                else if (_velocitying)
                {

                    _velocitying = false;
                }
            }

        }
    
        protected override void OnRender()
        {
            var font = Theme.Current.GetFont(Theme.Fonts.Default);

            TransitionGame.Instance.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);

            _designGrid.Draw(TransitionGame.Instance.Renderer, Game.CameraManager.Camera);

            //Draw coordinates over the desgin grid (probably want to move this to the DesignGrid istself)
            for (var x = -20; x <= 20.0f; x += 1)
                TransitionGame.Instance.Renderer.DrawNumber(font, new Vector2(x, 0), new Vector2(0.65f), x, Alignment.Centre, Color.White, TransparentRenderStyle.Instance);

            for (var y = -11; y <= 11; y += 1)
                TransitionGame.Instance.Renderer.DrawNumber(font, new Vector2(0, y), new Vector2(0.65f), y, Alignment.Centre, Color.White, TransparentRenderStyle.Instance);
            
            //Now draw the geometry
            var pixel = Game.TextureManager.GetTextureInfo("Pixel");
            var quadA = new Quad(_boxACentre, _boxAExtents * 2.0f);
            TransitionGame.Instance.Renderer.DrawSprite(ref quadA, pixel.TextureId, pixel.SourceRect, FlatTheme.Alizarin * 0.75f, TransparentRenderStyle.Instance);
            TransitionGame.Instance.Renderer.DrawString(font, _boxACentre, new Vector2(0.65f), "A", Alignment.Centre, Color.White, TransparentRenderStyle.Instance, 1000);

            var quadB = new Quad(_boxBCentre, _boxBExtents * 2.0f);
            TransitionGame.Instance.Renderer.DrawSprite(ref quadB, pixel.TextureId, pixel.SourceRect, FlatTheme.PeterRiver * 0.75f, TransparentRenderStyle.Instance);
            TransitionGame.Instance.Renderer.DrawString(font, _boxBCentre, new Vector2(0.65f), "B", Alignment.Centre, Color.White, TransparentRenderStyle.Instance, 1000);

            //Draw a box around the current selection.
            TransitionGame.Instance.Renderer.DrawBoxStyled(
                _selectedBox == SelectedBox.BoxA ? new Box2(_boxACentre - _boxAExtents, _boxAExtents*2) : new Box2(_boxBCentre - _boxBExtents, _boxBExtents*2), 
                Color.White, Renderer.LineStyle.Dotted,
                0.045f, 1500);

            //Velocities
            TransitionGame.Instance.Renderer.DrawBox(new Box2(_boxACentre + _boxAVelocity - _boxAExtents, _boxAExtents * 2), FlatTheme.Alizarin, TransparentRenderStyle.Instance, 0.045f, 1500);
            TransitionGame.Instance.Renderer.DrawBox(new Box2(_boxBCentre + _boxBVelocity - _boxBExtents, _boxBExtents * 2), FlatTheme.PeterRiver, TransparentRenderStyle.Instance, 0.045f, 1500);

            //Motion vector of each geometry
            TransitionGame.Instance.Renderer.DrawLineStyled(_boxACentre, _boxACentre + _boxAVelocity, FlatTheme.Alizarin * 0.5f, Renderer.LineStyle.Dotted, 0.02f);
            TransitionGame.Instance.Renderer.DrawLineStyled(_boxBCentre, _boxBCentre + _boxBVelocity, FlatTheme.PeterRiver * 0.5f, Renderer.LineStyle.Dotted, 0.02f);

            //Sizing handles
            var boxA = new Box2(_boxACentre - _boxAExtents, _boxAExtents * 2);
            var boxB = new Box2(_boxBCentre - _boxBExtents, _boxBExtents * 2);

            var boxASizer = IsMouseOverCorner(boxA);
            var boxBSizer = IsMouseOverCorner(boxB);

            if (boxASizer != Box2.Empty)
                TransitionGame.Instance.Renderer.DrawBox(boxASizer, Color.White, TransparentRenderStyle.Instance, 0.045f, 1500);
            else if (boxBSizer != Box2.Empty)
                TransitionGame.Instance.Renderer.DrawBox(boxBSizer, Color.White, TransparentRenderStyle.Instance, 0.045f, 1500);

            //Perform the collision test!
            float t1;
            float t2;
            var collision = Collisions.TestAABBSweep(_boxAExtents, _boxACentre, _boxAVelocity, _boxBExtents, _boxBCentre, _boxBVelocity, out t1, out t2);

            TransitionGame.Instance.Renderer.DrawString(font, new Vector2(-15, 6), new Vector2(1f), $"Collision: {collision}", Alignment.CentreLeft, collision ? FlatTheme.Emerald : FlatTheme.Silver, TransparentRenderStyle.Instance, 1000);
            TransitionGame.Instance.Renderer.DrawString(font, new Vector2(-15, 5), new Vector2(1f), $"t1: {t1:N2} ... t2: {t2:N2}", Alignment.CentreLeft, collision ? FlatTheme.Emerald : FlatTheme.Silver, TransparentRenderStyle.Instance, 1000);

            //Draw collision locations of each geometry if there is a collision
            if (collision)
            {
                TransitionGame.Instance.Renderer.DrawBoxStyled(new Box2(_boxACentre + (_boxAVelocity*t1) - _boxAExtents, _boxAExtents * 2), FlatTheme.Alizarin, Renderer.LineStyle.Dotted, 0.025f, 1500);
                TransitionGame.Instance.Renderer.DrawBoxStyled(new Box2(_boxBCentre + (_boxBVelocity * t1) - _boxBExtents, _boxBExtents * 2), FlatTheme.PeterRiver, Renderer.LineStyle.Dotted, 0.025f, 1500);
            }

            TransitionGame.Instance.Renderer.End();
        }
    }
}