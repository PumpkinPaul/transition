using System.Xml.Serialization;

namespace Transition
{
    /// <summary>
    /// Represents an interface for the data required to start a game at a particluar place.
    /// </summary>
    public interface IReadableCheckpoint
    {        
        string ShipName { get; }
        Difficulty Difficulty { get; }
        int Level { get; }
        int Score { get; }
        int Health { get; }
        int RagePower { get; }
    }
}
