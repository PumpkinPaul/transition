using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using Microsoft.Xna.Framework;
using Transition.Eval;
using Type = System.Type;

namespace Transition.Util 
{
    /// <summary>
    /// 
    /// </summary>
    public static class ScriptManager
    {	    
        /// <summary>
        /// A stash of variables which we automatically decode
        /// </summary>
        private static readonly IDictionary<string, string> Variables = new Dictionary<string, string>();

        public static void AddVariable(string name, string value)
        {
		    Variables[name] = value;
	    }

        public static void AddVariable(string name, float value)
        {
		    Variables[name] = value.ToString(CultureInfo.InvariantCulture);
	    }

        public static void AddVariable(string name, Vector2 value)
        {
		    Variables[name] = $"{value.X},{value.Y}";
        }

        public static void AddVariable(string name, Color value)
        {
		    Variables[name] = $"{value.R},{value.G},{value.B},{value.A}";
        }

        public static string Parse(string input) 
        {
            if (input.Length < 2 || input[0] != '=') {
			    return input;
		    }

		    var original = input;
		    // Replace all variables recursively
		    int dollarPosition;
		    var minPos = 0;
		    while ((dollarPosition = input.IndexOf('$', minPos)) != -1) 
            {
			    if (dollarPosition > 0 && input[dollarPosition - 1] == '\\') 
                {
				    // It was an escaped dollar
				    break;
			    }

			    // Scan to next operator
			    var endPosition = input.Length;
			    var escaped = false;
			    var tokenBuilder = new StringBuilder(input.Length);
			    tokenBuilder.Append("$");
			    
                for (var i = dollarPosition + 1; i < input.Length; i ++) 
                {
				    var c = input[i];
				    if (c == '\\') 
                    {
					    if (escaped) 
                        {
						    tokenBuilder.Append(c);
						    continue;
					    } 

    				    escaped = true;
				    }
				    if (escaped)
					    continue;

				    if (c == '+' || c == '-' || c == '/' || c == '*' || c == ',' || c == '$' || c == '(' || c == ')' || Char.IsWhiteSpace(c)) 
                    {
					    endPosition = i;
					    break;
				    } 
					 
                    tokenBuilder.Append(c);
			    }
			    var token = tokenBuilder.ToString();
			    try 
                {
				    var decoded = Decode(token);
				    if (!decoded.Equals(token)) 
					    input = input.Substring(0, dollarPosition) + decoded + input.Substring(endPosition);
				    else 
					    minPos = dollarPosition + 1;
				    
			    } 
                catch (Exception) 
                {
				    Debug.WriteLine("Failed to parse "+original);
				    throw;
			    }
		    }

		    var ret = new StringBuilder(input.Length * 2);
		    // First split the incoming string up into comma separated chunks, if any
		    var st = new StringTokenizer(input.Substring(1), ",");
		    while (st.HasMoreTokens()) 
            {
			    var subElement = st.NextToken().Trim();
			    if (subElement[0] == '=') 
				    subElement = subElement.Substring(1);

			    // Replace all escaped chars with themselves by simply removing the \
			    subElement = subElement.Replace("\\", "");
			    try 
                {
				    // If it's a string, simply use verbatim
				    if (Character.IsIdentifierStart(subElement[0])) 
					    ret.Append(subElement);
				    else 
					    ret.Append(Expression.Eval(subElement));
			    } 
                catch (Exception) 
                {
				    // It wasn't an expression after all. Or maybe there was an error
				    ret.Append(subElement);
				    Debug.WriteLine("Failed to parse " + original + " (sent " + subElement + " to parser)");
			    }
			    if (st.HasMoreTokens()) 
				    ret.Append(',');
		    }
		    
		    return ret.ToString();
        }

        private static string Decode(string input) 
        {
		    if (input != null && input.Length > 1 && input[0] == '$') {
			    var key = input.Substring(1);
                if (key[0] == '#') 
                {
                    // It's a class name and reflection job in the form $$full.class.name.member
                    var lastIdx = key.LastIndexOf('.');
                    var member = key.Substring(lastIdx + 1);
                    var className = key.Substring(1, lastIdx - 1);
                    var clazz = Type.GetType(className);
                    var field = clazz.GetField(member);
                    
                    return field.GetValue(null).ToString();
                }

		        if (Variables.ContainsKey(key) == false)
                    throw new InvalidOperationException("Unknown variable "+input);

                return Variables[key];
		    }
			 
            return input;
		}
    }
}
