﻿using System;
using System.Collections.Generic;

namespace Transition._____Temp.RewindBinaryFormatter
{
    [Serializable]
    public class World
    {
        readonly List<Entity> _entities = new List<Entity>();

        public void AddEntity(Entity entity)
        {
            _entities.Add(entity);
        }

        public void BeforeRewind()
        {
            foreach(var entity in _entities)
                entity.BeforeRewind();
        }

        public void StateLoaded(bool rewinding)
        {
            foreach(var entity in _entities)
                entity.StateLoaded(rewinding);
        }

        public void Update(bool wasRewinding)
        {
            foreach(var entity in _entities)
                entity.Update(wasRewinding: true);
        }

        public void Draw()
        {
            foreach (var entity in _entities)
                entity.Draw();
        }
    }
}