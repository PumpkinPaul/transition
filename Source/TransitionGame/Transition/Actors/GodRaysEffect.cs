using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Rendering;

namespace Transition.Actors
{
    /// <summary>
    /// Represents a 'god rays' effect - think Treasure Tomb / Stealth B@$t@rd
    /// </summary>
    public class GodRaysEffect : SpecialEffect
    {
        private GodRaysTemplate _template;
        
        private enum Phase
        {
            Hidden,
            FadeIn,
            Normal,
            FadeOut,
        }

        private Phase _phase;

        private float _masterAlpha;
        private int _ticks;
        private int _cachedTicks;

        static ushort[] _indices;
        readonly Geometry<VertexPositionColorTexture> _geometry = new Geometry<VertexPositionColorTexture>();

        private readonly float[] _angles; 
        private readonly float[] _rotations; 
        private readonly float[] _outerRadusMods;        

        private readonly int _rayCount;

        private float _innerRadius;
        private float _outerRadius;

        /// <summary>
        /// Creates a new instance of a GodRaysEffect.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        /// <param name="rayCount"></param>
        public GodRaysEffect(TransitionGame game, int rayCount) : base(game)
        {
            _rayCount = rayCount;

            //Have we got an index buff and is there enough room in there for all these points?
            if (_indices == null || rayCount < _indices.Length)
                CreateIndexBuffer();
            
            _angles = new float[rayCount];
            _outerRadusMods = new float[rayCount];
            _rotations = new float[rayCount];

            _geometry.RenderStyle = TransparentRenderStyle.Instance;
            _geometry.TextureId = -1;
            _geometry.Indices = _indices;
            _geometry.IndexCount = _indices.Length;

            
        }

        private void CreateIndexBuffer()
        {
            //Build the index buffer - 
            _indices = new ushort[(_rayCount * 6)];
            
            var vertexPointer = 0;
            for (var i = 0; i < _indices.Length; i += 6) 
            {
                //Triangle 1
                _indices[i] = (ushort)(vertexPointer);
                _indices[i + 1] = (ushort)(vertexPointer + 1);
                _indices[i + 2] = (ushort)(vertexPointer + 2);

                //Triangles 2
                _indices[i + 3] = (ushort)(vertexPointer + 2);
                _indices[i + 4] = (ushort)(vertexPointer + 1);
                _indices[i + 5] = (ushort)(vertexPointer + 3);

                vertexPointer += 4;
            }
        }

        protected override void OnInitialise(string name) 
        {
            base.OnInitialise(name);

            _ticks = 0;
            _cachedTicks = 0;
            _masterAlpha = 0;
            _phase = Phase.Hidden;

            //TODO:
            _active = true;
        }

        public void Setup(GodRaysTemplate template)
        {
            _template = template;

            float step = 360.0f / _rayCount; 

            var i = 0;
            for (var a = 0.0f; a < 360.0f; a += step, i++) 
            {
                _angles[i] = MathHelper.ToRadians(a);

                _outerRadusMods[i] = RandomHelper.Random.GetFloat(_template.OuterRadiusModMin, _template.OuterRadiusModMax);

                if (RandomHelper.Random.GetBool())
                    _rotations[i] = RandomHelper.Random.GetFloat(_template.RotationMin, _template.RotationMax);
                else
                    _rotations[i] = RandomHelper.Random.GetFloat(-_template.RotationMax, -_template.RotationMin);
            }

            //TODO:
            _active = true;
        }
        
        public void FadeIn()
        {
            if (_phase == Phase.FadeIn || _phase == Phase.Normal)
                return; //Already in or fading in

            _phase = Phase.FadeIn;
            _cachedTicks = 0;
        }

        public void FadeOut()
        {
            if (_phase == Phase.FadeOut || _phase == Phase.Hidden)
                return; //Already out or fading out

            _phase = Phase.FadeOut;
        }

        protected override void OnUpdate(GameTime gameTime)
        {   
            switch (_phase)
            {
                case Phase.Hidden:
                    break;

                case Phase.FadeIn:
                    if (_ticks < _template.FadeInTicks)
                        _ticks++;

                    if (_ticks >= _template.FadeInTicks)
                    {
                        _phase = Phase.Normal;
                        _masterAlpha = _template.MasterAlphaMax;
                        _ticks = 0;
                        _cachedTicks = _template.FadeInTicks;
                    }
                    else
                    {
                        var ratio = (float)_ticks / _template.FadeInTicks;
                        _masterAlpha = MathHelper.Lerp(_template.MasterAlphaMin, _template.MasterAlphaMax, ratio);
                    }
                    break;

                case Phase.Normal:
                    if (_ticks < _template.VisibleTicks)
                        _ticks++;

                    if (_ticks >= _template.VisibleTicks)
                    {
                        _cachedTicks += _template.VisibleTicks;
                        _ticks = 0;
                        FadeOut();
                    }
                    break;

                case Phase.FadeOut:
                    if (_ticks < _template.FadeOutTicks)
                        _ticks++;

                    if (_ticks >= _template.FadeOutTicks)
                    {
                        _ticks = 0;
                        _cachedTicks += _template.FadeOutTicks;
                        _masterAlpha = _template.MasterAlphaMin;
                        _phase = Phase.Hidden;
                    }
                    else
                    {
                        var ratio = (float)_ticks / _template.FadeOutTicks;
                        _masterAlpha = MathHelper.Lerp(_template.MasterAlphaMax, _template.MasterAlphaMin, ratio);
                    }
                    break;
            }
                
            var lifeRatio = (float)(_ticks + _cachedTicks) / _template.TotalTicks;
            _innerRadius = MathHelper.SmoothStep(_template.InnerRadiusStart, _template.InnerRadiusEnd, lifeRatio);
            _outerRadius = MathHelper.SmoothStep(_template.OuterRadiusStart, _template.OuterRadiusEnd, lifeRatio);

            //Rotate the rays)
            for (var i = 0; i < _rayCount; i++) 
                _angles[i] += _rotations[i];
        }

        public void DrawEffect() 
        {
            _geometry.TextureId = _template.TextureInfo.TextureId;
            _geometry.Layer = Layer;
            _geometry.Vertices.Clear();            
            
            var innerWidth = MathHelper.ToRadians(_template.WidthStart);
            var outerWidth = MathHelper.ToRadians(_template.WidthEnd);
            
            //Convert the list of angles to a 'rays'.)
            for (var i = 0; i < _rayCount; i++) 
            {
                var rotation = _rotations[i];

                var a1 = _angles[i] - innerWidth;
                var a2 = _angles[i] + innerWidth;
                var a3 = _angles[i] - outerWidth;
                var a4 = _angles[i] + outerWidth;

                var cos1 = (float)Math.Cos(a1 + rotation);
                var sin1 = (float)Math.Sin(a1 + rotation);

                var cos2 = (float)Math.Cos(a2 + rotation);
                var sin2 = (float)Math.Sin(a2 + rotation);

                var cos3 = (float)Math.Cos(a3 + rotation);
                var sin3 = (float)Math.Sin(a3 + rotation);

                var cos4 = (float)Math.Cos(a4 + rotation);
                var sin4 = (float)Math.Sin(a4 + rotation);

                // Construct rays as follows...
                // p3----p2
                // |    /|
                // |  /  |
                // |/    |
                // p1----p0

                //...and draw each ray in an anti clockwise motion
                //      2 
                //  3       1
                // 4         0
                //  5       7    
                //      6
                var outerRadius = _outerRadius + _outerRadusMods[i];

                var p0 = new Vector3(Position.X + cos1 * _innerRadius, Position.Y + sin1 * _innerRadius, 0.0f);
                var p1 = new Vector3(Position.X + cos2 * _innerRadius, Position.Y + sin2 * _innerRadius, 0.0f);
                var p2 = new Vector3(Position.X + cos3 * outerRadius, Position.Y + sin3 * outerRadius, 0.0f);
                var p3 = new Vector3(Position.X + cos4 * outerRadius, Position.Y + sin4 * outerRadius, 0.0f);

                var innerColor = _template.InnerColor * _masterAlpha;
                var outerColor = _template.OuterColor * _masterAlpha;

                _geometry.Vertices.Add(new VertexPositionColorTexture(p0, innerColor, new Vector2(_template.TextureInfo.SourceRect.Left, _template.TextureInfo.SourceRect.Bottom)));
                _geometry.Vertices.Add(new VertexPositionColorTexture(p1, innerColor, new Vector2(_template.TextureInfo.SourceRect.Left, _template.TextureInfo.SourceRect.Top)));
                _geometry.Vertices.Add(new VertexPositionColorTexture(p2, outerColor, new Vector2(_template.TextureInfo.SourceRect.Right, _template.TextureInfo.SourceRect.Bottom)));
                _geometry.Vertices.Add(new VertexPositionColorTexture(p3, outerColor, new Vector2(_template.TextureInfo.SourceRect.Right, _template.TextureInfo.SourceRect.Top)));
            }

            _game.Renderer.DrawGeometry(_geometry);
        }
    }
}
