using ProtoBuf;

namespace Transition.Animations
{
    /// <summary>
    /// A serialized version of a sprite
    /// </summary>
    [ProtoContract]
    public class SerializedSprite
    {				
        /// <summary>The Animation if any.</summary>
        [ProtoMember(1)]
        public string AnimationName;

        [ProtoMember(2)]
        public int Sequence;

        [ProtoMember(3)]
        public int Tick;

        [ProtoMember(4)]
        public int Layer;

        [ProtoMember(5)]
        public int Event;

        [ProtoMember(6)]
        public bool Active;

        [ProtoMember(7)]
        public bool Visible;

        [ProtoMember(8)]
        public bool Paused;

        [ProtoMember(9)]
        public string TextureInfoName;

        //static readonly EntityDataSurrogate Instance = new EntityDataSurrogate();


        public static implicit operator SerializedSprite(Sprite sprite)
        {
            if (sprite == null)
                return null;

            //return new EntityDataSurrogate { ResourceName = value.Name };
            //Instance.ResourceName = value.ResourceName;
            //return Instance;

            var animation = sprite.GetAnimation();
            var textureInfo = sprite.TextureInfo;

            return new SerializedSprite
            {
                AnimationName = animation == null ? null : animation.Name,

                Sequence = sprite.Sequence,
                Tick = sprite.Tick,
                Layer = sprite.Layer,
                Event = sprite.Event,
                Active = sprite.Active,
                Visible = sprite.Visible,
                Paused = sprite.Paused,
                TextureInfoName = textureInfo == null ? null : sprite.TextureInfo.SpriteImageName
            };
        }

        public static implicit operator Sprite(SerializedSprite value)
        {
            var sprite =  TransitionGame.Instance.SpritePool.Allocate();

            if (sprite == null)
                return null;

            sprite.Sequence = value.Sequence;
            sprite.Tick = value.Tick;
            sprite.Layer = value.Layer;
            sprite.Event = value.Event;
            sprite.Active = value.Active;
            sprite.Visible = value.Visible;
            sprite.Paused = value.Paused;

            if (value.AnimationName != null)
                sprite.SetAnimation(TransitionGame.Instance.AnimationManager.Get(value.AnimationName), rewind: false);

            if (value.TextureInfoName != null)
                sprite.TextureInfo = TransitionGame.Instance.TextureManager.GetTextureInfo(value.TextureInfoName);

            return sprite;
        }
    }
}

