using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the controls menu.
    /// </summary>
    public class ControlsMenu : Menu
    {
        TextLabelControl _upValue;
        TextLabelControl _downValue;
        TextLabelControl _leftValue;
        TextLabelControl _rightValue;
        TextLabelControl _fireValue;
        TextLabelControl _rollValue;
        TextLabelControl _pauseValue;

        /// <summary>
        /// Initialises a new instance of a ControlsMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
        public ControlsMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            _upValue = GetControl<TextLabelControl>("upValue");
            _downValue = GetControl<TextLabelControl>("downValue");
            _leftValue = GetControl<TextLabelControl>("leftValue");
            _rightValue = GetControl<TextLabelControl>("rightValue");
            _fireValue = GetControl<TextLabelControl>("fireValue");
           // _rollValue = GetControl<TextLabelControl>("rollValue");
            _pauseValue = GetControl<TextLabelControl>("pauseValue");
        }

        protected override void OnAddedToManager() 
        {
            _upValue.Text = Game.ActivePlayer.Profile.InputKeys.UpKeyHint;
            _downValue.Text = Game.ActivePlayer.Profile.InputKeys.DownKeyHint;
            _leftValue.Text = Game.ActivePlayer.Profile.InputKeys.LeftKeyHint;
            _rightValue.Text = Game.ActivePlayer.Profile.InputKeys.RightKeyHint;
            _fireValue.Text = Game.ActivePlayer.Profile.InputKeys.FireKeyHint;
            //_rollValue.Text = Game.ActivePlayer.Profile.InputKeys.RollKeyHint;
            _pauseValue.Text = Game.ActivePlayer.Profile.InputKeys.PauseKeyHint;
        }
    }
}
