using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// A set of keys that control game actions.
    /// </summary>
    public struct Quad
    {
        public Vector3 BottomLeft;
        public Vector3 TopLeft;
        public Vector3 BottomRight;
        public Vector3 TopRight;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="scale"></param>
        /// <param name="rotationZ"></param>
        public Quad(Vector2 position, Vector2 scale, float rotationZ)
        {
            BottomLeft = new Vector3(-0.5f, -0.5f, 0.0f);
            TopLeft = new Vector3(-0.5f, 0.5f, 0.0f);
            BottomRight = new Vector3(0.5f, -0.5f, 0.0f);
            TopRight = new Vector3(0.5f, 0.5f, 0.0f);

            var m = Matrix.CreateScale(scale.X, scale.Y, 0.0f) * Matrix.CreateRotationZ(rotationZ) * Matrix.CreateTranslation(position.X, position.Y, 0.0f);
            BottomLeft = Vector3.Transform(BottomLeft, m);
            TopLeft = Vector3.Transform(TopLeft, m);
            BottomRight = Vector3.Transform(BottomRight, m);
            TopRight = Vector3.Transform(TopRight, m);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="scale"></param>
        public Quad(Vector2 position, Vector2 scale)
        {
            BottomLeft = new Vector3(-0.5f, -0.5f, 0.0f);
            TopLeft = new Vector3(-0.5f, 0.5f, 0.0f);
            BottomRight = new Vector3(0.5f, -0.5f, 0.0f);
            TopRight = new Vector3(0.5f, 0.5f, 0.0f);

            var m = Matrix.CreateScale(scale.X, scale.Y, 0.0f) * Matrix.CreateTranslation(position.X, position.Y, 0.0f);
            BottomLeft = Vector3.Transform(BottomLeft, m);
            TopLeft = Vector3.Transform(TopLeft, m);
            BottomRight = Vector3.Transform(BottomRight, m);
            TopRight = Vector3.Transform(TopRight, m);
        }

        public Quad(float top, float left, float bottom, float right)
        {
            BottomLeft = new Vector3(left, bottom, 0.0f);
            TopLeft = new Vector3(left, top, 0.0f);
            BottomRight = new Vector3(right, bottom, 0.0f);
            TopRight = new Vector3(right, top, 0.0f);
        }

        public Quad(Rect rect)
        {
            BottomLeft = new Vector3(rect.Left, rect.Bottom, 0.0f);
            TopLeft = new Vector3(rect.Left, rect.Top, 0.0f);
            BottomRight = new Vector3(rect.Right, rect.Bottom, 0.0f);
            TopRight = new Vector3(rect.Right, rect.Top, 0.0f);
        }

        public Rect Rect
        {
            get { return new Rect(TopLeft.Y, TopLeft.X, BottomRight.Y, BottomRight.X); }
            set { TopLeftBottomRight(value.Top, value.Left, value.Bottom, value.Right); }
        }

        public void Top(float top)
        {
            TopLeft = new Vector3(TopLeft.X, top, 0.0f);
            TopRight = new Vector3(TopRight.X, top, 0.0f);
        }

        public void Bottom(float bottom)
        {
            BottomLeft = new Vector3(BottomLeft.X, bottom, 0.0f);
            BottomRight = new Vector3(BottomRight.X, bottom, 0.0f);
        }

        public void Right(float right)
        {
            BottomRight = new Vector3(right, BottomRight.Y, 0.0f);
            TopRight = new Vector3(right, TopRight.Y, 0.0f);
        }

        public void Left(float left)
        {
            BottomLeft = new Vector3(left, BottomLeft.Y, 0.0f);
            TopLeft = new Vector3(left, TopLeft.Y, 0.0f);
        }

        public void TopLeftBottomRight(float top, float left, float bottom, float right)
        {
            BottomLeft = new Vector3(left, bottom, 0.0f);
            TopLeft = new Vector3(left, top, 0.0f);
            BottomRight = new Vector3(right, bottom, 0.0f);
            TopRight = new Vector3(right, top, 0.0f);
        }

        public void Move(float x, float y)
        {
            var top = TopLeft.Y;
            var left = TopLeft.X;
            var bottom = BottomRight.Y;
            var right = BottomRight.X;

            BottomLeft = new Vector3(left + x, bottom + y, 0.0f);
            TopLeft = new Vector3(left + x, top + y, 0.0f);
            BottomRight = new Vector3(right + x, bottom + y, 0.0f);
            TopRight = new Vector3(right + x, top + y, 0.0f);
        }
    }
}