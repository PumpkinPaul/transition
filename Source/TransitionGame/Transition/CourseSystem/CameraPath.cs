using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Transition.CourseSystem
{
    public class CameraPath 
    {
        public List<Vector2> Vertices = new List<Vector2>();

        public int PathSegments
        {
            get { return (Vertices.Count - 1) / 3; }
        }

        public float Clamp(float cameraTime)
        {
            return MathHelper.Clamp(cameraTime, 0.0f, PathSegments);
        }

        public Vector2 Bezier(float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
        {
            var x = OrdinateX(t, p0, p1, p2, p3);
            var y = OrdinateY(t, p0, p1, p2, p3);

            return new Vector2(x, y);
        }

        static float OrdinateX(float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
        {
            return (float)((Math.Pow((1 - t), 3) * p0.X) + (3 * Math.Pow((1 - t), 2) * t * p1.X) + (3 * (1 - t) * (t * t) * p2.X) + ((t * t * t) * p3.X));
        }

        static float OrdinateY(float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
        {
            return (float)((Math.Pow((1 - t), 3) * p0.Y) + (3 * Math.Pow((1 - t), 2) * t * p1.Y) + (3 * (1 - t) * (t * t) * p2.Y) + ((t * t * t) * p3.Y));
        } 
    }
}
