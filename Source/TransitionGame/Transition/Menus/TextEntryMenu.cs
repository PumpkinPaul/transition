using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the pause menu.
    /// </summary>
    public class TextEntryMenu : Menu
    {
        const int KeyRows = 4;
        const int KeyCols = 10;

        readonly string[,] _keyboard = new [,]{
                {"1","2","3","4","5","6","7","8","9","0"}, 
                {"q","w","e","r","t","y","u","i","o","p"}, 
                {"a","s","d","f","g","h","j","k","l","-"},
                {"z","x","c","v","b","n","m",".","_","*"}};

        readonly Dictionary<MenuControl, ImageControl> _controlLink = new Dictionary<MenuControl, ImageControl>();

        readonly TextButtonControl[,] _keyControls = new TextButtonControl[KeyRows, KeyCols];

        TextLabelControl _textLabel;
        IntegerLabelControl _charCountLabel;
        TextLabelControl _charOfLabel;
        IntegerLabelControl _maxLengthLabel;
        string _initialText = string.Empty;

        int _maxLength;
        public int MaxLength 
        { 
            get { return _maxLength; }
            set 
            { 
                _maxLength = value; 
                _maxLengthLabel.Value = value;
            }
        }

        public string InitialText 
        {
            get { return _initialText; }
            set 
            {
                if (value == null)
                    value = string.Empty;

                if (value.Length > MaxLength)
                    value = value.Substring(0, MaxLength);

                _initialText = value; 

                _charCountLabel.Value = value.Length;
            }
        }

        public string Text 
        {
            get { return _textLabel.Text; }
            private set { _textLabel.Text = value; }
        }
        
        /// <summary>
        /// Initialises a new instance of a PauseMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public TextEntryMenu(TransitionGame game, string title) : base(game, title) { }

        protected override void OnConfigureMenu()
        {
            const int letterButtonWidth = 2;
            const int letterButtonHeight = 2;

            var anchorPosition = new Vector2(-13.5f, 2);
            var position = anchorPosition;

            for (var col = 0; col < KeyCols; col++)
            {
                position.Y = anchorPosition.Y;
                
                for (var row = 0; row < KeyRows; row++)                
                {
                    var button = new TextButtonControl(Game);
                    button.CanIncreaseDecrease = true;
                    button.Position = position;
                    //button.Text = "  " + _keyboard[row, col] + "  ";
                    button.Text = _keyboard[row, col];
                    button.Click += (sender, args) => 
                    { 
                        if (_textLabel.Text.Length < MaxLength) 
                        {
                            _textLabel.Text += ((TextButtonControl)sender).Text; 
                            _charCountLabel.Value = _textLabel.Text.Length; 
                        }
                    };

                    button.MinimumSize = new Vector2(letterButtonWidth - 0.1f, letterButtonHeight - 0.1f);
                    _keyControls[row, col] = button;

                    var imageControl = new ImageControl(Game);
                    imageControl.Position = button.Position;
                    imageControl.Tint = Color.White * 0.35f;
                    imageControl.Size = button.MinimumSize;
                    imageControl.TextureInfo = Game.TextureManager.GetTextureInfo("Pixel");
                    
                    position.Y -= letterButtonHeight;

                    _controlLink.Add(button, imageControl);

                    AddControl(imageControl, NoTabIndex);
                    AddControl(button, GetNextTabIndex());
                }

                position.X += letterButtonWidth;
            }

            position.Y = anchorPosition.Y;
            position.X += (letterButtonWidth * 1.75f);
            var backspaceButton = new TextButtonControl(Game);
            backspaceButton.CanIncreaseDecrease = true;
            backspaceButton.Position = position;
            backspaceButton.Text = "Delete";
            backspaceButton.MinimumSize = new Vector2((4 * letterButtonWidth) - 0.1f, letterButtonHeight - 0.1f);
            backspaceButton.DecreaseValue += (sender, args) => MenuManager.SetFocus(_keyControls[0, KeyCols - 1], false);
            backspaceButton.Click += (sender, args) => 
            { 
                if (_textLabel.Text.Length > 0) 
                {
                    _textLabel.Text = _textLabel.Text.Remove(_textLabel.Text.Length - 1); 
                    _charCountLabel.Value = _textLabel.Text.Length; 
                }
            };

            var imageControl2 = new ImageControl(Game);
            imageControl2.Position = backspaceButton.Position;
            imageControl2.Tint = Color.White * 0.35f;
            imageControl2.Size = backspaceButton.MinimumSize;
            imageControl2.TextureInfo = Game.TextureManager.GetTextureInfo("Pixel");

            AddControl(imageControl2, NoTabIndex);
            AddControl(backspaceButton, GetNextTabIndex());
            _controlLink.Add(backspaceButton, imageControl2);

            position.Y = backspaceButton.Position.Y - letterButtonHeight;
            position.X = backspaceButton.Position.X;
            var deleteButton = new TextButtonControl(Game);
            deleteButton.CanIncreaseDecrease = true;
            deleteButton.Position = position;
            deleteButton.Text = "Clear";
            deleteButton.MinimumSize = backspaceButton.MinimumSize;
            deleteButton.DecreaseValue += (sender, args) => MenuManager.SetFocus(_keyControls[1, KeyCols - 1], false);
            deleteButton.Click += (sender, args) => 
            { 
                if (_textLabel.Text.Length > 0) 
                {
                    _charCountLabel.Value = 0; 
                    _textLabel.Text = string.Empty; 
                }
            };

            var imageControl3 = new ImageControl(Game);
            imageControl3.Position = deleteButton.Position;
            imageControl3.Tint = Color.White * 0.35f;
            imageControl3.Size = backspaceButton.MinimumSize;
            imageControl3.TextureInfo = Game.TextureManager.GetTextureInfo("Pixel");

            AddControl(imageControl3, NoTabIndex);
            AddControl(deleteButton, GetNextTabIndex());
            _controlLink.Add(deleteButton, imageControl3);

            position.Y = deleteButton.Position.Y -letterButtonHeight;
            position.X = backspaceButton.Position.X;
            var spaceButton = new TextButtonControl(Game);
            spaceButton.CanIncreaseDecrease = true;
            spaceButton.Position = position;
            spaceButton.Text = "Space";
            spaceButton.MinimumSize = backspaceButton.MinimumSize;
            spaceButton.DecreaseValue += (sender, args) => MenuManager.SetFocus(_keyControls[2, KeyCols - 1], false);
            spaceButton.Click += (sender, args) => 
            { 
                if (_textLabel.Text.Length < MaxLength) 
                {
                    _textLabel.Text += " "; 
                    _charCountLabel.Value = _textLabel.Text.Length; 
                }
            };

            var imageControl4 = new ImageControl(Game);
            imageControl4.Position = spaceButton.Position;
            imageControl4.Tint = Color.White * 0.35f;
            imageControl4.Size = backspaceButton.MinimumSize;
            imageControl4.TextureInfo = Game.TextureManager.GetTextureInfo("Pixel");

            AddControl(imageControl4, NoTabIndex);
            AddControl(spaceButton, GetNextTabIndex());
            _controlLink.Add(spaceButton, imageControl4);

            position.Y = spaceButton.Position.Y -letterButtonHeight;
            position.X = backspaceButton.Position.X;
            var enterButton = new TextButtonControl(Game);
            enterButton.CanIncreaseDecrease = true;
            enterButton.Position = position;
            enterButton.Text = "Accept";
            enterButton.MinimumSize = backspaceButton.MinimumSize;
            enterButton.DecreaseValue += (sender, args) => MenuManager.SetFocus(_keyControls[3, KeyCols - 1], false);
            enterButton.Click += (sender, args) => Close();

            var imageControl5 = new ImageControl(Game);
            imageControl5.Position = enterButton.Position;
            imageControl5.Tint = Color.White * 0.35f;
            imageControl5.Size = backspaceButton.MinimumSize;
            imageControl5.TextureInfo = Game.TextureManager.GetTextureInfo("Pixel");

            AddControl(imageControl5, NoTabIndex);
            AddControl(enterButton, GetNextTabIndex());
            _controlLink.Add(enterButton, imageControl5);

            _keyControls[0, KeyCols - 1].IncreaseValue += (sender, args) => MenuManager.SetFocus(backspaceButton, false);
            _keyControls[1, KeyCols - 1].IncreaseValue += (sender, args) => MenuManager.SetFocus(deleteButton, false);
            _keyControls[2, KeyCols - 1].IncreaseValue += (sender, args) => MenuManager.SetFocus(spaceButton, false);
            _keyControls[3, KeyCols - 1].IncreaseValue += (sender, args) => MenuManager.SetFocus(enterButton, false);

            for (var row = 0; row < KeyRows; row++)
            {
                for (var col = 0; col < KeyCols; col++)
                {
                    var control = _keyControls[row, col];
                    var leftControl = col < 1 ? null : _keyControls[row, col - 1];
                    var rightControl = col >= KeyCols - 1? null : _keyControls[row, col + 1];
                    
                    if (leftControl != null)
                        control.DecreaseValue += (sender, args) => MenuManager.SetFocus(leftControl, false);

                    if (rightControl != null)
                        control.IncreaseValue += (sender, args) => MenuManager.SetFocus(rightControl, false);
                }
            }

             position.Y = anchorPosition.Y + letterButtonHeight;
             position.X = -14;
            _textLabel = new TextLabelControl(Game);
            _textLabel.FontSize = new Vector2(2.0f);
            _textLabel.Position = position;
            _textLabel.Alignment = Alignment.CentreLeft;
            _textLabel.Tint = Theme.LabelCaptionColor;
            AddControl(_textLabel, NoTabIndex);

            _charCountLabel = new IntegerLabelControl(Game);
            _charCountLabel.Position = new Vector2(-13.65f, -5.75f);
            _charCountLabel.Tint = Theme.LabelCaptionColor;
            _charCountLabel.FontSize = Theme.SmallFontSize;
            _charCountLabel.Alignment = Alignment.CentreRight;
            _charCountLabel.Value = 0;
            AddControl(_charCountLabel, NoTabIndex);

            _charOfLabel = new TextLabelControl(Game);
            _charOfLabel.Position = new Vector2(-13.4f, -5.75f);
            _charOfLabel.Tint = Theme.LabelCaptionColor;
            _charOfLabel.FontSize = Theme.SmallFontSize;
            _charOfLabel.Alignment = Alignment.CentreLeft;
            _charOfLabel.Text = "of";
            AddControl(_charOfLabel, NoTabIndex);

            _maxLengthLabel = new IntegerLabelControl(Game);
            _maxLengthLabel.Position = new Vector2(-12.4f, -5.75f);
            _maxLengthLabel.Tint = Theme.LabelCaptionColor;
            _maxLengthLabel.FontSize = Theme.SmallFontSize;
            _maxLengthLabel.Alignment = Alignment.CentreLeft;
            AddControl(_maxLengthLabel, NoTabIndex);

            MaxLength = 15;
        }

        protected override void OnAddedToManager()
        {
            Text = InitialText;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnGotFocus()
        {

        }

        protected override void OnUpdateMustCallBase() 
        {
            base.OnUpdateMustCallBase();

            foreach (var imageControl in _controlLink.Values)
            {
                imageControl.Tint = Color.White * 0.35f;
            }

            if (MenuManager.FocusControl != null && _controlLink.ContainsKey(MenuManager.FocusControl))
            {
                _controlLink[MenuManager.FocusControl].Tint = Color.White * 0.55f;
            }

            //if (_textLabel.Text.Length < MaxLength)
            //{
            //    _textLabel.Text += " ";
            //    _charCountLabel.Text = _textLabel.Text.Length.ToString();
            //}

            //TODO: something here to handle keyboard input
            //foreach (var control in _controlLink)
            //{
            //    control.Key.Visible = Game.InputMethodManager.InputMethod.SupportsKeyboard == false;
            //    control.Value.Visible = control.Key.Visible;

            //    if (control.Value.Visible == false && MenuManager.ActiveControl == control.Value)
            //        MenuManager.SetFocus(CloseButton, false);

            //}

            //var charIndex = _textLabel.Text.Length;
            //if (KeyboardHelper.HandleKeyboardInput(ref _textLabel.Text, ref charIndex, MaxLength))
            //    _charCountLabel.Value = _textLabel.Text.Length; 
        }

        /// <summary>
        /// Called when the 'back' button is pressed on the controller and the menu accepts the back command.
        /// </summary>
        /// <returns>True if the menu should play a sound effect; otherwise false.</returns>
        protected override bool OnBack()
        {
            Text = InitialText;
            return base.OnBack();
        }
    }
}
