﻿using Microsoft.Xna.Framework;
using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Octopus enemy
    /// </summary>
    [ProtoContract]
    public class Octopus : Enemy
    {
        const float SpeedY = 0.0175f;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public Octopus(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-11.face-right.animation")
                    : Game.AnimationManager.Get("enemy-11.face-left.animation"));

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnAddedToWorld(World world)
        {
            Velocity.Y = SpeedY;
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            var player = Game.World.Get<Player>(EntityKeys.Player);
            IsFacingRight = player.Position.X > Position.X;

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-11.face-right.animation")
                    : Game.AnimationManager.Get("enemy-11.face-left.animation"));

            Game.GameMap.HandleCollisions(this, respectPlatforms: false);
        }

        private void Shoot()
        {
            if (IsPlayerInRange == false)
                return;

            CreateBullet(new Vector2(0.1f, 0));
            CreateBullet(new Vector2(-0.1f, 0));
        }

        void CreateBullet(Vector2 velocity)
        { 
            var bullet = Game.EntityFactory.Create(typeof(EnemyBullet), "OctopusBullet");
            if (bullet != null)
            {
                bullet.Position = Position;
                bullet.Velocity = velocity;
                Game.World.Add(bullet);
            }
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            Position += Velocity;

            switch (entityCollisionEdge)
            {
                case CollisionEdge.Top:
                    Velocity.Y = -SpeedY;
                    IsFacingRight = true;
                    Shoot();
                    break;

                case CollisionEdge.Bottom:
                    Velocity.Y = SpeedY;
                    IsFacingRight = false;
                    Shoot();
                    break;
            }
        }
    }
}