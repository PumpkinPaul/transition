namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    class ParticleEmitterFactory
    {
        /// <summary>
        /// Creates a new instance of a ParticleEmitter .
        /// </summary>
        /// <param name="particleEmitterType">One of the Actorlass enum values indicating which actor to create.</param>
        /// <returns>An instance of the request Actor or null.</returns>
        public static IParticleEmitter Create(ParticleEmitterType particleEmitterType)
        {
            IParticleEmitter emitter = null;

            switch (particleEmitterType)
            {
                case ParticleEmitterType.Point:
					emitter = new ParticlePointEmitter();
					break;

                case ParticleEmitterType.Circle:
					emitter = new ParticleCircleEmitter();
					break;

                case ParticleEmitterType.Rectangle:
					emitter = new ParticleRectEmitter();
					break;

                case ParticleEmitterType.Ring:
					emitter = new ParticleRingEmitter();
					break;
            }

            return emitter;
        }
    }
}
