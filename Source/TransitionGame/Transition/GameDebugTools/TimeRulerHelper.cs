﻿using System;
using Microsoft.Xna.Framework;

namespace Transition.GameDebugTools
{
    public struct TimeRulerHelper : IDisposable
    {
        private readonly string _name;

        public static TimeRulerHelper WrapTimeRulerHelper()
        {
            return new TimeRulerHelper();
        }

        public TimeRulerHelper(string name, Color color)
        {
            _name = name;

            Gearset.GS.BeginMark(name, color);
        }

        public void Dispose()
        {
            Gearset.GS.EndMark(_name);
        }
    }
}