using System.IO;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Tool
    {
        public readonly TransitionGame Game;
        protected readonly LevelEditor LevelEditor;
        protected LevelEditorState LevelEditorState { get { return LevelEditor.LevelEditorState; } }
        
        public abstract string CursorName { get; }

        public LevelEditor.SubToolItem SubToolItem { get; protected set; }
        public virtual void SetSubToolItem(LevelEditor.SubToolItem subToolItem) { }

        public string ToolTip { get; protected set; }

        protected Tool(TransitionGame game) 
        {
            Game = game;
            LevelEditor = game.LevelEditor;
        }

        public virtual void Update() { }
        public virtual void Draw() { }

        public void SetCursor()
        {
            TransitionGame.Instance.SetCursor(Path.Combine(@"Content\Cursors\", CursorName));
        }
    }
}
