using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public class Theme
    {
        public static class Fonts
        {
            public const string Default = "fontName";
            public const string Alt = "altFontName";
        }

        public static class FontSizes
        {
            public const string Normal = "fontSize";
            public const string Small = "altFontSize";
        }

        public static class Colors
        {
            public static string Background = "backgroundColor"; 
            public static string Focus = "focusColor"; 
            public static string Hover = "hoverColor"; 
            public static string HelpText = "helpTextColor"; 
        }

        public static Theme Current;

        readonly Dictionary<string, Color> _colors = new Dictionary<string, Color>();
        readonly Dictionary<string, BitmapFont> _fonts = new Dictionary<string, BitmapFont>();
        readonly Dictionary<string, Vector2> _fontSizes = new Dictionary<string, Vector2>();

        /// <summary>The menu's main font for text</summary>
        public static BitmapFont Font; 
        public static string FontName = "League Gothic"; 
        public static BitmapFont AltFont; 
        public static string AltFontName = "Arial"; 
        public static readonly Vector2 FontSize = new Vector2(1.35f);
        public static readonly Vector2 SmallFontSize = new Vector2(0.9f);

        public static readonly Color StandardColor = new Color(255,255,255);
        
        public static Color AltColor1;

        public static Color Base1;
        public static Color Base2;
        public static Color Base3;

        public static Color LabelCaptionColor = new Color(52,73,94) * 1.75f;
        public static Color LabelValueColor = Color.Gray;

        public void AddFont(string name, BitmapFont value)
        {
            _fonts[name] = value;
        }

        public void AddFontSize(string name, Vector2 value)
        {
            _fontSizes[name] = value;
        }

        public void AddColor(string name, Color value)
        {
            _colors[name] = value;
        }

        public BitmapFont GetFont(string name)
        {
            return _fonts[name];
        }

        public Vector2 GetFontSize(string name)
        {
            return _fontSizes[name];
        }

        public Color GetColor(string name)
        {
            return _colors[name];
        }

        public static void Initialise()
        {
            var baseColour = new Color(52,73,94);
            var altColour = new Color(255,50,0);
            
            Base1 = baseColour;   
            Base2 = Base1 * 0.75f;
            Base3 = Base1 * 0.5f;
            Base2.A = 255;
            Base3.A = 255;

            AltColor1 = altColour;
        }

        public static void LoadContent(BaseGame game)
        {
            Current = game.Themes.GetTheme("Default");

            Font = game.FontManager.GetBitmapFont(FontName);
            AltFont = game.FontManager.GetBitmapFont(AltFontName);
        }
    }
 }
