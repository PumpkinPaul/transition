using System;
using System.Globalization;
using Microsoft.Xna.Framework;

namespace Transition
{
    public struct ColorHsl
    {
        public float H, S, L;
    }

    /// <summary>
    /// Helper class for colours
    /// </summary>
    public static class ColorHelper
    {
        /// <summary>
        /// Converts a Color from RGB to HSL
        /// See http://en.wikipedia.org/wiki/HSL_color_space#Conversion_from_RGB_to_HSL_or_HSV for details
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static ColorHsl RgbToHsl(Color color)
        {
            var r = color.R / (double)byte.MaxValue;
            var g = color.G / (double)byte.MaxValue;
            var b = color.B / (double)byte.MaxValue;

            var min = Math.Min(Math.Min(r, g), b);
            var max = Math.Max(Math.Max(r, g), b);
            var delta = max - min;

            var colorHsl = new ColorHsl();
            colorHsl.H = 0.0f;
            colorHsl.S = 0.0f;
            colorHsl.L = (float)((max + min) / 2.0f);

            if (delta != 0)
            {
                if (colorHsl.L < 0.5f)
                {
                    colorHsl.S = (float)(delta / (max + min));
                }
                else
                {
                    colorHsl.S = (float)(delta / (2.0f - max - min));
                }


                if (r == max)
                {
                    colorHsl.H = (float)((g - b) / delta);
                }
                else if (g == max)
                {
                    colorHsl.H = (float)(2f + (b - r) / delta);
                }
                else if (b == max)
                {
                    colorHsl.H = (float)(4f + (r - g) / delta);
                }
            }

            //Convert to degrees
            colorHsl.H = colorHsl.H * 60f;
            if (colorHsl.H < 0 )
                colorHsl.H += 360;

            return colorHsl;
        }

        /// <summary>
        /// Converts a Color from HSL to RGB
        /// See http://en.wikipedia.org/wiki/HSL_color_space#Conversion_from_HSL_to_RGB for details
        /// </summary>
        /// <param name="hsl"></param>
        /// <returns></returns>
        public static Color HslToRgb(ColorHsl hsl)
        {
            if (hsl.S == 0)
                return new Color(hsl.L, hsl.L, hsl.L);

            float temp2;

            if (hsl.L < 0.5f)
                temp2 = hsl.L * (1.0f + hsl.S);
            else
                temp2 = hsl.L + hsl.S - hsl.L * hsl.S;

            var temp1 = 2.0f * hsl.L - temp2;

            var h = hsl.H / 360;

            var rtemp3 = h + 1.0f / 3.0f;
            var gtemp3 = h;
            var btemp3 = h - 1.0f / 3.0f;

            if (rtemp3 < 0)
                rtemp3 += 1.0f;
            if (rtemp3 > 1)
                rtemp3 -= 1.0f;
            if (gtemp3 < 0)
                gtemp3 += 1.0f;
            if (gtemp3 > 1)
                gtemp3 -= 1.0f;
            if (btemp3 < 0)
                btemp3 += 1.0f;
            if (btemp3 > 1)
                btemp3 -= 1.0f;

            float r, g, b;

            if ((6.0f * rtemp3) < 1)
                r = temp1 + (temp2 - temp1) * 6.0f * rtemp3;
            else if (2.0f * rtemp3 < 1)
                r = temp2;
            else if (3.0f * rtemp3 < 2)
                r = temp1 + (temp2 - temp1) * ((2.0f / 3.0f) - rtemp3) * 6.0f;
            else
                r = temp1;

            if ((6.0f * gtemp3) < 1)
                g = temp1 + (temp2 - temp1) * 6.0f * gtemp3;
            else if (2.0f * gtemp3 < 1)
                g = temp2;
            else if (3.0f * gtemp3 < 2)
                g = temp1 + (temp2 - temp1) * ((2.0f / 3.0f) - gtemp3) * 6.0f;
            else
                g = temp1;

            if ((6.0f * btemp3) < 1)
                b = temp1 + (temp2 - temp1) * 6.0f * btemp3;
            else if (2.0f * btemp3 < 1)
                b = temp2;
            else if (3.0f * btemp3 < 2)
                b = temp1 + (temp2 - temp1) * ((2.0f / 3.0f) - btemp3) * 6.0f;
            else
                b = temp1;

            return new Color(r, g, b);

        }

        /// <summary>
        /// Gets the lightness of the given color
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static float GetLightness(Color color)
        {
            return RgbToHsl(color).L;
        }

        public static void SetLightness(ref Color color, float brightness)
        {
            var hsl = RgbToHsl(color);
            hsl.L = brightness;

            color = HslToRgb(hsl);
        }

        public static Color SetLightness(Color color, float brightness)
        {
            var hsl = RgbToHsl(color);
            hsl.L = brightness;

            return HslToRgb(hsl);
        }

        /// <summary>
        /// Creates a <see cref="Color"/> value from an RGBA or RGB hex string.  The string may begin with or without the hash mark (#) character.
        /// </summary>
        /// <param name="hexString">The RGBA hex string to parse.</param>
        /// <returns>A <see cref="Color"/> value as defined by the RGBA or RGB hex string. </returns>
        public static Color FromHtml(string hexString)
        {
            if (hexString.StartsWith("#", StringComparison.Ordinal))
                hexString = hexString.Substring(1);

            var hex = uint.Parse(hexString, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            var color = Color.Black;

            switch (hexString.Length)
            {
                case 8:
                    color.R = (byte)(hex >> 24);
                    color.G = (byte)(hex >> 16);
                    color.B = (byte)(hex >> 8);
                    color.A = (byte)(hex);
                    break;

                case 6:
                    color.R = (byte)(hex >> 16);
                    color.G = (byte)(hex >> 8);
                    color.B = (byte)(hex);
                    break;
            }

            return color;
        }
    }
}
