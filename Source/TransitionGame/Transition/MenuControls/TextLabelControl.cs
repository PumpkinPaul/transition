using System.Xml.Linq;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a label control.
    /// </summary>
    public class TextLabelControl : LabelControl
    {		
        /// <summary>The text to render.</summary>
        public string Text { get; set; }

		/// <summary>
		/// Initialises a new instance of a Label control.
		/// </summary>
		/// <param name="game">A reference to the game.</param>
        public TextLabelControl(BaseGame game) : base(game) { }

        public override void Load(XElement element)
        {
            base.Load(element);

            Text = element.GetAttributeString("text");
        }

        public override Rect Bounds
		{
			get { return Font.GetBounds(Position, FontSize, Text, Alignment); }
		}

        /// <summary>
        /// Allows a derived control to draw itself.
		/// </summary>
		protected override void OnDraw()
		{
            var tint = Tint;
            tint *= Overlay.Opacity;

            Game.Renderer.DrawString(Font, Position, FontSize, Rotation, Text, Alignment, tint, RenderStyle, Layer);
		}
    }
}
