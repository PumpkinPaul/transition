using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    public enum MenuResponse
    {
        Ok, 
        Cancel
    }

    /// <summary>
    /// Represents a base class for menus.
    /// </summary>
    public abstract class Menu : Overlay
    {
        public event EventHandler BeginClosing;
        public event EventHandler AfterClosed;
        
        public const float ValueDelta = 1.0f / 30f; //30fps

        enum Phase
        {
            Showing,
            Normal,
            Hiding,
            Hidden,
            Closing,
            Closed
        }

        /// <summary>The menu's current phase.</summary>
        Phase _phase;

        /// <summary>The manager for the menu.</summary>
        public MenuManager MenuManager;

        /// <summary>The menu's title text.</summary>
        public string Title;

        public MenuResponse Response;
		
		/// <summary>The list of controls that can be 'tabbed' through</summary>
		/// <remarks>
		/// 'Tabbed' here relates to the typical tabbing through controls on a windows form.  We will use an appropriate
		/// key or button as our tab key.
		/// </remarks>
		readonly List<MenuControl> _tabStopControls = new List<MenuControl>();

        protected ImageControl CloseButton;
        protected int CloseButtonTabIndex = 100000;

        public virtual string BackButtonText { get { return "Back"; } }

        /// <summary>
        /// Gets whether the menu allows itself to be closed with the back button.
        /// </summary>
        public virtual bool AllowCloseWhenBackPressed
        {
            get { return true; }
        }

        /// <summary>
        /// Gets whether the menu is closing.
        /// </summary>
        public bool IsClosing { get; private set; }

        /// <summary>
        /// Gets whether the menu has closed.
        /// </summary>
        public bool IsClosed { get; private set; }

        /// <summary>
        /// Gets whether the menu accepts user input.
        /// </summary>
        public bool AcceptsInput { get; private set; }

        public MenuControl CachedFocusControl { get; private set; }

        /// <summary>
        /// Gets whether the menu has the focus.
        /// </summary>
        public bool IsBeingShown
        {
            get { return _phase == Phase.Showing; }
        }

        /// <summary>
        /// Gets the default control (the one to display 
        /// </summary>
        public MenuControl DefaultControl
        {
            get
            {
                foreach (var control in _tabStopControls)
                {
                    if (control.Visible)
                        return control;
                }

                return null;
            }
        }
		
        /// <summary>
        /// Initialises a new instance of a Menu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
        protected Menu(BaseGame game, string title) : base(game)
		{
			Title = title;
            _phase = Phase.Showing;
		}

        public void RemoveControl(MenuControl control)
		{
            Controls.Remove(control);
            _tabStopControls.Remove(control);	
        }

        protected override void OnInitialise() 
        { 
            _tabStopControls.Clear();

            CloseButton = new ImageControl(Game)
            {
                TextureInfo = Game.TextureManager.GetTextureInfo("BackButton"),
                Size = new Vector2(2.0f, 0.75f),
                Position = new Vector2(-18.5f, 10.5f),
                Tint = Color.White
            };
            CloseButton.Click += (sender, args) => BackPressed();

            AddControl(CloseButton, CloseButtonTabIndex);
        }

        /// <summary>
        /// Allows a derived overlay to handle controls being added.
        /// </summary>
        /// <param name="menuControl">The menu control added.</param>
        /// <param name="order">The control's tab order.</param>
        protected override void OnControlAdded(MenuControl menuControl, int order)
        {
            if (order > NoTabIndex)
            {
                menuControl.TabIndex = order;
                _tabStopControls.Add(menuControl);
                _tabStopControls.Sort((control1, control2) => Comparer<int>.Default.Compare(control1.TabIndex, control2.TabIndex));
            }
        }

        /// <summary>
        /// Allows a derived overlay to update itself.
        /// </summary>
        protected override void OnUpdateMustCallBase()
        {
            base.OnUpdateMustCallBase();

            switch (_phase)
            {
                case Phase.Showing:
                    UpdatePhaseShowing();
                    break;

                case Phase.Normal:
                    break;

                case Phase.Hiding:
                    UpdatePhaseHiding();
                    break;

                case Phase.Hidden:
                    break;

                case Phase.Closing:
                    UpdatePhaseClosing();
                    break;

                case Phase.Closed:
                    break;
            }

            Opacity = Value;

            HandleCloseButtonVisibility();
        }

        private void UpdatePhaseShowing()
        {
            Value += ValueDelta;
            if (Value > 1.0f)
            {
                Value = 1.0f;
                AcceptsInput = true;
                _phase = Phase.Normal;
            }
        }

        private void UpdatePhaseHiding()
        {
            if (FadeOut())
                _phase = Phase.Hidden;
        }

        private void UpdatePhaseClosing()
        {
            AcceptsInput = false;
            if (FadeOut())
            {
                _phase = Phase.Closed;
                IsClosing = false;
                IsClosed = true;
                CachedFocusControl = null;
                OnClosed();
                OnAfterClosed();
            }
        }

        private bool FadeOut()
        {
            Value -= ValueDelta;
            if (Value >= 0.0f)
                return false;

            Value = 0.0f;

            return true;
        }

        public void HandleCloseButtonVisibility()
        {
            //Remove or add the menu close button based on the primary navigation method.
            if (Game.InputMethodManager.InputMethod.SupportsMouse)
            {
                if (Controls.Contains(CloseButton) == false)
                    AddControl(CloseButton, CloseButtonTabIndex);
            }
            else 
            {
                if (Controls.Contains(CloseButton))
                {
                    if (MenuManager.FocusControl == CloseButton)
                        MenuManager.FocusControl = DefaultControl;

                    RemoveControl(CloseButton);
                }
            }
        
            CloseButton.Visible = CloseButton.Visible && AllowCloseWhenBackPressed;
        }

        /// <summary>
        /// Occurs when a menu is added to the menu manager.
        /// </summary>
        public void AddedToManager()
        {
            IsClosed = false;
            IsClosing = false;
            AcceptsInput = false;

            Value = 0.0f;
		    Opacity = 0.0f;
            
            HandleCloseButtonVisibility();

            Response = MenuResponse.Ok; 

            OnAddedToManager();
            GotFocus();
        }

        public void RemovedFromManager()
        {
            OnRemovedFromManager();
        }

        /// <summary>
        /// Gets the next control that can be 'tabbed' to.
        /// </summary>
        public MenuControl GetNextFocusControl(MenuControl control)
        {
            return GetControlForFocus(control, 1);
        }

        /// <summary>
        /// Gets the previous control that can be 'tabbed' to.
        /// </summary>
        public MenuControl GetPreviousFocusControl(MenuControl control)
        {
            return GetControlForFocus(control, -1);
        }

        /// <summary>
        /// Gets the next control that can be 'tabbed' to.
        /// </summary>
        private MenuControl GetControlForFocus(MenuControl activeControl, int step)
        {
            var count = _tabStopControls.Count;

            if (count == 0)
                return activeControl;

            var control = activeControl;
            var index = _tabStopControls.IndexOf(activeControl);

            while (count > 0)
            {
                index = (index + step).Mod(_tabStopControls.Count);

                if (_tabStopControls[index].Visible)
                {
                    control = _tabStopControls[index];
                    break;
                }

                count--;
            }

            return control;
        }

        /// <summary>
        /// Occurs when the 'back' button is pressed on the controller.
        /// </summary>
        public void BackPressed()
        {
            if (AllowCloseWhenBackPressed == false)
                return;

            Response = MenuResponse.Cancel;

            if (OnBack())
                Game.PlaySound("MenuItemBack");
        }

        /// <summary>
		/// .
		/// </summary>
		public MenuControl GetTabStopHoverControl(Vector2 position)
		{
			//Reverse order so that controls on top get clicking priority.
			for (var i = _tabStopControls.Count - 1; i >=0; --i)
			{
				var control = _tabStopControls[i];

                if (control.Visible == false)
                    continue;
				
				if (control.Bounds.Contains(position))					
                    return control;
			}

            return null;
		}

        /// <summary>
		/// .
		/// </summary>
		public MenuControl GetHoverControl(Vector2 position)
		{
			//Reverse order so that controls on top get clicking priority.
			for (var i = Controls.Count - 1; i >=0; --i)
			{
				var control = Controls[i];

                if (control.Visible == false)
                    continue;
				
				if (control.Bounds.Contains(position))					
                    return control;
			}

            return null;
		}
				
		/// <summary>
		/// .
		/// </summary>
		public void GotFocus()
		{
			_phase = Phase.Showing;

            StartTransition("gotFocus");
            
            OnGotFocus();
		}

		/// <summary>
		/// .
		/// </summary>
        public void LostFocus()
		{
            AcceptsInput = false;
			_phase = Phase.Hiding;
            CachedFocusControl = MenuManager.FocusControl;

            StartTransition("lostFocus");
		}
		
		/// <summary>
		/// Closes the menu.
		/// </summary>
        /// <remarks>When the menu is closed it is removed from the menu manager.</remarks>
		public void Close()
		{
            MenuManager.MenuCloseRequested(this);

			IsClosing = true;
			_phase = Phase.Closing;

            StartTransition("close");
		}
			
		/// <summary>
        /// Occurs when a menu is added to the MenuManager.
        /// </summary>
        protected virtual void OnAddedToManager() { }

        /// <summary>
        /// Occurs when a menu is removed from the MenuManager.
        /// </summary>
        protected virtual void OnRemovedFromManager() { }
				
        /// <summary>
        /// Called when the 'back' button is pressed on the controller and the menu accepts the back command.
        /// </summary>
        /// <returns>True if the menu should play a sound effect; otherwise false.</returns>
        protected virtual bool OnBack()
        {
            Close();
            return true;
        }
		
		/// <summary>
		/// Called when the menu has finished its closing state.
		/// </summary>
		protected virtual void OnClosed() { }
				
		/// <summary>
        /// .
        /// </summary>
        protected virtual void OnGotFocus() { }

        protected virtual void OnBeginClosing()
        {
            var handler = BeginClosing;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        protected virtual void OnAfterClosed()
        {
            var handler = AfterClosed;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
