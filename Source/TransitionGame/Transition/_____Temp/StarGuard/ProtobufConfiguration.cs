using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Xna.Framework;
using ProtoBuf.Meta;
using Transition.Animations;
using Transition._____Temp.StarGuard.Entities;
using Transition._____Temp.StarGuard.EntityBlueprints;

namespace Transition._____Temp.StarGuard
{
    /// <summary>
    /// Star Guard implementation
    /// </summary>
    public class ProtobufConfiguration 
    {
        class Node
        {
            public int Level;
            public Type Type;
            public readonly Dictionary<Type, Node> Nodes = new Dictionary<Type, Node>();
        }

        static RuntimeTypeModel _runtimeTypeModel;

        ProtobufConfiguration() { }

        public static RuntimeTypeModel InitialiseProtobuf()
        {
            if (_runtimeTypeModel != null)
                return _runtimeTypeModel;

            _runtimeTypeModel = TypeModel.Create();
            _runtimeTypeModel.UseImplicitZeroDefaults = false;

            var types = Assembly.GetExecutingAssembly().GetTypes();
            foreach (var type in types)
            {
                //Register all of the pooled entities.
                if (typeof(Entity).IsAssignableFrom(type))
                {
                    System.Diagnostics.Debug.WriteLine("Type: " + type.Name + " is a pooled object");
                    _runtimeTypeModel[type].SetFactory(typeof(EntityFactory).GetMethod("GetEntity", new[] { typeof(Type) }));
                }

                //Register all of the 'Data' items that need surrogates.
                if (typeof(EntityBlueprint).IsAssignableFrom(type))
                {
                    System.Diagnostics.Debug.WriteLine("Type: " + type.Name + " is serialized as a EntityBlueprintSurrogate");
                    _runtimeTypeModel.Add(type, false).SetSurrogate(typeof(EntityBlueprintSurrogate));
                }
            }

            //Custom surrogates.
            _runtimeTypeModel.Add(typeof(KeyValuePair<string, Entity>), false).SetSurrogate(typeof(KeyRefPair<string, Entity>));
            _runtimeTypeModel.Add(typeof(Sprite), false).SetSurrogate(typeof(SerializedSprite));
            _runtimeTypeModel.Add(typeof(EntityBlueprint), false).SetSurrogate(typeof(EntityBlueprintSurrogate));
            _runtimeTypeModel.Add(typeof(EnemyBlueprint), false).SetSurrogate(typeof(EnemyBlueprintSurrogate));
            _runtimeTypeModel.Add(typeof(EnemyBulletBlueprint), false).SetSurrogate(typeof(EnemyBulletBlueprintSurrogate));
            _runtimeTypeModel.Add(typeof(ShipBlueprint), false).SetSurrogate(typeof(ShipBlueprintSurrogate));
            
            //Framework classes that can't be serialized by default.
            var vector2Type = _runtimeTypeModel.Add(typeof(Vector2), true);
            vector2Type.Add(1, "X");
            vector2Type.Add(2, "Y");

            //Notify the serializer of all the sub classes of game object
            var rootNode = new Node { Level = 0, Type = typeof(Entity) };
            foreach (var type in types)
            {
                if (!type.IsSubclassOf(typeof(Entity)))
                    continue;

                //Get all the sub types
                var baseTypes = new List<Type>();
                baseTypes.Insert(0, type);
                var t = type.BaseType;
                while (t != null && t != typeof(object))
                {
                    baseTypes.Insert(0, t);
                    t = t.BaseType;
                }

                var parentLevel = -1;
                Type parentType = null;
                foreach (var bt in baseTypes)
                {
                    PopulateNode(parentLevel, parentType, rootNode, bt);
                    parentLevel++;
                    parentType = bt;
                }
            }

            const int fieldNumber = 500;
            PopulateSerializationHeirarchy(_runtimeTypeModel, typeof(Entity), rootNode.Nodes, fieldNumber);

            _runtimeTypeModel.CompileInPlace();

            return _runtimeTypeModel;
        }

        static void PopulateNode(int parentLevel, Type parentType, Node node, Type type)
        {
            if (node.Level == parentLevel)
            {
                if (node.Nodes.ContainsKey(type) == false && node.Type == parentType)
                {
                    node.Nodes.Add(type, new Node { Level = parentLevel + 1, Type = type });
                }
                return;
            }

            foreach (var n in node.Nodes.Values)
                PopulateNode(parentLevel, parentType, n, type);
        }

        static void PopulateSerializationHeirarchy(RuntimeTypeModel runtimeTypeMopde, Type baseType, Dictionary<Type, Node> nodes, int fieldNumber)
        {
            foreach (var node in nodes.Values)
            {
                //var subType = metaType.AddSubType(fieldNumber++, node.Type);
                runtimeTypeMopde[baseType].AddSubType(fieldNumber++, node.Type);
                System.Diagnostics.Debug.WriteLine("Type: " + node.Type + " is serialized as a " + baseType + " sub type");
                PopulateSerializationHeirarchy(runtimeTypeMopde, node.Type, node.Nodes, fieldNumber);
            }
        }
    }
}