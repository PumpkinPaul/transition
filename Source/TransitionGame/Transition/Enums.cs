using System.ComponentModel;

namespace Transition
{
    /// <summary>
    /// The 'class' of actor
    /// </summary>
    /// <remarks>Allows for broad categorisation of things.</remarks>
    public enum ActorClassification
    {
        Alien = 0,
        EnemyBullet,
        Bullet,
        SpecialEffect,
        Label,
        ParticleSystem,
        Ship,
        Animated,
        Weapon,
    }

    /// <summary>
    /// .
    /// </summary>
    public enum Alignment
    {
        TopLeft = 0,
        TopCentre,
        TopRight,
        CentreLeft,
        Centre,
        CentreRight,
        BottomLeft,
        BottomCentre,
        BottomRight
    }	

    public enum FlowStyle
    {
        Horizontal = 0,
        Vertical,
    }

	/// <summary>
    /// The type of behaviour to perform when an actor leaves the play area
    /// </summary>
	public enum ConstrainBehaviour
	{
        None = 0,
		Clamp,
		Bounce,
		Deactivate
	}

    public enum DialogAction
    {
        Select = 0,
        Back
    }

    /// <summary>
    /// .
    /// </summary>
    public enum Difficulty
    {
        Easy = 0,
        Normal,
        Hard
    }
		
	/// <summary>
    /// .
    /// </summary>
    public enum Ownership
    {
        None = 0,
        AutoDeactivate,
        FlyTheNest
    }

	/// <summary>
    /// .
    /// </summary>
	public enum ParticleEmitterType
    {
        Point = 0,
        Circle,
        Rectangle,
		Ring
    }

    public enum SignedOutGamer
    {
        Primary = 0,
        NonPrimary
    }

    /// <summary>
    /// .
    /// </summary>
    public enum ParticleSystemType
    {
        Expired,
        Explosion,
        ResIn,
        ResOut,
        Spawn,
        Trail,
        CityRescued,
        CityFreed
    }

    public enum UnockFullVersionState
    {
        Disconnected = 0,
        ConnectedSignedOut,
        ConnectedSignedInCanPurchase,
        ConnectedSignedInCantPurchase
    }

    /// <summary>
    /// .
    /// </summary>
    public enum RageType
    {
        None = 0,

        [Description("Laser")]
        Laser,
        
        [Description("Homin Missiles")]
        HomingMissiles,

        [Description("Heat Seekers")]
        SeekerMissiles,

        [Description("Beserker")]
        Beserk,

        [Description("Plasma Beam")]
        PlasmaWeapon
    }

    public enum ReplayPlaybackMode
    {
        Off, 
        OnDemand,
        Background
    }
}
