using Nuclex.Input;
using Transition.Menus;

namespace Transition.Input
{
    /// <summary>
    /// Represents game input from a gamepad
    /// </summary>
    public class GamepadInputMethod : InputMethod
    {
        public override bool SupportsMouse { get { return false; } }
        public override bool SupportsKeyboard { get { return false; } }
        public override bool SupportsGamepad { get { return true; } }

        public override bool SupportsVibration { get { return true; } }

        public override bool CanChangeInputMethodIndex { get; set; }

        public override bool IsBeingUsed() 
        {
            for(var i = 0; i < GamePadHelper.MaxGamePads; i++)
            {
                var playerIndex = (ExtendedPlayerIndex)i;//TransitionGame.Instance.ActivePlayer.PlayerIndex;
                if (GamePadHelper.IsGamePadConnected(playerIndex) == false)
                    continue;

                var faceButtonPressed = GamePadHelper.IsAPressed(playerIndex) || GamePadHelper.IsBPressed(playerIndex) || GamePadHelper.IsXPressed(playerIndex) || GamePadHelper.IsYPressed(playerIndex);
                var directionPressed = GamePadHelper.IsDownPressed(playerIndex) || GamePadHelper.IsUpPressed(playerIndex) || GamePadHelper.IsLeftPressed(playerIndex) || GamePadHelper.IsRightPressed(playerIndex);
                var specialButtonPressed = GamePadHelper.IsStartPressed(playerIndex) || GamePadHelper.IsBackJustPressed(playerIndex);

                //I'll disable these checks as a face button could perform a button action (e.g. select Credits Menu) but there would be no focus / highlight colour yet
                //faceButtonPressed = false;
                //specialButtonPressed = false;

                if (faceButtonPressed || directionPressed || specialButtonPressed)
                {
                    PlayerIndex = playerIndex;
                    return true;
                }
            }

            return false;
        }

        public override GameInput GatherGameInput(ExtendedPlayerIndex playerIndex, PlayerProfile playerProfile)
        {
            var gameInput = new GameInput();

            var leftStick = GamePadHelper.GetLeftStick(playerIndex);
                        
            const float movementXThreshold = 0.05f;
            const float movementYThreshold = 0.15f;

            //Digital movement
            //if (GamePadHelper.IsDPadUpPressed(playerIndex))
            //    gameInput.Jump = true;
            //if (GamePadHelper.IsDPadDownPressed(playerIndex))
            //    gameInput.Rewind = true;
                        
            if (GamePadHelper.IsDPadLeftPressed(playerIndex))
                gameInput.Left = true;
            else if (GamePadHelper.IsDPadRightPressed(playerIndex))
                gameInput.Right = true;

            //Analogue movement (overrides digital movement)
            //if (leftStick.Y > movementYThreshold)
            //    gameInput.Up = leftStick.Y;
            //else if (leftStick.Y < -movementYThreshold)
            //    gameInput.Down = leftStick.Y;

            if (GamePadHelper.IsRightPressed(playerIndex))
                gameInput.Right = true;// leftStick.X;
            else if (GamePadHelper.IsLeftPressed(playerIndex))
                gameInput.Left = true;//leftStick.X;

            if (GamePadHelper.IsRightJustPressed(playerIndex))
                gameInput.RightJustPressed = true;// leftStick.X;
            else if (GamePadHelper.IsLeftJustPressed(playerIndex))
                gameInput.LeftJustPressed = true;//leftStick.X;

            //if (GamePadHelper.IsRightTriggerPressed(playerIndex))
            //    gameInput.Fire = true;

            //if (GamePadHelper.IsAPressed(playerIndex))
            //    gameInput.Jump = true;

            //if (GamePadHelper.IsAJustPressed(playerIndex))
            //    gameInput.JumpJustPressed = true;

            if (GamePadHelper.IsXPressed(playerIndex))
                gameInput.Fire = true;

            if (GamePadHelper.IsAPressed(playerIndex))
                gameInput.Jump = true;

            if (GamePadHelper.IsAJustPressed(playerIndex))
                gameInput.JumpJustPressed = true;

            if (GamePadHelper.IsRightTriggerPressed(playerIndex))
                gameInput.Fire = true;

            if (GamePadHelper.IsRightTriggerJustPressed(playerIndex))
                gameInput.FireJustPressed = true;

            //if (GamePadHelper.IsUpPressed(playerIndex))
            //    gameInput.Jump = true;

            //if (GamePadHelper.IsUpJustPressed(playerIndex))
            //    gameInput.JumpJustPressed = true;

            if (GamePadHelper.IsRightShoulderJustPressed(playerIndex))
                gameInput.AltFire = true;

            if (GamePadHelper.IsLeftShoulderPressed(playerIndex))
                gameInput.Rewind = true;

            if (GamePadHelper.IsLeftShoulderJustPressed(playerIndex))
                gameInput.RewindJustPressed = true;

            //Utilities...
            if (GamePadHelper.IsStartJustPressed(playerIndex))
                gameInput.Pause = true;

            return gameInput;
        }

        public override MenuInput GatherMenuInput(MenuManager menuManager, ExtendedPlayerIndex playerIndex, PlayerProfile playerProfile)
        {
            var menuInput = new MenuInput();
  
			menuInput.BackJustPressed = GamePadHelper.IsBJustPressed(playerIndex);
			menuInput.SelectPressed = GamePadHelper.IsAPressed(playerIndex);
			menuInput.SelectJustPressed = GamePadHelper.IsAJustPressed(playerIndex);
			menuInput.PreviousControlPressed = GamePadHelper.IsUpJustPressed(playerIndex);
			menuInput.NextControlPressed = GamePadHelper.IsDownJustPressed(playerIndex);
			menuInput.IncreaseValuePressed = GamePadHelper.IsRightJustPressed(playerIndex);
			menuInput.DecreaseValuePressed = GamePadHelper.IsLeftJustPressed(playerIndex);

			menuInput.StartPressed = GamePadHelper.IsStartPressed(playerIndex);
			menuInput.StartJustPressed = GamePadHelper.IsStartJustPressed(playerIndex);

			menuInput.XJustPressed = GamePadHelper.IsXJustPressed(playerIndex);
			menuInput.YJustPressed = GamePadHelper.IsYJustPressed(playerIndex);

            //Gamepad held
            menuInput.PreviousControlPressed = HandleHeldAction(ref menuInput, GamePadHelper.IsUpPressed(playerIndex), menuInput.PreviousControlPressed, ref PreviousControlHeldTicks);
            menuInput.NextControlPressed = HandleHeldAction(ref menuInput, GamePadHelper.IsDownPressed(playerIndex),  menuInput.NextControlPressed, ref NextControlHeldTicks);
            menuInput.DecreaseValuePressed = HandleHeldAction(ref menuInput, GamePadHelper.IsLeftPressed(playerIndex), menuInput.DecreaseValuePressed, ref DecreaseValueHeldTicks);
            menuInput.IncreaseValuePressed = HandleHeldAction(ref menuInput, GamePadHelper.IsRightPressed(playerIndex), menuInput.IncreaseValuePressed, ref IncreaseValueHeldTicks);

            return menuInput;
        }
    }
}
