using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Transition.ActorDatas;
using Transition.PostProcess;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public class Ship : Actor
    {
        static private readonly IShipState SpawnState = new ShipSpawnState();
        static private readonly IShipState SpawningState = new ShipSpawningState();
        static private readonly IShipState NormalState = new ShipNormalState();
        static private readonly IShipState CrashingState = new ShipCrashingState();
		
        /// <summary>A reference to the current state machine state.</summary>
        private IShipState _state;
		
		/// <summary>Configuration data for the ship.</summary>
        private ShipData _shipData;

        /// <summary>The player who owns this ship.</summary>
        private Player _player;

        /// <summary>The ship's weapon.</summary>
        private Weapon _weapon;

        private bool _invulnerable;
        private int _invulnerableTicks;
        public const int MaxInvulnerableTicks = 240;
        
        bool _isFlipping;
        bool _isRolling;
        bool _isRolled;

        private readonly List<ParticleSystem> _exaustEffects = new List<ParticleSystem>(4);

        int _deadTimer;

        bool _canFire;

        /// <summary>
        /// Creates a new instance of an Ship
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public Ship(TransitionGame game) : base(game)
        {
            _horizontalConstrainBehaviour = ConstrainBehaviour.Bounce;
			_verticalConstrainBehaviour = ConstrainBehaviour.Bounce;
        }

        /// <summary>
        /// Gets the type of actor.
        /// </summary>
        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.Ship; }
        }

        /// <summary>
        /// Gets whether this type of actor collides.
        /// </summary>
        public override int CollisionMask
        {
            get { return 1 << (int)ActorClassification; }
        }

        public Weapon Weapon { get { return _weapon; } }

        /// <summary>
        /// The player who owns this ship.
        /// </summary>
        public Player Player
        {
            get { return _player; }
            set { _player = value; }
        }

        /// <summary>
        /// The player who owns this ship.
        /// </summary>
        public ShipData ShipData
        {
            get { return _shipData; }
        }

        protected override void OnInitialise(string name)
        {
            base.OnInitialise(name);

            _shipData = _game.DataManager.ShipData(name);

            _state = Ship.SpawnState;
            _player = null;
            _invulnerableTicks = Ship.MaxInvulnerableTicks;

            _isFlipping = false;
            _isRolling = false;
            _isRolled = false;

            _canFire = true;
        }

        public void MakeInvulnerableWithEffect(int invulnerableTicks)
        {
            //MakeInvulnerable(invulnerableTicks);

            ////Create the ships's invulnerability effect
            //var invulnerableEffect = (ShipInvulnerabilityEffect)_game.ActorFactory.Create(typeof(ShipInvulnerabilityEffect));
            //invulnerableEffect.Position = Position;
            //invulnerableEffect.Scale = _scale;
            //invulnerableEffect.Owner = this;
            //invulnerableEffect.Ownership = Ownership.AutoDeactivate;
            //invulnerableEffect.LifeTicks = invulnerableTicks;
            //invulnerableEffect.Tint = Tint;
            //invulnerableEffect.Layer = Layer + 1;

            //_scene.AddActor(invulnerableEffect);
        }

        public void MakeInvulnerable(int invulnerableTicks)
        {
            _invulnerableTicks = invulnerableTicks; 
            _invulnerable = true;
        }

		/// <summary>
        /// Called when the actor is added to the scene.
        /// </summary>
        protected override void OnAddedToScene() 
	    {
            Position = Vector2.Zero;
		    Velocity = Vector2.Zero;
            _canFire = true;
            
		    SetDefaultWeapon();

            MakeInvulnerableWithEffect(Ship.MaxInvulnerableTicks);

            CreateTrailParticleSystems(ActorData.TrailEffects);
	    }

        /// <summary>
        /// Called when the actor's weapon expires
        /// </summary>
        protected override void OnExpireWeapon(Weapon weapon)
        {
            if (weapon == _weapon)
                SetDefaultWeapon();
        }

        /// <summary>
        /// Allows an object to update itself.
        /// </summary>
        protected override void OnUpdate(GameTime gameTime)
        {
            ApplyInput(); //May move this to state machine
            HandleInvulnerability();

            _state.OnUpdate(this);
        }

        /// <summary>
        /// Overridden to kill any cities being carried.
        /// </summary>
        protected override void OnDeactivate()
        {
            //Don't immediately kill the ship - have it explode over time.
            _state = Ship.CrashingState;
            _deadTimer = 60;
            Visible = false;
            _active = true;
            _collidable = false;

            ThrustEffect(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collider"></param>
        /// <param name="time"></param>
        protected override void OnCollision(Actor collider, float time)
	    {
		    switch (collider.ActorClassification) 
            {
			    case ActorClassification.Alien:
                    CollisionWithNastie(collider, time);
                    break;

			    case ActorClassification.EnemyBullet:
                    if (collider is Mine && _isFlipping)
                        return;

                    CollisionWithNastie(collider, time);
				    break;
		    }
	    }

        public void CollideMap(float t)
        {
            CollisionWithNastie(null, t);
        }
		
        /// <summary>
        /// Ship has collided with an object that hurts it.
        /// </summary>
        /// <param name="collider">The actor colliding with the ship - may be null if bash into wall</param>
        /// <param name="time">The time the collision occurred.</param>
        private void CollisionWithNastie(Actor collider, float time)
        {
            if (!_invulnerable)
            {
                RegisterHit(collider, 1);
                if (_health == 0)
                {
                    Destroy();
                }
                else
                {
                    //Give the ship a small amount of invulnerability?
                    _invulnerable = true;
                    _invulnerableTicks = 60;
                    _hitTicks = 16;
                    _game.CameraManager.ShakeCamera(0.4f, 45);
                    _game.ScreenFlashManager.AddFlash(Tint, 20);
                    _game.VibrationManager.AddLeftVibration(_game.GetPlayer(_playerId).PlayerIndex, 0.35f, 45);

                    DistortionEffects.SetColorEffect(DistortionEffects.GetReusableColorEffect(30, 60, 0, 16));
                    //Scene.CreateGotYouLabel(Position);
                }
            }

            if (collider != null)
                collider.RegisterHit(this, 1);
        }

        private void Destroy()
        {
            _invulnerable = true;
            _game.CameraManager.ShakeCamera(0.5f, 15);
            _game.ScreenFlashManager.AddFlash(Tint, 10);
            _game.VibrationManager.AddLeftVibration(_game.GetPlayer(_playerId).PlayerIndex, 0.75f, 90);

            DistortionEffects.SetColorEffect(DistortionEffects.GetReusableColorEffect(120, 180, 0, 32));
            DistortionEffects.SetHorizontalNoiseEffect(DistortionEffects.GetReusableHorizontalNoiseEffect(120, 180, 1.0f));
            Scene.CreateGotYouLabel(Position);
        }

        protected override void OnConstrained(bool constrainedTop, bool constrainedLeft, bool constrainedBottom, bool constrainedRight) 
        { 
            if (constrainedLeft)
                FlipLeftToRight();
            else if (constrainedRight)
                FlipRightToLeft();
        }

        private void FlipRightToLeft()
        {
            _isFlipping = true;
            Sprites[0].SetAppearance(_game.AnimationManager.Get("manta.flip.rtol.animation"));
        }

        private void FlipLeftToRight()
        {
            _isFlipping = true;
            Sprites[0].SetAppearance(_game.AnimationManager.Get("manta.flip.ltor.animation"));  
        }

        /// <summary>
        /// Applies player inout to the ship.
        /// </summary>
        protected void ApplyInput()
        {
            var velocityX = Velocity.X;
            var velocityY = Velocity.Y;

            ThrustEffect(false);

            //if (_state == NormalState)
            //{
            //    //Left and right
            //    if (Player.GameInput.Left < 0.0f)
            //    {
            //        if (_isFlipping == false || Velocity.X < 0) 
            //            velocityX += (Player.GameInput.Left * _shipData.AccelerationX);

            //        ThrustEffect(true);
            //    }
            //    else if (Player.GameInput.Right > 0.0f)
            //    {
            //        if (_isFlipping == false || Velocity.X > 0) 
            //            velocityX += (Player.GameInput.Right * _shipData.AccelerationX);

            //        ThrustEffect(true);
            //    }
            //    else
            //    {
            //        //No movement action so allow friction to slow the ship
            //        velocityX *= _shipData.FrictionX;

            //        ThrustEffect(false);
            //    }
            //}

            //if (_state == NormalState)
            //{
            //    //Up and down
            //    if (Player.GameInput.Up > 0.0f)
            //        velocityY += (Player.GameInput.Up * _shipData.AccelerationY);
            //    else if (Player.GameInput.Down < 0.0f)
            //        velocityY += (Player.GameInput.Down * _shipData.AccelerationY);
            //    {
            //        //No movement action so allow (universeLocation) friction to slow the ship
            //        velocityY *= _shipData.FrictionY;
            //    }

            //    if (Player.GameInput.Roll && _isFlipping == false && _isRolling == false)
            //    {
            //        BarrelRoll();   
            //    }
            //}

            //if (_state == NormalState || _state == CrashingState)
            //{
            //    //Ensure the ship doesn't exceed its maximum velocity
            //    var maxVelocityX = _shipData.MaxVelocityX ;

            //    if (_player.GameInput.Right > 0.0f && velocityX > 0.0f)
            //        maxVelocityX *= _player.GameInput.Right;
            //    else if (_player.GameInput.Left < 0.0f && velocityX < 0.0f)
            //        maxVelocityX *= -_player.GameInput.Left;

            //    //Friction!
            //    if (velocityX < 0.0f && velocityX < -maxVelocityX)
            //        velocityX *= _shipData.FrictionX; 
            //    else if (velocityX > 0.0f && velocityX > maxVelocityX)
            //        velocityX *= _shipData.FrictionX; 
            //}

            if (velocityY < 0.0f && velocityY < -_shipData.MaxVelocityY)
                velocityY = -_shipData.MaxVelocityY;
            else if (velocityY > 0.0f && velocityY > _shipData.MaxVelocityY)
                velocityY = _shipData.MaxVelocityY;

            if (_isFlipping == false && ((Velocity.X > 0.0f && velocityX < 0.0f) || (Velocity.X < 0.0f && velocityX > 0.0f)))
            {
                if (Velocity.X > 0.0f)
                    FlipRightToLeft();  
                else if (Velocity.X < 0.0f)
                    FlipLeftToRight();  
            }

            if (_isFlipping)
            {
                _isRolling = false;
                _isRolled = false;

                //An event id of 1 indicates the flip has finished.
                if (Sprites[0].Event == 1)
                {
                    _isFlipping = false;
                    Sprites[0].Event = 0;
                }
            }
            else if (_isRolling)
            {
                //An event id of 1 indicates the roll has finished.
                if (Sprites[0].Event == 1)
                {
                    _isRolled = !_isRolled;
                    _isRolling = false;
                    Sprites[0].Event = 0;
                }
            }

            if (_isRolled)
                _collisionRadius = _actorData.CollisionRadius * 0.4f;
            else
                _collisionRadius = _actorData.CollisionRadius;

            Velocity = new Vector2(velocityX, velocityY);

            //Face the correct direction based on pad input
            if (Velocity.X < 0.0f)
                _direction = (float)Math.PI;
            else if (Velocity.X > 0.0f)
                _direction = 0.0f;
            
            if (_canFire)
            {
                if (Player.GameInput.Fire)
                    FireWeapon(_weapon);

                if (Player.GameInput.AltFire)
                    Player.AttemptNuke();
            }
        }

        private void BarrelRoll()
        {
            if (_isRolling)
                return;

            if (_isFlipping)
                return;

            _isRolling = true;
            if (Velocity.X > 0.0f)
            {
                if (_isRolled)
                    Sprites[0].SetAppearance(_game.AnimationManager.Get("manta.roll.exit.right.animation"));  
                else
                    Sprites[0].SetAppearance(_game.AnimationManager.Get("manta.roll.enter.right.animation"));  
            }
            else
            {
                if (_isRolled)
                    Sprites[0].SetAppearance(_game.AnimationManager.Get("manta.roll.exit.left.animation"));  
                else
                    Sprites[0].SetAppearance(_game.AnimationManager.Get("manta.roll.enter.left.animation"));  
            }
        }

        public void FireWeapon(Weapon weapon)
        {
            if (weapon == null)
                return;
            
            weapon.Direction = Direction;
            weapon.Rotation = weapon.Direction;
            weapon.Fire();
        }

        public void FireNuke()
        {   
            //SmartBomb for now!
            CreateParticleSystems(ShipData.NukeEffects, Ownership.FlyTheNest);
            
            _game.CameraManager.ShakeCamera(0.99f, 60);
            _game.ScreenFlashManager.AddFlash(Tint, 5);
            _game.VibrationManager.AddLeftVibration(Player.PlayerIndex, 0.85f, 30);
            _scene.SmartBomb();
            var rippleEffect = (RippleEffect)_game.ActorFactory.Create(typeof(RippleEffect));
            if (rippleEffect != null)
            {
                rippleEffect.Position = Position;
                rippleEffect.Setup(_game.TextureManager.GetTextureId("Ripple4"), 90, 1.5f);
                Scene.AddActor(rippleEffect);
            }
        }

        /// <summary>
        /// Creates a list of particle system
        /// </summary>
        public void CreateTrailParticleSystems(List<ParticleEffect> effects)
        {
            _exaustEffects.Clear();

            foreach (var particleEffect in effects)
            {
                var particleSystem = CreateParticleSystem(particleEffect, Ownership.AutoDeactivate);
                if (particleSystem == null)
                    continue;

                _exaustEffects.Add(particleSystem);
            }
        }

        private void ThrustEffect(bool enable)
        {
            if (enable)
            {
                foreach (var particleSystem in _exaustEffects)
                    particleSystem.Start();
            }
            else
            {
                foreach (var particleSystem in _exaustEffects)
                    particleSystem.Stop();
            }
        }
    
        /// <summary>
        /// Sets the ship's default weapon.
        /// </summary>
	    private void SetDefaultWeapon() 
	    {
		    SetWeapon(ref _weapon, _shipData.WeaponName, Actor.EternalLife);
	    }

        /// <summary>
        /// Sets the active weapon
        /// </summary>
        /// <param name="weapon"></param>
        /// <param name="name"></param>
        /// <param name="lifeTicks"></param>
        private void SetWeapon(ref Weapon weapon, string name, int lifeTicks)
        {
            //Clean up any existing weapons
            if (weapon != null)
                weapon.Deactivate();

            //Time to set the new weapon
            weapon = (StandardWeapon)_game.ActorFactory.Create(typeof(StandardWeapon), name);
            
			weapon.PlayerId = _playerId;
            weapon.LifeTicks = lifeTicks;
            weapon.Owner = this;
            weapon.Ownership = Ownership.AutoDeactivate;
            weapon.Position = Position;
            weapon.Direction = _direction;
            weapon.Tint = Tint;
            _scene.AddActor(weapon);
        }

        /// <summary>
        /// Ship is invulnerable for a short period of time when spawning.
        /// </summary>
        private void HandleInvulnerability()
        {
            if (_invulnerable == false)
                return;
            
            _invulnerableTicks--;
            if (_invulnerableTicks <= 0)
                _invulnerable = false;
        }

        /// <summary>
        /// Base class for the Ship's state machine.
        /// </summary>
        public interface IShipState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="ship">The ship to update.</param>
            void OnUpdate(Ship ship);
        }

        /// <summary>
        /// Spawn behaviour for a Ship.
        /// </summary>
        public class ShipSpawnState : IShipState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="ship">The ship to update.</param>
            public void OnUpdate(Ship ship)
            {
                SpawnSystem.DefaultSpawn(ship);
                ship._state = Ship.SpawningState;
            }
        }

        /// <summary>
        /// Spawning behaviour for a Ship.
        /// </summary>
        public class ShipSpawningState : IShipState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="ship">The ship to update.</param>
            public void OnUpdate(Ship ship)
            {
                //Process the spawning state
                if (SpawnSystem.HandleSpawning(ship))
                {
                    //Finished spawing so advance the state machine
                    ship._state = Ship.NormalState;
                }	
            }
        }

        /// <summary>
        /// Normal behaviour for a Ship.
        /// </summary>
        public class ShipNormalState : IShipState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="ship">The ship to update.</param>
            public void OnUpdate(Ship ship)
            {
                //ship.ApplyInput();
            }
        }

        /// <summary>
        /// Crashing behaviour for a Ship.
        /// </summary>
        public class ShipCrashingState : IShipState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="ship">The ship to update.</param>
            public void OnUpdate(Ship ship)
            {
                ship.Velocity = Vector2.Zero;

                ship.Velocity.X *= 0.965f;
                ship.Velocity.Y *= 0.965f;

                if (ship.Velocity.X < 0)
                    ship._rotation = 0;
                else if (ship.Velocity.X > 0)
                    ship._rotation = (float)Math.PI;
                    

                ship._deadTimer--;

                //Delegate to state machine?
                //if (ship._deadTimer == 10 || ship._deadTimer == 30 || ship._deadTimer == 60 || ship._deadTimer == 100)
                //{
                //    ship.Explode();
                //    ship._game.CameraManager.ShakeCamera(1.0f, 10);
                //    ship._game.ScreenFlashManager.AddFlash(ship.Tint, 10);
                //    ship._game.VibrationManager.AddLeftVibration(ship._game.GetPlayer(ship._playerId).PlayerIndex, 0.5f, 45);
                //}

                if (ship._deadTimer <= 0)
                {
                    //ship._game.CameraManager.ShakeCamera(1.0f, 60);
                    //ship._game.ScreenFlashManager.AddFlash(ship.Tint, 30);
                    //ship._game.VibrationManager.AddLeftVibration(ship._game.GetPlayer(ship._playerId).PlayerIndex, 0.75f, 90);

                    //Paladin stuff - all health depleted
                    ship._active = false;
                }
            }
        }

        //Shop
        public void AddHealth()
        {
            if (_health < Player.MaxHealth)
                _health++;
        }
    }
}
