using System;
using System.IO;
using Microsoft.Xna.Framework.Storage;
using Transition.Input;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public class DemoRecorder
    {
        private const string StorageFolder = "Demos";
        private const string DemoSearchPattern = "*.*";

        /// <summary>
        /// .
        /// </summary>
        enum DemoRecorderState
        {
            Stopped = 0,
            Playing,
            Recording,
            Paused
        }

        readonly BaseGame _game;

		/// <summary></summary>
		private DemoRecorderState _state = DemoRecorderState.Stopped;
		
        public bool IsPaused => _state == DemoRecorderState.Paused;
        public bool IsPlaying => _state == DemoRecorderState.Playing;
        public bool IsRecording => _state == DemoRecorderState.Recording;
        public bool IsStopped => _state == DemoRecorderState.Stopped;
        public bool Finished => true;

        public DemoRecorder(BaseGame game)
        {
            if (game == null)
                throw new ArgumentNullException(nameof(game));

            _game = game;
        }

        /// <summary>
        /// Loads content
        /// </summary>
        public void LoadContent()
        {
			var folder = Path.Combine(Platform.ResourcesPath, "Demos");
            Load(Path.Combine (folder, "demo.bin"));
        }

        /// <summary>
        /// Loads a demo.
        /// </summary>
        public bool Load(string filename)
        {
            using (var fileStream = File.OpenRead(filename))
                return Load(fileStream);
        }

		/// <summary>
        /// Loads a demo.
        /// </summary>
		public bool Load(Stream fileStream)
		{
            return true;
		}

        public void LoadFromStorage(StorageContainer storageContainer)
        {
            if (string.IsNullOrEmpty(StorageFolder) == false && storageContainer.DirectoryExists(StorageFolder) == false)
            {
                storageContainer.CreateDirectory(StorageFolder);
                return;
            }

            var filenames = storageContainer.GetFileNames(Path.Combine(StorageFolder, DemoSearchPattern));

            if (filenames == null)
                return;

            Array.Sort(filenames);

            foreach (var filename in filenames)
            {
                using (var stream = storageContainer.OpenFile(filename, FileMode.Open, FileAccess.Read))
                    Load(stream);

                break; //Just do first demo for now
            }
        }

		/// <summary>
        /// Saves a demo.
        /// </summary>
		public bool Save(string filename)
		{
			return true;
		}
		
        /// <summary>
        /// 
        /// </summary>
		public void Play()
		{
			_state = DemoRecorderState.Playing;
        }

        /// <summary>
        /// 
        /// </summary>
		public void Resume()
		{
			_state = DemoRecorderState.Playing;
		}

        /// <summary>
        /// 
        /// </summary>
		public void Pause()
		{
			_state = DemoRecorderState.Paused;
		}

		/// <summary>
        /// 
        /// </summary>
		public void Stop()
		{
			_state = DemoRecorderState.Stopped;
		}
		
		/// <summary>
        /// 
        /// </summary>
		public void Record()
		{
			_state = DemoRecorderState.Recording;
		}
								
        public float Progress => 0.0f;
    }
}
