namespace Transition.Util
{
    public class Math2
    {
        public static double SquareWave(float value, uint steps = 4, float amplitude = 1.0f)
        {
            var neg = value < 0.0;
            var absValue = neg ? -value : value;

            var f = (1.0f / steps);

            var stepId = (int)(absValue / f) % steps;
            var zeroOrOne = stepId % 2;
            var retValue = zeroOrOne > 0.0f ? amplitude : -amplitude;

            return !neg ? retValue : -retValue;
        }
    }
}
