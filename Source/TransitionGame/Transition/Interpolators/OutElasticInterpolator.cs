using System;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;

namespace Transition
{
    [Serializable]
    public sealed class OutElasticInterpolator : InterpolatorBase, ISerializable
    {
	    public static readonly OutElasticInterpolator Instance = new OutElasticInterpolator();

	    /**
	     * OutElasticInterpolator can only be accessed as a singleton.
	     */
	    public OutElasticInterpolator() : base() { }
		 
	    /**
	     * Sine interpolation.
	     */
	    //@Override
	    public override float Interpolate(float a, float b, float ratio) 
        {
		    if (a == b) 
			    return a;
		    
		    if (ratio < 0.0f) 
			    ratio = 0.0f;
		    else if (ratio > 1.0f) 
			    ratio = 1.0f;
		    

            double s = 1 / 4;

            return (float)(b * Math.Pow(2, -10 * ratio) * Math.Sin((ratio - s) * (2 * MathHelper.TwoPi)) + b + a);
	    }

        public float EaseIn(float min, float max, float tweenState)
        {
            //if (tweenState.IsAlmostNull())
            //    return min;
            //if (tweenState.IsAlmost(1))
            //    return max;

			//var distance = b - a;
			//return b + (float)(distance * Math.Pow(2, -10 * ratio) * Math.Sin(4.666f * MathHelper.Pi * ratio - MathHelper.PiOver2));

            const float span = 2.75f;
			const float weight = 7.5625f;

			var distance = max - min;

			if (tweenState < 1.0f / span)
				return min + distance*tweenState*tweenState*weight;

			if(tweenState < 2.0f / span)
			{
				tweenState -= 1.5f / span;
				return min + distance*(tweenState*tweenState*weight + 0.75f);
			}

			if(tweenState < 2.5f / span)
			{
				tweenState -= 2.25f / span;
				return min + distance*(tweenState*tweenState*weight + 0.9375f);
			}

			tweenState -= 2.625f / span;
			return min + distance*(tweenState*tweenState*weight + 0.984375f);
        }


	    public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.SetType(typeof(SingletonSerializationHelper));
        }

        [Serializable]
        private sealed class SingletonSerializationHelper : IObjectReference 
        {
            //GetRealObject is called after this object is deserialized.
            public Object GetRealObject(StreamingContext context) 
            {
                // When deserialiing this object, return a reference to the Singleton object instead.
                return Instance;
            }
        }
    }
}
