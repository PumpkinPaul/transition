using Transition.ActorDatas;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Alien : Actor
    {
        /// <summary>Configuration data for the alien.</summary>
        public EnemyData AlienData;

        public override bool IsNukeable { get { return true; } }

        /// <summary>
        /// Creates a new instance of an Alien
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        protected Alien(TransitionGame game) : base(game) { }

        /// <summary>
        /// Gets the type of actor
        /// </summary>
        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.Alien; }
        }

        /// <summary>
        /// Gets whether this type of actor collides
        /// </summary>
        public override int CollisionMask
        {
            get { return 1 << (int)ActorClassification; }
        }

        /// <summary>
        /// Gets or sets the homing bullet targetting us.
        /// </summary>
	    public Bullet Bullet { get; set; }

        /// <summary>
        /// Gets or sets whether the alien is targetted (by homing missiles, etc).
        /// </summary>
	    public bool IsTargetted 
	    {
		    get { return Bullet != null; }
	    }

        protected override void OnInitialise(string name)
        {
            base.OnInitialise(name);

            AlienData = _game.DataManager.EnemyData(name);

            Bullet = null;

            var level = _game.LevelManager.LevelNumber;
            var healthMod = ((int)((level - 1) / 10)) * 0.25f;

            _health += (int)(_health * healthMod);
        }

        public virtual void HandleBulletHit(Bullet bullet)
        {
            //Aliens can do what they want here - currently used for shield handling in Landers and Bodyguards - go see those classes!
        }

        public void Kill()
        {
            Player player = null;

            //If the player killed the alien set the chaining details
            if (_playerId != -1)
            {
                player = _game.GetPlayer(_playerId);
            }

            var scoreBonus = AlienData.ScoreBonus;
            if (scoreBonus <= 0) 
                return;

            var scoreToAdd = scoreBonus;

            //If the player didn't kill the alien then we need to work out which player should get the poits for the death
            //(maybe the player outlived a certain type of alien and got points that way)
            if (player == null)
            {
                if (_game.PlayerCount == 1)
                    player = _game.ActivePlayer;
            }

            if (player != null)
                player.AddScore(scoreToAdd);

            _scene.CreateScoreBonusLabel(Position, scoreToAdd);
        }

        /// <summary>
        /// Occurs when an actor is no longer to be updated.
        /// </summary>
        protected override void OnDeactivate()
	    {
            if (Bullet != null) 
            {
		        Bullet.TargetKilled();
		        Bullet = null;
	        }
	    }
    }
}
