﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Microsoft.Xna.Framework;
using Transition.ActorDatas;
using Transition.Animations;

namespace Transition.Test 
{
    public class ObjectMangler
    {
        readonly System.Random _random = new System.Random();

        readonly HashSet<Type> _ignoreTypes;
        readonly Dictionary<Type, Func<object>> _comparisions = new Dictionary<Type, Func<object>>();

        public ObjectMangler(HashSet<Type> ignoreTypes, Dictionary<Type, Func<object>> manglers)
        {
            _ignoreTypes = ignoreTypes ?? new HashSet<Type>();

            _comparisions.Add(typeof(bool), () => _random.NextDouble() > 0.5f);
            _comparisions.Add(typeof(int), () => _random.Next(int.MinValue, int.MaxValue));
            _comparisions.Add(typeof(float), () => (float)_random.NextDouble());
            _comparisions.Add(typeof(Vector2), () => RandomVector2());
            _comparisions.Add(typeof(Box2), () => new Box2(RandomVector2(), RandomVector2()));
            _comparisions.Add(typeof(string), () => _random.Next(int.MinValue, int.MaxValue).ToString());

            if (manglers != null)
            {
                foreach (var item in manglers)
                    _comparisions[item.Key] = item.Value;
            }
        }

        Vector2 RandomVector2()
        {
            return new Vector2((float)_random.NextDouble(), (float)_random.NextDouble());
        }

        public void Mangle<T>(T source)
        {
            System.Diagnostics.Debug.WriteLine("ObjectMangler.Mangle(object source)");

            if (source == null) {
                System.Diagnostics.Debug.WriteLine("Source is null, unable to mangle");
                return;
            }

            var sourceType = source.GetType();

            System.Diagnostics.Debug.WriteLine("About to mangle a: " + sourceType.Name);

            const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

            var fields = sourceType.GetFields(bindingFlags);
            foreach(var fieldInfo in fields)
            {
                var key = fieldInfo.FieldType;

                if (_ignoreTypes.Contains(key))
                {
                    System.Diagnostics.Debug.WriteLine("ObjectMangler ignored a field: " + fieldInfo.FieldType.Name + " " + fieldInfo.Name);
                    continue;
                }

                if (_comparisions.ContainsKey(key) == false)
                    throw new InvalidOperationException("No mangler defined for: " + key.Name);
                    
                var func = _comparisions[key];
                var value = func();
                fieldInfo.SetValue(source, value);
                System.Diagnostics.Debug.WriteLine("ObjectMangler mangled a field: " + fieldInfo.FieldType.Name + " " + fieldInfo.Name + " = " + value);
            }
        }
    }
}