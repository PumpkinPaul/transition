{
  "InputKeys": {
    "Up": 38,
    "Down": 40,
    "Left": 37,
    "Right": 39,
    "Fire": 88,
    "Pause": 80,
    "UpKeyHint": "Up",
    "DownKeyHint": "Down",
    "LeftKeyHint": "Left",
    "RightKeyHint": "Right",
    "SelectKeyHint": "X",
    "FireKeyHint": "X",
    "PauseKeyHint": "P"
  },
  "Checkpoints": [],
  "FileName": "10EE1D3A-FE90-4cd1-87E3-5E52B36809BA",
  "Name": "Player 2",
  "Guid": "10EE1D3A-FE90-4cd1-87E3-5E52B36809BA",
  "ProfileType": 0,
  "SelectedShipName": "Ship1",
  "CompletedAchievements": [],
  "RequiresSave": false
}