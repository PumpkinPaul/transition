using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Rendering;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public class BlastEffect : EnemyBullet
    {
        const int Step = 8;
		static readonly ushort[] StripIndices;

        readonly Geometry<VertexPositionColorTexture> _geometry = new Geometry<VertexPositionColorTexture>();

		int _duration;
	
		float _finalRadius;
	
		float _width;
	
		float _radius;
	
		int _tick;
	
		TextureInfo _textureInfo;

		int _fadeDuration;

		bool _fading;
	
		bool _fadeWhenExpanding;
	
		public int Alpha = 255;

        static BlastEffect()
        {
            //This builds the indices for a TrinagleStrip - we're not using them though now
            //StripIndices = new ushort[(720 / Step)];
            //for (var i = 0; i < StripIndices.Length; i ++) 
            //    StripIndices[i] = (ushort) i;
			
            //StripIndices[StripIndices.Length - 2] = 0;
            //StripIndices[StripIndices.Length - 1] = 1;

            //90 verts = 45 tris

            StripIndices = new ushort[6 + ((720 / Step) -2) * 3]; //* 6 for verts per quad + 6 for last quad to join end to start
            int vertexPointer = 0;
            for (var i = 0; i < StripIndices.Length - 6; i += 6) 
            {
                //The Index Buffer
                //Triangle 1
                StripIndices[i] = (ushort)(vertexPointer);
                StripIndices[i + 1] = (ushort)(vertexPointer + 1);
                StripIndices[i + 2] = (ushort)(vertexPointer + 2);

                //Triangles 2
                StripIndices[i + 3] = (ushort)(vertexPointer + 2);
                StripIndices[i + 4] = (ushort)(vertexPointer + 1);
                StripIndices[i + 5] = (ushort)(vertexPointer + 3);

                vertexPointer += 2;
            }
			
            //Join the end of the blast to the start!
            StripIndices[StripIndices.Length - 6] = (ushort)(vertexPointer);
            StripIndices[StripIndices.Length - 5] = (ushort)(vertexPointer + 1);
            StripIndices[StripIndices.Length - 4] = 0;
            StripIndices[StripIndices.Length - 3] = 0;
            StripIndices[StripIndices.Length - 2] = (ushort)(vertexPointer + 1);
            StripIndices[StripIndices.Length - 1] = 1;
        }

        /// <summary>
        /// Creates a new instance of a BlastEffect
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public BlastEffect(TransitionGame game) : base(game) 
        {
            _geometry.RenderStyle = TransparentRenderStyle.Instance;
            _geometry.Indices = StripIndices;

            _horizontalConstrainBehaviour = ConstrainBehaviour.None;
            _verticalConstrainBehaviour = ConstrainBehaviour.None;
        }

        protected override void OnInitialise(string name) 
        {
            base.OnInitialise(name);

            _geometry.Vertices.Clear();

            //Set in Setup
		    //private int duration;
		    //private int fadeDuration;
		    //private float finalRadius;
		    //private float width;
		    //private float x, y;
		    //private Texture2D texture;

		    _radius = 0;
	        _tick = 0;
	        _fading = false;
	        _fadeWhenExpanding = false;
	        Alpha = 255;

            _textureInfo = _game.TextureManager.GetTextureInfo("BlastEffect2px");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="duration"></param>
        /// <param name="fadeDuration"></param>
        /// <param name="radius"></param>
        /// <param name="width"></param>
        /// <param name="textureInfo"></param>
        public void Setup(Vector2 position, int duration, int fadeDuration, float radius, float width, TextureInfo textureInfo) 
        {
			Position = position;
			_duration = duration;
			_fadeDuration = fadeDuration;
			_finalRadius = radius;
			_width = width;

		    _textureInfo = textureInfo ?? _textureInfo;
            _collidable = true;
		}

        protected override void OnUpdate(GameTime gameTime) 
        {
            _tick++;
			if (!_fading) 
            {
				_radius = SineInterpolator.Instance.Interpolate(0.0f, _finalRadius, (float)_tick / _duration);
				if (_tick >= _duration && _fadeWhenExpanding == false) 
                {
					_tick = 0;
					_fading = true;
				}
            }
			else 
		        _radius = _finalRadius;

            Scale = ActorData.BaseSize * _radius;

            if (IsBlastActive == false)
                Deactivate();
        }

        protected override void  OnDraw(bool isOnscreen)
        {
            var xx = Position.X;
            var yy = Position.Y;

            //Do offset!
            if (Offset != Vector2.Zero)
            {
                xx += Offset.X;
                yy += Offset.Y;
            }

            float innerRadius;

            // Draw rings
			float fadeAlpha;
			if (_fading) 
            {
				fadeAlpha = LinearInterpolator.Instance.Interpolate(1.0f, 0.0f, (float) _tick / _fadeDuration);
				innerRadius = SineInterpolator.Instance.Interpolate(Math.Max(0.0f, _finalRadius - _width), _finalRadius, (float) _tick / _fadeDuration);
			} 
            else 
            {
				if (_fadeWhenExpanding) 
					fadeAlpha = LinearInterpolator.Instance.Interpolate(1.0f, 0.0f, (float) _tick / _fadeDuration);
				else 
					fadeAlpha = 1.0f;
				
				innerRadius = SineInterpolator.Instance.Interpolate(0.0f, Math.Max(0.0f, _finalRadius - _width), (float) _tick / _duration);
			}

            _geometry.Vertices.Clear();
            for (var i = 0; i < 360; i += Step) 
            {
                var c1 = Tint;
                c1.A  = (byte)(fadeAlpha * Alpha);// / 255.0f;

                _geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(xx + ((float)Math.Cos(MathHelper.ToRadians(i))) * _radius, yy + ((float)Math.Sin(MathHelper.ToRadians(i))) * _radius, 0.0f), c1, new Vector2(_textureInfo.SourceRect.Left, _textureInfo.SourceRect.Top)));
                
                //c1.A = 0;
                _geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(xx + ((float)Math.Cos(MathHelper.ToRadians(i))) * innerRadius, yy + ((float)Math.Sin(MathHelper.ToRadians(i))) * innerRadius, 0.0f), c1, new Vector2(_textureInfo.SourceRect.Left, _textureInfo.SourceRect.Bottom)));
            }
            
            _geometry.TextureId = _textureInfo.TextureId;
            _geometry.Layer = Layer;
            _game.Renderer.DrawGeometry(_geometry);
        }

        public float Ratio
        {
            get 
            {
			    if (_fading || _fadeWhenExpanding) 
				    return (float) _tick / _fadeDuration;
			     
				return (float) _tick / _duration;
            }
		}
	
        public bool IsBlastActive 
        { 
            get { return _fadeWhenExpanding ? _tick < _duration : (!_fading || _tick < _fadeDuration); }
        }
	
		public float Radius { get { return _radius; } }

		public bool IsFading { get { return _fading; } }
		
		public bool FadeWhenExpanding { set { _fadeWhenExpanding = value; } }
    }
}
