﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Rendering;

namespace Transition._____Temp.StarGuard
{
    public class Lava
    {
        const int SegmentsPerTile = 1;

        //We only count the tips of the waves - we will render the bottoms but no need to keep a track of them!
        int _waveTipCount;

        //Spring constant for forces applied by adjacent points
        const float SpringConstant = 0.0005f;

        //Sprint constant for force applied to baseline
        const float SpringConstantBaseline = 0.0005f;

        //Damping to apply to speed changes
        const float Damping = 0.97f;

        //Number of iterations of point-influences-point to do on wave per step
        //(this makes the waves animate faster)
        const int Iterations = 3;

        class WaterPoint
        {
            public float X;
            public float Y;
            public float BaseLine;
            public float SpeedY;
            public float Mass;
        }

        WaterPoint[] _waterPoints;

        //A phase difference to apply to each sine
        float _offset;

        const int NumBackgroundWaves = 3;
        const float BackgroundWaveMaxHeight = 0.125f;//0.125f;//0.075f;
        const float BackgroundWaveCompression = 1.25f;
        
        //Amounts by which a particular sine is offset
        readonly float[] _sineOffsets = new float[NumBackgroundWaves];
        
        //Amounts by which a particular sine is amplified
        readonly float[] _sineAmplitudes = new float[NumBackgroundWaves];
        
        //Amounts by which a particular sine is stretched
        readonly float[] _sineStretches = new float[NumBackgroundWaves];
        
        //Amounts by which a particular sine's offset is multiplied
        readonly float[] _offsetStretches = new float[NumBackgroundWaves];

        readonly Geometry<VertexPositionColorTexture> _geometry;
        readonly TextureInfo _textureInfo;

        int _lastLavaX;
        int _lastLavaY;
        readonly List<Box2> _lavaBounds = new List<Box2>();

        public Lava()
        {
            _textureInfo = TransitionGame.Instance.TextureManager.GetTextureInfo("Pixel");
            _geometry = new Geometry<VertexPositionColorTexture>
            {
                TextureId = _textureInfo.TextureId,
                RenderStyle = TransparentRenderStyle.Instance
            };
        }

        public void AddTile(int x, int y)
        { 
            var newTile = new Box2(x, y, GameMap.TileSize, GameMap.TileSize * 1.1f);

            //Is it a brand new chunk of lava or can we expand the last one
            if (_lastLavaX == x - 1 && _lastLavaY == y)
            {
                var previousLavaTile = _lavaBounds.Last();
                previousLavaTile.Union(newTile);

                _lavaBounds[_lavaBounds.Count - 1] = previousLavaTile;

            }
            else
                _lavaBounds.Add(newTile);

            _lastLavaX = x;
            _lastLavaY = y;
        }

        public void Initialise()
        {
            if (_lavaBounds.Count == 0)
                return;

            CreateIndexData();

            InitialiseWavePoints();
            InitialiseBackgroundWaves();
        }

        void CreateIndexData()
        {
            var indices = new List<ushort>();

            var vertex = 0;
            foreach (var section in _lavaBounds)
            {
                var boxWidth = (int) Math.Round(section.Width);

                //We only count the tips here for points! - (top and bottoms comes later when actually creating vertices)
                var sections = (SegmentsPerTile * boxWidth);

                //Add the tip to close off section.
                _waveTipCount += sections + 1;

                //We will create the indices now
                //here is an example of 2 sections of lava { 1 tiles a gap and 5 tiles }

                //tips...
                //1---2---3     4---5---6---7---8---9---10--11--12--13--14

                //vertices
                //1---3---5     7---9---11--13--15--17--19--21--23--25--27
                //|\  |\  |     |\  |\  |\  |\  |\  |\  |\  |\  |\  |\  |
                //| 1 | 1 |     | 2 | 2 | 3 | 3 | 4 | 4 | 5 | 5 | 6 | 6 |   <--these are the relative 'ids' of the tiles I guess
                //|  \|  \|     |  \|  \|  \|  \|  \|  \|  \|  \|  \|  \|
                //0---2---4     6---8---10--12--14--16--18--20--22--24--26

                //12 sections
                //24 triangles

                //Populate the array with references to indices in the vertex buffer
                var triangles = sections * 2;
                for (var i = 0; i < triangles; i++)
                {
                    if (i % 2 == 0) //even, so anticlockwise
                    {
                        indices.Add((ushort)vertex);
                        indices.Add((ushort)(vertex + 1));
                        indices.Add((ushort)(vertex + 2));
                    }
                    else //odd, therefore clockwise
                    {
                        indices.Add((ushort)(vertex + 2));
                        indices.Add((ushort)(vertex + 1));
                        indices.Add((ushort)(vertex + 3));

                        vertex += 2;
                    }
                }
                vertex += 2;
            }

            _geometry.Indices = indices.ToArray();
        }

        /// <summary>
        /// Make points to go on the wave
        /// </summary>
        private void InitialiseWavePoints()
        {
            _waterPoints = new WaterPoint[_waveTipCount];

            var idx = 0;
            foreach (var section in _lavaBounds)
            {
                var boxWidth = (int)Math.Round(section.Width);
                var segments = (SegmentsPerTile * boxWidth);
                

                for (var i = 0; i <= segments; i++)
                {
                    //This represents a point on the wave
                    var newPoint = new WaterPoint
                    {
                        X = MathHelper.Lerp(section.Left, section.Right, (float)i / segments),
                        Y = GameMap.TileSize,
                        BaseLine = section.Bottom,
                        SpeedY = 0,
                        Mass = 1.0f
                    };

                    _waterPoints[idx++] = newPoint;
                }
            }
        }

        private void InitialiseBackgroundWaves()
        {
            //Set each sine's values to a reasonable random value
            for (var i = 0; i < NumBackgroundWaves; i++)
            {
                _sineOffsets[i] = 0.00099999991f * (-1 + 2 * RandomHelper.FastRandom.NextFloat());
                _sineAmplitudes[i] = RandomHelper.FastRandom.NextFloat() * BackgroundWaveMaxHeight;
                _sineStretches[i] = RandomHelper.FastRandom.NextFloat() * BackgroundWaveCompression;
                _offsetStretches[i] = RandomHelper.FastRandom.NextFloat() * BackgroundWaveCompression;
            }
        }

        /// <summary>
        /// This function sums together the sines generated above, given an input value x
        /// </summary>
        /// <param name="x">.</param>
        private float OverlapSines(float x)
        {
            var result = 0.0f;
            for (var i = 0; i < NumBackgroundWaves; i++)
                result += _sineOffsets[i] + _sineAmplitudes[i] * (float)Math.Sin(x * _sineStretches[i] + (0.125f * _offset) * _offsetStretches[i]);

            return result * 0.7f;
        }

        public bool Contains(Box2 entityBounds)
        {
            foreach (var bounds in _lavaBounds)
            {
                if (bounds.Overlaps(entityBounds))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Update the positions of each wave point
        /// </summary>
        public void Update()
        {
            if (_lavaBounds.Count == 0)
                return;

            _offset += 0.55f;

            for (var i = 0; i < Iterations; i++)
            {
                for (var n = 0; n < _waveTipCount; n++)
                {
                    var p = _waterPoints[n];
                    //force to apply to this point
                    var force = 0.0f;

                    //forces caused by the point immediately to the left or the right
                    float forceFromLeft;
                    float forceFromRight;

                    if (n == 0)
                    {
                        //wrap to left-to-right
                        forceFromLeft = SpringConstant * _waterPoints[_waveTipCount - 1].Y - p.Y;
                    }
                    else
                    {
                        //normally
                        forceFromLeft = SpringConstant * _waterPoints[n - 1].Y - p.Y;
                    }

                    if (n == _waveTipCount - 1)
                    {
                        //wrap to right-to-left
                        forceFromRight = SpringConstant * _waterPoints[0].Y - p.Y;
                    }
                    else
                    {
                        //normally
                        forceFromRight = SpringConstant * _waterPoints[n + 1].Y - p.Y;
                    }

                    //Also apply force toward the baseline
                    var forceToBaseline = SpringConstantBaseline * p.BaseLine - p.Y;

                    //Sum up forces
                    force += forceFromLeft;
                    force += forceFromRight;
                    force += forceToBaseline;

                    //Calculate acceleration
                    var acceleration = force / p.Mass;

                    //Apply acceleration (with damping)
                    p.SpeedY = Damping * p.SpeedY + acceleration;

                    //Apply speed
                    p.Y += p.SpeedY;
                }
            }

        }

        public void Draw()
        {
            if (_lavaBounds.Count == 0)
                return;

            //var color = new Color(28, 35, 26);
            var color = new Color(208, 137, 76);

            _geometry.Vertices.Clear();
            for (var i = 0; i < _waveTipCount; i++)
            {
                var p = _waterPoints[i];
                var y = 1.0f + p.BaseLine + p.Y + OverlapSines(p.X);

                _geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(p.X, p.BaseLine, 0.0f), color, new Vector2(_textureInfo.SourceRect.Left, _textureInfo.SourceRect.Bottom)));
                _geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(p.X, y, 0.0f), color, new Vector2(_textureInfo.SourceRect.Left, _textureInfo.SourceRect.Top)));
            }

            TransitionGame.Instance.Renderer.Begin(Renderer.SortStrategy.None);
            TransitionGame.Instance.Renderer.DrawGeometry(_geometry);
            TransitionGame.Instance.Renderer.End();
        }
    }
}