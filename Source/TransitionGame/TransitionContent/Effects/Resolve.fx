#include "CommonPuppy.inc"

sampler backbuffer : register(s0);
sampler distortion : register(s1);
sampler rowNoise : register(s2);
sampler scanlines : register(s3);
sampler vblank : register(s4);
			
float2 distStrength; 

float2 crtRed;
float2 crtGreen;
float2 crtBlue;
float crtVSync;
float vblankPos;
float vblankHeight;

struct VS_IN
{
	float4 position: POSITION;
    float2 texCoord: TEXCOORD0;
	float4 color: COLOR;
};

struct VS_OUT 
{
	float4 position: POSITION;
	float2 texCoord: TEXCOORD0;
	float4 color: COLOR;
	float pos: TEXCOORD1;
	
};

/*
	Needed to add a vertex shader to return the gl_FragCoord required by the pixel shader

	float4 gl_FragCoord : TEXCOORD1;		
	e.g. 
	Before transform (i.e. input position of vertex) : 0 <- x -> 800   &&   0 <- y -> 600 (screen coordinates)
	After transform (e.g. WVP transform)			 : -1 <- x -> 1   &&   -1 <- y -> 1
*/
void resolve_glvert(inout float4 gl_Position : POSITION0, inout float2 gl_TexCoord : TEXCOORD0, out float2 gl_FragCoord : TEXCOORD1)
{
	gl_FragCoord = gl_Position;  //require screen coords (0..w, 0..h}
	gl_Position = ftransform2(gl_Position);
	gl_TexCoord = gl_TexCoord;
}

/* 
	Intermediary resolve fragment shader. Does vsync offset, rgb offset, and row offset too. 
*/
float4 resolve_glfrag(float2 gl_TexCoord : TEXCOORD0, float2 gl_FragCoord : TEXCOORD1) : COLOR0
{
	float2 texCoords = gl_TexCoord;
	texCoords += float2(tex1D(rowNoise, texCoords.y).r - 0.5, crtVSync);

	float2 offset = tex2D(distortion, texCoords).rg;
	offset *= distStrength; 

	float scanlineDim = tex1D(scanlines, gl_FragCoord.y * 0.25).a; //use .x for vertical 'scanlines' and .y for traditional horizontal scanlines
	//CONVERSION - flipped to get correct vblank direction
	//float vblankDim = tex1D(vblank, (vblankPos - gl_FragCoord.y) / vblankHeight).r;
	float vblankDim = tex1D(vblank, (gl_FragCoord.y - vblankPos) / vblankHeight).r;
	
	return float4(
		tex2D(backbuffer, texCoords + offset + crtRed).r,
		tex2D(backbuffer, texCoords + offset + crtGreen).g, 
		tex2D(backbuffer, texCoords + offset + crtBlue).b,
	1.0) * scanlineDim * vblankDim;
}

technique Technique1
{
    pass Pass1
    {
		VertexShader = compile vs_2_0 resolve_glvert();

		#if SM4
		PixelShader = compile ps_4_0_level_9_1 resolve_glfrag();
		#elif SM3
		PixelShader = compile ps_3_0 resolve_glfrag();
		#else
		PixelShader = compile ps_2_0 resolve_glfrag();
		#endif
    }
}

