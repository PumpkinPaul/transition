
using Transition.Actors;

namespace Transition
{
    /// <summary>
    /// Represents an damage limitation device.
    /// </summary>
    /// <remarks>
    /// Shields can be used to turn health based damage into hit based damage. 
    /// You'd use them if you wanted to guarantee an actor takes x shots to kill no matter how powerful the weapon or how much health the actor has.
    /// They can be turned on / off at will according to the required behaviour and can be optionally drawn (although that code hasn't been added here and is still in LanderAlien)
    /// </remarks>
    public class Shield
    {
        public bool Active { get; set; }
        public int MaxValue { get; private set; }
        public int Value  { get; private set; }

        Actor _actor;

        public void Initialise(Actor actor, int value)
        {
            _actor = actor;

            Active = false;
            MaxValue = value;
            Value = value;
        }

        /// <summary>
        /// Reduces the shields value (if it has the capability to absorb damage still).
        /// </summary>
        /// <returns>True if damage was absorbed; otherwise false.</returns>
        public bool TakeDamage()
        {
            if (Value <= 0)
                return false;

            Value--;

            return true;
        }

        public TextureInfo GetTextureInfo()
        {
            var game = TransitionGame.Instance;

            //We have 10 steps worth of shieldy goodness that we can draw (36 degree increments forming a circle).
            var ratio = Value / (float)MaxValue;
 
            if (ratio >= 0.91f)
                return game.TextureManager.GetTextureInfo("ShieldGauge100");
                
            if (ratio >= 0.81f)
                return game.TextureManager.GetTextureInfo("ShieldGauge90");

            if (ratio >= 0.71f)
                return game.TextureManager.GetTextureInfo("ShieldGauge80");

            if (ratio >= 0.61f)
                return game.TextureManager.GetTextureInfo("ShieldGauge70");

            if (ratio >= 0.51f)
                return game.TextureManager.GetTextureInfo("ShieldGauge60");

            if (ratio >= 0.41f)
                return game.TextureManager.GetTextureInfo("ShieldGauge50");

            if (ratio >= 0.31f)
                return game.TextureManager.GetTextureInfo("ShieldGauge40");

            if (ratio >= 0.21f)
                return game.TextureManager.GetTextureInfo("ShieldGauge30");

            if (ratio >= 0.11f)
                return game.TextureManager.GetTextureInfo("ShieldGauge20"); 

            return game.TextureManager.GetTextureInfo("ShieldGauge10");
        }

        public void TurnOn()
        {
            if (Active)
                return;

            _actor.CacheHealth();
            
            Active = true;
        }

        public void TurnOff()
        {
            if (Active == false)
                return;

            _actor.ResetHealthFromCache();

            Active = false;
        }

        public void HandleBulletHit(Bullet bullet)
        {
            //So a bullet has hit us - we *may* be shielded - if we are we'd want to reduce the shield's value if it can absorb damage.
            if (Active == false)
                return;
                
            //currently hit based damage instead of health based so take a single shot of damage.
            if (TakeDamage())
            {
                _actor.Flash();
            }
            else
            {
                //If the shield has been destroyed then we can reset the lander's health so it will start taking damage.
                TurnOff();   
            }
        }
    }
}
