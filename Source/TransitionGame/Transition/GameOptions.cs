using System;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework.Storage;
using Transition.Storage;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class GameOptions
    {
        public int Width = 1280;
        public int Height = 720;
        public bool FullScreen;
        public bool Vibration;
        public bool TrailEffects = true;
        public bool CrtEffects;
        public bool Bloom;
        public float MusicVolume = 0.5f;
        public float SfxVolume = 0.5f;

        //Options for the Start Menu
        public int PlayerCount = 1;
        public string Player1Profile = string.Empty;
        public Difficulty Player1Difficulty = Difficulty.Normal;
        public string SignedInProfile;
        public bool RecordDemo;
        public ReplayPlaybackMode ReplayPlaybackMode;

        public float ScreenSize = 1.0f;
        public int OnlineTimeout = 10000;
        public int OnlineReadWriteTimeout = 10000;

        public static string Filename = "Options.xml";

        /// <summary>The location of the saved options file.</summary>
        private string _location;

        public static GameOptions LoadFromStorage(StorageContainer storageContainer)
        {
            GameOptions options;

            if (storageContainer.FileExists(Filename))
            {
                using (var fileStream = storageContainer.OpenFile(Filename, FileMode.Open))
                {
                    var x = new XmlSerializer(typeof (GameOptions));
                    options = (GameOptions)x.Deserialize(fileStream);
                }
            }
            else
            {
                options = new GameOptions();
            }

            options._location = Filename;

            return options;
        }

        /// <summary>
        /// Saves the options to disk.
        /// </summary>
        public void Save(BaseGame game)
        {
            game.StorageManager.Save(this, _location, format: StorageManager.Format.XmlSerializer);
        }
    }
}