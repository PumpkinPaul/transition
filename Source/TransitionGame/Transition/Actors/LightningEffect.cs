using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Rendering;

namespace Transition.Actors
{
    /// <summary>
    /// Represents lightning effects in the scene.
    /// </summary>
    /// p2[0]1----p2[i] --> p2[n]
    ///     |\    |
    ///     |  \  |
    ///     |    \|
    /// p1[0]-----p1[1] --> p1[n]
    public class LightningEffect : SpecialEffect
    {
        static readonly ushort[] Indices;
        readonly Geometry<VertexPositionColorTexture> _geometry = new Geometry<VertexPositionColorTexture>();

        private const int PointCount = 10;
        private const int Segments = PointCount - 1;
        private readonly Vector2[] _p1 = new Vector2[PointCount];
        private readonly Vector2[] _p2 = new Vector2[PointCount];
        private readonly Vector2[] _p1Previous = new Vector2[PointCount];
        private readonly Vector2[] _p2Previous = new Vector2[PointCount];
        private readonly Vector2[] _p1Target = new Vector2[PointCount];
        private readonly Vector2[] _p2Target = new Vector2[PointCount];
        private readonly float[] _displacement = new float[PointCount];

        public Color StartColor;
        public Color EndColor;

        private float _widthStart;
        private float _widthEnd;
        private float _displacementStart;
        private float _displacementEnd;
        private int _steps = 10;
        private int _ticks;

        private bool _anchorTail = true;
        private bool _anchorHead = true;

        private bool _collapseTail;
        private bool _collapseHead = true;

        public Vector2 TargetPosition;
        private Vector2 _previousTargetPosition;
        
        static LightningEffect()
        {
            //Build the index buffer - it's always the same so can do it up front.
            Indices = new ushort[Segments * 6]; 
            var vertexPointer = 0;
            for (var i = 0; i < Indices.Length; i += 6) 
            {
                //Triangle 1
                Indices[i] = (ushort)(vertexPointer);
                Indices[i + 1] = (ushort)(vertexPointer + 1);
                Indices[i + 2] = (ushort)(vertexPointer + 2);

                //Triangles 2
                Indices[i + 3] = (ushort)(vertexPointer + 2);
                Indices[i + 4] = (ushort)(vertexPointer + 1);
                Indices[i + 5] = (ushort)(vertexPointer + 3);

                vertexPointer += 2;
            }
        }

        /// <summary>
        /// Creates a new instance of a SpecialEffects.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public LightningEffect(TransitionGame game) : base(game)
        {
            _geometry.RenderStyle = TransparentRenderStyle.Instance;
            _geometry.TextureId = -1;
            _geometry.Indices = Indices;
        }

        protected override void OnInitialise(string name) 
        {
            base.OnInitialise(name);

            //TODO:
            _active = true;
        }

        public void Setup(Color startColor, Color endColor, float widthStart, float widthEnd, float displacementStart, float displacementEnd, int steps, bool anchorHead, bool anchorTail, bool collapseHead, bool collapseTail)
        {
            StartColor = startColor;
            EndColor = endColor;
            _widthStart = widthStart;
            _widthEnd = widthEnd;
            _displacementStart = displacementStart;
            _displacementEnd = displacementEnd;
            _steps = steps;
            
            _anchorHead = anchorHead;
            _anchorTail = anchorTail;
            
            _collapseHead = collapseHead;
            _collapseTail = collapseTail;
        }

        protected override void OnAddedToScene()
        {    		
            CreateDisplacements();
        }

        public void CreateDisplacements()
        {
            for (var i = 0; i < PointCount; i++) 
            {
                var segmentRatio = i / (float)(PointCount - 1);

                var displacement = MathHelper.Lerp(_displacementStart, _displacementEnd, segmentRatio);
                _displacement[i] = RandomHelper.Random.GetFloat(-displacement, displacement);

                _p1Previous[i] = _p1Target[i];
                _p2Previous[i] = _p2Target[i];
            }
        }

        protected override void OnPostUpdate()
        {
            _ticks++;

            //Create a vector from position to target and a vector step
            var v = Position;
            var v1 = TargetPosition - Position;
            var vectorStep = v1 / (float)Segments;

            var n = new Vector2(v1.Y, -v1.X);
            Vector2.Normalize(ref n, out n);
               
            //Create displacements...
            if (_ticks >= _steps)
            {
                CreateDisplacements();

                _ticks = 0;
            }

            //If the steps > 1 then the lightning will take time to get to the position/target - we don't want this so we need to rejig the current positions to pretend that they were
            //always like this.
            if (Position != _previousPosition)
            {
                //var d1 = TargetPosition - _previousTargetPosition;
                var d2 = Position - _previousPosition;

                var d = d2;//d1;// - d2;

                var dStep = d / (float)Segments;

                for (var j = 0; j < PointCount; j++) 
                {
                    _p1Previous[j] += dStep * (PointCount - 1 - j);
                    _p2Previous[j] += dStep * (PointCount - 1 - j);
                }
            }

            //If the steps > 1 then the lightning will take time to get to the position/target - we don't want this so we need to rejig the current positions to pretend that they were
            //always like this.
            if (TargetPosition != _previousTargetPosition)
            {
                var d1 = TargetPosition - _previousTargetPosition;
                //var d2 = Position - _previousPosition;

                var d = d1;// - d2;

                var dStep = d / (float)Segments;

                for (var j = 0; j < PointCount; j++) 
                {
                    _p1Previous[j] += dStep * j;
                    _p2Previous[j] += dStep * j;
                }
            }

			for (var i = 0; i < PointCount; i++) 
            {
                var segmentRatio = i / (float)(PointCount - 1);

                //Lerp the width of the strand from tail to tip...
                float width;   
                if ((i == 0 && _collapseHead || i == PointCount - 1 && _collapseTail))
                {
                    width = 0.001f;
                }
                else
                {
                    //Displace the current point along the normal and set the width... 
                    width = MathHelper.Lerp(_widthStart, _widthEnd, segmentRatio);
                }

                var halfWidth = n * width;

                //Create a new displacement vector if the timer has 'pinged'...
                var displacementVector = Vector2.Zero;

                if ((i == 0 && _anchorHead || i == PointCount - 1 && _anchorTail) == false) 
                {
                    displacementVector = n * _displacement[i];
                }

                _p1Target[i] = v + displacementVector + halfWidth;
                _p2Target[i] = v + displacementVector - halfWidth;

				v += vectorStep;
			}

			//Lerp between the current position and the target position based on time passed.
			for (var i = 0; i < PointCount; i++) 
            {
                var tickRatio =  (float)_ticks / _steps;
				Vector2.Lerp(ref _p1Previous[i], ref _p1Target[i], tickRatio, out _p1[i]); 
                Vector2.Lerp(ref _p2Previous[i], ref _p2Target[i], tickRatio, out _p2[i]); 
            }

            _previousTargetPosition = TargetPosition;
        }

        public void DrawEffect() 
        {
            //if (_length < 2)
            //    return;

            //var alpha = _fading ? LinearInterpolator.Instance.Interpolate(1.0f, 0.0f, (float)_tick / (float) FadeDuration) : 1.0f;
            //var color = new Color(0.0f, 0.94f * alpha, 1.0f * alpha, alpha);
            
            //var textureInfo = _game.TextureManager.GetTextureInfo("lightning1");
            //_geometry.TextureId = textureInfo.TextureId;
            _geometry.Layer = Layer;
            _geometry.Vertices.Clear();            

            //Convert the list of points to a quad.
            for (var i = 0; i < PointCount; i++) 
            {
                var ratio = i / (float)PointCount;
                var color = Color.Lerp(StartColor, EndColor, ratio);
                _geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(_p1[i].X, _p1[i].Y, 0), color, Vector2.One));
                _geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(_p2[i].X, _p2[i].Y, 0), color, Vector2.Zero));
                //_geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(_p1[i].X, _p1[i].Y, 0), color, new Vector2(textureInfo.SourceRect.Left, textureInfo.SourceRect.Bottom)));
                //_geometry.Vertices.Add(new VertexPositionColorTexture(new Vector3(_p2[i].X, _p2[i].Y, 0), color, new Vector2(textureInfo.SourceRect.Right, textureInfo.SourceRect.Top)));
            }

            _game.Renderer.DrawGeometry(_geometry);
        }
    }
}
