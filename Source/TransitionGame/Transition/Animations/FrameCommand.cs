using System;
using System.Xml.Linq;

namespace Transition.Animations
{
    /// <summary>
    /// An Animation command which displays an image for a number of frames.
	/// An Animation will typically consist of a number of FrameCommands, followed by a GotoCommand to set the sequence back to the beginning.
    /// </summary>
    public class FrameCommand : Command
    {			
        /// <summary>The duration of the frame.</summary>
	    int _duration;
	
		/// <summary>Offset for positioning additional sprites or emitters - eg for making eyes stick to a gidrah during walk cycle.</summary>
		float _childXOffset, _childYOffset;
	
		/// <summary>The name of the image to display...</summary>
		string _appearanceName;
	
		/// <summary>... or a framelist index</summary>
		int _idx;
	
		/// <summary>The new appearance to display.</summary>
		[NonSerialized] private IAppearance _spriteAppearance;
        			
        /// <summary>
        /// Executes this command. The command may have a 0 duration, in which case the next command should be executed. In this case, execute() returns
	    /// true to indicate that the next command should execute. 
        /// </summary>
        /// <param name="target">The target Animated thing</param>
        /// <returns>If the next command should be immediately executed.</returns>
	    public override bool Execute(Sprite target)
        {
            var currentSequence = target.Sequence;
            var currentTick = target.Tick + 1;

            if (currentTick == 1) 
            {
                target.ChildXOffset = _childXOffset;
                target.ChildYOffset = _childYOffset;

                var twiddle = false;

                if (_appearanceName == null)
                {
                    // Using frameindex
                    twiddle = target.SetFrame(_idx);
                }
            
                if (_spriteAppearance == null)
                {
                    twiddle = false;
                }

                twiddle = _spriteAppearance.ToSprite(target);

                if (twiddle) 
					return false; // Don't execute the next command - already been done when we set an animation
			}
	
			if (currentTick > _duration) 
            {
				target.Sequence = currentSequence + 1;
				target.Tick = 0;
				return true; // Next command
			}

            target.Tick = currentTick;
            return false; // Don't execute the next command
        }

        public override void Load(XElement element) 
        {
            if (element.Attribute("idx") != null)
                _idx = element.GetAttributeInt32("idx");
            else
            {
                _appearanceName = element.GetAttributeString("i");
                if (TransitionGame.Instance.TextureManager.GetTextureInfo(_appearanceName) == null)
                    throw new InvalidOperationException($"Missing image '{_appearanceName}' specified in {element}");
            }

            _duration = element.GetAttributeInt32("d");
			
            _childXOffset = element.GetAttributeSingle("childXOffset");
            _childYOffset = element.GetAttributeSingle("childYOffset");
	    }

		protected override void DoCreate() 
        {
            //TODO: Transition - Sprites - how do we handle animation here?
			// Load the image
			if (_appearanceName != null) 
				_spriteAppearance = TransitionGame.Instance.TextureManager.GetTextureInfo(_appearanceName);
		}
	
		protected override void DoDestroy() 
        {
			_spriteAppearance = null;
		}
    }
}
