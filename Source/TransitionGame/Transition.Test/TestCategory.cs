﻿using NUnit.Framework;

namespace Transition.Test 
{
    public static class TestCategory 
    {
        public const string UnitTests = "Unit Tests";
        public const string IntegrationTests = "Integration Tests";
    }
}
