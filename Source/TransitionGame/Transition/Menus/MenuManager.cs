using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Nuclex.Input;
using Transition.Input;
using Transition.MenuControls;
using Transition.Rendering;

namespace Transition.Menus
{
    /// <summary>
    /// Responsible for managing menus.
    /// </summary>
    public class MenuManager
    {
        public const int UnspecifiedControllerId = -1;

        //Menu Manager states...
        static private readonly IMenuManagerState FadeInState = new MenuManagerFadeInState();
        static private readonly IMenuManagerState FadeOutState = new MenuManagerFadeOutState();
        static private readonly IMenuManagerState NormalState = new MenuManagerNormalState();
        static private readonly IMenuManagerState HiddenState = new MenuManagerHiddenState();

        /// <summary>A reference to the game.</summary>
        private readonly BaseGame _game;

        /// <summary>A reference to the current state.</summary>
        private IMenuManagerState _state;

        /// <summary>Container for various controls - help text, button hints, etc.</summary>
        private readonly Overlay _backgroundOverlay;
        private readonly Overlay _foregroundOverlay;
        
        //Overlay controls...
        private TextLabelControl _activeControlHelpTextLabel;
        private ButtonHintControl _selectButtonHint;
        private ButtonHintControl _backButtonHint;
        private ImageControl _primaryGamerPictureImage;

        private KeyHintControl _selectKeyHint;
        private KeyHintControl _backKeyHint;
        
        /// <summary>Opacity for the overlay.</summary>
        private float _opacity;

        public MenuInput MenuInput { get; private set; }

        //public ControlType PrimaryNavigation = ControlType.GamePad;

        /// <summary>A list of currently 'active' menus </summary>
        /// <remarks>Menus get added and removed as required.</remarks>
        private readonly List<Menu> _menus = new List<Menu>();

        /// <summary>The mouse pointer.</summary>
		private readonly MousePointer _mousePointer;

        /// <summary>Indicates whether the manager should close all its menus.</summary>
        private bool _closingAll;

        /// <summary>The id of the controller responsible for navigating menus.</summary>
        private int _primaryControllerId = UnspecifiedControllerId;

        /// <summary>Id of the texture to use when the gamer linked to the primary controller is not signed in.</summary>
        private TextureInfo _notSignedInTextureInfo;

        readonly Dictionary<Type, Menu> _registeredMenus  = new Dictionary<Type, Menu>();

        public IEnumerable<Menu> Menus => _menus;

        /// <summary>
        /// Initialises a new instance of a MenuManager.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public MenuManager(BaseGame game)
        {
            _game = game;
            _mousePointer = new MousePointer(game, this);
            _state = HiddenState;
            _backgroundOverlay = new Overlay(_game);
            _foregroundOverlay = new Overlay(_game);
        }

        public void RegisterMenu(Menu menu, string name = null)
        {
            if (menu == null)
                throw new ArgumentNullException(nameof(menu));

            var key = menu.GetType();

            _registeredMenus[key] = menu;
            menu.Register(name);
        }
		
        /// <summary>
		/// Gets the position of the mouse pointer in world units.
		/// </summary>
		public Vector2 MouseWorldPosition => _mousePointer.WorldPosition;

        /// <summary>
		/// Gets the position of the mouse pointer in screen units.
		/// </summary>
		public Vector2 MouseWindowPosition => _mousePointer.WindowPosition;

        /// <summary>
        /// Gets whether the manager has any menus to manage.
        /// </summary>
        public bool HasMenu => _menus.Count > 0;

        /// <summary>
        /// Gets the id of the primary controller.
        /// </summary>
        /// <remarks>The primary controller is the only one allowed to navigate the menus.  
        /// A value of -1 indicates that no controller has been set yet.</remarks>
        public int PrimaryControllerId => _primaryControllerId;

        /// <summary>
        /// Gets the top menu - the one accepting input.
        /// </summary>
        public Menu TopMenu => _menus.Count > 0 ? _menus[_menus.Count - 1] : null;

        public Menu BottomMenu => _menus.Count > 0 ? _menus[0] : null;

        /// <summary>
        /// Gets whether the menu manager contains the pause menu.
        /// </summary>
        public bool ContainsMenu(Type type)
        {
            return _menus.Contains(GetMenu(type));
        }

        /// <summary>Gets the control the mouse pointer is hovering over.</summary>
        /// <remarks>This could be null!</remarks>
        public MenuControl HoverControl { get; private set; }

        /// <summary>Gets the control that has the focus.</summary>
        /// <remarks>This could be null!</remarks>
        public MenuControl FocusControl { get; set; }

        MenuControl _mouseEnterControl;
        //bool _logMouseMove = true;

        MenuControl _previousHoverControl;

        public void LoadContent()
        {
            _notSignedInTextureInfo = _game.TextureManager.GetTextureInfo("DefaultGamerpic");
            
            ConfigureOverlays();
        }

        public void ConfigureOverlays()
        { 
            _backgroundOverlay.Register("MenuManagerBackground");
            _foregroundOverlay.Register("MenuManagerForeground");

            _selectButtonHint = _foregroundOverlay.GetControl<ButtonHintControl>("selectButtonHint");
            _backButtonHint = _foregroundOverlay.GetControl<ButtonHintControl>("backButtonHint");
            
            _selectKeyHint = _foregroundOverlay.GetControl<KeyHintControl>("selectKeyHint");
            _backKeyHint = _foregroundOverlay.GetControl<KeyHintControl>("backKeyHint");
            
            _primaryGamerPictureImage = _foregroundOverlay.GetControl<ImageControl>("primaryGamerPictureImage");
            _activeControlHelpTextLabel = _foregroundOverlay.GetControl<TextLabelControl>("activeControlHelpTextLabel");
        }

        public void ConfigureMenus()
        {
            foreach (var menu in _registeredMenus.Values)
                menu.ConfigureOverlay();
        }

        /// <summary>
        /// Fades the menu manager in.
        /// </summary>
        public void FadeIn()
        {
            _state = MenuManager.FadeInState;
            _backgroundOverlay.StartTransition("fadeIn");
            _foregroundOverlay.StartTransition("fadeIn");
        }

        /// <summary>
        /// Fades the menu manager out.
        /// </summary>
        public void FadeOut()
        {
            _state = MenuManager.FadeOutState;
        }

        /// <summary>
        /// Gets a menu of a specific type.
        /// </summary>
        public Menu GetMenu(Type type)
        {
            return _registeredMenus.ContainsKey(type) == false ? null : _registeredMenus[type];
        }

        /// <summary>
        /// Adds the default menus after a play session
        /// </summary>
        /// <remarks>Default menus are currently Intro and MainMenu.</remarks>
        public void AddDefaultMenus()
        {
            //Insert the menus under any existing menus (e.g. Pause, Heroes, Results)?
            if (HasMenu)
            {
                InsertMenu(typeof(IntroMenu), 0);
                InsertMenu(typeof(MainMenu), 1);
            }
            else
            {
                //No menu currently showing so just add them as normal.
                AddMenu(typeof(IntroMenu));
                AddMenu(typeof(MainMenu));
            }
        }

        /// <summary>
        /// Adds a menu to the manager.
        /// </summary>
        public void AddMenu(Type type)
        {
            AddMenu(GetMenu(type));
        }

        /// <summary>
        /// Adds a menu to the manager.
        /// </summary>
        public void AddMenu(Menu menu)
        {
            if (_menus.Contains(menu))
                return;

            if (HasMenu)
                TopMenu.LostFocus();
            
            menu.MenuManager = this;

            _menus.Add(menu);

            menu.AddedToManager();

            SetFocus(menu.DefaultControl, false);

            _closingAll = false;
        }

        /// <summary>
        /// Inserts a menu to the manager directly under the top menu.
        /// </summary>
        private void InsertMenu(Type type, int index)
        {
            var menu = GetMenu(type);

            if (HasMenu == false)
            {
                AddMenu(menu);
                return;
            }

            if (_menus.Contains(menu))
                return;

            //Just make sure the menu below us loses focus.
            if (index > 0)
                _menus[index - 1].LostFocus();

            SetFocus(menu.DefaultControl, false);

            menu.MenuManager = this;

            _menus.Insert(index, menu);

            menu.AddedToManager();

            _closingAll = false;
        }

        /// <summary>
        /// Closes all menus.
        /// </summary>
        public void CloseAllMenus()
        {
            _closingAll = true;

            foreach (var menu in _menus)
            {
                if (menu.CachedFocusControl != null)
                    menu.CachedFocusControl.PerformLostFocus();

                menu.Close();
            }

            FocusControl = null;
        }

        /// <summary>
        /// Updates the menu manager.
        /// </summary>
        public void Update()
        {
            _state.Update(this);

            _backgroundOverlay.Update();
            _foregroundOverlay.Update();

            _game.IsMouseVisible = _game.InputMethodManager.InputMethod.SupportsMouse;

            if (_game.InputMethodManager.InputMethod.SupportsMouse)
                _mousePointer.Update();
            else
                HoverControl = null;

            if (HasMenu == false)
                return;

            UpdateOverlayControlProperties();

            //Get input
            MenuInput = _game.InputMethodManager.InputMethod.GatherMenuInput(this, _game.ActivePlayer.PlayerIndex, _game.ActivePlayer.Profile);

            HandleBackPressed();
            UpdateMenus();

            //What is the top menu doing?
            if (TopMenu.IsClosed)
            {
                RemoveTopMenu();
            }
            else if (TopMenu.IsClosing)
            {
                UpdateTopMenuClosing();
            }
            else
            {
                UpdateTopMenuNormal();
            }
        }

        public void MenuCloseRequested(Menu menu)
        {
            if (menu != TopMenu)
                return;
            
            if (_menus.Count > 1)
            {
                SetFocus(_menus[_menus.Count - 2].CachedFocusControl, false);
            }
        }

        /// <summary>
        /// Updates the propertie of the overlay controls.
        /// </summary>
        private void UpdateOverlayControlProperties()
        {
            _activeControlHelpTextLabel.Visible = FocusControl != null && string.IsNullOrEmpty(FocusControl.HelpText) == false;

            if (_primaryControllerId == UnspecifiedControllerId)
                return;

            if (_game.InputMethodManager.InputMethod.SupportsKeyboard)
            {
                _selectKeyHint.Visible = FocusControl != null && string.IsNullOrEmpty(FocusControl.AButtonText) == false;
                _backKeyHint.Visible = HasMenu && TopMenu.AllowCloseWhenBackPressed;
                
                if (_selectKeyHint.Visible)
                {
                    if (_game.InputMethodManager.InputMethod.SupportsMouse)
                        _selectKeyHint.Text = "LMB";
                    else
                    {
                        if (_game.ActivePlayer != null)
                            _selectKeyHint.Text = _game.ActivePlayer.Profile.InputKeys.SelectKeyHint;
                        else
                            _selectKeyHint.Text = "";
                    }
                }

                if (_backKeyHint.Visible && HasMenu)
                    _backKeyHint.HintText = TopMenu.BackButtonText;
                
                _selectButtonHint.Visible = false;
                _backButtonHint.Visible = false; 
            }
            else
            {
                _selectButtonHint.Visible = FocusControl != null && string.IsNullOrEmpty(FocusControl.AButtonText) == false;
                _backButtonHint.Visible = HasMenu && TopMenu.AllowCloseWhenBackPressed;
				
                _selectKeyHint.Visible = false;
                _backKeyHint.Visible = false;
            }

            var helpText = FocusControl != null ? FocusControl.HelpText : string.Empty;
            var aButtonText = FocusControl != null ? FocusControl.AButtonText : string.Empty;

            if (_activeControlHelpTextLabel.Visible)
                _activeControlHelpTextLabel.Text = helpText;

            if (_selectButtonHint.Visible)
                _selectButtonHint.HintText = aButtonText;

            if (_selectKeyHint != null && _selectKeyHint.Visible)
                _selectKeyHint.HintText = aButtonText;

            if (_backButtonHint.Visible && HasMenu)
                _backButtonHint.HintText = TopMenu.BackButtonText;
        }

        /// <summary>
        /// Handles the player pressing the 'back out of a menu' button
        /// </summary>
        private void HandleBackPressed()
        {
            //Handle the 'back' button - allows a menu to go back no matter which control has the focus.
            if (MenuInput.BackJustPressed && TopMenu != null && TopMenu.AcceptsInput)
                TopMenu.BackPressed();
        }

        /// <summary>
        /// Updates the menus.
        /// </summary>
        private void UpdateMenus()
        {
            //Allow each menu to update itself
            //Note - menus can add other menus so can't use foreach here.
// ReSharper disable ForCanBeConvertedToForeach
            for (var i = 0; i < _menus.Count; ++i)
// ReSharper restore ForCanBeConvertedToForeach
            {
                if (_menus[i].IsClosed == false)
                    _menus[i].Update();
            }
        }

        /// <summary>
        /// Removes the top menu from the list of menus. 
        /// </summary>
        private void RemoveTopMenu()
        {
            TopMenu.RemovedFromManager();
            _menus.Remove(TopMenu);
        }

        public void InputMethodChanged(object sender, EventArgs e)
        {
            SetPrimaryControllerId((int)_game.InputMethodManager.InputMethod.PlayerIndex);

            if (TopMenu == null || TopMenu.AcceptsInput == false)
                return;
            
            TopMenu.HandleCloseButtonVisibility();

            if (FocusControl == null)
                SetFocus(TopMenu.DefaultControl, false);
        }

        public void InputMethodPlayerIndexChanged(object sender, EventArgs e)
        {
            SetPrimaryControllerId((int)_game.InputMethodManager.InputMethod.PlayerIndex);
        }

        public void SetFocus(MenuControl control, bool playSound)
        {
            if (control == FocusControl)
                return;

            if (FocusControl != null)
                FocusControl.PerformLostFocus();

            if (control != null)
            {
                if (playSound)
                    _game.PlaySound("MenuItemGotFocus");

                control.PerformGotFocus();
            }

            FocusControl = control;
        }

        /// <summary>
        /// Handles processing when the top menu is in the closing state. 
        /// </summary>
        private void UpdateTopMenuClosing()
        {
            if (_closingAll == false)
            {
                //Previous menu now has focus (if one exists)
                if (_menus.Count > 1 && _menus[_menus.Count - 2].IsBeingShown == false)
                    _menus[_menus.Count - 2].GotFocus();
            }
        }

        private void UpdateTopMenuNormal()
        {
            if (TopMenu.AcceptsInput == false) 
                return;

            //Perform all the common gamepad and keyboard actions
            if (MenuInput.PreviousControlPressed)
            {
                if (FocusControl != null)
                {
                    if (FocusControl.CapturesPreviousNextControlRequest)
                        FocusControl.CapturePreviousControl();
                    else
                        SetFocus(GetPreviousFocusControl(), true);   
                }
            }
            else if (MenuInput.NextControlPressed)
            {
                if (FocusControl != null)
                {
                    if (FocusControl.CapturesPreviousNextControlRequest)
                        FocusControl.CaptureNextControl();
                    else
                        SetFocus(GetNextFocusControl(), true);   
                }
            }

            if (MenuInput.IncreaseValuePressed && FocusControl != null) 
            {
                FocusControl.PerformIncreaseValue();
            }
            else if (MenuInput.DecreaseValuePressed && FocusControl != null)
            {
                FocusControl.PerformDecreaseValue();
            }

            //CHeck out the mouse input - we try to mimic Windows Forms behaviours and event orders as much as possible.
            if (_game.InputMethodManager.InputMethod.SupportsMouse)
            {
                _previousHoverControl = HoverControl;

                //A mouse button down stops the system from setting a new hover control so only test if it is currently in an up state
                if (MouseHelper.IsLeftButtonUp)
                {
                    HoverControl = TopMenu.GetHoverControl(MenuInput.PointerPosition);
                }

                MenuControl mouseUpControl = null;

                //MouseUp will fire for the _mouseEnterControl - it does not depend on any focus or current hover control.
                if (MouseHelper.IsLeftButtonJustUp)
                {
                    if (_mouseEnterControl != null)
                        mouseUpControl = _mouseEnterControl;
                }

                //Mouse enters and leaves a control. 
                //Note - we don't test too see if the mouse actually moves as we also need to handle the situation where a mouse up happens after a drag off the _mouseEnterControl
                if (HoverControl != _previousHoverControl)
                {
                    //MouseLeave is tested first (but should only fire if an MouseEnter has already fired).
                    if (_mouseEnterControl != null)
                    {
                        _mouseEnterControl.PerformMouseLeave();
                        _mouseEnterControl = null;
                        //_logMouseMove = true;
                    }
                    if (HoverControl != null)
                    {
                        _mouseEnterControl = HoverControl;
                        HoverControl.PerformMouseEnter();
                    }
                }

                //MouseDown event
                if (MouseHelper.IsLeftButtonJustDown)
                {
                    if ((HoverControl != null && HoverControl.TabIndex != Overlay.NoTabIndex) && HoverControl != FocusControl)
                        SetFocus(HoverControl, false);

                    //Perform the mouse down on the _mouseEnterControl
                    if (_mouseEnterControl != null)
                        _mouseEnterControl.PerformMouseDown();
                }

                //Simple mouse moving over the current hover control.
                //NOTE - the HoverControl is only queried when the LMB is in an up state
                if (MouseHelper.HasMouseMoved)
                {
                    if (HoverControl != null)
                    {
                        //if (_logMouseMove)
                            HoverControl.PerformMouseMove();

                        //_logMouseMove = false;
                    }
                }

                //Test to see of the keyboard Action key should cause a click to fire.
                if (MenuInput.SelectJustReleased)
                {
                    //Action key caused the click!
                    if (FocusControl != null)
                        FocusControl.PerformClick();
                }
                //Otherwise test to see if if it the left mouse button being released
                else if (MouseHelper.IsLeftButtonJustUp)
                {
                    //Mouse caused the click - fire the event if the mouse down and the mouse up originated for the same control
                    if (FocusControl != null && _mouseEnterControl != null && FocusControl == HoverControl && FocusControl == _mouseEnterControl && mouseUpControl != null && mouseUpControl == _mouseEnterControl)// && _previousHoverControl == HoverControl)
                        FocusControl.PerformClick();
                }

                if (mouseUpControl != null)
                    mouseUpControl.PerformMouseUp();
            }
            else
            {
                //Non mouse supporting input schemes - test to see if the Click event should fire.
                if (FocusControl == null)
                    return;

                if ((MenuInput.SelectJustPressed && FocusControl.AToSelect) || (MenuInput.StartJustPressed && FocusControl.StartToSelect))
                {
                    FocusControl.PerformClick();
                }
            }
        }

        ///// <summary>
        ///// Handles normal processing for the top menu.
        ///// </summary>
        ///// <remarks>Basically any state other than closed or closing.</remarks>
        //private void UpdateTopMenuNormalOLD()
        //{
        //        if (TopMenu.AcceptsInput == false) 
        //        return;
                
        //    if (_game.InputMethodManager.InputMethod.SupportsMouse)
        //    {
        //        HoverControl = TopMenu.GetHoverControl(MenuInput.PointerPosition);

        //        if (MenuInput.SelectPressed == false)
        //        {
        //            if (!(MenuInput.SelectJustPressed || (FocusControl != null && FocusControl.CapturesPreviousNextControlRequest)))
        //            {
        //                var mouseOverTabStopControl = TopMenu.GetTabStopHoverControl(MenuInput.PointerPosition);
                                
        //                //Don't focus controls that shouldn't get it!
        //                if (mouseOverTabStopControl != null && mouseOverTabStopControl.TabIndex == -1)
        //                    mouseOverTabStopControl = null;

        //                if (FocusControl != mouseOverTabStopControl)
        //                {
        //                    if (FocusControl != null)
        //                        FocusControl.LoseFocus();

        //                    if (mouseOverTabStopControl != null)
        //                        mouseOverTabStopControl.GetFocus();
        //                }

        //                FocusControl = mouseOverTabStopControl;
        //            }
        //        }
        //    }
   
        //    if (MenuInput.PreviousControlPressed)
        //    {
        //        if (FocusControl != null)
        //        {
        //            if (FocusControl.CapturesPreviousNextControlRequest)
        //                FocusControl.CapturePreviousControl();
        //            else
        //                SetFocus(GetPreviousFocusControl(), true);   
        //        }
        //    }
        //    else if (MenuInput.NextControlPressed)
        //    {
        //        if (FocusControl != null)
        //        {
        //            if (FocusControl.CapturesPreviousNextControlRequest)
        //                FocusControl.CaptureNextControl();
        //            else
        //                SetFocus(GetNextFocusControl(), true);   
        //        }
        //    }

        //    if (MenuInput.IncreaseValuePressed && FocusControl != null) 
        //    {
        //        FocusControl.IncreaseValue();
        //    }
        //    else if (MenuInput.DecreaseValuePressed && FocusControl != null)
        //    {
        //        FocusControl.DecreaseValue();
        //    }
        
        //    if (FocusControl == null)
        //        return;

        //    if ((((_game.InputMethodManager.InputMethod.SupportsMouse && MenuInput.SelectJustReleased) || (_game.InputMethodManager.InputMethod.SupportsMouse == false && MenuInput.SelectJustPressed)) && FocusControl.AToSelect) || (MenuInput.StartJustPressed && FocusControl.StartToSelect))
        //    {
        //        //Click could close the menu and change the active control so cache it and check it after the click returns.
        //        var topMenu = TopMenu;
        //        var cachedActiveControl = FocusControl;
        //        FocusControl.Click();

        //        if (topMenu == TopMenu)
        //        {
        //            //Does the control allow mouseup processing when it's no longer the active control
        //            if (_game.InputMethodManager.InputMethod.SupportsMouse && (cachedActiveControl.AllowsOutOfBoundsMouseUp || cachedActiveControl == FocusControl))
        //                cachedActiveControl.PerformMouseUp();
        //        }
        //    }
        //    else if ((MenuInput.SelectPressed && FocusControl.AToSelect) || (MenuInput.StartPressed && FocusControl.StartToSelect))
        //    {
        //        if (_game.InputMethodManager.InputMethod.SupportsMouse)
        //            FocusControl.PerformMouseDown();
        //    }

        //    if (_game.InputMethodManager.InputMethod.SupportsMouse)
        //    {
        //        if (FocusControl == null)
        //            return;

        //        FocusControl.PerformMouseMove();
        //    }
        //}

        /// <summary>
        /// Sets the primary controller to the last controller to have registered input.
        /// </summary>
        public void SetPrimaryControllerId()
        {
            SetPrimaryControllerId(UnspecifiedControllerId);
        }

        /// <summary>
        /// Sets the primary controller.
        /// </summary>
        /// <param name="id">The id of the primary controller.</param>
        public void SetPrimaryControllerId(int id)
        {
            
            if (id == -2) //back to intro menu
            {
                _primaryControllerId = UnspecifiedControllerId;
            }
            else if (id == -1)
            {
                ClearPrimaryControllerGamerpic();

                //IsUsingKeyboard = true;
                _primaryControllerId = 0; //default to player 1 this stops us from having to check for -1s all over the place

                _selectKeyHint.Visible = true;
                _backKeyHint.Visible = true;
            
                _selectButtonHint.Visible = false;
                _backButtonHint.Visible = false;
            }
            else
            {
                _primaryControllerId = id;
                //IsUsingKeyboard = false;
				
				_selectButtonHint.Visible = true;
                _backButtonHint.Visible = true;
                
				#if WINDOWS || WINDOWS_STOREAPP || MONOMAC || LINUX
	                _selectKeyHint.Visible = false;
	                _backKeyHint.Visible = false;
	            
					var profileInfo = _game.PlayerProfileManager.GetProfileInfo((ExtendedPlayerIndex)id);
	                _primaryGamerPictureImage.TextureInfo = profileInfo.GamepicTextureInfo;
				#endif
            }
        }

        /// <summary>
        /// Gets the next control that can be 'tabbed' to.
        /// </summary>
        private MenuControl GetNextFocusControl()
        {
            return TopMenu.GetNextFocusControl(FocusControl);
        }

        /// <summary>
        /// Gets the previous control that can be 'tabbed' to.
        /// </summary>
        private MenuControl GetPreviousFocusControl()
        {
            return TopMenu.GetPreviousFocusControl(FocusControl);
        }

        /// <summary>
        /// Adds a pause menu if it's not already added to the menu manager.
        /// </summary>
        /// <returns>True if a PauseMenu was added; otherwise false.</returns>
        public bool EnsurePauseMenu()
        {
            foreach (var menu in _menus)
            {
                if (menu is PauseMenu)
                    return false;
            }

            AddMenu(typeof(PauseMenu));

            return true;
        }

        /// <summary>
        /// Clears the gamerpic indicating the primary controller.
        /// </summary>
        private void ClearPrimaryControllerGamerpic()
        {
            _primaryGamerPictureImage.TextureInfo = _notSignedInTextureInfo;
        }

        /// <summary>
        /// Renders all menus.
        /// </summary>
        public void Draw()
        {
            if (!HasMenu)
                return;

            _game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);

            DrawBackgroundOverlay();
			DrawMenus();
            DrawForegroundOverlay();

            _game.Renderer.End();
        }

        /// <summary>
        /// Draws the menus.
        /// </summary>
        public void DrawMenus()
        {
            foreach (var menu in _menus)
                menu.Draw();
        }

        /// <summary>
        /// Draws the menu manager's overlay
        /// </summary>
        /// <remarks>The overlay contains all the button hint, gamer pictures, etc.</remarks>
        private void DrawBackgroundOverlay()
        {
            if (TopMenu is IntroMenu == false)
            {
                _backgroundOverlay.Opacity = _opacity; 
                _backgroundOverlay.Draw();
            }
        }

        /// <summary>
        /// Draws the menu manager's overlay
        /// </summary>
        /// <remarks>The overlay contains all the button hint, gamer pictures, etc.</remarks>
        private void DrawForegroundOverlay()
        {
            if (TopMenu is IntroMenu == false)
            {
                _foregroundOverlay.Opacity = _opacity; 
                _foregroundOverlay.Draw();
            }
        }

        /// <summary>
        /// Exits the game.
        /// </summary>
        /// <remarks>Closes the application and returns to the desktop / dashboard.</remarks>
        public void ExitGame()
        {
            _game.Exit();
        }

        /// <summary>
        /// Base class for the MenuManager's state machine.
        /// </summary>
        public interface IMenuManagerState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="menuManager">The menuManager to update.</param>
            void Update(MenuManager menuManager);
        }

        /// <summary>
        /// MenuManager is fading in.
        /// </summary>
        public struct MenuManagerFadeInState : IMenuManagerState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="menuManager">The menuManager to update.</param>
            public void Update(MenuManager menuManager)
            {
                menuManager._opacity += 0.05f;
                if (menuManager._opacity >= 1.0f)
                {
                    menuManager._opacity = 1.0f;
                    menuManager._state = MenuManager.NormalState;
                }
            }
        }

        /// <summary>
        /// MenuManager is fading out.
        /// </summary>
        public struct MenuManagerFadeOutState : IMenuManagerState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="menuManager">The menuManager to update.</param>
            public void Update(MenuManager menuManager)
            {
                menuManager._opacity -= 0.05f;
                if (menuManager._opacity <= 0.0f)
                {
                    menuManager._opacity = 0.0f;
                    menuManager._state = MenuManager.HiddenState;
                }
            }
        }

        /// <summary>
        /// Normal state of the menuManager.
        /// </summary>
        public struct MenuManagerNormalState : IMenuManagerState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="menuManager">The menuManager to update.</param>
            public void Update(MenuManager menuManager) { }
        }

        /// <summary>
        /// Hidden state for the menuManager.
        /// </summary>
        public struct MenuManagerHiddenState : IMenuManagerState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="menuManager">The menuManager to update.</param>
            public void Update(MenuManager menuManager) { }
        }
    }
}
