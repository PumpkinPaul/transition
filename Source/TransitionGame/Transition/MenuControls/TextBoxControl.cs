using System;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.Input;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a text entry control.
    /// </summary>
    /// <remarks>Uses the XNA text entry dialog for Windows and Xbox.</remarks>
    public class TextBoxControl : MenuControl
    {
        /// <summary>Where the text is rendered in relation to the control's position.</summary>
        public Alignment Alignment = Alignment.CentreLeft;

        /// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;

        /// <summary>The size of the font.</summary>
        public Vector2 FontSize;
        
        /// <summary>The maximum number of character allowed.</summary>
        public byte MaxLength = 16;

        /// <summary>The text to render.</summary>
        string _text;

        string _gotFocusText;

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <remarks>If the text is larger than the MaxLength it will be trimmed.</remarks>
        public string Text
        {
            get { return _text; }
            set
            {
                if (string.IsNullOrEmpty(value) == false)
                {
                    if (value.Length > MaxLength)
                        value = value.Substring(0, Math.Min(value.Length, MaxLength));
                }

                _text = value;
            }
        }

        public Vector2 Size;
        public Vector2 TextOffset = new Vector2(0.1f, -0.05f);
        public Color TextTint;
        public float BorderWidth = 0.085f;

        int _ticks;
        bool _caretVisible;
        int _caretIndex;

        int _selectionStartIndex;
        int _selectionEndIndex;

        int _anchorCaretIndex;

        public override bool AutoTabIndex { get { return true; } }

        /// <summary>
        /// Initialises a new instance of a TextEntryControl.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public TextBoxControl(BaseGame game) : base(game)
        {
            Font = Theme.Current.GetFont(Theme.Fonts.Default);
            FontSize = Theme.Current.GetFontSize(Theme.FontSizes.Normal);
            Size = new Vector2(5.0f, FontSize.X);
            Tint = Theme.Current.GetColor(Theme.Colors.Background) * 1.5f;
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            Font = ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            Alignment = element.GetAttributeEnum("alignment", Alignment);
            MaxLength = element.GetAttributeByte("maxLength", MaxLength);
            Text = element.GetAttributeString("text", Text);
            TextTint = element.GetAttributeColor("textTint", TextTint);
            TextOffset = element.GetAttributeVector2("textOffset", TextOffset);
            Size = element.GetAttributeVector2("size", Size);
            BorderWidth = element.GetAttributeSingle("borderWidth", BorderWidth);
        }

        /// <summary>
        /// Gets the control bounds (a rectangle defining the control's limits).
        /// </summary>
        public override Rect Bounds
        {
            get { return new Rect(Position, Size, Alignment); }
        }

        public void ResetText()
        {
            Text = "0";//_gotFocusText;
        }

        protected override void OnGotFocus() 
        {
            _gotFocusText = _text;
            _caretVisible = true;
            Game.NuclexInputManager.GetKeyboard().CharacterEntered += CharacterEntered;
        }

        protected override void OnLostFocus() 
        {
            _caretVisible = false;
            Game.NuclexInputManager.GetKeyboard().CharacterEntered -= CharacterEntered;
        }

        /// <summary>
        /// Allows a derived control to update itself.
        /// </summary>
        protected override void OnUpdate()
        {
            if (Game.MenuManager.FocusControl != this)
                return;

            _ticks++;
            if (_ticks > 24)
            {
                _caretVisible = !_caretVisible;
                _ticks = 0;
            }

            HandleSpecialKeys();
        }

        void HandleSpecialKeys()
        {
            var minSelectionIndex = Math.Min(_selectionStartIndex, _selectionEndIndex);
            var maxSelectionIndex = Math.Max(_selectionStartIndex, _selectionEndIndex);
            var selectionLength = maxSelectionIndex - minSelectionIndex;

            if (KeyboardHelper.IsKeyJustPressed(Keys.Delete) && (_caretIndex < _text.Length || selectionLength > 0))
            {
                if (selectionLength > 0)
                {
                    //Delete selected text
                    if (minSelectionIndex > -1)
                    {
                        DeleteSelectedText(minSelectionIndex, maxSelectionIndex);
                    }
                    else
                    {
                        _caretIndex = 0;
                    }

                    ClearSelection();
                }
                else
                {
                    _text = _text.Remove(_caretIndex, 1);
                }
            }
            else if (KeyboardHelper.IsKeyJustPressed(Keys.Home))
            {
                if (_selectionEndIndex == -1)
                {
                    _selectionStartIndex = _caretIndex;
                }

                _caretIndex = 0;

                if (KeyboardHelper.IsKeyPressed(Keys.LeftShift) || KeyboardHelper.IsKeyPressed(Keys.RightShift))
                {
                    //Select
                    _selectionEndIndex = _caretIndex;    
                }
                else
                {
                    ClearSelection();  
                }
            }
            else if (KeyboardHelper.IsKeyJustPressed(Keys.End))
            {
                if (_selectionEndIndex == -1)
                {
                    _selectionStartIndex = _caretIndex;
                }

                _caretIndex = _text.Length;

                if (KeyboardHelper.IsKeyPressed(Keys.LeftShift) || KeyboardHelper.IsKeyPressed(Keys.RightShift))
                {
                    //Select
                    _selectionEndIndex = _caretIndex; 
                }
                else
                {
                    ClearSelection();
                }
            }
            else if (KeyboardHelper.IsKeyJustPressed(Keys.Z) && (KeyboardHelper.IsKeyPressed(Keys.LeftControl) || KeyboardHelper.IsKeyPressed(Keys.RightControl)))
            {
                Text = _gotFocusText;
                _caretIndex = _text.Length;
                ClearSelection();
            }
        }

        void DeleteSelectedText(int minSelectionIndex, int maxSelectionIndex)
        {
            var selectionLength = maxSelectionIndex - minSelectionIndex;

            if (selectionLength > 0 && minSelectionIndex > -1)
            {
                _text = _text.Remove(minSelectionIndex, maxSelectionIndex - minSelectionIndex);
                _caretIndex = minSelectionIndex;
            }
        }

        void ClearSelection()
        {
            _selectionStartIndex = -1;
            _selectionEndIndex = -1;
        }

        void CharacterEntered(char character)
        {
            var minSelectionIndex = Math.Min(_selectionStartIndex, _selectionEndIndex);
            var maxSelectionIndex = Math.Max(_selectionStartIndex, _selectionEndIndex);
            var selectionLength = maxSelectionIndex - minSelectionIndex;

            if (character == 8 && ((_caretIndex > 0 && _text.Length > 0) || selectionLength > 0))
            {
                if (selectionLength > 0)
                {
                    //Delete selected text
                    DeleteSelectedText(minSelectionIndex, maxSelectionIndex);
                    ClearSelection();
                }
                else
                {
                    // Remove 1 character at end
                    _text = _text.Remove(_caretIndex - 1, 1);
                    _caretIndex--;
                }
            }
            //Paste
            else if (character == 22)
            {
                var dataObject = Clipboard.GetDataObject();

                if (dataObject != null && dataObject.GetDataPresent(DataFormats.Text))
                {
                    DeleteSelectedText(minSelectionIndex, maxSelectionIndex);

                    var textToInsert = (string)dataObject.GetData(DataFormats.Text);

                    Text = _text.Insert(_caretIndex, textToInsert);
                    _caretIndex += textToInsert.Length;
                    if (_caretIndex > _text.Length)
                        _caretIndex = Text.Length;

                    ClearSelection();
                }
            }
            //Cut
            else if (character == 24)
            {
                if (selectionLength > 0 && _selectionStartIndex > -1)
                {
                    Clipboard.SetText(_text.Substring(minSelectionIndex, maxSelectionIndex - minSelectionIndex));
                    //Delete selected text
                    DeleteSelectedText(minSelectionIndex, maxSelectionIndex);
                }

                ClearSelection();
            }
            //Copy
            else if (character == 3)
            {
                if (selectionLength > 0 && _selectionStartIndex > -1)
                {
                    Clipboard.SetText(_text.Substring(minSelectionIndex, maxSelectionIndex - minSelectionIndex));
                }
            }
            else if (character >= 32 && character <= 127)
            {
                if (selectionLength > 0)
                {
                    //Delete selected text
                    DeleteSelectedText(minSelectionIndex, maxSelectionIndex);
                }

                if (_text.Length < MaxLength)
                {
                    // Then add the letter to our inputText. Check also the shift state!
                    if (_caretIndex >= _text.Length)
                    {
                        _text += character;
                    }
                    else
                    {
                        _text = _text.Insert(_caretIndex, character.ToString());
                        if (_text.Length > MaxLength)
                            _text = _text.Substring(0, MaxLength);
                    }

                    _caretIndex++;

                    ClearSelection();
                }
            }
        }

        void ShowCaret()
        {
            _ticks = 0;
            _caretVisible = true;
        }

        /// <summary>
        /// .
        /// </summary>
        protected override bool OnIncreaseValue()
        {
            if (_caretIndex >= _text.Length)
                return false;

            ShowCaret();

            ++_caretIndex;

            if (KeyboardHelper.IsKeyPressed(Keys.LeftShift) || KeyboardHelper.IsKeyPressed(Keys.RightShift))
            {
                if (_selectionEndIndex == -1)
                {
                    _selectionStartIndex = _caretIndex - 1;
                    _selectionEndIndex = _caretIndex - 1;
                }
                _selectionEndIndex++;
            }
            else
            {
                _selectionStartIndex = _caretIndex;
                _selectionEndIndex = _selectionStartIndex;
            }
            return true;
        }

        /// <summary>
        /// .
        /// </summary>
        protected override bool OnDecreaseValue()
        {
            if (_caretIndex <= 0)
                return false;

            ShowCaret();

            --_caretIndex;

            if (KeyboardHelper.IsKeyPressed(Keys.LeftShift) || KeyboardHelper.IsKeyPressed(Keys.RightShift))
            {
                if (_selectionEndIndex == -1)
                {
                    _selectionStartIndex = _caretIndex + 1;
                    _selectionEndIndex = _caretIndex + 1;
                }
                _selectionEndIndex--;
            }
            else
            {
                _selectionStartIndex = _caretIndex;
                _selectionEndIndex = _selectionStartIndex;
            }
            return true;
        }

        protected override void OnMouseDown() 
        {
            ClearSelection();
            SetPositionOfCaret(dragging: false);
        }

        protected override void OnMouseMove() 
        {
            if (MouseHelper.IsLeftButtonDown)
                SetPositionOfCaret(dragging: true);
        }

        void SetPositionOfCaret(bool dragging)
        {
            //Default to the end - why not, sure this is a good idea
            _caretIndex = _text.Length;

            ShowCaret();

            //Where is the mouse in relation to the text? We need to get the index of the caret at the mouse position
            var bounds = Bounds;
            var mouseRelative = new Vector2(Game.MenuManager.MouseWorldPosition.X - bounds.Left, bounds.Top - Game.MenuManager.MouseWorldPosition.Y);

            if (mouseRelative.X < TextOffset.X)
                _caretIndex = 0;
            else if (mouseRelative.X > Font.GetTextLength(_text, FontSize.X))
                _caretIndex = _text.Length;
            else
            {
                mouseRelative.X -= TextOffset.X;

                for (var i = 0; i < _text.Length; i++)
                {
                    var l1 = Font.GetTextLength(_text, FontSize.X, i);
                    var l2 = TextOffset.X + Font.GetTextLength(_text, FontSize.X, i + 1);

                    if (mouseRelative.X < l1 || mouseRelative.X > l2)
                        continue;

                    _caretIndex = i;
                    break;
                }
            }

            if (dragging)
            {
                _selectionEndIndex = _caretIndex;
            }
            else
            {
                _anchorCaretIndex = _caretIndex;
                _selectionStartIndex = _anchorCaretIndex;
                _selectionEndIndex = _anchorCaretIndex;
            }
        }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
        {
            var tint = EffectiveTint;
            
            //Background
            var rect = Bounds;
            var q = new Quad(rect);
            Game.Renderer.DrawSprite(ref q, Game.Renderer.PixelTextureInfo.TextureId, Game.Renderer.PixelTextureInfo.SourceRect, tint, RenderStyle, Layer);
            if (BorderWidth > 0.0f)
                Game.Renderer.DrawBox(rect, tint * 1.25f, RenderStyle, BorderWidth, Layer);

            //Text
            var textPosition = Position + TextOffset;
            tint = TextTint;
            tint *= Overlay.Opacity;
            Game.Renderer.DrawString(Font, textPosition, FontSize, _text, Alignment, tint, RenderStyle, Layer);

            //Selected Text
            if (IsFocusControl)
            {
                var start = textPosition.X + Font.GetTextLength(_text, FontSize.X, _selectionStartIndex);
                var end = textPosition.X + Font.GetTextLength(_text, FontSize.X, _selectionEndIndex);
                var selectionQuad = new Quad(rect.Top, Math.Min(start, end), rect.Bottom, Math.Max(start, end));
                Game.Renderer.DrawSprite(ref selectionQuad, Game.Renderer.PixelTextureInfo.TextureId, Game.Renderer.PixelTextureInfo.SourceRect, tint * 0.5f, RenderStyle, Layer);
            }

            //Caret
            if (_caretVisible)
            {
                var textWidth = Font.GetTextLength(_text, FontSize.X, _caretIndex);
                q.Left(textPosition.X + textWidth);
                q.Right(textPosition.X + textWidth + 0.1f);

                Game.Renderer.DrawSprite(ref q, Game.Renderer.PixelTextureInfo.TextureId, Game.Renderer.PixelTextureInfo.SourceRect, tint, RenderStyle, Layer);
            }
        }
    }
}
