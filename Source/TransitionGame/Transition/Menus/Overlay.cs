using System;
using System.Collections.Generic;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the base class for hud, menus, etc.
    /// </summary>
    public class Overlay
    {
        public const int NoTabIndex = -1;
        public const int UnspecifiedTabIndex = -2;

        protected int NextTabIndex { get; private set; }

        protected int GetNextTabIndex()
        {
            return NextTabIndex++;
        }
        
        /// <summary>Some arbitrary value used to control whatever you want.</summary>
        /// <remarks>Menu states increase and decrease this as the menu gets and loses focus.
        /// Could be used to control the position or scale or menu items - anything you want really.</remarks>
		public float Value;

		/// <summary>The overlay's opacity.</summary>
        /// <remarks>0 is transparent, 1 is opaque.</remarks>
        public float Opacity = 0.01f;

        /// <summary>The list of controls comprising the menu.</summary>
        protected readonly List<MenuControl> Controls = new List<MenuControl>();
        protected readonly Dictionary<string, MenuControl> ControlMap = new Dictionary<string, MenuControl>();

        protected virtual bool ShouldDraw { get { return true; } }

        /// <summary>A reference to the game object.</summary>
        protected readonly BaseGame Game;

        protected string RegisteredName;

        /// <summary>
        /// Initialises a new instance of an Overlay.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public Overlay(BaseGame game)
        {
            Game = game;
        }

        public void RegisterClone(string name)
        {
            ConfigureOverlay(name);
        }

        public void RegisterClone(Type type)
        {
            ConfigureOverlay(type.Name);
        }

        public void Register(string name = null)
        {
            ConfigureOverlay(name);
        }

        public void ConfigureOverlay(string name = null)
        { 
            Controls.Clear();
            ControlMap.Clear();

            OnInitialise();

            if (name != null)
                RegisteredName = name;

            //Get the configuration from the overlay template manager
            var template = Game.OverlayTemplateManager.GetTemplate(name ?? RegisteredName ?? GetType().Name);

            if (template != null)
            {
                foreach (var packet in template)
                {
                    var menuControl = packet.Value;

                    //Determie the tab index - we incorporate auto tabindex assigning ala HTML forms where appropriate
                    var tabIndex = menuControl.TabIndex;
                    if (tabIndex == UnspecifiedTabIndex)
                        tabIndex = menuControl.AutoTabIndex ? GetNextTabIndex() : NoTabIndex;
                    
                    AddControl(menuControl, tabIndex);
                    ControlMap.Add(packet.Key, menuControl);
                }
            }

            OnConfigureMenu();
        }

        protected virtual void OnInitialise() 
        {
            NextTabIndex = 0;
        }

        protected virtual void OnConfigureMenu() { }

        public TMenuControl GetControl<TMenuControl>(string name) where TMenuControl : MenuControl
        {
            return (TMenuControl)ControlMap[name];
        }

        /// <summary>
        /// Adds a control to the overlay.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="order"></param>
        public void AddControl(MenuControl control, int order)
        {
            control.Overlay = this;
            Controls.Add(control);

            OnControlAdded(control, order);
        }

        public void StartTransition(string name)
        {
            foreach (var control in Controls)
                control.StartTransition(name);
        }

        /// <summary>
        /// Updates the overlay.
        /// </summary>
        public void Update()
        {
            OnUpdateMustCallBase();

            foreach (var control in Controls)
                control.Update();
        }

        /// <summary>
        /// Draws the overlay.
        /// </summary>
        public void Draw()
        {
            if (Opacity <= 0.000001f)
                return;

            if (ShouldDraw == false)
                return;

            OnPreDraw();

            DrawControls();

            //Alow any derived overlays to draw.
            OnDraw();

            OnPostDraw();
        }

        /// <summary>
        /// Draws the controls.
        /// </summary>
        private void DrawControls()
        {
            foreach (var control in Controls)
                control.Draw();
        }

        /// <summary>
        /// Allows a derived control to handle controls being added.
        /// </summary>
        /// <param name="menuControl"></param>
        /// <param name="order"></param>
        protected virtual void OnControlAdded(MenuControl menuControl, int order) { }
        protected virtual void OnUpdateMustCallBase() { }
        protected virtual void OnPreDraw() { }
        protected virtual void OnDraw() { }
        protected virtual void OnPostDraw() { }
    }
}
