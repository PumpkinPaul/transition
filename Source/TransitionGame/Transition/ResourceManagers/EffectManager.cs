using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.ResourceManagers
{
    /// <summary>
    /// A class to manage effect resources.
    /// </summary>
    public class EffectManager : ContentResourceManager
    {
        public override string FolderName { get { return "Effects"; } }

        readonly GraphicsDevice _graphicsDevice;

        /// <summary>Storage for the effects.</summary>
        readonly Dictionary<string, Effect> _effects = new Dictionary<string, Effect>();

        /// <summary>
        /// Creates a new instance of an EffectManager.
        /// </summary>
        public EffectManager(ContentManager contentManager, GraphicsDevice graphicsDevice) : base(contentManager) 
        {
            if (graphicsDevice == null)
                throw new ArgumentNullException(nameof(graphicsDevice));

            _graphicsDevice= graphicsDevice;
        }

        /// <summary>
        /// Loads all shaders in a specified folder.
        /// </summary>
        public override void LoadContent()
        {
            _effects.Clear();

            AddEffect("BasicEffect", new BasicEffect(_graphicsDevice) { TextureEnabled = true, VertexColorEnabled = true });

            var folder = Path.Combine(Platform.ContentFolderName, "Effects");

            var assetName = "FlashEffect";
            AddEffect(assetName, ContentManager.Load<Effect>(Path.Combine(folder, assetName)));

            assetName = "RippleDistortion";
            AddEffect(assetName, ContentManager.Load<Effect>(Path.Combine(folder, assetName)));

            assetName = "CircularGauge";
            AddEffect(assetName, ContentManager.Load<Effect>(Path.Combine(folder, assetName)));

            assetName = "HologramEffect";
            AddEffect(assetName, ContentManager.Load<Effect>(Path.Combine(folder, assetName)));

            assetName = "WobblySineWave";
            AddEffect(assetName, ContentManager.Load<Effect>(Path.Combine(folder, assetName)));
        }

        /// <summary>
        /// Adds an effect.
        /// </summary>
        /// <param name="assetName">The name of the asset to retrieve.</param>
        /// <param name="effect">The effect to add.</param>
        public void AddEffect(string assetName, Effect effect)
        {
            _effects.Add(assetName, effect);
        }

        /// <summary>
        /// Gets an effect.
        /// </summary>
        /// <param name="assetName">The name of the asset to retrieve.</param>
        public Effect GetEffect(string assetName)
        {
            Effect effect;
            _effects.TryGetValue(assetName, out effect);

            return effect;
        }
    }
}
