﻿#if WINDOWS_STOREAPP

    namespace System.IO
    {
        public class File
        {
            public static FileStream OpenRead(string path)
            {
                return new FileStream();
            }

            public static FileStream Create(string path)
            {
                return new FileStream();
            }
        }
    }
#endif
