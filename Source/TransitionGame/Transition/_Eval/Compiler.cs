/*
 * Copyright 2008  Reg Whitton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Transition.Eval
{
	using System;
	
	class Compiler 
    {
		private readonly Tokeniser _tokeniser;
	
		internal Compiler(String expression) 
        {
			_tokeniser = new Tokeniser(expression);
		}
	
		internal Operation Compile() 
        {
			var expression = Compile(null, null, 0, (char) 0, -1);
	
			/*
			 * If expression is a variable name or decimal constant value then we need to put into a NOP operation.
			 */
			if (expression is Operation) 
				return (Operation) expression;
			
			return Operation.NopOperationfactory(expression);
		}
	
		private Object Compile(Object preReadOperand, Operator preReadOperator, int nestingLevel, char endOfExpressionChar, int terminatePrecedence) 
        {
			var operand = preReadOperand ?? GetOperand(nestingLevel);
			var op = preReadOperator ?? _tokeniser.GetOperator(endOfExpressionChar);
	
			while (op != Operator.End) 
            {
				if (op == Operator.Ternary) 
                {
					var operand2 = Compile(null, null, nestingLevel, ':', -1);
					var operand3 = Compile(null, null, nestingLevel, endOfExpressionChar, -1);
					operand = Operation.TenaryOperationFactory(op, operand, operand2, operand3);
					op = Operator.End;
				} 
                else 
                {
					var nextOperand = GetOperand(nestingLevel);
					var nextOperator = _tokeniser.GetOperator(endOfExpressionChar);
					if (nextOperator == Operator.End) 
                    {
						/* We are at the end of the expression */
						operand = Operation.BinaryOperationfactory(op, operand, nextOperand);
						op = Operator.End;
						if (preReadOperator != null && endOfExpressionChar != 0) 
                        {
							/* The bracket also terminates an earlier expression. */
							_tokeniser.PushBack(Operator.End);
						}
					} 
                    else if (nextOperator.Precedence <= terminatePrecedence)
                    {
						/*
						 * The precedence of the following op effectively brings this expression to an end.
						 */
						operand = Operation.BinaryOperationfactory(op, operand, nextOperand);
						_tokeniser.PushBack(nextOperator);
						op = Operator.End;
					} 
                    else if (op.Precedence >= nextOperator.Precedence) 
                    {
						/* The current op binds tighter than any following it */
						operand = Operation.BinaryOperationfactory(op, operand, nextOperand);
						op = nextOperator;
					} 
                    else 
                    {
						/*
						 * The following op binds tighter so compile the following expression first.
						 */
						operand = Operation.BinaryOperationfactory(op, operand,
								Compile(nextOperand, nextOperator, nestingLevel, endOfExpressionChar, op.Precedence));
						op = _tokeniser.GetOperator(endOfExpressionChar);
						if (op == Operator.End && preReadOperator != null && endOfExpressionChar != 0) 
                        {
							/* The bracket also terminates an earlier expression. */
							_tokeniser.PushBack(Operator.End);
						}
					}
				}
			}
			return operand;
		}
	
		private Object GetOperand(int nestingLevel) {
			var operand = _tokeniser.GetOperand();
			if (operand is char && (char)operand == Tokeniser.StartNewExpression) 
            {
				operand = Compile(null, null, nestingLevel + 1, ')', -1);
			} 
            else if (operand is Operator) 
            {
				/* Can get unary operators when expecting operand */
				return Operation.UnaryOperationfactory((Operator) operand, GetOperand(nestingLevel));
			}
			return operand;
		}
	}
}

