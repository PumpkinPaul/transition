using System;
using Microsoft.Xna.Framework;
using Nuclex.Input;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the view profile menu.
    /// </summary>
    public class MedalsMenu : Menu
    {
        //Controls...
        AchievementsControl _achievementsControl;
        ListControl _pageList;
		/// <summary>
        /// Creates a new instance of a HeroesMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
        public MedalsMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            _achievementsControl = new AchievementsControl(Game);
            var pageLabel = new TextLabelControl(Game);
            _pageList = new ListControl(Game);

            _achievementsControl.Font = Theme.Font;
            _achievementsControl.FontSize = Theme.SmallFontSize * 1.1f;
            _achievementsControl.Position = new Vector2(-11.0f, 5.0f);
            _achievementsControl.Tint = Theme.LabelCaptionColor; 
            _achievementsControl.LockedTint = Theme.LabelValueColor;

            _achievementsControl.FontSize = Theme.FontSize;
            _achievementsControl.Position = new Vector2(-15.0f, 4.75f);

            pageLabel.FontSize = Theme.SmallFontSize;
            pageLabel.Position = new Vector2(-0.5f, -5.65f);
            pageLabel.Text = "page";
            pageLabel.Tint = Theme.LabelValueColor;
            pageLabel.Alignment = Alignment.CentreRight;

            _pageList.FontSize = Theme.SmallFontSize;
            _pageList.Position = new Vector2(0.5f, -5.65f);
            for (var i = 1; i < 3; ++i)
                _pageList.AddOption(i);
            _pageList.SelectedIndex = 0;
            _pageList.IncreaseValue += PageChanged;
            _pageList.DecreaseValue += PageChanged;

            AddControl(_achievementsControl, -1);
            AddControl(pageLabel, -1);
            AddControl(_pageList, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        public PlayerProfile Profile
        {
            get { return _achievementsControl.Profile; }
            set { _achievementsControl.Profile = value; }
        }

        /// <summary>
        /// Occurs when a menu is added to the manager.
        /// </summary>
        protected override void OnAddedToManager()
        {
            _pageList.SelectedIndex = 0;
            _achievementsControl.PageIndex = _pageList.SelectedIndex;
            var profileInfo = Game.PlayerProfileManager.GetProfileInfo((ExtendedPlayerIndex)MenuManager.PrimaryControllerId);
            _achievementsControl.Profile = Game.PlayerProfileManager.GetProfile(profileInfo.DisplayName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageChanged(object sender, EventArgs e)
        {
            _achievementsControl.PageIndex = _pageList.SelectedIndex;
        }
    }
}
