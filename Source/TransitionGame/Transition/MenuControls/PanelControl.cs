using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control for containing other controls.
    /// </summary>
    public class PanelControl : MenuControl
    {
        /// <summary>The size of the image in world units.</summary>
        public Vector2 Size;

        /// <summary>Initialises a new instance of an ImageControl.</summary>
        /// <param name = "game">A reference to the game.</param>
        public PanelControl(BaseGame game) : base(game) 
        {
            Size = Vector2.One;
            Tint = Color.White;
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            Size = element.GetAttributeVector2("size");
        }

        /// <summary>
        ///   Gets the control bounds (a rectangle defining the control's limits).
        /// </summary>
        public override Rect Bounds
        {
            get { return new Rect(Position - (Size / 2.0f), Size); }
        }
    }
}