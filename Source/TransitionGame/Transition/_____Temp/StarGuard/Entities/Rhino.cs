﻿using System;
using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Extensions;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Rhino enemy (walks, charges)
    /// </summary>
    [ProtoContract]
    public class Rhino : Enemy
    {
        const float MaxSpeedY = 0.35f;

        public enum Phase
        {
            Thinking,
            Walking,
            Charging,
            Jumping,
        }

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        Phase _phase;

        [ProtoMember(2)]
        int _countdownTicks;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here
        float _walkSpeed;// = 0.015f;
        float _chargeSpeed;// = 0.0395f;
        float _maxSpeedX;
        float _jumpSpeed;// = 0.275f;
        float _chanceOfThinking;// = 0.3f;
        float _runAndTurnThreshold;// = 3.0f;

        //----------------------------------------------------------------------------------------------------

        public Rhino(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _phase = Phase.Thinking;
            _countdownTicks = 0;
           
            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() 
        { 
            RandomHelper.FastRandom.Reinitialise(Seed);
            _walkSpeed = RandomHelper.FastRandom.NextFloat(0.015f, 0.02f);
            _chargeSpeed = RandomHelper.FastRandom.NextFloat(0.045f, 0.055f);
            _jumpSpeed = RandomHelper.FastRandom.NextFloat(0.29f, 0.31f);
            _chanceOfThinking = RandomHelper.FastRandom.NextFloat(0.25f, 0.35f);
            _runAndTurnThreshold = RandomHelper.FastRandom.Next(1, 4);

            _maxSpeedX = _chargeSpeed * 2;
        }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        bool CanSeePlayer
        {
            get
            {
                var bounds = GetBoundingBox();

                var player = Game.World.Get<Player>(EntityKeys.Player);
                var playerBounds = player.GetBoundingBox();

                return bounds.Y.FloatEquals(playerBounds.Y, 0.1f);
            }
        }

        void BeginThinkingPhase()
        {
            //Stop
            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-10.face-right.animation")
                : Game.AnimationManager.Get("enemy-10.face-left.animation"));

            _phase = Phase.Thinking;
            _countdownTicks = RandomHelper.Random.GetInt(60, 120);
            Velocity.X = 0.0f;
        }

        void BeginWalkingPhase()
        {
            _countdownTicks = RandomHelper.Random.GetInt(60, 120);

            if (_phase != Phase.Walking)
            {
                var previousVelocityX = Velocity.X;
                if (RandomHelper.Random.GetFloat() <= 0.5)
                    Velocity.X = _walkSpeed;
                else
                    Velocity.X = -_walkSpeed;

                if (previousVelocityX.FloatEquals(Velocity.X) == false)
                    IsFacingRight = Velocity.X > 0;

                Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-10.walk-right.animation")
                    : Game.AnimationManager.Get("enemy-10.walk-left.animation"));
            }

            _phase = Phase.Walking;
        }

        void BeginChargingPhase()
        {
            _phase = Phase.Charging;

            Sprites[0].SetAppearance(IsFacingRight
                ? Game.AnimationManager.Get("enemy-10.charge-right.animation")
                : Game.AnimationManager.Get("enemy-10.charge-left.animation"));
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            switch (_phase)
            {
                case Phase.Thinking:
                    UpdateThinkingPhase();
                    break;

                case Phase.Walking:
                    UpdateWalkingPhase();
                    break;

                case Phase.Charging:
                    UpdateChargingPhase();
                    break;

                case Phase.Jumping:
                    UpdateJumpingPhase();
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(_phase), $"Unknown _phase: {_phase}");
            }

            Velocity.Y += TestPhaseStarGuard.Gravity;

            Velocity.X = MathHelper.Clamp(Velocity.X, -_maxSpeedX, _maxSpeedX);
            Velocity.Y = MathHelper.Clamp(Velocity.Y, -MaxSpeedY, MaxSpeedY);

            Game.GameMap.HandleCollisions(this);
        }

        void UpdateThinkingPhase()
        {
            if (CanSeePlayer)
            {
                BeginChargingPhase();
                return;
            }

            _countdownTicks--;
            if (_countdownTicks <= 0)
            {
                BeginWalkingPhase();
            }
        }

        void UpdateWalkingPhase()
        {
            if (CanSeePlayer)
            {
                BeginChargingPhase();
                return;
            }

            _countdownTicks--;
            if (_countdownTicks > 0)
                return;

            //Countdown expired - do something!

            //Only a 30% chance we'll stop and think
            if (RandomHelper.Random.GetFloat() <= _chanceOfThinking)
            {
                BeginThinkingPhase();
            }
            else
            {
                //Reset the walking phase
                BeginWalkingPhase();
            }
        }

        void UpdateChargingPhase()
        {
            var player = Game.World.Get<Player>(EntityKeys.Player);

            var previousVelocity = Velocity;

            //Determine if the zomboid should turn around 
            //+ve player is to right of zomboid
            //-ve player is to left of zomboid
            var positionDelta = player.Position.X - Position.X; 
            var turnThreshold = 0.0f;
            if ((IsFacingLeft && positionDelta > 0) || (IsFacingRight && positionDelta < 0))
                turnThreshold = _runAndTurnThreshold;

            if (player.Position.X - turnThreshold > Position.X)
            {
                Velocity.X = _chargeSpeed;
                IsFacingRight = true;
            }
            else if (player.Position.X + turnThreshold < Position.X)
            {
                Velocity.X = -_chargeSpeed;
                IsFacingRight = false;
            }

            if (previousVelocity.X.FloatEquals(Velocity.X) == false)
            {
                Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-10.charge-right.animation")
                    : Game.AnimationManager.Get("enemy-10.charge-left.animation"));
            }

            //We should jump if we are blocked by a wall in the direction we are travelling
            var directionSign = Math.Sign(Velocity.X);
            var lookAheadOffset = new Vector2(directionSign * GameMap.TileSize / 2.0f, 0.0f);
            var tileCollision = Game.GameMap.GetTileCollision(Position + new Vector2(directionSign * CollisionBounds.Width / 2, 0) + lookAheadOffset);
            if (tileCollision == GameMap.TileCollision.Impassable)
            {
                Jump();
            }

            //Jumping off platforms!
            var myFloor = Game.GameMap.GetTileCoordinates(Position);
            myFloor.Y--;

            var targetCoords = myFloor;
            if (Velocity.X > 0)
            {
                targetCoords.X++;
                if (Game.GameMap.GetTileCollision(myFloor) == GameMap.TileCollision.Passable && Game.GameMap.GetTileCollision(targetCoords) == GameMap.TileCollision.Passable)
                {
                    Jump();
                }
            }
            else if (Velocity.X < 0)
            {
                targetCoords.X--;
                if (Game.GameMap.GetTileCollision(myFloor) == GameMap.TileCollision.Passable && Game.GameMap.GetTileCollision(targetCoords) == GameMap.TileCollision.Passable)
                {
                    Jump();
                }
            }
        }

        void UpdateJumpingPhase()
        {
            if (OnGround)
            {
                //Imediately target the player on landing!
                var player = Game.World.Get<Player>(EntityKeys.Player);
                if (player.Position.X > Position.X && IsFacingLeft)
                    TurnAround();
                else if (player.Position.X < Position.X && IsFacingRight)
                    TurnAround();

                BeginChargingPhase();
            }
        }

        void Jump()
        {
            _phase = Phase.Jumping;
            Velocity.Y += _jumpSpeed;

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-10.face-right.animation")
                    : Game.AnimationManager.Get("enemy-10.face-left.animation"));
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            if ((entityCollisionEdge & CollisionEdge.Left) != 0)
            {
                Velocity.X = BounceSpeed;
                IsFacingRight = true;
                if (_phase == Phase.Walking)
                    Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-10.walk-right.animation"));
                else if (_phase == Phase.Charging)
                    Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-10.charge-right.animation"));
            }
            else if ((entityCollisionEdge & CollisionEdge.Right) != 0)
            {
                Velocity.X = -BounceSpeed;
                IsFacingRight = false;
                if (_phase == Phase.Walking)
                    Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-10.walk-left.animation"));
                else if (_phase == Phase.Charging)
                    Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-10.charge-left.animation"));
            }
        }

        void TurnAround()
        {
            IsFacingRight = !IsFacingRight;
        }

        float BounceSpeed
        {
            get 
            {
                switch (_phase)
                {
                    case Phase.Walking: 
                        return _walkSpeed;

                    case Phase.Charging:
                        return _chargeSpeed * 2.0f;

                    default:
                        return 0.0f;
                }
            }
        }
    }
}