float4x4 World;
float4x4 View;
float4x4 Projection;
float2 Viewport;

float4 ftransform(float4 gl_Position)
{
	float4 worldPosition = mul(gl_Position, World);
    float4 viewPosition = mul(worldPosition, View);
    return mul(viewPosition, Projection);
}

float4 ftransform2(float4 gl_Position)
{
	float4 pos = gl_Position;
	// Half pixel offset for correct texel centering.
	pos.xy = gl_Position.xy -= 0.5;
    // Viewport adjustment.
	pos.xy = gl_Position.xy / Viewport;
	pos.xy *= float2(2, -2);
	pos.xy -= float2(1, -1);

	return pos;
}