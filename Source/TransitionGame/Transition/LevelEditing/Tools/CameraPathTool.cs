using Microsoft.Xna.Framework;
using Transition.Input;
using Transition.Rendering;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class CameraPathTool : Tool
    {
        bool _controlling;
        Vector2 _anchorPosition;

        int _hoverNodeId = -1;

        public override string CursorName { get { return "ToolboxCameraPath.png"; } }

        public CameraPathTool(TransitionGame game) : base(game) { }

        public override void Update()
        {
            //Are we over a control point?
            var nodeSize = new Vector2(0.75f);

            if (MouseHelper.IsLeftButtonUp && _controlling == false)
            {
                ToolTip = "Add a new node";
                _hoverNodeId = -1;

                for (var index = 0; index < LevelEditor.Course.CameraPath.Vertices.Count; index++)
                {
                    //Skip real points as we are only interested in control points
                    if (index % 3 == 0)
                        continue;

                    var node = LevelEditor.Course.CameraPath.Vertices[index];
                    var r = new Box2(node - nodeSize / 2.0f, nodeSize);

                    if (r.Contains(LevelEditor.WorldPosition) == false)
                        continue;
            
                    ToolTip = "Move control point";    
                    _hoverNodeId = index;
                    break;
                }
            }

            if (_hoverNodeId != -1)
                UpdateSelectNode();
           else
                UpdateAddNode();
        }

        void UpdateSelectNode()
        {
            if (MouseHelper.IsLeftButtonJustDown && MouseHelper.IsMouseInWindow)
            {
                if (_hoverNodeId != -1)
                {
                    _controlling = true;
                    _anchorPosition = LevelEditor.Course.CameraPath.Vertices[_hoverNodeId];
                }
            }
            else if (MouseHelper.IsLeftButtonDown && _controlling)
            {
                //Move the active control point...
                var delta = (LevelEditor.ObjectPosition - _anchorPosition);

                LevelEditor.Course.CameraPath.Vertices[_hoverNodeId] += delta;

                //...and do its twin (the one that sits the other side of the real node) by the inverse delta.
                var twinId = -1;
                if ((_hoverNodeId + 1) % 3 == 0)
                    twinId = _hoverNodeId + 2;
                else if ((_hoverNodeId - 1) % 3 == 0)
                    twinId = _hoverNodeId - 2;

                if (twinId > 0 && twinId < LevelEditor.Course.CameraPath.Vertices.Count)
                    LevelEditor.Course.CameraPath.Vertices[twinId] -= delta;

                _anchorPosition = LevelEditor.ObjectPosition;
            }
            else if (MouseHelper.IsLeftButtonJustUp && _controlling)
            {
                _controlling = false;

                LevelEditor.SaveEditorState("Move control point");
            }
        }

        void UpdateAddNode()
        {
            if (MouseHelper.IsLeftButtonJustDown && LevelEditor.IsMouseOverCanvas)
            {
                var lastPoint = LevelEditor.Course.CameraPath.Vertices[LevelEditor.Course.CameraPath.Vertices.Count - 1];
                var previousControlPoint = LevelEditor.Course.CameraPath.Vertices[LevelEditor.Course.CameraPath.Vertices.Count - 2];

                var lastDelta = lastPoint - previousControlPoint;

                var newPosition = LevelEditor.ObjectPosition;

                //Add new control point 1
                LevelEditor.Course.CameraPath.Vertices.Add(lastPoint + lastDelta);

                //Add new control point 2
                LevelEditor.Course.CameraPath.Vertices.Add(newPosition - lastDelta);

                //Add point
                LevelEditor.Course.CameraPath.Vertices.Add(newPosition);

                //Select the new node.
                LevelEditorState.ClearSelection();
                LevelEditorState.SelectedNodeIds.Add(LevelEditor.Course.CameraPath.Vertices.Count - 1);
                LevelEditor.SaveEditorState("Add camera path");
            }
        }

        public override void Draw()
        {
            if (_hoverNodeId != -1)
            {
                var textureInfo = Game.TextureManager.GetTextureInfo("BezierPoint");
                var q = new Quad(LevelEditor.Course.CameraPath.Vertices[_hoverNodeId], new Vector2(0.75f), 0);
                Game.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, Theme.Current.GetColor(Theme.Colors.Focus), TransparentRenderStyle.Instance);
            }
        }
    }
}
