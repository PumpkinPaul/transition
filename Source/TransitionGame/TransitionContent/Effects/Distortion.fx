#include "CommonPuppy.inc"

sampler distortion : register(s0);

/*
	Vertex shader
	in: strength
	out: strengthOut
*/
void distortion_glvert(inout float4 gl_Position : POSITION0, inout float2 gl_TexCoord : TEXCOORD0, in float strength : FOG, out float2 strengthOut : TEXCOORD1)
{
    gl_Position = ftransform2(gl_Position);
	gl_TexCoord = gl_TexCoord;
	strengthOut = strength;
}

/*
	Distortion fragment shader, for drawing distortion maps
	requires: TEXCOORD0, distortion
	in: strengthOut
*/
float4 distortion_glfrag(float2 gl_TexCoord : TEXCOORD0, float strengthOut : TEXCOORD1) : COLOR0
{
	float2 offset = tex2D(distortion, gl_TexCoord.xy).rg;
    offset = (offset - float2(0.5, 0.5)) * strengthOut.x * 2.0;
		
    return float4(offset, 0, 1);
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 distortion_glvert();

		#if SM4
		PixelShader = compile ps_4_0_level_9_1 distortion_glfrag();
		#elif SM3
		PixelShader = compile ps_3_0 distortion_glfrag();
		#else
		PixelShader = compile ps_2_0 distortion_glfrag();
		#endif
    }
}