#if LINUX
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Doppelganger
{
	static class ProgramMac
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main (string[] args)
		{
			#if DEBUG
				Environment.CurrentDirectory = new FileInfo(System.Reflection.Assembly.GetEntryAssembly().Location).Directory.FullName;
			#endif

			using (var game = new DoppelgangerGame ()) 
				game.Run ();
		}
	}
}
#endif


