using Microsoft.Xna.Framework;
using Transition.CourseSystem;
using Transition.Input;
using Transition.Rendering;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class TileTool : Tool
    {
        public override string CursorName { get { return "ToolboxTiles.png"; } }

        public int SelectedTileId { get; set; }

        string SelectedTileName { get { return "Tile" + SelectedTileId; } }

        readonly Vector2 _defaultTileScale = new Vector2(4.0f);

        public TileTool(TransitionGame game) : base(game) { }

        public override void Update()
        {
            if (MouseHelper.IsLeftButtonJustDown && LevelEditor.IsMouseOverCanvas)
            {
                var tile = new Tile 
                {
                    Color = LevelEditor.ObjectColor, 
                    Position = LevelEditor.ObjectPosition, 
                    Rotation = 0, 
                    Scale = _defaultTileScale, 
                    Texture = SelectedTileName 
                };

                LevelEditor.Course.Tiles.Add(tile);

                //Select the new tile.
                LevelEditorState.ClearSelection();
                LevelEditorState.SelectedTileIds.Add(LevelEditor.Course.Tiles.Count - 1);
                LevelEditor.SaveEditorState("Add new tile");
            }  
        }

        public override void Draw()
        {
            var textureInfo = Game.TextureManager.GetTextureInfo(SelectedTileName);
            var renderStyle = TransparentRenderStyle.Instance;
            var color = LevelEditor.ObjectColor;

            var tileQuad = new Quad(LevelEditor.ObjectPosition, _defaultTileScale, 0);
            Game.Renderer.DrawSprite(ref tileQuad, textureInfo.TextureId, textureInfo.SourceRect, color, renderStyle);
        }
    }
}
