using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public struct ParticleEffect
	{	
        public ParticleSystemType Type;
		public ParticleSystemTemplate Template;
		public float Delay;
		public float Lifetime;
		public Vector2 Offset;
	}
}
