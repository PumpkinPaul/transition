{
  "InputKeys": {
    "Up": 38,
    "Down": 40,
    "Left": 37,
    "Right": 39,
    "Fire": 88,
    "Pause": 80,
    "UpKeyHint": "Up",
    "DownKeyHint": "Down",
    "LeftKeyHint": "Left",
    "RightKeyHint": "Right",
    "SelectKeyHint": "X",
    "FireKeyHint": "X",
    "PauseKeyHint": "P"
  },
  "Checkpoints": [],
  "FileName": "1BC52086-0451-4c75-81BD-47042DEF3195",
  "Name": "Player 1",
  "Guid": "1BC52086-0451-4c75-81BD-47042DEF3195",
  "ProfileType": 0,
  "SelectedShipName": "Ship1",
  "CompletedAchievements": [],
  "RequiresSave": false
}