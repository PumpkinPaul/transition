using ProtoBuf;

namespace Transition.ActorDatas
{
	/// <summary>
	/// Configuration data surrogate for serialization.
	/// </summary>
    [ProtoContract]
    public class EnemyBulletDataSurrogate
    {
        [ProtoMember(1)]
        public string Name;

        static readonly EnemyBulletDataSurrogate Instance = new EnemyBulletDataSurrogate();

        public static implicit operator EnemyBulletDataSurrogate(EnemyBulletData value)
        {
            Instance.Name = value.Name;
            return Instance;
        }

        public static implicit operator EnemyBulletData(EnemyBulletDataSurrogate value)
        {
            return TransitionGame.Instance.DataManager.EnemyBulletData(value.Name);
        }
    }
}
