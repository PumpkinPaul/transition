namespace Transition.GamePhases
{
    /// <summary>
    /// Handles waiting in a menu for the game to start.
    /// </summary>
    public class MenuGamePhase : GamePhase 
    { 
        public MenuGamePhase(BaseGame game) : base(game) { }
    }
}

