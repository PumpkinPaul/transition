using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Nuclex.Input;
using Transition.Input;

namespace Transition
{
    /// <summary>
    /// Manages controller vibrations.
    /// </summary>
    public class VibrationManager
    {
        private const int MaxVibrations = 10;

        /// <summary>.</summary>
        private readonly List<Vibration> _leftVibrations = new List<Vibration>(VibrationManager.MaxVibrations);
        private readonly List<Vibration> _rightVibrations = new List<Vibration>(VibrationManager.MaxVibrations);
		
		/// <summary>.</summary>
		private readonly float[] _leftPower = new float[GamePadHelper.MaxXInputGamePads];
        private readonly float[] _rightPower = new float[GamePadHelper.MaxXInputGamePads];

        public bool Enabled;
        public bool SupportsVibration;
        public bool DemoPlaying;

		/// <summary>
		/// Adds a vibration to the left motor.
		/// </summary>
        /// <param name="playerIndex">The index of the player to ad the vibration for.</param>
        /// <param name="power">The severity of the left motor.</param>
        /// <param name="duration">The number of ticks to vibrate for.</param>
		public void AddLeftVibration(ExtendedPlayerIndex playerIndex, float power, int duration)
		{
            if (CanAddVibration(playerIndex) == false)
                return;

		    var vibration = new Vibration((int)playerIndex, power, duration, power / duration);
			_leftVibrations.Add(vibration);
		}

        /// <summary>
        /// Adds a vibration to the right motor.
        /// </summary>
        /// <param name="playerIndex">The index of the player to ad the vibration for.</param>
        /// <param name="power">The severity of the right motor.</param>
        /// <param name="duration">The number of ticks to vibrate for.</param>
        public void AddRightVibration(ExtendedPlayerIndex playerIndex, float power, int duration)
        {
            if (CanAddVibration(playerIndex) == false)
                return;

            var vibration = new Vibration((int)playerIndex, power, duration, power / duration);
            _rightVibrations.Add(vibration);
        }

        private bool CanAddVibration(ExtendedPlayerIndex playerIndex)
        {
            if (Enabled == false)
                return false;

            if (DemoPlaying)
                return false;

            if (SupportsVibration == false)
                return false;

            if ((int)playerIndex > (int)PlayerIndex.Four)
                return false;

            return true;
        }

        /// <summary>
		/// .
		/// </summary>
        public void Update()
        {
            if (Enabled == false)
            {
                _leftVibrations.Clear();
                _rightVibrations.Clear();
                return;
            }

            UpdateLeftMotor();
            UpdateRightMotor();
            SetVibration();
        }
		
		/// <summary>
		/// .
		/// </summary>
		private void UpdateLeftMotor()
		{
			//Calculate how much to 'vibrate'.
			_leftPower[0] = 0.0f;
            _leftPower[1] = 0.0f;
            _leftPower[2] = 0.0f;
            _leftPower[3] = 0.0f;

			for (var i = 0; i < _leftVibrations.Count; ++i)
			{
                var vibration = _leftVibrations[i];
				//If the current vibration has finished remove it from the list
                if (vibration.Duration == 0) 
				{
                    _leftVibrations.RemoveAt(i);
					--i;
				}
				//Add the vibration amount to the total.
				else 
				{
                    --vibration.Duration;
                    _leftPower[vibration.PlayerId] += vibration.Power;
                    vibration.Power -= vibration.PowerDelta;
				}
			}                
		}

        /// <summary>
        /// .
        /// </summary>
        private void UpdateRightMotor()
        {
            //Calculate how much to 'vibrate'.
            _rightPower[0] = 0.0f;
            _rightPower[1] = 0.0f;
            _rightPower[2] = 0.0f;
            _rightPower[3] = 0.0f;

            for (var i = 0; i < _rightVibrations.Count; ++i)
            {
                var vibration = _rightVibrations[i];
                //If the current vibration has finished remove it from the list
                if (vibration.Duration == 0)
                {
                    _rightVibrations.RemoveAt(i);
                    --i;
                }
                //Add the vibration amount to the total.
                else
                {
                    --vibration.Duration;
                    _rightPower[vibration.PlayerId] += vibration.Power;
                    vibration.Power -= vibration.PowerDelta;
                }
            }
        }

        private void SetVibration()
        {
            #if !WINDOWS_STOREAPP
                GamePad.SetVibration(PlayerIndex.One, _leftPower[0], _rightPower[0]);
                GamePad.SetVibration(PlayerIndex.Two, _leftPower[1], _rightPower[1]);
                GamePad.SetVibration(PlayerIndex.Three, _leftPower[2], _rightPower[2]);
                GamePad.SetVibration(PlayerIndex.Four, _leftPower[3], _rightPower[3]);
            #endif
        }
    }
}
