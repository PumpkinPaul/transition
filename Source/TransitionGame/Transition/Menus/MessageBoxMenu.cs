using System;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the message box menu.
    /// </summary>
    public class MessageBoxMenu : Menu
    {
        /// <summary>
        /// Represent the possible response from the player.
        /// </summary>
        public enum MessageBoxResult
        {
            Yes = 0,
            No
        }

        //Controls...
        TextLabelControl _headingLabel;
        TextLabelControl _questionLabel;
        TextButtonControl _yesButton;
        TextButtonControl _noButton;

        /// <summary>The last response from the player.</summary>
        public static MessageBoxResult Result;

        /// <summary>
        /// Initialises a new instance of a MessageBoxMenu.
        /// </summary>
		/// <param name="game">A reference to the game.</param>
		/// <param name="title">The </param>
        public MessageBoxMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            _headingLabel = GetControl<TextLabelControl>("headingLabel");
            _questionLabel = GetControl<TextLabelControl>("questionLabel");
            _yesButton = GetControl<TextButtonControl>("yesButton");
            _noButton = GetControl<TextButtonControl>("noButton");

            _yesButton.Click += YesButtonClick;
            _yesButton.CanIncreaseDecrease = true;
            _yesButton.IncreaseValue += (sender, args) => MenuManager.SetFocus(_noButton, false);

            _noButton.Click += NoButtonClick;
            _noButton.CanIncreaseDecrease = true;
            _noButton.DecreaseValue += (sender, args) => MenuManager.SetFocus(_yesButton, false);
        }

        /// <summary>
        /// Gets or sets the message to display.
        /// </summary>
        public string Heading
        {
            get { return _headingLabel.Text; }
            set { _headingLabel.Text = value; }
        }

        /// <summary>
        /// Gets or sets the message to display.
        /// </summary>
        public string Message
        {
            get { return _questionLabel.Text; }
            set { _questionLabel.Text = value; }
        }

        /// <summary>
        /// Gets or sets the text to display for the yes button.
        /// </summary>
        public string YesButtonText
        {
            get { return _yesButton.Text; }
            set { _yesButton.Text = value; }
        }

        /// <summary>
        /// Gets or sets the text to display for the no button.
        /// </summary>
        public string NoButtonText
        {
            get { return _noButton.Text; }
            set { _noButton.Text = value; }
        }

        /// <summary>
        /// Gets or sets whether to show the yes button.
        /// </summary>
        public bool ShowYesButton
        {
            get { return _yesButton.Visible; }
            set { _yesButton.Visible = value; }
        }

        /// <summary>
        /// Gets or sets whether to show the no button.
        /// </summary>
        public bool ShowNoButton
        {
            get { return _noButton.Visible; }
            set { _noButton.Visible = value; }
        }

        /// <summary>
        /// Called when the 'back' button is pressed on the controller and the menu accepts the back command.
        /// </summary>
        /// <returns>True if the menu should play a sound effect; otherwise false.</returns>
        protected override bool OnBack()
        {
            MessageBoxMenu.Result = MessageBoxResult.No;

            return base.OnBack();
        }

        /// <summary>
        /// Event handler for the yes button click.
        /// </summary>
		private void YesButtonClick(object sender, EventArgs e)
		{
            MessageBoxMenu.Result = MessageBoxResult.Yes;
			Close();
		}

        /// <summary>
        /// Event handler for the no button click.
        /// </summary>
        private void NoButtonClick(object sender, EventArgs e)
        {
            MessageBoxMenu.Result = MessageBoxResult.No;
            Response = MenuResponse.Cancel;
            Close();
        }
    }
}
