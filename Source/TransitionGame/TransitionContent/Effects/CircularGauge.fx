float4x4 World;
float4x4 View;
float4x4 Projection;
uniform extern texture UserTexture;

sampler baseSampler = sampler_state
{
    Texture = <UserTexture>;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    ADDRESSU = CLAMP;
    ADDRESSV = CLAMP;
};

// TODO: add effect parameters here.
 
struct VS_IN
{
    float4 Position: POSITION;
	float4 Colour: COLOR;
    float2 TextureCoords: TEXCOORD0;
	float2 Value: TEXCOORD1;
};

struct VS_OUT 
{
   float4 ScreenPosition:   POSITION;
   float4 Colour: COLOR;
   float2 TextureCoords: TEXCOORD0;
   float2 Value : TEXCOORD1;
};


VS_OUT VertexShaderFunction(VS_IN input)
{
    VS_OUT output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.ScreenPosition = mul(viewPosition, Projection);
	output.Colour = input.Colour;
	output.TextureCoords = input.TextureCoords;
	output.Value = input.Value;

    return output;
}

sampler GradientSampler : register(s2);

const float threshold = 0.01;
const float empty = 0.25;

float4 PixelShaderFunction(float4 color : COLOR0, float2 texCoord : TEXCOORD0, float2 v : TEXCOORD1) : COLOR0
{
	float value = v.x;

	float4 gradient = tex2D(GradientSampler, texCoord);
	float4 mask = tex2D(baseSampler, texCoord);

	if (gradient.a > value)
		mask.rgba *= 0.25;

	return mask * color;

	if (gradient.a > value + threshold)
	{
		mask.rgba *= empty;
	}
	else if (gradient.a > value)
	{
		float v = (gradient.a - value) / threshold;
		mask.rgba *= lerp(1.0, 0.25, v);
	}
	
    return mask * color;
}

technique Technique1
{
    pass P0
	{
		#if SM4
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
		//#elif SM3
		#else
		//PixelShader = compile ps_3_0 PixelShaderFunction();
		//#else
		PixelShader = compile ps_2_0 PixelShaderFunction();
		#endif

		VertexShader = compile vs_2_0 VertexShaderFunction();
	}
}
