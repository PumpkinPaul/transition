using System;

namespace Transition.PostProcess
{
    /// <summary>
    /// A class for making noise on the fly
    /// </summary>
    class PerlinNoise 
    {
	    private readonly InterpolatorBase _interpolator; // Used for interpolated noise.

	    // Perlin noise parameters
	    private readonly float _persistence;
	    private readonly int _octaves;
	    private readonly int _scale;
	    private readonly int _seed;

	    public PerlinNoise(int seed, int scale, int octaves, float persistence) : 
		    this(seed, scale, octaves, persistence, LinearInterpolator.Instance) { }

	    public PerlinNoise(int seed, int scale, int octaves, float persistence, InterpolatorBase i) 
        {
		    _seed = seed;
		    _scale = scale;
		    _octaves = octaves;
		    _persistence = persistence;
		    _interpolator = i;
	    }

	    /// <summary>
	    /// Get the Perlin noise at a particular point.
	    /// </summary>
	    /// <param name="x"></param>
	    /// <param name="y"></param>
	    /// <returns></returns>
	    public float FireNoise(int x, int y) 
        {
		    var total = 0.0f;
		    var amplitude = _persistence;
		    var frequency = 1;

		    for (var i = 0; i < _octaves; i++) 
            {
			    total += Math.Abs(InterpolatedNoise(x * frequency, y * frequency)) * amplitude;
			    amplitude /= 2.0f;
			    frequency <<= 1;
		    }
		    return total;
	    }

	    /// <summary>
	    /// Returns interpolated noise (i.e. noise using fractional coordinates).
	    /// </summary>
	    /// <param name="x"></param>
	    /// <param name="y"></param>
	    /// <returns></returns>
	    public float InterpolatedNoise(int x, int y) 
        {
		    var iX = x / _scale;
		    var fX = x % _scale / (float) _scale;
		    var iY = y / _scale;
		    var fY = y % _scale / (float) _scale;

		    var v1 = GetNoise(iX, iY);
		    var v2 = GetNoise(iX + 1, iY);
		    var v3 = GetNoise(iX, iY + 1);
		    var v4 = GetNoise(iX + 1, iY + 1);
		    var i1 = _interpolator.Interpolate(v1, v2, fX);
		    var i2 = _interpolator.Interpolate(v3, v4, fX);
		    return _interpolator.Interpolate(i1, i2, fY);
	    }

	    /// <summary>
	    /// Return the raw noise at a particular point
	    /// </summary>
	    /// <param name="x"></param>
	    /// <param name="y"></param>
	    /// <returns></returns>
	    public float GetNoise(int x, int y) 
        {
		    var n = x + y * (57 + _seed);
		    n = n << 13 ^ n;
		    return 1.0f - (n * (n * n * 15731 + 789221) + 1376312589 & 0x7fffffff) / 1073741824.0f;
	    }

	    /// <summary>
	    /// Get the Perlin noise at a particular point.
	    /// </summary>
	    /// <param name="x"></param>
	    /// <param name="y"></param>
	    /// <returns></returns>
	    public float GetPerlinNoise(int x, int y) 
        {
		    var total = 0.0f;
		    var amplitude = 1.0f;
		    var frequency = 1;

		    for (var i = 0; i < _octaves; i++) 
            {
			    total += InterpolatedNoise(x * frequency, y * frequency) * amplitude;
			    amplitude *= _persistence;
			    frequency <<= 1;
		    }
		    return total;
	    }

	    /// <summary>
	    /// Returns interpolated noise
	    /// </summary>
	    /// <param name="x"></param>
	    /// <param name="y"></param>
	    /// <returns></returns>
	    public float SmoothInterpolatedNoise(int x, int y) 
        {
		    var iX = x / _scale;
		    var fX = x % _scale / (float) _scale;
		    var iY = y / _scale;
		    var fY = y % _scale / (float) _scale;

		    var v1 = SmoothNoise(iX, iY);
		    var v2 = SmoothNoise(iX + 1, iY);
		    var v3 = SmoothNoise(iX, iY + 1);
		    var v4 = SmoothNoise(iX + 1, iY + 1);
		    var i1 = _interpolator.Interpolate(v1, v2, fX);
		    var i2 = _interpolator.Interpolate(v3, v4, fX);
		    return _interpolator.Interpolate(i1, i2, fY);
	    }

	    public float SmoothNoise(int x, int y) 
        {
		    var corners = (GetNoise(x - 1, y - 1) + GetNoise(x + 1, y - 1) + GetNoise(x - 1, y + 1) + GetNoise(x + 1, y + 1)) / 16.0f;
		    var sides = (GetNoise(x - 1, y) + GetNoise(x + 1, y) + GetNoise(x, y - 1) + GetNoise(x, y + 1)) / 8.0f;
		    var center = GetNoise(x, y) / 4.0f;
		    return corners + sides + center;
	    }
    }
}