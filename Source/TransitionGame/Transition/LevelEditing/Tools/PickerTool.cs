using Microsoft.Xna.Framework;
using Transition.Input;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class PickerTool : Tool
    {
        public override string CursorName { get { return "ToolboxColorPicker.png"; } }

        public PickerTool(TransitionGame game) : base(game) { }

        public override void Update()
        {
            if (MouseHelper.IsLeftButtonJustDown) 
            {
                var picked = false;
                var currentColor = LevelEditor.ObjectColor;

                var color = LevelEditor.Course.BackgroundColor;

                //Try labels first...
                for (var index = LevelEditor.Course.Labels.Count - 1; index >= 0; index--)
                {
                    var label = LevelEditor.Course.Labels[index];

                    var labelBounds = LevelEditor.Course.LabelFont.GetBounds(label.Position, label.FontSize, label.Text, label.Alignment);

                    if (labelBounds.Contains(Game.LevelEditor.WorldPosition, label.Rotation) == false)
                        continue;

                    color = label.Color;
                    picked = true;
                    break;
                }

                //...then tiles
                if (picked == false)
                {
                    for (var index = LevelEditor.Course.Tiles.Count - 1; index >= 0; index--)
                    {
                        var tile = LevelEditor.Course.Tiles[index];

                        var halfScale = tile.Scale / 2.0f;

                        var bottomLeft = new Vector2(tile.Position.X - halfScale.X, tile.Position.Y - halfScale.Y);
                        var box2 = new Box2(bottomLeft, tile.Scale);

                        if (box2.Contains(Game.LevelEditor.WorldPosition) == false)
                            continue;

                        color = tile.Color;

                        break;
                    }
                }

                if (color != currentColor)
                    LevelEditor.ApplyObjectColorToSelection(color, saveState: true);
            }
        }

        public override void Draw() { }
    }
}
