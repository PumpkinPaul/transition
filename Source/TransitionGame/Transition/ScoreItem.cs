using System;

namespace Transition
{
	/// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ScoreItem
    {
        public int Rank { get; set; }
        public int Score { get; set; }
        public int Ticks { get; set; }
        public int Level { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public string DisplayScore { get; set; }
        public string DisplayTime { get; set; }
        public string DisplayLevel { get; set; }
        public string LevelName { get; set; }
        
		/// <summary>
		/// 
		/// </summary>
        public bool IsNewItemBetter(ScoreItem newItem)
		{
		    if ((newItem.Score > Score) || ((newItem.Score == Score) && (newItem.Ticks < Ticks)))
			    return true;
		    
            return false;
		}

	    /// <summary>
		/// 
		/// </summary>
        public static int Compare(ScoreItem item1, ScoreItem item2)
	    {
	        if ((item1.Score > item2.Score) || ((item1.Score == item2.Score) && (item1.Ticks < item2.Ticks)))
                return -1;
	
            return 1;
	    }
    }
}
