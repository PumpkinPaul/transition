using Microsoft.Xna.Framework;

namespace Transition.Input
{
    /// <summary>
    /// Menu navigation input actions
    /// </summary>
    public struct MenuInput
    {
        public bool BackJustPressed;

        public bool SelectPressed;
        public bool SelectJustPressed;

        public bool SelectJustReleased;

        public bool StartPressed;
        public bool StartJustPressed;

        public bool PreviousControlPressed;
        public bool NextControlPressed;

        public bool IncreaseValuePressed;
        public bool DecreaseValuePressed;

        public bool XJustPressed;
        public bool YJustPressed;

        public Vector2 PointerPosition;
    }
}