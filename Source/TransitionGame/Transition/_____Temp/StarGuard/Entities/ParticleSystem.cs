﻿using System;
using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Rendering;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Rewindable Particle System with limited state.
    /// </summary>
    [ProtoContract]
    public class ParticleSystem : Entity
    {
        const int MaxTicks = 60;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        int _ticks;

        [ProtoMember(2)]
        public string TemplateName;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here
        //int _seed;

        //----------------------------------------------------------------------------------------------------

        public ParticleSystem(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.ParticleSystem;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _ticks = 0;
            TemplateName = null;

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization()
        {
            InitialiseNonSerialized();
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            _ticks++;
            if (_ticks >= MaxTicks)
                Deactivate();
        }

        protected override void OnDraw()
        {
            var template = Game.ParticleTemplateManager.GetTemplate(TemplateName);
            var textureInfo = template.TextureInfo;
            //MathHelper.TwoPi * (i / (float)MaxParticles)

            var c = 0;
 
            RandomHelper.DeterministicRandom.Seed = Seed;
            var ratio = _ticks / (float)MaxTicks;

            for (var i = 0; i < template.EmissionResidue; i++)
            {
                var speed = RandomHelper.DeterministicRandom.GetFloat(template.LinearSpeedMin, template.LinearSpeedMax);
                //var damping = template.LinearDamping < 1.0f ?  ratio + ((1 - ratio) * template.LinearDamping) : ratio;
                var distance = Interpolate.EaseOutHard(0, speed, ratio);
                //var distance = MathHelper.Lerp(0, speed, damping);

                var angle = RandomHelper.DeterministicRandom.GetFloat(0, MathHelper.TwoPi);
                var x = (float)Math.Cos(angle);
                var y = (float)Math.Sin(angle);
    
                var scaleX = RandomHelper.DeterministicRandom.GetFloat(template.SizeBands[0].SizeMinX, template.SizeBands[0].SizeMaxX);
                var scaleY = template.SymetricalParticles ? scaleX : RandomHelper.DeterministicRandom.GetFloat(template.SizeBands[0].SizeMinY, template.SizeBands[0].SizeMaxY);

                scaleX = MathHelper.Lerp(scaleX, 0, ratio);
                scaleY = MathHelper.Lerp(scaleY, 0, ratio);

                var direction = template.DirectionalParticles ? angle : 0.0f;
                    
                var quad = new Quad(Position + new Vector2(x * distance, y * distance), new Vector2(scaleX, scaleY), direction);

                Color tint;
                if (template.ColorBands[0].Colors != null)
                    tint = template.ColorBands[0].Colors[++c%template.ColorBands[0].Colors.Count];
                else
                    tint = new Color(RandomHelper.DeterministicRandom.GetVector4(template.ColorBands[0].ColourMin, template.ColorBands[0].ColourMax));

                Game.Renderer.DrawSprite(ref quad, textureInfo.TextureId, textureInfo.SourceRect, tint * (1.0f - ratio), TransparentRenderStyle.Instance, Layers.ActorForegroundEffect);
            }
        }
    }
}