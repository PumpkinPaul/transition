using System;
using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    class Collisions
    {
        public static bool TestSweptCircleCircle(Vector2 position1, Vector2 velocity1, float radius1, Vector2 position2, Vector2 velocity2, float radius2)
        {
            //Relative position and velocity
            var dp = position1 - position2;
            var dv = velocity1 - velocity2;

            //Minimal distance squared
            var r = radius1 + radius2;
            //dP^2-r^2
            var pp = dp.X * dp.X + dp.Y * dp.Y - r * r;
            //(1)Check if the spheres are already intersecting
            if (pp < 0)
                return true;

            //dP*dV
            var pv = dp.X * dv.X + dp.Y * dv.Y;
            //(2)Check if the spheres are moving away from each other
            if (pv >= 0)
                return false;

            //dV^2
            var vv = dv.X * dv.X + dv.Y * dv.Y;
            //(3)Check if the spheres can intersect within 1 frame
            if ((pv + vv) <= 0 && (vv + 2 * pv + pp) >= 0)
                return false;

            var d = pv * pv - pp * vv;
            return (d > 0);
        }

        public static bool TestSweptCircleCircle(Vector2 position1, Vector2 velocity1, float radius1, Vector2 position2, Vector2 velocity2, float radius2, out float t)
        {
            t = -1;

            var s = position2 - position1;  //Vector between sphere centers
            var v = velocity2 - velocity1;  //Relative motion of s1 with respect to stationary s0
            var r = radius2 + radius1;      //Sum of radii
            var c = Vector2.Dot(s, s) - (r * r);

            if (c < 0.0f)
            {
                // Spheres initially overlapping so exit directly
                t = 0.0f;
                return true;
            }

            var a = Vector2.Dot(v, v);
            if (a < float.Epsilon) return false; //Spheres not moving relative each other

            var b = Vector2.Dot(v, s);
            if (b >= 0.0f) return false; // Spheres not moving towards each other

            var d = b * b - a * c;
            if (d < 0.0f) return false; // No real-valued root, spheres do not intersect

            t = (-b - (float)Math.Sqrt(d)) / a;

            return t > 0 && t <= 1.0f;
        }

        public static bool TestAABBSweep(Vector2 extents1, Vector2 centre1, Vector2 velocity1, Vector2 extents2, Vector2 centre2, Vector2 velocity2, out float t0, out float t1)
        {
            //var font = Theme.Current.GetFont(Theme.Fonts.Default);

            var aabb1 = new AABB(centre1, extents1);
            var aabb2 = new AABB(centre2, extents2);

            //Check if the boxes are currently overlapping
            if (aabb1.Overlaps(aabb2))
            {
                t0 = t1 = 0;
                //TransitionGame.Instance.Renderer.DrawString(font, new Vector2(-15, 5), new Vector2(1f), "Overlaps!", Alignment.CentreLeft, FlatTheme.Silver, TransparentRenderStyle.Instance, 1000);
                return true;
            }

            //The problem is solved in A's frame of reference so get relative velocity (in normalized time)
            var relativeVelocity = velocity2 - velocity1;

            //First and last time of overlap along each axis
            var firstOverlapTime = new Vector2(float.MinValue);
            var lastOverlapTime = new Vector2(float.MaxValue);

            var overlapX = false;
            var overlapY = false;

            //Find the first possible times of overlap...
            if (relativeVelocity.X < 0)
            {
                if (aabb1.MinX < aabb2.MaxX)
                {
                    overlapX = true;
                    firstOverlapTime.X = (aabb1.MaxX - aabb2.MinX) / relativeVelocity.X;
                }
            }
            else if (relativeVelocity.X > 0)
            {
                if (aabb2.MinX < aabb1.MaxX)
                {
                    overlapX = true;
                    firstOverlapTime.X = (aabb1.MinX - aabb2.MaxX) / relativeVelocity.X;
                }
            }
            else if (aabb1.MinX <= aabb2.MaxX && aabb1.MaxX >= aabb2.MinX)
            {
                overlapX = true;
                firstOverlapTime.X = 0.0f;
                lastOverlapTime.X = 1.0f;
            }

            if (relativeVelocity.Y < 0)
            {
                if (aabb1.MinY < aabb2.MaxY)
                {
                    overlapY = true;
                    firstOverlapTime.Y = (aabb1.MaxY - aabb2.MinY) / relativeVelocity.Y;
                }
            }
            else if (relativeVelocity.Y > 0)
            {
                if (aabb2.MinY < aabb1.MaxY)
                {
                    overlapY = true;
                    firstOverlapTime.Y = (aabb1.MinY - aabb2.MaxY) / relativeVelocity.Y;
                }
            }
            else if (aabb1.MinY <= aabb2.MaxY && aabb1.MaxY >= aabb2.MinY)
            {
                overlapY = true;
                firstOverlapTime.Y = 0.0f;
                lastOverlapTime.Y = 1.0f;
            }

            //...and the last possible time of overlap
            if (aabb2.MaxX > aabb1.MinX && relativeVelocity.X < 0)
                lastOverlapTime.X = (aabb1.MinX - aabb2.MaxX) / relativeVelocity.X;
            else if (aabb1.MaxX > aabb2.MinX && relativeVelocity.X > 0)
                lastOverlapTime.X = (aabb1.MaxX - aabb2.MinX) / relativeVelocity.X;

            if (aabb2.MaxY > aabb1.MinY && relativeVelocity.Y < 0)
                lastOverlapTime.Y = (aabb1.MinY - aabb2.MaxY) / relativeVelocity.Y;
            else if (aabb1.MaxY > aabb2.MinY && relativeVelocity.Y > 0)
                lastOverlapTime.Y = (aabb1.MaxY - aabb2.MinY) / relativeVelocity.Y;

            if (overlapX == false || overlapY == false)
            {
                //TransitionGame.Instance.Renderer.DrawString(font, new Vector2(-15, 4), new Vector2(1f), $"No overlap on at least one axis - overlapX: {overlapX} overlapY: {overlapY}", Alignment.CentreLeft, FlatTheme.Silver, TransparentRenderStyle.Instance, 1000);
                t0 = t1 = 0;
                return false;
            }

            //TransitionGame.Instance.Renderer.DrawString(font, new Vector2(-15, 4), new Vector2(1f), $"u_0.X: {firstOverlapTime.X}", Alignment.CentreLeft, FlatTheme.Silver, TransparentRenderStyle.Instance, 1000);
            //TransitionGame.Instance.Renderer.DrawString(font, new Vector2(-15, 2), new Vector2(1f), $"u_1.X: {lastOverlapTime.X}", Alignment.CentreLeft, FlatTheme.Asbestos, TransparentRenderStyle.Instance, 1000);
            //TransitionGame.Instance.Renderer.DrawString(font, new Vector2(-15, 3), new Vector2(1f), $"u_0.Y: {firstOverlapTime.Y}", Alignment.CentreLeft, FlatTheme.Silver, TransparentRenderStyle.Instance, 1000);
            //TransitionGame.Instance.Renderer.DrawString(font, new Vector2(-15, 1), new Vector2(1f), $"u_1.Y: {lastOverlapTime.Y}", Alignment.CentreLeft, FlatTheme.Asbestos, TransparentRenderStyle.Instance, 1000);

            //Possible first time of overlap is the largest of the axis values
            t0 = Math.Max(firstOverlapTime.X, firstOverlapTime.Y);

            //So then the possible last time of overlap is the smallest of the overlaps values
            t1 = Math.Min(lastOverlapTime.X, lastOverlapTime.Y);

            //They could have only collided if the first time of overlap occurred before the last time of overlap
            return t0 >= 0 && t0 <= 1 && t0 <= t1;
        }

        public static bool TestSweptCircleAxisAlignedBoundingBox(Vector2 sc, Vector2 d, float radius, BoundingBox b, out float t)
        {
            // Compute the AABB resulting from expanding b by sphere radius r
            var e = b;
            e.Min.X -= radius; e.Min.Y -= radius;// e.Min.z -= s.r;
            e.Max.X += radius; e.Max.Y += radius;// e.Max.z += s.r;
            // Intersect ray against expanded AABB e. Exit with no intersection if ray
            // misses e, else get intersection point p and time t as result
            Vector2 p;
            if (!IntersectRayAxiaAlignedBoundingBox(sc, d, e, out t, out p) || t > 1.0f)
                return false;

            // Compute which min and max faces of b the intersection point p lies
            // outside of. Note, u and v cannot have the same bits set and
            // they must have at least one bit set among them
            int u = 0, v = 0;
            if (p.X < b.Min.X) u |= 1;
            if (p.X > b.Max.X) v |= 1;
            if (p.Y < b.Min.Y) u |= 2;
            if (p.Y > b.Max.Y) v |= 2;
            //if (p.z < b.Min.z) u |= 4;
            //if (p.z > b.Max.z) v |= 4;
            // �Or� all set bits together into a bit mask (note: here u + v == u | v)
            var m = u + v;
            // Define line segment [c, c+d] specified by the sphere movement
            var seg = new Segment(sc, sc + d);

            // If all 3 bits set (m == 7) then p is in a vertex region
            //if (m == 7) {
            // If all 2 bits set (m == 3) then p is in a vertex region
            const int allBits = 3;//7
            if (m == allBits)
            {
                // Must now intersect segment [c, c+d] against the capsules of the three
                // edges meeting at the vertex and return the best time, if one or more hit
                var tmin = float.MaxValue;
                var t1 = tmin;
                var t2 = tmin;
                if (IntersectSegmentCapsule(seg.P1, seg.P2, Corner(b, v), Corner(b, v ^ 1), radius, ref t1))
                {

                    tmin = Math.Min(t1, tmin);
                }

                if (IntersectSegmentCapsule(seg.P1, seg.P2, Corner(b, v), Corner(b, v ^ 2), radius, ref t2))
                    tmin = Math.Min(t2, tmin);

                //if (Intersections.IntersectSegmentCapsule(seg.P1, seg.P2, Corner(b, v), Corner(b, v ^ 4), radius, out t))
                //    tmin = Math.Min(t, tmin);

                if (tmin >= float.MaxValue) return false; // No intersection
                t = tmin;

                return t >= 0 && t < 1.0f;// true; // Intersection at time t == tmin
            }

            // If only one bit set in m, then p is in a face region
            if ((m & (m - 1)) == 0)
            {
                // Do nothing. Time t from intersection with
                // expanded box is correct intersection time
                return true;
            }

            // p is in an edge region. Intersect against the capsule at the edge
            return IntersectSegmentCapsule(seg.P1, seg.P2, Corner(b, u ^ allBits), Corner(b, v), radius, ref t);
        }

        public struct AABB
        {
            public Vector2 Centre;
            public Vector2 Extents; //x,y,z extents

            public AABB(Vector2 centre, Vector2 extents)
            {
                Centre = centre;
                Extents = extents;
            }

            //returns true if this is overlapping b
            public bool Overlaps(AABB b)
            {
                var T = b.Centre - Centre;//vector from A to B
                return Math.Abs(T.X) <= (Extents.X + b.Extents.X) && Math.Abs(T.Y) <= (Extents.Y + b.Extents.Y); 
            }

            public float MinX => Centre.X - Extents.X;
            public float MinY => Centre.Y - Extents.Y;
            public float MaxX => Centre.X + Extents.X;
            public float MaxY => Centre.Y + Extents.Y;
        }

        // Support function that returns the AABB vertex with index n
        static Vector2 Corner(BoundingBox b, int n)
        {
            return new Vector2((((n & 1) > 0) ? b.Max.X : b.Min.X), (((n & 2) > 0) ? b.Max.Y : b.Min.Y));//, (((n & 4) > 0) ? b.Max.Z : b.Min.Z));
        }

        //=== Section 5.3.3: =============================================================

        // Intersect ray R(t) = p + t*d against AABB a. When intersecting,
        // return intersection distance tmin and point q of intersection
        public static bool IntersectRayAxiaAlignedBoundingBox(Vector2 p, Vector2 d, BoundingBox a, out float tmin, out Vector2 q)
        {
            tmin = 0.0f;                // set to -FLT_MAX to get first hit on line
            var tmax = float.MaxValue;  // set to max distance ray can travel (for segment)

            q = Vector2.Zero;

            if (IntersectRayAxiaAlignedBoundingBoxComponent(a.Min.X, a.Max.X, p.X, d.X, ref tmin, ref tmax) == false)
                return false;

            if (IntersectRayAxiaAlignedBoundingBoxComponent(a.Min.Y, a.Max.Y, p.Y, d.Y, ref tmin, ref tmax) == false)
                return false;

            // Ray intersects all '2' /*3*/ slabs. Return point (q) and intersection t value (tmin) 
            q = p + d * tmin;
            return true;
        }

        private static bool IntersectRayAxiaAlignedBoundingBoxComponent(float boundingBoxMin, float boundingBoxMax, float p, float d, ref float tmin, ref float tmax)
        {
            if (Math.Abs(d) < float.Epsilon)
            {
                // Ray is parallel to slab. No hit if origin not within slab
                if (p < boundingBoxMin || p > boundingBoxMax) return false;
            }
            else
            {
                // Compute intersection t value of ray with near and far plane of slab
                var ood = 1.0f / d;
                var t1 = (boundingBoxMin - p) * ood;
                var t2 = (boundingBoxMax - p) * ood;
                // Make t1 be intersection with near plane, t2 with far plane
                if (t1 > t2) Swap(ref t1, ref t2);
                // Compute the intersection of slab intersections intervals
                tmin = Math.Max(tmin, t1);
                tmax = Math.Min(tmax, t2);

                // Exit with no collision as soon as slab intersection becomes empty
                if (tmin > tmax) return false;
            }

            return true;
        }

        //https://github.com/vancegroup-mirrors/hapi/blob/master/src/CollisionObjects.cpp

        //public static bool IntersectSegmentCapsule(Segment seg, Vector2 p, Vector2 q, float radius, out float t)
        public static bool IntersectSegmentCapsule(Vector2 sa, Vector2 sb, Vector2 p, Vector2 q, float r, ref float t)
        {
            Vector2 d = q - p, m = sa - p, n = sb - sa;
            var md = Vector2.Dot(m, d);
            var nd = Vector2.Dot(n, d);
            var dd = Vector2.Dot(d, d);

            // Test if segment fully outside either endcap of capsule.
            if (md < 0.0f && md + nd < 0.0f)
            {
                // Segment outside 'p'.
                return IntersectSegmentSphere(sa, n, p, r, ref t);
            }
            if (md > dd && md + nd > dd)
            {
                // Segment outside 'q'
                return IntersectSegmentSphere(sa, n, q, r, ref t);
            }
            var nn = Vector2.Dot(n, n);
            var mn = Vector2.Dot(m, n);
            var a = dd * nn - nd * nd;
            var k = Vector2.Dot(m, m) - r * r;
            var c = dd * k - md * md;
            if (Math.Abs(a) < float.Epsilon)
            {
                // Segment runs parallel to cylinder axis.
                if (c > 0.0f) return false; // 'a' and thus the segment lies outside cyl.
                // Now known that segment intersects cylinder. Figure out how.
                if (md < 0.0f)
                    // Intersect against 'p' endcap.
                    IntersectSegmentSphere(sa, n, p, r, ref t);
                else if (md > dd)
                    // Intersect against 'q' encap.
                    IntersectSegmentSphere(sa, n, q, r, ref t);
                else
                    t = 0.0f; // 'a' lies inside cylinder.

                return true;
            }

            var b = dd * mn - nd * md;
            var discr = b * b - a * c;
            if (discr < 0.0f) return false; // No real roots; no intersection.

            var t0 = t;
            t = (-b - (float)Math.Sqrt(discr)) / a;

            if (md + t * nd < 0.0f)
            {
                // Intersection outside cylinder on 'p' side;
                return IntersectSegmentSphere(sa, n, p, r, ref t);
            }

            if (md + t * nd > dd)
            {
                // Intsection outside cylinder on 'q' side.
                return IntersectSegmentSphere(sa, n, q, r, ref t);
            }
            t = t0;

            // Intersection if segment intersects cylinder between end-caps.
            return t >= 0.0f && t <= 1.0f;
        }

        //https://github.com/vancegroup-mirrors/hapi/blob/master/src/CollisionObjects.cpp

        // Intersects segment r = p * t * d, |d| = tmax,
        // 0 <= t <= tmax with sphere
        // defined by point s_c and r and, if intersecting, returns t value of
        // interseection and intersection point q.
        // Used by intersectSegmentCapsule
        public static bool IntersectSegmentSphere(Vector2 p, Vector2 d, Vector2 sC, float r, ref float t)
        {
            Vector2 dp;

            var p1 = p;
            var p2 = d;
            var sc = sC;

            dp.X = p2.X - p1.X;
            dp.Y = p2.Y - p1.Y;
            //dp.z = p2.z - p1.z;
            var a = dp.X * dp.X + dp.Y * dp.Y;
            var b = 2 * (dp.X * (p1.X - sc.X) + dp.Y * (p1.Y - sc.Y));
            var c = sc.X * sc.X + sc.Y * sc.Y;
            c += p1.X * p1.X + p1.Y * p1.Y;// + p1.z * p1.z;
            c -= 2 * (sc.X * p1.X + sc.Y * p1.Y);// + sc.z * p1.z);
            c -= r * r;
            var discriminant = b * b - 4 * a * c;
            if (Math.Abs(a) < float.Epsilon || discriminant < 0)
                return false;

            var sqrt = (float)Math.Sqrt(discriminant);
            var mu1 = (-b + sqrt) / (2 * a);
            var mu2 = (-b - sqrt) / (2 * a);

            t = Math.Min(mu1, mu2);

            return true;
        }

        // Returns true if sphere s intersects AABB b, false otherwise
        public static bool TestSphereAxiaAlignedBoundingBox(Vector2 position, float radius, BoundingBox b)
        {
            // Compute squared distance between sphere center and AABB
            var sqDist = SqDistPointAxiaAlignedBoundingBox(position, b);

            // Sphere and AABB intersect if the (squared) distance
            // between them is less than the (squared) sphere radius
            return sqDist <= radius * radius;
        }


        // Computes the square distance between a point p and an AABB b
        public static float SqDistPointAxiaAlignedBoundingBox(Vector2 p, BoundingBox b)
        {
            var sqDist = 0.0f;

            // For each axis count any excess distance outside box extents
            if (p.X < b.Min.X) sqDist += (b.Min.X - p.X) * (b.Min.X - p.X);
            if (p.X > b.Max.X) sqDist += (p.X - b.Max.X) * (p.X - b.Max.X);

            if (p.Y < b.Min.Y) sqDist += (b.Min.Y - p.Y) * (b.Min.Y - p.Y);
            if (p.Y > b.Max.Y) sqDist += (p.Y - b.Max.Y) * (p.Y - b.Max.Y);

            return sqDist;
        }

        public static void Swap(ref float t1, ref float t2)
        {
            var temp = t1;
            t1 = t2;
            t2 = temp;
        }

    }

    struct Segment
    {
        public Vector2 P1;
        public Vector2 P2;

        public Segment(Vector2 p1, Vector2 d)
        {
            P1 = p1;
            P2 = P1 + d;
        }
    }
}
