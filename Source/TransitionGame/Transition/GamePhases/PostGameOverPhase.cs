using System;
using Transition.Menus;

namespace Transition.GamePhases
{
    /// <summary>
    /// Handles the end of a game.
    /// </summary>
    public class PostGameOverPhase : GamePhase
    {					
        public PostGameOverPhase(BaseGame game) : base(game) { }

        /// <summary>
		///
		/// </summary>
        protected override void OnUpdate()
		{
            //TODO: Transition
            var game = ((TransitionGame)Game);

            if (Game.DemoRecorder.IsPlaying)
                
                game.DemoFinished();
            else
            {
                if (Game.PlayerCount == 1)
                {
                    //TODO: Transition
                    var topLevel = ((TransitionGame)Game).LevelManager.LevelNumber;

                    //Check the GameMode here - interesting stuff will probably need to happen
                    var scoreItem = new ScoreItem
                    {
                        Score = Game.ActivePlayer.Score,
                        Name = Game.ActivePlayer.Profile.Name,
                        LevelName = game.LevelManager.Level.Name,
                        Level = topLevel,
                        Ticks = Math.Max(Game.ActivePlayer.TopTicks, Game.ActivePlayer.BottomTicks)
                    };


                    Game.EndPlaySession(); //This will change the state.

                    //TODO: Transition
                    var scoreTable = ((TransitionGame)Game).GetScoreTable(Game.ActivePlayer.Difficulty);
                    var pointsBattleScoreAdded = scoreTable.AddScoreIfBetter(scoreItem);
                    if (pointsBattleScoreAdded)
                    {
                        scoreTable.Save(Game);
                    }

                    AddGameOverMenu(pointsBattleScoreAdded, scoreTable, scoreItem);
                }
            }
		}				

        private void AddGameOverMenu(bool scoreAdded, ScoreTable scoreTable, ScoreItem scoreItem)
        {
            //For Xbox we'll use profiles but for anything else allow a name to be entered
            var gameOverMenu = (GameOverMenu)Game.MenuManager.GetMenu(typeof(GameOverMenu));
            gameOverMenu.ScoreAdded = scoreAdded;
            gameOverMenu.ScoreItem = scoreItem;
            gameOverMenu.ScoreTable = scoreTable;
            
            Game.MenuManager.AddMenu(gameOverMenu);
        }
    }
}

