using Microsoft.Xna.Framework;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public class ScoreBonusLabel : Label
    {
        public Vector2 Acceleration;

        /// <summary>
        /// Creates a new instance of a ScoreBonusLabel.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public ScoreBonusLabel(TransitionGame game) : base(game) 
        {
            _horizontalConstrainBehaviour = ConstrainBehaviour.None;
            _verticalConstrainBehaviour = ConstrainBehaviour.None;
        }

        protected override void OnUpdate(GameTime gameTime) 
        {
            base.OnUpdate(gameTime);

            Velocity += Acceleration;
            Velocity.X *= 0.95f;
        }
    }
}
