using System;
using Microsoft.Xna.Framework;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the how to play menu.
    /// </summary>
    public class HowToPlayMenu : Menu
    {
        /// <summary>The number of how to play images.</summary>
        //Set the positions for each caption
        private readonly string[] _descrptions = new [] 
        {
            "kill all aliens to progress", "it's a race against time", "beware!!! he can't be shot", "protect the astronauts from abduction",
            "collect falling astronauts to rescue them", "switch worlds to clear both and avoid danger", "2 player - score points to earn rockets then unleash hell!!!", 
            "score the most points to win", "complete levels in the quickest time", "your worst level is recorded so play both world"
        };

        //Controls...
        private SlidingImageListControl _actionsImageList; 

        /// <summary>
        /// Initialises a new instance of a HowToPlayMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public HowToPlayMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            _actionsImageList = new SlidingImageListControl(Game);

            _actionsImageList.Position = new Vector2(0.0f, 0.0f);
            //_actionsImageList.AddImage(Game.TextureManager.GetTextureId("HowToPlay1"), Color.Gray);
            _actionsImageList.SelectedIndex = 0;
            _actionsImageList.LargeImageScale = 14.0f;
            _actionsImageList.NormalImageScale = 13.0f;
            _actionsImageList.ImageSpacer = 2.0f;
            _actionsImageList.ImageGetsFocusColour = true;
            _actionsImageList.Tint = Color.White;
            _actionsImageList.HelpText = string.Empty;
            _actionsImageList.PulseSelectedItem = false;
            _actionsImageList.IncreaseValue += ActionsImageListValueChanged;
            _actionsImageList.DecreaseValue += ActionsImageListValueChanged;
            _actionsImageList.AButtonText = string.Empty;

            AddControl(_actionsImageList, GetNextTabIndex());       
        }

        /// <summary>
        /// Occurs when a menu is added to the MenuManager.
        /// </summary>
        protected override void OnAddedToManager()
        {
            _actionsImageList.SelectedIndex = 0;
            _actionsImageList.HelpText = _descrptions[_actionsImageList.SelectedIndex];
        }       

        /// <summary>
        /// Occurs when the action image list value changes.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The data for the event.</param>
        private void ActionsImageListValueChanged(object sender, EventArgs e)
        {
            _actionsImageList.HelpText = _descrptions[_actionsImageList.SelectedIndex];
        }
    }
}
