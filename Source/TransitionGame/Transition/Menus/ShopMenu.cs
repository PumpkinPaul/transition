using System;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the shop menu.
    /// </summary>
    public class ShopMenu : Menu
    {
        TextLabelControl _cashValueLabel;
        TextLabelControl _lootHelpTextLabel;
        LootControl _healthLoot;
        LootControl _bulletDamageLoot;
        LootControl _fireRateLoot;
        LootControl _nukeLoot;
        LootControl _rageLoot;

        LootControl _cityDefenceLoot;
        LootControl _cityAttackLoot;
        
        readonly int[] _healthCost = new[] { 50, 125, 200, 300, 400, 525, 670, 875, 1150, 1500 };
        readonly int[] _nukeCost = new[] {250, 500, 1000, -1 };
        readonly int[] _rageCost = new[] {200, 400, 600, 800, 1000 };
        readonly int[] _cityDefenceCost = new[] {500, 1000, 2000, -1 };
        readonly int[] _cityAttackCost = new[] {500, 1000, 2000, -1 };

        public override string BackButtonText { get { return "Continue"; } }

        /// <summary>
        /// Initialises a new instance of a ShopMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public ShopMenu(BaseGame game, string title) : base(game, title) { }

        protected override void OnConfigureMenu()
        { 
            const float wide = 8.0f;
            const float col1 = -13.0f;
            const float col2 = -4.0f;
            const float col3 = 5.0f;

            var cashText = new TextLabelControl(Game);
            cashText.FontSize = Theme.FontSize * 0.75f;
            cashText.Tint = Theme.StandardColor;
            cashText.Text = "cash";
            cashText.Position = new Vector2(col1 + 0.3f, 6.0f);

            _cashValueLabel = new TextLabelControl(Game);
            _cashValueLabel.FontSize = Theme.FontSize * 0.75f;
            _cashValueLabel.Tint = Theme.StandardColor;
            _cashValueLabel.Text = "$0";
            _cashValueLabel.Position = new Vector2(col1 + 0.3f + Theme.Font.GetTextLength(cashText.Text + " ", cashText.FontSize.X), 6.0f);

            _lootHelpTextLabel = new TextLabelControl(Game);
            _lootHelpTextLabel.FontSize = Theme.FontSize * 1.2f;
            _lootHelpTextLabel.Tint = Theme.Current.GetColor(Theme.Colors.HelpText);
            _lootHelpTextLabel.Position = new Vector2(0.0f, 6.25f);
            _lootHelpTextLabel.Alignment = Alignment.TopCentre;

            _healthLoot = new LootControl(Game);
            _bulletDamageLoot = new LootControl(Game);
            _fireRateLoot = new LootControl(Game);
            _nukeLoot = new LootControl(Game);
            _rageLoot = new LootControl(Game);
            _cityDefenceLoot = new LootControl(Game);
            _cityAttackLoot = new LootControl(Game);

            _healthLoot.Description = "lives";
            _healthLoot.Cost = 100;
            _healthLoot.IconTextureInfo = Game.TextureManager.GetTextureInfo("ShopCityDefence");
            _healthLoot.HelpText = "increase your remaining lives";
            _healthLoot.Position = new Vector2(col1, 4);
            _healthLoot.Width = wide;
            _healthLoot.MaxValue = Player.MaxHealth;
            _healthLoot.Click += HealthLootClicked;
            _healthLoot.IncreaseValue += (sender, args) => MenuManager.SetFocus(_nukeLoot, false);

            _bulletDamageLoot.Description = "gun power";
            _bulletDamageLoot.Cost = 100;
            _bulletDamageLoot.IconTextureInfo = Game.TextureManager.GetTextureInfo("ShopGunPower");
            _bulletDamageLoot.HelpText = "increase power of bullets";
            _bulletDamageLoot.Position = new Vector2(col1, 2);
            _bulletDamageLoot.UnlockLevel = 2;
            _bulletDamageLoot.Width = wide;
            _bulletDamageLoot.Click += BulletDamageLootClicked;
            _bulletDamageLoot.IncreaseValue += (sender, args) => MenuManager.SetFocus(_rageLoot, false);

            _fireRateLoot.Description = "fire rate";
            _fireRateLoot.Cost = 100;
            _fireRateLoot.IconTextureInfo = Game.TextureManager.GetTextureInfo("ShopFireRate");
            _fireRateLoot.HelpText = "increase fire rate of weapon";
            _fireRateLoot.Position = new Vector2(col1, 0);
            _fireRateLoot.UnlockLevel = 3;
            _fireRateLoot.Width = wide;
            _fireRateLoot.Click += FireRateLootClicked;

            _nukeLoot.Description = "nukes";
            _nukeLoot.Cost = 0;
            _nukeLoot.IconTextureInfo = Game.TextureManager.GetTextureInfo("ShopNuke");
            _nukeLoot.HelpText = "screen filling destruction";
            _nukeLoot.Position = new Vector2(col2, 4);
            _nukeLoot.UnlockLevel = 3;
            _nukeLoot.Width = wide;
            _nukeLoot.MaxValue = Player.MaxNukes;
            _nukeLoot.Click += NukeLootClicked;
            _nukeLoot.DecreaseValue += (sender, args) => MenuManager.SetFocus(_healthLoot, false);
            _nukeLoot.IncreaseValue += (sender, args) => MenuManager.SetFocus(_cityDefenceLoot, false);

            _rageLoot.Description = "rage";
            _rageLoot.Cost = 0;
            _rageLoot.IconTextureInfo = Game.TextureManager.GetTextureInfo("RageIcon");
            _rageLoot.HelpText = "unleash awesome firepower";
            _rageLoot.Position = new Vector2(col2, 2);
            _rageLoot.UnlockLevel = 5;
            _rageLoot.Width = wide;
            _rageLoot.MaxValue = Player.MaxRagePower;
            _rageLoot.Click += RageLootClicked;
            _rageLoot.DecreaseValue += (sender, args) => MenuManager.SetFocus(_bulletDamageLoot, false);
            _rageLoot.IncreaseValue += (sender, args) => MenuManager.SetFocus(_cityAttackLoot, false);

            _cityDefenceLoot.Description = "city defence";
            _cityDefenceLoot.Cost = 0;
            _cityDefenceLoot.IconTextureInfo = Game.TextureManager.GetTextureInfo("ShopShield");
            _cityDefenceLoot.HelpText = "protect city from invaders";
            _cityDefenceLoot.Position = new Vector2(col3, 4);
            _cityDefenceLoot.UnlockLevel = 3;
            _cityDefenceLoot.Width = wide;
            _cityDefenceLoot.MaxValue = Player.MaxCityDefence;
            _cityDefenceLoot.Click += CityDefenceLootClicked;
            _cityDefenceLoot.DecreaseValue += (sender, args) => MenuManager.SetFocus(_nukeLoot, false);
         
            _cityAttackLoot.Description = "city attack";
            _cityAttackLoot.Cost = 0;
            _cityAttackLoot.IconTextureInfo = Game.TextureManager.GetTextureInfo("ShopCityAttack");
            _cityAttackLoot.HelpText = "counter measures from alien attack";
            _cityAttackLoot.Position = new Vector2(col3, 2);
            _cityAttackLoot.UnlockLevel = 8;
            _cityAttackLoot.Width = wide;
            _cityAttackLoot.MaxValue = Player.MaxCityAttack;
            _cityAttackLoot.Click += CityAttackLootClicked;
            _cityAttackLoot.DecreaseValue += (sender, args) => MenuManager.SetFocus(_rageLoot, false);

            var continueButton = new TextButtonControl(Game);
			continueButton.Position = new Vector2(0.0f, -5.0f);
			continueButton.Text = "continue";
			continueButton.Click += (sender, e) => Close();
            continueButton.Font = Theme.Font;
            continueButton.FontSize = Theme.FontSize * 1.75f;
            continueButton.Alignment = Alignment.Centre;
            continueButton.HelpText = "exit shop and continue playing";

            var tabOrder = -1;
            AddControl(cashText, -1);
            AddControl(_cashValueLabel, -1);
            AddControl(_lootHelpTextLabel, -1);
            AddControl(_healthLoot, ++tabOrder);
            AddControl(_bulletDamageLoot, ++tabOrder);
            AddControl(_fireRateLoot, ++tabOrder);
            AddControl(_nukeLoot, ++tabOrder);
            AddControl(_rageLoot, ++tabOrder);
            AddControl(_cityDefenceLoot, ++tabOrder);
            AddControl(_cityAttackLoot, ++tabOrder);
            
            AddControl(continueButton, ++tabOrder);
        }

        protected override void OnAddedToManager() 
        {
            RefreshHealth();
            RefreshBulletDamage();
            RefreshFireRate();
            RefreshNukes();
            RefreshRagePower();
            RefreshCityDefence();
            RefreshCityAttack();
            RefreshCash();
        }

        protected override void OnUpdateMustCallBase() 
        {
            base.OnUpdateMustCallBase();

            //TODO: Transition
            var game = (TransitionGame)Game;

            _lootHelpTextLabel.Visible = false;

            var lootControl = MenuManager.FocusControl as LootControl;
            if (lootControl != null)
            {
                if (game.LevelManager.LevelNumber < lootControl.UnlockLevel)
                {
                    _lootHelpTextLabel.Visible = true;
                    _lootHelpTextLabel.Text = lootControl.LockedMessage;
                }
                else if (lootControl.Value == lootControl.MaxValue)
                {
                    _lootHelpTextLabel.Visible = true;
                    _lootHelpTextLabel.Text = "Fully Upgraded";
                }
                else if (lootControl.Cost > Game.ActivePlayer.Money)
                {
                    _lootHelpTextLabel.Visible = true;
                    _lootHelpTextLabel.Text = "Insufficient Funds";
                }
                else
                {
                    _lootHelpTextLabel.Visible = true;
                    _lootHelpTextLabel.Text = "Available to Buy";
                }
            }

            //Push data to loot controls to determine state
            _bulletDamageLoot.CurrentLevelNumber = game.LevelManager.LevelNumber;
            _cityAttackLoot.CurrentLevelNumber = game.LevelManager.LevelNumber;
            _cityDefenceLoot.CurrentLevelNumber = game.LevelManager.LevelNumber;
            _fireRateLoot.CurrentLevelNumber = game.LevelManager.LevelNumber;
            _healthLoot.CurrentLevelNumber = game.LevelManager.LevelNumber;
            _nukeLoot.CurrentLevelNumber = game.LevelManager.LevelNumber;
            _rageLoot.CurrentLevelNumber = game.LevelManager.LevelNumber;

            //Push data to loot controls to determine state
            _bulletDamageLoot.CurrentMoney = Game.ActivePlayer.Money;
            _cityAttackLoot.CurrentMoney = Game.ActivePlayer.Money;
            _cityDefenceLoot.CurrentMoney = Game.ActivePlayer.Money;
            _fireRateLoot.CurrentMoney = Game.ActivePlayer.Money;
            _healthLoot.CurrentMoney = Game.ActivePlayer.Money;
            _nukeLoot.CurrentMoney = Game.ActivePlayer.Money;
            _rageLoot.CurrentMoney = Game.ActivePlayer.Money;
        }

        private void HealthLootClicked(object sender, EventArgs e)
        {
            if (_healthLoot.IsLocked)
                return;

            if (Game.ActivePlayer.BuyHealth(_healthLoot.Cost) == false) 
                return;

            RefreshHealth();
            RefreshCash();
        }

        private void BulletDamageLootClicked(object sender, EventArgs e)
        {
            if (_bulletDamageLoot.IsLocked)
                return;

            if (Game.ActivePlayer.BuyBulletDamage(_bulletDamageLoot.Cost) == false) 
                return;

            RefreshBulletDamage();
            RefreshCash();
        }

        private void FireRateLootClicked(object sender, EventArgs e)
        {
            if (_fireRateLoot.IsLocked)
                return;

            if (Game.ActivePlayer.BuyFireRate(_fireRateLoot.Cost) == false) 
                return;

            RefreshFireRate();
            RefreshCash();
        }

        private void NukeLootClicked(object sender, EventArgs e)
        {
            if (_nukeLoot.IsLocked)
                return;

            if (Game.ActivePlayer.BuyNuke(_nukeLoot.Cost) == false) 
                return;

            RefreshNukes();
            RefreshCash();
        }

        private void RageLootClicked(object sender, EventArgs e)
        {
            if (_rageLoot.IsLocked)
                return;

            //if (Game.ActivePlayer.BuyRagePower(_rageLoot.Cost) == false) 
            //    return;

            RefreshRagePower();
            RefreshCash();
        }

        private void CityDefenceLootClicked(object sender, EventArgs e)
        {
            if (_cityDefenceLoot.IsLocked)
                return;

            if (Game.ActivePlayer.BuyCityDefence(_cityDefenceLoot.Cost) == false) 
                return;

            RefreshCityDefence();
            RefreshCash();
        }

        private void CityAttackLootClicked(object sender, EventArgs e)
        {
            if (_cityAttackLoot.IsLocked)
                return;

            if (Game.ActivePlayer.BuyCityAttack(_cityDefenceLoot.Cost) == false) 
                return;

            RefreshCityAttack();
            RefreshCash();
        }

        private void RefreshHealth()
        {
            _healthLoot.Value = Game.ActivePlayer.Ship.Health;
            _healthLoot.Cost = _healthCost[_healthLoot.Value - 1];
        }

        private void RefreshBulletDamage()
        {
            var weaponData = Game.ActivePlayer.Ship.Weapon.WeaponData;
            var bulletData = Game.DataManager.BulletData(weaponData.BulletName);

            _bulletDamageLoot.MaxValue = Game.ActivePlayer.Ship.Weapon.MaxBulletDamageId + 1;
            _bulletDamageLoot.Value = Game.ActivePlayer.Ship.Weapon.BulletDamageId + 1;
            _bulletDamageLoot.Cost = bulletData.Damage[_bulletDamageLoot.Value - 1].Cost;
        }

        private void RefreshFireRate()
        {
            _fireRateLoot.MaxValue = Game.ActivePlayer.Ship.Weapon.MaxFireRateId + 1;
            _fireRateLoot.Value = Game.ActivePlayer.Ship.Weapon.FireRateId + 1;
            _fireRateLoot.Cost = Game.ActivePlayer.Ship.Weapon.WeaponData.FireRates[_fireRateLoot.Value - 1].Cost;
        }

        private void RefreshNukes()
        {
            _nukeLoot.Value = Game.ActivePlayer.Nukes;
            _nukeLoot.Cost = _nukeCost[_nukeLoot.Value];
        }

        private void RefreshRagePower()
        {
            //_rageLoot.Value = Game.ActivePlayer.RagePower;
            _rageLoot.Cost = _rageCost[_rageLoot.Value - 1];
        }

        private void RefreshCityDefence()
        {
            _cityDefenceLoot.Value = Game.ActivePlayer.CityDefence;
            _cityDefenceLoot.Cost = _cityDefenceCost[_cityDefenceLoot.Value];
        }

        private void RefreshCityAttack()
        {
            _cityAttackLoot.Value = Game.ActivePlayer.CityAttack;
            _cityAttackLoot.Cost = _cityAttackCost[_cityAttackLoot.Value];
        }

        private void RefreshCash()
        {
            _cashValueLabel.Text = "$" + Game.ActivePlayer.Money;
        }
    }
}
