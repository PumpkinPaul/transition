using System;
using Microsoft.Xna.Framework;
using Transition.Actors;

namespace Transition.Steering
{
    /// <summary>
    /// 
    /// </summary>
    public static class SteerLibrary
    {
        const float DefaultSeekRatio = 0.01f;

        public static Vector2 Wander (Actor source, float dt, ref Vector2 internalTarget, float volatility, float turnSpeed)
        {
            // Make sure we have a target
		    if (internalTarget.LengthSquared() == 0) 
            {
			    internalTarget = source.Position;
			    internalTarget.X += volatility;
		    }

		    var offset = internalTarget - source.Position;
		    float angle;
		    if (offset.LengthSquared() > 0) 
            {
			    // Work out the angle to the target from the character
			    angle = (float)Math.Atan2(offset.Y, offset.X);
		    }
		    else
		    {
			    // We're on top of the target, move it away a little.
			    angle = 0;
		    }

		    // Move the target to the boundary of the volatility circle.
		    internalTarget = source.Position;
		    internalTarget.X += volatility * (float)Math.Cos(angle);
		    internalTarget.Y += volatility * (float)Math.Sin(angle);

		    // Add the turn to the target
		    internalTarget.X += RandomBinomial(turnSpeed);
		    internalTarget.Y += RandomBinomial(turnSpeed);

		    return Seek(source, internalTarget, 0.0f);
        }

        private static float RandomBinomial(float max)
        {
            return RandomHelper.DeterministicRandom.GetFloat(max) - RandomHelper.DeterministicRandom.GetFloat(max);
        }

        public static Vector2 Seek(Actor source, Vector2 target, float ratio)
        {
            var position = source.Position;

            var offset = target - position;

            if (ratio == 0.0f)
                return offset - source.Velocity;

            return Vector2.Lerp(source.Velocity, offset, ratio);
        }

        public static Vector2 Flee(Actor source, Vector2 target, float ratio)
        {
            var position = source.Position;

            var offset = position - target;

            if (ratio == 0.0f)
                return offset - source.Velocity;

            return Vector2.Lerp(source.Velocity, offset, ratio);
        }

        public static Vector2 Pursuit(Actor source, Actor quarry)
        {
            return Pursuit(source, quarry, float.MaxValue, DefaultSeekRatio);
        }
       
        public static Vector2 Pursuit(Actor source, Actor quarry, float maxPredictionTime, float ratio)
        {
            // offset from this to quarry, that distance, unit vector toward quarry
            var offset = quarry.Position - source.Position;
            var distance = offset.Length();
            var unitOffset = offset / distance;

            // how parallel are the paths of "this" and the quarry
            // (1 means parallel, 0 is pependicular, -1 is anti-parallel)
            var sourceForward = Vector2.Normalize(source.Velocity);
            var quarryForward = Vector2.Normalize(quarry.Velocity);
            float parallelness = Vector2.Dot(sourceForward, quarryForward);

            // how "forward" is the direction to the quarry
            // (1 means dead ahead, 0 is directly to the side, -1 is straight back)
            float forwardness = Vector2.Dot(sourceForward, unitOffset);

            float directTravelTime = distance / source.Velocity.Length();
            int f = IntervalComparison(forwardness,  -0.707f, 0.707f);
            int p = IntervalComparison(parallelness, -0.707f, 0.707f);

            float timeFactor = 0; // to be filled in below

            // Break the pursuit into nine cases, the cross product of the quarry being [ahead, aside, or behind] us and heading
            // [parallel, perpendicular, or anti-parallel] to us.
            switch (f)
            {
                case +1:
                    switch (p)
                    {
                    case +1:          // ahead, parallel
                        timeFactor = 4;
                        break;
                    case 0:           // ahead, perpendicular
                        timeFactor = 1.8f;
                        break;
                    case -1:          // ahead, anti-parallel
                        timeFactor = 0.85f;
                        break;
                    }
                    break;

                case 0:
                    switch (p)
                    {
                        case +1:          // aside, parallel
                            timeFactor = 1;
                            break;
                        case 0:           // aside, perpendicular
                            timeFactor = 0.8f;
                            break;
                        case -1:          // aside, anti-parallel
                            timeFactor = 4;
                            break;
                    }
                break;

                case -1:
                    switch (p)
                    {
                        case +1:          // behind, parallel
                            timeFactor = 0.5f;
                            break;
                        case 0:           // behind, perpendicular
                            timeFactor = 2;
                            break;
                        case -1:          // behind, anti-parallel
                            timeFactor = 2;
                            break;
                    }
                    break;
            }

            // estimated time until intercept of quarry
             float et = directTravelTime * timeFactor;

            // xxx experiment, if kept, this limit should be an argument
             float etl = (et > maxPredictionTime) ? maxPredictionTime : et;

            // estimated position of quarry at intercept
             var target = PredictFuturePosition(quarry, etl);

            return Seek(source, target, ratio);
        }

        public static float ScalarRandomWalk ( float initial, float walkspeed, float min, float max)
        {
            float next = initial + (((RandomHelper.DeterministicRandom.GetFloat() * 2) - 1) * walkspeed);
            if (next < min) return min;
            if (next > max) return max;
            return next;
        }

        public static int IntervalComparison(float x, float lowerBound, float upperBound)
        {
            if (x < lowerBound) return -1;
            if (x > upperBound) return +1;
            return 0;
        }

        private static Vector2 PredictFuturePosition(Actor target, float predictionTime)
        {
            return target.Position + (target.Velocity * predictionTime);
        }
    }
}
