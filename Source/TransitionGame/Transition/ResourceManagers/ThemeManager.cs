using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Transition.Util;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Manages overlays configuration data.
	/// </summary>
    public class ThemeManager : ResourceManager
    {
        public override string FolderName { get { return "Themes"; } }

        readonly BaseGame _game;

        readonly Dictionary<string, Theme> _themes = new Dictionary<string, Theme>();

		/// <summary>
        /// Creates a new instance of a OverlaysManager.
        /// </summary>
        public ThemeManager(BaseGame game)
        {
            if (game == null)
                throw new ArgumentNullException(nameof(game));

            _game = game;
        }
        
        /// <summary>
        /// Loads all actor configuration data into the data manager.
        /// </summary>
        public override void LoadContent()
        {
            var folder = Path.Combine(Platform.ResourcesPath, FolderName);

            foreach (var filename in Directory.GetFiles(folder, "*.xml").OrderBy(f => f))
                LoadTemplateData(filename);
        }

		/// <summary>
        /// Load the template configuration data.
        /// </summary>
        private void LoadTemplateData(string path)
        {
            var xdocument = XDocument.Load(path);

            if (xdocument.Root == null)
                throw new XmlException("Missing Root element in " + path);

            var themeElement = xdocument.Root;

            var name = themeElement.GetAttributeString("name");

            var theme = _themes.ContainsKey(name) ? _themes[name] : new Theme();

            foreach (var fontElement in themeElement.Elements("fonts").Elements("font"))
            {
                var fontName = fontElement.GetAttributeString("name");
                var fontValue = fontElement.GetAttributeString("value");
                var font = _game.FontManager.GetBitmapFont(fontValue);

                theme.AddFont(fontName, font);

                ScriptManager.AddVariable(fontName, fontValue);
            }   

            foreach (var fontSizeElement in themeElement.Elements("fontSizes").Elements("fontSize"))
            {
                var fontSizeName = fontSizeElement.GetAttributeString("name");
                var fontSizeValue = fontSizeElement.GetAttributeVector2("value");

                theme.AddFontSize(fontSizeName, fontSizeValue);

                ScriptManager.AddVariable(fontSizeName, fontSizeValue);
            }   

            foreach (var colorElement in themeElement.Elements("colors").Elements("color"))
            {
                var colorName = colorElement.GetAttributeString("name");
                var color = colorElement.GetAttributeColor("value");

                theme.AddColor(colorName, color);

                ScriptManager.AddVariable(colorName, color);
            }  
            
            _themes[name] = theme;  
        }

		/// <summary>
        /// Gets a Theme for a key.
		/// </summary>
        public Theme GetTheme(string name)
		{
		    return _themes.ContainsKey(name) ? _themes[name] : null;
		}
    }
}
