﻿using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Rendering;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Map checkpoints
    /// </summary>
    [ProtoContract]
    public class Flag : Entity
    {
        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public Flag(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.Flag;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization()
        {
            InitialiseNonSerialized();
        }

        protected override void OnDraw()
        {
            base.OnDraw();

            Game.Renderer.DrawString(Theme.Current.GetFont(Theme.Fonts.Default), Position + new Vector2(0, 1), Vector2.One, "Checkpoint", Alignment.Centre, new Color(0,0,0,64), AlphaBlendingRenderStyle.Instance);
        }
    }
}