Doppelganger
------------

Doppelganger is a frantic 2d side scrolling blaster in the vein of old classics like Defender and (Jeff Minter's) Iridis Alpha.

Initially this was my entry into Microsoft's Dream Build Play XNA competition and I had about 10 weeks or so to learn XNA and create a game with it.

Since the competition closed I've spent the last 3 or 4 months adding a few extra features to it (read what they are in the version history below).
    
    
    
How to play
-----------

The core concept of the game is based on the Eugene Jarvis classic Defender.  You control a highly manouverable space craft which you must fly over a planet's surface killing all aliens on a level to progress.

Dotted along the landscape are a number of astronauts that must be protected.  The astronauts are vulnerable to player shots so take care.  They are also prone to 'abduction' by the pink 'lander' aliens who hunt them and attempt to take the from the planet surface into deep space - if they get too far the landers mutate into even more fearsome foes intent on huting the player to extinction!  Oh my!  
The sharper shooters amongst you can shoot the landers mid abduction and hence increase their points tally - If the astronauts fall too far they will explode and be lost forever.
Once all the astronauts have been lost, destroyed or abducted the world descends into chaos and anti matter particles appears spitting death at the player.  Try and stop this happening if possible.

A 'level' consists of a number of aliens that must be destroyed in a certain time.  If they are all destroyed the player progresses to the next level.  If the time limit expires a gong sounds and 'hunter' aliens warp in and attempt to [insert something sinister about eating brains, etc]. Gulp!
A short while after the hunters warp in an alarm will sound and spikey red hunters warp in - these are dead nasty, invulnerable and shoot a fair bit so erm, try to be quick.

Oh yeah nearly forgot!  The screen is split in two each containing a world, both playing simultaneously - The player can warp between these worlds at will (in a 1 player game).

Good luck soldier!



2 player game
-------------

In a 2 player game each player inhabits their own world (player 1 on top / player 2 on the bottom) so world switching is abandoned.  
However, players can earn homing missiles that they can launch into their opponents world.  A homing missile is gained for every 1000 points scored up to a maximum of 6 at any one time.
Your opponents 'ghost' appears in your world (and on your scanner) so you can determine their position and therefore when best to launch.  One missile is launched each time you press the 'switch world button' which in 2 player becoms the 'launch homer button'.



HUD
---

The heads up display show various important bits of info.  
From left to right: Number of astronauts remaining in the level, number of ships left, score, scanner, time played so far, current level and time remaining for the level.



Hints and tips
--------------

The number of points gained for each action will be displayed on screen - see if you can spot scoring opportunites
Try to clear a level before the spikey red alien warp in - they'll warp in to the opposite world and are invulnerable to player shots - they can be destroyed however - can you find how?



Controls
--------

The game can be played with either the keyboard or an XBox 360 pad (USB or wireless via the adapter).  Support for non XBox pads / joysticks is planned but deploying the dlls that allow me to code this is proving somewhat troublesome so the feature had been dropped for now.  (NOTE - there is a utility called JoyToKey which should allow you to map any type of joystick to the relevant keyboard actions - Google for it).
 
     Keys Menu
    -------------
    Move selection: Up / Down arrows
    Change selected item's value: Left / Right arrows
    Select menu option: X
    
    (Keys chosen to reflect the keys required for game play actions - aids MAME style setups)
    
    Keys Player 1
    -------------
    Move ship: Arrow Keys
    Shoot: X
    Switch World (1 player game): Z
    Launch Homing Missile (2 player game): Z
    Pause: P
    
    Keys Player 2
    -------------
    Move ship: W, A, S, D
    Shoot: L
    Switch World (1 player): K
    Launch Homing Missile (2 player): K
    Pause: P
    
    * keys can be configured.


    XBox 360 GamePad (Windows and XBox)
    -----------------------------------
    Move ship: Left Stick / D-pad
    Shoot: X / A
    Switch World (1 player game): Left Trigger / Right
    Launch Homing Missile (2 player game): Left Trigger / Right
    Pause: Back
    
    * pads 1 and 2 are used.

Achievements
------------

These are based on XBox 360 achievements - If you're not familiar with the concept read on...They are simply little tasks that can be performed which become unlocked when complete (e.g. complete level 5, score a certain amount, etc)
You can view the achievements via the Profiles menu (they are linked to profiles).  Each achievement can be completed in easy, medium and hard difficulties (indicated by the little stars next to each one) and will be highlighted when unlocked.



Online Features (see the Privacy Policy below)
----------------------------------------------

Doppelganger has some limited online capability.  

You can create online profiles - this will be so you can 'share' profiles between different games (e.g. when we release our next game you can choose to import your online profile so that all your records, scores and achievements are logged against a common profile). 

Leaderboards - each game mode / difficulty has its own online score board.  If you have created an online profile and are currently playing with that your score will be posted after each game (regardless of whether it is good enough for the local score table).



Thanks
------

Just a few folks I'd like to thank for various reasons.

* First and foremost my wife for putting up with me disappearing off to the computer all too often and thinking about scrolling routines constantly.
* Eugene Jarvis for the phenomenal game that is Defender.
* Jeff Minter for producing crazy shooters for 20 odd years - endless fun.  Iridis Alpha being the catalyst for Doppelganger's dual worlds.
* Fog @ Binary Zoo Studios for listening to my game designs, providing feedback and keeping me on my toes by producng some excellent games.
* PomPom Games for the sublime Mutant Storm - this is the game that actually made me get off my ass and finish something.
* The community at YakYak for testing, feedback, support and basically putting up with me squatting on their servers while pimping my game designs.
* Ian Greenhouse and David Degville ideas and testing.



Verion History
--------------

Version 1.4
-----------
Fixed up the installer to ignore dependancies (.Net Framework SP1 setup was throwing an error) - these must now be installed separately - see web site for System Requirements.


Version 1.3
-----------
Compressed all game audio (~33MB --> PC ~9MB / XBox ~4MB).


Version 1.2
-----------
Converted to XNA 2.0
Reordered the list of available resolutions to display in ascending order of horizontal dimension (previously they were grouped by standard / widescreen).
Added a 'working directory' to the desktop icon.
Added an 'error log' shortcut. 


Version 1.1
-----------
Klaxon warning and 'gong' sound effects for when spikey red hunters and hunters warp in.
Added a shockwave effect.
Added a second landscape layer for a nice paralax effect.
Reconfigurable keys.
Homing Missile for multiplayer games.
Simultaneous 2 player action.
Online profiles and leaderboards.
Achievements added.
Added marker arrows to alert the player to the direction of abduction.
Added scanner / radar.
2 game modes available; Points Battle and Time Attack
3 difficulty levels now available; Easy, Medium, Hard.
Stopped music when game is paused or loses focus.
Added a Bloom shader to produce a glow effect.

Version 1.0
-----------
Initial version coded for Microsoft's Dream Build Play Competition.

Privacy Policy
--------------

I just make simple little games for fun and am not interested in harvesting any email addresses, personal info or whatever - any data retrieved by Pumpkin Games will NEVER be released to 3rd parties or sold for profit.

Doppelganger sends 3 requests to the 'Net.
1) When creating an online profile
2) When posting a score online
3) When viewing the online leaderboards.

Your IPAddress will be sent as part of this info.  If this makes you wary then simply use the offline profiles.

Cheers,
Paul Cunningham

http://www.pumpkin-games.net