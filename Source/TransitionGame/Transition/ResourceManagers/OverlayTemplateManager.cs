using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Transition.Gearset;
using Transition.MenuControls;

using ControlsMap = System.Collections.Generic.Dictionary<string, Transition.MenuControls.MenuControl>;
using OverlaysControlsMap = System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, Transition.MenuControls.MenuControl>>;
using UserControlsMap = System.Collections.Generic.Dictionary<string, System.Xml.Linq.XElement>;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Manages overlays configuration data.
	/// </summary>
    public class OverlayTemplateManager : ResourceManager
    {
        public class ControlParser
        {
            public MenuControl ParseControls(string containerName, string namePrefix, ControlsMap controlsMap, XElement element, string path, MenuControl parent, BaseGame game)
            {
                var type = FindType(element.Name.LocalName);
                if (type == null)
                    throw new InvalidOperationException("Unknown MenuControl type [" + element.Name.LocalName + "] in " + path);

                var name = element.GetAttributeString("name");
                if (string.IsNullOrEmpty(name))
                    throw new InvalidOperationException("Menu Controls must have a name defined in " + path);

                var constructor = type.GetConstructor(new[] { typeof(BaseGame) });
                if (constructor == null)
                    throw new InvalidOperationException("The object does not contain an appropriate constructor: e.g. Class(BaseGame)");

                var control = (MenuControl)constructor.Invoke(new object[] { game });
                control.Load(element);

                if (parent != null)
                {
                    //Nested control declaration
                    control.Parent = parent;
                }
                else
                {
                    //Linked control declaration
                    var parentName = element.GetAttributeString("parent");
                    if (parentName != null)
                    {
                        if (controlsMap.ContainsKey(parentName) == false)
                            throw new InvalidOperationException($"Control '{name}' has an invalid parent '{parentName}'");

                        control.Parent = controlsMap[parentName];
                    }
                }

                if (control.Parent != null)
                {
                    control.Parent.AddChild(control);
                }

                XElement nestedElement;

                if (type.Name == "UserControl")
                {
                    //Get the user control element out of the map
                    var source = element.GetAttributeString("source");
                    if (string.IsNullOrEmpty(source))
                        throw new InvalidOperationException("User Controls must have a source attribute defined in " + path);

                    if (UserControlsMap.ContainsKey(source) == false)
                        throw new InvalidOperationException("Unknown UserControl source [" + source + "] in " + path);

                    nestedElement = UserControlsMap[source];
                    
                    AddControlMap(namePrefix, controlsMap, namePrefix + name, control);

                    namePrefix += name + ".";
                }
                else
                {                 
                    AddControlMap(containerName, controlsMap, namePrefix + name, control);

                    //Nested controls?
                    nestedElement = element.Element("controls");  
                }

                
                if (nestedElement == null)
                    return control;

                foreach (var el in nestedElement.Elements())
                {
                    ParseControls(containerName, namePrefix, controlsMap, el, path, control, game);
                }

                return control;
            }

            void AddControlMap(string containerName, ControlsMap controlsMap, string name, MenuControl control)
            {
                if (controlsMap.ContainsKey(name))
                {
                    GS.Log("Error", "There is already a control named: " + name + " on: " + containerName);
                    return;
                }
                
                controlsMap.Add(name, control);
            }
        }

        public override string FolderName { get { return "Overlays"; } }

        readonly BaseGame _game;

        readonly OverlaysControlsMap _overlaysControlsMap = new OverlaysControlsMap();
        static readonly UserControlsMap UserControlsMap = new UserControlsMap();

		/// <summary>
        /// Creates a new instance of a OverlaysManager.
        /// </summary>
        public OverlayTemplateManager(BaseGame game)
        {
            if (game == null)
                throw new ArgumentNullException(nameof(game));

            _game = game;
        }
        
        /// <summary>
        /// Loads all actor configuration data into the data manager.
        /// </summary>
        public override void LoadContent()
        {
            var folder = Path.Combine(Platform.ResourcesPath, FolderName, "UserControls");

            foreach (var filename in Directory.GetFiles(folder, "*.xml").OrderBy(f => f))
                LoadUserControlData(filename);

            folder = Path.Combine(Platform.ResourcesPath, FolderName);

            foreach (var filename in Directory.GetFiles(folder, "*.xml").OrderBy(f => f))
                LoadTemplateData(filename);
        }

        /// <summary>
        /// Load the template configuration data.
        /// </summary>
        private void LoadUserControlData(string path)
        {
            var xdocument = XDocument.Load(path);

            if (xdocument.Root == null)
                throw new XmlException("Missing Root element in " + path);

            var filename = Path.GetFileNameWithoutExtension(path);

            if (filename == null)
                throw new XmlException("Invalid userControl:" + path);

            var userControlElement = xdocument.Root;

            var userControlName = userControlElement.GetAttributeString("name");

            var key = userControlName ?? filename;

            UserControlsMap[key] = userControlElement;
        }

		/// <summary>
        /// Load the template configuration data.
        /// </summary>
        private void LoadTemplateData(string path)
        {
            var xdocument = XDocument.Load(path);

            if (xdocument.Root == null)
                throw new XmlException("Missing Root element in " + path);

            var menuClass = Path.GetFileNameWithoutExtension(path);

            if (menuClass == null)
                throw new XmlException("Invalid menuClass:" + path);

            var overlayElement = xdocument.Root;

            var overlayName = overlayElement.GetAttributeString("name");

            var key = overlayName ?? menuClass;

            var overlay = _overlaysControlsMap.ContainsKey(key) ? _overlaysControlsMap[key] : new ControlsMap();
            overlay.Clear();

            var controlParser = new ControlParser();
            foreach (var element in overlayElement.Elements())
            {
                if (ParseCommonElement(element))
                    continue;

                controlParser.ParseControls(key, string.Empty, overlay, element, path, null, _game);
            }   
            
            _overlaysControlsMap[key] = overlay;    
        }

        private static Dictionary<string, Type> _types;

        public static Type FindType(string typeName)
        {
            var key = typeName.ToLower();
            if (_types == null)
            {
                _types = new Dictionary<string, Type>();
                foreach (var type in typeof(OverlayTemplateManager).Assembly.GetTypes())
                {
                    var name = type.Name.ToLower();
                    if (!_types.ContainsKey(name))
                        _types.Add(name, type);
                }
            }

            return _types.ContainsKey(key) ? _types[key] : null;
        }

		/// <summary>
        /// Gets an overlay template.
		/// </summary>
        public Dictionary<string, MenuControl> GetTemplate(string name)
		{
		    return _overlaysControlsMap.ContainsKey(name) ? _overlaysControlsMap[name] : null;
		}
    }
}
