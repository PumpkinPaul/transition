namespace Transition.Resource
{
    /// <summary>
    /// A base class for resources.
    /// </summary>
    public abstract class Resource
    {
        public void Create()
        {
            DoCreate();
        }

        protected virtual void DoCreate() { }

        public void Destroy()
        {
            DoDestroy();
        }

        protected virtual void DoDestroy() { }
    }
}
