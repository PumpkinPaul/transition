﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Transition.Input;
using Transition.Rendering;

namespace Transition._____Temp.RewindBinaryFormatter
{
    [Serializable]
    public class PlayerEntity : Entity
    {
        public override void Update(bool wasRewinding)
        {
            if (KeyboardHelper.IsKeyPressed(Keys.Left))
            {
                Position.X -= 0.1f;
            }
            else if (KeyboardHelper.IsKeyPressed(Keys.Right))
            {
                Position.X += 0.1f;
            }

            if (KeyboardHelper.IsKeyPressed(Keys.Up))
            {
                Position.Y += 0.1f;
            }
            else if (KeyboardHelper.IsKeyPressed(Keys.Down))
            {
                Position.Y -= 0.1f;
            }

            if (KeyboardHelper.IsKeyPressed(Keys.Space))
            {
                Scale.X += 0.025f;
                Scale.Y += 0.025f;
            }   
        }

        public override void Draw()
        {
            var q = new Quad(Position, Scale);
            var textureInfo = TransitionGame.Instance.TextureManager.GetTextureInfo("Pixel");
            TransitionGame.Instance.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, Color.DarkBlue, TransparentRenderStyle.Instance);
        }
    }
}