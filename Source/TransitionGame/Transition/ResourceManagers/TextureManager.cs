using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.ResourceManagers
{
    /// <summary>
    /// A class to manage texture resources
    /// </summary>
    public class TextureManager : ContentResourceManager
    {
        public override string FolderName { get { return "SpriteSheetDefs"; } }

        /// <summary>The list of textures.</summary>
        readonly List<Texture2D> _textures = new List<Texture2D>();

        readonly IDictionary<string, TextureInfo> _textureInfos = new Dictionary<string, TextureInfo>();

        /// <summary>
        /// Creates a new instance of a TextureManager class.
        /// </summary>
        public TextureManager(ContentManager contentManager) : base(contentManager) { }

        /// <summary>
        /// Loads all texture in the specified folder.
        /// </summary>
        public override void LoadContent()
        {
            LoadAllTextures();
            LoadAllSpriteSheets();
        }

        private void LoadAllTextures()
        {
            _textures.Clear();

            var folder = Path.Combine(Platform.ContentPath, "Textures");

            foreach (var filename in Directory.GetFiles(folder, "*.xnb"))
            {
                var assetName = Path.GetFileNameWithoutExtension(filename);

                if (string.IsNullOrEmpty(assetName))
                    throw new FileLoadException("Invalid file name - Resource file needs a name:" + filename);

                folder = Path.Combine(Platform.ContentFolderName, "Textures");
                AddTexture(assetName, ContentManager.Load<Texture2D>(Path.Combine(folder, assetName)));
            }
        }

        private void LoadAllSpriteSheets()
        {
            _textureInfos.Clear();

            var folder = Path.Combine(Platform.ResourcesPath, FolderName);
            var filenames = Directory.GetFiles(folder, "*.xml");

            foreach (var filename in filenames)
            {
                //Load the imagebank def
                var xdocument = XDocument.Load(filename);

                if (xdocument.Root == null)
                    throw new XmlException("Invalid configuration file - No root element: " + filename);

                foreach (var imageBankElement in xdocument.Root.Elements("imagebank"))
                {
                    var name = imageBankElement.Attribute("name").Value;
                    var defaultstyle = imageBankElement.Attribute("defaultstyle").Value;

                    var textureName = name.Replace(".png", string.Empty);

                    var texture = GetTexture(textureName);

                    foreach (var spriteImageElement in imageBankElement.Elements("spriteimage"))
                    {
                        var spriteImageName = spriteImageElement.Attribute("name").Value;
                        var x = Convert.ToInt32(spriteImageElement.Attribute("x").Value);
                        var y = Convert.ToInt32(spriteImageElement.Attribute("y").Value);
                        var w = Convert.ToInt32(spriteImageElement.Attribute("w").Value);
                        var h = Convert.ToInt32(spriteImageElement.Attribute("h").Value);
                        var hx = Convert.ToInt32(spriteImageElement.Attribute("hx").Value);
                        var hy = Convert.ToInt32(spriteImageElement.Attribute("hy").Value);

                        //Convert the absolute texture coords into a virtual 0...1 range
                        var top = y/(float) texture.Height;
                        var left = x/(float) texture.Width;
                        var width = w/(float) texture.Width;
                        var height = h/(float) texture.Height;

                        var spriteImage = new TextureInfo();
                        spriteImage.AssetName = textureName;
                        spriteImage.SpriteImageName = spriteImageName;
                        spriteImage.TextureId = GetTextureId(textureName);
                        spriteImage.Texture = texture;
                        spriteImage.SourceRect = new Rect(top, left, top + height, left + width);
                        spriteImage.FlippedSourceRect = new Rect(top, left + width, top + height, left);
                        spriteImage.Origin = new Vector2(hx, hy);

                        var styleAttribute = spriteImageElement.Attribute("style");
                        spriteImage.Style = styleAttribute != null ? styleAttribute.Value : defaultstyle;

                        _textureInfos[spriteImageName] = spriteImage;
                    }
                }
            }

            //Add in a blank one...
            _textureInfos[string.Empty] = new TextureInfo {TextureId = -1, SourceRect = Rect.TextureRect};

            AddTexturesToTextureInfos("Grunge1", "Grunge2", "Cohort", "MainMenu", "64x64Gradient");
        }

        void AddTexturesToTextureInfos(params string[] names)
        {
            foreach (var name in names)
            {
                //'Boss image' for coding at Medgate UK ;-)
                var image1 = new TextureInfo
                {
                    AssetName = name,
                    SpriteImageName = name,
                    TextureId = GetTextureId(name),
                    Texture = GetTexture(name),
                    SourceRect = new Rect(0, 0, 1, 1),
                    FlippedSourceRect = new Rect()
                };
                image1.Origin = new Vector2(image1.Texture.Width / 2.0f, image1.Texture.Height / 2.0f);

                _textureInfos[name] = image1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetName"></param>
        /// <param name="texture"></param>
        public int AddTexture(string assetName, Texture2D texture)
        {
            texture.Name = assetName;
            _textures.Add(texture);

            return _textures.Count - 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetName"></param>
        /// <param name="textureInfo"></param>
        public void AddTextureInfo(string assetName, TextureInfo textureInfo)
        {
            textureInfo.AssetName = assetName;
            _textureInfos.Add(assetName, textureInfo);
        }

        /// <summary>
        /// Gets a TextureInfo.
        /// </summary>
        /// <param name="key"></param>
        public TextureInfo GetTextureInfo(string key)
        {
            if (_textureInfos.ContainsKey(key) == false)
                return null;

            return _textureInfos[key];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetName"></param>
        /// <returns></returns>
        public Texture2D GetTexture(string assetName)
        {
            foreach (var texture in _textures)
            {
                if (texture.Name == assetName)
                    return texture;
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureId"></param>
        /// <returns></returns>
        public Texture2D GetTexture(int textureId)
        {
            return _textures[textureId];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetName"></param>
        /// <returns></returns>
        public int GetTextureId(string assetName)
        {
            for (var i = 0; i <  _textures.Count; ++i)
            {
                if (_textures[i].Name == assetName)
                    return i;
            }
   
            return -1;
        }
    }
}
