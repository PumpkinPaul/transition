using Transition.CourseSystem;
using Transition.Input;
using Transition.Rendering;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class LabelTool : Tool
    {
        public override string CursorName { get { return "ToolboxText.png"; } }

        readonly BitmapFont _font;

        public LabelTool(TransitionGame game) : base(game) 
        {
            _font = Theme.Font;
        }

        public override void Update()
        {
            if (MouseHelper.IsLeftButtonJustDown)
            {
                var label = new Label
                {
                    Color = LevelEditor.ObjectColor, 
                    Position = LevelEditor.ObjectPosition,
                    Rotation = LevelEditor.ObjectRotation,
                    FontSize = LevelEditor.ObjectScale,
                    Text = LevelEditor.LabelText,
                    Alignment = Alignment.Centre
                };
                LevelEditor.Course.Labels.Add(label);
                LevelEditor.SaveEditorState("Add text");
            }
        }

        public override void Draw()
        {
            Game.Renderer.DrawString(_font, LevelEditor.ObjectPosition, LevelEditor.ObjectScale, LevelEditor.ObjectRotation, LevelEditor.LabelText, Alignment.Centre, LevelEditor.ObjectColor, TransparentRenderStyle.Instance);
        }
    }
}
