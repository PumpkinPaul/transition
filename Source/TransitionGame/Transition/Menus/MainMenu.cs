using System;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the main menu.
    /// </summary>
    public class MainMenu : Menu
    {
        ListControl _playListControl;

        enum PlayOptions
        {
            //QuickPlay,
            NewGame,
            ResumeCheckpoint
        }

        /// <summary>
        /// Creates a new instance of a MainMenu.
        /// </summary>
        public MainMenu(BaseGame game, string title) : base(game, title) { }

        protected override void OnConfigureMenu()
        {
            _playListControl = GetControl<ListControl>("playListControl");
            _playListControl.TextButtonClicked += PlayListTextButtonClicked;
            _playListControl.Click += PlayListClicked;

            GetControl<TextButtonControl>("scoresButton").Click += (sender, args) => MenuManager.AddMenu(MenuManager.GetMenu(typeof(HeroesMenu)));
            GetControl<TextButtonControl>("controlsButton").Click += (sender, args) => MenuManager.AddMenu(MenuManager.GetMenu(typeof(ControlsMenu)));
            GetControl<TextButtonControl>("settingsButton").Click += (sender, args) => MenuManager.AddMenu(MenuManager.GetMenu(typeof(SettingsMenu)));
            GetControl<TextButtonControl>("creditsButton").Click += (sender, args) => MenuManager.AddMenu(MenuManager.GetMenu(typeof(CreditsMenu)));
            GetControl<TextButtonControl>("backButton").Click += (sender, args) => BackPressed();
            GetControl<TextButtonControl>("quitButton").Click += (sender, args) => MenuManager.ExitGame();
        }

        protected override bool OnBack()
        {
            Close();
            Game.ReturnToIntro();
            
            return true;
        }

        void PlayListTextButtonClicked(object sender, EventArgs e)
        {
            if (Game.InputMethodManager.InputMethod.SupportsMouse)
                return;

            PlayListClicked(sender, e);
        }

        void PlayListClicked(object sender, EventArgs e)
        {
            switch ((PlayOptions)_playListControl.SelectedIndex)
            {
                //case PlayOptions.QuickPlay:
                //    break;

                case PlayOptions.NewGame:
                    MenuManager.AddMenu(MenuManager.GetMenu(typeof(NewGameMenu)));
                    break;

                case PlayOptions.ResumeCheckpoint:
                    MenuManager.AddMenu(MenuManager.GetMenu(typeof(ResumeCheckpointMenu)));
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
