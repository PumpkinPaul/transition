using Microsoft.Xna.Framework;
using Transition.ActorDatas;

namespace Transition.Actors
{
    /// <summary>
    /// Represents a player's bullet
    /// </summary>
    public abstract class Bullet : Actor
    {
		/// <summary>Screen flash colours for different actors.</summary>
        protected static readonly Color CityFlash = Color.DarkRed;
        protected static readonly Color AlienFlash = Color.DarkGray;

        /// <summary>Configuration data for the bullet.</summary>
        protected BulletData BulletData;

        /// <summary>Indicates whether the bullet is being deactivated.</summary>
        protected bool DeactivateMe;

        public Alien Target { get; set; }
        public int DamageId;

        public Vector2 InitialVelocity;

        /// <summary>
        /// Creates a new instance of a Bullet
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        protected Bullet(TransitionGame game) : base(game) { }

        /// <summary>
        /// Gets the type of actor
        /// </summary>
        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.Bullet; }
        }

        /// <summary>
        /// Gets whether this type of actor collides
        /// </summary>
        public override int CollisionMask
        {
            get { return 1 << (int)ActorClassification; }
        }

        protected override void OnInitialise(string name)
        {
            base.OnInitialise(name);

            BulletData = _game.DataManager.BulletData(name);

            Target = null;
            DamageId = 0;
            InitialVelocity = Vector2.Zero;

            DeactivateMe = false;
            _horizontalConstrainBehaviour = ConstrainBehaviour.Deactivate;
            _verticalConstrainBehaviour = ConstrainBehaviour.Deactivate;
        }

        /// <summary>
        /// 
        /// </summary>
	    protected override void OnAddedToScene()
	    {
            if (BulletData.Spread != 0)
            {
                Direction += RandomHelper.DeterministicRandom.GetFloat(-BulletData.Spread, BulletData.Spread);
                Rotation = Direction;
            }

            float speed;
            if (BulletData.SpeedMin != BulletData.SpeedMax)
                speed = RandomHelper.DeterministicRandom.GetFloat(BulletData.SpeedMin, BulletData.SpeedMax);
            else
                speed = BulletData.SpeedMax;

            Velocity = InitialVelocity + VectorHelper.Polar(speed, Direction);

            _lifeTicks = BulletData.LifeTicks;
	    }

        protected override void OnConstrained(bool constrainedTop, bool constrainedLeft, bool constrainedBottom, bool constrainedRight) 
        {
            Explode();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void OnUpdate(GameTime gameTime)
        {
            base.OnUpdate(gameTime);

            Velocity *= BulletData.Friction;

            if (DeactivateMe && (_previousPosition == Position)) 
            {
			    Velocity = Vector2.Zero;
			    Deactivate();
                Explode();
		    }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collider"></param>
        /// <param name="time"></param>
        protected override void OnCollision(Actor collider, float time)
        {
            //Debug.Assert(collider.ActorClassification == ActorClassification.Alien, "Expecting an alien type here");
            var alien = collider as Alien;
            if (alien != null)
                alien.HandleBulletHit(this);

            if (collider.Health != Actor.InfiniteHealth)
			{
				collider.PlayerId = _playerId;
		        collider.RegisterHit(this, BulletData.Damage[DamageId].Damage);
                collider._hitTicks = 8;
			}
            
            if (collider.Active == false)
            {
                _game.CameraManager.ShakeCamera(0.05f, 5);
                _game.ScreenFlashManager.AddFlash(Bullet.AlienFlash, 5);
                _game.CameraManager.ShakeCamera(0.25f, 5);
                _game.VibrationManager.AddRightVibration(_game.GetPlayer(_playerId).PlayerIndex, 0.75f, 10);
                
                if (collider.ActorClassification == ActorClassification.Alien)
                        ((Alien)collider).Kill();
            }

            _health = 0; //Bullets always die?

            if (_health <= 0)
            {
                //Fast moving entities look like they have not reached their target (due to interpolation
                //between previous and current frame - set a flag to deactivate next time
                //TODO: Transition
                if (BulletData.FastMoving)
                {
                    DeactivateMe = true;
                    _collidable = false;

                    Velocity *= time;
                }
                else
                {
                    Velocity = Vector2.Zero;
			        Deactivate();
                    Explode();
                }
            }
        }

        public void TargetKilled()
        {
            Target = null;
            OnTargetKilled();
        }

        protected virtual void OnTargetKilled() { }

        protected override void  OnDeactivate()
        {
            if (Target != null) 
            {
		        Target.Bullet = null;
                Target = null;
	        }
        }
    }
}
