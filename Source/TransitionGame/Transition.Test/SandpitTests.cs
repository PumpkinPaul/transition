﻿using System.IO;
using System.Collections.Specialized;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using ProtoBuf;
using ProtoBuf.Meta;

namespace Transition.Test 
{
    [ProtoContract]
    struct BitMask32
    {
        [ProtoMember(1)]
        uint _flags;

        public BitMask32(uint flags)
        {
            _flags = flags;
        }

        public bool this[uint mask]
        {
            get { return (_flags & mask) == mask;  }
            set { _flags |= mask; }
        }
    }

    [ProtoContract]
    class BoolTest
    {
        [ProtoMember(1)]
        public bool Active { get; set; }

        [ProtoMember(2)]
        public bool Visible { get; set; }

        [ProtoMember(3)]
        public bool Collidable { get; set; }

        [ProtoMember(4)]
        public bool OnGround { get; set; }

        public BoolTest()
        {
            Collidable = true;
        }
    }

    [ProtoContract]
    class BitVectorTest
    {
        [ProtoMember(1)]
        public BitVector32 Flags;

        public BitVectorTest()
        {
            var activeFlag = BitVector32.CreateMask();
            var visibleFlag = BitVector32.CreateMask(activeFlag);
            var collidableFlag = BitVector32.CreateMask(visibleFlag);
            var onGroundFlag = BitVector32.CreateMask(collidableFlag);

            Flags[collidableFlag] = true;
        }
    }

    [ProtoContract]
    class BitMaskTest
    {
        [ProtoMember(1)]
        public BitMask32 Flags;

        public BitMaskTest()
        {
            const int activeFlag = 1;
            const int visibleFlag = 2;
            const int collidableFlag = 4;
            const int onGroundFlag = 8;

            Flags[activeFlag] = false;
            Flags[visibleFlag] = true;
            Flags[collidableFlag] = false;
            Flags[onGroundFlag] = true;
        }
    }

    [ProtoContract]
    public class BitVector32Surrogate
    {
        [ProtoMember(1)]
        public int Data;

        static readonly BitVector32Surrogate Instance = new BitVector32Surrogate();

        public static implicit operator BitVector32Surrogate(BitVector32 value)
        {
            Instance.Data = value.Data;
            return Instance;
        }

        public static implicit operator BitVector32(BitVector32Surrogate value)
        {
            return new BitVector32(value.Data);
        }
    }

    /// <summary>
    /// Responsible for testing serialization / deserialization
    /// </summary>
    /// <remarks>
    /// This forms the basis of our rewinding and replays and MUST be 100% ROBUST. We are allowed to change the game design to facilitate this testing
    /// but we will not got down a TDD rabbit hole. We must also be aware of any performance issues that could arise due to the approach taken here (Interfaces / Garbage)
    /// </remarks>
    [TestFixture, Category(TestCategory.IntegrationTests)]
    public class Sandpit
    {
        static RuntimeTypeModel _runtimeTypeModel;

        [TestCase]
        public void Test1()
        {
            //var flags = new BitVector32(0);

            //var activeFlag = BitVector32.CreateMask();
            //var visibleFlag = BitVector32.CreateMask(activeFlag);
            //var collidableFlag = BitVector32.CreateMask(visibleFlag);
            //var onGroundFlag = BitVector32.CreateMask(collidableFlag);

            //System.Diagnostics.Debug.WriteLine($"{flags[activeFlag]}");

            _runtimeTypeModel = TypeModel.Create();
            _runtimeTypeModel.UseImplicitZeroDefaults = false;
            _runtimeTypeModel.Add(typeof(BitVector32), false).SetSurrogate(typeof(BitVector32Surrogate));

            _runtimeTypeModel.CompileInPlace();

            var boolTest = new BoolTest();

            var ms1 = new MemoryStream();
            //_runtimeTypeModel.SerializeWithLengthPrefix(ms1, boolTest, typeof(BoolTest), PrefixStyle.Base128, 1);
            _runtimeTypeModel.Serialize(ms1, boolTest);
            
            var bitVectorTest = new BitVectorTest();
            var ms2 = new MemoryStream();
            _runtimeTypeModel.Serialize(ms2, bitVectorTest);//, typeof(BitVectorTest), PrefixStyle.Base128, 1);

            var bitMaskTest = new BitMaskTest();
            var ms3 = new MemoryStream();
            _runtimeTypeModel.Serialize(ms3, bitMaskTest);//, typeof(BitVectorTest), PrefixStyle.Base128, 1);

            var intTest = 4;
            var ms4 = new MemoryStream();
            _runtimeTypeModel.Serialize(ms4, intTest);//, typeof(BitVectorTest), PrefixStyle.Base128, 1);

            var uintTest = 5;
            var ms5 = new MemoryStream();
            _runtimeTypeModel.Serialize(ms5, uintTest);//, typeof(BitVectorTest), PrefixStyle.Base128, 1);


            var uintTest2 = 3375;
            var ms52 = new MemoryStream();
            _runtimeTypeModel.Serialize(ms52, uintTest2);//, typeof(BitVectorTest), PrefixStyle.Base128, 1);

            byte byteTest = 17;
            var ms6 = new MemoryStream();
            _runtimeTypeModel.Serialize(ms6, byteTest);//, typeof(BitVectorTest), PrefixStyle.Base128, 1);

            System.Diagnostics.Debug.WriteLine($"boolTest:{boolTest}, position:{ms1.Position}");
            System.Diagnostics.Debug.WriteLine($"bitVectorTest:{bitVectorTest}, position:{ms2.Position}");
            System.Diagnostics.Debug.WriteLine($"bitMaskTest:{bitMaskTest}, position:{ms3.Position}");

            System.Diagnostics.Debug.WriteLine($"intTest:{intTest}, position:{ms4.Position}");
            System.Diagnostics.Debug.WriteLine($"uintTest:{uintTest}, position:{ms5.Position}");
            System.Diagnostics.Debug.WriteLine($"uintTest2:{uintTest2}, position:{ms52.Position}");
            System.Diagnostics.Debug.WriteLine($"byteTest:{byteTest}, position:{ms6.Position}");

            ms1.Dispose();
            ms2.Dispose();
            ms3.Dispose();
            ms4.Dispose();
            ms5.Dispose();
            ms6.Dispose();
        }
    }
}
