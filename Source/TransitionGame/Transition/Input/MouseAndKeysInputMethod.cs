using Microsoft.Xna.Framework.Input;
using Nuclex.Input;
using Transition.Menus;

namespace Transition.Input
{
    /// <summary>
    /// Represents game input from a mouse and keyboard
    /// </summary>
    /// <remarks>
    /// This is a complex control method akin to a desktop OS
    /// </remarks>
    /// <example>
    /// You might use this if you nee full WIMP style control (for an editor maybe)
    /// </example>
    public class MouseAndKeysInputMethod : KeyboardInputMethod
    {
        public override bool SupportsMouse { get { return true; } }

        public override bool IsBeingUsed() 
        {
            var mouseDetected = MouseHelper.HasMouseMoved || MouseHelper.IsLeftButtonJustDown || MouseHelper.IsRightButtonJustDown;
            if (mouseDetected)
                return true;
            
            return base.IsBeingUsed();
        }

        public override GameInput GatherGameInput(ExtendedPlayerIndex playerIndex, PlayerProfile playerProfile)
        {
            var gameInput = new GameInput();

            if (MouseHelper.IsRightButtonJustDown)
                gameInput.AltFire = true;

            SetGameInputFromKeyboard(ref gameInput, playerProfile);
            
            return gameInput;
        }

        public override MenuInput GatherMenuInput(MenuManager menuManager, ExtendedPlayerIndex playerIndex, PlayerProfile playerProfile)
        {
            var menuInput = new MenuInput();

            //Mouse
            menuInput.PointerPosition = menuManager.MouseWorldPosition;
            //menuInput.SelectPressed = MouseHelper.MouseLeftButtonPressed;
            //menuInput.SelectJustPressed = MouseHelper.MouseLeftButtonJustPressed;
            //menuInput.SelectJustReleased = MouseHelper.MouseLeftButtonJustReleased;
         
            //Keyboard
            menuInput.SelectPressed = menuInput.SelectPressed || KeyboardHelper.IsKeyPressed(Keys.Enter);
            menuInput.SelectJustPressed = menuInput.SelectJustPressed || KeyboardHelper.IsKeyJustPressed(Keys.Enter);
            menuInput.SelectJustReleased = menuInput.SelectJustReleased || KeyboardHelper.IsKeyJustReleased(Keys.Enter);
            menuInput.PreviousControlPressed = KeyboardHelper.IsKeyJustPressed(Keys.Tab) && KeyboardHelper.Keyboard.IsKeyDown(Keys.LeftShift);
            menuInput.NextControlPressed = KeyboardHelper.IsKeyJustPressed(Keys.Tab) && KeyboardHelper.Keyboard.IsKeyUp(Keys.LeftShift);
            menuInput.IncreaseValuePressed = KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Right);
	        menuInput.DecreaseValuePressed = KeyboardHelper.IsKeyJustPressed(playerProfile.InputKeys.Left);
            menuInput.BackJustPressed = menuInput.BackJustPressed || KeyboardHelper.IsKeyJustPressed(Keys.Escape);

            //Keyboard held
            menuInput.PreviousControlPressed = HandleHeldAction(ref menuInput, KeyboardHelper.Keyboard.IsKeyDown(Keys.Tab) && KeyboardHelper.Keyboard.IsKeyDown(Keys.LeftShift), menuInput.PreviousControlPressed, ref PreviousControlHeldTicks);
            menuInput.NextControlPressed = HandleHeldAction(ref menuInput, KeyboardHelper.Keyboard.IsKeyDown(Keys.Tab) && KeyboardHelper.Keyboard.IsKeyUp(Keys.LeftShift),  menuInput.NextControlPressed, ref NextControlHeldTicks);
            menuInput.DecreaseValuePressed = HandleHeldAction(ref menuInput, KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Left), menuInput.DecreaseValuePressed, ref DecreaseValueHeldTicks);
            menuInput.IncreaseValuePressed = HandleHeldAction(ref menuInput, KeyboardHelper.Keyboard.IsKeyDown(playerProfile.InputKeys.Right), menuInput.IncreaseValuePressed, ref IncreaseValueHeldTicks);

            return menuInput;
        }
    }
}
