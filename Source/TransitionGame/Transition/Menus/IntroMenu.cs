using System;
using Microsoft.Xna.Framework.Input;
using Nuclex.Input;
using Transition.GamePhases;
using Transition.Input;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the intro menu.
    /// </summary>
    public class IntroMenu : Menu
    {
        NotificationPopupControl _profilePopup;
        TextLabelControl _xboxGamePadLabel1;
        TextLabelControl _xboxGamePadLabel2;
        TextLabelControl _directInputGamePadLabel1;
        TextLabelControl _directInputGamePadLabel2;
        ImageControl _aButton;
        ImageControl _xboxGamePadImage;
        ImageControl _directInputGamePadImage;

        TextLabelControl _keysLabels1;
        TextLabelControl _keysLabels2;
        TextLabelControl _keysLabels3;
        TextLabelControl _keysHelptext;
            
        private string _signedInProfile = "Player 1";

        /// <summary>
        /// Initialises a new instance of a IntroMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
        public IntroMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            base.OnConfigureMenu();

            _xboxGamePadLabel1 = GetControl<TextLabelControl>("xboxGamePadLabel1");
            _xboxGamePadLabel2 = GetControl<TextLabelControl>("xboxGamePadLabel2");
            _aButton = GetControl<ImageControl>("aButton");
            _xboxGamePadImage = GetControl<ImageControl>("xboxGamePadImage");

            _keysLabels1 = GetControl<TextLabelControl>("keysLabels1");
            _keysLabels2 = GetControl<TextLabelControl>("keysLabels2");
            _keysLabels3 = GetControl<TextLabelControl>("keysLabels3");
            _keysHelptext = GetControl<TextLabelControl>("keysHelptext");

            _directInputGamePadLabel1 = GetControl<TextLabelControl>("directInputGamePadLabel1");
            _directInputGamePadLabel2 = GetControl<TextLabelControl>("directInputGamePadLabel2");
            _directInputGamePadImage = GetControl<ImageControl>("directInputGamePadImage");

            _profilePopup = GetControl<NotificationPopupControl>("profilePopup");
            var versionLabel = GetControl<TextLabelControl>("versionLabel");
            var startButton = GetControl<TextButtonControl>("startButton");

            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            
            versionLabel.Text += " " + version;
            #if PROFILE
                versionLabel.Text += ".PROFILE";
            #elif RELEASE
                //NAR
            #elif DEBUG
                versionLabel.Text += ".DEBUG";
            #else
                versionLabel.Text = "ver 0.0.1.UNKNOWN";
            #endif

            versionLabel.FontSize = Theme.FontSize * 0.75f;

            startButton.Click += StartButtonClick;
            
            _profilePopup.ImageScale = _profilePopup.FontSize * 0.67f;
            _profilePopup.MaxVisibleTicks = NotificationPopupControl.StayVisible;  
            _profilePopup.Click += (sender, args) => ShowProfilesMenu();
            _profilePopup.Reset();

            PlayerSignedInOrOut();
        }

        /// <summary>
        /// Gets whether the menu allows itself to be closed with the back button.
        /// </summary>
        public override bool AllowCloseWhenBackPressed
        {
            get { return false; }
        }

        protected override void OnAddedToManager()
        {
            var defaultKey = Game.PlayerProfileManager.GetProfileInfo(ExtendedPlayerIndex.One).DisplayName;
            var key = Game.GameOptions.SignedInProfile;
            
            Game.ActivePlayer.Profile =  Game.PlayerProfileManager.GetProfile(key) ?? Game.PlayerProfileManager.GetProfile(defaultKey);

            if (Game.ActivePlayer.Profile.ProfileType != ProfileType.Local)
                Game.GameOptions.SignedInProfile = string.Empty;

            _profilePopup.Reset();
            PlayerSignedInOrOut();

            Game.GamepadInputMethod.CanChangeInputMethodIndex = true;

            var menu = MenuManager.GetMenu(typeof(ProfilesMenu));
            menu.AfterClosed += ProfilesMenuClosed;

            _keysLabels1.Visible = (Game.ActivePlayer.Profile.InputKeys.Fire == Keys.X  
                    && Game.ActivePlayer.Profile.InputKeys.Up == Keys.Up && Game.ActivePlayer.Profile.InputKeys.Down == Keys.Down 
                    && Game.ActivePlayer.Profile.InputKeys.Left == Keys.Left && Game.ActivePlayer.Profile.InputKeys.Right == Keys.Right);

            _keysLabels2.Visible = _keysLabels1.Visible;
            _keysLabels3.Visible = _keysLabels1.Visible;
            _keysHelptext.Visible = _keysLabels1.Visible;
        }

        protected override void OnRemovedFromManager() 
        {
            var menu = MenuManager.GetMenu(typeof(ProfilesMenu));
            menu.AfterClosed -= ProfilesMenuClosed;
        }

        private void ShowProfilesMenu()
        {
            _signedInProfile = Game.GameOptions.SignedInProfile;
            var menu = MenuManager.GetMenu(typeof(ProfilesMenu));
            MenuManager.SetPrimaryControllerId(); 
            MenuManager.FadeIn();
            MenuManager.AddMenu(menu);
        }

        private void ProfilesMenuClosed(object o, EventArgs e)
        {
            MenuManager.SetPrimaryControllerId();

            //Was there a change of signed in profile?
            if (_signedInProfile != Game.GameOptions.SignedInProfile)
            {
                _signedInProfile = Game.GameOptions.SignedInProfile;

                PlayerSignedInOrOut();
                _profilePopup.FadeOut();                        
            }
        }

        private void PlayerSignedInOrOut()
        {
            if (string.IsNullOrEmpty(Game.GameOptions.SignedInProfile) == false)
                _profilePopup.AddMessage("Welcome back " + Game.GameOptions.SignedInProfile);
            else
                _profilePopup.AddMessage("Not signed in - click here to choose a profile");
        }

        /// <summary>
        /// Allows a derived menu to update itself.
        /// </summary>
        protected override void OnUpdateMustCallBase()
        {
            base.OnUpdateMustCallBase();

            #if WINDOWS || WINDOWS_STOREAPP || LINUX || MONOMAC
                //TEST for an Xbox360 gamepad - what shall we do about Direct Input here?
                if (GamePadHelper.IsGamePadConnected(ExtendedPlayerIndex.One) || GamePadHelper.IsGamePadConnected(ExtendedPlayerIndex.Two) || GamePadHelper.IsGamePadConnected(ExtendedPlayerIndex.Three) || GamePadHelper.IsGamePadConnected(ExtendedPlayerIndex.Four))
                {
                    _xboxGamePadLabel1.Visible = true;
                    _xboxGamePadLabel2.Visible = true;
                    _aButton.Visible = true;
                    _xboxGamePadImage.Visible = true;
                }
                else
                {
                    _xboxGamePadLabel1.Visible = false;
                    _xboxGamePadLabel2.Visible = false;
                    _aButton.Visible = false;   
                    _xboxGamePadImage.Visible = false;
                }

                if (GamePadHelper.IsGamePadConnected(ExtendedPlayerIndex.Five) || GamePadHelper.IsGamePadConnected(ExtendedPlayerIndex.Six) || GamePadHelper.IsGamePadConnected(ExtendedPlayerIndex.Seven) || GamePadHelper.IsGamePadConnected(ExtendedPlayerIndex.Eight))
                {
                    _directInputGamePadLabel1.Visible = true;
                    _directInputGamePadLabel2.Visible = true;
                    _directInputGamePadImage.Visible = true;
                }
                else
                {
                    _directInputGamePadLabel1.Visible = false; 
                    _directInputGamePadLabel2.Visible = false;
                    _directInputGamePadImage.Visible = false;
                }
            #endif
        }

        private void ShowMainMenu()
        {
            MenuManager.SetPrimaryControllerId();          

            Game.ChangePhase(typeof(MenuGamePhase));
            MenuManager.FadeIn();
            MenuManager.AddMenu(typeof(MainMenu));

            //TODO: Transition
            ((TransitionGame)Game).Scene.RemoveAllActors();
            //TODO: Transition
            ((TransitionGame)Game).Scene.KillParticles();

            if (Game.GameOptions.ReplayPlaybackMode == ReplayPlaybackMode.Background)
                //TODO: Transition
                ((TransitionGame)Game).DemoFinished();
        }

        /// <summary>
        /// Event handler for the start button click.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The data for the event.</param>
        private void StartButtonClick(object sender, EventArgs e)
        {
            ShowMainMenu();
        }

        protected override void OnClosed() 
        {
            Game.GamepadInputMethod.CanChangeInputMethodIndex = false;
        }
    }
}
