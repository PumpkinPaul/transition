﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Krypton.Common;

namespace Krypton.Lights
{
    public class Light2D : ILight2D
    {
        bool _isOn = true;
        Vector2 _position = Vector2.Zero;
        float _angle;
        Texture2D _texture;
        Color _color = Color.White;
        float _range = 1;
        float _fov = MathHelper.TwoPi;
        float _intensity = 1;

        /// <summary>
        /// The light's position
        /// </summary>
        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        /// <summary>
        /// The X coordinate of the light's position
        /// </summary>
        public float X
        {
            get { return _position.X; }
            set { _position.X = value; }
        }

        /// <summary>
        /// The Y coordinate of the light's position
        /// </summary>
        public float Y
        {
            get { return _position.Y; }
            set { _position.Y = value; }
        }

        /// <summary>
        /// The light's angle
        /// </summary>
        public float Angle
        {
            get { return _angle; }
            set { _angle = value; }
        }

        /// <summary>
        /// The texture used as the base light map, from which shadows will be subtracted
        /// </summary>
        public Texture2D Texture
        {
            get { return _texture; }
            set { _texture = value; }
        }

        /// <summary>
        /// The color used to tint the light's texture
        /// </summary>
        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        /// <summary>
        /// The light's maximum radius (or "half width", if you will)
        /// </summary>
        public float Range
        {
            get { return _range; }
            set { _range = value; }
        }

        /// <summary>
        /// Gets or sets the light's field of view. This value determines the angles at which the light will cease to draw
        /// </summary>
        public float Fov
        {
            get { return _fov; }
            set
            {
                _fov = MathHelper.Clamp(value, 0, MathHelper.TwoPi);
            }
        }

        /// <summary>
        /// Gets or sets the light's intensity. Think pixel = (tex * color) ^ (1 / intensity)
        /// </summary>
        public float Intensity
        {
            get { return _intensity; }
            set { _intensity = MathHelper.Clamp(value, 0.01f, 3f); }
        }

        /// <summary>
        /// Gets or sets a value indicating weither or not to draw the light
        /// </summary>
        public bool IsOn
        {
            get { return _isOn; }
            set { _isOn = value; }
        }

        /// <summary>
        /// Draws shadows from the light's position outward
        /// </summary>
        /// <param name="helper">A render helper for drawing shadows</param>
        /// <param name="hulls">The shadow hulls used to draw shadows</param>
        public void Draw(KryptonRenderHelper helper, List<ShadowHull> hulls)
        {
            // Draw the light only if it's on
            if (!_isOn)
                return;

            // Make sure we only render the following hulls
            helper.ShadowHullVertices.Clear();
            helper.ShadowHullIndicies.Clear();

            // Loop through each hull
            foreach (var hull in hulls)
            {
                //if(hull.Bounds.Intersects(this.Bounds))
                // Add the hulls to the buffer only if they are within the light's range
                if (hull.Visible && IsInRange(hull.Position - Position, hull.MaxRadius * Math.Max(hull.Scale.X, hull.Scale.Y) + Range))
                {
                    helper.BufferAddShadowHull(hull);
                }
            }

            var shadowEffect = helper.Effect.Techniques["PointLight_Shadow_Fast"];
            helper.Effect.CurrentTechnique = shadowEffect;
            // Set the effect parameters
            helper.Effect.Parameters["LightPosition"].SetValue(_position);
            helper.Effect.Parameters["Texture0"].SetValue(_texture);
            helper.Effect.Parameters["LightIntensityFactor"].SetValue(1 / (_intensity * _intensity));

            shadowEffect.Passes["ShadowStencil"].Apply();
            helper.BufferDraw();

            shadowEffect.Passes["Light"].Apply();
            helper.DrawClippedFov(_position, _angle, _range * 2, _color, _fov);
        }

        /// <summary>
        /// Determines if a vector's length is less than a specified value
        /// </summary>
        /// <param name="offset">Offset</param>
        /// <param name="dist">Distance</param>
        /// <returns></returns>
        static bool IsInRange(Vector2 offset, float dist)
        {
            if (offset.X * offset.X + offset.Y * offset.Y < dist * dist)
                return true;

            return false;
        }

        /// <summary>
        /// Gets the world-space bounds which contain the light
        /// </summary>
        public BoundingRect Bounds
        {
            get
            {
                BoundingRect rect;

                rect.Min.X = _position.X - _range;
                rect.Min.Y = _position.Y - _range;
                rect.Max.X = _position.X + _range;
                rect.Max.Y = _position.Y + _range;

                return rect;
            }
        }
    }
}
