using System;
using Microsoft.Xna.Framework;
using Nuclex.Input;
using Transition.Input;
using Transition.Rendering;

namespace Transition.GamePhases
{
    /// <summary>
    /// Handles the splash screen at the beginning of the game.
    /// </summary>
    public class SplashScreenPhase : GamePhase
    {
        Quad _splashQuadStruct;
        Vector2 _splashScale = new Vector2(16.0f, 16.0f);
        Vector4 _splashTint = Vector4.One;

        public event EventHandler SplashComplete;

        public SplashScreenPhase(BaseGame game) : base(game) { }

        protected override void OnUpdate()
        {
            var skip = false;

            for (var i = 0; i < 8; ++i)
            {
                if (GamePadHelper.IsAJustPressed((ExtendedPlayerIndex)i) || GamePadHelper.IsStartJustPressed((ExtendedPlayerIndex)i))
                {
                    skip = true;
                    break;
                }
            }

            if (MouseHelper.IsLeftButtonJustUp || MouseHelper.IsRightButtonJustDown)
                skip = true;

            if (KeyboardHelper.IsKeyJustPressed(Microsoft.Xna.Framework.Input.Keys.Escape) || KeyboardHelper.IsKeyJustPressed(Microsoft.Xna.Framework.Input.Keys.Space) || KeyboardHelper.IsKeyJustPressed(Microsoft.Xna.Framework.Input.Keys.Enter))
                skip = true;

            if (ElapsedTicks > 100)
            {
                _splashTint.W -= 0.01f;
            }

            _splashScale *= 1.001f;
            _splashQuadStruct = new Quad(Vector2.Zero, _splashScale, 0.0f);

            
            if (ElapsedTicks > 250 || skip)
            {
                Game.SoundManager.PlaySound("SplashDone");
                OnSplashComplete(EventArgs.Empty);
                return;
            }
        }

        protected override void OnRender()
        {
            var logoSprite = Game.TextureManager.GetTextureInfo("PumpkinSplash");

            Game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);
            Game.Renderer.DrawSprite(ref _splashQuadStruct, logoSprite.TextureId, logoSprite.SourceRect, new Color(_splashTint), TransparentRenderStyle.Instance);
            Game.Renderer.End();
        }

        protected void OnSplashComplete(EventArgs e)
        {
            var handler = SplashComplete;
            handler?.Invoke(this, e);
        }
    }
}
