using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Manages the screen flashes.
    /// </summary>
    public class ScreenFlashManager
    {
        readonly List<ScreenFlash> _screenFlashes = new List<ScreenFlash>();

        Color _clearColor;
        Color _currentColor;
		
		/// <summary>
		/// Gets the current colour of the screen.
		/// </summary>
		public Color CurrentColor
		{
			get { return _currentColor; }
		}
		
		/// <summary>
		/// Gets or sets the default colour of the screen.
		/// </summary>
		public Color ClearColor
		{
			get { return _clearColor; }
			set 
			{ 
				_clearColor = value; 
				_currentColor = value; 
			}
		}

        /// <summary>
        /// Adds a screen flash to the background.
        /// </summary>
        /// <param name="color">The colour of the flash.</param>
        /// <param name="duration">How long the flash lasts - flashes fade to the default colur of time.</param>
        public void AddFlash(Color color, int duration)
		{
            var flash = new ScreenFlash(color, duration);
            _screenFlashes.Add(flash);
		}
		
		/// <summary>
		/// .
		/// </summary>
		public void Update()
		{
			UpdateScreenFlash();
		}
		
		/// <summary>
		/// .
		/// </summary>
		private void UpdateScreenFlash()
		{
			//Calculate how much to 'flash' the screen this update
			_currentColor = _clearColor;

			for (var i = 0; i < _screenFlashes.Count; ++i)
			{
				var flash = _screenFlashes[i];

				//If the current flash has finished remove it from the list
                if (flash.Ticks <= 0) 
				{
					_screenFlashes.RemoveAt(i);
					--i;
				}
				//Add the flash colour to the final screen colour - allows us to combine flashes of different colour.
				else 
				{
					flash.Ticks--;
                    var ratio = flash.Ticks / (float)flash.Duration;
                    _currentColor.R += (byte)(flash.Color.R * ratio);
                    _currentColor.G += (byte)(flash.Color.G * ratio);
                    _currentColor.B += (byte)(flash.Color.B * ratio);
                    _currentColor.A += (byte)(flash.Color.A * ratio); 
				}
			}
		}
    }
}
