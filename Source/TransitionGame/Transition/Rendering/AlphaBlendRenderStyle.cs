using Microsoft.Xna.Framework.Graphics;
using Transition.ResourceManagers;

namespace Transition.Rendering
{
    /// <summary>
    /// An style to handle Alpha blending
    /// </summary>
    public class AlphaBlendingRenderStyle : RenderStyle
    {
	    public static AlphaBlendingRenderStyle Instance;

        public override RenderStyleType RenderStyleId => RenderStyleType.AlphaBlend;

        protected override void OnRegister(EffectManager effectManager)
        {
            Instance = this;
            Effect = effectManager.GetEffect("BasicEffect");
        }

        public override void Setup(GraphicsDevice graphicsDevice)
        {
            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
        }
    }
}
