using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Transition.Animations;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Manages animation configuration data.
	/// </summary>
    public class AnimationManager : ResourceManager
    {
        public override string FolderName { get { return "Animations"; } }

	    readonly Dictionary<string, Animation> _animations = new Dictionary<string, Animation>();
                
        /// <summary>
        /// Loads all actor configuration data into the data manager.
        /// </summary>
        public override void LoadContent()
        {
            var folder = Path.Combine(Platform.ResourcesPath, FolderName);

            foreach (var filename in Directory.GetFiles(folder, "*.xml"))
                LoadAnimation(filename);
        }

        private void LoadAnimation(string filename)
        {
            var xdocument = XDocument.Load(filename);
			
            if (xdocument.Root == null)
                throw new XmlException("Missing Root element in " + filename);

			//Read in all of the animation...
            foreach (var actorElement in xdocument.Root.Elements("animation"))
            {
                var animation = new Animation();
                animation.Name = actorElement.GetAttributeString("name");

                foreach (var element in actorElement.Elements())
                {
                    Command command = null;
                    switch (element.Name.LocalName)
                    {
                        case "event":
                            command = new EventCommand();
                            break;

                        case "frame":
                            command = new FrameCommand();
                            break;

                        case "goto":
                            command = new GotoCommand();
                            break;

                        case "label":
                            command = new LabelCommand();
                            break;
                    }

                    if (command == null)
                        throw new InvalidOperationException("Unknonwn command (" + element.Name.LocalName + ") in " + filename);

                    command.Load(element);

                    //TODO: Transition - Sprites -move this!
                    command.Create();
                    animation.AddCommand(command);
                    
                }

                _animations[animation.Name] = animation;
            }
        }

        public Animation Get(string name)
        {
            return _animations.Get(name);
        }
    }
}
