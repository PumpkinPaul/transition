﻿using Microsoft.Xna.Framework;
using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Flier enemy
    /// </summary>
    [ProtoContract]
    public class Flier : Enemy
    {
        const int MaxCountdownTicks = 240;
        public const float SpeedY = 0.025f;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        int _countdownTicks;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public Flier(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _countdownTicks = MaxCountdownTicks;

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-09.face-right.animation")
                    : Game.AnimationManager.Get("enemy-09.face-left.animation"));

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnAddedToWorld(World world)
        {
            Velocity.Y = SpeedY;
            _countdownTicks = RandomHelper.FastRandom.Next(0, MaxCountdownTicks);
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            var player = Game.World.Get<Player>(EntityKeys.Player);
            IsFacingRight = player.Position.X > Position.X;

            Sprites[0].SetAppearance(IsFacingRight
                    ? Game.AnimationManager.Get("enemy-09.face-right.animation")
                    : Game.AnimationManager.Get("enemy-09.face-left.animation"));

            Game.GameMap.HandleCollisions(this, respectPlatforms: false);

            _countdownTicks--;

            if (_countdownTicks > 0)
                return;

            _countdownTicks = MaxCountdownTicks;

            if (IsFacingRight && player.Position.X > Position.X || IsFacingLeft && player.Position.X < Position.X)
                Shoot();
        }

        void Shoot()
        {
            if (IsPlayerInRange == false)
                return;

            var player = Game.World.Get<Player>(EntityKeys.Player);

            var offset = new Vector2(RandomHelper.FastRandom.NextFloat(-2.0f, 2.0f));
            var speed = RandomHelper.FastRandom.NextFloat(0.075f, 0.1f);
            var angle = (float)VectorHelper.GetAngle(Position, player.Position + offset);
            var velocity = VectorHelper.Polar(speed, angle);

            var bullet = Game.EntityFactory.Create(typeof(EnemyBullet), "FlierBullet");
            if (bullet != null)
            {
                bullet.Position = Position;
                bullet.Velocity = velocity;
                Game.World.Add(bullet);
            }
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge)
        {
            Position += Velocity;

            switch (entityCollisionEdge)
            {
                case CollisionEdge.Top:
                    Velocity.Y = -SpeedY;
                    IsFacingRight = true;
                    break;

                case CollisionEdge.Bottom:
                    Velocity.Y = SpeedY;
                    IsFacingRight = false;
                    break;
            }
        }
    }
}