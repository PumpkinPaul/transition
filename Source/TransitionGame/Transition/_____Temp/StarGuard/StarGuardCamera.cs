//using Microsoft.Xna.Framework;

//namespace Transition.Cameras
//{
//    public class StarGuardCamera : Camera
//    {
//        private Vector3 _anchor;
//        private Vector3 _location;
//        int _ticks;
//        private int _duration;

//        bool _playerWasOnGround;

//        private bool _trackY;

//        public StarGuardCamera(BaseGame game) : base(game) 
//        { 
//            _location = Position;
//        }

//        public override void Update()
//        {
//            //Just have the proxy do it's stuff.
//            //SetLocation();
//            //UpdateLocation();
//        }

//        void UpdateLocation()
//        {
//            if (_duration > 0)
//            {
//                _ticks++;
//                if (_ticks > _duration)
//                {
//                    _ticks = 0;
//                    _duration = 0;
//                }

//                var ratio = _duration > 0 ? (float)_ticks / _duration : 1;
//                Position.Y = MathHelper.Lerp(_anchor.Y, _location.Y, ratio);
//            }
//            else
//                Position.Y = _location.Y;
//        }

//        void SetLocation()
//        {
//            var player = TransitionGame.Instance.Player;

//            Position.X = player.Position.X;

//            //If the player steps off ground track his y location directly / instantly
//            if (_playerWasOnGround && !player.OnGround && player.Velocity.Y < 0)
//                _trackY = true;
//            else if (_trackY && player.OnGround)
//                _trackY = false;

//            if (_trackY)
//            {
//                if (Position.Y >= player.Position.Y)
//                    MoveToY(player.Position.Y, 0);
//            }
//            else
//            {
//                if (player.OnGround && Position.Y < player.Position.Y)
//                {
//                    MoveToY(player.Position.Y, 45);
//                }
//                else if (player.OnGround && Position.Y > player.Position.Y)
//                {
//                    MoveToY(player.Position.Y, 10);
//                }
//            }

//            _playerWasOnGround = player.OnGround;
//        }

//        public void MoveToY(float y, int ticks)
//        {
//            _duration = ticks;
//            _ticks = 0;
//            _location = new Vector3(Position.X, y, Position.Z);
//            _anchor = Position;
//        }

//        public override void Render()
//        {
//            View = Matrix.CreateTranslation(new Vector3(-Position.X, -Position.Y, Position.Z));
//        }
//    }
//}
