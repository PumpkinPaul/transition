using System.Globalization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.Rendering
{
    public struct VertexPositionColorTextureFloat : IVertexType
    {
        // ReSharper disable MemberCanBePrivate.Global
		public readonly Vector3 Position;
        public readonly Color Color;
        public readonly Vector2 TextureCoordinate;
        public readonly Vector2 Value;
        // ReSharper restore MemberCanBePrivate.Global

		public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0), 
            new VertexElement(12, VertexElementFormat.Color, VertexElementUsage.Color, 0), 
            new VertexElement(16, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(24, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 1)
            )

		{
			Name = "VertexPositionColorTextureFloat.VertexDeclaration"
		};

		/// <summary>Gets a vertex declaration.</summary>
		VertexDeclaration IVertexType.VertexDeclaration { get { return VertexDeclaration; } }

        /// <summary>Initializes a new instance of the VertexPositionColorTexture class.</summary>
        /// <param name="position">Position of the vertex.</param>
        /// <param name="color">Color of the vertex.</param>
        /// <param name="textureCoordinate">Texture coordinate of the vertex.</param>
        /// <param name="value"></param>
        public VertexPositionColorTextureFloat(Vector3 position, Color color, Vector2 textureCoordinate, float value)
		{
			Position = position;
			Color = color;
			TextureCoordinate = textureCoordinate;
            Value = new Vector2(value);
		}

		/// <summary>Gets the hash code for this instance.</summary>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = (int)2166136261;
                // Suitable nullity checks etc, of course :)
                hash = hash * 16777619 ^ Position.GetHashCode();
                hash = hash * 16777619 ^ Color.GetHashCode();
                hash = hash * 16777619 ^ TextureCoordinate.GetHashCode();
                hash = hash * 16777619 ^ Value.GetHashCode();
                return hash;
            }
        }

		/// <summary>Retrieves a string representation of this object.</summary>
		public override string ToString()
		{
			return string.Format(CultureInfo.CurrentCulture, "{{Position:{0} Color:{1} TextureCoordinate:{2}, Value{3}}}", Position, Color, TextureCoordinate, Value);
		}
		/// <summary>Compares two objects to determine whether they are the same.</summary>
		/// <param name="left">Object to the left of the equality operator.</param>
		/// <param name="right">Object to the right of the equality operator.</param>
		public static bool operator ==(VertexPositionColorTextureFloat left, VertexPositionColorTextureFloat right)
		{
			return left.Position == right.Position && left.Color == right.Color && left.TextureCoordinate == right.TextureCoordinate;
		}
		/// <summary>Compares two objects to determine whether they are different.</summary>
		/// <param name="left">Object to the left of the inequality operator.</param>
		/// <param name="right">Object to the right of the inequality operator.</param>
		public static bool operator !=(VertexPositionColorTextureFloat left, VertexPositionColorTextureFloat right)
		{
			return !(left == right);
		}
		/// <summary>Returns a value that indicates whether the current instance is equal to a specified object.</summary>
		/// <param name="obj">The Object to compare with the current VertexPositionColorTexture.</param>
		public override bool Equals(object obj)
		{
			return obj != null && !(obj.GetType() != GetType()) && this == (VertexPositionColorTextureFloat)obj;
		}
    }
}
