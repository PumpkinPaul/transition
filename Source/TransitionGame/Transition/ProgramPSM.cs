#if PSM
	using System;
	using System.IO;
	
	using Sce.PlayStation.Core;
	using Sce.PlayStation.Core.Environment;
	using Sce.PlayStation.Core.Graphics;
	using Sce.PlayStation.Core.Imaging;
	using Sce.PlayStation.Core.Input;
	
	
	namespace Doppelganger
	{
		public class ProgramPsm
		{
			static protected GraphicsContext graphics;
			
			public static void Main (string[] args)
			{	
				using (var game = new DoppelgangerGame())
					game.Run();
			}
		}
	}
#endif