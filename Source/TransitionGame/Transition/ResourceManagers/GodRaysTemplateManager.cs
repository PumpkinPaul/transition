using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Manages god rays template configuration data.
	/// </summary>
    public class GodRaysTemplateManager : ResourceManager
    {
        public override string FolderName { get { return "GodRays"; } }

        readonly TextureManager _textureManager;

	    readonly Dictionary<string, GodRaysTemplate> _templates = new Dictionary<string, GodRaysTemplate>();

		/// <summary>
        /// Creates a new instance of a GodRaysTemplateManager.
        /// </summary>
        public GodRaysTemplateManager(TextureManager textureManager)
        {
            if (textureManager == null)
                throw new ArgumentNullException(nameof(textureManager));

            _textureManager = textureManager;
        }
        
        /// <summary>
        /// Loads all actor configuration data into the data manager.
        /// </summary>
        public override void LoadContent()
        {
            var folder = Path.Combine(Platform.ResourcesPath, FolderName);

            foreach (var filename in Directory.GetFiles(folder, "*.xml"))
                LoadTemplateData(filename);
        }

		/// <summary>
        /// Load the template configuration data.
        /// </summary>
        private void LoadTemplateData(string filename)
        {
            var xdocument = XDocument.Load(filename);

            if (xdocument.Root == null)
                throw new XmlException("Missing Root element in " + filename);

            var templateName = Path.GetFileNameWithoutExtension(filename);

            if (templateName == null)
                throw new XmlException("Invalid filename:" + filename);

            var godRaysElement = xdocument.Root;
   		
			var template = _templates.ContainsKey(templateName) ? _templates[templateName] : new GodRaysTemplate();

            template.RayCount = godRaysElement.GetAttributeInt32("rayCount");

            template.TextureInfo = _textureManager.GetTextureInfo(godRaysElement.GetAttributeString("texture"));
    
            template.MasterAlphaMin = godRaysElement.GetAttributeSingle("masterAlphaMin");
            template.MasterAlphaMax = godRaysElement.GetAttributeSingle("masterAlphaMax");
        
            template.FadeInTicks = godRaysElement.GetAttributeInt32("fadeInTicks");
            template.VisibleTicks = godRaysElement.GetAttributeInt32("visibleTicks");
            template.FadeOutTicks = godRaysElement.GetAttributeInt32("fadeOutTicks");

            template.InnerColor = godRaysElement.GetAttributeColor("innerColor");
            template.OuterColor = godRaysElement.GetAttributeColor("outerColor");

            template.InnerRadiusStart = godRaysElement.GetAttributeSingle("innerRadiusStart");
            template.InnerRadiusEnd = godRaysElement.GetAttributeSingle("innerRadiusEnd");

            template.OuterRadiusStart = godRaysElement.GetAttributeSingle("outerRadiusStart");
            template.OuterRadiusEnd = godRaysElement.GetAttributeSingle("outerRadiusEnd");
            template.OuterRadiusModMin = godRaysElement.GetAttributeSingle("outerRadiusModMin");
            template.OuterRadiusModMax = godRaysElement.GetAttributeSingle("outerRadiusModMax");

            template.WidthStart = godRaysElement.GetAttributeSingle("widthStart");
            template.WidthEnd = godRaysElement.GetAttributeSingle("widthEnd");

            template.RotationMin = godRaysElement.GetAttributeSingle("rotationMin");
            template.RotationMax = godRaysElement.GetAttributeSingle("rotationMax");

			_templates[templateName] = template;         
        }

		/// <summary>
        /// Gets a GodRaysTemplate for a key.
		/// </summary>
        public GodRaysTemplate GetTemplate(string name)
        {
            if (_templates.ContainsKey(name))
                return _templates[name];
            
            return null;
        }
    }
}
