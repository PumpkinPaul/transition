using System;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.Rendering
{
    /// <summary>
    /// A class used to draw batches of sprites.
    /// </summary>
    public class GeometryBuffer<T> : IGeometryBuffer where T : struct, IVertexType
    {
        //This constant controls how large the vertices buffer is. Larger buffers will require growing less often, which can increase performance. However, having
        //buffer that is unnecessarily large will waste memory.
        private const int DefaultSize = 4096;

        //We'll default to 4 for SPRITES here. Some things like BlastEffects will have a LOT more but hey ho! Something to look at
        private const int VerticesPerSprite = 4;
        private const int IndicesPerSprite = 6;

        //The device that we will issue draw calls to.
        private readonly GraphicsDevice _graphicsDevice;

        //Buffers sent to the graphics card containing the data to draw.
        private DynamicVertexBuffer _vertexBuffer;
        private DynamicIndexBuffer _indexBuffer;

        public readonly Type VertexType;

        private T[] _vertices = new T[DefaultSize * VerticesPerSprite];
        private ushort[] _indices = new ushort[DefaultSize * IndicesPerSprite];

        public int VertexPointer { get; set; }
        public int IndexPointer { get; set; }

        //Keeps track of how many vertices have been added. Value increases until we run out of space in the buffer, at which time they grow automatically.
        public int TotalVertices { get; set; }
        public int TotalIndices { get; set; }

        public DynamicVertexBuffer VertexBuffer { get { return _vertexBuffer;  } }

        private bool _isDisposed;

        public GeometryBuffer(GraphicsDevice graphicsDevice)
        {
            _graphicsDevice = graphicsDevice;
            VertexType = typeof(T);

            AllocateGraphicsResources();
        }

        public void AllocateGraphicsResources()
        {
            if ((_vertexBuffer == null) || _vertexBuffer.IsDisposed)
            {
                var vertexType = default(T);

                _vertexBuffer = new DynamicVertexBuffer(_graphicsDevice, vertexType.VertexDeclaration, _vertices.Length, BufferUsage.WriteOnly);
                _vertexBuffer.SetData(_vertices);
                _vertexBuffer.ContentLost += (sender, e) =>
                {
                    TotalVertices = 0;
                    TotalIndices = 0;
                };
            }

            if ((_indexBuffer == null) || _indexBuffer.IsDisposed)
            {
                _indexBuffer = new DynamicIndexBuffer(_graphicsDevice, IndexElementSize.SixteenBits, _indices.Length, BufferUsage.WriteOnly);

                _indexBuffer.SetData(_indices);

                _indexBuffer.ContentLost += (sender, args) => _indexBuffer.SetData(_indices);
            }
        }

        public void Reset()
        {
            VertexPointer = 0;
            IndexPointer = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        void Dispose(bool disposing)
        {
            if (!disposing || _isDisposed)
                return;

            if (_vertexBuffer != null)
                _vertexBuffer.Dispose();

            if (_indexBuffer != null)
                _indexBuffer.Dispose();

            _isDisposed = true;
        }

        public bool BufferWillOverflow(int newVerticesCount, int newIndicesCount)
        {
            return (TotalVertices + newVerticesCount >= _vertices.Length || TotalIndices + newIndicesCount >= _indices.Length);
        }

        public void GrowArrays()
        {
            Array.Resize(ref _vertices, _vertices.Length * 2);
            Array.Resize(ref _indices, _indices.Length * 2);

            _vertexBuffer = null;
            _indexBuffer = null;

            AllocateGraphicsResources();
        }

        public void CopyVertices(Geometry geometry, int count)
        {
            CopyVertices(geometry as Geometry<T>, count);
        }

        void CopyVertices(Geometry<T> geometry, int count)
        {
            geometry.Vertices.CopyTo(_vertices, count);
        }

        public void CopyIndices(ushort[] indices, int indexPointer, int indexCount, int indexOffset)
        {
            for (var i = 0; i < indexCount; i++)
            {
                _indices[indexPointer] = (ushort)(indices[i] + indexOffset);
                indexPointer++;
            }
        }

        public void SetBuffersToGraphicsDevice()
        {
            _graphicsDevice.SetVertexBuffer(_vertexBuffer);
            _graphicsDevice.Indices = _indexBuffer;
        }

        public void SetData()
        {
            _vertexBuffer.SetData(_vertices, 0, _vertices.Length, SetDataOptions.Discard);
            _indexBuffer.SetData(_indices, 0, _indices.Length, SetDataOptions.Discard);
        }
    }
}