using System;
using System.Collections.Generic;
using Nuclex.Input;

namespace Transition.Input
{
    /// <summary>
    /// Manages the input methods.
    /// </summary>
    public class InputMethodManager
    {
        public event EventHandler InputMethodChanged;
        public event EventHandler InputMethodIndexChanged;

        readonly List<InputMethod> _inputMethods = new List<InputMethod>();

        public InputMethod InputMethod { get; set; }

        public void AddInputMethod(InputMethod inputMethod)
        {
            _inputMethods.Add(inputMethod);
        }

        public void DetermineActiveInputMethod(ExtendedPlayerIndex activePlayerIndex)
        {
            //Test to see if other input methods are vying for control!
            foreach(var inputMethod in _inputMethods)
            {
                //Skip the check if the input method is already active UNLESS it supports changing indices (e.g. like a gamepad - where the input method stays the same but the active controller changes)
                if (inputMethod != InputMethod || inputMethod.CanChangeInputMethodIndex)
                {
                    if (inputMethod.IsBeingUsed())
                    {
                        var playerIndexChanged = inputMethod.PlayerIndex != activePlayerIndex;
                        
                        if (InputMethod != inputMethod)
                        {
                            InputMethod = inputMethod;
                            OnInputMethodChanged(EventArgs.Empty);
                        }

                        if (playerIndexChanged)
                            OnInputMethodIndexChanged(EventArgs.Empty);
                        
                        return;
                    }
                }
            }
        }

        protected void OnInputMethodChanged(EventArgs e)
        {
            var handler = InputMethodChanged;
            if (handler != null)
                handler(this, e);
        }

        protected void OnInputMethodIndexChanged(EventArgs e)
        {
            var handler = InputMethodIndexChanged;
            if (handler != null)
                handler(this, e);
        }
    }
}
