using System;
using System.Runtime.Serialization;

namespace Transition
{
    [Serializable]
    public sealed class LinearInterpolator : InterpolatorBase, ISerializable 
    {
	    public static readonly LinearInterpolator Instance = new LinearInterpolator();

	    public override float Interpolate(float a, float b, float ratio) {
		    if (a == b) 
			    return a;
		    
		    if (ratio < 0.0f) 
			    ratio = 0.0f;
		    else if (ratio > 1.0f) 
			    ratio = 1.0f;
		    
		    return a * (1f - ratio) + b * ratio;
	    }

	    public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.SetType(typeof(SingletonSerializationHelper));
        }

        [Serializable]
        private sealed class SingletonSerializationHelper : IObjectReference 
        {
            // GetRealObject is called after this object is deserialized.
            public Object GetRealObject(StreamingContext context) 
            {
                // When deserialiing this object, return a reference to the Singleton object instead.
                return Instance;
            }
        }
    }
}
