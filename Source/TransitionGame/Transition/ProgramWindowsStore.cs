﻿#if WINDOWS_STOREAPP
	using System;

	namespace Doppelganger
	{
	    /// <summary>
	    /// The main class.
	    /// </summary>
	    public static class ProgramWindowsStore
	    {
	        /// <summary>
	        /// The main entry point for the application.
	        /// </summary>
	        static void Main()
	        {
	            var factory = new MonoGame.Framework.GameFrameworkViewSource<DoppelgangerGame>();
	            Windows.ApplicationModel.Core.CoreApplication.Run(factory);
	        }
	    }
	}
#endif