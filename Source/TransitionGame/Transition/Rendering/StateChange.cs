namespace Transition.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public class StateChange
    {
        public int TextureId;
        public RenderStyle RenderStyle;
        public int StartIndex;
    }
}
