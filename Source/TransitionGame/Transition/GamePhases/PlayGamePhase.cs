using Transition.Actors;

namespace Transition.GamePhases
{
    /// <summary>
    /// Handles normal play state - of course, it could be a demo playing here too as well as the player.
    /// </summary>
    public class PlayGamePhase : GamePhase
    {							
        int _player1DeadTicks;

        public PlayGamePhase(BaseGame game) : base(game) { }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnCreate()
        {
            _player1DeadTicks = 0;
        }

		/// <summary>
		///
		/// </summary>
        protected override void OnUpdate()
		{
            if (((TransitionGame)Game).LevelManager.IsPlaying == false)
                return;

            if (Game.DemoRecorder.IsPlaying == false)
                Game.AchievementsManager.Update();

            //Test to see if the player has been destroyed
            if (Game.ActivePlayer.Ship.Active == false)
            {
                if (_player1DeadTicks == 0)
                    Game.ActivePlayer.LoseLife();

                ++_player1DeadTicks;

                if (Game.ActivePlayer.Lives == 0)
                {
                    if (/*_player1DeadTicks == 180 && */Game.PlayerCount == 1)
                        Game.ChangePhase(typeof(GameOverPhase));
                }
                else
                {
                    if (_player1DeadTicks == 120)
                    {
                        _player1DeadTicks = 0;
                        //Add the ship to the scene
                        //TODO: Transition
                        ((TransitionGame)Game).Scene.AddActor(Game.ActivePlayer.Ship);
                    }
                }
            }
		}
    }
}

