using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class TileSelector : Selector
    {
        public TileSelector(SelectionTool selectionTool) : base(selectionTool) { }

        public override int SelectionCount { get { return LevelEditorState.SelectedTileIds.Count; } } 

        public override SelectionTool.Feature MouseJustDownOverCanvas()
        {
            //Determine if we should move current selection or begin a new drag selection operation
            var selectionBounds = LevelEditor.GetTileSelectionBounds();

            return selectionBounds.Contains(LevelEditor.WorldPosition) ? SelectionTool.Feature.Move : SelectionTool.Feature.Drag;
        }
        
        public override void Moving(Vector2 delta)
        {
            //Move all the selected tiles
            foreach (var tileId in LevelEditorState.SelectedTileIds)
            {
                var tile = LevelEditor.GetTileByIndex(tileId);

                tile.Position += delta;
            }                
        }
       
        public override bool DragCompleted(bool isShiftDown, Rect selectionRect)
        {
            var oldSelectedTiles = new List<int>(LevelEditorState.SelectedTileIds);

            //if (KeyboardHelper.IsKeyPressed(Keys.LeftShift) == false)
            if (isShiftDown == false)
                LevelEditorState.SelectedTileIds.Clear();

            //Select all the things in the drag area
            for (var index = 0; index < LevelEditor.Course.Tiles.Count; index++)
            {
                var tile = LevelEditor.Course.Tiles[index];

                //All corners have to be in selection area
                var halfScale = tile.Scale / 2.0f;
                var topRight = new Vector2(tile.Position.X + halfScale.X, tile.Position.Y + halfScale.Y);
                var bottomLeft = new Vector2(tile.Position.X - halfScale.X, tile.Position.Y - halfScale.Y);

                if (selectionRect.Contains(bottomLeft) == false || selectionRect.Contains(topRight) == false)
                    continue;

                if (LevelEditorState.SelectedTileIds.Contains(index) == false)
                    LevelEditorState.SelectedTileIds.Add(index);
            }

            var firstNotSecond = oldSelectedTiles.Except(LevelEditorState.SelectedTileIds).ToList();
            var secondNotFirst = LevelEditorState.SelectedTileIds.Except(oldSelectedTiles).ToList();

            return firstNotSecond.Count + secondNotFirst.Count > 0;
        }

        public override Vector4 GetAcumulatedColorVector()
        {
            //Set the color of the object to the average of all selected
            if (SelectionCount == 0)
                return base.GetAcumulatedColorVector();

            var color = Vector4.Zero;
            foreach (var labelId in LevelEditorState.SelectedTileIds)
            {
                var label = LevelEditor.GetTileByIndex(labelId);
                color += label.Color.ToVector4();
            }

            return color;
        }
    }
}
