using System.Collections.Generic;

namespace Transition._____Temp.StarGuard
{
    /// <summary>
    /// 
    /// </summary>
    public class CollisionMapManager
    {
        /// <summary></summary>
        private readonly HashSet<int> _gamemapColliders = new HashSet<int>();
        private readonly Dictionary<int, int> _collisionMap = new Dictionary<int, int>();

        public void RegisterEntityCollider(EntityClassification classification1, EntityClassification classification2)
        {
            RegisterEntityColliderPair(classification1, classification2);
            RegisterEntityColliderPair(classification2, classification1);
        }

        void RegisterEntityColliderPair (EntityClassification classification1, EntityClassification classification2)
        {
            var key = (int)classification1;

            //Add the item if it doesn't extsts - update it if it does
            if (!_collisionMap.ContainsKey(key))
                _collisionMap.Add(key, (1 << (int)classification2));
            else
            {
                var value = _collisionMap[key];
                value = value | (1 << (int)classification2);
                _collisionMap[key] = value;
            }
        }

        public void RegisterGameMapCollider(EntityClassification classification)
        {
            var key = (int)classification;

            if (!_gamemapColliders.Contains(key))
                _gamemapColliders.Add(key);
        }

        public bool TestClassificationsCollide(EntityClassification classification1, EntityClassification classification2)
        {
            var key1 = (int)classification1;

            if (_collisionMap.ContainsKey(key1))
                return (_collisionMap[key1] & 1 << (int)classification2) > 0;

            var key2 = (int)classification2;

            if (_collisionMap.ContainsKey(key2))
                return (_collisionMap[key2] & 1 << (int)classification1) > 0;

            return false;
        }

        public bool IsMapCollider(EntityClassification classification)
        {
            return _gamemapColliders.Contains((int)classification);
        }

        public bool IsEntityCollider(EntityClassification classification)
        {
            return _collisionMap.ContainsKey((int)classification);   
        }
    }
}
