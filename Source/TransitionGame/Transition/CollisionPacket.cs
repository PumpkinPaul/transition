using Transition.Actors;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public struct CollisionPacket
    {
        public Actor Actor1;
        public Actor Actor2;
        public float Time;

        /// <summary>
        /// Creates a new instance of a CollisionPacket.
        /// </summary>
        /// <param name="actor1">The first actor in the collision.</param>
        /// <param name="actor2">The second actor in the collision.</param>
        /// <param name="time">The (normalised) time the collision occurred.</param>
        public CollisionPacket(Actor actor1, Actor actor2, float time)
        {
            Actor1 = actor1;
            Actor2 = actor2;
            Time = time;
        }
    }
}
