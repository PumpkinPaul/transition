using ProtoBuf;

namespace Transition.ActorDatas
{
	/// <summary>
	/// Configuration data surrogate for serialization.
	/// </summary>
    [ProtoContract]
    public class ActorDataSurrogate
    {
        [ProtoMember(1)]
        string _name;

        static readonly ActorDataSurrogate Instance = new ActorDataSurrogate();

        public static implicit operator ActorDataSurrogate(ActorData value)
        {
            if (value == null)
                return null;

            Instance._name = value.Name;
            return Instance;
        }

        public static implicit operator ActorData(ActorDataSurrogate value)
        {
            return value == null ? null : TransitionGame.Instance.DataManager.ActorData(value._name);
        }
    }
}
