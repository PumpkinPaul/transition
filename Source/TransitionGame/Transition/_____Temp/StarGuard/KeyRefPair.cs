using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Cameras;
using Transition.GamePhases;
using Transition.Input;
using Transition.PostProcess;

namespace Transition._____Temp.StarGuard
{
    [ProtoContract]
    public struct KeyRefPair<TKey,TValue> 
    {
        [ProtoMember(1)]        
        public TKey Key { get; private set; }

        [ProtoMember(2, AsReference = true)]
        public TValue Value { get; private set; }

        public KeyRefPair(TKey key, TValue value) : this() 
        {
            Key = key;
            Value = value;
        }

        public static implicit operator KeyValuePair<TKey,TValue>(KeyRefPair<TKey,TValue> val)
        {
            return new KeyValuePair<TKey,TValue>(val.Key, val.Value);
        }

        public static implicit operator KeyRefPair<TKey,TValue>(KeyValuePair<TKey,TValue> val)
        {
            return new KeyRefPair<TKey,TValue>(val.Key, val.Value);
        }
    }
}