using System;
using System.Collections.Generic;

namespace Transition.Animations
{
    /// <summary>
    /// An Animation specifies a sequence of Commands which control the image displayed by an Animated thing.
    /// </summary>
    public class Animation : IAppearance
    {
        public string Name;

        public const int UnknownLabel = -1;

        /// <summary>The commands to execute for the animation</summary>
        readonly List<Command> _commands = new List<Command>();

        /// <summary>Animation labels.</summary>
        readonly IDictionary<string, int?> _labels = new Dictionary<string, int?>();

        public void AddCommand(Command command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command)); 

            if (command is LabelCommand)
            {
                _labels.Add(((LabelCommand)command).Id, _commands.Count);
            }
            else
                _commands.Add(command);       
        }

        /// <summary>
	    /// Animate an animated thing. The animated thing should pass in its current
	    /// sequence and current tick.
	    ///
	    /// The tick is increased by one, and if this is greater than or equal to
	    /// the duration specified for the current sequence, the sequence number is
	    /// increased by one and the new sequence's command is executed. If the
	    /// duration of the command is 0 then the next command is executed, and so on,
	    /// until a duration &gt; 0 is encountered.
	    ///
	    /// If there are no more sequences then nothing happens.
	    ///
	    /// When a command is executed, it is applied to the Animated thing.
	    ///
	    /// @param animated The thing that wants to be animated
	    /// @param tickRate The number of ticks that pass this time
	    /// </summary>
	    public void Animate(Sprite animated)
        {

		    int currentSequence;

		    do {
			    currentSequence = animated.Sequence;

			    // Do nothing if the current sequence is not valid
			    if (currentSequence < 0 || currentSequence >= _commands.Count) 
				    return;

		    } while (_commands[currentSequence].Execute(animated));
	    }

        /// <summary>
        /// Get the sequence number of the label.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>UnknownLabel if the label isn't found</returns>
	    public int GetLabel(string id) 
        {
		    if (_labels == null) 
			    return UnknownLabel;
		    
		    var ret = _labels.Get(id);
		    if (ret == null) 
			    return UnknownLabel;

			return ret.Value;
	    }

        public bool ToSprite(Sprite target) 
        {
            target.SetAnimation(this);
            return true;
	    }
    }
}

