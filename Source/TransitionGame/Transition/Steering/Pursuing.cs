using System;
using Microsoft.Xna.Framework;
using Transition.Actors;

namespace Transition.Steering
{
    /// <summary>
    /// 
    /// </summary>
    public class Pursuing
    {
        readonly float _volatility;

        Actor _actor;
        Vector2 _internalTarget;

        public Rect Bounds;

        public Pursuing(float volatility)
        {
            _volatility = volatility;
        }

        public void Initialise(Actor actor)
        {
            _actor = actor;

            _actor.Direction = RandomHelper.DeterministicRandom.GetFloat(0.0f, MathHelper.TwoPi); 
            _internalTarget = VectorHelper.Polar(1.0f, _actor.Direction);
        }

        public void ActorAddedToScene()
        {
            Update(1.0f);
            _actor.Velocity = Vector2.Zero;
        }

        public void Update(float speed)
        {
            var steering = SteerLibrary.Wander(_actor, 1.0f, ref _internalTarget, _volatility, 1.75f);

            const float padding = 3.0f;

            if (_internalTarget.Y > Bounds.Top - padding)
                _internalTarget.Y = Bounds.Top - padding;
            else if (_internalTarget.Y < Bounds.Bottom + padding)
                _internalTarget.Y = Bounds.Bottom + padding;
                        
            _actor.Velocity += steering;
            VectorHelper.Truncate(ref _actor.Velocity, speed);
            _actor.Direction = (float)Math.Atan2(_actor.Velocity.Y, _actor.Velocity.X);
        }

        public void ActorConstrained()
        {		
			_actor.Direction = -_actor.Direction;
            
            if (_internalTarget.X > Bounds.Right)
                _internalTarget.X = Bounds.Right - _volatility;
            else if (_internalTarget.X < Bounds.Left)
                _internalTarget.X = Bounds.Left + _volatility;	
        }

        public void FlipHorizontalTarget()
        {
            var dx = _internalTarget.X - _actor.Position.X;
            dx = -dx;
            _internalTarget.X = _actor.Position.X + dx;
        }
    }
}
