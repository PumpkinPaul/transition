﻿using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    [ProtoContract]
    public class Weapon : Entity
    {
        //This will be moved to WeaponData
        const int FireRate = 8;

        public enum Phase
        {
            Ready,
            LoadingBullet
        }

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        Phase _phase;

        /// <summary>Indicates whether the weapon is firing this update.</summary>
        [ProtoMember(2)]
        bool _firing;

        /// <summary>Indicates whether the weapon was firing last update.</summary>
        [ProtoMember(3)]
        bool _previouslyFiring;

        /// <summary>The number of game ticks it takes to reload the weapon.</summary>
        [ProtoMember(4)]
		int _loadBulletTicks;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public Weapon(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.Weapon;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _phase = Phase.Ready;

            _firing = false;
            _previouslyFiring = false;
		    _loadBulletTicks = 0;
            
            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        } 

        protected override void OnUpdate(bool wasRewinding)
        {   		
		    switch (_phase) {
		        case Phase.Ready:
		            UpdateReadyPhase();
		            break;

		        case Phase.LoadingBullet:
		            UpdateLoadingBulletPhase();
		            break;
		    }
    		
		    _previouslyFiring = _firing;
		    _firing = false;
        }

        void UpdateReadyPhase() 
        {

        }

        void UpdateLoadingBulletPhase()
        {
            _loadBulletTicks++;

		    var fireRate = FireRate;
		    if (_loadBulletTicks > fireRate) {
			    //Bullet loaded so ready the weapon
			    _loadBulletTicks -= fireRate;
			    _phase = Phase.Ready;
		    }	
        }

         /// <summary>
        ///  request to fire the weapon
        /// </summary>
        /// <remarks>May not fire if the weapon isn't ready.</remarks>
        public bool Fire()
        {
            var fired = false;
              
            if (_phase == Phase.Ready)
            {
                _firing = true;
                fired = OnCreateBullet();
                _phase = Phase.LoadingBullet;
            }

             return fired;
         }

        /// <summary>
        /// Called after the weapon has created its bullet.
        /// </summary>
        /// <remarks>Standard behaviour for a wepaon - subclasses may override </remarks>
        protected virtual bool OnCreateBullet()
        {
            var bullet = CreateBullet();
            if (bullet == null) 
                return false;

            const float speed = 0.3f;
            bullet.Position = Position;
            bullet.Velocity = VectorHelper.Polar(speed, Direction);
            
            Game.World.Add(bullet);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual Bullet CreateBullet()
        {
            return (Bullet)Game.EntityFactory.Create(typeof(Bullet));
        }
    }
}