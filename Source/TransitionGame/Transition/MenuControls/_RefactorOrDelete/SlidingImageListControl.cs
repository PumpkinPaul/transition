using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition
{
    /// <summary>
    /// A control that displays a list of images representing options that scroll smoothly.
    /// </summary>
    public class SlidingImageListControl : MenuControl
    {
        //Control states...
        private readonly NormalState _normalState = new NormalState();
        private readonly SlidingLeftState _slidingLeftState = new SlidingLeftState();
        private readonly SlidingRightState _slidingRightState = new SlidingRightState();

        /// <summary>The control's current state.</summary>
        private SlidingImageListControlState _state;

        public Color TextTint;
        public bool ShowText;
        public bool ImageGetsFocusColour = true;
        public bool ImageGetFocusStrobe;
        public float ImageSpacer;
        public float LargeImageScale = 1.5f;
        public int MaxPulseTicks = 30;
        public float NormalImageScale = 1.0f;
        public bool PulseSelectedItem = true;

        public bool AllowWrapItems;

        /// <summary>The index of the selected option.</summary>
        public int SelectedIndex = -1;

        /// <summary></summary>
        private readonly List<TextureInfo> _images = new List<TextureInfo>();
        private readonly List<Color> _colours = new List<Color>();
        private readonly List<bool> _enables = new List<bool>();
        private readonly List<string> _text = new List<string>();
        private readonly List<string> _helpText = new List<string>();
        private readonly List<int> _pulses = new List<int>();

        private int _pulseInc = 1;

        /// <summary>
        /// Initialises a new instance of a SlidingImageListControl.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public SlidingImageListControl(BaseGame game) : base(game)
        {
            ChangeState(_normalState);
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            TextTint = element.GetAttributeColor("textTint", TextTint);
            ShowText = element.GetAttributeBool("showText", ShowText);
            ImageGetsFocusColour = element.GetAttributeBool("imageGetsFocusColour", ImageGetsFocusColour);
            ImageGetFocusStrobe = element.GetAttributeBool("imageGetFocusStrobe", ImageGetFocusStrobe);
            ImageSpacer = element.GetAttributeSingle("imageSpacer", ImageSpacer);
            LargeImageScale = element.GetAttributeSingle("largeImageScale", LargeImageScale);
            MaxPulseTicks = element.GetAttributeInt32("maxPulseTicks", MaxPulseTicks);
            NormalImageScale = element.GetAttributeSingle("normalImageScale", NormalImageScale);
            PulseSelectedItem = element.GetAttributeBool("pulseSelectedItem", PulseSelectedItem);
            AllowWrapItems = element.GetAttributeBool("allowWrapItems", AllowWrapItems);
        }

        /// <summary>
        /// Gets the number of options in the list.
        /// </summary>
        public int Count
        {
            get { return _images.Count; }
        }

        /// Gets the text to display for the A button action.
        public override string LeftRightButtonText
        {
            get { return "change value"; }
        }

        /// <summary>
        /// /// Occurs when a control is clicked.
        /// </summary>
        /// <returns></returns>
        protected override bool OnClick()
        {
            return _state.ShouldRaiseClick;
               
        }

        //TODO: Transition - Bounds property
        //TODO:Transition - Mouse handling

        /// <summary>
        /// Tells a control to increase its value.
        /// </summary>
        protected override bool OnIncreaseValue()
        {
            if (AllowWrapItems == false && SelectedIndex >= _images.Count - 1)
                return false;

            var canChangeState = _state.CanChangeState;

            if (canChangeState)
                ChangeState(_slidingLeftState);
            
            return false;
        }

        /// <summary>
        /// Tells a control to deccrease its value.
        /// </summary>
        protected override bool OnDecreaseValue()
        {
            if (AllowWrapItems == false && SelectedIndex <= 0)
                return false;

            var canChangeState = _state.CanChangeState;

            if (canChangeState)
                ChangeState(_slidingRightState);
            
            return false;
        }

        /// <summary>
        /// Allows a derived control to update itself.
        /// </summary>
        protected override void OnUpdate()
        {
            _state.Update(this);

            PulseItem();
        }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
        {
            //TODO: Do something with this property in Draw
            //ImageGetsFocusColour

            var maxPositionX = Game.CameraManager.Camera.Bounds.Right;
            var position = Position;

            var imageWidthIncludingSpacer = NormalImageScale + ImageSpacer;

            //var slideOffset = (Interpolate.EaseBothSoft(0.0f, imageWidthIncludingSpacer, _state.Value) * _state.Direction);// + (SelectedIndex * imageWidthIncludingSpacer);
            var slideOffset = (MathHelper.SmoothStep(0.0f, imageWidthIncludingSpacer, _state.Value) * _state.Direction);// + (SelectedIndex * imageWidthIncludingSpacer);
            position.X -= slideOffset;

            var drawn = 0;
            var i = SelectedIndex;
            while(drawn <= _images.Count)
            {
                var scale = MathHelper.SmoothStep(NormalImageScale, LargeImageScale, _pulses[i] / (float)MaxPulseTicks);

                Color tint;
                if (i == SelectedIndex && Game.MenuManager.FocusControl == this)
                {
                    if (ImageGetsFocusColour)
                        tint = Tint;
                    else
                        tint = _colours[i];
                }
                else
                    tint = _colours[i];

                tint *= Overlay.Opacity;

                var pos = position;
                if (AllowWrapItems)
                {
                    //If the image is offscreen to the right we need to draw it way off to the left far enough to allow the missing images to but up to the selected item
   
                    if (pos.X > (maxPositionX + (scale / 2.0f)))
                    {
                        var selectedImageX = Position.X - slideOffset;
                        var remainingImagesCount = _images.Count - drawn;

                        pos.X = selectedImageX - (remainingImagesCount * imageWidthIncludingSpacer);
                    }
                }

                var q = new Quad(pos, new Vector2(scale), 0.0f);
                Game.Renderer.DrawSprite(ref q, _images[i].TextureId, _images[i].SourceRect, tint, RenderStyle);

                if (ShowText)
                {
                    tint = TextTint;
                    tint *= Overlay.Opacity;
                    Game.Renderer.DrawString(Theme.Font, pos + new Vector2(0, (-scale / 4.0f) - 2.0f), new Vector2((scale / NormalImageScale) * 1.25f), _text[i], Alignment.Centre, tint, RenderStyle);
                }

                position.X += NormalImageScale + ImageSpacer;

                i = (i + 1) % _images.Count;
                
                drawn++;
            }
        }

        /// <summary>
        /// Changes the control state.
        /// </summary>
        /// <param name="newState">The new state.</param>
        private void ChangeState(SlidingImageListControlState newState)
        {
            _state = newState;
            _state.Initialise(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureInfo"></param>
        public void AddImage(TextureInfo textureInfo)
        {
            AddImage(textureInfo, Color.White, string.Empty); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureInfo"></param>
        /// <param name="helpText"></param>
        public void AddImage(TextureInfo textureInfo, string helpText)
        {
            AddImage(textureInfo, Tint, string.Empty, helpText);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureInfo"></param>
        /// <param name="color"></param>
        /// <param name="helpText"></param>
        public void AddImage(TextureInfo textureInfo, Color color, string helpText)
        {
            AddImage(textureInfo, color, string.Empty, helpText); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureInfo"></param>
        /// <param name="color"></param>
        /// <param name="text"></param>
        /// <param name="helpText"></param>
        public void AddImage(TextureInfo textureInfo, Color color, string text, string helpText)
        {
            _images.Add(textureInfo);
            _colours.Add(color);
            _text.Add(text);
            _helpText.Add(helpText);

            _pulses.Add(0);
            _enables.Add(true);
        }

        /// <summary>
        /// Clears all the options from the list.
        /// </summary>
        public void ClearImages()
        {
            SelectedIndex = -1;
            _images.Clear();
            _colours.Clear();
            _enables.Clear();
            _text.Clear();
            _helpText.Clear();
            _pulses.Clear();
        }

        /// <summary>
        /// Edits an option int the list.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="textureInfo"></param>
        public void EditImage(int index, TextureInfo textureInfo)
        {
            _images[index] = textureInfo;
        }

        /// <summary>
        /// Pulses the active item.
        /// </summary>
        private void PulseItem()
        {
            if (PulseSelectedItem == false)
                return;

            for (var i = 0; i < _pulses.Count; i++)
            {
                if (SelectedIndex != i ||  Game.MenuManager.FocusControl != this)
                {
                    //Elegantly reduce the size of the non active image back to normal
                    if (_pulses[i] > 0)
                        _pulses[i]--;
                }
                else
                {
                    //Pulse the selected image
                    _pulses[i] += _pulseInc;

                    if (_pulses[i] > MaxPulseTicks)
                        _pulseInc = -_pulseInc;
                    else if (_pulses[i] < 0)
                    {
                        _pulseInc = -_pulseInc;  
                        _pulses[i] = 0;
                    }
                }
            }
            
            if (SelectedIndex != -1)
                HelpText = _helpText[SelectedIndex];
        }

        /// <summary>
        /// Represents a base class to use for this control's states.
        /// </summary>
        private abstract class SlidingImageListControlState
        {
            /// <summary>The amount to increase the value by each update.</summary>
            protected const float ValueDelta = 0.06f;

            /// <summary>The current value - (clamped to 0.0f...x...1.0f).</summary>
            public float Value;

            /// <summary>Gets whether the control should raise the click event.</summary>
            public abstract bool ShouldRaiseClick { get; }

            /// <summary>Gets whether the state is allowed to change.</summary>
            public abstract bool CanChangeState { get; }

            /// <summary>Gets the direction of the change (1, 0 or -1 to indicate left, stationary or right).</summary>
            public abstract int Direction { get; }

            /// <summary>Initialises the state.</summary>
            /// <param name="control">A reference to the control.</param>
            public abstract void Initialise(SlidingImageListControl control);

            /// <summary>Updates the state.</summary>
            /// <param name="control">A reference to the control.</param>
            public abstract void Update(SlidingImageListControl control);

            /// <summary>
            /// Occurs when the selected index value is changed.
            /// </summary>
            /// <param name="control">A reference to the control.</param>
            protected void ValueChanged(SlidingImageListControl control)
            {
                Value = 0.0f;
                control.PerformIncreaseValue();
                control.ChangeState(control._normalState);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private class NormalState : SlidingImageListControlState
        {
            /// <summary>Gets the direction of the change (1, 0 or -1 to indicate left, stationary or right).</summary>
            public override int Direction { get { return 0; } }

            /// <summary>Gets whether the control should raise the click event.</summary>
            public override bool ShouldRaiseClick { get { return true; } }

            /// <summary>Gets whether the state is allowed to change.</summary>
            public override bool CanChangeState { get { return true; } }

            /// <summary>Initialises the state.</summary>
            /// <param name="control">A reference to the control.</param>
            public override void Initialise(SlidingImageListControl control)
            {
                control.CapturesPreviousNextControlRequest = false;
            }

            /// <summary>Updates the state.</summary>
            /// <param name="control">A reference to the control.</param>
            public override void Update(SlidingImageListControl control)
            {
                //NAR - just sit there for now.
            }
        }

        private class SlidingLeftState : SlidingImageListControlState
        {
            /// <summary>Gets the direction of the change (1, 0 or -1 to indicate left, stationary or right).</summary>
            public override int Direction { get { return 1; } }

            /// <summary>Gets whether the control should raise the click event.</summary>
            public override bool ShouldRaiseClick { get { return false; } }

            /// <summary>Gets whether the state is allowed to change.</summary>
            public override bool CanChangeState { get { return false; } }

            /// <summary>Initialises the state.</summary>
            /// <param name="control">A reference to the control.</param>
            public override void Initialise(SlidingImageListControl control)
            {
                control.Game.PlaySound("MenuItemChange");
                Value = 0.0f;
                control.CapturesPreviousNextControlRequest = true;
            }

            /// <summary>Updates the state.</summary>
            /// <param name="control">A reference to the control.</param>
            public override void Update(SlidingImageListControl control)
            {
                Value += ValueDelta;
                if (Value >= 1.0f)
                {
                    ++control.SelectedIndex;

                    if (control.SelectedIndex > control._images.Count - 1)
                        control.SelectedIndex = 0;

                    ValueChanged(control);
                }
            }
        }

        private class SlidingRightState : SlidingImageListControlState
        {
            /// <summary>Gets the direction of the change (1, 0 or -1 to indicate left, stationary or right).</summary>
            public override int Direction { get { return -1; } }

            /// <summary>Gets whether the control should raise the click event.</summary>
            public override bool ShouldRaiseClick { get { return false; } }

            /// <summary>Gets whether the state is allowed to change.</summary>
            public override bool CanChangeState { get { return false; } }

            /// <summary>Initialises the state.</summary>
            /// <param name="control">A reference to the control.</param>
            public override void Initialise(SlidingImageListControl control)
            {
                control.Game.PlaySound("MenuItemChange");
                Value = 0.0f;
                control.CapturesPreviousNextControlRequest = true;
            }

            /// <summary>Updates the state.</summary>
            /// <param name="control">A reference to the control.</param>
            public override void Update(SlidingImageListControl control)
            {
                Value += ValueDelta;
                if (Value >= 1.0f)
                {
                    --control.SelectedIndex;

                    if (control.SelectedIndex < 0)
                        control.SelectedIndex = control._images.Count - 1;

                    ValueChanged(control);
                }
            }
        }
    }
}