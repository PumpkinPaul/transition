using System;
using Microsoft.Xna.Framework;
using Transition.Interpolators;

namespace Transition.PostProcess 
{
    /// <summary>
    /// A class for implementing several simultaneous distortion effects
    /// </summary>
    public class DistortionEffects 
    {
	    public interface FX 
        {
		    void Tick();

            bool IsReady { get; }
	    }

        /// <summary>A flag to 'disable' the distortion effects (e.g. during GUI, etc)
        /// </summary>
        public static bool Active;

        private static RollEffect _reusableRollEffect;
        private static ColorEffect _reusableColorEffect;
        private static HorizontalNoiseEffect _reusableHorizontalNoiseEffect;

        public static RollEffect GetReusableRollEffect(int maxDuration, float maxSpeed) 
        {
            if (_reusableRollEffect == null) 
                _reusableRollEffect = new RollEffect(maxDuration, maxSpeed);
            else 
                _reusableRollEffect.Reset(maxDuration, maxSpeed);
            
            return _reusableRollEffect;   
        }

        public static ColorEffect GetReusableColorEffect(int minDuration, int maxDuration, int minDistance, int maxDistance) 
        {
            if (_reusableColorEffect == null) 
                _reusableColorEffect = new ColorEffect(minDuration, maxDuration, minDistance, maxDistance);
            else 
                _reusableColorEffect.Reset(minDuration, maxDuration, minDistance, maxDistance);
            
            return _reusableColorEffect;   
        }

        public static HorizontalNoiseEffect GetReusableHorizontalNoiseEffect(int minDuration, int maxDuration, float maxAmount) 
        {
            if (_reusableHorizontalNoiseEffect == null) 
                _reusableHorizontalNoiseEffect = new HorizontalNoiseEffect(minDuration, maxDuration, maxAmount);
            else 
                _reusableHorizontalNoiseEffect.Reset(minDuration, maxDuration, maxAmount);
            
            return _reusableHorizontalNoiseEffect;   
        }

	    /// <summary>
	    /// 
	    /// </summary>
	    public class RollEffect : FX 
        {
		    private int _maxDuration;
		    private float _maxSpeed;

		    float _pos;
		    float _speed;
		    int _tick;
		    int _duration;

		    public RollEffect(int maxDuration, float maxSpeed) 
            {
			    Reset(maxDuration, maxSpeed);
		    }

		    private void Init() 
            {
			    _tick = 0;
                _duration = RandomHelper.Random.GetInt(1, _maxDuration);
                _speed = RandomHelper.Random.GetFloat() * _maxSpeed;
		    }

	        void FX.Tick() {
			    _pos += _speed;
			    DistortionsProcessor.SetVsyncOffset(_pos);

			    _tick++;
			    if (_tick >= _duration) 
                {
                    if (RandomHelper.Random.GetFloat() < 0.25f)
					    Init();
				    else 
                    { 
					    DistortionsProcessor.SetVsyncOffset(0.0f);
					    SetRollEffect(null);
				    }
			    }
		    }

            internal void Reset(int maxDuration, float maxSpeed)
            {
                _maxDuration = maxDuration;
			    _maxSpeed = maxSpeed;

                _pos = 0;

			    Init();    
            }

            public bool IsReady { get { return _tick == 0; } }
	    }

	    
	    public class ColorEffect : FX 
        {
            private int _maxDuration, _minDuration;
		    private int _maxDistance, _minDistance;

		    Vector2 _red;
		    Vector2 _green;
		    Vector2 _blue;

		    int _tick;
		    int _duration;
		    bool _outThenIn;
		    double _redRotate, _greenRotate, _blueRotate;
		    double _redAngle, _greenAngle, _blueAngle;
		    double _redDist, _greenDist, _blueDist;

		    public ColorEffect(int minDuration, int maxDuration, int minDistance, int maxDistance) 
            {
			    Reset(minDuration, maxDuration, minDistance, maxDistance);
		    }

		    private void Init() 
            {
			    _tick = 0;
                _duration = RandomHelper.Random.GetInt(_minDuration, _maxDuration);
                _outThenIn = RandomHelper.Random.GetBool();

                _redAngle = RandomHelper.Random.GetFloat() * Math.PI * 2.0;
                _greenAngle = RandomHelper.Random.GetFloat() * Math.PI * 2.0;
                _blueAngle = RandomHelper.Random.GetFloat() * Math.PI * 2.0;

                if (RandomHelper.Random.GetBool()) 
                {
                    _redRotate = RandomHelper.Random.GetFloat() * Math.PI * 0.005;
                    _greenRotate = RandomHelper.Random.GetFloat() * Math.PI * 0.005;
                    _blueRotate = RandomHelper.Random.GetFloat() * Math.PI * 0.005;
			    } 
                else 
				    _redRotate = _greenRotate = _blueRotate = 0.0;

                _redDist = RandomHelper.Random.GetFloat(_minDistance, _maxDistance);
                _greenDist = RandomHelper.Random.GetFloat(_minDistance, _maxDistance);
                _blueDist = RandomHelper.Random.GetFloat(_minDistance, _maxDistance);
			    
                SetVectors();
		    }

		    private void SetVectors() 
            {
			    _red.X = (float)(Math.Cos(_redAngle) * _redDist);
                _red.Y = (float)(Math.Sin(_redAngle) * _redDist);
			    
                _green.X = (float)(Math.Cos(_greenAngle) * _greenDist);
                _green.Y = (float)(Math.Sin(_greenAngle) * _greenDist);

			    _blue.X = (float)(Math.Cos(_blueAngle) * _blueDist);
                _blue.Y = (float)(Math.Sin(_blueAngle) * _blueDist);
		    }

	        void FX.Tick() 
            {
			    _tick++;

			    float ratio = _tick / (float) _duration;
			    float dist;
			    if (_outThenIn) 
                {
				    if (ratio >= 0.5) 
					    dist = CosineInterpolator.Instance.Interpolate(1.0f, 0.0f, ratio * 2.0f - 1.0f);
				    else 
					    dist = SineInterpolator.Instance.Interpolate(0.0f, 1.0f, ratio * 2.0f);
			    } 
                else 
				    dist = SineInterpolator.Instance.Interpolate(1.0f, 0.0f, ratio);

			    _redAngle += _redRotate;
			    _greenAngle += _greenRotate;
			    _blueAngle += _blueRotate;
			    SetVectors();

			    if (_tick >= _duration) 
                {
				    DistortionsProcessor.SetRedOffset(0.0f, 0.0f);
				    DistortionsProcessor.SetGreenOffset(0.0f, 0.0f);
				    DistortionsProcessor.SetBlueOffset(0.0f, 0.0f);
				    SetColorEffect(null);
			    } 
                else 
                {
				    DistortionsProcessor.SetRedOffset(_red.X * dist, _red.Y * dist);
				    DistortionsProcessor.SetGreenOffset(_green.X * dist, _green.Y * dist);
				    DistortionsProcessor.SetBlueOffset(_blue.X * dist, _blue.Y * dist);
			    }
		    }

            public void Reset(int minDuration, int maxDuration, int minDistance, int maxDistance) 
            {
                _minDuration = minDuration;
			    _maxDuration = maxDuration;
			    _minDistance = minDistance;
			    _maxDistance = maxDistance;

			    Init();
            }

            public bool IsReady { get { return _tick == 0; } }
	    }

	    public class HorizontalNoiseEffect : FX 
        {
		    private int _minDuration, _maxDuration;
		    private float _maxAmount;
		    private readonly float[] _noise = new float[DistortionsProcessor.RowNoiseResolution];
		    private readonly PerlinNoise _noiseFunction = new PerlinNoise(DateTime.Now.Millisecond, 2, 6, 0.75f, SineInterpolator.Instance);

		    float _compression;
		    int _offset;
		    int _tick;
		    int _duration;
		    float _bias;
		    bool _even;

		    public HorizontalNoiseEffect(int minDuration, int maxDuration, float maxAmount) 
            {
			    Reset(minDuration, maxDuration, maxAmount);
		    }

		    private void Init() 
            {
			    _tick = 0;
                _duration = RandomHelper.Random.GetInt(_minDuration, _maxDuration);
                _compression = RandomHelper.Random.GetFloat() * 10;
                _offset = RandomHelper.Random.GetInt(0, _noise.Length - 1);
                _bias = RandomHelper.Random.GetFloat(0, 2) * 0.5f;
                _even = RandomHelper.Random.GetBool();
		    }

	        void FX.Tick() 
            {
			    _tick++;
			    if (_tick >= _duration) 
                {
                    if (RandomHelper.Random.GetFloat() < 0.25f)
					    Init();
				    else 
                    {
					    SetHorizontalNoiseEffect(null);
					    for (int i = 0; i < _noise.Length; i++) 
						    _noise[i] = 0.0f;
				    }
                }
			    else 
				    CalculateNoise();
			    
			    DistortionsProcessor.SetRowNoise(_noise);
		    }

		    private void CalculateNoise() 
            {
			    float ratio = _tick / (float) _duration;
			    if (_even) 
                {
				    for (int i = 0; i < _noise.Length; i++) 
                    {
					    float size = (float) Math.Pow(CosineInterpolator.Instance.Interpolate(1.0f, 0.0f, ratio), 1.0 + _compression) * 64.0f;
					    float noiseData = _noiseFunction.SmoothInterpolatedNoise(i, _tick) - 0.5f;
					    _noise[i] = -(noiseData + _bias) * _maxAmount * size;
				    }
			    } 
                else 
                {
				    for (int i = 0; i < _noise.Length; i++) 
                    {
					    float ratio2 = 1.0f - (i / (float)_noise.Length);
					    float size = (float) Math.Pow(CosineInterpolator.Instance.Interpolate(1.0f, 0.0f, ratio), 1.0 + _compression) * 64.0f;
					    float noiseData = _noiseFunction.SmoothInterpolatedNoise(i, _tick) - 0.5f;
					    _noise[(_noise.Length - 1) - ((i + _offset) % _noise.Length)] = -((noiseData + _bias) * _maxAmount * size * ratio2);
				    }
			    }
		    }

            public void Reset(int minDuration, int maxDuration, float maxAmount) 
            {
                _minDuration = minDuration;
			    _maxDuration = maxDuration;
			    _maxAmount = maxAmount;

			    Init();
            }

            public bool IsReady { get { return _tick == 0; } }
	    }

	    private static FX _rollEffect;
	    private static FX _horizontalNoiseEffect;
	    private static FX _colorEffect;

	    /**
	     * No c'tor
	     */
	    private DistortionEffects() { }

	    public static void SetRollEffect(FX effect) 
        {
            if (Active == false)
            {
                if (effect == null)
                    _rollEffect = null;

                return;
            }

		    _rollEffect = effect;
	    }

	    public static void SetHorizontalNoiseEffect(FX effect) 
        {
            if (Active == false)
            {
                if (effect == null)
                    _horizontalNoiseEffect = null;

                return;
            }

		    _horizontalNoiseEffect = effect;
	    }

	    public static void SetColorEffect(FX effect) 
        {
            if (Active == false)
            {
                if (effect == null)
                    _colorEffect = null;

                return;
            }

		    _colorEffect = effect;
	    }

	    public static void Tick() 
        {
		    if (_rollEffect != null) 
			    _rollEffect.Tick();
		    
		    if (_horizontalNoiseEffect != null) 
			    _horizontalNoiseEffect.Tick();
		    
		    if (_colorEffect != null) 
			    _colorEffect.Tick();
	    }
    }
}