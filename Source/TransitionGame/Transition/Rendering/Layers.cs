namespace Transition.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public static class Layers
    {
        public const int Background = -1000;

        public const int City = -500;

	    public const int Default = 0;

        public const int ActorBackgroundEffect = 1400;
        public const int Actor = 1600;
        public const int ActorForegroundEffect = 3200;
    }
}
