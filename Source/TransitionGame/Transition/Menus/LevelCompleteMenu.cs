using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the level complete menu.
    /// </summary>
    public class LevelCompleteMenu : Menu
    {
        public event EventHandler StartGame;

        ImageControl _employeeImage;

        bool _awaitingQuitConfirm;

        public override string BackButtonText => "Resume";

        /// <summary>
        /// Initialises a new instance of a LevelCompleteMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
        public LevelCompleteMenu(BaseGame game, string title) : base(game, title) { }

        protected override void OnConfigureMenu()
        {
            GetControl<TextButtonControl>("nextButton").Click += (sender, args) => NextLevel();
            GetControl<TextButtonControl>("repeatButton").Click += (sender, args) => RepeatLevel();
            GetControl<TextButtonControl>("quitButton").Click += (sender, args) => ShowQuitConfirm();

            _employeeImage = GetControl<ImageControl>("employeeImage");

        }

        protected override void OnAddedToManager()
        {
            var shipBlueprint = Game.EntityBlueprints.ShipBlueprint(Game.InitialCheckpoint.ShipName);
            _employeeImage.TextureInfo = shipBlueprint.MenuTextureInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnGotFocus()
        {
            if (_awaitingQuitConfirm)
            {
                if (MessageBoxMenu.Result == MessageBoxMenu.MessageBoxResult.Yes)
                {
                    QuitGame();
                }
                _awaitingQuitConfirm = false;
            }
        }

        void NextLevel()
        {
            var currentCheckpoint = TransitionGame.Instance.InitialCheckpoint;

            HandleCheckpoint(currentCheckpoint);

            var handler = StartGame;
            handler?.Invoke(this, EventArgs.Empty);
        }


        private void HandleCheckpoint(IReadableCheckpoint currentCheckpoint)
        {
            Checkpoint checkpoint = null;
            var updateCheckpoint = false;

            if (Game.ActivePlayer.Profile.Checkpoints.Count() < currentCheckpoint.Level)
            {
                //We don't have enough checkpoints so add a new one
                checkpoint = new Checkpoint(currentCheckpoint);
                Game.ActivePlayer.Profile.AddCheckpoint(checkpoint);
            }
            else
            {
                //TODO: updating check point stats
                ////Grab the current checkpoint and compare some key values to see if we should update it.
                checkpoint = new Checkpoint(Game.ActivePlayer.Profile.GetCheckpoint(currentCheckpoint.Level - 1));

                ////Score is the primary driver, if it's not as good as the checkpoint we don't save.
                //if (Player1.Score < checkpoint.Score)
                //    return;

                ////In true Llamasoft 'Resume Best' tradition, you must have at least 3 'lives' to update the checkpoint or have more 'lives' than the checkpoint. 
                //if (Player1.Ship.Health < 3 && Player1.Ship.Health < checkpoint.Health)
                //    return;

                ////If the scores are the same but the money is less bail!
                //if (Player1.Score == checkpoint.Score && Player1.Money < checkpoint.Money)
                //    return;

                ////Bail, current progress is deemed not as good as current checkpoint attributes.
                ////return;

                ////We just need to update the current checkpoint.
                updateCheckpoint = false;
            }

            checkpoint.Level = currentCheckpoint.Level + 1;
            //checkpoint.Score = Player1.Score;

            //if (updateCheckpoint)
            //    Game.ActivePlayer.Profile.UpdateCheckpoint(checkpoint);

            TransitionGame.Instance.InitialCheckpoint = checkpoint;
        }

        void RepeatLevel()
        {
            Game.Restart();
            Close();
        }

        /// <summary>
        /// Shows the restart confirm menu
        /// </summary>
        private void ShowQuitConfirm()
        {
            _awaitingQuitConfirm = true;

            var messageBoxMenu = (MessageBoxMenu)MenuManager.GetMenu(typeof(MessageBoxMenu));
            messageBoxMenu.ShowNoButton = true;
            messageBoxMenu.ShowYesButton = true;
            messageBoxMenu.YesButtonText = "Yes";
            messageBoxMenu.NoButtonText = "No";
            messageBoxMenu.Title = "Confirm Quit";
            messageBoxMenu.Message = "Are you sure you want to quit?";
            MenuManager.AddMenu(messageBoxMenu);
        }

        /// <summary>
        /// Quits the current game.
        /// </summary>
        /// <remarks>Quits the current game and returns to the main menu.</remarks>
        private void QuitGame()
        {
            Game.Quit();
            Close();
        }
    }
}
