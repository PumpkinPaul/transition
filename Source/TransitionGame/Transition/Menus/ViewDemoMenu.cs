using System;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the view demo menu.
    /// </summary>
    public class ViewDemoMenu : Menu
    {
        //Ordered list of game options - add images to the list control in this order!
        enum MenuOptions
        {
            Play,
            Pause,
            Stop,
        }

        ImageListControl _imageListControl;
        TextLabelControl _replayLabel;
        SliderControl _timelineSlider;

        float _replayOpacity = 1.0f;
        float _replayOpacityDelta = -0.02f; 

		/// <summary>
        /// Creates a new instance of a CreditsMenu.
        /// </summary>
		public ViewDemoMenu(BaseGame game, string title) : base(game, title) { }

        protected override void OnConfigureMenu()
        {
            _replayLabel = new TextLabelControl(Game);
            _replayLabel.Position = new Vector2(0.0f, 9.25f);
			_replayLabel.Text = "REPLAY";
            _replayLabel.Font = Theme.Font;
            _replayLabel.FontSize = Theme.FontSize * 2.0f;
            _replayLabel.Tint = Theme.StandardColor;
            _replayLabel.Alignment = Alignment.Centre;

            AddControl(_replayLabel, NoTabIndex);
		
            _imageListControl = new ImageListControl(Game);
            _imageListControl.Position = new Vector2(0.0f, 9.25f);
            _imageListControl.Tint = Theme.StandardColor;
            _imageListControl.NormalImageScale = 1.5f;
            _imageListControl.LargeImageScale = 2.0f;
            _imageListControl.ImageSpacer = 0.15f;
            _imageListControl.ShowText = false;
            _imageListControl.AddImage(Game.TextureManager.GetTextureInfo("MenuItemResume"), "play");
            _imageListControl.AddImage(Game.TextureManager.GetTextureInfo("MenuItemPause"), "pause the replay");
            _imageListControl.SelectedIndex = 0;
            _imageListControl.Click += ImageListControlClick;

            CloseButton.Size = new Vector2(1.15f);
            CloseButton.Position = new Vector2(CloseButton.Position.X, 9.35f);

            _timelineSlider = new SliderControl(Game);
            _timelineSlider.Position = new Vector2(-2.5f, 8.35f);
            _timelineSlider.Tint = Theme.StandardColor;
            _timelineSlider.Size = new Vector2(5.0f, 0.2f);
            _timelineSlider.Fill = SliderControl.FillMode.LeftToRight;
            _timelineSlider.Value = 0;

            AddControl(_timelineSlider, NoTabIndex);
        }

        protected override void OnAddedToManager()
        {
            base.OnAddedToManager();

            if (Game.GameOptions.ReplayPlaybackMode == ReplayPlaybackMode.OnDemand)
                ((TransitionGame)Game).DemoFinished();
        }

        private void ImageListControlClick(object sender, EventArgs e)
        {
            switch((MenuOptions)(_imageListControl.SelectedIndex))
            {
                case MenuOptions.Play:
                    Game.Unpause();
                    break;

                case MenuOptions.Pause:
                    Game.SimplePause();
                    break;

                case MenuOptions.Stop:
                    OnBack();
                    break;
            }
        }

        protected override void OnUpdateMustCallBase() 
        {
            base.OnUpdateMustCallBase();

            _replayOpacity += _replayOpacityDelta;

            if (_replayOpacity < 0.25f)
            {
                _replayOpacity = 0.25f;
                _replayOpacityDelta = -_replayOpacityDelta;
            }
            else if (_replayOpacity > 1.0f)
            {
                _replayOpacity = 1;
                _replayOpacityDelta = -_replayOpacityDelta;
            }

            var tint = Theme.StandardColor;
            tint *= _replayOpacity;
            _replayLabel.Tint = tint;

            _timelineSlider.Value = (int)Game.DemoRecorder.Progress * 100;

            if (MenuManager.MenuInput.StartJustPressed || MenuManager.MenuInput.SelectJustPressed)
                CloseThisMenu();
        }

        /// <summary>
        /// Occurs when the 'back' button on the controller is pressed.
        /// </summary>
        protected override bool OnBack()
        {
            CloseThisMenu();

            return true;
        }

        private void CloseThisMenu()
        {
            Game.FadeOut();
            Close();

            if (Game.GameOptions.ReplayPlaybackMode == ReplayPlaybackMode.OnDemand)
                ((TransitionGame)Game).DemoFinished();
        }
    }
}
