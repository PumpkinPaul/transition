//#undef USE_GEARSET

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Gearset;
using KiloWatt.Runtime.Support;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nuclex.Input;
using Transition.Achievements;
using Transition.Actors;
using Transition.Cameras;
using Transition.Extensions;
using Transition.GameDebugTools;
using Transition.GamePhases;
using Transition.Gearset;
using Transition.Input;
using Transition.MenuControls;
using Transition.Menus;
using Transition.PostProcess;
using Transition.Rendering;
using Transition.ResourceManagers;
using Transition.Storage;
using Transition._____Temp;

namespace Transition
{
    /// <summary>
    /// Represents a bonus item - a collectable that gives a (points) bouns.
    /// </summary>
    public abstract class BaseGame : Game
    {
        public float GameTimeInSeconds { get; private set; }

        private int _savedPrimaryControllerId = -1;

        //Graphics
		public GraphicsDeviceManager Graphics { get; }
        public Renderer Renderer;
        public readonly SpritePool SpritePool;

        public Viewport FullscreenViewport { get; protected set; }
        public Viewport RenderViewport { get; private set; }
        
        public DemoRecorder DemoRecorder { get ; protected set; }
        public readonly StorageManager StorageManager;
        public readonly SoundManager SoundManager;
        public readonly VibrationManager VibrationManager;
        public readonly ScreenFlashManager ScreenFlashManager;
        public readonly MenuManager MenuManager;
        public readonly GamePhaseManager GamePhaseManager;
        public readonly AchievementsManager AchievementsManager;

        //Cameras
        public readonly CameraManager CameraManager;
        public readonly Camera GameCamera;
        public readonly Camera GuiCamera;

        //Input
        public readonly InputManager NuclexInputManager;
        public readonly InputMethodManager InputMethodManager;
        public readonly InputMethod GamepadInputMethod;
        public readonly InputMethod MouseAndKeysInputMethod;

        //Scene
        public readonly ActorFactory ActorFactory;

        //Resource Managers
        public EffectManager EffectManager { get; private set; }
        public FontManager FontManager { get; }
        public TextureManager TextureManager { get; }
        public ParticleTemplateManager ParticleTemplateManager { get; }
        public GodRaysTemplateManager GodRaysTemplateManager { get; }
        public readonly AnimationManager AnimationManager;
        public readonly DataManager DataManager;
        public readonly PlayerProfileManager PlayerProfileManager;
        public readonly OverlayTemplateManager OverlayTemplateManager;
        public readonly ThemeManager Themes;
        public readonly GameMapManager GameMapManager;
        public readonly EntityBlueprintManager EntityBlueprints;

        //Post Processors
        protected readonly PostProcessManager PostProcess;
        public readonly DistortionsProcessor DistortionsProcessor;
        public readonly BloomProcessor BloomProcessor;
        public readonly BarrelDistortionProcessor BarrelDistortionProcessor;
        public readonly RippleDistortionProcessor RippleDistortionProcessor;

        //Gearset
        FpsCounter _fpsCounter;
        
        //Threading
        public ThreadPoolComponent ThreadPoolComponent { get; private set; }

        public int DrawId { get; private set; }

        /// <summary>
        /// Gets the GameOptions.
        /// </summary>
        public GameOptions GameOptions { get; protected set; }

        private float _viewportZoom = 1.0f;
        private const float TitleSafeTest = 50.0f;

        protected bool GameHasFocus;

        protected bool IsSplashComplete { get; set; }

        public float ViewportZoom 
        { 
            get { return _viewportZoom; } 
            set 
            { 
                _viewportZoom = value;
                SetViewports();
                OnViewportZoom();
            } 
        }

        protected virtual void OnViewportZoom() 
        {
            GameOptions.ScreenSize = ViewportZoom;
        }

        public Matrix ProjectionMatrix;
        public Matrix WorldMatrix;

        public Matrix ViewMatrix
        {
            get { return CameraManager.ViewMatrix; }
            set { CameraManager.ViewMatrix = value; }
        }

        protected bool HasMenu => MenuManager.HasMenu;
        public bool IsPaused { get; protected set; }

        /// <summary>The checkpoint used to start the game. This may be a new one for a brand new game or a reference to one of the player's previously saved checkpoints.</summary>
        public IReadableCheckpoint InitialCheckpoint;

        //TODO: Transition
        /// <summary>A list of players playing the game.</summary>
        protected readonly List<Player> Players = new List<Player>();

        /// <summary>Gets player 1.</summary>
        public Player ActivePlayer => Players[0];

        /// <summary>
        /// The number of players playing the game.
        /// </summary>
        public int PlayerCount = 1;

        bool _resourcesChanged;

        protected BaseGame(string gameName) 
        {
            SetCursor(Path.Combine(Platform.ContentPath, "Cursors", "MousePointer.png"));

            GameOptions = new GameOptions();

            Graphics = new GraphicsDeviceManager(this)
            {
                PreferMultiSampling = true,
                PreferredBackBufferWidth = GameOptions.Width,
                PreferredBackBufferHeight = GameOptions.Height,
                IsFullScreen = GameOptions.FullScreen
            };
            Graphics.ApplyChanges();

            Activated += OnActivated;
            Deactivated += OnDeactivated;

            RandomHelper.DeterministicRandom.Seed = DateTime.Now.Millisecond;
            RandomHelper.Random.Seed = DateTime.Now.Millisecond;

            Content.RootDirectory = string.Empty;

            #if PROFILE
                Graphics.SynchronizeWithVerticalRetrace = true;
                IsFixedTimeStep = true;
            #else
                Graphics.SynchronizeWithVerticalRetrace = true;
                IsFixedTimeStep = true;
            #endif

            Components.Add(ThreadPoolComponent = new ThreadPoolComponent(this));

            SpritePool = new SpritePool(false, 4096);

            StorageManager = new StorageManager(this, gameName);
            StorageManager.StorageDeviceAction += StorageManagerStorageDeviceSelected;

            ScreenFlashManager = new ScreenFlashManager();
            VibrationManager = new VibrationManager();
            //WebManager = new WebManager(this);
            MenuManager = new MenuManager(this);
            GamePhaseManager = new GamePhaseManager();
            AchievementsManager = new AchievementsManager();
            //TODO: Transition
            ActorFactory = new ActorFactory((TransitionGame)this);
          
            NuclexInputManager = new InputManager(Services, Window.Handle);
            Components.Add(NuclexInputManager);
          
            //Input Methods
            GamepadInputMethod = new GamepadInputMethod();
            MouseAndKeysInputMethod = new KeyboardInputMethod();// MouseAndKeysInputMethod();
            InputMethodManager = new InputMethodManager();
            InputMethodManager.AddInputMethod(GamepadInputMethod);
            InputMethodManager.AddInputMethod(MouseAndKeysInputMethod);
            InputMethodManager.InputMethod = GamepadInputMethod;
            InputMethodManager.InputMethodChanged += MenuManager.InputMethodChanged;
            InputMethodManager.InputMethodIndexChanged += MenuManager.InputMethodPlayerIndexChanged;
                        
            //Cameras
            GuiCamera = new Camera(this);
            GameCamera = new UridiumFollowPlayerCamera(this);
            CameraManager = new CameraManager();
            CameraManager.AddCamera(GuiCamera);
            CameraManager.AddCamera(GameCamera);
            CameraManager.Camera = GuiCamera;

            SoundManager = new SoundManager(Content);
            TextureManager = new TextureManager(Content);
            FontManager = new FontManager(TextureManager);
            ParticleTemplateManager = new ParticleTemplateManager(TextureManager);
            GodRaysTemplateManager = new GodRaysTemplateManager(TextureManager);
            AnimationManager = new AnimationManager();
            //DataManager = new DataManager(gameName + ".Actors.", TextureManager, AnimationManager, ParticleTemplateManager);
            DataManager = new DataManager("Transition._____Temp.StarGuard.Entities.", TextureManager, AnimationManager, ParticleTemplateManager);
            EntityBlueprints = new EntityBlueprintManager("Transition._____Temp.StarGuard.Entities.", TextureManager, AnimationManager, ParticleTemplateManager);

            PlayerProfileManager = new PlayerProfileManager(TextureManager);
            OverlayTemplateManager = new OverlayTemplateManager(this);
            Themes = new ThemeManager(this);
            GameMapManager = new GameMapManager(this);

            PostProcess = new PostProcessManager(this, null);

            #if WINDOWS || MONOMAC || LINUX
                //7 is usual, 3 is nice too, as is 2
                BloomProcessor = new BloomProcessor(this);
                BarrelDistortionProcessor = new BarrelDistortionProcessor(this);
                DistortionsProcessor = new DistortionsProcessor(this);
                RippleDistortionProcessor = new RippleDistortionProcessor(this);

                PostProcess.AddProcessor(DistortionsProcessor);
                PostProcess.AddProcessor(BloomProcessor);
                PostProcess.AddProcessor(BarrelDistortionProcessor);
                PostProcess.AddProcessor(RippleDistortionProcessor);
            #endif

            WatchFolders();
        }

        private void WatchFolders()
        {
            var watcher = new FileSystemWatcher();
            watcher.Path = "../../../Resources";
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "*.*";
            watcher.IncludeSubdirectories = true;
            watcher.Changed += (sender, e) => { _resourcesChanged = true; };
            watcher.EnableRaisingEvents = true;
        }

        protected override void Initialize() 
        {
            GS.Initialize(this, true);
            _fpsCounter = new FpsCounter(this);
            Components.Add(_fpsCounter);
#if WINDOWS
                DebugSystem.Initialize(this, Platform.ContentPath + "/Fonts/Debug");
#else
			    DebugSystem.Initialize(this, Platform.Content + "/SpriteFonts/Font");
#endif

            Theme.Initialise();

            DemoRecorder = new DemoRecorder(this);
            GameCamera.Initialise();
            GuiCamera.Initialise();

            //Calling the base method here will call LoadContent before returning back here to finialize initializaition. Bear that in mind when 
            base.Initialize();
      
            //Initialise the final rendering viewport - this will allow the user to size the rendered image to match their display device
            SetViewports();

#if PROFILE || DEBUG
                RegisterDebugCommands();
                DebugSystem.Instance.DebugCommandUI.ExecuteCommand("debug");
#endif

            GamePadHelper.InputManager = NuclexInputManager;

            //Do something here with game phases!
            var splashScreenPhase = new SplashScreenPhase(this);
            splashScreenPhase.SplashComplete += SplashComplete;

            var gameOverPhase = new GameOverPhase(this);
            //gameOverPhase.NotifyGameOver += CreateGameOverMessage;

            GamePhaseManager.Register(splashScreenPhase);
            GamePhaseManager.Register(gameOverPhase);
            GamePhaseManager.Register(new IntroMenuPhase(this));
            GamePhaseManager.Register(new MenuGamePhase(this));
            GamePhaseManager.Register(new PlayGamePhase(this));
            GamePhaseManager.Register(new PostGameOverPhase(this));
            GamePhaseManager.Register(new TestPhase(this));

            GamePhaseManager.ChangePhase(typeof(SplashScreenPhase));
            //GamePhaseManager.ChangePhase(typeof(TestPhase));

            GS.Action(()=> GS.GearsetComponent.Console.Inspect("Profiler", GS.GearsetComponent.Console.Profiler));

            GS.AddQuickAction("Intro", () => SplashComplete(null, EventArgs.Empty));
            GS.AddQuickAction("Test", () => GamePhaseManager.ChangePhase(typeof(TestPhase)));
            GS.AddQuickAction("Collisions", () =>
            {
                GamePhaseManager.Register(new TestPhaseCollisions(this));
                GamePhaseManager.ChangePhase(typeof (TestPhaseCollisions));
            });
        }

        protected abstract IGameInput CreateGameInput();

        [Conditional("WINDOWS")]
        public void SetCursor(string filename)
        {
            var cur = new System.Drawing.Bitmap(filename, true);
            System.Drawing.Graphics.FromImage(cur);
            var ptr = cur.GetHicon();
            var c = new Cursor(ptr);
            Control.FromHandle(Window.Handle).Cursor = c;
        }

        /// <summary>
        /// Sets the size of the final viewport for rendering
        /// </summary>
        /// <remarks>Enables the ability to 'zoom' the screen in to respect title safe areas in a better way.</remarks>
        void SetViewports()
        {
            FullscreenViewport = GraphicsDevice.Viewport;

            const float targetAspectRatio = 16.0f / 9.0f;
            var actualAspectRatio = FullscreenViewport.Width / (float)FullscreenViewport.Height;

            var isWideScreen = actualAspectRatio.FloatEquals(targetAspectRatio);
            GS.Log($"isWideScreen: { isWideScreen }");

            var targetHeight = FullscreenViewport.Width / targetAspectRatio;
            var heightDifference = FullscreenViewport.Height - targetHeight;

            RenderViewport = new Viewport(
                    (int)MathHelper.Lerp(FullscreenViewport.TitleSafeArea.X + TitleSafeTest, FullscreenViewport.X, ViewportZoom),
                    (int)MathHelper.Lerp(FullscreenViewport.TitleSafeArea.Y + TitleSafeTest + (heightDifference / 2.0f), FullscreenViewport.Y + (heightDifference / 2.0f), ViewportZoom),
                    (int)MathHelper.Lerp(FullscreenViewport.TitleSafeArea.Width - (TitleSafeTest * 2), FullscreenViewport.Width, ViewportZoom),
                    (int)MathHelper.Lerp(FullscreenViewport.TitleSafeArea.Height - (TitleSafeTest * 2) - (heightDifference), FullscreenViewport.Height - (heightDifference), ViewportZoom));
        }

        protected override void LoadContent() 
        {
            base.LoadContent();

            EffectManager = new EffectManager(Content, GraphicsDevice);

            //Resource managers
            SoundManager.LoadContent();
            TextureManager.LoadContent();
            EffectManager.LoadContent();
            FontManager.LoadContent();
            Themes.LoadContent();
            ParticleTemplateManager.LoadContent();
            GodRaysTemplateManager.LoadContent();     

            DemoRecorder.LoadContent();

            PostProcess.LoadContent();

            Theme.LoadContent(this);
            GamePadHelper.LoadContent();

            Renderer = new Renderer(this);

            //Register the graphics styles
            new AdditiveRenderStyle().Register(EffectManager);
            new AlphaBlendingRenderStyle().Register(EffectManager);
            new FlashRenderStyle().Register(EffectManager);
            new OpaqueRenderStyle().Register(EffectManager);
            new TransparentRenderStyle().Register(EffectManager);
            new RippleRenderStyle().Register(EffectManager);
            new PointClampRenderStyle().Register(EffectManager);
            new PointWrapRenderStyle().Register(EffectManager);
            new CircleGaugeRenderStyle().Register(EffectManager);
            new HologramRenderStyle().Register(EffectManager);
            new WobblySineWaveRenderStyle().Register(EffectManager);

            AnimationManager.LoadContent();
            DataManager.LoadContent();
            EntityBlueprints.LoadContent();
            PlayerProfileManager.LoadContent();
            OverlayTemplateManager.LoadContent();
            MenuManager.LoadContent();
        }

        /// <summary>
        /// Must be called when the screen resolution is changed.
        /// </summary>
        public void ResolutionsChanged()
        {
            ViewportZoom = ViewportZoom; //Set the render viewports

            InitialiseTransform();

            //Need to advise all the post processing components that the res has changed as they might need to create new render targets.
            PostProcess.ResolutionChanged();
        }

        private void StorageManagerStorageDeviceSelected(object sender, StorageDeviceActionEventArgs e)
        {
            OnStorageManagerStorageDeviceSelected(sender, e);
        }

        protected abstract void OnStorageManagerStorageDeviceSelected(object sender, StorageDeviceActionEventArgs e);

        /// <summary>
        /// Changes the current phase of the game.
        /// </summary>
        public void ChangePhase(Type type)
        {
			GamePhaseManager.ChangePhase(type);
		}

        /// <summary>
        /// 
        /// </summary>
        protected override void BeginRun()
        {
            base.BeginRun();

            CreatePlayers();

            //Setup the view and projection matrices.
            InitialiseTransform();

			SoundManager.MusicVolume = GameOptions.MusicVolume;
            SoundManager.SfxVolume = GameOptions.SfxVolume;
        }

        /// <summary>
        /// Creates the default players.
        /// </summary>
        /// <remarks>This is the maximum number of players who can play the actual amount playing may be  </remarks>
        private void CreatePlayers()
        {
            //One player for now!
            Players.Clear();

            var profileName = PlayerProfileManager.GetProfileInfo(ExtendedPlayerIndex.One).DisplayName;
            
            var player = new Player();
            player.Profile = PlayerProfileManager.GetProfile(profileName);

            Players.Add(player);
        }

        /// <summary>
        /// Sets up the view and projection matrices.
        /// </summary>
        void InitialiseTransform()
        {
            WorldMatrix = Matrix.Identity;

            //Sets the view matrix
            CameraManager.Reset();

            //Render viewport now respects aspect ratios (it is modified for non 16:9 one) so the projection is constant now
            ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45.0f), RenderViewport.Width / (float)RenderViewport.Height, 1.0f, 1000.0f);
        }

        private void RegisterDebugCommands()
        {
            var host = Services.GetService(typeof(IDebugCommandHost)) as IDebugCommandHost;

            if (host == null) 
                return;

            host.RegisterCommand("vsync", "Toggle VSync [on/off]", DebugCommandToggleVSync);
            host.RegisterCommand("fixedstep", "Toggle fixed time step [on/off]", DebugCommandToggleFixedStep);
            host.RegisterCommand("load", "Reload all XML scripts", DebugCommandReloadScripts);
            host.RegisterCommand("monkeying", "Toggle monkeying [on/off]", DebugCommandToggleMonkeying);
            host.RegisterCommand("music", "play music [off/music name/[empty:random]", DebugCommandPlayMusic);

            DebugCommandToggleFixedStep(host, null, new List<string> {"on"});

            OnRegisterDebugCommands(host);
        }

        protected virtual void OnRegisterDebugCommands(IDebugCommandHost host) { }

        /// <summary>
        /// Debug command to toggle vsync on/off.
        /// </summary>
        private void DebugCommandToggleFixedStep(IDebugCommandHost host, string command, IList<string> arguments)
        {
            if (arguments == null ||arguments.Count == 0)
                host.Echo(IsFixedTimeStep ? "on" : "off");

            if (arguments == null)
                return;

            foreach (var arg in arguments)
            {
                switch (arg.ToLower())
                {
                    case "on":
                        IsFixedTimeStep = true;
                        break;

                    case "off":
                        IsFixedTimeStep = false;
                        break;
                }
            }
        }

        /// <summary>
        /// Debug command to toggle vsync on/off.
        /// </summary>
        private void DebugCommandToggleVSync(IDebugCommandHost host, string command, IList<string> arguments)
        {
            if (arguments.Count == 0)
                host.Echo(Graphics.SynchronizeWithVerticalRetrace ? "on" : "off");

            foreach (var arg in arguments)
            {
                switch (arg.ToLower())
                {
                    case "on":
                        Graphics.SynchronizeWithVerticalRetrace = true;
                        GraphicsDevice.Reset();
                        break;

                    case "off":
                        Graphics.SynchronizeWithVerticalRetrace = false;
                        GraphicsDevice.Reset();
                        break;
                }
            }
        }

        /// <summary>
        /// Debug command to toggle vsync on/off.
        /// </summary>
        private void DebugCommandPlayMusic(IDebugCommandHost host, string command, IList<string> arguments)
        {
            if (arguments.Count == 0)
            {
                SoundManager.PlayMusic();
                host.Echo("Now playing..." + SoundManager.MusicName);
            }
            else
            {
                if (arguments[0].ToLower() == "off")
                {
                    SoundManager.StopMusic(true);
                    host.Echo("Music off");
                }
                else 
                {
                    SoundManager.PlayMusic(arguments[0]);
                    host.Echo("Now playing..." + SoundManager.MusicName);
                }
            }
        }

        /// <summary>
        /// Debug command to toggle vsync on/off.
        /// </summary>
        private static void DebugCommandToggleMonkeying(IDebugCommandHost host, string command, IList<string> arguments)
        {
            if (arguments.Count == 0)
                MenuControl.Monkeying = !MenuControl.Monkeying;
            else
            {
                if (arguments[0].ToLower() == "on")
                    MenuControl.Monkeying = true;
                else if (arguments[0].ToLower() == "off")
                    MenuControl.Monkeying = false;
            }

            host.Echo(MenuControl.Monkeying ? "monkeying is now on" : "monkeying is now off");
        }

        private void DebugCommandReloadScripts(IDebugCommandHost host, string command, IList<string> arguments)
        {
            //Copy scripts to correct area

            host.Echo("Loading Sounds...");
            SoundManager.Reload(); 
            host.Echo("Loading particles...");
            ParticleTemplateManager.Reload(); 
            host.Echo("Loading godrays...");
            GodRaysTemplateManager.Reload(); 
            host.Echo("Loading godrays...");
            Themes.Reload(); 
            host.Echo("Loading animation...");
            AnimationManager.Reload();
            host.Echo("Loading actors...");
            DataManager.Reload();
            host.Echo("Loading entities...");
            EntityBlueprints.Reload();
            host.Echo("Loading overlays...");
            OverlayTemplateManager.Reload();
            
            //TODO: Do HUD here as well
            //TODO: DO Levels here - we want some way of subscribing new handlers for game specific stuff.
            host.Echo("Loading game maps...");
            GameMapManager.Reload();

            MenuManager.ConfigureOverlays();
            MenuManager.ConfigureMenus();

            MenuManager.TopMenu?.GotFocus();

            ScreenFlashManager.ClearColor = Theme.Current.GetColor(Theme.Colors.Background);

            OnReloadScripts();
        }

        protected virtual void OnReloadScripts() { }

        protected override void OnActivated(object sender, EventArgs args)
        {
            GameHasFocus = true;

            if (DemoRecorder == null)
                return;

            if (DemoRecorder.IsPlaying)
            {
                IsPaused = false;
                if (SoundManager != null)
                    SoundManager.MusicVolume = GameOptions.MusicVolume;
            }
            else if (MenuManager != null)
            {
                if (MenuManager.ContainsMenu(typeof(ShopMenu)))
                    return;

                if (MenuManager.ContainsMenu(typeof(PauseMenu)) == false || MenuManager.GetMenu(typeof(PauseMenu)).IsClosing)
                {
                    IsPaused = false;
                    if (SoundManager != null)
                        SoundManager.MusicVolume = GameOptions.MusicVolume;
                }
            }
        }

        protected override void OnDeactivated(object sender, EventArgs args)
        {
            GameHasFocus = false;

            #if USE_GEARSET
                return;
            #endif

            Pause(MenuManager.UnspecifiedControllerId);
        }

        /// <summary>
        /// Pauses the game.
        /// </summary>
        protected void Pause(int controllerId)
        {
            if (IsPaused)
                return;

            IsPaused = true;
            SoundManager.MusicVolume = 0.0f;

            if (IsSplashComplete == false)
                return;

            if (MenuManager.HasMenu)
                return;

            if (DemoRecorder.IsPlaying)
                return;

            FadeOut();

            //Although the user cannot initiate pause if there is a menu showing, the app could lose focus and the
            //default OnDeactivate event could attempt the pause.  If it does there may be a BusyBox menu showing
            var menuAdded = MenuManager.EnsurePauseMenu();

            if (controllerId == -1)
                return;

            if (menuAdded == false)
                return;

            //If a pause menu was added and the player causing the pause is not the primary controller
            //(e.g. in a 2 plater game the primary player may not even being playing)
            if (controllerId != MenuManager.PrimaryControllerId)
            {
                _savedPrimaryControllerId = MenuManager.PrimaryControllerId;
                MenuManager.SetPrimaryControllerId(controllerId);
            }
        }

        /// <summary>
        /// Forces a pause of the game.
        /// </summary>
        public void SimplePause()
        {
            IsPaused = true;
        }
		
        /// <summary>
        /// Unpauses the game.
        /// </summary>
        public void Unpause()
        {
            IsPaused = false;
            SoundManager.MusicVolume = GameOptions.MusicVolume;
        }

        /// <summary>
        /// Resets the primary controller it had been changed (e.g. in a two player game).
        /// </summary>
        public void ResetPrimaryControllerIfRequired()
        {
            //Reset the primary controller?
            if (_savedPrimaryControllerId != -1)
            {
                MenuManager.SetPrimaryControllerId(_savedPrimaryControllerId);
                _savedPrimaryControllerId = -1;
            }
        }

        void SplashComplete(object sender, EventArgs e)
        {
            IsSplashComplete = true;
        
            ChangePhase(typeof(IntroMenuPhase));
            MenuManager.AddMenu(typeof(IntroMenu));

            ScreenFlashManager.ClearColor = Theme.Current.GetColor(Theme.Colors.Background);

            SoundManager.PlayMusic();
        }

        /// <summary>
        /// Resumes the current play session.
        /// </summary>
        public void Resume()
        {
            Unpause();
            FadeIn();
        }

        /// <summary>
        /// Restarts the current play session.
        /// </summary>
        public void Restart()
        {
            Unpause();
            BeginPlaySession();
            FadeIn();
        }

        /// <summary>
        /// Quits the current play session.
        /// </summary>
        public void Quit()
        {
            Unpause();
            EndPlaySession();
        }

        public abstract void BeginPlaySession();
        public abstract void ContinuePlaySession();
        public abstract void EndPlaySession();
        
        public void ReturnToIntro()
        {
            MenuManager.FadeOut();
            MenuManager.SetPrimaryControllerId(-2);
            
            DemoRecorder.Stop();

            OnReturnToIntro();
        }

        protected virtual void OnReturnToIntro() { }

        /// <summary>
        /// 
        /// </summary>
        public void FadeIn()
        {
            MenuManager.FadeOut();

            OnFadeIn();
        }

        protected virtual void OnFadeIn() { }

        /// <summary>
        /// 
        /// </summary>
        public void FadeOut()
        {
            MenuManager.FadeIn();

            OnFadeOut();
        }

        protected virtual void OnFadeOut() { }

        /// <summary>
        /// Plays the requested sound (or music).
        /// </summary>
        public void PlaySound(string name)
        {
            SoundManager.PlaySound(name);
        }

        void RefreshResources()
        {
            var host = Services.GetService(typeof(IDebugCommandHost)) as IDebugCommandHost;

            if (host == null) 
                return;

            DebugCommandReloadScripts(host, null, new List<string> {"load"});
        }

        protected sealed override void Update(GameTime gameTime) 
        {
            //We must call StartFrame at the top of Update to indicate to the TimeRuler that a new frame has started.
            GS.StartFrame();

            GameTimeInSeconds = (float)gameTime.TotalGameTime.TotalSeconds;

            //Reload the resources if any of the file contents have changed.
            if (_resourcesChanged)
            {
                RefreshResources();
                _resourcesChanged = false;
                GameHasFocus = true;
            }

            StorageManager.Update();
            
            if (GameHasFocus == false)
                return;

            GS.BeginMark("GameBase:Update", FlatTheme.PeterRiver);

            GS.Plot("FPS", _fpsCounter.Fps);
            GS.Plot("Renderables", Renderer.RenderablesCount);
            GS.Plot("Render Batches", Renderer.DrawCalls);
            
            base.Update(gameTime);

            KeyboardHelper.Update();
            GamePadHelper.Update();
            MouseHelper.Update();

            //If the sleep has no effect, I must be GPU bound.
            //If skipping Update speeds things up, I must be CPU bound.
            //If skipping Update has no effect but sleeping does slow things down, the two must be evenly balanced.
#if USE_GEARSET
                if (GS.GearsetComponent.Console.Profiler.DoUpdate() == false)
                {
                    GS.EndMark("GameBase:Update");
                    return;
                }   
#endif

            if (MenuManager.HasMenu)
                InputMethodManager.DetermineActiveInputMethod(ActivePlayer.PlayerIndex); 

            //StorageManager.Update();
            
            VibrationManager.SupportsVibration = InputMethodManager.InputMethod.SupportsVibration;
            VibrationManager.DemoPlaying = DemoRecorder.IsPlaying;
            VibrationManager.Update();
            
            MenuManager.Update();

            DistortionEffects.Active = !MenuManager.HasMenu;
            RippleDistortionProcessor.Active = !MenuManager.HasMenu || MenuManager.TopMenu is ViewDemoMenu;
            
            //if (GetGameInput() == false)
            //    return;

            if (IsPaused == false)
            {
                GamePhaseManager.Update();
                SoundManager.Update();
                ScreenFlashManager.Update();
                //WebManager.Update();
            }

            OnUpdate(gameTime);

            GS.EndMark("GameBase:Update");
        }

        protected abstract void OnUpdate(GameTime gameTime);

        protected sealed override void Draw(GameTime gameTime) 
        {
            DrawId++;

            GS.BeginMark("GameBase:Draw", FlatTheme.Pomegrantate);
            Renderer.BeginFrame();

            PostProcess.PreRender();
            GraphicsDevice.Viewport = RenderViewport;
            GraphicsDevice.Clear(ScreenFlashManager.CurrentColor);

            //Be nice if I could wrap the following up into a nice conditional constuct somehow
            //using (new TimeRulerHelper(1, "OnDraw(gameTime);", FlatTheme.Pumpkin))
            //    OnDraw(gameTime);

            GS.BeginMark("OnDraw(gameTime);", FlatTheme.Pumpkin);
            OnDraw(gameTime);
            GS.EndMark("OnDraw(gameTime);");

            GS.BeginMark("PostProcess:PostRender();", FlatTheme.Sunflower);
            GraphicsDevice.Viewport = FullscreenViewport;
            PostProcess.PostRender();
            GS.EndMark("PostProcess:PostRender();");

            GS.BeginMark("base:Draw(gameTime);", FlatTheme.Emerald);
            base.Draw(gameTime);
            GS.EndMark("base:Draw(gameTime);");

            GS.EndMark("GameBase:Draw");
        }

        protected abstract void OnDraw(GameTime gameTime);
    
                /// <summary>
        /// Must be called when the CRT Post Processing stuff has changed.
        /// </summary>
        public void CrtEffectChanged()
        {
            BarrelDistortionProcessor.Enabled = GameOptions.CrtEffects;
            DistortionsProcessor.Enabled = GameOptions.CrtEffects;
        }

        /// <summary>
        /// Must be called when the Bloom Post Processing stuff has changed.
        /// </summary>
        public void BloomChanged()
        {
            BloomProcessor.Enabled = GameOptions.Bloom;
        }

        public void VibrationChanged()
        {
            VibrationManager.Enabled = GameOptions.Vibration;
        }

        public static long GetTotalMemory()
        {
            return GC.GetTotalMemory(true);
        }
    }
}
