using Transition.ActorDatas;

namespace Transition.Actors
{
    /// <summary>
    /// Represents an enemy bullet
    /// </summary>
    public abstract class EnemyBullet : Actor
    {
        public EnemyBulletData EnemyBulletData;

        public TextureInfo BlastTextureInfo;
        public int BlastDuration;
        public int BlastFadeDuration;
        public float BlastRadius;
        public float BlastWidth;
        
        public override bool IsNukeable { get { return true; } }

        /// <summary>
        /// Creates a new instance of an AlienBullet
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        protected EnemyBullet(TransitionGame game) : base(game)
        {
            _horizontalConstrainBehaviour = ConstrainBehaviour.Deactivate;
			_verticalConstrainBehaviour = ConstrainBehaviour.Deactivate;
        }

        /// <summary>
        /// Gets the type of actor
        /// </summary>
        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.EnemyBullet; }
        }

        /// <summary>
        /// Gets whether this type of actor collides.
        /// </summary>
        public override int CollisionMask
        {
            get { return 1 << (int)ActorClassification; }
        }

        protected override void OnInitialise(string name)
        {
            base.OnInitialise(name);

            EnemyBulletData = _game.DataManager.EnemyBulletData(name);

            BlastDuration = 60;
            BlastFadeDuration = 15;
            BlastRadius = 2.0f;
            BlastWidth = 2.0f;
        }

        protected override void OnAddedToScene()
        {
            //Play a sound?
            if (EnemyBulletData.Sounds != null && EnemyBulletData.Sounds.Count > 0)
                _game.PlaySound(EnemyBulletData.Sounds[RandomHelper.Random.GetInt(0, EnemyBulletData.Sounds.Count - 2)]);
        }

        protected override void OnConstrained(bool constrainedTop, bool constrainedLeft, bool constrainedBottom, bool constrainedRight)
        {
            MaybeCreateBlast();

            if (EnemyBulletData.Visible)
                CreateParticleSystems(_actorData.ExpiredEffects, Ownership.None);
        }

        protected override void OnExpire()
        {
            MaybeCreateBlast();
        }

        private void MaybeCreateBlast()
        {
            if (EnemyBulletData.Explode)
            {
                var blastEffect = (BlastEffect)_game.ActorFactory.Create(typeof(BlastEffect));
                if (blastEffect != null)
                {
                    blastEffect.Setup(Position, BlastDuration, BlastFadeDuration, BlastRadius, BlastWidth, BlastTextureInfo);
                    //blastEffect.Tint = ColourScheme.Colours[0].BaseColour;
                    Scene.AddActor(blastEffect);
                }
            }

            base.OnExpire();
        }
    }
}
