using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Transition._____Temp.StarGuard.EntityBlueprints;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control for displaying the possible ships / ship info.
    /// </summary>
    public class ShipSelectionControl : MenuControl
    {			
        private class ShipInfo
        {
            private readonly TextLabelControl _shipClassification;
            private readonly ImageControl _shipImage;

            private readonly TextLabelControl _accelerationValue;
            private readonly TextLabelControl _speedValue;
            private readonly TextLabelControl _accelerationCaption;
            private readonly TextLabelControl _speedCaption;
            private readonly TextLabelControl _descriptionCaption;
            private readonly TextLabelControl _descriptionDescription;
            private readonly TextLabelControl _weaponCaption;
            private readonly TextLabelControl _weaponDescription;
            private readonly TextLabelControl _rageCaption;
            private readonly TextLabelControl _rageDescription;
            private readonly TextLabelControl _nukeCaption;
            private readonly TextLabelControl _nukeDescription;

            private readonly SliderControl _accelerationSlider;
            private readonly SliderControl _speedSlider;

            public readonly ShipSelectionControl Parent;
            public readonly ShipBlueprint ShipBlueprint;
            public Vector2 TargetPosition;
            public float Alpha;
            public float TargetAlpha;

            /// <summary>The position of the control relative to its parent if it has one; otherwise world coordinates.</summary>
            private Vector2 _position;
            private Vector2 _anchorPosition;
            public Vector2 Position
            {
                get { return Parent != null ? Parent.Position + _position : _position; }
                set
                {
                    _position = value;
                    _anchorPosition = value;
                }
            }

            public ShipInfo(BaseGame game, ShipSelectionControl shipSelectionControl, ShipBlueprint shipBlueprint, Vector2 basePosition, float alpha)
            {
                ShipBlueprint = shipBlueprint;
                Position = basePosition;
                Alpha = alpha;
                Parent = shipSelectionControl;

                var x = Position.X;
                var x1 = -5.0f + Position.X;
                var x2 = -10.5f + Position.X;
                var x3 = 5.0f + Position.X;
                var yPosition = 5.0f + Position.Y;
                const float valueOffsetY = 0.8f;

                _shipClassification = new TextLabelControl(game);
                _shipClassification.Position = new Vector2(shipSelectionControl.Position.X, yPosition);
                _shipClassification.Text = shipBlueprint.Classification;
                _shipClassification.Tint = Theme.LabelCaptionColor * alpha;
                _shipClassification.Alignment = Alignment.Centre;
                _shipClassification.FontSize = new Vector2(2.0f);
                _shipClassification.Parent = shipSelectionControl;

                yPosition = 0.0f + shipSelectionControl.Position.Y;
                _shipImage = new ImageControl(game);
                _shipImage.Position = new Vector2(x, yPosition);
                _shipImage.Tint = Theme.StandardColor * alpha;
                _shipImage.Size = new Vector2(8.5f);
                _shipImage.TextureInfo = shipBlueprint.MenuTextureInfo;
                _shipImage.Parent = shipSelectionControl;
     
                _accelerationValue = new TextLabelControl(game);
                _speedValue = new TextLabelControl(game);
                _accelerationCaption = new TextLabelControl(game);
                _speedCaption = new TextLabelControl(game);
                _descriptionCaption = new TextLabelControl(game);
                _descriptionCaption = new TextLabelControl(game);
                _descriptionDescription = new TextLabelControl(game);
                _weaponCaption = new TextLabelControl(game);
                _weaponDescription = new TextLabelControl(game);
                _rageCaption = new TextLabelControl(game);
                _rageDescription = new TextLabelControl(game);
                _nukeCaption = new TextLabelControl(game);
                _nukeDescription = new TextLabelControl(game);

                _accelerationValue.Parent = shipSelectionControl;
                _speedValue.Parent = shipSelectionControl;
                _accelerationCaption.Parent = shipSelectionControl;
                _speedCaption.Parent = shipSelectionControl;
                _descriptionCaption.Parent = shipSelectionControl;
                _descriptionCaption.Parent = shipSelectionControl;
                _descriptionDescription.Parent = shipSelectionControl;
                _weaponCaption.Parent = shipSelectionControl;
                _weaponDescription.Parent = shipSelectionControl;
                _rageCaption.Parent = shipSelectionControl;
                _rageDescription.Parent = shipSelectionControl;
                _nukeCaption.Parent = shipSelectionControl;
                _nukeDescription.Parent = shipSelectionControl;

                yPosition = 3.0f + shipSelectionControl.Position.Y;

                _accelerationCaption.Position = new Vector2(x1, yPosition);
                _accelerationCaption.Text = "Acceleration";
                _accelerationCaption.Tint = Theme.LabelCaptionColor * alpha;
                _accelerationCaption.Alignment = Alignment.CentreRight;

                yPosition -= valueOffsetY;

                _accelerationSlider = new SliderControl(game);
                _accelerationSlider.Position = new Vector2(x1, yPosition);
                _accelerationSlider.Tint = Theme.LabelValueColor * alpha;
                _accelerationSlider.Size = new Vector2(5.0f, 0.5f);
                _accelerationSlider.Fill = SliderControl.FillMode.RightToLeft;
                _accelerationSlider.Step = 1;
                _accelerationSlider.Value = (int)MathHelper.Lerp(50, 100, 1 - ((0.04f - shipBlueprint.AccelerationX) * 100));

                _accelerationValue.Position = new Vector2(x2, yPosition);
                _accelerationValue.Text = ((int)(_accelerationSlider.Value * 100)) + "%";
                _accelerationValue.Tint = Theme.LabelValueColor * alpha;
                _accelerationValue.Alignment = Alignment.CentreRight;

                yPosition = 0.5f + shipSelectionControl.Position.Y;

                _speedCaption.Position = new Vector2(x1, yPosition);
                _speedCaption.Text = "Speed";
                _speedCaption.Tint = Theme.LabelCaptionColor * alpha;
                _speedCaption.Alignment = Alignment.CentreRight;

                yPosition -= valueOffsetY;

                _speedSlider = new SliderControl(game);
                _speedSlider.Position = new Vector2(x1, yPosition);
                _speedSlider.Tint = Theme.LabelValueColor * alpha;
                _speedSlider.Size = new Vector2(5.0f, 0.5f);
                _speedSlider.Fill = SliderControl.FillMode.RightToLeft;
                _speedSlider.Step = 1;
                _speedSlider.Value = (int)MathHelper.Lerp(50, 100, 1.0f - ((0.65f - shipBlueprint.MaxVelocityX) / 0.25f));

                _speedValue.Position = new Vector2(x2, yPosition);
                _speedValue.Text = ((int)(_speedSlider.Value * 100)) + "%";
                _speedValue.Tint = Theme.LabelValueColor * alpha;
                _speedValue.Alignment = Alignment.CentreRight;

                yPosition = -2.0f + shipSelectionControl.Position.Y;

                _nukeCaption.Position = new Vector2(x1, yPosition);
                _nukeCaption.Text = "Nuke";
                _nukeCaption.Tint = Theme.LabelCaptionColor * alpha;
                _nukeCaption.Alignment = Alignment.CentreRight;

                yPosition -= valueOffsetY;

                _nukeDescription.Position = new Vector2(x1, yPosition);
                _nukeDescription.Text = shipBlueprint.Nuke;
                _nukeDescription.Tint = Theme.LabelValueColor * alpha;
                _nukeDescription.Alignment = Alignment.CentreRight;

                yPosition = 3.0f + shipSelectionControl.Position.Y;

                _descriptionCaption.Position = new Vector2(x3, yPosition);
                _descriptionCaption.Text = "Information";
                _descriptionCaption.Tint = Theme.LabelCaptionColor * alpha;
                _descriptionCaption.Alignment = Alignment.CentreLeft;

                yPosition -= valueOffsetY;

                _descriptionDescription.Position = new Vector2(x3, yPosition);
                _descriptionDescription.Text = shipBlueprint.Description;
                _descriptionDescription.Tint = Theme.LabelValueColor * alpha;
                _descriptionDescription.Alignment = Alignment.CentreLeft;

                yPosition = 0.5f + shipSelectionControl.Position.Y;

                _weaponCaption.Position = new Vector2(x3, yPosition);
                _weaponCaption.Text = "Weapon";
                _weaponCaption.Tint = Theme.LabelCaptionColor * alpha;
                _weaponCaption.Alignment = Alignment.CentreLeft;

                yPosition -= valueOffsetY;

                _weaponDescription.Position = new Vector2(x3, yPosition);
                _weaponDescription.Text = game.DataManager.WeaponData(ShipBlueprint.WeaponName).Description;
                _weaponDescription.Tint = Theme.LabelValueColor * alpha;
                _weaponDescription.Alignment = Alignment.CentreLeft;

                yPosition = -2.0f + shipSelectionControl.Position.Y;

                _rageCaption.Position = new Vector2(x3, yPosition);
                _rageCaption.Text = "Rage";
                _rageCaption.Tint = Theme.LabelCaptionColor * alpha;
                _rageCaption.Alignment = Alignment.CentreLeft;

                yPosition -= valueOffsetY;

                _rageDescription.Position = new Vector2(x3, yPosition);
                _rageDescription.Text = shipBlueprint.RageType.Description();
                _rageDescription.Tint = Theme.LabelValueColor * alpha;
                _rageDescription.Alignment = Alignment.CentreLeft;

                shipSelectionControl.AddControl(_shipClassification, -1);
                shipSelectionControl.AddControl(_accelerationValue, -1);
                shipSelectionControl.AddControl(_speedValue, -1);
                shipSelectionControl.AddControl(_shipImage, -1);
                shipSelectionControl.AddControl(_accelerationCaption, -1);
                shipSelectionControl.AddControl(_speedCaption, -1);
                shipSelectionControl.AddControl(_descriptionCaption, -1);
                shipSelectionControl.AddControl(_descriptionDescription, -1);
                shipSelectionControl.AddControl(_weaponCaption, -1);
                shipSelectionControl.AddControl(_weaponDescription, -1);
                shipSelectionControl.AddControl(_rageCaption, -1);
                shipSelectionControl.AddControl(_rageDescription, -1);
                shipSelectionControl.AddControl(_nukeCaption, -1);
                shipSelectionControl.AddControl(_nukeDescription, -1);

                shipSelectionControl.AddControl(_accelerationSlider, -1);
                shipSelectionControl.AddControl(_speedSlider, -1);
            }
            
            public void Update(float ratio)
            {
                //var x = Interpolate.BackBoth(Position.X, TargetPosition.X, ratio);
                var alpha = MathHelper.SmoothStep(Alpha, TargetAlpha, ratio);

                //This is the base position for this info...
                var x = MathHelper.SmoothStep(Position.X, TargetPosition.X, ratio);
                var position = new Vector2(x, Position.Y);

                var x1 = position.X - 5.0f;
                var x2 = position.X - 10.5f;
                var x3 = position.X + 5.0f;
                const float valueOffsetY = 0.8f;

                _shipClassification.Tint = Theme.LabelCaptionColor * alpha;
               
                var yPosition = 0.0f + position.Y;

                _shipImage.Tint = Theme.StandardColor * alpha;
                _shipImage.Position = new Vector2(position.X, yPosition);

                yPosition = 3.0f + position.Y;
       
                _accelerationCaption.Tint = Theme.LabelCaptionColor * alpha;
                _accelerationCaption.Position = new Vector2(x1, yPosition);

                yPosition -= valueOffsetY;
           
                _accelerationSlider.Tint = Theme.LabelValueColor * alpha;
                _accelerationSlider.Position = new Vector2(x1, yPosition);

                _accelerationValue.Tint = Theme.LabelValueColor * alpha;
                _accelerationValue.Position = new Vector2(x2, yPosition);

                yPosition = 0.5f + position.Y;
           
                _speedCaption.Tint = Theme.LabelCaptionColor * alpha;
                _speedCaption.Position = new Vector2(x1, yPosition);

                yPosition -= valueOffsetY;
       
                _speedSlider.Tint = Theme.LabelValueColor * alpha;
                _speedSlider.Position = new Vector2(x1, yPosition);

                _speedValue.Tint = Theme.LabelValueColor * alpha;
                _speedValue.Position = new Vector2(x2, yPosition);

                yPosition = -2.0f + position.Y;

                _nukeCaption.Tint = Theme.LabelCaptionColor * alpha;
                _nukeCaption.Position = new Vector2(x1, yPosition);

                yPosition -= valueOffsetY;

                _nukeDescription.Tint = Theme.LabelValueColor * alpha;
                _nukeDescription.Position = new Vector2(x1, yPosition);

                yPosition = 3.0f + position.Y;

                _descriptionCaption.Tint = Theme.LabelCaptionColor * alpha;
                _descriptionCaption.Position = new Vector2(x3, yPosition);

                yPosition -= valueOffsetY;

                _descriptionDescription.Tint = Theme.LabelValueColor * alpha;
                _descriptionDescription.Position = new Vector2(x3, yPosition);

                yPosition = 0.5f + position.Y;

                _weaponCaption.Tint = Theme.LabelCaptionColor * alpha;
                _weaponCaption.Position = new Vector2(x3, yPosition);

                yPosition -= valueOffsetY;

                _weaponDescription.Tint = Theme.LabelValueColor * alpha;
                _weaponDescription.Position = new Vector2(x3, yPosition);

                yPosition = -2.0f + position.Y;

                _rageCaption.Tint = Theme.LabelCaptionColor * alpha;
                _rageCaption.Position = new Vector2(x3, yPosition);

                yPosition -= valueOffsetY;

                _rageDescription.Tint = Theme.LabelValueColor * alpha;
                _rageDescription.Position = new Vector2(x3, yPosition);
            }
        }

        private readonly Vector2 _leftButtonOffset = new Vector2(-4.0f, 5.0f);
        private const float ItemSpacer = 30.0f;

        private enum Phase
        {
            NextItem,
            Normal,
            PreviousItem,
        }

        private Phase _phase = Phase.Normal;

        private int _selectedIndex;
        private readonly TextureInfo _previousImageInfo;
        private readonly TextureInfo _nextImageInfo;

        private int _ticks;
        private const int TransitionTicks = 30;

        private readonly List<ShipInfo> _ships = new List<ShipInfo>();

        /// <summary></summary>
		public Vector2 ButtonSize;

        public string SelectedShipName => _ships[_selectedIndex].ShipBlueprint.Name;
        public string SelectedShipDescription => _ships[_selectedIndex].ShipBlueprint.Description;

        /// <summary>
        /// Initialises a new instance of an ImageControl.
        /// </summary>
		/// <param name="game">A reference to the game.</param>
		public ShipSelectionControl(BaseGame game) : base(game) 
        {
            ButtonSize = new Vector2(1.25f);
            _nextImageInfo = Game.TextureManager.GetTextureInfo("NextItem");
            _previousImageInfo = Game.TextureManager.GetTextureInfo("PreviousItem");        
        }

        public void Initialise(IEnumerable<ShipBlueprint> shipBlueprints)
        {
            var position = Position;
            var index = 0;
            
            foreach (var shipBlueprint in shipBlueprints)
            {
                var alpha = index == 0 ? 1.0f : 0.0f;
                position = new Vector2(0);
                var shipInfo = new ShipInfo(Game, this, shipBlueprint, position, alpha);
                shipInfo.TargetPosition = position;
                shipInfo.TargetAlpha = alpha;

                _ships.Add(shipInfo);   

                position.X += ItemSpacer;
                index++;
             }
        }
        
        /// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
		public override Rect Bounds
		{
            get 
            { 
                var halfButtonSize = ButtonSize / 2.0f;
                var left = Position.X + _leftButtonOffset.X - halfButtonSize.X;
                var right = -left;

                var top = Position.Y + _leftButtonOffset.Y + halfButtonSize.Y;
                var bottom = top - ButtonSize.Y;

                return new Rect(top, left, bottom, right); 
            }
		}

        /// Gets the text to display for the left and right dpad / stick action.
        public override string LeftRightButtonText => "change ship";

        /// <summary>
        /// Adds a control to the overlay.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="order"></param>
        public void AddControl(MenuControl control, int order)
        {
            Overlay.AddControl(control, order);
        }

        /// <summary>
        /// Gets or sets the index of the selected item.
        /// </summary>
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set 
            { 
                if (_selectedIndex != value)
                {
                    InstantTransitionToSelectedIndex(_selectedIndex, value);
                    _selectedIndex = value; 
                }
            }
        }

        		/// <summary>
		/// Sets the selected item in the list.
		/// </summary>
		/// <param name="textToFind">The text used to perform the match.</param>
		public void SetSelectedItemByText(string textToFind)
		{			
			var index = _ships.FindIndex(shipInfo => (shipInfo.ShipBlueprint.Name == textToFind));
			if (index > -1)
				SelectedIndex = index;
		}

        private bool MouseOverPreviousButton
        {
            get
            {
                var r = Bounds;
                r.Right = r.Left + ButtonSize.X;
                return r.Contains(Game.MenuManager.MouseWorldPosition);
            }
        }

        private bool MouseOverNextButton
        {
            get
            {
                var r = Bounds;
                r.Left = r.Right - ButtonSize.X;
                return r.Contains(Game.MenuManager.MouseWorldPosition);
            }
        }

        //Only allow a input if we aren't transitioning
        private bool AcceptsInput => _phase == Phase.Normal;

        /// <summary>
		/// .
		/// </summary>
		protected override void OnMouseUp()
		{
            if (AcceptsInput == false)
                return;

            if (_selectedIndex > 0)
            {
                if (MouseOverPreviousButton)
                    PerformDecreaseValue();
            }

            if (_selectedIndex < _ships.Count - 1)
            {
                if (MouseOverNextButton)
                    PerformIncreaseValue();
            }
		}

        protected override bool OnClick() 
        {
            if (AcceptsInput == false)
                return false;

            if (Game.InputMethodManager.InputMethod.SupportsMouse == false)
                return true;

            if (MouseOverPreviousButton || MouseOverNextButton)
                return false;
                
            //Hmm, don;t want this control to be able to select
            return false;
        }

        /// <summary>
        /// Tells a control to increase its value.
        /// </summary>
        protected override bool OnIncreaseValue()
        {
            if (_selectedIndex >= _ships.Count - 1)
                return false;

            return MaybeStartTransition(Phase.NextItem, -ItemSpacer, _selectedIndex + 1);
        }

        /// <summary>
        /// Tells a control to decrease its value.
        /// </summary>
        protected override bool OnDecreaseValue()
        {
            if (_selectedIndex <= 0)
                return false;

            return MaybeStartTransition(Phase.PreviousItem, ItemSpacer, _selectedIndex - 1);
        }

        private void InstantTransitionToSelectedIndex(int currentSelectedIndex, int newSelectedIndex)
        {
            //How many transitions? And which direction
            var itemSpacer = (currentSelectedIndex - newSelectedIndex) * ItemSpacer;
            foreach(var shipInfo in _ships)
            {
                shipInfo.TargetPosition = shipInfo.Position + new Vector2(itemSpacer, 0);
            }

            _ships[currentSelectedIndex].TargetAlpha = 0.0f;
            _ships[newSelectedIndex].TargetAlpha = 1.0f;

            foreach(var shipInfo in _ships)
            {
                shipInfo.Position = shipInfo.TargetPosition;
                shipInfo.Alpha = shipInfo.TargetAlpha;
                shipInfo.Update(1.0f);
            }
        }

        private bool MaybeStartTransition(Phase phase, float targetX, int targetIndex)
        {
            //We want to begin a transition to the previous item if we can
            if (_phase != Phase.Normal)
                return false;

            foreach(var shipInfo in _ships)
                shipInfo.TargetPosition = shipInfo.Position + new Vector2(targetX, 0);

            _ships[_selectedIndex].TargetAlpha = 0.0f;
            _ships[targetIndex].TargetAlpha = 1.0f;

            _ticks = TransitionTicks;
            _phase = phase; 

            return true;
        }

        protected override void OnUpdate() 
        {
            if (_ticks > 0)
            {
                _ticks--;

                if (_ticks == 0)
                {
                    //Transition complete!

                    if (_phase == Phase.PreviousItem)
                        _selectedIndex--;
                    else if (_phase == Phase.NextItem)
                        _selectedIndex++;
                 
                    _phase = Phase.Normal;

                    //Just ensure that there's no rounding errors or whatnot, eh?
                    foreach(var shipInfo in _ships)
                    {
                        shipInfo.Position = shipInfo.TargetPosition;
                        shipInfo.Alpha = shipInfo.TargetAlpha;
                    }
                }
                else 
                {
                    //Perform the transition
                    var ratio = _ticks / (float)TransitionTicks;
                    foreach(var shipInfo in _ships)
                        shipInfo.Update(1.0f - ratio);
                }
            }

            AButtonText = string.Empty;

            if (Game.InputMethodManager.InputMethod.SupportsMouse)
            {
                if (MouseOverPreviousButton)
                {
                    AButtonText = "Previous";
                    ActiveHelpText = CachedHelpText;
                }
                else if(MouseOverNextButton)
                {
                    AButtonText = "Next";
                    ActiveHelpText = CachedHelpText;
                }
                else
                    ActiveHelpText = string.Empty;
            }
            else
            { 
                AButtonText = "Select";
                HelpText = CachedHelpText;
            }
        }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
		{
            var tc = Tint;
            var tc1 = tc;

            if (_selectedIndex > -1)
            {
                var tint = Tint;
                tint *= Overlay.Opacity;
                tc1 = tint;
                tint *= FocusValue;
                
                tc = tint;
            }

            var location = Position + _leftButtonOffset;
            //Draw previous button
            var q = new Quad(location, ButtonSize, 0.0f);
            Game.Renderer.DrawSprite(ref q, _previousImageInfo.TextureId, _previousImageInfo.SourceRect, _selectedIndex > 0 ? tc : tc1 * InactiveAlphaModifier, RenderStyle);

            //Draw next button
            location.X = -location.X;
            q = new Quad(location, ButtonSize, 0.0f);
            Game.Renderer.DrawSprite(ref q, _nextImageInfo.TextureId, _nextImageInfo.SourceRect, _selectedIndex < _ships.Count - 1 ? tc : tc1 * InactiveAlphaModifier, RenderStyle);

		}
    }
}
