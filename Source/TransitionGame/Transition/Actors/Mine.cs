using Microsoft.Xna.Framework;
using Transition.Steering;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public class Mine : EnemyBullet
    {
        const float Acceleraton = 0.01f;
        const float Deceleraton = 0.03f;
        const float MaxSpeed = 0.6f;
        const float MinSpeed = 0.1f;

        private enum Phase
        {
            Spawn,
            Spawning,
            Normal,
        }

        private Phase _phase;

        private Vector2 _targetOffset;
	  
        float _speed;
        float _distanceSquared;

        /// <summary>
        /// Creates a new instance of a MineAlien.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public Mine(TransitionGame game) : base(game) 
        { 
            _horizontalConstrainBehaviour = ConstrainBehaviour.None;
            _verticalConstrainBehaviour = ConstrainBehaviour.None;
        }

        protected override void OnInitialise(string name)
        {
            base.OnInitialise(name);

            _phase = Phase.Spawn;

            _targetOffset = Vector2.Zero;

            _lifeTicks = 480;
            _speed = 0.0f;
            _distanceSquared = 0.0f;
        }

        /// <summary>
        /// Allows an object to update itself.
        /// </summary>
        protected override void OnUpdate(GameTime gameTime)
        {
            switch (_phase)
            {
                case Phase.Spawn:
                    SpawnSystem.DefaultSpawn(this);
                    _phase = Phase.Spawning;
                    break;

                case Phase.Spawning:
                    if (SpawnSystem.HandleSpawning(this)) 
					    _phase = Phase.Normal;
                    break;

                case Phase.Normal:
                    UpdateNormalPhase();
                    break;
            }

            _rotation += 0.05f;
        }
		
        private void UpdateNormalPhase()
        {
            var target = _game.ActivePlayer.Ship;

            if ((target.Position - Position).LengthSquared() > (35 * 35))
                _lifeTicks = 0;

            var futurePositionDelta = (target.Position + target.Velocity) - (Position + Velocity);

            var distanceSquared = futurePositionDelta.LengthSquared();
              
            var vn = Vector2.Normalize(Velocity);
            var tvn = Vector2.Normalize(target.Velocity);

            var dot = Vector2.Dot(vn, tvn);

            //Decelerate if moving away from target
            if(distanceSquared > _distanceSquared && dot < 0.0 && _speed > float.Epsilon)
                _speed -= Deceleraton;
            else
                _speed += Acceleraton;

            _speed = MathHelper.Clamp(_speed, MinSpeed, MaxSpeed);

            //Lerp the turning angle to limit the amount of turn the faster we go
            var turnAngle = MathHelper.Lerp(0.125f, 0.0125f, _speed / MaxSpeed);

            _targetOffset = _game.ActivePlayer.Ship.Velocity;
			var shipFound = HomingSystem.FacePlayerShip (_game, Position, ref _direction,  turnAngle, _targetOffset);

            //_rotation = _direction;

            //Move towards the players ship
            Velocity = VectorHelper.Polar(_speed, _direction);

            _distanceSquared = distanceSquared;
        }

        protected override void OnExpire() 
        {
            base.OnExpire();

            _game.SoundManager.PlaySound("MineExpired1");
        }
    }
}

