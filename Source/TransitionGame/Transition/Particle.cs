using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Transition.Actors;
using Transition.Rendering;

namespace Transition
{
    public struct TimeBand
    {
        public float StartTime;
        public float EndTime;
    }

    /// <summary>
    /// .
    /// </summary>
    public class Particle
    {
        private const int MaxBands = 5;

        private readonly TransitionGame _game;
        private bool _active;

        private Quad _quadStruct;

        /// <summary>Angle of direction in radians.</summary>
        private Vector2 _velocity;

        /// <summary>Angle of rotation (for rendering) in degrees.</summary>
        private float _rotation;

        private float _maxAge;
        private float _age;
        private float _speed;

        private Vector4 _colour;
        private Vector2 _size;
        //private float _wobbleX;
        //private float _wobbleY;
        private float _spin;

        private Vector4 _colourAdjustment;
        private float _sizeAdjustmentX;
        private float _sizeAdjustmentY;
        
        //private Vector2 _gravityAdjustment;
        //private float _wobbleAdjustmentX;
        //private float _wobbleAdjustmentY;
        //private float _spinAdjustment;

        private ParticleSystemTemplate _template;

        private int _colorTransitionId;
        private readonly List<TimeBand> _colorTransitions = new List<TimeBand>(MaxBands);

        private int _sizeTransitionId;
        private readonly List<TimeBand> _sizeTransitions = new List<TimeBand>(MaxBands);
        
        private Vector4 _tint;
        private Color _color;

        public Vector2 Position;

        /// <summary>The direction the particle is travelling in (radians).</summary>
        public float Direction;

        public Particle(TransitionGame game)
        {
            _game = game;
        }

        /// <summary>
        /// .
        /// </summary>
        public bool Active
        {
            get { return _active; }
        }

        /// <summary>
        /// Gets the id of the texture for this particle.
        /// </summary>
        public int TextureId
        {
            get { return _template.TextureId; }
        }

        /// <summary>
        /// .
        /// </summary>
        public void Reset(ParticleSystem particleSystem)
        {
            _rotation = 0.0f;
            _active = true;
            _age = 0.0f;
            _spin = 0.0f;
            //_wobbleX = 0.0f;
            //_wobbleY = 0.0f;

            _colorTransitionId = 0;
            _sizeTransitionId = 0;

            _template = particleSystem.Template;

            var colorBand = _template.ColorBands[_colorTransitionId];
            var sizeBand = _template.SizeBands[_sizeTransitionId];

            _colour = RandomHelper.Random.GetVector4(ref colorBand.ColourMin, ref colorBand.ColourMax);

            _size.X = RandomHelper.Random.GetFloat(sizeBand.SizeMinX, sizeBand.SizeMaxX);

            if (_template.SymetricalParticles)
                _size.Y = _size.X;
            else
                _size.Y = RandomHelper.Random.GetFloat(sizeBand.SizeMinY, sizeBand.SizeMaxY);

            _maxAge = RandomHelper.Random.GetFloat(_template.LifetimeMin, _template.LifetimeMax);
            _speed = RandomHelper.Random.GetFloat(_template.LinearSpeedMin, _template.LinearSpeedMax);

            _velocity = VectorHelper.Polar(_speed, Direction);

            if (_template.DirectionalParticles)
                _rotation = MathHelper.ToDegrees(Direction);
            else
                _rotation = 0.0f;

            //TODO: Transition - gravity will become 'forces'
            //var gravityStartX = 0.0f;
            //var gravityEndX = 0.0f;
            //GetGravityComponents(_template.GravityStartMinX, _template.GravityStartMaxX, _template.GravityEndMinX, _template.GravityEndMaxX, ref gravityStartX, ref gravityEndX);

            //var gravityStartY = 0.0f;
            //var gravityEndY = 0.0f;
            //GetGravityComponents(_template.GravityStartMinY, _template.GravityStartMaxY, _template.GravityEndMinY, _template.GravityEndMaxY, ref gravityStartY, ref gravityEndY);

            //_gravity = new Vector2(gravityStartX, gravityStartY);
            //_gravityAdjustment = new Vector2((gravityEndX - gravityStartX) /_maxAge, (gravityEndY - gravityStartY)/_maxAge);

            CreateTransitions(_template.ColorBands.Count, _colorTransitions); 
            CreateTransitions(_template.SizeBands.Count, _sizeTransitions); 

            //Calculate the adustments needed to from the start state to the next state
            SetColorAdjustment();
            SetSizeAdjustment();

            _tint = _template.InheritTint ? particleSystem.Tint.ToVector4() : Vector4.One;
        }

        private void CreateTransitions(int bandCount,  IList transitions)
        {
            //e.g. 5 bands defined in template is 4 transition steps
            var transitionCount = bandCount - 1;
            transitions.Clear();

            //Set time_band values for this particle
            //(will differ for each particle even from the same system as the life has a random element)
            var stepTime = _maxAge * (1.0f / transitionCount);
            var startTime = 0.0f;

            for (var i = 0; i < transitionCount; ++i)
            {
                var tb = new TimeBand();
                tb.StartTime = startTime;
                startTime += stepTime;
                tb.EndTime = startTime;

                transitions.Add(tb);
            }
        }

        /// <summary>
        /// .
        /// </summary>
        public void Kill()
        {
            _active = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeDelta"></param>
        /// <param name="force"></param>
        public void Update(float timeDelta, float force)
        {
            var tempParticleTime = _age + timeDelta;

            //Run it's course?
            if (tempParticleTime > _maxAge)
            {
                Kill();
                return;
            }

            if (_colorTransitions.Count > 0 && tempParticleTime > _colorTransitions[_colorTransitionId].EndTime)
            {
                _colorTransitionId++;
                SetColorAdjustment();
            }

            if (_sizeTransitions.Count > 0 && tempParticleTime > _sizeTransitions[_sizeTransitionId].EndTime)
            {
                _sizeTransitionId++;
                SetSizeAdjustment();
            }

            _age += timeDelta;

            //begin Apply adjustment
            _colour.X += _colourAdjustment.X*timeDelta;
            _colour.Y += _colourAdjustment.Y*timeDelta;
            _colour.Z += _colourAdjustment.Z*timeDelta;
            _colour.W += _colourAdjustment.W*timeDelta;

            var c = _colour*_tint;
            _color = new Color(c);

            _size.X += _sizeAdjustmentX*timeDelta;
            _size.Y += _sizeAdjustmentY*timeDelta;          
            //end apply adjustment 

            //1
            //Position.X += (_velocity.X * timeDelta) + _gravity.X;
            //Position.Y += (_velocity.Y * timeDelta) + _gravity.Y;
            //Position.X += force;

            //2
            //_velocity.X += _gravity.X;
            //_velocity.Y += _gravity.Y;

            Position.X += _velocity.X*timeDelta;
            Position.Y += _velocity.Y*timeDelta;
            Position.X += force;

            if (_template.Bounded)
            {
                if (_template.BoundsBottom && Position.Y < _game.LevelManager.Bounds.Bottom)
                {
                    Position.Y = _game.LevelManager.Bounds.Bottom;
                    _velocity.Y = -_velocity.Y;
                }
                if (_template.BoundsTop && Position.Y > _game.LevelManager.Bounds.Top)
                {
                    Position.Y = _game.LevelManager.Bounds.Top;
                    _velocity.Y = -_velocity.Y;
                }
                if (_template.BoundsLeft && Position.X < _game.LevelManager.Bounds.Left)
                {
                    Position.X = _game.LevelManager.Bounds.Left;
                    _velocity.X = -_velocity.X;
                }
                if (_template.BoundsRight && Position.X > _game.LevelManager.Bounds.Right)
                {
                    Position.X = _game.LevelManager.Bounds.Right;
                    _velocity.X = -_velocity.X;
                }
            }

            //DAMPING
            _velocity.X *= _template.LinearDamping;
            _velocity.Y *= _template.LinearDamping;

            _rotation += _spin * timeDelta;
            if (_rotation > 360.0f)
                _rotation -= 360.0f;

            else if (_rotation < 0.0f)
                _rotation += 360.0f;

            _quadStruct = new Quad(Position, _size, MathHelper.ToRadians(_rotation));
        }

        /// <summary>
        /// .
        /// </summary>
        private void SetColorAdjustment()
        {
            if (_template.ColorBands.Count == 1)
            {
                _colourAdjustment.X = 0;
                _colourAdjustment.Y = 0;
                _colourAdjustment.Z = 0;
                _colourAdjustment.W = 0;
                return;
            }

            var nextBand = _template.ColorBands[_colorTransitionId + 1];

            //Difference between new values and current values
            var timeAdjust = (_maxAge / _colorTransitions.Count); // number of time steps
            var colourAdjust = (RandomHelper.Random.GetVector4(nextBand.ColourMin, nextBand.ColourMax) - _colour);

            _colourAdjustment.X = colourAdjust.X / timeAdjust;
            _colourAdjustment.Y = colourAdjust.Y / timeAdjust;
            _colourAdjustment.Z = colourAdjust.Z / timeAdjust;
            _colourAdjustment.W = colourAdjust.W / timeAdjust;
        }

        /// <summary>
        /// .
        /// </summary>
        private void SetSizeAdjustment()
        {
            if (_template.SizeBands.Count == 1)
            {
                _sizeAdjustmentX = 0;
                _sizeAdjustmentY = 0;
                return;
            }

            var nextBand = _template.SizeBands[_sizeTransitionId + 1];

            //Difference between new values and current values
            var timeAdjust = (_maxAge / _sizeTransitions.Count); // number of time steps

            _sizeAdjustmentX = (RandomHelper.Random.GetFloat(nextBand.SizeMinX, nextBand.SizeMaxX) - _size.X) / timeAdjust;
            if (_template.SymetricalParticles)
                _sizeAdjustmentY = _sizeAdjustmentX;
            else
                _sizeAdjustmentY = (RandomHelper.Random.GetFloat(nextBand.SizeMinY, nextBand.SizeMaxY) - _size.Y) / timeAdjust;
        }

        //TODO: Transition - frustum culling
        public bool IsOnScreen { get { return true; } }

        public void Draw()
        {
            _game.Renderer.DrawSprite(ref _quadStruct, TextureId, _template.TextureInfo.SourceRect, _color, TransparentRenderStyle.Instance);
        }
    }
}