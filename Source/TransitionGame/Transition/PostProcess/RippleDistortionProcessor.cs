#if !WINDOWS_PHONE
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Transition.Rendering;

namespace Transition.PostProcess
{
    /// <summary>
    /// Renders a ripple distortion effect.
    /// </summary>
    public class RippleDistortionProcessor : BaseProcessor
    {
        //Effect in the base class will be used for RippleCombine.
        private Effect _rippleEffect;

        private RenderTarget2D _ripplesRenderTarget;

        private Renderer _renderer;

        /// <summary>
        /// Initialises a new instance of a <see cref="BarrelDistortionProcessor">BarrelDistortionProcessor</see>.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public RippleDistortionProcessor(BaseGame game) : base(game) { }

        /// <summary>
        /// Occurs when a derived effect needs to load content.
        /// </summary>
        protected override void OnLoadContent()
        {
            _renderer = new Renderer((TransitionGame)Game);

            _rippleEffect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/RippleDistortion"));
            Effect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/RippleCombine"));

            ResolutionChanged();
        }

        /// <summary>
        /// Occurs when a derived effect needs to load content.
        /// </summary>
        protected override void OnUnloadContent()
        {
            _rippleEffect.Dispose();
            Effect.Dispose();
            _ripplesRenderTarget.Dispose();
        }

        public override void ResolutionChanged()
        {
            //Look up the resolution and format of our main backbuffer.
            var pp = Game.GraphicsDevice.PresentationParameters;

            var width = pp.BackBufferWidth;
            var height = pp.BackBufferHeight;

            var format = pp.BackBufferFormat;

            //Create rendertarget for the processing. These are half the size of the backbuffer, in order to minimize fillrate costs. 
            //Reducing the resolution in this way doesn't hurt quality, because we are going to be blurring the bloom images in any case.
            //width /= 2;
            //height /= 2;

            _ripplesRenderTarget = new RenderTarget2D(Game.GraphicsDevice, width, height, false, format, DepthFormat.None);
        }

        protected override void OnPreRender() 
        {
            _renderer.Begin(Renderer.SortStrategy.None);
        }

        public void DrawSprite(ref Quad quad, int textureId, Rect textureCoords, Color tint, RenderStyle renderStyle, int layer = 0)
        {
            if (Enabled && Active)
                _renderer.DrawSprite(ref quad, textureId, textureCoords, tint, renderStyle, layer);
        }

        /// <summary>
        /// Draws the post processing effect.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="deltaTime">Time passed since the last frame.</param>
        /// <param name="texture">The texture to draw.</param>
        /// <param name="renderTarget"></param>
        protected override bool OnDraw(SpriteBatch spriteBatch, float deltaTime, Texture2D texture, RenderTarget2D renderTarget)
        {            
            //texture is the scene
            //renderTarget is where to draw it!
            if (_renderer.Count == 0)
            {
                _renderer.End();
                return false;
            }

            //First draw the ripples.
            Game.GraphicsDevice.SetRenderTarget(_ripplesRenderTarget);
            Game.GraphicsDevice.Clear(Color.Black);

            _renderer.End();

            Game.GraphicsDevice.SetRenderTarget(renderTarget);
            Game.GraphicsDevice.Textures[1] = _ripplesRenderTarget;

            return base.OnDraw(spriteBatch, deltaTime, texture, renderTarget);
        }
    }   
}
#endif