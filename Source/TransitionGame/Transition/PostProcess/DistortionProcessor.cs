using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.PostProcess
{
    /// <summary>
    /// Manages the 'old broken monitor' effects.
    /// </summary>
    public class DistortionsProcessor : BaseProcessor
    {
        public static int Margin = 25;

        private const float OffsetScale = 0.00125f;
        private const int VBlankInterval = 256;
        private const int VBlankHeight = 512;

        public static int RowNoiseResolution = 256;
			
        /** CRT distortion texture */
        private static Texture2D _crtDistortionTexture;
	
        /** Contains row noise offsets */
        private static float[] _rowNoiseArray;
	
        private static Vector2 _redOffset;
        private static Vector2 _greenOffset;
        private static Vector2 _blueOffset;
        private static float _vsyncOffset;
        private static int _vblankPos;
        
        //The XNA equivalemts to make it work!
        private RenderTarget2D _backBufferTarget;
        private RenderTarget2D _intermediaryRenderTarget;
        private RenderTarget2D _distortionRenderTarget;
       
        /** Individual distortions */
	    private static Texture2D _smallDistortionTexture;

        private static Texture2D _rowNoiseTexture;
        private Texture2D _scanlinesTexture;
        private Texture2D _vblankTexture;

	    private const int VertexPositionOffset = 0;
	    private const int VertexTexcoordOffset = 2 * sizeof(float);
	    private const int VertexStrengthOffset = 2 * sizeof(float) + 2 * sizeof(float);

        private Effect _distortionEffect;
        private Effect _resolveEffect;
        private Effect _finalEffect;
        
        Effect _currentEffect;
        BlendState _currentBlendState;
        RenderTarget2D _screenRenderTarget;

        /**
         final Attribute location of the distortion strength attribute in the final shader
         */
        //private static EffectParameter strengthLocation;

        /**
         * Attribute location of the distortion strength attribute in the intermediate resolve shader
         */
        private static EffectParameter _distStrengthLocation;

        /**
         * (vec2) are uniforms adding an offset to each color channel, and is used to simulate the colors channels separating. These
         * coordinates are added to the texture coordinates used to sample each color channel, so they should be in the range -1.0 to
         * 1.0, but usually just a very small value.
         */
        private static EffectParameter _crtRedLocation, _crtGreenLocation, _crtBlueLocation;
	
        /**
         * Controls where the V-sync seam appears. A value of 0.0 (or any integer) results in a normal view. A value of 0.5 switches the
         * top and bottom parts of the screen around. This value is not limited to 0.0 to 1.0 and can be any value possible.
         */
        private static EffectParameter _crtVsyncLocation;
	
        private static EffectParameter _vblankPosLocation;
        private static EffectParameter _vblankHeightLocation;

        /**
         * The constant deciding how many pixels a distortion of 1.0 distorts by. >_>' Since we're using a float texture for the
         * distortion map the value can actually go over 1.0, but the accuracy rapidly deteriorates, so setting a reasonable value here
         * is important. I'd say 500 is a definitive limit.
         */
        private const float DistortionPixels = 16.0f;

        /** Inited flag */
        private static bool _initialised;
	
        private Vector2 _vec2Temp;

        public readonly float[] CrtEffectValues = new float[5 * 4];

        [StructLayout(LayoutKind.Sequential)]
        public struct DistortionVertexType : IVertexType
        {
            public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration(new [] {
			    new VertexElement(VertexPositionOffset, VertexElementFormat.Vector2, VertexElementUsage.Position, 0),
			    new VertexElement(VertexTexcoordOffset, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
			    new VertexElement(VertexStrengthOffset, VertexElementFormat.Single, VertexElementUsage.Fog, 0)}
            );

            VertexDeclaration IVertexType.VertexDeclaration => VertexDeclaration;
        }

        public static bool IsSupported => true;

        /// <summary>
        /// Initialises a new instance of a <see cref="DistortionsProcessor">DistortionsProcessor</see>.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public DistortionsProcessor(BaseGame game) : base(game) { }

        /// <summary>
        /// Occurs when a derived effect needs to load content.
        /// </summary>
        protected override void OnLoadContent()
        {
            InitialiseAllEffects();
        }

        public override void ResolutionChanged()
        {
            if (!_initialised) 
                return;
          
            AllocateIntermediaryBuffer();
            CreateDistortionRenderTarget();
	
            _vec2Temp.X = DistortionPixels / Game.GraphicsDevice.Viewport.Width;
            _vec2Temp.Y = DistortionPixels / Game.GraphicsDevice.Viewport.Height;

            _distStrengthLocation.SetValue(_vec2Temp);

            _finalEffect.Parameters["distortionPixels"].SetValue(_vec2Temp);
        }

        private void AllocateIntermediaryBuffer() 
        {
            //Look up the resolution and format of our main backbuffer.
            var pp = Game.GraphicsDevice.PresentationParameters;

            var width = pp.BackBufferWidth;
            var height = pp.BackBufferHeight;

            var format = pp.BackBufferFormat;

            _intermediaryRenderTarget = new RenderTarget2D(Game.GraphicsDevice, width, height, false, format, DepthFormat.None);
        }

        private void InitialiseAllEffects()
        {
            if (!IsSupported || _initialised) 
                return;

            //Create some dynamic textures
            CreateDistortionRenderTarget();
            CreateRowNoiseTexture();
            CreateScanlinesTexture();
            CreateVBlankTexture();
            CreateSmallDistortionTexture();
            CreateCrtDistortionTexture();

            LoadShaders();
            
            _initialised = true;
	
            ResolutionChanged();
        }

        private void CreateDistortionRenderTarget()
        {
            _distortionRenderTarget = new RenderTarget2D(Game.GraphicsDevice, Game.Window.ClientBounds.Width, Game.Window.ClientBounds.Height, false, SurfaceFormat.HalfVector2, DepthFormat.None);
        }

        private void CreateRowNoiseTexture()
        {
            _rowNoiseTexture = new Texture2D(Game.GraphicsDevice, RowNoiseResolution, 1, false, SurfaceFormat.Single);

            _rowNoiseArray = new float[RowNoiseResolution];

            for (var i = 0; i < RowNoiseResolution; i++) 
                _rowNoiseArray[i] = 0.5f;

            _rowNoiseTexture.SetData(_rowNoiseArray);
        }

        private void CreateScanlinesTexture()
        {
            var data = new [] {(byte) 244, (byte) 255, (byte) 224, (byte) 214};
 
            _scanlinesTexture = new Texture2D(Game.GraphicsDevice, 4, 1, false, SurfaceFormat.Alpha8);
            _scanlinesTexture.SetData(data);
        }

        private void CreateVBlankTexture()
        {
            const int size = 256;
            const int band = 32;

            var data = new List<float>(size);
            for (var i = 0; i < size - (band * 2); i++) 
                data.Add(1.0f);
            
            for (var i = 0; i < band; i++) 
                data.Add(CubicBezierInterpolator.EaseInOut.Interpolate(1.0f, 0.875f, i / (float) band));
            
            for (var i = 0; i < band; i++) 
                data.Add(CubicBezierInterpolator.EaseInOut.Interpolate(0.875f, 1.0f, i / (float) band));
            
            _vblankTexture = new Texture2D(Game.GraphicsDevice, size, 1, false, SurfaceFormat.Single);
            _vblankTexture.SetData(data.ToArray());
        }

        private void CreateSmallDistortionTexture()
        {
            const int distortionTexSize = 512;
		    const int halfDistortionTexSize = distortionTexSize / 2;

            var data = new List<byte>(2 * distortionTexSize * distortionTexSize);

            for (var y = 0; y < distortionTexSize; y++) 
            {
                for (var x = 0; x < distortionTexSize; x++) 
                {
                    var dx = halfDistortionTexSize - x;
                    var dy = halfDistortionTexSize - y;

                    var distance = Math.Sqrt(dx * dx + dy * dy);
                    var intensity = DistortionFunction(distance / halfDistortionTexSize);
	
                    var distortionX = (int) Math.Round(127 + 127 * dx / distance * intensity);
                    var distortionY = (int) Math.Round(127 + 127 * dy / distance * intensity);

                    data.Add((byte)distortionX);
                    data.Add((byte)distortionY);
                }
            }
	
            _smallDistortionTexture = new Texture2D(Game.GraphicsDevice, distortionTexSize, distortionTexSize, false, SurfaceFormat.NormalizedByte2);
		    _smallDistortionTexture.SetData(data.ToArray());
        }

        private static double DistortionFunction(double x) 
        {
			return Math.Max((x * x * x - x * x * x * x * x) * 5, 0);
		}

        private void LoadShaders()
        {
            _distortionEffect = Effect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/Distortion"));

            LoadResolveShader();
            LoadFinalResolveShader();
        }

        private void LoadResolveShader()
        {
            _resolveEffect = Effect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/Resolve"));

            _distStrengthLocation = _resolveEffect.Parameters["distStrength"];
            _crtRedLocation = _resolveEffect.Parameters["crtRed"];
            _crtGreenLocation = _resolveEffect.Parameters["crtGreen"];
            _crtBlueLocation = _resolveEffect.Parameters["crtBlue"];
            _crtVsyncLocation = _resolveEffect.Parameters["crtVSync"];
            _vblankPosLocation = _resolveEffect.Parameters["vblankPos"];
            _vblankHeightLocation = _resolveEffect.Parameters["vblankHeight"];
        }

        private void LoadFinalResolveShader()
        {
            _finalEffect = Effect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/Final"));
        }

        private void CreateCrtDistortionTexture() 
        {
            var width = Game.Window.ClientBounds.Width;
            var height = Game.Window.ClientBounds.Height;
            int halfWidth = width / 2, halfHeight = height / 2;
            var diagonal = Math.Sqrt(halfWidth * halfWidth + halfHeight * halfHeight);
	
            var data = new List<short>(2 * width * height);
            for (var y = 0; y < height; y++) 
            {
                for (var x = 0; x < width; x++) 
                {
                    var dx = halfWidth - x + 0.5;
                    var dy = halfHeight - y + 0.5;
	
                    var distance = Math.Sqrt(dx * dx + dy * dy) / diagonal;
                    distance = 1 - distance * distance * distance;
	
                    var distX = distance * dx / diagonal;
                    var distY = distance * dy / diagonal;
	
                    var distortionX = (int)Math.Round(short.MaxValue + short.MaxValue * distX);
                    var distortionY = (int)Math.Round(short.MaxValue + short.MaxValue * distY);

                    data.Add((short)distortionX);
                    data.Add((short)distortionY);
                }
            }
	            
            _crtDistortionTexture = new Texture2D(Game.GraphicsDevice, width, height, false, SurfaceFormat.Rg32);	
            _crtDistortionTexture.SetData(data.ToArray());
        }

        public static void SetRedOffset(float x, float y) 
        {
            _redOffset = new Vector2(x * OffsetScale, y * OffsetScale);
        }
	
        public static void SetGreenOffset(float x, float y) 
        {
            _greenOffset = new Vector2(x * OffsetScale, y * OffsetScale);
        }
	
        public static void SetBlueOffset(float x, float y) 
        {
            _blueOffset = new Vector2(x * OffsetScale, y * OffsetScale);
        }
	
        public static void SetVsyncOffset(float offset) 
        {
            _vsyncOffset = offset;
        }
	
        public static void SetRowNoise(float[] noise) 
        {
            if (!_initialised) 
                return;
            
            Debug.Assert(noise.Length == RowNoiseResolution, "Row noise length is " + noise.Length + ", must be " + RowNoiseResolution);
	
            for (var i = 0; i < noise.Length; i++)
                _rowNoiseArray[i] = (noise[i] * OffsetScale + 0.5f);
           
            TransitionGame.Instance.GraphicsDevice.Textures[2] = null;
            _rowNoiseTexture.SetData(_rowNoiseArray);
        }
        /// <summary>
        /// Occurs when a derived effect needs to load content.
        /// </summary>
        protected override void OnUnloadContent()
        {
            _intermediaryRenderTarget.Dispose();

            _distortionEffect.Dispose();
            _resolveEffect.Dispose();
            _finalEffect.Dispose();

            _distortionRenderTarget.Dispose();
            _rowNoiseTexture.Dispose();
            _scanlinesTexture.Dispose();
            _vblankTexture.Dispose();
        }

        /// <summary>
        /// Draws the post processing effect.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="deltaTime">Time passed since the last frame.</param>
        /// <param name="texture">The texture to draw.</param>
        /// <param name="renderTarget"></param>
        protected override bool OnDraw(SpriteBatch spriteBatch, float deltaTime, Texture2D texture, RenderTarget2D renderTarget)
        {
            //Scene is rendered to the texture - cache it
            _backBufferTarget = (RenderTarget2D)texture;
            _screenRenderTarget = renderTarget;
            
            var viewport = new Vector2(Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height);
            _distortionEffect.Parameters["Viewport"].SetValue(viewport);
            _resolveEffect.Parameters["Viewport"].SetValue(viewport);

            //Draw all the effects, distortions, row noise, scanlines, vblank
            DistortBackBuffer(spriteBatch);

            //Now using BarrelDistortionProcessor
            //DrawCRTDistortionEffect();
           
            DistortionEffects.Tick();

            DistortIntermediaryBuffer(spriteBatch);

            return true;
        }

        public void DistortBackBuffer(SpriteBatch spriteBatch) 
        {
            Game.GraphicsDevice.SetRenderTarget(_intermediaryRenderTarget);
            Game.GraphicsDevice.Clear(Color.Black);
            _currentBlendState = BlendState.AlphaBlend;
            Game.GraphicsDevice.Textures[0] = _backBufferTarget;
            Game.GraphicsDevice.Textures[1] = _distortionRenderTarget;
            Game.GraphicsDevice.Textures[2] = _rowNoiseTexture;
            Game.GraphicsDevice.Textures[3] = _scanlinesTexture;
            Game.GraphicsDevice.Textures[4] = _vblankTexture;
            Game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            Game.GraphicsDevice.SamplerStates[1] = SamplerState.PointClamp;
            Game.GraphicsDevice.SamplerStates[2] = SamplerState.PointClamp;
            Game.GraphicsDevice.SamplerStates[3] = SamplerState.LinearWrap;
            Game.GraphicsDevice.SamplerStates[4] = SamplerState.PointWrap;
            _currentEffect = _resolveEffect;
            _crtRedLocation.SetValue(_redOffset);
            _crtGreenLocation.SetValue(_greenOffset);
            _crtBlueLocation.SetValue(_blueOffset);
            _crtVsyncLocation.SetValue(_vsyncOffset);

            _vec2Temp.X = DistortionPixels / Game.Window.ClientBounds.Width;
            _vec2Temp.Y = DistortionPixels / Game.Window.ClientBounds.Height;
            _distStrengthLocation.SetValue(_vec2Temp);

            _vblankPos++;
            if (_vblankPos > VBlankInterval) {
                _vblankPos = 0;
            }
	        _vblankPosLocation.SetValue(LinearInterpolator.Instance.Interpolate(0.0f, VBlankHeight, _vblankPos / (float)VBlankInterval));
            _vblankHeightLocation.SetValue(VBlankHeight);
            
            DrawFullscreenQuad(spriteBatch);
        }

        ///// <summary>
        ///// Draws the CRT distortion effect: clears the distortion FBO and draws the CRT distortion map onto it.
        ///// </summary>
        //public void DrawCRTDistortionEffect() 
        //{
        //    if (!_initialised) 
        //        return;

        //    Game.GraphicsDevice.SetRenderTarget(this._distortionRenderTarget); 
        //    GL.CurrentEffect = _distortionEffect;
        //    GL.Disable(GL.GL_BLEND);
            
        //    Game.GraphicsDevice.Textures[0] = _crtDistortionTexture;
        //    Game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
            
        //    //To draw a distortion: 1. Setup distortion vertex data in VBOs and IBOs 2. Setup these vertex attribute settings:
		    
        //    _crtVbo.render();
        //    var vbo = _crtVbo.map();

        //    int idx = 0;
        //    vbo[idx++] = 0.0f;
        //    vbo[idx++] = 0.0f;
        //    vbo[idx++] = 0.0f;
        //    vbo[idx++] = 0.0f;
        //    vbo[idx++] = CrtStrength;
		    
        //    vbo[idx++] = Invaders.getPhysicalWidth();
        //    vbo[idx++] = 0.0f;
        //    vbo[idx++] = 1.0f;
        //    vbo[idx++] = 0.0f;
        //    vbo[idx++] = CrtStrength;
		    
        //    vbo[idx++] = Invaders.getPhysicalWidth();
        //    vbo[idx++] = Invaders.getPhysicalHeight();
        //    vbo[idx++] = 1.0f;
        //    vbo[idx++] = 1.0f;
        //    vbo[idx++] = CrtStrength;
		    
        //    vbo[idx++] = 0.0f;
        //    vbo[idx++] = Invaders.getPhysicalHeight();
        //    vbo[idx++] = 0.0f;
        //    vbo[idx++] = 1.0f;
        //    vbo[idx++] = CrtStrength;
		    

        //    _crtIbo.render();
        //    var ibo = _crtIbo.map();
        //    for (int i = 0; i < GL.FixedTriangleIndices.Length; i++) {
        //        ibo[i] = GL.FixedTriangleIndices[i];
        //    }

        //    _crtVbo.unmap();
        //    _crtIbo.unmap();

        //    GL.SetBufferARB(vbo);
        //    GL.SetBufferARB(ibo);

        //    //glEnableClientState(GL_VERTEX_ARRAY);
        //    //glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        //    //glEnableVertexAttribArray(strengthLocation);
        //    //glVertexPointer(2, GL_FLOAT, VERTEX_BYTE_SIZE, VERTEX_POSITION_OFFSET);
        //    //glTexCoordPointer(2, GL_FLOAT, VERTEX_BYTE_SIZE, VERTEX_TEXCOORD_OFFSET);
        //    //glVertexAttribPointer(strengthLocation, 1, GL_FLOAT, false, VERTEX_BYTE_SIZE, VERTEX_STRENGTH_OFFSET);
        //    //glBindTexture(GL_TEXTURE_2D, crtDistortion);
        //    //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0L);

        //    GL.DrawRangeElements(GL.GL_TRIANGLES, 6, GL.GL_UNSIGNED_SHORT, 0);

        //    //// Cleanup
        //    //glDisableClientState(GL_VERTEX_ARRAY);
        //    //glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        //    //glDisableVertexAttribArray(strengthLocation);
        //    //glDisable(GL_BLEND);
        //    GLShaderProgram.useFixed();
        //}

        private void DistortIntermediaryBuffer(SpriteBatch spriteBatch)
        {
            Game.GraphicsDevice.SetRenderTarget(_screenRenderTarget);
            Game.GraphicsDevice.Clear(Color.Black);
            _currentBlendState = BlendState.Opaque;
            Game.GraphicsDevice.Textures[0] = _intermediaryRenderTarget;
            Game.GraphicsDevice.Textures[1] = _distortionRenderTarget;
            Game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
            Game.GraphicsDevice.SamplerStates[1] = SamplerState.PointClamp;
            _currentEffect = _finalEffect;          
            DrawFullscreenQuad(spriteBatch);
        }

                /// <summary>
        /// Helper for drawing a texture into a rendertarget, using
        /// a custom shader to apply postprocessing effects.
        /// </summary>
        /// <param name="spriteBatch"></param>
        void DrawFullscreenQuad(SpriteBatch spriteBatch)
        {
            DrawFullscreenQuad(spriteBatch, Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height);
        }

        /// <summary>
        /// Helper for drawing a texture into the current rendertarget,
        /// using a custom shader to apply postprocessing effects.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="width">The width of the destination rectangle.</param>
        /// <param name="height">The height of the destination rectangle.</param>
        void DrawFullscreenQuad(SpriteBatch spriteBatch, int width, int height)
        {
            spriteBatch.Begin(0, _currentBlendState, Game.GraphicsDevice.SamplerStates[0], null, null, _currentEffect);
            spriteBatch.Draw((Texture2D)Game.GraphicsDevice.Textures[0], new Rectangle(0, 0, width, height), Color.White);
            spriteBatch.End();
        }
    }
}
