﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.Xna.Framework.Audio;

namespace Transition._____Temp.StarGuard 
{
    [Serializable]
    public class DynamicAudio
    {
        //Stack<DynamicSoundEffectInstance> _usedSources = new Stack<DynamicSoundEffectInstance>();
        readonly Dictionary<string, DynamicAudioInfo> _availableSources = new Dictionary<string, DynamicAudioInfo>();

        /// <summary>
        /// LoadContent will be called once per game and is the place to load all of your content.
        /// </summary>
        public void LoadContent()
        {
            var folder = Path.Combine(Platform.ContentPath, "DynamicSounds");

            foreach (var filename in Directory.GetFiles(folder, "*.wav"))
                LoadSound(filename);
        }

        void LoadSound(string filename)
        {
            Debug.WriteLine($"Loading dynamic sound file: {filename}");

            // Create a new SpriteBatch, which can be used to draw textures.Open);
            Stream waveFileStream = File.Open(filename, FileMode.Open);

            var reader = new BinaryReader(waveFileStream);

            var chunkId = reader.ReadInt32();
            var fileSize = reader.ReadInt32();
            var riffType = reader.ReadInt32();
            var fmtId = reader.ReadInt32();
            var fmtSize = reader.ReadInt32();
            int fmtCode = reader.ReadInt16();
            int channels = reader.ReadInt16();
            var sampleRate = reader.ReadInt32();
            var fmtAvgBps = reader.ReadInt32();
            int fmtBlockAlign = reader.ReadInt16();
            int bitDepth = reader.ReadInt16();

            if (fmtSize == 18)
            {
                // Read any extra values
                int fmtExtraSize = reader.ReadInt16();
                reader.ReadBytes(fmtExtraSize);

                Trace.WriteLine("has extra values");
            }

            var dataId = reader.ReadInt32();
            var dataSize = reader.ReadInt32();

            Debug.WriteLine($"chunkId: {chunkId}");
            Debug.WriteLine($"fileSize: {fileSize}");
            Debug.WriteLine($"riffType: {riffType}");
            Debug.WriteLine($"fmtId: {fmtId}");
            Debug.WriteLine($"fmtSize: {fmtSize}");
            Debug.WriteLine($"fmtCode: {fmtCode}");
            Debug.WriteLine($"channels: {channels}");
            Debug.WriteLine($"sampleRate: {sampleRate}");
            Debug.WriteLine($"fmtAvgBps: {fmtAvgBps}");
            Debug.WriteLine($"fmtBlockAlign: {fmtBlockAlign}");
            Debug.WriteLine($"bitDepth: {bitDepth}");
            Debug.WriteLine($"dataId: {dataId}");
            Debug.WriteLine($"dataSize: {dataSize}");
            
            var instance = new DynamicSoundEffectInstance(sampleRate, (AudioChannels) channels);
            var info = new DynamicAudioInfo
            {
                SoundData = reader.ReadBytes(dataSize),
                SampleSizeInBytes = instance.GetSampleSizeInBytes(TimeSpan.FromMilliseconds(200))
            };
            
            var fileInfo = new FileInfo(filename);
            _availableSources.Add(Path.GetFileNameWithoutExtension(fileInfo.Name), info);

            //var durationInSeconds = byteArray.Length / (float)fmtAvgBps;
        }

        public DynamicAudioInfo GetAudioInfo(string key)
        {
            return _availableSources[key];
        }
    }
}