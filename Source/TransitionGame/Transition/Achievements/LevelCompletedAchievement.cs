namespace Transition.Achievements
{
    /// <summary>
    /// Abstract base class for achievements
    /// </summary>
    public class LevelCompletedAchievement : Achievement
    {
        public LevelCompletedAchievement(string guid, int targetLevel) : base(guid, $"Level {targetLevel} Completed")
        {

        }

        protected override bool OnIsGainedThisSession()
        {
            return true;
        }
    }
}

