namespace Transition.MenuControls
{
    /// <summary>Represents a control that displays hints for controller actions.</summary>
    public class ButtonHintControl : ActionHintControl
    {		
		/// <summary>Initialises a new instance of a ButtonHintControl control.</summary>
		/// <param name="game">A reference to the game.</param>
        public ButtonHintControl(BaseGame game) : base(game) { }
    }
}
