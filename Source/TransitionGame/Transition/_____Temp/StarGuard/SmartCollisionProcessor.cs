///*
// * Copyright (c) 2003 Shaven Puppy Ltd
// * All rights reserved.
// *
// * Redistribution and use in source and binary forms, with or without
// * modification, are permitted provided that the following conditions are
// * met:
// *
// * * Redistributions of source code must retain the above copyright
// *   notice, this list of conditions and the following disclaimer.
// *
// * * Redistributions in binary form must reproduce the above copyright
// *   notice, this list of conditions and the following disclaimer in the
// *   documentation and/or other materials provided with the distribution.
// *
// * * Neither the name of 'Shaven Puppy' nor the names of its contributors
// *   may be used to endorse or promote products derived from this software
// *   without specific prior written permission.
// *
// * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// */

//using System.Collections.Generic;
//using System.Reflection;
//using Feature = com.shavenpuppy.jglib.resources.Feature;
//using PumpkinGames.DotNetExtras;
//using PerformanceMeasuring.GameDebugTools;
//using System;

//namespace invaders {
	
//    [Serializable]
//    public partial class SmartCollisionFeature : Feature {
	
//        [Final] private static readonly long serialVersionUID = 1L;
	
//        //Feature Data.
//        private string target;
//        private string collisionMethod;
//        private string collisionMethodsPrefix;

//        /// <summary>Contains a mapping of a type to a list - add entites who can collide this tick (are active, visible, etc).</summary>
//        /// <remarks>This map would contain a list for each type of entity that has the potential to coliide.</remarks>
//        private readonly Dictionary<Type, List<Entity>> _potentialsMap = new Dictionary<Type, List<Entity>>();

//        /// <summary>This map is used to put an entity into the correct potentials list at runtime</summary>
//        /// <remarks>e.g. I am a MoneyPowerup but I go in the Powerup potential list.</remarks>
//        private readonly Dictionary<Type, Type> _entityTypeToPotentialsType = new Dictionary<Type, Type>();
        
//        /// <summary>A mapping of type to a list of types that the type collides with.</summary>
//        private readonly Dictionary<Type, List<Type>> _typeToListOfCollidableTypes = new Dictionary<Type, List<Type>>();

//        public readonly Dictionary<C5.KeyValuePair<Entity,Entity>, bool> RuntimeCollisionTest = new Dictionary<C5.KeyValuePair<Entity,Entity>, bool>();
		
//        /// <summary>The number of collision tests pre frame.</summary>
//        public int CollisionTestCount;

//        /// <summary>The number of potential colliders per frame.</summary>
//        public int PotentialColiders;

//        /// <summary>The number of actual colliders per frame.</summary>
//        public int ActualColiders;

//        public SmartCollisionFeature(String name) : base(name) {
//            setAutoCreated();
//        }

//        protected override void doCreate() {
//            base.doCreate();

//            var assembly = Assembly.GetExecutingAssembly();
//            var targetType = assembly.GetType(target);

//            foreach(var type in assembly.GetTypes()) {
//                if (type == targetType || !targetType.IsAssignableFrom(type)) 
//                    continue;

//                var typeDerivedFromEntity = type;

//                //We have an something that is derived from entity.
//                var typesICanCollideWith = new List<Type>();

//                //Check if the collision method is defined and see if this entity has overridden a collision method 
//                foreach(var method in typeDerivedFromEntity.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly)) {
//                    if (method.Name != collisionMethod)
//                        continue;
                    
//                    var parameters = method.GetParameters();
//                    if (parameters.Length > 1)
//                        throw new InvalidOperationException("Expecting a method with one parameter of type Entity");

//                    //Ok we've found the collision method - this type is a potential collider
//                    var typeDefiningOnCollision = typeDerivedFromEntity;

//                    var potentialsList = new List<Entity>();
//                    this._potentialsMap.Add(typeDefiningOnCollision, potentialsList);
//                    this._entityTypeToPotentialsType.Add(typeDefiningOnCollision, typeDefiningOnCollision);

//                    //NOTE! this type may be abstract and it's actually derived types that will be spawned. So we need to find all of the sub classes of
//                    //this type and map them to the same type!
//                    //e.g. each entity of type:Bullet will be added to this list at run time if it is a potential collider (it's active, etc)
//                    foreach(var subClass in assembly.GetTypes()) {
//                        //Ignore outself and any types that are not sub classes of us.
//                        if (typeDefiningOnCollision == subClass || subClass.IsSubclassOf(typeDefiningOnCollision) == false)
//                            continue;

//                        //We have created a mapping from the sub type to the type defining the 'onCollision' methods.
//                        this._entityTypeToPotentialsType.Add(subClass, typeDefiningOnCollision);
//                    }

//                    //It may also have a base type that gets spawned so do those too.
//                    var baseType = typeDefiningOnCollision.BaseType;
//                    while (baseType != null) {

//                        if (baseType.IsSubclassOf(targetType))
//                            this._entityTypeToPotentialsType.Add(baseType, typeDefiningOnCollision);

//                        baseType = baseType.BaseType;
//                    }
                    
//                    //Now we have the class heirarchy for 'type' - we'll navigate this to search any level for the overridden methods
//                    //onCollisionWithXXX
//                    //so MoneyPowerup->PowerupInstance->Powerup any one of those classes may override the methods.
//                    foreach(var typeMapping in this._entityTypeToPotentialsType) {
//                        //We are only interested in the types in the heirarchy for the current type - there's maybe lot's of heirarchies!
//                        if (typeMapping.Value != typeDefiningOnCollision)
//                            continue;

//                        //Get the methods of the mapped type (e.g. MoneyPowerup may be the mapped type and PowerupInstance the thing that we have mapped to)
//                        var mappedType =  typeMapping.Key;
//                        foreach(var method2 in mappedType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly)) {
//                            if (method2.Name.StartsWith(collisionMethodsPrefix) == false)
//                                continue;

//                            var collidesWithType = method2.GetParameters()[0].ParameterType;

//                            //There are some onCollisionWith methods that take things other than entity - we are not interested inthose here.
//                            if (collidesWithType.IsSubclassOf(targetType) == false)
//                                continue; 

//                            if (typesICanCollideWith.Contains(collidesWithType) == false)
//                                typesICanCollideWith.Add(collidesWithType);

//                        }

//                        //this type is defined to collide with the types in typesCollideWith.
//                        //e.g. type:Bullet, List{ type:Gidrah, type:Saucer, type:Parachute }
//                        this._typeToListOfCollidableTypes[typeDefiningOnCollision] = typesICanCollideWith;
//                    }
//                }
//            }
//        }
        
//        /// <summary>
//        /// Registers an entity with the manager.
//        /// </summary>
//        /// <param name="entity">An Entity that can potentially colide with other entity.</param>
//        public void registerEntity(Entity entity)
//        {
//            //Add this entity to the correct potential colliders list.
//            this._potentialsMap[entity.GetType()].Add(entity);
//        }

//        /// <summary>
//        /// Tick the collision manager.
//        /// </summary>
//        public void tickEverything(List<Entity> entities)
//        {
//            DebugSystem.Instance.Properties.Entities = entities.size();

//            RuntimeCollisionTest.Clear();

//            this.CollisionTestCount = 0;
//            this.ActualColiders = 0;
//            this.PotentialColiders = 0;

//            this.registerPotentialColliders(entities);
            
//            this.testForCollisions(entities);

//            DebugSystem.Instance.Properties.CollisionTests = CollisionTestCount;
//            DebugSystem.Instance.Properties.ActualColiders = ActualColiders;
//            DebugSystem.Instance.Properties.PotentialColiders = PotentialColiders;

//            this.clearPotentialColliders();
//        }

//        /// <summary>
//        /// Add all of the entities into the correct lists
//        /// </summary>
//        /// <param name="entities"></param>
//        private void registerPotentialColliders(List<Entity> entities) {

//            // Then process all the collisions (starting at 1, not 0...)
//            for (int i = 0; i < entities.size();) {
//                var entity = entities.get(i);
//                if (!entity.isActive()) {
//                    // The entity is dead, so we'll cull it now
//                    entities.remove(i);
//                } else {
//                    this._potentialsMap[this._entityTypeToPotentialsType[entity.GetType()]].Add(entity);
//                    i++;
//                    this.PotentialColiders++;
//                }
//            }
//        }

//        /// <summary>
//        /// Tests for collisons between the potential colliders.
//        /// </summary>
//        private void testForCollisions(List<Entity> entities)
//        {
//            for(int i = 0; i < entities.Count;) {
//                var src = entities[i];
//                //src is an instance of an entity
//                //e.g. bullet#23, gidrah#47, gidrah#54
//                if (src.isActive() == false) {
//                    // The entity is dead, so we'll cull it now
//                    entities.remove(i);
//                } else {
                   
//                    //Get the lists of potential collider types that the src entity can collide with
//                    //e.g. If I am a Bullet I may collide with Gidrah, Saucer, Parachute
//                    var typesICollideWith = this._typeToListOfCollidableTypes[this._entityTypeToPotentialsType[src.GetType()]];

//                    if (typesICollideWith.Count > 0) {
//                        //Loop through all the types I collide with and get the actual Lists that contain the entity instances.
//                        foreach(var typeICollideWith in typesICollideWith) {
//                            //Get the list of potential colliders for the particular type I collide with
//                            //e.g. dest will be instances of things that the src entity will need to check for collision against. 
//                            var destList = this._potentialsMap[this._entityTypeToPotentialsType[typeICollideWith]];
//                            for (int j = 0; j < destList.Count && src.isActive();) {
//                                var dest = destList[j];

//                                if (dest.isActive() == false) {
//                                    destList.Remove(dest);
//                                } else {
//                                    var pair = new C5.KeyValuePair<Entity,Entity>();
//                                    pair.Key = src;
//                                    pair.Value = dest;

//                                    if (RuntimeCollisionTest.ContainsKey(pair)) {
//                                        j++;
//                                        continue;
//                                    }

//                                    var pair2 = new C5.KeyValuePair<Entity,Entity>();
//                                    pair2.Key = dest;
//                                    pair2.Value = src;

//                                    if (RuntimeCollisionTest.ContainsKey(pair2)) {
//                                        j++;
//                                        continue;
//                                    }

//                                    RuntimeCollisionTest.Add(pair, true);
//                                    RuntimeCollisionTest.Add(pair2, true);

//                                    this.CollisionTestCount++;
//                                    if (src.canCollide() && dest.canCollide() && src.isTouching(dest)) {
//                                        //Inform both entities of the collision, in no particular order
//                                        src.onCollision(dest);
//                                        dest.onCollision(src);
//                                        if (!src.isActive()) {
//                                            entities.remove(i--);
//                                            break;
//                                        }
//                                        this.ActualColiders++;
//                                    }

//                                    j++;
//                                }
//                            }

//                            if (src.isActive() == false)
//                                break;
//                        }
//                    }

//                    i++;
//                }
//            }


//            ////Test for updates for each potential collider. Colliders are stored in lists along with actors of the same types.
//            ////This optimisation will allow us to skip checking gidrah / gidrah or bullet / bullet tests.

//            ////Loop through each list (bullet list, gidrah list, powerup list, etc).
//            //foreach (var potential in this._potentialsMap) {
           
//            //    //Test to see if the potential colliders list has any actors (there may not be any powerups for example)
//            //    if (potential.Value.Count == 0)
//            //        continue;

//            //    //Get a list of the lists that these potential colliders collide with.
//            //    var typesICollideWith = this._typeToTypesMap[potential.Key];

//            //    //The actors in the potential may not be flagged to collide with other types
//            //    //e.g. we may only tag Ships colliding with Gidrahs not the other way around - stops doubling the number of tests we need to perform).
//            //    if (typesICollideWith == null)
//            //        continue;

//            //    //There are some lists of potential actors that the current type collides with.  Loop through each list in turn.
//            //    foreach(var type in typesICollideWith) {
                
//            //        //Test each Entity in the primary list (potentialList) against each of the the actors in the current collideList
//            //        foreach (var src in potential.Value) {
                    
//            //            foreach (var dest in  this._potentialsMap[type]) {
                        
//            //                this.CollisionTestCount++;
//            //                if (src.canCollide() && dest.canCollide() && src.isTouching(dest)) {
//            //                    //Inform both entities of the collision, in no particular order
//            //                    src.onCollision(dest);
//            //                    dest.onCollision(src);
//            //                    if (!src.isActive()) {
//            //                        //entities.remove(i--);
//            //                        break;
//            //                    }
//            //                    this.ActualColiders++;
//            //                }
//            //            }
//            //        }
//            //    }
//            //}
//        }

//        /// <summary>
//        /// Clears the lists of potential colliders.
//        /// </summary>
//        private void clearPotentialColliders()
//        {
//            foreach (var list in this._potentialsMap.Values)
//                list.Clear();
//        }

//        ///// <summary>
//        ///// Processes the actual colliders.
//        ///// </summary>
//        //public void ProcessCollisions()
//        //{
//        //    //Sort collisions based on time.
//        //    this._actualColliders.Sort(delegate(CollisionPacket cp1, CollisionPacket cp2)
//        //    {
//        //        return Comparer<float>.Default.Compare(cp1.Time, cp2.Time);
//        //    });

//        //    //Process each collison
//        //    foreach (CollisionPacket packet in this._actualColliders)
//        //        packet.Actor1.Collide(packet.Actor2, packet.Time);

//        //    this._actualColliders.Clear();
//        //}
//    }
//}

