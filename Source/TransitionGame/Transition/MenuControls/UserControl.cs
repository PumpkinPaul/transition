using System.Xml.Linq;

namespace Transition.MenuControls
{
    /// <summary>
    /// 
    /// </summary>
    public class UserControl : MenuControl
    {		
        /// <summary>Where the text is rendered in relation to the control's position.</summary>
        public string Source;

        public UserControl(BaseGame game) : base(game) { }

        public override void Load(XElement element)
        {
            base.Load(element);

            Source = element.GetAttributeString("source", Source);
        }
    }
}
