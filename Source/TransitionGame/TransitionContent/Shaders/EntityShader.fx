texture baseTexture;
float4x4 worldViewProjection;

sampler baseSampler = sampler_state
{
    Texture = < baseTexture >;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    ADDRESSU = CLAMP;
    ADDRESSV = CLAMP;
};

struct VS_INPUT
{
	#if SM4
		float4 ObjectPos: SV_POSITION;
	#else
		float4 ObjectPos: POSITION;
	#endif
    float2 TextureCoords: TEXCOORD0;
    float4 Colour: COLOR;
};

struct VS_OUTPUT 
{
	#if SM4
		float4 ScreenPos:   SV_POSITION;
	#else
		float4 ScreenPos:   POSITION;
	#endif
	float2 TextureCoords: TEXCOORD0;
	float4 Colour: COLOR;
};

struct PS_OUTPUT 
{
	float4 Color:   COLOR;
};

VS_OUTPUT VertexShaderFunction(VS_INPUT In)
{
   VS_OUTPUT Out;

    //Move to screen space
    Out.ScreenPos = mul(In.ObjectPos, worldViewProjection);
    Out.TextureCoords = In.TextureCoords;
    Out.Colour = In.Colour;
    
    return Out;
}

PS_OUTPUT PixelShaderFunction(VS_OUTPUT In)
{
    PS_OUTPUT Out;
    float2 v = In.TextureCoords;
    
    Out.Color = tex2D(baseSampler,v);
                
    Out.Color.r = mul(In.Colour.r, Out.Color.r);
    Out.Color.g = mul(In.Colour.g, Out.Color.g);
    Out.Color.b = mul(In.Colour.b, Out.Color.b);
    Out.Color.a = mul(In.Colour.a, Out.Color.a);
        
    return Out;
}

//--------------------------------------------------------------//
// Technique Section for Simple screen transform
//--------------------------------------------------------------//
technique Simple
{
   pass Single_Pass
   {
		#if SM4
		VertexShader = compile vs_4_0_level_9_1 VertexShaderFunction();
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
		#elif SM3
		VertexShader = compile vs_3_0 VertexShaderFunction();
		PixelShader = compile ps_3_0 PixelShaderFunction();
		#else
		VertexShader = compile vs_2_0 VertexShaderFunction();
		PixelShader = compile ps_2_0 PixelShaderFunction();
		#endif
   }
}