﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.Xna.Framework.Audio;

namespace Transition._____Temp.RewindBinaryFormatter 
{
    [Serializable]
    public class Audio
    {
        Stack<DynamicSoundEffectInstance> _usedSources = new Stack<DynamicSoundEffectInstance>();
        Stack<DynamicSoundEffectInstance> _availableSources = new Stack<DynamicSoundEffectInstance>();

        public int count;
        public byte[] byteArray;

        /// <summary>
        /// LoadContent will be called once per game and is the place to load all of your content.
        /// </summary>
        public void LoadContent()
        {
            Stream waveFileStream = File.Open(@"..\..\..\..\TransitionContent\Sounds\AlienExplosion1a.wav", FileMode.Open);

            var reader = new BinaryReader(waveFileStream);

            var chunkId = reader.ReadInt32();
            var fileSize = reader.ReadInt32();
            var riffType = reader.ReadInt32();
            var fmtId = reader.ReadInt32();
            var fmtSize = reader.ReadInt32();
            int fmtCode = reader.ReadInt16();
            int channels = reader.ReadInt16();
            var sampleRate = reader.ReadInt32();
            var fmtAvgBps = reader.ReadInt32();
            int fmtBlockAlign = reader.ReadInt16();
            int bitDepth = reader.ReadInt16();

            if (fmtSize == 18)
            {
                // Read any extra values
                int fmtExtraSize = reader.ReadInt16();
                reader.ReadBytes(fmtExtraSize);

                Trace.WriteLine("has extra values");
            }

            var dataId = reader.ReadInt32();
            var dataSize = reader.ReadInt32();

            Debug.WriteLine($"chunkId: {chunkId}");
            Debug.WriteLine($"fileSize: {fileSize}");
            Debug.WriteLine($"riffType: {riffType}");
            Debug.WriteLine($"fmtId: {fmtId}");
            Debug.WriteLine($"fmtSize: {fmtSize}");
            Debug.WriteLine($"fmtCode: {fmtCode}");
            Debug.WriteLine($"channels: {channels}");
            Debug.WriteLine($"sampleRate: {sampleRate}");
            Debug.WriteLine($"fmtAvgBps: {fmtAvgBps}");
            Debug.WriteLine($"fmtBlockAlign: {fmtBlockAlign}");
            Debug.WriteLine($"bitDepth: {bitDepth}");
            Debug.WriteLine($"dataId: {dataId}");
            Debug.WriteLine($"dataSize: {dataSize}");
            
            byteArray = reader.ReadBytes(dataSize);

            _availableSources.Push(new DynamicSoundEffectInstance(sampleRate, (AudioChannels)channels));

            count = _availableSources.Peek().GetSampleSizeInBytes(TimeSpan.FromMilliseconds(200));

            var durationInSeconds = byteArray.Length / (float)fmtAvgBps;
        }

        public DynamicSoundEffectInstance GetDynamicSoundEffectInstance()
        {
            return _availableSources.Peek();
        }
    }
}