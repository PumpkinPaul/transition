namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class ActorsTool : Tool
    {
        public override string CursorName { get { return "ToolboxActors.png"; } }

        public ActorsTool(TransitionGame game) : base(game) { }

        public override void Update() { }

        public override void Draw() { }
    }
}
