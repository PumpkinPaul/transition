using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// Represents an oriented bounding box in 2d space.
    /// </summary>
    public struct Obb2
    {
        public static readonly Obb2 Empty = new Obb2();

		/// <summary>.</summary>
		public Vector2 Min { get; private set; }
        public Vector2 Max { get; private set; }

        public float Top { get { return Max.Y; } }
        public float Left { get { return Min.X; } }
        public float Bottom { get { return Min.Y; } }
        public float Right { get { return Max.X; } }

        public Obb2(Rect rect, Vector2 origin, float rotation) : this()
        {
            var p1 = new Vector2(rect.Left, rect.Top);
            var p2 = new Vector2(rect.Right, rect.Top);
            var p3 = new Vector2(rect.Left, rect.Bottom);
            var p4 = new Vector2(rect.Right, rect.Bottom);

            p1 = VectorHelper.RotateAboutOrigin(p1, origin, rotation);
            p2 = VectorHelper.RotateAboutOrigin(p2, origin, rotation);
            p3 = VectorHelper.RotateAboutOrigin(p3, origin, rotation);
            p4 = VectorHelper.RotateAboutOrigin(p4, origin, rotation);

            var minX = float.MaxValue;
            var minY = float.MaxValue;
            var maxX = float.MinValue;
            var maxY = float.MinValue;

            if (p1.X < minX) minX = p1.X;
            if (p2.X < minX) minX = p2.X;
            if (p3.X < minX) minX = p3.X;
            if (p4.X < minX) minX = p4.X;

            if (p1.Y < minY) minY = p1.Y;
            if (p2.Y < minY) minY = p2.Y;
            if (p3.Y < minY) minY = p3.Y;
            if (p4.Y < minY) minY = p4.Y;

            if (p1.X > maxX) maxX = p1.X;
            if (p2.X > maxX) maxX = p2.X;
            if (p3.X > maxX) maxX = p3.X;
            if (p4.X > maxX) maxX = p4.X;

            if (p1.Y > maxY) maxY = p1.Y;
            if (p2.Y > maxY) maxY = p2.Y;
            if (p3.Y > maxY) maxY = p3.Y;
            if (p4.Y > maxY) maxY = p4.Y;

            Min = new Vector2(minX, minY);
            Max = new Vector2(maxX, maxY);
        }

        /// <summary>
		/// .
		/// </summary>
		public bool Contains(Vector2 position)
		{
            return (position.X >= Left && position.X <= Right && position.Y <= Top && position.Y >= Bottom);
		}

        public override string ToString()
        {
            return $"Min:{Min} Max:{Max}";
        }
    }
}
