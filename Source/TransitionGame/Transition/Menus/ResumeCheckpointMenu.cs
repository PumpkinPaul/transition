using System;
using System.Linq;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the start menu.
    /// </summary>
    public class ResumeCheckpointMenu : Menu
    {
        public event EventHandler StartGame;

        TextStripControl _checkpointList;
        RepeaterControl _disabledCheckpointList;

        TextLabelControl _scoreValue;
        TextLabelControl _shipValue;
        TextLabelControl _difficultyValue;

        bool _loadOptions;

        /// <summary>
        /// Initialises a new instance of a StartMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
        public ResumeCheckpointMenu(BaseGame game, string title) : base(game, title) { }

        protected override void OnConfigureMenu()
        {

            _checkpointList = GetControl<TextStripControl>("checkpointList");
            _checkpointList.SelectedIndex = -1;
            _checkpointList.DecreaseValue += CheckpointChanged;
            _checkpointList.IncreaseValue += CheckpointChanged;
            _checkpointList.Click += PlayButtonClick;

            _scoreValue = GetControl<TextLabelControl>("scoreValue");
            _shipValue = GetControl<TextLabelControl>("shipValue");
            _difficultyValue = GetControl<TextLabelControl>("difficultyValue");

            _disabledCheckpointList = GetControl<RepeaterControl>("disabledCheckpointList");
        }

        private void CheckpointChanged(object sender, EventArgs e)
        {
            RefreshCheckpointValues();
        }

        private void RefreshCheckpointValues()
        {
            var checkpoint = Game.ActivePlayer.Profile.GetCheckpoint(_checkpointList.SelectedIndex);

            var shipBlueprint = Game.EntityBlueprints.ShipBlueprints().FirstOrDefault(s => s.Name == checkpoint.ShipName);

            if (shipBlueprint == null)
                throw new InvalidOperationException($"Unable to find selected ship: {checkpoint.ShipName}");

            _scoreValue.Text = checkpoint.Score.ToString();
            _shipValue.Text = shipBlueprint.Classification;
            _difficultyValue.Text = checkpoint.Difficulty.ToString();
        }

        /// <summary>
        /// Occurs when a menu is added to the MenuManager.
        /// </summary>
        protected override void OnAddedToManager()
        {
            if (_loadOptions == false)
            {
                SetControlValuesFromOptions();
                _loadOptions = true;
            }

            _checkpointList.ClearItems();

            //If there are checkpoints allow RESUMING
            if (Game.ActivePlayer.Profile.Checkpoints.Any())
            {
                foreach (var checkpoint in Game.ActivePlayer.Profile.Checkpoints)
                    _checkpointList.AddText($"{checkpoint.Level:00}");

                _checkpointList.SelectedIndex = 0;

                if (_checkpointList.SelectedIndex > -1)
                    RefreshCheckpointValues();

                _disabledCheckpointList.DataSource = Enumerable.Range(2, 6).Select(v => new { Value = v.ToString("00")});
            }
        }

        /// <summary>
        /// Sets the values of the controls form the game options.
        /// </summary>
        private void SetControlValuesFromOptions()
        {
            
        }

        /// <summary>
        /// Event handler for the play button click.
        /// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The data for the event.</param>
		private void PlayButtonClick(object sender, EventArgs e)
		{
            //Resume game from checkpoint
            Game.InitialCheckpoint = Game.ActivePlayer.Profile.GetCheckpoint(_checkpointList.SelectedIndex);
            Game.GameOptions.Player1Difficulty = Game.InitialCheckpoint.Difficulty;

            var handler = StartGame;
            handler?.Invoke(this, EventArgs.Empty);
        }
    }
}
