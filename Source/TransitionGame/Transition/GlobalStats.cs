using System.Drawing.Text;

namespace Transition
{
    /// <summary>
    /// Represents stats / attributes that are not affected by rewind
    /// </summary>
    public class GlobalStats
    {
        public int PlayerDeaths { get; private set; }

        public void PlayerDied()
        {
            PlayerDeaths++;
        }
    }
 }
