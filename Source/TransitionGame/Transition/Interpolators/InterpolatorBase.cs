using System;

namespace Transition
{
    [Serializable]
    public abstract class InterpolatorBase
    {
	    public abstract float Interpolate(float a, float b, float ratio);
    }
}
