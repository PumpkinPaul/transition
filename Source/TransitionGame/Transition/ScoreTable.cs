using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Storage;
using Transition.MenuControls;
using Transition.Storage;

namespace Transition
{
	/// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ScoreTable
    {
		/// <summary>.</summary>
        public const int MaxScoreItems = 50;

        #region Fields
		/// <summary>.</summary>
        private readonly List<ScoreItem> _scores = new List<ScoreItem>();

        private readonly List<ScoreItem> _onlineScores = new List<ScoreItem>(ScoreTableControl.PageSize);

        /// <summary>.</summary>
        private int _currentScoreId = -1;

        /// <summary>The location of the saved scores file.</summary>
        private string _location;
        #endregion

        #region Constructors

        /// <summary>
        /// 
        /// </summary>
        public ScoreTable()
        {
            for (int i = 0; i < _onlineScores.Capacity; ++i )
                _onlineScores.Add(new ScoreItem());
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public List<ScoreItem> Scores
        {
            get { return _scores; }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<ScoreItem> OnlineScores
        {
            get { return _onlineScores; }
        }

        /// <summary>
        /// 
		/// </summary>
        public int CurrentScoreId
        {
            get { return _currentScoreId; }
        }
        #endregion

        #region Methods
        public static ScoreTable LoadFromStorage(StorageContainer storageContainer, string filename)
        {
            //Test to see if the file exists - create a new table if one not already on disk.
            if (storageContainer.FileExists(filename) == false)            
                return GenerateTable(filename);  

            using (var fs = storageContainer.OpenFile(filename, FileMode.Open))
                return Load(fs, filename); 
        }

		/// <summary>
		/// 
		/// </summary>
        private static ScoreTable Load(Stream stream, string filename)
        {
            var x = new System.Xml.Serialization.XmlSerializer(typeof(ScoreTable));
            var table = (ScoreTable)x.Deserialize(stream);

            table._location = filename;
            table.Sort();

            return table;
        }

        private static ScoreTable GenerateTable(string filename)
        {
            var scoreTable = new ScoreTable();
            scoreTable.CreateDefaultTable();
            scoreTable._location = filename;
            return scoreTable;
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateDefaultTable()
        {
            for (var i = 0; i < MaxScoreItems; ++i)
            {
                var item = new ScoreItem
                {
                    Level = 0,
                    Score = (i + 1)*100,
                    Name = "pumpkin",
                    Ticks = 0,
                    LevelName = "Zinc"
                };

                SetDisplayItems(item);

                _scores.Add(item);
            }

            Sort();
        }

        /// <summary>
        /// Sets the display values for numeric values
        /// </summary>
        /// <remarks>Saves us having to ToString() each frame causing garbage.</remarks>
        /// <param name="scoreItem">The score item containing the values.</param>
        private static void SetDisplayItems(ScoreItem scoreItem)
        {
            scoreItem.DisplayScore = scoreItem.Score.ToString();
            scoreItem.DisplayTime = ConversionHelper.TicksToDisplayTime(scoreItem.Ticks);
            scoreItem.DisplayLevel = scoreItem.Level.ToString();
        }

        /// <summary>
        /// Saves the score table.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public void Save(BaseGame game)
        {
            game.StorageManager.Save(this, _location, format: StorageManager.Format.XmlSerializer);
        }

		/// <summary>
		/// 
		/// </summary>
        private void Sort()
        {
            _scores.Sort(ScoreItem.Compare);
        }

		/// <summary>
		/// 
		/// </summary>
        public bool AddScoreIfBetter(ScoreItem scoreItem)
        {
            var addedScore = false;
            if (_scores.Count < MaxScoreItems)
            {
                SetDisplayItems(scoreItem);

                _scores.Add(scoreItem);
                addedScore = true;
            }
            else
            {
                foreach (var item in _scores)
                {
                    if (item.IsNewItemBetter(scoreItem) == false) 
                        continue;

                    SetDisplayItems(scoreItem);

                    addedScore = true;
                    _scores.Add(scoreItem);
                    Sort();
                    _scores.RemoveAt(_scores.Count - 1);

                    _currentScoreId = _scores.IndexOf(scoreItem);
                    break;
                }
            }

            if (addedScore == false)
            {
                SetDisplayItems(scoreItem);
            }

            return addedScore;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearOnlineScoreTable()
        {
            foreach (ScoreItem item in _onlineScores)
            {
                item.Score = 0;
                item.Level = 0;
                item.Name = string.Empty;
                item.Ticks = 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void PopulateOnlineScoreTable(string scores)
        {
            if (string.IsNullOrEmpty(scores))
                return;

            var rows = scores.Split('\n');
            var endIndex = Math.Min(rows.Length - 1, _onlineScores.Capacity);
            
            for (int i = 0; i < endIndex; ++i)
            {
                string[] data = rows[i].Split('|');

                _onlineScores[i].Score = Convert.ToInt32(data[1]);
                _onlineScores[i].Level = Convert.ToInt32(data[2]);
                _onlineScores[i].Ticks = Convert.ToInt32(data[3]);
                _onlineScores[i].Name = data[4];
            }
        }
        #endregion
    }
}
