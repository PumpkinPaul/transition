﻿using System.Collections;
using System.Collections.Generic;
using ProtoBuf;
using Transition._____Temp.StarGuard.Entities;

namespace Transition._____Temp.StarGuard
{
    [ProtoContract]
    public class EntityCollection : IEnumerable<KeyValuePair<string, Entity>>
    {
        const string DefaultKey = null;

        //An ordered list of entities where we can also look up an object by a key - an exception is thrown if a duplicate key is added.
        [ProtoMember(1)]
        readonly HashList<string, Entity> _entitities = new HashList<string, Entity>(16, EqualityComparer<string>.Default, DefaultKey);

        public Entity this[int index] => _entitities[index];
        public Entity this[string key] => _entitities[key];

        public Entity Get(int index) { return _entitities[index]; }
        public Entity Get(string key) { return _entitities[key]; }

        public T Get<T>(int index) where T : Entity { return (T)_entitities[index]; }
        public T Get<T>(string key) where T : Entity { return (T)_entitities[key]; }

        public void Add(Entity entity)
        {
            _entitities.Add(DefaultKey, entity);
        }

        public void Add(string name, Entity entity)
        {
            _entitities.Add(name, entity);
        }

        public bool Remove(string key)
        {
            return _entitities.Remove(key);
        }

        public void Remove(int index)
        {
            _entitities.RemoveAt(index);
        }

        public IEnumerator<KeyValuePair<string, Entity>> GetEnumerator()
        {
            return _entitities.List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}