using System;
using System.IO;
using Microsoft.Xna.Framework;
using Transition.CourseSystem;
using Transition.LevelEditing;
using Transition.MenuControls;
using Transition.MenuControls.UserControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the credits menu.
    /// </summary>
    public class LevelEditorMenu : Menu
    {
        enum MenuItems
        {
            NewCourse,
            OpenCourse,
            SaveCourse,
            Spacer1,
            Undo,
            Redo,
            Spacer2,
            CourseDetails,
            Spacer3,
            PlayCourse
        }

        //Ordered list of game options - add images to the list control in this order!
        enum ToolBoxItems
        {
            Select,
            CameraPanning,
            Tiles,
            BezierPath,
            Text,
            //Decals,
            ColorPicker,
            Actors
        }

        NotificationPopupControl _infoPopup;

        ImageListControl _toolboxImageList;
        ImageListControl _menuImageList;
        ImageListControl _tilesImageList;

        ImageControl _coursePanel;
        ImageControl _tilesPanel;
        ImageControl _objectPanel;
        ImageControl _textPanel;

        ImageControl _selectMenuBar;
        
        CheckBoxControl _selectTilesCheckBox;
        CheckBoxControl _selectNodesCheckBox;
        CheckBoxControl _selectTextCheckBox;

        SliderControl _objectSizeSlider;
        SliderControl _objectRotationSlider;
        IntegerLabelControl _objectRotationValue;
        
        CheckBoxControl _gridCheckBox;
        SliderControl _gridSizeSlider;
        FloatLabelControl _gridSizeLabel;

        RepeaterControl _undoRepeater;
        TextLabelControl _undoPanelMemory;

        TextLabelControl _courseName;
        
        TextBoxControl _addLabelTextBox;

        ColorControl _objectColorControl;
        ColorControl _courseColorControl;

        //'Collision' window for editor stateIds - if the stateId leaves the 'collision' window move it!
        int _previousEditorStateId;
        int _minEditorStateId;
        int _maxEditorStateId;


        bool _refreshEditorFromGridControls;
        bool _refreshEditorFromObjectControls;
        bool _refreshEditorFromCourseControls;

        readonly MessageBoxMenu _confirmMenu;
            
        /// <summary>
        /// Initialises a new instance of a CreditsMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public LevelEditorMenu(BaseGame game, string title) : base(game, title) 
        {
            TransitionGame.Instance.LevelEditor.CourseInitialised += CourseInitialised;
            TransitionGame.Instance.LevelEditor.CourseSaved += (sender, e) => { _infoPopup.AddMessage("Course Saved", exclusive: true); };

            //_confirmMenu = new MessageBoxMenu(game, "Confirmation");
            //game.MenuManager.RegisterMenu(_confirmMenu, "levelEditorConfirm");
            //_confirmMenu.ShowYesButton = true;
            //_confirmMenu.ShowNoButton = true;
            /*_confirmMenu.AfterClosed += (sender, e) => 
            {
                var confirmMenu = sender as MessageBoxMenu;
                if (confirmMenu == null)
                    return;

                if (confirmMenu.Response == MenuResponse.Ok)
                {
                    var menuItem = (MenuItems)(_menuImageList.SelectedIndex);
                    switch (menuItem)
                    {
                        case MenuItems.NewCourse:
                            TransitionGame.Instance.LevelEditor.NewCourse();
                            break;

                        case MenuItems.OpenCourse:
                            var fileListMenu = (FileListMenu)MenuManager.GetMenu(typeof(FileListMenu));
                            TransitionGame.Instance.LevelEditor.OpenCourse(fileListMenu.FileName);
                            break;
                     }
                 }
            };
            */
        }
        
        /// <summary>
        /// Gets whether the menu allows itself to be closed with the back button.
        /// </summary>
        public override bool AllowCloseWhenBackPressed
        {
            get { return false; }
        }

        protected override void OnConfigureMenu() 
        {
            _infoPopup = GetControl<NotificationPopupControl>("infoPopup");

            _selectMenuBar = GetControl<ImageControl>("selectMenuBar");

            _coursePanel = GetControl<ImageControl>("coursePanel");
            _tilesPanel = GetControl<ImageControl>("tilesPanel");
            _objectPanel = GetControl<ImageControl>("objectPanel");
            _textPanel = GetControl<ImageControl>("textPanel");
            
            _menuImageList = GetControl<ImageListControl>("menuImageList");
            _menuImageList.SelectedIndex = 0;
            _menuImageList.Click += MenuImageListClick;

            _toolboxImageList = GetControl<ImageListControl>("toolboxImageList");
            _toolboxImageList.SelectedIndex = 0;
            _toolboxImageList.Click += ToolboxImageListClick;

            var cameraCheckBox = (CheckBoxControl)ControlMap["cameraCheckBox"];
            cameraCheckBox.Value = TransitionGame.Instance.LevelEditor.CameraLayerVisible;
            cameraCheckBox.Click += (sender, e) => { TransitionGame.Instance.LevelEditor.CameraLayerVisible = !TransitionGame.Instance.LevelEditor.CameraLayerVisible; };

            var decalsCheckBox = (CheckBoxControl)ControlMap["decalsCheckBox"];
            decalsCheckBox.Value = TransitionGame.Instance.LevelEditor.DecalLayerVisible;
            decalsCheckBox.Click += (sender, e) => {TransitionGame.Instance.LevelEditor.DecalLayerVisible = !TransitionGame.Instance.LevelEditor.DecalLayerVisible; };

            var textCheckBox = (CheckBoxControl)ControlMap["textCheckBox"];
            textCheckBox.Value = TransitionGame.Instance.LevelEditor.TextLayerVisible;
            textCheckBox.Click += (sender, e) => { TransitionGame.Instance.LevelEditor.TextLayerVisible = !TransitionGame.Instance.LevelEditor.TextLayerVisible; };

            var tilesCheckBox = (CheckBoxControl)ControlMap["tilesCheckBox"];
            tilesCheckBox.Value = TransitionGame.Instance.LevelEditor.TilesLayerVisible;
            tilesCheckBox.Click += (sender, e) => { TransitionGame.Instance.LevelEditor.TilesLayerVisible = !TransitionGame.Instance.LevelEditor.TilesLayerVisible; };

            var actorsCheckBox = (CheckBoxControl)ControlMap["actorsCheckBox"];
            actorsCheckBox.Value = TransitionGame.Instance.LevelEditor.ActorsLayerVisible;
            actorsCheckBox.Click += (sender, e) => { TransitionGame.Instance.LevelEditor.ActorsLayerVisible = !TransitionGame.Instance.LevelEditor.ActorsLayerVisible; };

            TransitionGame.Instance.LevelEditor.SetToolboxItem(LevelEditor.ToolboxItem.SelectAndTransform);

            _tilesImageList = GetControl<ImageListControl>("tilesImageList");
            _tilesImageList.ClearImages();
            for(var i = 0; i <= 14; i++)
                _tilesImageList.AddImage(Game.TextureManager.GetTextureInfo("Tile" + i), _toolboxImageList.Tint, "Tile" + i);
            _tilesImageList.SelectedIndex = 0;
            _tilesImageList.Click += (sender, e) => { TransitionGame.Instance.LevelEditor.SelectedTileId = _tilesImageList.SelectedIndex; };

            _objectSizeSlider = GetControl<SliderControl>("objectSizeSlider");
            _objectSizeSlider.ValueChanged += ObjectSizeSliderValueChanged;

            _objectRotationSlider = GetControl<SliderControl>("objectRotationSlider");
            _objectRotationSlider.ValueChanged += ObjectRotationSliderValueChanged;

            _objectRotationValue = GetControl<IntegerLabelControl>("objectRotationValue");

            _objectColorControl = new ColorControl(this, "objectColor");
            _objectColorControl.ColorChanged += (sender, e) =>
            {
                if (_refreshEditorFromObjectControls == false)
                    return;

                TransitionGame.Instance.LevelEditor.ApplyObjectColorToSelection(_objectColorControl.Color, !e.IsDragging);
            };

            _courseColorControl = new ColorControl(this, "courseColor");
            _courseColorControl.ColorChanged += (sender, e) =>
            {
                if (_refreshEditorFromCourseControls == false)
                    return;

                TransitionGame.Instance.LevelEditor.ApplyBackgroundColor(_courseColorControl.Color, !e.IsDragging);
            };
            
            _gridCheckBox = GetControl<CheckBoxControl>("gridCheckBox");
            _gridCheckBox.Click += (sender, e) => 
            {
                if (_refreshEditorFromGridControls == false)
                    return;

                TransitionGame.Instance.LevelEditor.DesignGrid.Active = _gridCheckBox.Value;
            };

            _gridSizeSlider = GetControl<SliderControl>("gridSizeSlider");
            _gridSizeSlider.ValueChanged += (sender, e) => 
            { 
                if (_refreshEditorFromGridControls == false)
                    return;

                TransitionGame.Instance.LevelEditor.DesignGrid.StepId = _gridSizeSlider.Value; 
                _gridSizeLabel.Value = TransitionGame.Instance.LevelEditor.DesignGrid.Step;
            };

            _gridSizeLabel = GetControl<FloatLabelControl>("gridSizeLabel");

            _undoRepeater = GetControl<RepeaterControl>("undoRepeater");
            _undoRepeater.PageIndex = 0;
            _undoRepeater.PageSize = 22;
            _undoRepeater.ItemControlDraw += (menuControl, e) => 
            {
                switch (menuControl.Name)
                {
                    case "undoRepeaterId":
                        var undoRepeaterId = (IntegerLabelControl)menuControl;
                        undoRepeaterId.Tint = Theme.Current.GetColor(Theme.Colors.Focus);
                        undoRepeaterId.Value = e.Rank;
                        break;

                    case "undoAction":
                        var textLabelControl = (TextLabelControl)menuControl;
                        textLabelControl.Tint = e.Rank == TransitionGame.Instance.LevelEditor.EditorStateId ? Color.White : Color.Gray;
                        
                        break;
                }
            };

            _undoPanelMemory = GetControl<TextLabelControl>("undoPanelMemory");

            _courseName = GetControl<TextLabelControl>("courseName");
            
            _addLabelTextBox = GetControl<TextBoxControl>("addLabelTextBox");

            _selectTilesCheckBox = GetControl<CheckBoxControl>("selectTilesCheckBox");
            _selectTilesCheckBox.Click += (sender, e) => { TransitionGame.Instance.LevelEditor.CanSelectTiles = _selectTilesCheckBox.Value; };
            TransitionGame.Instance.LevelEditor.CanSelectTiles = _selectTilesCheckBox.Value;

            _selectNodesCheckBox = GetControl<CheckBoxControl>("selectNodesCheckBox");
            _selectNodesCheckBox.Click += (sender, e) => { TransitionGame.Instance.LevelEditor.CanSelectNodes = _selectNodesCheckBox.Value; };
            TransitionGame.Instance.LevelEditor.CanSelectNodes = _selectNodesCheckBox.Value;

            _selectTextCheckBox = GetControl<CheckBoxControl>("selectTextCheckBox");
            _selectTextCheckBox.Click += (sender, e) => { TransitionGame.Instance.LevelEditor.CanSelectLabels = _selectTextCheckBox.Value; };
            TransitionGame.Instance.LevelEditor.CanSelectLabels = _selectTextCheckBox.Value;
        }

        protected override void OnAddedToManager() 
        {
            _undoRepeater.DataSource = TransitionGame.Instance.LevelEditor.StateBuffer; 

            _maxEditorStateId = _undoRepeater.PageSize - 1;

            _refreshEditorFromGridControls = true;
            _refreshEditorFromObjectControls = true;

            _gridSizeLabel.Value = TransitionGame.Instance.LevelEditor.DesignGrid.Step;
            _gridSizeSlider.MaxValue = TransitionGame.Instance.LevelEditor.DesignGrid.MaxSteps;
            
            if (TransitionGame.Instance.LevelEditor.Course == null)
                CreateNewCourse();

            InitialiseCourseDetails();
        }

        protected override void OnUpdateMustCallBase() 
        {
            base.OnUpdateMustCallBase();

            UpdateGridControlsFromEditor();
            UpdateBufferControlFromEditor();
            UpdateObjectColorControlsFromEditor();

            TransitionGame.Instance.LevelEditor.LabelText = _addLabelTextBox.Text;
        }

        void UpdateGridControlsFromEditor()
        {
            _refreshEditorFromGridControls = false;
            _gridCheckBox.Value = TransitionGame.Instance.LevelEditor.DesignGrid.Active;  
            _gridSizeSlider.Value = TransitionGame.Instance.LevelEditor.DesignGrid.StepId;  
            _refreshEditorFromGridControls = true;
        }

        void UpdateBufferControlFromEditor()
        {
            var memory = 0;
            foreach (var sb in TransitionGame.Instance.LevelEditor.StateBuffer)
                memory += sb.State.Length;

            _undoPanelMemory.Text = $"{memory/1024.0f:0}K";

            if (TransitionGame.Instance.LevelEditor.EditorStateId == _previousEditorStateId)
                return;

            var reSkip = false;
            var id = TransitionGame.Instance.LevelEditor.EditorStateId;
            if (id < _minEditorStateId)
            {
                _minEditorStateId = id;
                _maxEditorStateId = _minEditorStateId + _undoRepeater.PageSize - 1;
                reSkip = true;
            }
            else if (id > _maxEditorStateId)
            {
                _maxEditorStateId = id;
                _minEditorStateId = _maxEditorStateId - _undoRepeater.PageSize + 1;
                reSkip = true;
            }

            if (reSkip)
            {
                var skip = _minEditorStateId;
                _undoRepeater.Skip = skip;
            }

            _previousEditorStateId = TransitionGame.Instance.LevelEditor.EditorStateId;
        }

        void UpdateObjectColorControlsFromEditor()
        {
            _refreshEditorFromObjectControls = false;
            _objectColorControl.Color = TransitionGame.Instance.LevelEditor.ObjectColor;
            _refreshEditorFromObjectControls = true;
        }

        private void ObjectSizeSliderValueChanged(object sender, EventArgs e)
        {
            if (_refreshEditorFromObjectControls == false)
                return;
            
            TransitionGame.Instance.LevelEditor.SetNormalizedTextSize(_objectSizeSlider.NormalizedValue);
        }

        private void ObjectRotationSliderValueChanged(object sender, EventArgs e)
        {
            if (_refreshEditorFromObjectControls == false)
                return;

            _objectRotationValue.Value = (int)MathHelper.Lerp(0, 360, _objectRotationSlider.NormalizedValue);
            TransitionGame.Instance.LevelEditor.SetTextRotation(_objectRotationSlider.NormalizedValue);
        }

        private void MenuImageListClick(object sender, EventArgs e)
        {
            var menuItem = (MenuItems)(_menuImageList.SelectedIndex);

            switch (menuItem)
            {
                case MenuItems.NewCourse:
                    if (ConfirmChangeLevel(menuItem) == false)
                        return;
                    
                    CreateNewCourse();
                    break;

                case MenuItems.OpenCourse:
                    if (ConfirmChangeLevel(menuItem) == false)
                        return;

                    var fileMenu = (FileListMenu)MenuManager.GetMenu(typeof(FileListMenu));
                    var dir = Path.Combine(Platform.ContentPath, "Courses");
                    var files = Directory.GetFiles(dir, "*.course");
                    fileMenu.SetFiles(files);
                    fileMenu.AfterClosed += FileMenuAfterClosed;
                    MenuManager.AddMenu(fileMenu);
                    break;

                case MenuItems.SaveCourse:
                    TransitionGame.Instance.LevelEditor.SaveCourse();
                    break;

                case MenuItems.Spacer1:
                    break;

                case MenuItems.Undo:
                    TransitionGame.Instance.LevelEditor.Undo();
                    break;

                case MenuItems.Redo:
                    TransitionGame.Instance.LevelEditor.Redo();
                    break;

                case MenuItems.CourseDetails:
                    _coursePanel.Visible = !_coursePanel.Visible;
                    break;

                case MenuItems.PlayCourse:
                    TransitionGame.Instance.BeginPlaySession();
                    break;
            }
        }

        void CreateNewCourse()
        {
            TransitionGame.Instance.LevelEditor.NewCourse();
            _infoPopup.AddMessage("New Course", exclusive: true);
        }

        void FileMenuAfterClosed(object sender, EventArgs e)
        {
            var fileListMenu = (FileListMenu)sender;
            fileListMenu.AfterClosed -= FileMenuAfterClosed;
            if ((fileListMenu).Response == MenuResponse.Ok)
            {
                //var filepath = Path.Combine(Platform.ResourcesPath, "Courses", fileListMenu.FileName);
                var filepath = fileListMenu.FileName;
                TransitionGame.Instance.LevelEditor.OpenCourse(filepath);
                _infoPopup.AddMessage("Course Loaded", exclusive:true);
            }
            //(FileListMenu)MenuManager.GetMenu(typeof(FileListMenu)).AfterClosed -= fileMenu_AfterClosed;
        }

        private void ToolboxImageListClick(object sender, EventArgs e)
        {
            _tilesPanel.Visible = false;
            _selectMenuBar.Visible = false;
            _textPanel.Visible = false;
            
            switch((ToolBoxItems)(_toolboxImageList.SelectedIndex))
            {
                case ToolBoxItems.CameraPanning:
                    TransitionGame.Instance.LevelEditor.SetToolboxItem(LevelEditor.ToolboxItem.CanvasPanning);
                    break;

                case ToolBoxItems.Select:
                    _selectMenuBar.Visible = true;
                    TransitionGame.Instance.LevelEditor.SetToolboxItem(LevelEditor.ToolboxItem.SelectAndTransform);
                    break;

                case ToolBoxItems.Tiles:
                    _tilesPanel.Visible = true;
                    _objectPanel.Visible = true;
                    TransitionGame.Instance.LevelEditor.SetToolboxItem(LevelEditor.ToolboxItem.Tiles);
                    break;

                case ToolBoxItems.BezierPath:
                    TransitionGame.Instance.LevelEditor.SetToolboxItem(LevelEditor.ToolboxItem.CameraPath);
                    break;

                case ToolBoxItems.Text:
                    _objectPanel.Visible = true;
                    _textPanel.Visible = true;
                    TransitionGame.Instance.LevelEditor.SetToolboxItem(LevelEditor.ToolboxItem.Text);
                    break;

                case ToolBoxItems.ColorPicker:
                    _objectPanel.Visible = true;
                    TransitionGame.Instance.LevelEditor.SetToolboxItem(LevelEditor.ToolboxItem.ColorPicker);
                    break;

                case ToolBoxItems.Actors:
                    TransitionGame.Instance.LevelEditor.SetToolboxItem(LevelEditor.ToolboxItem.Actors);
                    break;

                //case ToolBoxItems.Decals:
                //    break;
            }
        }

        bool ConfirmChangeLevel(MenuItems menuItem)
        {
            if (TransitionGame.Instance.LevelEditor.IsDirty == false)
                return true;

            if (menuItem == MenuItems.NewCourse)
                _confirmMenu.Heading = "New Course";
            else if (menuItem == MenuItems.OpenCourse)
                _confirmMenu.Heading = "Open Course";
            else
                return true;

            MenuManager.AddMenu(_confirmMenu);
            return false;
        }

        void CourseInitialised(object sender, EventArgs e)
        {
            InitialiseCourseDetails();
        }

        void InitialiseCourseDetails()
        {
            _refreshEditorFromCourseControls = false;

            _courseName.Text = TransitionGame.Instance.LevelEditor.Course.Name;
            _courseColorControl.Color = TransitionGame.Instance.LevelEditor.Course.BackgroundColor;
            
            _refreshEditorFromCourseControls = true;
        }
    }
}
