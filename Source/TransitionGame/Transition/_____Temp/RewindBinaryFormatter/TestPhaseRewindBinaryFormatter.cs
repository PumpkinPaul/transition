using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Transition.Extensions;
using Transition.GamePhases;
using Transition.Input;
using Transition.PostProcess;
using Transition.Rendering;

namespace Transition._____Temp.RewindBinaryFormatter
{
    /// <summary>
    /// Handles the splash screen at the beginning of the game.
    /// </summary>
    public class TestPhaseRewindBinaryFormatter : GamePhase
    {
        int _frameId = 0;
        bool _latched;
        bool _rewinding;

        World _world;
        public static Audio _audio = new Audio();

        private readonly Stack<Stream> _state = new Stack<Stream>();

        SoundEntity _sfx;

        public TestPhaseRewindBinaryFormatter(BaseGame game) : base(game) { }

        protected override void OnCreate()
        {
            Game.ScreenFlashManager.ClearColor = Color.White;

            //TODO: Testing - add back in if ever required
            //_audio.LoadContent();
            _world = new World();
            _world.AddEntity(new PlayerEntity());
        }

        int _rewindThisManyFrames = 3;
        protected override void OnUpdate()
        {
            var wasRewinding = _rewinding;

            _rewinding = false;

            if (KeyboardHelper.IsKeyPressed(Keys.R) == false)
                _latched = false;

            if (!_latched && KeyboardHelper.IsKeyPressed(Keys.R))
            {
                if (_frameId == 0)
                {
                    _latched = true;
                }
                else if (_frameId > _rewindThisManyFrames - 1)
                {
                    //Rewind time!
                    _rewinding = true;

                }
            }

            if (_rewinding)
            {
                TransitionGame.Instance.DistortionsProcessor.Active = true;
                TransitionGame.Instance.DistortionsProcessor.Enabled = true;

                var ce = DistortionEffects.GetReusableColorEffect(30, 90, 0, 8);
                var hne = DistortionEffects.GetReusableHorizontalNoiseEffect(120, 180, 0.25f);
                //var re = DistortionEffects.GetReusableRollEffect(10, 0.01f);

                if (ce.IsReady)
                    DistortionEffects.SetColorEffect(ce);

                if (hne.IsReady)
                    DistortionEffects.SetHorizontalNoiseEffect(hne);

                //if (re.IsReady)
                //    DistortionEffects.SetRollEffect(re);
            }

            if (_rewinding)
            {
                Game.ScreenFlashManager.ClearColor = Color.LightGray;

                _frameId -= _rewindThisManyFrames;

                _world.BeforeRewind();

                LoadState(rewinding: true);
            }
            else
            {
                Game.ScreenFlashManager.ClearColor = Color.White;
                TransitionGame.Instance.DistortionsProcessor.Active = false;
                TransitionGame.Instance.DistortionsProcessor.Enabled = false;

                _world.Update(wasRewinding);


                if (_frameId.In(60, 120, 180))
                {
                    var enemy = new EnemyEntity();
                    enemy.Position.X = _frameId / 20.0f;
                    enemy.Position.Y = 8.0f;
                    _world.AddEntity(enemy);

                    var sfx = new SoundEntity();
                    sfx.Play();
                    _world.AddEntity(sfx);
                }

                SaveState();
                _frameId++;
            }
        }

        //private void LoadState(bool rewinding)
        //{        
        //    Stream stream = null;

        //    for(var i = 0; i < _rewindThisManyFrames; i++) 
        //        stream = _state.Pop();

        //    if (stream == null)
        //        return;

        //    stream.Position = 0;
        //    var bf = new BinaryFormatter();
        //    _world = (World)bf.Deserialize(stream);
        //    _world.StateLoaded(rewinding);
        //}

        //private void SaveState()
        //{
        //    var memoryStream = new MemoryStream();

        //    var bf = new BinaryFormatter();
        //    bf.Serialize(memoryStream, _world);

        //    _state.Push(memoryStream); 
        //}

        readonly BinaryFormatter _binaryFormatter = new BinaryFormatter();
        readonly MemoryStream _memoryStream = new MemoryStream();
        readonly Stack<long> _streamPositions = new Stack<long>();

        private void LoadState(bool rewinding)
        {
            ////Things that need to happen before loading the world state
            ////WE ARE THROWING THE CURRENT WORLD STATE AWAY
            ////We want to create our deserialized objects from a pool - the used items can be flagged as cleared
            //EntityFactory.FreeAllObjects(); 


            var position = 0L;

            for (var i = 0; i < _rewindThisManyFrames; i++)
                position = _streamPositions.Pop();

            _memoryStream.Position = position;

            _world = (World)_binaryFormatter.Deserialize(_memoryStream);
            _world.StateLoaded(rewinding);
        }

        private void SaveState()
        {
            _streamPositions.Push(_memoryStream.Position);
            _binaryFormatter.Serialize(_memoryStream, _world);
        }

        protected override void OnRender()
        {
            TransitionGame.Instance.CameraManager.Render();
            TransitionGame.Instance.CameraManager.Camera = TransitionGame.Instance.GameCamera;

            TransitionGame.Instance.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);

            TransitionGame.Instance.Renderer.DrawString(TransitionGame.Instance.FontManager.GetBitmapFont("Bebas Neue"), new Vector2(18.0f, 9.0f), Vector2.One, "RewindBinary Formatter", Alignment.CentreRight, Color.DarkBlue, TransparentRenderStyle.Instance);

            if (_rewinding)
                TransitionGame.Instance.Renderer.DrawString(TransitionGame.Instance.FontManager.GetBitmapFont("Bebas Neue"), new Vector2(-18.0f, 9.0f), Vector2.One, "REWINDING", Alignment.CentreLeft, Color.DarkBlue, TransparentRenderStyle.Instance);

            var q = new Quad(10.0f, -18.0f, 9.75f, -18 + (_frameId * 0.01f));
            var textureInfo = TransitionGame.Instance.TextureManager.GetTextureInfo("Pixel");
            TransitionGame.Instance.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, Color.DarkBlue, TransparentRenderStyle.Instance);

            _world.Draw();
            TransitionGame.Instance.Renderer.End();
        }
    }
}
