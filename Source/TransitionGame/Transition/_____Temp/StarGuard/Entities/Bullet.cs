﻿using Microsoft.Xna.Framework;
using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    [ProtoContract]
    public class Bullet : Entity
    {
        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public Bullet(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.Bullet;

        public override void Initialise(string name)
        {
            base.Initialise(name);
            
            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        } 

        protected override void OnUpdate(bool wasRewinding)
        {
            TransitionGame.Instance.GameMap.HandleCollisions(this);
        }

        protected override void OnCollisionWithEntity(Entity collider, float time) 
        { 
            Deactivate();

            CreateSparks(Position.X > collider.Position.X ? CollisionEdge.Left : CollisionEdge.Right);
        }

        public override void OnCollisionWithMap(CollisionEdge entityCollisionEdge) 
        {
            Deactivate();

            CreateSparks(entityCollisionEdge);
            Sound.Play(Game, "BulletWall", Position);
        }

        void CreateSparks(CollisionEdge entityCollisionEdge)
        {
            var ps = Game.EntityFactory.Create<ParticleSystem>();
            if (ps == null)
                return;

            ps.Position = Position + new Vector2(entityCollisionEdge == CollisionEdge.Right ? CollisionBounds.Width * 0.5f : CollisionBounds.Width * -0.5f, 0.0f);
            ps.TemplateName = "BulletSparks";
            Game.World.Add(ps);
        }
    }
}