using System;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the pause menu.
    /// </summary>
    public class PauseMenu : Menu
    {
        //Ordered list of game options - add images to the list control in this order!
        private enum MenuOptions
        {
            Resume,
            Restart,
            Quit,
            Hiscores,
            Controls,
            Settings,
        }

        private ImageListControl _imageListControl;

        private bool _awaitingRestartConfirm;
        private bool _awaitingQuitConfirm;

        public override string BackButtonText => "Resume";

        /// <summary>
        /// Initialises a new instance of a PauseMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public PauseMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {            
            _imageListControl = GetControl<ImageListControl>("imageListControl");
            var iconColor = _imageListControl.Tint;

            _imageListControl.AddImage(Game.TextureManager.GetTextureInfo("MenuItemResume"), iconColor, "resume", "continue playing the game");
            _imageListControl.AddImage(Game.TextureManager.GetTextureInfo("MenuItemRestart"), iconColor, "restart", "restart the game");
            _imageListControl.AddImage(Game.TextureManager.GetTextureInfo("MenuItemQuit"), iconColor, "quit", "quit this play session");
            _imageListControl.AddImage(Game.TextureManager.GetTextureInfo("MenuItemHiscores"), iconColor, "scores", "view high score tables");
            _imageListControl.AddImage(Game.TextureManager.GetTextureInfo("MenuItemControls"), iconColor, "controls", "view game controls");
            _imageListControl.AddImage(Game.TextureManager.GetTextureInfo("MenuItemSettings"), iconColor, "settings", "configure game settings (video, audio, etc)");   
            _imageListControl.SelectedIndex = 0;
            _imageListControl.Click += ImageListControlClick;

            GetControl<TextButtonControl>("resumeButton").Click += (sender, args) => ResumeGame();
            GetControl<TextButtonControl>("restartButton").Click += (sender, args) => ShowRestartConfirm();
            GetControl<TextButtonControl>("quitButton").Click += (sender, args) => ShowQuitConfirm();
            GetControl<TextButtonControl>("scoresButton").Click += (sender, args) => MenuManager.AddMenu(MenuManager.GetMenu(typeof(HeroesMenu)));
            GetControl<TextButtonControl>("controlsButton").Click += (sender, args) => MenuManager.AddMenu(MenuManager.GetMenu(typeof(ControlsMenu)));
            GetControl<TextButtonControl>("settingsButton").Click += (sender, args) => MenuManager.AddMenu(MenuManager.GetMenu(typeof(SettingsMenu)));
            GetControl<TextButtonControl>("creditsButton").Click += (sender, args) => MenuManager.AddMenu(MenuManager.GetMenu(typeof(CreditsMenu)));
        }

        private void ImageListControlClick(object sender, EventArgs e)
        {
            switch((MenuOptions)(_imageListControl.SelectedIndex))
            {
                case MenuOptions.Resume:
                    ResumeGame();
                    break;

                case MenuOptions.Restart:
                    ShowRestartConfirm();
                    break;

                case MenuOptions.Quit:
                    ShowQuitConfirm();
                    break;

                case MenuOptions.Hiscores:
                    MenuManager.AddMenu(MenuManager.GetMenu(typeof(HeroesMenu)));
                    break;

                case MenuOptions.Controls:
                    MenuManager.AddMenu(MenuManager.GetMenu(typeof(ControlsMenu)));
                    break;

                case MenuOptions.Settings:
                    MenuManager.AddMenu(MenuManager.GetMenu(typeof(SettingsMenu)));
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnGotFocus()
        {
            if (_awaitingRestartConfirm)
            {
                if (MessageBoxMenu.Result == MessageBoxMenu.MessageBoxResult.Yes)
                {
                    RestartGame();
                }
                _awaitingRestartConfirm = false;
            }
            else if (_awaitingQuitConfirm)
            {
                if (MessageBoxMenu.Result == MessageBoxMenu.MessageBoxResult.Yes)
                {
                    QuitGame();
                }
                _awaitingQuitConfirm = false;
            }
        }

        /// <summary>
        /// Called when the 'back' button is pressed on the controller and the menu accepts the back command.
        /// </summary>
        /// <returns>True if the menu should play a sound effect; otherwise false.</returns>
        protected override bool OnBack()
        {
            ResumeGame();
            return true;
        }

        /// <summary>
        /// Called when the menu has finished its closing state.
        /// </summary>
        protected override void OnClosed()
		{
            Game.ResetPrimaryControllerIfRequired();
		}

        /// <summary>
        /// Shows the restart confirm menu
        /// </summary>
        private void ShowRestartConfirm()
        {
            _awaitingRestartConfirm = true;

            var messageBoxMenu = (MessageBoxMenu)MenuManager.GetMenu(typeof(MessageBoxMenu));
            messageBoxMenu.ShowNoButton = true;
            messageBoxMenu.ShowYesButton = true;
            messageBoxMenu.YesButtonText = "Yes";
            messageBoxMenu.NoButtonText = "No";
            messageBoxMenu.Title = "Confirm Restart";
            messageBoxMenu.Message = "Are you sure you want to restart?";
            MenuManager.AddMenu(messageBoxMenu);
        }

        /// <summary>
        /// Shows the restart confirm menu
        /// </summary>
        private void ShowQuitConfirm()
        {
            _awaitingQuitConfirm = true;

            var messageBoxMenu = (MessageBoxMenu)MenuManager.GetMenu(typeof(MessageBoxMenu));
            messageBoxMenu.ShowNoButton = true;
            messageBoxMenu.ShowYesButton = true;
            messageBoxMenu.YesButtonText = "Yes";
            messageBoxMenu.NoButtonText = "No";
            messageBoxMenu.Title = "Confirm Quit";
            messageBoxMenu.Message = "Are you sure you want to quit?";
            MenuManager.AddMenu(messageBoxMenu);
        }

        /// <summary>
        /// Resumes the current game.
        /// </summary>
        /// <remarks>Resumes the current game.</remarks>
        private void ResumeGame()
        {
            Game.Resume();
            Close();
        }

        /// <summary>
        ///Restarts the current game.
        /// </summary>
        /// <remarks>Restarts the game from the beginning - resets scores, lives, level, etc.</remarks>
        private void RestartGame()
        {
            Game.Restart();
            Close();
        }

        /// <summary>
        /// Quits the current game.
        /// </summary>
        /// <remarks>Quits the current game and returns to the main menu.</remarks>
        private void QuitGame()
        {        
            Game.Quit();
            Close();    
        }
    }
}
