using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// A control that displays a list of options.
    /// </summary>
    public class ListControl : MenuControl
    {
        public enum LayoutStyleType
        {
            ButtonTextButton,
            ButtonButtonText
        }

        public event EventHandler TextButtonClicked;	

		/// <summary></summary>
		private readonly List<string> _options = new List<string>();

        /// <summary></summary>
        private const float SpacerWidth = 0.2f;

        /// <summary>
        /// Gets or sets the font to use for text rendering.
        /// </summary>
        public BitmapFont Font { get; set; }

        /// <summary>
        /// Gets or sets the font to use for text rendering.
        /// </summary>
        public Alignment Alignment { get; set; }

        /// <summary>
        /// Gets or sets the size of text rendering.
        /// </summary>
        public Vector2 FontSize { get; set; }

        /// <summary>
        /// Gets or sets the size of the previous and next buttons.
        /// </summary>
        public Vector2 ButtonSize { get; set; }

        /// <summary>
        /// Gets or sets the id of the texture to use for the previous button.
        /// </summary>
        public TextureInfo PreviousImageInfo { get; set; }

        /// <summary>
        /// Gets or sets the id of the texture to use for the next button.
        /// </summary>
        public TextureInfo NextImageInfo { get; set; }

        /// <summary>
        /// Gets or sets the index of the selected item.
        /// </summary>
        public int SelectedIndex { get; set; }

        public LayoutStyleType LayoutStyle { get; set; }

        public float MinTextWidth { get; set; }
        
        public override bool AutoTabIndex => true;

        public int Delay { get; set; }

        int _delayTicks = 0;
        bool _canTransition = true;
        

        /// <summary>
		/// .
		/// </summary>
        public ListControl(BaseGame game) : base(game) 
        {
		    SelectedIndex = -1;
		    AButtonText = null;
            Alignment = Alignment.CentreLeft;
            ButtonSize = new Vector2(0.5f);
			Font = Theme.Current.GetFont(Theme.Fonts.Default);
            FontSize = Theme.Current.GetFontSize(Theme.FontSizes.Normal);
            LayoutStyle = LayoutStyleType.ButtonTextButton;
            MinTextWidth = 0.0f;

            NextImageInfo = Game.TextureManager.GetTextureInfo("NextItem");
            PreviousImageInfo = Game.TextureManager.GetTextureInfo("PreviousItem");
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            Alignment = element.GetAttributeEnum("alignment", Alignment);
            Font = ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            LayoutStyle = element.GetAttributeEnum("layoutStyle", LayoutStyle);
            MinTextWidth = element.GetAttributeSingle("minTextWidth", MinTextWidth);

            ButtonSize = element.GetAttributeVector2("buttonSize", ButtonSize);
            PreviousImageInfo = ParseTexture(element, "previousImageInfo", PreviousImageInfo);
            NextImageInfo = ParseTexture(element, "nextImageInfo", NextImageInfo);

            Delay = element.GetAttributeInt32("delay", Delay);

            //Content! Might be items
            var selectedIndex = -1;
            foreach (var itemElement in element.Elements("item"))
            {
                selectedIndex++;
                var text = itemElement.GetAttributeString("text");
                if (text == null)
                    continue;

                AddOption(text);

                var selected = itemElement.GetAttributeBool("selected", false);
                if (selected)
                    SelectedIndex = selectedIndex;
            }
        }
		
		/// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
		public override Rect Bounds
		{
            get
            {
                var height = Math.Max(ButtonSize.Y, FontSize.Y);
                var length = MeasureTextLength + ((ButtonSize.X + SpacerWidth) * 2.0f); //text plus the two button inc spacers

                var bounds = new Rect(Position, new Vector2(length, height), Alignment);


                return bounds;
            }
		}

        /// <summary>
        /// Gets the selected text.
        /// </summary>
        public string SelectedText
        {
            get 
            {
                if (SelectedIndex != -1)
                    return _options[SelectedIndex];
                
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the selected index as a boolean.
        /// </summary>
        /// <remarks>
        /// Returns false if either no items are selected or the first item is selected.
        /// Otherwise returns true;
        /// </remarks>
        public bool SelectedItemAsBoolean => SelectedIndex > 0;

        /// <summary>
        /// Gets the number of options in the list.
        /// </summary>
        public int Count => _options.Count;

        public override string LeftRightButtonText => "change value";

        /// <summary>
		/// .
		/// </summary>
		protected override void OnMouseUp()
		{
            if (SelectedIndex > 0)
            {
                if (IsMouseOverPreviousButton)
                    PerformDecreaseValue();
            }

            if (SelectedIndex < Count - 1)
            {
                if (IsMouseOverNextButton)
                    PerformIncreaseValue();    
            }

            if (IsMouseOverTextButton)
                RaiseTextButtonClicked(EventArgs.Empty);
		}
		
		/// <summary>
		/// .
		/// </summary>
		protected override bool OnIncreaseValue()
		{
            if (SelectedIndex >= _options.Count - 1)
                return false;

            if (_canTransition == false)
                return false;

            ++SelectedIndex;
            _canTransition = false;
            _delayTicks = 0;
            return true;

		}
		
		/// <summary>
		/// .
		/// </summary>
		protected override bool OnDecreaseValue()
		{
            if (SelectedIndex <= 0)
                return false;

            if (_canTransition == false)
                return false;

            --SelectedIndex;
            _canTransition = false;
            _delayTicks = 0;
            return true;
		}

        protected override void OnUpdate()
        {
            //Handle delayed transitions between selected indices - allows us to hook into Transitions effectively.
            if (Delay <= 0)
            {
                _canTransition = true;
            }
            else
            {
                if (_canTransition == false)
                {
                    _delayTicks++;
                    if (_delayTicks >= Delay)
                    {
                        _delayTicks = 0;
                        _canTransition = true;
                    }
                }
            }

            if (Game.InputMethodManager.InputMethod.SupportsMouse)
            {
                if (IsMouseOverPreviousButton)
                {
                    ActiveAButtonText = "Previous";
                    ActiveHelpText = CachedHelpText;
                }
                else if(IsMouseOverNextButton)
                {
                    ActiveAButtonText = "Next";
                    ActiveHelpText = CachedHelpText;
                }
                else if (IsMouseOverTextButton)
                {
                    ActiveAButtonText = CachedAButtonText;
                    ActiveHelpText = CachedHelpText;
                }
            }
            else
            { 
                AButtonText = CachedAButtonText;
                HelpText = CachedHelpText;
            }
        }

        private bool IsMouseOverPreviousButton
        {
            get
            {
                var r = Bounds;

                r.Right = r.Left + ButtonSize.X;
                return r.Contains(Game.MenuManager.MouseWorldPosition);
            }
        }

        private bool IsMouseOverNextButton
        {
            get
            {
                var r = Bounds;

                switch (LayoutStyle)
                {
                    case LayoutStyleType.ButtonButtonText:
                        r.Left += ButtonSize.X + SpacerWidth;
                        r.Right = r.Left + ButtonSize.X;
                        return r.Contains(Game.MenuManager.MouseWorldPosition);

                    case LayoutStyleType.ButtonTextButton:
                    default:
                        r.Left = r.Right - ButtonSize.X;
                        return r.Contains(Game.MenuManager.MouseWorldPosition);
                }
            }
        }

        private bool IsMouseOverTextButton
        {
            get
            {
                var r = Bounds;

                switch (LayoutStyle)
                {
                    case LayoutStyleType.ButtonButtonText:
                        r.Left += (ButtonSize.X + SpacerWidth) * 2;
                        return r.Contains(Game.MenuManager.MouseWorldPosition);

                    case LayoutStyleType.ButtonTextButton:
                    default:
                        r.Left += ButtonSize.X + SpacerWidth;
                        r.Right = r.Left + MeasureTextLength;
                        return r.Contains(Game.MenuManager.MouseWorldPosition);
                }
            }
        }

        float MeasureTextLength
        {
            get
            {
                var textLength = Font.GetTextLength(SelectedText, FontSize.X);

                return Math.Max(MinTextWidth, textLength);
            }
        }


		/// <summary>
		/// .
		/// </summary>
        protected override void OnDraw()
		{
		    var halfButtonSize = ButtonSize / 2.0f;
            
		    var tint = EffectiveTint;

            var bounds = Bounds;

            var location = new Vector2(bounds.Left + halfButtonSize.X, bounds.Bottom + ((bounds.Top - bounds.Bottom) / 2.0f));

            DrawPreviousButton(location, tint);

            switch (LayoutStyle)
            {
                case LayoutStyleType.ButtonButtonText:
                    //Draw next button
                    location.X += ButtonSize.X + SpacerWidth;
                    DrawNextButton(location, tint);

                    //Draw the text
                    //location.Y -= 0.05f;
                    location.X += ButtonSize.X + SpacerWidth - halfButtonSize.X;
                    DrawText(location, tint, Alignment.CentreLeft, measureText: false);
                    break;

                case LayoutStyleType.ButtonTextButton:
                default:
                    //Draw the text
                    //location.Y -= 0.05f;
                    var l = location.X;
                    location.X = bounds.Left + (bounds.Width / 2.0f);
                    var length = DrawText(location, tint, Alignment.Centre, measureText: true);

                    //Draw next button
                    location.X = l + halfButtonSize.X + SpacerWidth + length + SpacerWidth + halfButtonSize.X;
                    DrawNextButton(location, tint);
                    break;
            }
		}

        void DrawPreviousButton(Vector2 location, Color tint)
        {
            //Draw previous button
            var q = new Quad(location, ButtonSize, 0.0f);
            Game.Renderer.DrawSprite(ref q, PreviousImageInfo.TextureId, PreviousImageInfo.SourceRect, tint * (SelectedIndex > 0 ? 1.0f : InactiveAlphaModifier), RenderStyle, Layer);
        }

        void DrawNextButton(Vector2 location, Color tint)
        {
            //Draw next button
            var q = new Quad(location, ButtonSize, 0.0f);
            Game.Renderer.DrawSprite(ref q, NextImageInfo.TextureId, NextImageInfo.SourceRect, tint * (SelectedIndex < Count - 1 ? 1.0f : InactiveAlphaModifier), RenderStyle, Layer);
        }

        float DrawText(Vector2 location, Color tint, Alignment alignment, bool measureText)
        {
            //Draw the text
            if (SelectedIndex <= -1) 
                return 0.0f;

            Game.Renderer.DrawString(Font, location, FontSize, _options[SelectedIndex], alignment, tint, RenderStyle, Layer);

            if (measureText == false)
                return 0.0f;

            return MeasureTextLength;
        }

		/// <summary>
		/// Adds an option to the list.
		/// </summary>
		/// <param name="value">The value to add.</param>
		public void AddOption(string value)
		{
			_options.Add(value);
		}
		
		/// <summary>
		/// Adds an option to the list.
		/// </summary>
		/// <param name="value">The value to add.</param>
		public void AddOption(int value)
		{
			_options.Add(value.ToString(CultureInfo.InvariantCulture));
		}
		
        /// <summary>
        /// Adds no and yes options to the list.
        /// </summary>
        public void AddBooleanOptions()
        {
            _options.Add("no");
            _options.Add("yes");
        }

		/// <summary>
		/// Clears all the options from the list.
		/// </summary>
		public void ClearOptions()
		{
			SelectedIndex = -1;
			_options.Clear();
		}
		
		/// <summary>
		/// Sets the selected item in the list.
		/// </summary>
		/// <param name="textToFind">The text used to perform the match.</param>
		public void SetSelectedItemByText(string textToFind)
		{			
			var index = _options.FindIndex(optionText => (optionText == textToFind));
			if (index > -1)
				SelectedIndex = index;
		}

        /// <summary>
        /// Sets the selected item in the list.
        /// </summary>
        /// <param name="value">A value indicating the item in the list to set.</param>
        public void SetSelectedItemByBoolean(bool value)
        {
            SelectedIndex = value == false ? 0 : 1;
        }

        protected virtual void RaiseTextButtonClicked(EventArgs e)
		{		
			//Now raise the event to any subscribers
			//Note the lock to prevent race condition (needed in XNA?)
            var handler = TextButtonClicked;

            if (handler != null)
                handler(this, e);
		}
    }
}
