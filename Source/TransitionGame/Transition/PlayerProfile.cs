using System.Collections.Generic;
using Newtonsoft.Json;
using Transition.Achievements;
using Transition.Input;
using Transition.Storage;

namespace Transition
{
    public enum ProfileType
    {
        BuiltIn,
        Local,
        Xbox360
    }

    /// <summary>
    /// Responsible for storing information about a player profile.
    /// </summary>
    public class PlayerProfile
    {
        /// <summary>Gets or sets the name of the profile.</summary>
        [JsonProperty(Order = 1)]
        public string Name { get; set; }

        /// <summary>Gets or sets the file name of the profile.</summary>
        [JsonProperty(Order = 2)]
        public string FileName { get; set; }

        /// <summary>Gets or sets the guid for the profile.</summary>
        [JsonProperty(Order = 3)]
        public string Guid { get; set; }

        [JsonProperty(Order = 4)]
        public ProfileType ProfileType { get; set; }

        [JsonProperty(Order = 5)]
        public string SelectedShipName { get; set; }

        [JsonProperty(Order = 6)]
        public readonly Keyset InputKeys = new Keyset();

        /// <summary>A list of achievements.</summary>
        /// <remarks>This is all the achievements available - some may not have been gained yet.</remarks>
        [JsonProperty(Order = 7)]
        public List<CompletedAchievement> CompletedAchievements { get; set; }

        [JsonRequired]
        [JsonProperty("Checkpoints", Order = 8)]
        readonly List<Checkpoint> _checkpoints = new List<Checkpoint>();

        /// <summary>
        /// A list of checkpoints the player has reached.
        /// </summary>
        [JsonIgnore]
        public IEnumerable<IReadableCheckpoint> Checkpoints => _checkpoints;

        /// <summary>Whether the profile requires saving.</summary>
        [JsonIgnore]
        public bool RequiresSave { get; set; }

        public PlayerProfile()
        {
            CompletedAchievements = new List<CompletedAchievement>();
 
            ProfileType = ProfileType.Local;
            SelectedShipName = "Ship1";
        }

        public void AddCheckpoint(Checkpoint checkpoint)
        {
            RequiresSave = true;
            _checkpoints.Add(checkpoint);
        }

        public IReadableCheckpoint GetCheckpoint(int checkpointId)
        {
            return _checkpoints[checkpointId];
        }

        /// <summary>
        /// Saves the profile.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public void Save(BaseGame game)
        {
            game.StorageManager.Save(this, FileName, format: StorageManager.Format.Json);

            RequiresSave = false;
        }
    }
}
