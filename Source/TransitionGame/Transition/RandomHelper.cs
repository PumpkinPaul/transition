using System;

namespace Transition
{
    /// <summary>
    /// Helper class to generate random numbers.
    /// </summary>
    public static class RandomHelper
    {
        public static Random DeterministicRandom = new Random();
        public static Random Random = new Random();
        public static FastRandom FastRandom = new FastRandom(DateTime.Now.Millisecond);
    }
}

