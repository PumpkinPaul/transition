using System;

using Microsoft.Xna.Framework;
using Transition.Rendering;

namespace Transition
{
    /// <summary>
    /// Represents the and individual layer of stars
    /// </summary>
    public class StarFieldLayer
    {
        /// <summary>An instance of the game.</summary>
        private readonly TransitionGame _game;

        /// <summary>An instance of the game.</summary>
        private Rect _bounds;

        private readonly float _starFieldwidth;
                
        /// <summary>The number of stars making up the layer.</summary>
        private readonly int _starCount;

        /// <summary>The number of stars making up the layer.</summary>
        private readonly float _starSize;

        //Arrays of stuff for rendering.
        private readonly Quad[] _stars;

        private readonly Color[] _colors;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="game"></param>
        /// <param name="bounds"></param>
        /// <param name="stars"></param>
        /// <param name="starSize"></param>
        /// <param name="bottomGutter"></param>
        /// <param name="topGutter"></param>
        public StarFieldLayer(TransitionGame game, Rect bounds, int stars, float starSize, float bottomGutter, float topGutter)
        {
            _game = game;
            _bounds = bounds;
            _starCount = stars;
            _starSize = starSize;
            
            _starFieldwidth = Math.Abs(bounds.Left) + bounds.Right;

            _stars = new Quad[_starCount * 4];
            _colors = new Color[_starCount];

            InitialiseStars(bottomGutter, topGutter);
        }

        /// <summary>
        /// Initialises the render points - these points share position info with the main points array
        /// but they are VertexPositionColour items that are specifically required for drawing.
        /// </summary>
        private void InitialiseStars(float bottomGutter, float topGutter)
        {
            //Fill the buffer
            var idx = 0;
            for (var i = 0; i < _starCount; ++i)
            {
                var position = new Vector2(RandomHelper.Random.GetFloat(-20.0f, 20.0f), RandomHelper.Random.GetFloat(_bounds.Bottom + bottomGutter, _bounds.Top + topGutter));
                var quad = new Quad(position, new Vector2(_starSize), MathHelper.PiOver4);

                _stars[idx++] = quad;

                _colors[i] = Microsoft.Xna.Framework.Color.Gray;
            }
        }

        public Color Color
        {
            set 
            {
                for (var i = 0; i < _starCount; ++i)
                    _colors[i] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Update(float force)
        {
            //Scroll the stars
            UpdateStars(force);
        }

          /// <summary>
        /// Saves the positions of the star points
        /// </summary>
        /// <param name="force"></param>
        private void UpdateStars(float force)
        {
            force *= 0.5f;

            for (var i = 0; i < _starCount; ++i)
            {
                _stars[i].Move(force, 0);

                //Wrap the stars
                if (_stars[i].TopRight.X < _bounds.Left)
                {
                    _stars[i].Move(_starFieldwidth, 0);
                }
                else if (_stars[i].TopLeft.X > _bounds.Right)
                {
                    _stars[i].Move(-_starFieldwidth, 0);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Draw()
        {
            var spriteImage = _game.TextureManager.GetTextureInfo("ParticleSquareAlphaBorder");

            for(var i = 0; i < _starCount; i++)
            {
                var q = _stars[i];
                _game.Renderer.DrawSprite(ref q, spriteImage.TextureId, spriteImage.SourceRect, _colors[i], TransparentRenderStyle.Instance);
            }
        }
    }
}
