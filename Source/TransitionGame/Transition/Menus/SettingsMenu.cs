using System;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the settings menu.
    /// </summary>
    public class SettingsMenu : Menu
    {
        public static bool ToggleFullscreen;
        
        TextStripControl _textStripControl;

        //Audio
        TextLabelControl _soundLabel;
        TextLabelControl _musicLabel;

        SliderControl _soundSlider;        
        SliderControl _musicSlider;

        //Video
        TextLabelControl _displayLabel;
        TextLabelControl _fullscreenLabel;
        TextLabelControl _screenSizeLabel;
        TextLabelControl _trailEffectsLabel;
        TextLabelControl _crtEffectsLabel;
        TextLabelControl _bloomLabel;

        ListControl _displayList;
        CheckBoxControl _fullscreenCheckBox;
        SliderControl _screenSizeSlider;
        CheckBoxControl _trailEffectsCheckBox;
        CheckBoxControl _crtEffectsCheckBox;
        CheckBoxControl _bloomCheckBox;

        //Prefs
        TextLabelControl _vibrationLabel;
        TextLabelControl _recordDemoLabel;
        TextLabelControl _replayLabel;
   
        CheckBoxControl _vibrationCheckBox;
        CheckBoxControl _recordDemoCheckBox;
        ListControl _replayModeList;
        
        /// <summary>Indicates the selected index of the display list when the menu is added to the manager.</summary>
        int _initialDisplayIndex;

        /// <summary>Indicates the selected index of the full screen list when the menu is added to the manager.</summary>
        bool _initialFullscreenChecked;

        /// <summary>
        /// Initialises a new instance of a SettingsMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
        public SettingsMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            _textStripControl = (TextStripControl)ControlMap["textStripControl"];
            _textStripControl.IncreaseValue += TextStripControlValueChanged;
            _textStripControl.DecreaseValue += TextStripControlValueChanged;
            _textStripControl.MouseUp += TextStripControlMouseUp;

            //Audio
            _soundLabel = (TextLabelControl)ControlMap["soundLabel"];
            _musicLabel = (TextLabelControl)ControlMap["musicLabel"];

            _soundSlider = (SliderControl)ControlMap["soundSlider"];
            _soundSlider.IncreaseValue += SfxSliderValueChanged;
            _soundSlider.DecreaseValue += SfxSliderValueChanged;

            _musicSlider = (SliderControl)ControlMap["musicSlider"];
            _musicSlider.IncreaseValue += MusicSliderValueChanged;
            _musicSlider.DecreaseValue += MusicSliderValueChanged;

            //Video
            _displayLabel = (TextLabelControl)ControlMap["displayLabel"];
            _fullscreenLabel = (TextLabelControl)ControlMap["fullscreenLabel"];
            _screenSizeLabel = (TextLabelControl)ControlMap["screenSizeLabel"];
            _trailEffectsLabel = (TextLabelControl)ControlMap["trailEffectsLabel"];
            _crtEffectsLabel = (TextLabelControl)ControlMap["crtEffectsLabel"];
            _bloomLabel = (TextLabelControl)ControlMap["bloomLabel"];

            _displayList = (ListControl)ControlMap["displayList"];
            
            _fullscreenCheckBox = (CheckBoxControl)ControlMap["fullscreenCheckBox"];
            _fullscreenCheckBox.Click += FullScreenListValueChanged;

            _screenSizeSlider = (SliderControl)ControlMap["screenSizeSlider"];
            _screenSizeSlider.IncreaseValue += ScreenSizeSliderValueChanged;
            _screenSizeSlider.DecreaseValue += ScreenSizeSliderValueChanged;

            _trailEffectsCheckBox = (CheckBoxControl)ControlMap["trailEffectsCheckBox"];
            _trailEffectsCheckBox.Click += TrailEffectsListValueChanged;

            _crtEffectsCheckBox = (CheckBoxControl)ControlMap["crtEffectsCheckBox"];
            _crtEffectsCheckBox.Click += CrtEffectsListValueChanged;

            _bloomCheckBox = (CheckBoxControl)ControlMap["bloomCheckBox"];
            _bloomCheckBox.Click += BloomListValueChanged;

            //Prefs
            _vibrationLabel = (TextLabelControl)ControlMap["vibrationLabel"];

            _vibrationCheckBox = (CheckBoxControl)ControlMap["vibrationCheckBox"];
            _vibrationCheckBox.Click += VibrationListValueChanged;

            _recordDemoLabel = (TextLabelControl)ControlMap["recordDemoLabel"];

            _recordDemoCheckBox = (CheckBoxControl)ControlMap["recordDemoCheckBox"];
            _recordDemoCheckBox.Click += RecordDemoCheckBoxValueChanged;

            _replayLabel = (TextLabelControl)ControlMap["replayLabel"];

            _replayModeList = (ListControl)ControlMap["replayModeList"];
            _replayModeList.IncreaseValue += ReplayModeListChanged;
            _replayModeList.DecreaseValue += ReplayModeListChanged;
            _replayModeList.SelectedIndex = 1;

            TextStripControlValueChanged(_textStripControl, EventArgs.Empty);

            SetValuesFromOptions();
        }

        /// <summary>
        /// Occurs when a menu is added to the MenuManager.
        /// </summary>
        protected override void OnAddedToManager()
        {
            if (ToggleFullscreen)
            {
                _fullscreenCheckBox.Value = !_fullscreenCheckBox.Value;
                ToggleFullscreen = false;
            }

            SetValuesFromOptions();

            //Save these so we know if they have changed when we exit the screen
            _initialDisplayIndex = _displayList.SelectedIndex;
            _initialFullscreenChecked = _fullscreenCheckBox.Value;
        }

        private void TextStripControlValueChanged(object sender, EventArgs e)
        {
            ShowControls();
        }

        private void TextStripControlMouseUp(object sender, EventArgs e)
        {
            ShowControls();
        }

        private void ShowControls()
        {
            if (_textStripControl.SelectedIndex == 0)
            {
                //Audio
                _soundLabel.Visible = true;
                _musicLabel.Visible = true;
                _soundSlider.Visible = true;
                _musicSlider.Visible = true;

                _displayLabel.Visible = false;
                _fullscreenLabel.Visible = false;
                _screenSizeLabel.Visible = false;
                _trailEffectsLabel.Visible = false;
                _crtEffectsLabel.Visible = false;
                _bloomLabel.Visible = false;
                _displayList.Visible = false;
                _fullscreenCheckBox.Visible = false;
                _screenSizeSlider.Visible = false;
                _trailEffectsCheckBox.Visible = false;
                _crtEffectsCheckBox.Visible = false;
                _bloomCheckBox.Visible = false;

                _vibrationLabel.Visible = false;
                _vibrationCheckBox.Visible = false;
                _recordDemoLabel.Visible = false;
                _recordDemoCheckBox.Visible = false;
                _replayLabel.Visible = false;
                _replayModeList.Visible = false;
            }
            else if (_textStripControl.SelectedIndex == 1)
            {
                //Video
                _soundLabel.Visible = false;
                _musicLabel.Visible = false;
                _soundSlider.Visible = false;
                _musicSlider.Visible = false;

                _displayLabel.Visible = true;
                _fullscreenLabel.Visible = true;
                _screenSizeLabel.Visible = true;
                _screenSizeSlider.Visible = true;
                _trailEffectsLabel.Visible = true;
                _crtEffectsLabel.Visible = true;
                _bloomLabel.Visible = true;
                _displayList.Visible = true;
                _fullscreenCheckBox.Visible = true;
                _trailEffectsCheckBox.Visible = true;
                _crtEffectsCheckBox.Visible = true;
                _bloomCheckBox.Visible = true;

                _vibrationLabel.Visible = false;
                _vibrationCheckBox.Visible = false;
                _recordDemoLabel.Visible = false;
                _recordDemoCheckBox.Visible = false;
                _replayLabel.Visible = false;
                _replayModeList.Visible = false;
            }
            else if (_textStripControl.SelectedIndex == 2)
            {
                //Prefs
                _soundLabel.Visible = false;
                _musicLabel.Visible = false;
                _soundSlider.Visible = false;
                _musicSlider.Visible = false;

                _displayLabel.Visible = false;
                _fullscreenLabel.Visible = false;
                _screenSizeLabel.Visible = false;
                _trailEffectsLabel.Visible = false;
                _crtEffectsLabel.Visible = false;
                _bloomLabel.Visible = false;
                _displayList.Visible = false;
                _fullscreenCheckBox.Visible = false;
                _screenSizeSlider.Visible = false;
                _trailEffectsCheckBox.Visible = false;
                _crtEffectsCheckBox.Visible = false;
                _bloomCheckBox.Visible = false;

                _vibrationLabel.Visible = true;
                _vibrationCheckBox.Visible = true;
                _recordDemoLabel.Visible = true;
                _recordDemoCheckBox.Visible = true;
                _replayLabel.Visible = true;
                _replayModeList.Visible = true;
            }
        }

        /// <summary>
        /// Called when the menu has finished its closing state.
        /// </summary>
        protected override void OnClosed()
        {
            //Set display if properties changed.
            var applyChanges = false;
            if (_initialDisplayIndex != _displayList.SelectedIndex)
            {
                var resolution = _displayList.SelectedText.Split('x');
                Game.Graphics.PreferredBackBufferWidth = Convert.ToInt32(resolution[0].Trim());
                Game.Graphics.PreferredBackBufferHeight = Convert.ToInt32(resolution[1].Trim());
                applyChanges = true;
            }

            if (_initialFullscreenChecked != _fullscreenCheckBox.Value)
            {
                Game.Graphics.ToggleFullScreen();
                applyChanges = true;
            }

            if (applyChanges)
            {
                Game.Graphics.ApplyChanges();
                Game.ResolutionsChanged();
            }

            SaveOptions();
        }

        /// <summary>
        /// Sets the control values from game options.
        /// </summary>
        private void SetValuesFromOptions()
        {
            _musicSlider.SetValueFromNormalizedValue(Game.GameOptions.MusicVolume);
            _soundSlider.SetValueFromNormalizedValue(Game.GameOptions.SfxVolume);

            _crtEffectsCheckBox.Value = Game.GameOptions.CrtEffects;
            _bloomCheckBox.Value = Game.GameOptions.Bloom;

            _displayList.SetSelectedItemByText(Game.GameOptions.Width + " x " + Game.GameOptions.Height);
            _fullscreenCheckBox.Value = Game.GameOptions.FullScreen;
            _trailEffectsCheckBox.Value = Game.GameOptions.TrailEffects;
            _screenSizeSlider.Value = (int)Game.GameOptions.ScreenSize * 100;

            _vibrationCheckBox.Value = Game.GameOptions.Vibration;
            _recordDemoCheckBox.Value = Game.GameOptions.RecordDemo;
            _replayModeList.SelectedIndex = (int)Game.GameOptions.ReplayPlaybackMode;
        }

        /// <summary>
        /// Save the options (in memory and to file).
        /// </summary>
        private void SaveOptions()
        {
            Game.GameOptions.Width = Game.Graphics.PreferredBackBufferWidth;
            Game.GameOptions.Height = Game.Graphics.PreferredBackBufferHeight;
            Game.GameOptions.Save(Game);
        }

        /// <summary>
        /// Event handler for the Sfx slider value changed events.
        /// </summary>
        private void SfxSliderValueChanged(object sender, EventArgs e)
        {
            Game.SoundManager.SfxVolume = _soundSlider.NormalizedValue;
            Game.GameOptions.SfxVolume = _soundSlider.NormalizedValue;
        }

        /// <summary>
        /// Event handler for the music slider value changed events.
        /// </summary>
        private void MusicSliderValueChanged(object sender, EventArgs e)
        {
            Game.SoundManager.MusicVolume = _musicSlider.NormalizedValue;
            Game.GameOptions.MusicVolume = _musicSlider.NormalizedValue;
        }

        /// <summary>
        /// Event handler for the vibration list value changed events.
        /// </summary>
        private void VibrationListValueChanged(object sender, EventArgs e)
        {
            Game.GameOptions.Vibration = _vibrationCheckBox.Value;
            Game.VibrationChanged();
        }

        /// <summary>
        /// Event handler for the vibration list value changed events.
        /// </summary>
        private void RecordDemoCheckBoxValueChanged(object sender, EventArgs e)
        {
            Game.GameOptions.RecordDemo = _recordDemoCheckBox.Value;
        }

        /// <summary>
        /// Event handler for the vibration list value changed events.
        /// </summary>
        private void ReplayModeListChanged(object sender, EventArgs e)
        {
            Game.GameOptions.ReplayPlaybackMode = (ReplayPlaybackMode)_replayModeList.SelectedIndex;

            ((TransitionGame)Game).DemoFinished();
        }
        
        /// <summary>
        /// Event handler for the crt effects list value changed events.
        /// </summary>
        private void CrtEffectsListValueChanged(object sender, EventArgs e)
        {
            Game.GameOptions.CrtEffects = _crtEffectsCheckBox.Value;
            Game.CrtEffectChanged();
        }

        /// <summary>
        /// Event handler for the bloom list value changed events.
        /// </summary>
        private void BloomListValueChanged(object sender, EventArgs e)
        {
            Game.GameOptions.Bloom = _bloomCheckBox.Value;
            Game.BloomChanged();
        }
        
        /// <summary>
        /// Event handler for the full screen list value changed events.
        /// </summary>
        private void FullScreenListValueChanged(object sender, EventArgs e)
        {
            Game.GameOptions.FullScreen = _fullscreenCheckBox.Value;
        }

        /// <summary>
        /// Event handler for the trail effects list value changed events.
        /// </summary>
        private void TrailEffectsListValueChanged(object sender, EventArgs e)
        {
            Game.GameOptions.TrailEffects = _trailEffectsCheckBox.Value;
        }
		
        /// <summary>
        /// Event handler for the screen size slider value changed events.
        /// </summary>
        private void ScreenSizeSliderValueChanged(object sender, EventArgs e)
        {
            Game.GameOptions.ScreenSize = _screenSizeSlider.NormalizedValue;
            Game.ViewportZoom = _screenSizeSlider.NormalizedValue;
        }
    }
}