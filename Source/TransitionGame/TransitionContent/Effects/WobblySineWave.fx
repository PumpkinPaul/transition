float4x4 World;
float4x4 View;
float4x4 Projection;
uniform extern texture UserTexture;

sampler baseSampler = sampler_state
{
    Texture = <UserTexture>;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    ADDRESSU = CLAMP;
    ADDRESSV = CLAMP;
};

// TODO: add effect parameters here.
 
struct VS_IN
{
    float4 Position : POSITION;
    float2 TextureCoords : TEXCOORD0;
    float4 Colour : COLOR;
};

struct VS_OUT
{
    float4 ScreenPosition : POSITION;
    float2 TextureCoords : TEXCOORD0;
    float4 Colour : COLOR;
};


VS_OUT VertexShaderFunction(VS_IN input)
{
    VS_OUT output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.ScreenPosition = mul(viewPosition, Projection);
    output.TextureCoords = input.TextureCoords;
    output.Colour = input.Colour;

    return output;
}









float WAVES = 2.0;

float time = 1.0f;
float4x4 MatrixTransform;
float2 resolution = {854.0, 480.0 };

void SpriteVertexShader(inout float4 color : COLOR0, inout float2 texCoord : TEXCOORD0, inout float4 position : SV_Position)
{
    position = mul(position, MatrixTransform);
}

#define A .1 // Amplitude
#define V 8. // Velocity
#define W 3. // Wavelength
#define T .1 // Thickness
#define S 3. // Sharpness

float sine(float2 p, float o)
{
    return pow(T / abs((p.y + sin((p.x * W + o)) * A)), S);
}

float4 PixelShaderFunction(float4 pos : TEXCOORD) : COLOR0
{		
    float2 uv = -1.0 + 2.0 * pos.xy;
    uv.y *= 3.0;

    float time2 = fmod(time * 20.0, 1000.0);
    	
    float3 color = float3(0.0, 0.0, 0.0);

    for (float i = 0.0; i < WAVES + 1.0; i++)
    {
        //float i = 1.0;
        float freq = 1.0 * 4.0;

        float2 p = float2(uv);

        p.x += i * 0.04 + freq * 0.03;
        p.y += sin(p.x * 10.0 + time2) * cos(p.x * 2.0) * freq * 0.2 * ((i + 1.0) / WAVES);
        float intensity = abs(0.01 / p.y) * clamp(freq, 0.35, 2.0);
        color += float3(12.0 * intensity * (i / 5.0), 1.0 * intensity, 0.25 * intensity) * (3.0 / WAVES);
    }

    return float4(color, 0.0);
}

technique SineWave
{
	pass P0
	{
		#if SM4
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
		#elif SM2
		PixelShader = compile ps_3_0 PixelShaderFunction();
		#else
        //VertexShader = compile vs_3_0 SpriteVertexShader();
        VertexShader = compile vs_3_0 VertexShaderFunction();
		PixelShader = compile ps_3_0 PixelShaderFunction();
		#endif
	}
}