using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.Menus;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control for {DECRIPTION HERE}
    /// </summary>
    public class GroupingControl : MenuControl
    {
        /// <summary>The size of the image in world units.</summary>
        public Vector2 Size;

        public Color FocusTint;

        public override bool AutoTabIndex { get { return true; } }

        /// <summary>Initialises a new instance of an ImageControl.</summary>
        /// <param name = "game">A reference to the game.</param>
        public GroupingControl(BaseGame game) : base(game) 
        {
            Size = Vector2.One;
            Tint = Color.White;
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            Size = element.GetAttributeVector2("size");
            FocusTint = element.GetAttributeColor("focusTint", FocusTint);
        }

        /// <summary>
        ///   Gets the control bounds (a rectangle defining the control's limits).
        /// </summary>
        public override Rect Bounds
        {
            get { return new Rect(Position - (Size / 2.0f), Size); }
        }


        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnUpdate()
        {
            
        }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
        {
            var bounds = Bounds;

            var tint = Tint * Overlay.Opacity;
            var focusTint = FocusTint * Overlay.Opacity;

            var q = new Quad(bounds);
            Game.Renderer.DrawSprite(ref q, Game.Renderer.PixelTextureInfo.TextureId, Game.Renderer.PixelTextureInfo.SourceRect, tint, RenderStyle, Layer);

            if (Game.MenuManager.FocusControl == null)
                return;

            if (Children.Contains(Game.MenuManager.FocusControl) == false)
                return;

            var rect = Game.MenuManager.FocusControl.Bounds;
            rect.Left = bounds.Left + 0.45f;
            rect.Right = bounds.Right - 0.45f;
            q = new Quad(rect);
            Game.Renderer.DrawSprite(ref q, Game.Renderer.PixelTextureInfo.TextureId, Game.Renderer.PixelTextureInfo.SourceRect, focusTint, RenderStyle, Layer + 1);
        }
    }
}