using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Transition.Animations;

namespace Transition.ActorDatas
{
	/// <summary>
	/// Configuration data for actors.
	/// </summary>
    public class ActorData
    {
        public class Layer
        {
            public IAppearance Appearance;    
            public int DrawLayer;
        }

        public Type ActorType;
        public string Name;
		public Layer[] Layers;
        public int InitialHealth;
        public float CollisionRadius;
        public float SpawnScaleIncrement;
        public string ExplosionSfx;
		public Vector2 BaseSize;
		public List<ParticleEffect> SpawnEffects = new List<ParticleEffect>();
		public List<ParticleEffect> TrailEffects = new List<ParticleEffect>();
        public List<ParticleEffect> ExpiredEffects = new List<ParticleEffect>();
		public List<ParticleEffect> ExplosionEffects = new List<ParticleEffect>();
        public SimpleTrailEffectData SimpleTrail;
    }
}
