using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Transition.Cameras;
using Transition.CourseSystem;
using Transition.Input;
using Transition.LevelEditing.Tools;
using Transition.MenuControls;
using Transition.Menus;
using Transition.Rendering;
using Color = Microsoft.Xna.Framework.Color;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace Transition.LevelEditing
{
    /// <summary>
    /// Represents a control for displaying the 'city being attacked' warning.
    /// </summary>
    public class LevelEditor
    {
        public event EventHandler CourseInitialised;
        public event EventHandler CourseSaved;

        readonly TransitionGame _game;

        public class StateEntry
        {
            public string State;
            public string Action;

            public override string ToString() 
            {
                return Action ?? base.ToString();
            }
        }

        public LevelEditorState LevelEditorState { get; private set; }
        readonly Clipboard _clipboard;

        public enum ToolboxItem
        {
            CanvasPanning = 0,
            SelectAndTransform,
            Tiles,
            Text,
            CameraPath,
            ColorPicker,
            Actors
        }

        public enum SubToolItem
        {

        }

        ToolboxItem _toolboxItem = ToolboxItem.CanvasPanning;

        public int SelectedTileId
        {
            get { return _tileTool.SelectedTileId; }
            set { _tileTool.SelectedTileId = value; }
        }

        public Camera Camera { get; private set; }
        public Course Course 
        {
            get { return LevelEditorState != null ? LevelEditorState.Course : null; }
            private set { LevelEditorState.Course = value; }
        }

        public bool CameraLayerVisible 
        { 
            get { return Course != null && Course.CameraLayerVisible; }
            set 
            { 
                if (Course != null)
                    Course.CameraLayerVisible = value;
            } 
        }

        public bool DecalLayerVisible 
        { 
            get { return Course != null && Course.DecalLayerVisible; }
            set 
            { 
                if (Course != null)
                    Course.DecalLayerVisible = value;
            } 
        }

        public bool TextLayerVisible 
        { 
            get { return Course != null && Course.TextLayerVisible; }
            set 
            { 
                if (Course != null)
                    Course.TextLayerVisible = value;
            } 
        }

        public bool TilesLayerVisible 
        { 
            get { return Course != null && Course.TilesLayerVisible; }
            set 
            { 
                if (Course != null)
                    Course.TilesLayerVisible = value;
            } 
        }

        public bool ActorsLayerVisible 
        { 
            get { return Course != null && Course.ActorsLayerVisible; }
            set 
            { 
                if (Course != null)
                    Course.ActorsLayerVisible = value;
            } 
        }

        public bool Visible { get; private set; }

        public Vector2 WorldPosition { get; private set; }

        //Text layer item properties
        internal float ObjectRotation { get; private set; }
        internal Vector2 ObjectScale { get; private set; }

        public int EditorStateId { get; private set; }
        readonly List<StateEntry> _editorStates = new List<StateEntry>();

        public IEnumerable<StateEntry> StateBuffer { get { return _editorStates; } }

        //Grid
        public DesignGrid DesignGrid { get; private set; }

        //Tools
        Tool _activeTool;
        SelectionTool _selectionTool;
        PanningTool _panningTool;
        TileTool _tileTool;
        LabelTool _labelTool;
        CameraPathTool _pathTool;
        PickerTool _pickerTool;
        ActorsTool _actorsTool;

        bool _canSelectTiles;
        bool _canSelectNodes;
        bool _canSelectLabels;
        
        public bool CanSelectTiles 
        { 
            get { return _canSelectTiles; }
            set 
            {
                _canSelectTiles = value;
                if (value == false && LevelEditorState.SelectedTileIds.Count > 0)
                {
                    LevelEditorState.SelectedTileIds.Clear();
                    SaveEditorState("Clear selection");
                }
            }
        }
        
        public bool CanSelectLabels 
        { 
            get { return _canSelectLabels; }
            set 
            {
                _canSelectLabels = value;
                if (value == false && LevelEditorState.SelectedLabelIds.Count > 0)
                {
                    LevelEditorState.SelectedLabelIds.Clear();
                    SaveEditorState("Clear selection");
                }
            }
        }

        public bool CanSelectNodes 
        { 
            get { return _canSelectNodes; }
            set 
            {
                _canSelectNodes = value;
                if (value == false && LevelEditorState.SelectedNodeIds.Count > 0)
                {
                    LevelEditorState.SelectedNodeIds.Clear();
                    SaveEditorState("Clear selection");
                }
            }
        }

        public bool IsDirty { get; private set; }

        public string LabelText { get; set; }

        public LevelEditor(TransitionGame game)
        {
            _game = game;

            CreateTools();

            ObjectColor = Color.White;
            SetNormalizedTextSize(0);

            Camera = new Camera(game);
            Camera.Position.Z = -50f;
            Camera.Initialise();
            game.CameraManager.AddCamera(Camera);

            DesignGrid = new DesignGrid();
            _clipboard = new Clipboard(this);
        }

        void CreateTools()
        {
            _activeTool = _selectionTool = new SelectionTool(_game);
            _panningTool = new PanningTool(_game);
            _tileTool = new TileTool(_game);
            _labelTool = new LabelTool(_game);
            _pathTool = new CameraPathTool(_game); 
            _pickerTool = new PickerTool(_game);
            _actorsTool = new ActorsTool(_game);
        }

        public void InitialiseCourse(Course course)
        {
            LevelEditorState = new LevelEditorState();

            _editorStates.Clear();
            EditorStateId = -1;

            Course = course;
            SaveEditorState("Initialise Level");

            //Tools
            CreateTools();

            IsDirty = false;

            OnCourseInitialised(EventArgs.Empty);
        }

        public void Show()
        {
            if (Visible)
                return;

            Visible = true;

            _game.MenuManager.AddMenu(typeof(LevelEditorMenu));
        }

        public void Hide()
        {
            if (Visible == false)
                return;

            var menu = _game.MenuManager.GetMenu(typeof(LevelEditorMenu));
            menu.Close();

            Visible = false;
        }

        public void ToggleVisibility()
        {
            if (Visible)
                Hide();
            else
                Show();
        }

        public Color ObjectColor { get; set; }

        public void ApplyBackgroundColor(Color value, bool saveState)
        {
            Course.BackgroundColor = value;

            if (LevelEditorState == null)
                return;

            if (saveState)
                SaveEditorState("Set background colour");
        }

        public void ApplyObjectColorToSelection(Color value, bool saveState)
        {
            ObjectColor = value;

            if (LevelEditorState == null)
                return;

            if (LevelEditorState.SelectedTileIds.Count > 0 || LevelEditorState.SelectedLabelIds.Count > 0)
            {
                LevelEditorState.SelectedTileIds.ForEach(id => GetTileByIndex(id).Color = value);
                LevelEditorState.SelectedLabelIds.ForEach(id => GetLabelByIndex(id).Color = value);

                if (saveState)
                    SaveEditorState("Set object colour");
            }
        }

        internal Tile GetTileByIndex(int id)
        {
            return Course.Tiles[id];
        }

        internal Label GetLabelByIndex(int id)
        {
            return Course.Labels[id];
        }

        internal Vector2 GetCameraNodeByIndex(int id)
        {
            return Course.CameraPath.Vertices[id];
        }

        internal Vector2 ObjectPosition
        {
            get 
            {
                //Depends on grid snapping!

                if (DesignGrid.Active == false)
                    return WorldPosition;
                
                var halfStep = DesignGrid.Step / 2.0f;
                var wp = WorldPosition + new Vector2(Math.Sign(WorldPosition.X) * halfStep, Math.Sign(WorldPosition.Y) * halfStep);

                return new Vector2(wp.X - wp.X % DesignGrid.Step, wp.Y - wp.Y % DesignGrid.Step);
            }
        }

        public void SetToolboxItem(ToolboxItem toolboxItem)
        {
            _toolboxItem = toolboxItem;

            switch(_toolboxItem)
            {
                case ToolboxItem.SelectAndTransform:
                    _activeTool = _selectionTool;
                    break;

                case ToolboxItem.CanvasPanning:
                    _activeTool = _panningTool;
                    break;

                case ToolboxItem.Tiles:
                    _activeTool = _tileTool;
                    break;

                case ToolboxItem.Text:
                    _activeTool = _labelTool;
                    break;

                case ToolboxItem.CameraPath:
                    _activeTool = _pathTool;
                    break;

                case ToolboxItem.ColorPicker:
                    _activeTool = _pickerTool;
                    break;

                case ToolboxItem.Actors:
                    _activeTool = _actorsTool;
                    break;

                default:
                    throw new InvalidOperationException("Unknown editor tool requested: " + _toolboxItem);
            }

            _activeTool.SetCursor();
        }

        public void SetSubToolItem(SubToolItem subToolItem)
        {
            _activeTool.SetSubToolItem(subToolItem);
        }

        public void SetNormalizedTextSize(float value)
        {
            var size = MathHelper.Lerp(1.0f, 8.0f, value);
            ObjectScale = new Vector2(size);
        }
        
        public void SetTextRotation(float value)
        {
            ObjectRotation = MathHelper.Lerp(0, MathHelper.TwoPi, value);
        }
        
        public void Update()
        {
            if (KeyboardHelper.IsKeyJustPressed(Keys.E) && KeyboardHelper.IsKeyPressed(Keys.LeftControl))
                ToggleVisibility();

            if (Visible == false)
                return;

            UpdateMousePointer();

            HandleShortcutKeys();

            if (IsMouseOverCanvas)
            {
                if (MouseHelper.IsLeftButtonJustDown)
                    _game.MenuManager.SetFocus(null, false);

                _activeTool.Update();
            }
        }
        		
        void UpdateMousePointer()
        {
            var cameraPosition = Camera.Position;
            Camera.Position.X = 0.0f;
            Camera.Position.Y = 0.0f;

            var p = _game.RenderViewport.Unproject(new Vector3(_game.MenuManager.MouseWindowPosition.X, _game.MenuManager.MouseWindowPosition.Y, 0), _game.ProjectionMatrix, Camera.GetViewMatrix(), Matrix.Identity);

            var zoom = -Camera.Position.Z;
            p.X += (cameraPosition.X / zoom);
            p.Y += (cameraPosition.Y / zoom);
            WorldPosition = new Vector2(p.X * zoom, p.Y * zoom);

            Camera.Position = cameraPosition;
        }

        void HandleShortcutKeys()
        {
            if (KeyboardHelper.IsKeyPressed(Keys.LeftControl))
            {
                if (KeyboardHelper.IsKeyJustPressed(Keys.S))
                {
                    SaveCourse();
                }
                else if (KeyboardHelper.IsKeyJustPressed(Keys.A))
                {
                    SelectAll();
                }
                if (KeyboardHelper.IsKeyJustPressed(Keys.D))
                {
                    SelectNone();
                }
            }
            
            if (_game.MenuManager.FocusControl == null || _game.MenuManager.FocusControl is TextBoxControl == false)
            {
                if (KeyboardHelper.IsKeyJustPressed(Keys.Z))
                {
                    Undo();
                }
                if (KeyboardHelper.IsKeyJustPressed(Keys.Y))
                {
                    Redo();
                }


                //Zooming an panning
                if (MouseHelper.MouseWheelDelta != 0)
                {
                    if (KeyboardHelper.IsKeyPressed(Keys.LeftControl))
                    {
                        Camera.Position.Z += MouseHelper.MouseWheelDelta * 0.05f;
                    }
                    else if (KeyboardHelper.IsKeyPressed(Keys.LeftShift))
                    {
                        Camera.Position.X -= MouseHelper.MouseWheelDelta * 0.015f;
                    }
                    else
                    {
                        Camera.Position.Y += MouseHelper.MouseWheelDelta * 0.015f;
                    }
                }

                //Delete Selections
                if (KeyboardHelper.IsKeyJustPressed(Keys.Delete))
                {
                    if (DeleteSelectedCourseItems())
                        SaveEditorState("Delete selected items");
                }

                //Clipboard
                if (KeyboardHelper.IsKeyPressed(Keys.LeftControl))
                {
                    //Cut (Ctrl + X)
                    if (KeyboardHelper.IsKeyJustPressed(Keys.X))
                    {
                        _clipboard.Cut(LevelEditorState);
                    }

                    //Copy (Ctrl + C)
                    if (KeyboardHelper.IsKeyJustPressed(Keys.C))
                    {
                        _clipboard.Copy(LevelEditorState);
                    }

                    //Paste (Ctrl + V)
                    if (KeyboardHelper.IsKeyJustPressed(Keys.V))
                    {
                        _clipboard.Paste(LevelEditorState);
                    }
                }
            }

            if (_game.MenuManager.FocusControl != null)
                return;


            if (LevelEditorState.HasSelection)
            {
                var move = false;

                var deltaF = DesignGrid.Active ? DesignGrid.Step : 1.0f;
                var delta = Vector2.Zero;
                //Ctrl modifier
                if (KeyboardHelper.IsKeyPressed(Keys.LeftControl))
                {
                    deltaF *= 5.0f;
                }

                if (KeyboardHelper.IsLeftJustPressed)
                {
                    delta = new Vector2(-deltaF, 0);
                    move = true;
                }
                if (KeyboardHelper.IsRightJustPressed)
                {
                    delta = new Vector2(deltaF, 0);
                    move = true;
                }
                if (KeyboardHelper.IsUpJustPressed)
                {
                    delta = new Vector2(0, deltaF);
                    move = true;
                }
                if (KeyboardHelper.IsDownJustPressed)
                {
                    delta = new Vector2(0, -deltaF);
                    move = true;
                }

                if (move)
                {
                    //Move tile selection
                    if (LevelEditorState.SelectedTileIds.Count > 0)
                        LevelEditorState.SelectedTileIds.ForEach(id => GetTileByIndex(id).Position += delta);

                    //Move label selection
                    if (LevelEditorState.SelectedLabelIds.Count > 0)
                        LevelEditorState.SelectedLabelIds.ForEach(id => GetLabelByIndex(id).Position += delta);     

                    //Move node selection
                    if (LevelEditorState.SelectedNodeIds.Count > 0)
                    {
                    
                        LevelEditorState.SelectedNodeIds.ForEach(nodeId => {
                            if (nodeId > 0)
                                Course.CameraPath.Vertices[nodeId - 1] += delta;

                            Course.CameraPath.Vertices[nodeId] += delta;

                            if (nodeId < Course.CameraPath.Vertices.Count - 1)
                                Course.CameraPath.Vertices[nodeId + 1] += delta;
                        });
                    }

                    SaveEditorState("Move selected items");
                }
            }
        }

        internal static bool IsMouseOverCanvas { get { return MouseHelper.IsMouseInWindow && TransitionGame.Instance.MenuManager.HoverControl == null; } }

        public void SaveEditorState(string action)
        {
            if (EditorStateId < _editorStates.Count - 1)
                _editorStates.RemoveRange(EditorStateId + 1, _editorStates.Count - 1 - EditorStateId);

            var contents = JsonConvert.SerializeObject(LevelEditorState);
            _editorStates.Add(new StateEntry { Action = action, State = contents });

            EditorStateId = _editorStates.Count - 1;

            IsDirty = true;
        }

        public IEnumerable<Tile> GetSelectedTiles()
        {
            foreach (var index in LevelEditorState.SelectedTileIds)
                yield return Course.Tiles[index];
        }

        public IEnumerable<Label> GetSelectedLabels()
        {
            foreach (var index in LevelEditorState.SelectedLabelIds)
                yield return Course.Labels[index];
        }

        public bool DeleteSelectedCourseItems()
        {
            var selectedCount = LevelEditorState.SelectedTileIds.Count + LevelEditorState.SelectedNodeIds.Count + LevelEditorState.SelectedLabelIds.Count;

            //Tiles
            //Get the actual tiles to delete so that we don't get any IndexOutOfRange exceptions when deleting.
            var selectedTiles = new List<Tile>();
            LevelEditorState.SelectedTileIds.ForEach(id => selectedTiles.Add(GetTileByIndex(id)));
            selectedTiles.ForEach(tile => LevelEditorState.Course.Tiles.Remove(tile));
            LevelEditorState.SelectedTileIds.Clear();

            //Labels
            //Get the actual tiles to delete so that we don't get any IndexOutOfRange exceptions when deleting.
            var selectedLabels = new List<Label>();
            LevelEditorState.SelectedLabelIds.ForEach(id => selectedLabels.Add(GetLabelByIndex(id)));
            selectedLabels.ForEach(label => LevelEditorState.Course.Labels.Remove(label));
            LevelEditorState.SelectedLabelIds.Clear();

            //Camera Nodes
            var deletedNodes = new List<int>();
            LevelEditorState.SelectedNodeIds.Sort();
            foreach (var id in LevelEditorState.SelectedNodeIds)
            {
                if (id - 1 > -1)
                    deletedNodes.Add(id - 1);

                deletedNodes.Add(id);

                if (id + 1 < LevelEditorState.Course.CameraPath.Vertices.Count)
                    deletedNodes.Add(id + 1);
            }

            for(var i = deletedNodes.Count - 1; i >= 0; i--)
                LevelEditorState.Course.CameraPath.Vertices.RemoveAt(deletedNodes[i]);
            
            LevelEditorState.SelectedNodeIds.Clear();

            return selectedCount > 0;
        }

        public void NewCourse()
        {
            var course = Course.CreateDefaultCourse();
            InitialiseCourse(course);
        }

        public void OpenCourse(string courseName)
        {
            var course = Course.Load(courseName ?? Course.FileName);

            InitialiseCourse(course);
        }

        public void SaveCourse()
        {
            Course.Save();
            IsDirty = false;
            OnCourseSaved(EventArgs.Empty);
        }

        void SelectAll()
        {
            var selectionCount = LevelEditorState.SelectionCount;

            LevelEditorState.SelectAll();
            _selectionTool.RefreshEditorObjectColor();

            if (selectionCount != LevelEditorState.SelectionCount)
                SaveEditorState("Select all items");
        }

        void SelectNone()
        {
            if (LevelEditorState.HasSelection)
            {
                LevelEditorState.ClearSelection();
                SaveEditorState("Clear selection");
            }
        }

        public void Undo()
        {
            if (EditorStateId <= 0)
                return;
            
            EditorStateId--;
            var state = _editorStates[EditorStateId];

            LevelEditorState = JsonConvert.DeserializeObject<LevelEditorState>(state.State);

            IsDirty = true;
        }

        public void Redo()
        {
            if (EditorStateId >= _editorStates.Count - 1)
                return;
            
            EditorStateId++;
            var state = _editorStates[EditorStateId];

            LevelEditorState = JsonConvert.DeserializeObject<LevelEditorState>(state.State);

            IsDirty = true;
        }

        public Box2 GetTileSelectionBounds()
        {
            //Draw the selection
            if (LevelEditorState.SelectedTileIds.Count <= 0)
                return Box2.Empty;

            var top = float.MinValue;
            var left = float.MaxValue;
            var bottom = float.MaxValue;
            var right = float.MinValue;

            foreach (var index in LevelEditorState.SelectedTileIds)
            {
                var tile = GetTileByIndex(index);

                var expand = (tile.Scale / 2.0f);

                if (tile.Position.X - expand.X < left)
                    left = tile.Position.X - expand.X;

                if (tile.Position.X + expand.X > right)
                    right = tile.Position.X + expand.X;

                if (tile.Position.Y - expand.Y < bottom)
                    bottom = tile.Position.Y - expand.Y;

                if (tile.Position.Y + expand.Y > top)
                    top = tile.Position.Y + expand.Y;
            }

            return new Box2(left, bottom, right - left, top - bottom);
        }

        public Box2 GetNodeSelectionBounds()
        {
            if (LevelEditorState.SelectedNodeIds.Count == 0)
                return Box2.Empty;

            var top = float.MinValue;
            var left = float.MaxValue;
            var bottom = float.MaxValue;
            var right = float.MinValue;

            var expand = new Vector2(0.5f);

            foreach (var index in LevelEditorState.SelectedNodeIds)
            {
                var node = Course.CameraPath.Vertices[index];

                if (node.X - expand.X < left)
                    left = node.X - expand.X;

                if (node.X + expand.X > right)
                    right = node.X + expand.X;

                if (node.Y - expand.Y < bottom)
                    bottom = node.Y - expand.Y;

                if (node.Y + expand.Y > top)
                    top = node.Y + expand.Y;
            }
            

            return new Box2(left, bottom, right - left, top - bottom);
        }

        public Box2 GetLabelSelectionBounds()
        {
            //Draw the selection
            if (LevelEditorState.SelectedLabelIds.Count <= 0)
                return Box2.Empty;

            var top = float.MinValue;
            var left = float.MaxValue;
            var bottom = float.MaxValue;
            var right = float.MinValue;

            foreach (var index in LevelEditorState.SelectedLabelIds)
            {
                var label = GetLabelByIndex(index);
                var labelBounds = Course.LabelFont.GetBounds(label.Position, label.FontSize, label.Text, label.Alignment);

                var obb2 = new Obb2(labelBounds, label.Position, label.Rotation);

                if (obb2.Min.X < left)
                    left = obb2.Min.X;

                if (obb2.Max.X > right)
                    right = obb2.Max.X;

                if (obb2.Min.Y < bottom)
                    bottom = obb2.Min.Y;

                if (obb2.Max.Y > top)
                    top = obb2.Max.Y;
            }

            return new Box2(left, bottom, right - left, top - bottom);
        }

        public void Draw()
        {
            if (Visible == false)
                return;

            //Draw stuff with level camera
            _game.CameraManager.Camera = Camera;
            _game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);

            _activeTool.Draw();
            
            DesignGrid.Draw(_game.Renderer, Camera);
            
            //Draw the selection
            if (LevelEditorState.HasSelection)
            {
                var top = float.MinValue;
                var left = float.MaxValue;
                var bottom = float.MaxValue;
                var right = float.MinValue;

                var selectionLineWidth = _game.MenuManager.FocusControl == null ? 0.15f : 0.035f;

                if (LevelEditorState.SelectedTileIds.Count > 0)
                {
                    foreach (var index in LevelEditorState.SelectedTileIds)
                    {
                        var tile = GetTileByIndex(index);

                        var expand = (tile.Scale / 2.0f);

                        _game.Renderer.DrawBoxStyled(tile.Position.Y + expand.Y, tile.Position.X - expand.X, tile.Position.Y - expand.Y, tile.Position.X + expand.X, Color.Red/*Theme.Current.GetColor(Theme.Colors.Focus)*/, Renderer.LineStyle.Dotted, selectionLineWidth);

                        if (tile.Position.X - expand.X < left)
                            left = tile.Position.X - expand.X;

                        if (tile.Position.X + expand.X > right)
                            right = tile.Position.X + expand.X;

                        if (tile.Position.Y - expand.Y < bottom)
                            bottom = tile.Position.Y - expand.Y;

                        if (tile.Position.Y + expand.Y > top)
                            top = tile.Position.Y + expand.Y;
                    }
                }

                if (LevelEditorState.SelectedLabelIds.Count > 0)
                {
                    foreach (var index in LevelEditorState.SelectedLabelIds)
                    {
                        var label = GetLabelByIndex(index);
                        var labelBounds = Course.LabelFont.GetBounds(label.Position, label.FontSize, label.Text, label.Alignment);

                        var obb2 = new Obb2(labelBounds, label.Position, label.Rotation);

                        _game.Renderer.DrawBoxStyled(obb2.Top, obb2.Left, obb2.Bottom, obb2.Right, Color.Red/*Theme.Current.GetColor(Theme.Colors.Focus)*/, Renderer.LineStyle.Dotted, selectionLineWidth);

                        if (obb2.Min.X < left)
                            left = obb2.Min.X;

                        if (obb2.Max.X > right)
                            right = obb2.Max.X;

                        if (obb2.Min.Y < bottom)
                            bottom = obb2.Min.Y;

                        if (obb2.Max.Y > top)
                            top = obb2.Max.Y;
                    }
                }

                //Draw the node selection
                if (LevelEditorState.SelectedNodeIds.Count > 0)
                {
                    var expand = new Vector2(0.5f);

                    var pointSize = Vector2.One;
                    var textureInfo = _game.TextureManager.GetTextureInfo("BezierPoint");

                    foreach (var index in LevelEditorState.SelectedNodeIds)
                    {
                        var node = Course.CameraPath.Vertices[index];

                        //_game.GeometryBatch.DrawBoxStyled(new Rect(node, expand * 2, Alignment.Centre), Color.Red, GeometryBatch.LineStyle.Dotted, selectionLineWidth);

                        var q = new Quad(node, pointSize, 0);
                        _game.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, Color.Red/*Theme.Current.GetColor(Theme.Colors.Focus)*/, TransparentRenderStyle.Instance);

                        if (node.X - expand.X < left)
                            left = node.X - expand.X;

                        if (node.X + expand.X > right)
                            right = node.X + expand.X;

                        if (node.Y - expand.Y < bottom)
                            bottom = node.Y - expand.Y;

                        if (node.Y + expand.Y > top)
                            top = node.Y + expand.Y;
                    }
                }

                _game.Renderer.DrawBoxStyled(top, left, bottom, right, Color.White, Renderer.LineStyle.Dotted, selectionLineWidth);
            }

            _game.Renderer.End();

            //Stuff that needs to be drawn with the GUI Camera
            _game.CameraManager.Camera = _game.GuiCamera;
            _game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);

            var mouseHintFontSize = new Vector2(0.85f);
            
            var helpTextPosition =  _game.MenuManager.MouseWorldPosition + new Vector2(0.0f, 1.25f);
            var coordsPosition =  _game.MenuManager.MouseWorldPosition + new Vector2(0.0f, 0.5f);
            var mouseHelpText = $"X:{ObjectPosition.X:0.00}, Y:{ObjectPosition.Y:0.00}";
            var font = Theme.Font;

            if (string.IsNullOrEmpty(_activeTool.ToolTip) == false)
            {
                _game.Renderer.DrawString(font, helpTextPosition, mouseHintFontSize, 0, _activeTool.ToolTip, Alignment.CentreLeft, Color.Black, TransparentRenderStyle.Instance);
                _game.Renderer.DrawString(font, helpTextPosition + new Vector2(-0.03f, 0.03f), mouseHintFontSize, 0, _activeTool.ToolTip, Alignment.CentreLeft, Theme.Current.GetColor(Theme.Colors.Hover), TransparentRenderStyle.Instance);
            }

            _game.Renderer.DrawString(font, coordsPosition, mouseHintFontSize, 0, mouseHelpText, Alignment.CentreLeft, Color.Black, TransparentRenderStyle.Instance);
            _game.Renderer.DrawString(font, coordsPosition + new Vector2(-0.03f, 0.03f), mouseHintFontSize, 0, mouseHelpText, Alignment.CentreLeft, Color.White, TransparentRenderStyle.Instance);

            _game.Renderer.End();
        }

        protected virtual void OnCourseInitialised(EventArgs e)
        {
            var handler = CourseInitialised;
            if (handler != null)
                handler(this, e);
        }

        protected virtual void OnCourseSaved(EventArgs e)
        {
            var handler = CourseSaved;
            if (handler != null)
                handler(this, e);
        }
    }
}
