float2 DisplacementScroll;

//Scene texture.
sampler TextureSampler : register(s0) = sampler_state
{
	AddressU = Clamp;
	AddressV = Clamp;
	AddressW = Clamp;
	MinFilter = Linear;
	MagFilter = Linear;
	MipFilter = Linear;
};

//Displacement texture containing the offsets to use.
sampler DisplacementSampler : register(s1) = sampler_state
{
	AddressU = Clamp;
	AddressV = Clamp;
	AddressW = Clamp;
	MinFilter = Linear;
	MagFilter = Linear;
	MipFilter = Linear;
};

float4 main(float4 color : COLOR0, float2 texCoord : TEXCOORD0) : COLOR0
{
	//Look up the displacement amount.
	//It is in the range 0...1 for both red and green channels. Blue is strength of displacement.
    float4 displacement = tex2D(DisplacementSampler, texCoord);
	//Convert the displacement to the range -0.5...0.5 (taking into account overal strength)
	displacement.rg -= (0.5 * displacement.b);
	
	//Scale the strength - this is just some arbritrary value.
	float strength = displacement.b * 0.9994;
	
	//Offset the sceen texture coordinate by the amount of the displacement.
	texCoord.x += (displacement.r * strength);
	texCoord.y += (displacement.g * strength);

    // Look up into the main texture.
    return tex2D(TextureSampler, texCoord) * color;
}

technique Refraction
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 main();
    }
}
