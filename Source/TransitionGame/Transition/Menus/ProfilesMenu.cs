using System;
using Nuclex.Input;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the sign in / sign out / profiles menu
    /// </summary>
    public class ProfilesMenu : Menu
    {
        TextLabelControl _profileMessage;
        ListControl _profilesList;
        TextLabelControl _profileLabel;
        TextLabelControl _noProfilesLabel;
        TextButtonControl _signInOrOutButton;
        string _cachedSignedInProfile;

        bool _awaitingProfile;

        /// <summary>
        /// Initialises a new instance of a ProfilesMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public ProfilesMenu(BaseGame game, string title) : base(game, title) { }

        protected override void OnConfigureMenu()
        {
            _profileMessage = GetControl<TextLabelControl>("profileMessage");
            _profileLabel = GetControl<TextLabelControl>("profileLabel");
            _profilesList = GetControl<ListControl>("profilesList");
            _noProfilesLabel = GetControl<TextLabelControl>("noProfilesLabel");
            _signInOrOutButton = GetControl<TextButtonControl>("signInOrOutButton");
            var editButton = GetControl<TextButtonControl>("editButton");
            var createButton = GetControl<TextButtonControl>("createButton");

            _profilesList.Click += ProfileListClicked;
            _profilesList.TextButtonClicked += ProfileListTextButtonClicked;
            _profilesList.DecreaseValue += ProfilesListChanged;
            _profilesList.IncreaseValue += ProfilesListChanged;

			_signInOrOutButton.Click += ProfileListTextButtonClicked;
           
			editButton.Click += (sender, e) => ShowTextEntry(_profilesList.SelectedText);
			createButton.Click += (sender, e) => ShowTextEntry(string.Empty);
        }

        protected override void OnAddedToManager()
        {
            base.OnAddedToManager();

            _awaitingProfile = false;

            if (Game.ActivePlayer.Profile.ProfileType == ProfileType.BuiltIn)
            {
                _profileMessage.Text = "You are not currently signed in";
            }
            else
            {
                _profileMessage.Text = "You are currently signed in as " + Game.ActivePlayer.Profile.Name;
            }

            if (Game.PlayerProfileManager.Profiles.Count > 0)
            {
                LoadProfiles();
                _profilesList.Visible = true;
                _profileLabel.Visible = true;
                _noProfilesLabel.Visible = false;
            }
            else
            {
                _profilesList.Visible = false;
                _profileLabel.Visible = false;
                _noProfilesLabel.Visible = true;
            }

            _cachedSignedInProfile = Game.GameOptions.SignedInProfile;

            SetProfileListAButtonText();
        }

        private void LoadProfiles()
        {
            _profilesList.ClearOptions();
            
            foreach (var profile in Game.PlayerProfileManager.Profiles)
            {
                if (profile.ProfileType != ProfileType.BuiltIn)
                    _profilesList.AddOption(profile.Name);
            }

            //Select the first option or the signed in profile if we find it.
            if (_profilesList.Count > 0)
            {
                _profilesList.SelectedIndex = 0;
                _profilesList.SetSelectedItemByText(Game.GameOptions.SignedInProfile);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnGotFocus()
        {
            if (_awaitingProfile)
            {
                var textEntryMenu = (TextEntryMenu)Game.MenuManager.GetMenu(typeof(TextEntryMenu));
                if (textEntryMenu.Response == MenuResponse.Ok)
                {
                    if (string.IsNullOrEmpty(textEntryMenu.InitialText))
                    {
                        //Create a new profile
                        var playerProfile = Game.PlayerProfileManager.CreateProfile(textEntryMenu.Text);
                        playerProfile.Save(Game);
                    }
                    else
                    {
                        //Edit the existing profile.
                        var playerProfile = Game.PlayerProfileManager.GetProfile(textEntryMenu.InitialText);
                        playerProfile.Name = textEntryMenu.Text;
                        playerProfile.Save(Game);
                    }

                    LoadProfiles();

                    _profilesList.SetSelectedItemByText(textEntryMenu.Text);

                    SetProfileListAButtonText();
                }
                _awaitingProfile = false;
            }
        }

        private void ShowTextEntry(string name)
        {
            _awaitingProfile = true;
            var textEntryMenu = (TextEntryMenu)Game.MenuManager.GetMenu(typeof(TextEntryMenu));
            textEntryMenu.Title = "Enter Profile Name";
            textEntryMenu.MaxLength = 15;
            textEntryMenu.InitialText = name;
            
            Game.MenuManager.AddMenu(textEntryMenu);
        }
        
        /// <summary>
        /// Save the options (in memory and to file).
        /// </summary>
        private void SaveOptions()
        {
            Game.GameOptions.Save(Game);
        }

        ///// <summary>
        ///// Sets the control values from game options.
        ///// </summary>
        //private void SetValuesFromOptions()
        //{
        //    //_profilesList.SetSelectedItemByText(Game.GameOptions.SignedInProfile);
        //}

        /// <summary>
        /// Event handler for the Sfx slider value changed events.
        /// </summary>
        private void ProfilesListChanged(object sender, EventArgs e)
        {
            SetProfileListAButtonText();
        }

        private void SetProfileListAButtonText()
        {
            if (_cachedSignedInProfile == _profilesList.SelectedText)
            {
                _profilesList.CachedAButtonText = "Sign Out";
                _signInOrOutButton.Text = "Sign Out";
                _signInOrOutButton.HelpText = "Sign out of the logged in profile";
            }
            else
            {
                _profilesList.CachedAButtonText = "Sign In";
                _signInOrOutButton.Text = "Sign In";
                _signInOrOutButton.HelpText = "Sign in to the selected profile";
            }
        }

        private void ProfileListTextButtonClicked(object sender, EventArgs e)
        {
            //if (MenuManager.PrimaryNavigation == ControlType.MouseAndKeys)
                HandleSignInOrOut();
        }

        private void ProfileListClicked(object sender, EventArgs e)
        {
            if (Game.InputMethodManager.InputMethod.SupportsGamepad || (Game.InputMethodManager.InputMethod.SupportsKeyboard && Game.InputMethodManager.InputMethod.SupportsKeyboard == false))
                HandleSignInOrOut();
        }

        private void HandleSignInOrOut()
        {
            if (_cachedSignedInProfile == _profilesList.SelectedText)
            {
                //Cal this a sign out...
                Game.GameOptions.SignedInProfile = string.Empty;
            }
            else
            {
               Game.GameOptions.SignedInProfile = _profilesList.SelectedText; 
            }

            var key = Game.GameOptions.SignedInProfile;
            
            if (string.IsNullOrEmpty(Game.GameOptions.SignedInProfile))
            {
                key = Game.PlayerProfileManager.GetProfileInfo(ExtendedPlayerIndex.One).DisplayName;
            }

            Game.ActivePlayer.Profile = Game.PlayerProfileManager.GetProfile(key);
            
            Close();
            MenuManager.FadeOut();
        }

        protected override bool OnBack() 
        {
            base.OnBack();

            MenuManager.FadeOut();

            return true;
        }

        /// <summary>
        /// Called when the menu has finished its closing state.
        /// </summary>
        protected override void OnClosed()
        {
            SaveOptions();
        }
    }
}
