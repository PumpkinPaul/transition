float4x4 World;
float4x4 View;
float4x4 Projection;
uniform extern texture UserTexture;

sampler baseSampler = sampler_state
{
    Texture = <UserTexture>;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    ADDRESSU = CLAMP;
    ADDRESSV = CLAMP;
};

// TODO: add effect parameters here.
 
struct VS_IN
{
    float4 Position: POSITION;
    float2 TextureCoords: TEXCOORD0;
    float4 Colour: COLOR;
};

struct VS_OUT 
{
   float4 ScreenPosition:   POSITION;
   float2 TextureCoords: TEXCOORD0;
   float4 Colour: COLOR;
};


VS_OUT VertexShaderFunction(VS_IN input)
{
    VS_OUT output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.ScreenPosition = mul(viewPosition, Projection);
	output.TextureCoords = input.TextureCoords;
    output.Colour = input.Colour;

    return output;
}