﻿using Microsoft.Xna.Framework;
using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Responsible for tracking player in the game world - StarGuard camera will utilise this
    /// </summary>
    [ProtoContract]
    public class CameraProxy : Entity
    {
        public enum Phase
        {
            Normal,
            Flag
        }

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        Vector2 _anchor;

        [ProtoMember(2)]
        Vector2 _location;

        [ProtoMember(3)]
        int _ticks;

        [ProtoMember(4)]
        int _duration;

        [ProtoMember(5)]
        bool _playerWasOnGround;

        [ProtoMember(6)]
        bool _trackY;

        [ProtoMember(7)]
        Phase _phase;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public CameraProxy(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.CameraProxy;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            _location = Position;
            _anchor = _location;
            _ticks = 0;
            _duration = 0;
            _playerWasOnGround = false;
            _trackY = false;
            _phase = Phase.Normal;

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }   

        public void MoveToY(float y, int ticks)
        {
            _duration = ticks;
            _ticks = 0;
            _location = new Vector2(Position.X, y);
            _anchor = Position;
        }

        public void MoveTo(Vector2 location, int ticks)
        {
            _duration = ticks;
            _ticks = 0;
            _location = location;
            _anchor = Position;

            _phase = Phase.Flag;
        }

        public override void PostUpdate()
        {
            switch(_phase)
            {
                case Phase.Normal:
                    PostUpdatePhaseNormal();
                    break;

                case Phase.Flag:
                    PostUpdatePhaseFlag();
                    break;
            }
        }

        void PostUpdatePhaseNormal()
        {
            if (_duration > 0)
            {
                _ticks++;
                if (_ticks > _duration)
                {
                    _ticks = 0;
                    _duration = 0;
                }

                var ratio = _duration > 0 ? (float)_ticks / _duration : 1;
                Position.Y = MathHelper.Lerp(_anchor.Y, _location.Y, ratio);
            }
            else
                Position.Y = _location.Y;

            var player = (Player)Game.World[EntityKeys.Player];

            Position.X = player.Position.X;

            //If the player steps off ground track his y location directly / instantly
            if (_playerWasOnGround && !player.OnGround || player.Velocity.Y < 0)
                _trackY = true;
            else if (_trackY && player.OnGround)
                _trackY = false;

            if (_trackY)
            {
                if (Position.Y >= player.Position.Y)
                    MoveToY(player.Position.Y, 5);
            }
            else
            {
                if (player.OnGround && Position.Y < player.Position.Y)
                {
                    MoveToY(player.Position.Y, 45);
                }
                else if (player.OnGround && Position.Y > player.Position.Y)
                {
                    MoveToY(player.Position.Y, 10);
                }
            }

            _playerWasOnGround = player.OnGround;
        }

        void PostUpdatePhaseFlag()
        {
            if (_duration > 0)
            {
                _ticks++;
                if (_ticks > _duration)
                {
                    _ticks = 0;
                    _duration = 0;
                    _phase = Phase.Normal;
                    TransitionGame.Instance.Player.SetPhaseNormal();
                }

                var ratio = _duration > 0 ? (float)_ticks / _duration : 1;

                Position.X = Interpolate.EaseBothMedium(_anchor.X, _location.X, ratio);
                Position.Y = Interpolate.EaseBothMedium(_anchor.Y, _location.Y, ratio);
            }
            else
            {
                Position = _location;
                _phase = Phase.Normal;

            }
        }
    }
}