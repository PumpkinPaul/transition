using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Transition.Cameras;
using Transition.CourseSystem;
using Transition.GamePhases;
using Transition.Input;
using Transition.Menus;

namespace Transition._____Temp.Svg
{
    /// <summary>
    /// 
    /// </summary>
    public class TestPhaseSvg : GamePhase
    {
        Vector2 _previousCameraPosition;
        Camera _testCamera;
        float _cameraTime;
        float _previousCameraTime;

        Course Course
        {
            get { return TransitionGame.Instance.LevelEditor.Course; }
        }

        public TestPhaseSvg(BaseGame game) : base(game) 
        { 
            
        }

        protected override void OnCreate()
        {
            _testCamera = new Camera(Game);
            _testCamera.Position.Z = -11.7f;
            _testCamera.Initialise();
            Game.CameraManager.AddCamera(_testCamera);

            var filepath = Path.Combine(Platform.ResourcesPath, "Courses", "Course2.course");

            var course = Course.Load(filepath);

            TransitionGame.Instance.LevelEditor.InitialiseCourse(course);
            
            var cameraPosition1 = Course.GetCameraPosition(_cameraTime);
            var cameraPosition2 = Course.GetCameraPosition(_cameraTime + 0.005f);
            
            _testCamera.Rotation = MathHelper.PiOver2 - (float)Math.Atan2(cameraPosition2.Y - cameraPosition1.Y, cameraPosition2.X - cameraPosition1.X);

            Gearset.GS.Inspect("Level Editor", TransitionGame.Instance.LevelEditor, true);
        }

        bool init = true;

        protected override void OnUpdate()
        {
            if (init == false)
            {
                ShowMenu();
                init = true;
            }

            Game.ScreenFlashManager.ClearColor = Course.BackgroundColor;

            HandleDebugKeys();

            var cameraPosition = Course.GetCameraPosition(_cameraTime);
            _testCamera.Position.X = cameraPosition.X;
            _testCamera.Position.Y = cameraPosition.Y;

            if (_cameraTime > _previousCameraTime)
                _testCamera.Rotation = MathHelper.PiOver2 - (float)Math.Atan2(cameraPosition.Y - _previousCameraPosition.Y, cameraPosition.X - _previousCameraPosition.X);
            else if (_cameraTime < _previousCameraTime)
                _testCamera.Rotation = MathHelper.PiOver2 - (float)Math.Atan2(_previousCameraPosition.Y - cameraPosition.Y, _previousCameraPosition.X - cameraPosition.X);

            _previousCameraPosition = cameraPosition;
            _previousCameraTime = _cameraTime;
        }

        void ShowMenu()
        {
            //TODO: TEMP!
            Game.MenuManager.SetPrimaryControllerId();          
            Game.ChangePhase(typeof(MenuGamePhase));
            Game.MenuManager.FadeIn();
            Game.MenuManager.AddMenu(typeof(MainMenu));            
        }

        private void HandleDebugKeys()
        {
            if (KeyboardHelper.IsKeyJustPressed(Keys.M) && KeyboardHelper.IsKeyPressed(Keys.LeftControl))
                ShowMenu();

            if (KeyboardHelper.IsKeyJustPressed(Keys.T) && KeyboardHelper.IsKeyPressed(Keys.LeftControl))
            {
                Game.MenuManager.AddMenu(typeof(TestMenu));   
            }

            if (KeyboardHelper.IsKeyPressed(Keys.Q))
                _testCamera.Position.Z += 0.3f;
            else if (KeyboardHelper.IsKeyPressed(Keys.A))
                _testCamera.Position.Z -= 0.3f;

            if (KeyboardHelper.IsKeyPressed(Keys.Right))
                _testCamera.Position.X += 0.2f;
            else if (KeyboardHelper.IsKeyPressed(Keys.Left))
                _testCamera.Position.X -= 0.2f;

            if (KeyboardHelper.IsKeyPressed(Keys.Up))
                _testCamera.Position.Y += 0.2f;
            else if (KeyboardHelper.IsKeyPressed(Keys.Down))
                _testCamera.Position.Y -= 0.2f;

            if (KeyboardHelper.IsKeyPressed(Keys.W))
                _cameraTime += 0.005f;
            else if (KeyboardHelper.IsKeyPressed(Keys.S))
                _cameraTime -= 0.005f;

            //if (KeyboardHelper.KeyboardKeyPressed(Keys.E))
            //    _testCamera.Rotation += 0.005f;
            //else if (KeyboardHelper.KeyboardKeyPressed(Keys.D))
            //    _testCamera.Rotation -= 0.005f;

            _cameraTime = Course.CameraPath.Clamp(_cameraTime);
        }

        protected override void OnRender()
        {
            Game.GraphicsDevice.Clear(Course.BackgroundColor);

            Game.CameraManager.Render();

            if (TransitionGame.Instance.LevelEditor.Visible == false)
                Game.CameraManager.Camera = _testCamera;
            else
                Game.CameraManager.Camera = TransitionGame.Instance.LevelEditor.Camera;

            Course.Draw();
        }
    }
}