using System;
using Transition.Actors;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public static class SpawnSystem
    {
        /// <summary>
        /// Spawns an actor in the scene.
        /// </summary>
        /// <param name="actor"></param>
        public static void DefaultSpawn(Actor actor)
	    {			
            //foreach(ParticleEffect particleEffect in actor.ActorData.SpawnEffects)
            //{
            //    ParticleSystem particleSystem = new ParticleSystem(this._game);
				
            //    particleSystem.Template = particleEffect.Template;
            //    particleSystem.Lifetime = particleEffect.Lifetime;
            //    particleSystem.Position = actor.Position;
            //    particleSystem.Offset = particleEffect.Offset;
            //    particleSystem.Owner = actor;
            //    particleSystem.Ownership = Ownership.FlyTheNest;
				
            //    actor.Scene.AddActor(particleSystem);
            //}

            ////GameState::playSample(actor_data->getSpawnSFX());

            ////if (actor->getActorData()->getSpawnScaleIncrement() != 0) 
            ////{
            ////	actor->setScale(Vector(0.0f, 0.0f, 0.0f));
            ////}
	    }

        /// <summary>
        /// Processes an actor whilst they are spawning.
        /// </summary>
        /// <param name="actor">The actor to spawn</param>
        /// <returns>True if the actor has finished spawning; otherwise false.</returns>
        public static bool HandleSpawning(Actor actor)
        {
            ////Flag to indicate if we need to advance the state machine
            bool finishedSpawning = false;

            float spawningScale = actor.SpawningScale;

            if (actor.SpawningScale < 1.0f)
                actor.SpawningScale += actor.ActorData.SpawnScaleIncrement;
            else
            {
                //Actor has finished spawning 
                finishedSpawning = true;
                actor.SpawningScale = 1.0f;
                actor.Spawned = true;
                actor.Collidable = actor.ActorData.CollisionRadius > 0.0f;
            }

            return finishedSpawning;
        }
    }
}
