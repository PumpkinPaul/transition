#if WINDOWS
	using System;
	using System.Diagnostics;
	using System.IO;
	using System.Windows.Forms;
	using System.Linq;
	using PumpkinGames.AutoUpdaterLibrary;
	
    namespace Transition
	{
	    static class Program
	    {
            /// <summary>
            /// The main entry point for the application.
            /// </summary>
            #if USE_GEARSET
                [STAThread]
            #endif
	        static void Main(string[] args)
	        {

                //Transition.Util.ScriptManager.AddVariable("firstName", "paul");
                //Transition.Util.ScriptManager.AddVariable("lastName", "cunningham");
                //var result = Transition.Util.ScriptManager.Parse("=$firstName $lastName");
                //var c = Transition.Util.ScriptManager.Parse("$#Transition.Theme.Accent1");
                //c = Transition.Util.ScriptManager.Parse("$#Transition.Program.TestColor");
                //var result = Transition.Eval.Expression.eval("(3 + 4) * 5");
                //ScriptManager.AddVariable("test", Color.White);
                //ScriptManager.AddVariable("num", "7");
                //var c = ScriptManager.Parse("=$test * 0.5");
                
	            //Allow a safe mode batch file to delete the options file 
	            if (args.Length > 0 && args[0] == "-1")
	            {
	                var optionsFile = GameOptions.Filename; 
	                if (File.Exists(optionsFile))
	                    File.Delete(optionsFile);
	            }

	            Trace.AutoFlush = true;
	            var applicationDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DisasterIncorporated");
	            var applicationLogFile =  Path.Combine(applicationDataPath, "DisasterIncorporated.log");
	            var updaterLogFile = Path.Combine(applicationDataPath, "DisasterIncorporatedUpdate.log");

	            if (args.Contains(LaunchArguments.PreserveLogFile) == false)
	            {
	                FileManager.DeleteFile(applicationLogFile);
	                FileManager.DeleteFile(updaterLogFile);
	            }

	            FileManager.EnsureFileDirectory(applicationLogFile); 
	            Trace.Listeners.Add(new TextWriterTraceListener(applicationLogFile));

	            Trace.WriteLine($"{DateTime.Now}\r\nExecuting Disaster Incorporated...");

	            try
	            { 
	                if (args.Contains(LaunchArguments.IgnoreUpgrade) == false)
	                {
	                    //Check to see if there is a new version of the app available - if there is then the auto updater will spawn and we can exit this app.
	                    Trace.WriteLine("Checking for a new version...");

	                    IAutoUpdater autoUpdater = new AutoUpdater("DisasterIncorporated", "DisasterIncorporated.exe", "AutoUpdaterLibrary.dll", updaterLogFile);
	                    if (autoUpdater.CheckIfNewerVersionAvailable())
	                    {
	                        Trace.WriteLine("A new version exists - exiting game in preparation for upgrade.");
	                        return;
	                    }

	                    Trace.WriteLine("There is either not a new version available or the update was ignored.");
	                }
	            }
	            catch (Exception ex)
	            { 
	                Trace.WriteLine(ex);
	            }

	            //No newer version or error so just run whatever version of the app we have already....

	            using (var game = new TransitionGame())
	            {              
	                try
	                {
	                    Trace.WriteLine("Executing game.");
	                    game.Run();
	                }
	                catch (Exception ex)
	                {
	                    Trace.IndentLevel = 0;
	                    Trace.WriteLine("-------------------------------------------------------------------------------------------");
	                    Trace.WriteLine(DateTime.Now + " " + ex.Message);
	                    Trace.IndentLevel = 1;
	                    Trace.WriteLine(ex.StackTrace + @"\n");

	                    MessageBox.Show(ex.Message + @"\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
	                }
	            }

                Environment.Exit(0);
	        }
	    }
	}
#endif
