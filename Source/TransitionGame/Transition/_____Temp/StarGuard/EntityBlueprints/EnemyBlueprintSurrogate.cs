using ProtoBuf;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
	/// <summary>
	/// Configuration data surrogate for serialization.
	/// </summary>
    [ProtoContract]
    public class EnemyBlueprintSurrogate
    {
        [ProtoMember(1)]
        public string Name;

        static readonly EnemyBlueprintSurrogate Instance = new EnemyBlueprintSurrogate();

        public static implicit operator EnemyBlueprintSurrogate(EnemyBlueprint value)
        {
            if (value == null)
                return null;

            Instance.Name = value.Name;
            return Instance;
        }

        public static implicit operator EnemyBlueprint(EnemyBlueprintSurrogate value)
        {
            return TransitionGame.Instance.EntityBlueprints.EnemyBlueprint(value.Name);
        }
    }
}
