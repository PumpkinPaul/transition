using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.ResourceManagers
{
	/// <summary>
	/// Manages fonts.
	/// </summary>
    public class FontManager : ResourceManager
    {
        public override string FolderName { get { return "BitmapFonts"; } }

	    readonly TextureManager _textureManager;

		readonly Dictionary<string, BitmapFont> _bitmapFonts = new Dictionary<string, BitmapFont>();

		/// <summary>
        /// Creates a new instance of a FontManager.
        /// </summary>
        public FontManager(TextureManager textureManager) 
        {
            if (textureManager == null)
                throw new ArgumentNullException(nameof(textureManager));

            _textureManager = textureManager;
        }
        
        /// <summary>
        /// Loads all actor configuration data into the data manager.
        /// </summary>
        public override void LoadContent()
        {
            _bitmapFonts.Clear();

            var folder = Path.Combine(Platform.ResourcesPath, FolderName);

            foreach (var filename in Directory.GetFiles(folder, "*.xml"))
                LoadTemplate(filename);
        }

		/// <summary>
        /// Load the font configuration data.
        /// </summary>
        private void LoadTemplate(string filename)
        {
            var charMetrics = new List<BitmapCharacterMetrics>();

            var xdocument = XDocument.Load(filename);
            var bitmapFontElement = xdocument.Root;

            if (bitmapFontElement == null)
                throw new XmlException($"The font file {filename} is not a valid XML file - No root element");
            
            var fontName = bitmapFontElement.GetAttributeString("name");
            var texture = _textureManager.GetTexture(bitmapFontElement.GetAttributeString("textureAsset"));
            var fixedWidth = bitmapFontElement.GetAttributeBool("fixedWidth");
            var lineHeight = bitmapFontElement.GetAttributeSingle("lineHeight", 1.0f);
            var heightScale = bitmapFontElement.GetAttributeSingle("heightScale", 1.0f);
            var minChar = Convert.ToInt32(bitmapFontElement.GetAttributeString("minChar"), 16);
            var maxChar = Convert.ToInt32(bitmapFontElement.GetAttributeString("maxChar"), 16);
            var textureId = _textureManager.GetTextureId(texture.Name);

            
            //Old Format
            var tiles = bitmapFontElement.GetAttributeVector2("tiles", new Vector2(16,8));

			var tileWidth = texture.Width / tiles.X; //The number of pixels representing an individual tile.
				
			if (fixedWidth)
			{
				for (var i = 0; i < tiles.X * tiles.Y; ++i)
					charMetrics.Add(new BitmapCharacterMetrics(0.0f, 1.0f));
			}
			else
			{
				var charsElement = bitmapFontElement.Elements("chars");

				//Read in the width of each character...
				foreach (var charElement in charsElement.Elements("char"))
				{
					var paddingLeft = charElement.GetAttributeInt32("paddingLeft") / tileWidth;
					var width = charElement.GetAttributeInt32("width") / tileWidth;
					charMetrics.Add(new BitmapCharacterMetrics(paddingLeft, width));
				}
			}

            var font = new BitmapFont(textureId, tiles, fixedWidth, heightScale, charMetrics.ToArray(), lineHeight, minChar, maxChar);
			_bitmapFonts[fontName] = font;
        }

		/// <summary>
        /// Gets a BitmapFont by key.
		/// </summary>
        public BitmapFont GetBitmapFont(string name)
        {
            if (_bitmapFonts.ContainsKey(name))
                return _bitmapFonts[name];
            
            return null;
        }
    }
}
