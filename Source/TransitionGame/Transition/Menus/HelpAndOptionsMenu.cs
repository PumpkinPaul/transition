using System;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents the help and options menu.
    /// </summary>
    public class HelpAndOptionsMenu : Menu
    {
		/// <summary>
        /// Creates a new instance of a CreditsMenu.
        /// </summary>
		public HelpAndOptionsMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            var howToPlayButton = new TextButtonControl(this.Game);
            var controlsButton = new TextButtonControl(this.Game);
            var settingsButton = new TextButtonControl(this.Game);
            var creditsButton = new TextButtonControl(this.Game);

            howToPlayButton.Position = new Vector2(0.0f, 5.0f);
            howToPlayButton.Text = "how to play";
            howToPlayButton.Click += HowToPlayButtonClick;
            howToPlayButton.Font = Theme.Font;
            howToPlayButton.FontSize = Theme.FontSize;
            howToPlayButton.Alignment = Alignment.Centre;
            howToPlayButton.HelpText = "Show the game instructions";

            controlsButton.Position = new Vector2(0.0f, 4.0f);
            controlsButton.Text = "controls";
            controlsButton.Click += ControlsButtonClick;
            controlsButton.Font = Theme.Font;
            controlsButton.FontSize = Theme.FontSize;
            controlsButton.Alignment = Alignment.Centre;
            controlsButton.HelpText = "Show the gamepad controls";

            settingsButton.Position = new Vector2(0.0f, 3.0f);
            settingsButton.Text = "settings";
            settingsButton.Click += SettingsButtonClick;
            settingsButton.Font = Theme.Font;
            settingsButton.FontSize = Theme.FontSize;
            settingsButton.Alignment = Alignment.Centre;
            settingsButton.HelpText = "configure game settings (video, audio, etc)";

            creditsButton.Position = new Vector2(0.0f, 2.0f);
            creditsButton.Text = "credits";
            creditsButton.Click += CreditsButtonClick;
            creditsButton.Font = Theme.Font;
            creditsButton.FontSize = Theme.FontSize;
            creditsButton.Alignment = Alignment.Centre;
            creditsButton.HelpText = "The people behind transition - who did what";         

            //Add the controls to the array of active controls.
            this.AddControl(howToPlayButton, 1);
            this.AddControl(controlsButton, 2);
            this.AddControl(settingsButton, 3);
            this.AddControl(creditsButton, 4);
        }

        /// <summary>
        /// Event handler for the how to play button click.
        /// </summary>
		private void HowToPlayButtonClick(object sender, EventArgs e)
		{
            this.MenuManager.AddMenu(this.MenuManager.GetMenu(typeof(HowToPlayMenu)));
		}

        /// <summary>
        /// Event handler for the controls button click.
        /// </summary>
        private void ControlsButtonClick(object sender, EventArgs e)
        {
            this.MenuManager.AddMenu(this.MenuManager.GetMenu(typeof(ControlsMenu)));
        }

        /// <summary>
        /// Event handler for the settings button click.
        /// </summary>
        private void SettingsButtonClick(object sender, EventArgs e)
        {
            this.MenuManager.AddMenu(this.MenuManager.GetMenu(typeof(SettingsMenu)));
        }

        /// <summary>
        /// Event handler for the credits button click.
        /// </summary>
        private void CreditsButtonClick(object sender, EventArgs e)
        {
            this.MenuManager.AddMenu(this.MenuManager.GetMenu(typeof(CreditsMenu)));
        }
    }
}
