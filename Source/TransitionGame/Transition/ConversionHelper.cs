using System;
using System.Globalization;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics.PackedVector;

namespace Transition
{
    /// <summary>
    /// Helper class for data conversions
    /// </summary>
    public static class ConversionHelper
    {
        /// <summary>
        /// Converts
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static float FromYaklyDegreesToRadians(int angle)
        {
            return MathHelper.Pi * (angle / 65535.0f);
        }

        /// <summary>
        /// Converts a string to a Vector2.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        public static Vector2 ToVector2(string value)
        {
            if (string.IsNullOrEmpty(value))
                return Vector2.Zero;

            var components = value.Split(',');
            if (components.Length > 1)
                return new Vector2(ToSingle(components[0].Trim()), ToSingle(components[1].Trim()));
            
            return new Vector2(ToSingle(components[0].Trim()), ToSingle(components[0].Trim()));
        }

        /// <summary>
        /// Converts a string to a Vector4
        /// </summary>
        /// <param name="value">The value to convert.</param>
        public static Vector4 ToVector4(string value)
        {
            if (string.IsNullOrEmpty(value))
                return Vector4.Zero;

            var components = value.Split(',');
            return new Vector4(ToSingle(components[0].Trim()), ToSingle(components[1].Trim()), ToSingle(components[2].Trim()), ToSingle(components[3].Trim()));
        }

        public static Box2 ToBox2(string value)
        {
            if (string.IsNullOrEmpty(value))
                return Box2.Empty;

            var components = value.Split(',');
            return new Box2(ToSingle(components[0].Trim()), ToSingle(components[1].Trim()), ToSingle(components[2].Trim()), ToSingle(components[3].Trim()));
        }

        /// <summary>
        /// Converts a string to a Color.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        public static Color ToColor(string value)
        {
            if (string.IsNullOrEmpty(value))
                return Color.White;

            if (value.StartsWith("hsv", StringComparison.Ordinal))
            {
                
            }
            else if (value.StartsWith("#", StringComparison.Ordinal))
            {
                return ColorHelper.FromHtml(value);
            }
            else if (value.StartsWith("vector3", StringComparison.Ordinal))
            {
                value = value.Replace("vector3(", string.Empty).Replace(")", string.Empty);
                var vcomponents = value.Split(',');
                return new Color(ToSingle(vcomponents[0].Trim()), ToSingle(vcomponents[1].Trim()), ToSingle(vcomponents[2].Trim()), 1.0f);
            }
            else if (value.StartsWith("vector4", StringComparison.Ordinal))
            {
                value = value.Replace("vector4(", string.Empty).Replace(")", string.Empty);
                var vcomponents = value.Split(',');
                var valpha = vcomponents.Length == 4 ? ToSingle(vcomponents[3].Trim()) : 1.0f;
                return new Color(ToSingle(vcomponents[0].Trim()), ToSingle(vcomponents[1].Trim()), ToSingle(vcomponents[2].Trim()), valpha);
            }
            //else rgb

            var components = value.Split(',');
            var alpha = components.Length == 4 ? Convert.ToByte(components[3].Trim()) : (byte)255;
            return new Color(Convert.ToByte(components[0].Trim()), Convert.ToByte(components[1].Trim()), Convert.ToByte(components[2].Trim()), alpha);
        }

        /// <summary>
        /// Converts a string to a Color.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        public static Color ToPreMultipliedColor(string value)
        {
            if (string.IsNullOrEmpty(value))
                return Color.White;

            var components = value.Split(',');
            var alpha = components.Length == 4 ? Convert.ToByte(components[3].Trim()) : (byte)255;
            var a = alpha / 255.0f;
            return new Color(
                    Convert.ToByte(components[0].Trim()) * a,
                    Convert.ToByte(components[1].Trim()) * a,
                    Convert.ToByte(components[2].Trim()) * a,
                    alpha);
        }

        public static bool ToBool(string value)
        {
            return ToBool(value, false);
        }

        public static bool ToBool(string value, bool defaultValue)
        {
            var result = defaultValue;

            if (!string.IsNullOrEmpty(value))
                result = bool.Parse(value);

            return result;
        }

        public static byte ToByte(string value)
        {
            return ToByte(value, 0);
        }

        public static byte ToByte(string value, byte defaultValue)
        {
            byte result;

            return byte.TryParse(value, NumberStyles.Integer, CultureInfo.InvariantCulture, out result) ? result : defaultValue;
        }

        public static int ToInt32(string value)
        {
            return ToInt32(value, 0);
        }

        public static int ToInt32(string value, int defaultValue)
        {
            int result;

            if (int.TryParse(value, NumberStyles.Integer, CultureInfo.InvariantCulture, out result))
                return result;
            
            return defaultValue;
        }

        public static float ToSingle(string value)
        {
            return ToSingle(value, 0.0f);
        }

        public static float ToSingle(string value, float defaultValue)
        {
            float result;

            if (float.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out result))
                return result;
            
            return defaultValue;
        }

        /// <summary>
        /// Converts a number of game ticks to a time for display purposes.
        /// </summary>
        /// <param name="ticks">The number of ticks to convert.</param>
        /// <returns>A string representing the number of ticks.</returns>
        public static string TicksToDisplayTime(int ticks)
        {
            var timeSpan = new TimeSpan((long)((10000000.0f / 60.0f) * ticks));
            return timeSpan.Minutes.ToString("00") + ":" + timeSpan.Seconds.ToString("00");
        }
    }
}
