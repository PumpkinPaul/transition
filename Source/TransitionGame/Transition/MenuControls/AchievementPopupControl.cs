using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition
{
    /// <summary>
    /// Represents a control for displaying the 'city being attacked' warning.
    /// </summary>
    public class AchievementPopupControl : MenuControl
    {			
        private enum Phase
        {
            Hidden,
            FadeIn,
            Normal,
            FadeOut,
        }

        private const float MaxMasterAlpha = 1.0f;
        private const float MinMasterAlpha = 0.0f;

        private const int MaxVisibleTicks = 180;
        private int _ticks;
        private const int MaxFadeInTicks = 30;
        private const int MaxFadeOutTicks = 30;
        
        private float _masterAlpha;
        private Vector2 _position;

        private readonly Queue<string> _achievements = new Queue<string>();

        private Phase _phase;

        public Vector2 TargetPosition;
        public Vector2 EndPosition;

        /// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;
		
		/// <summary>The size of the font.</summary>
        public Vector2 FontSize;

        /// <summary>The size of the font.</summary>
        public Vector2 TitleFontSize;

        /// <summary>The id of the texture to use for drawing.</summary>
        public TextureInfo TextureInfo;

        public Vector2 ImageScale;

        public Alignment Alignment;

        public Color TitleTint;
        public Color BackgroundTint;

        /// <summary>
        /// Initialises a new instance of an ImageControl.
        /// </summary>
		/// <param name="game">A reference to the game.</param>
		public AchievementPopupControl(BaseGame game) : base(game) 
        { 
            Font = Theme.Font;
            FontSize = Theme.FontSize;
            Visible = false;
            ImageScale = Vector2.One;
            _masterAlpha = MinMasterAlpha;
            _phase = Phase.Hidden;
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            TargetPosition = element.GetAttributeVector2("targetPosition", TargetPosition);
            EndPosition = element.GetAttributeVector2("endPosition", EndPosition);
            ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            TitleFontSize = element.GetAttributeVector2("titleFontSize", TitleFontSize);
            TextureInfo = ParseTexture(element, "textureInfo", TextureInfo);
            ImageScale = element.GetAttributeVector2("imageScale", ImageScale);
            Alignment = element.GetAttributeEnum("alignment", Alignment);
            TitleTint = element.GetAttributeColor("titleTint", TitleTint);
            BackgroundTint = element.GetAttributeColor("backgroundTint", BackgroundTint);
        }

        /// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
		public override Rect Bounds
		{
            get { return new Rect(); }
		}

        public void FadeIn(string achievementText)
        {
            _achievements.Enqueue(achievementText);

            if (_phase == Phase.FadeIn || _phase == Phase.Normal)
                return; 

            _phase = Phase.FadeIn;
            Visible = true;
        }

        public void FadeOut()
        {
            if (_phase == Phase.FadeOut || _phase == Phase.Hidden)
                return; //Already out or fading out

            _phase = Phase.FadeOut;
        }

        public void Reset()
        {
            _phase = Phase.Hidden;
            _achievements.Clear();
        }

        protected override void OnUpdate() 
        {           
            switch (_phase)
            {
                case Phase.Hidden:
                    break;

                case Phase.FadeIn:
                    if (_ticks < MaxFadeInTicks)
                        _ticks++;

                    if (_ticks >= MaxFadeInTicks)
                    {
                        _phase = Phase.Normal;
                        _masterAlpha = MaxMasterAlpha;
                        _ticks = 0;
                    }
                    else
                    {
                        var ratio = (float)_ticks / MaxFadeInTicks;
                        _masterAlpha = MathHelper.Lerp(MinMasterAlpha, MaxMasterAlpha, ratio);
                        _position.X = Interpolate.Bounce(Position.X, TargetPosition.X, ratio);
                        _position.Y = Interpolate.Bounce(Position.Y, TargetPosition.Y, ratio);
                    }
                    break;

                case Phase.Normal:
                    if (_ticks < MaxVisibleTicks)
                        _ticks++;

                    if (_ticks >= MaxVisibleTicks)
                    {
                        _ticks = 0;
                        FadeOut();
                    }
                    break;

                case Phase.FadeOut:
                    if (_ticks < MaxFadeOutTicks)
                        _ticks++;

                    if (_ticks >= MaxFadeOutTicks)
                    {
                        _ticks = 0;
                        _achievements.Dequeue();
                        _masterAlpha = MinMasterAlpha;

                        if (_achievements.Count == 0)
                        {
                            _phase = Phase.Hidden;
                            Visible = false;
                        }
                        else 
                        {
                            _phase = Phase.FadeIn;
                        }
                    }
                    else
                    {
                        var ratio = (float)_ticks / MaxFadeOutTicks;
                        _masterAlpha = MathHelper.Lerp(MaxMasterAlpha, MinMasterAlpha, ratio);
                        _position.X = MathHelper.Lerp(TargetPosition.X, EndPosition.X, ratio);
                        _position.Y = MathHelper.Lerp(TargetPosition.Y, EndPosition.Y, ratio);
                    }
                    break;
            }
        }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
		{
            if (_achievements.Count == 0)
                return;

            const string titleText = "Medal Gained";
            const float padding = 0.35f;

            var text = _achievements.Peek();

            var position = _position;
            var titleBounds = Font.GetBounds(position, TitleFontSize, titleText, Alignment.Centre);
            var textOffset = new Vector2(0, (TitleFontSize.Y / 2.0f) + (FontSize.Y / 2.0f));
            var textBounds = Font.GetBounds(position - textOffset, FontSize, titleText, Alignment.Centre);

            //Draw background
            var pixelTextureInfo = Game.TextureManager.GetTextureInfo("Pixel");
            var bgTint = BackgroundTint;       
            bgTint *= Overlay.Opacity * _masterAlpha;  
            var textLength = Font.GetTextLength(text, FontSize.X);
            var q = new Quad(titleBounds.Top + padding, position.X - (textLength / 2) - padding, textBounds.Bottom - padding, position.X + (textLength / 2) + padding);
            Game.Renderer.DrawSprite(ref q, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, bgTint, RenderStyle, Layer);

            //Draw an icon
            //var iconTint = TitleTint;       
            //iconTint *= this.Overlay.Opacity * _masterAlpha;  
            //var iconQuad = new QuadStruct(new Vector2(titleBounds.Left - (ImageScale.X / 2.0f) - 0.1f, position.Y), _imageScale);
            //_game.GeometryBatch.DrawSprite(ref iconQuad, TextureInfo.TextureId, TextureInfo.SourceRect, iconTint.ToColor(), RenderStyle);

            //Draw title text
            var tint = TitleTint;         
            var tintColor = tint;
            tintColor *= Overlay.Opacity * _masterAlpha;
            Game.Renderer.DrawString(Font, position, TitleFontSize, titleText, Alignment.Centre, tintColor, RenderStyle, Layer);

            //Draw achievement text
            tint = Tint;
            tint *= Overlay.Opacity * _masterAlpha;
            var textColor = tint;
            position.Y -= textOffset.Y;
            Game.Renderer.DrawString(Font, position, FontSize, text, Alignment.Centre, textColor, RenderStyle, Layer);   

            //Draw a border
            var bounds = q.Rect;
            const float lineWidth = 0.04f;

            tint = Tint;
            tint *= Overlay.Opacity * _masterAlpha * 0.5f;

            var corner = new Quad();
            corner.Rect = new Rect(bounds.Top, bounds.Left, bounds.Top - lineWidth, bounds.Right);
            Game.Renderer.DrawSprite(ref corner, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, tint, RenderStyle, Layer);

            corner.Rect = new Rect(bounds.Bottom + lineWidth, bounds.Left, bounds.Bottom, bounds.Right);
            Game.Renderer.DrawSprite(ref corner, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, tint, RenderStyle, Layer);

            corner.Rect = new Rect(bounds.Top, bounds.Left, bounds.Bottom, bounds.Left + lineWidth);
            Game.Renderer.DrawSprite(ref corner, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, tint, RenderStyle, Layer);

            corner.Rect = new Rect(bounds.Top, bounds.Right - lineWidth, bounds.Bottom, bounds.Right);
            Game.Renderer.DrawSprite(ref corner, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, tint, RenderStyle, Layer);
		}
    }
}
