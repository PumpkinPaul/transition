using System;
using Microsoft.Xna.Framework;
using Transition.Menus;

namespace Transition.MenuControls.UserControls
{
    /// <summary>Represents a base control that displays hints for controller actions.</summary>
    public class ColorControl
    {
        public class ColorChangedEventArgs : EventArgs
        {
            public bool IsDragging { get; private set; }

            public static readonly ColorChangedEventArgs Dragging = new ColorChangedEventArgs { IsDragging = true };
            public new static readonly ColorChangedEventArgs Empty = new ColorChangedEventArgs();
        }

        public event EventHandler<ColorChangedEventArgs> ColorChanged;

        bool _raiseColorChanged;

        readonly SliderControl _redSlider;
        readonly SliderControl _greenSlider;
        readonly SliderControl _blueSlider;
        readonly SliderControl _alphaSlider;
        readonly TextBoxControl _hexText;
        readonly ImageControl _preview;

        public ColorControl(Overlay overlay, string name)
        {
            _redSlider = overlay.GetControl<SliderControl>(name + ".redSlider");
            _greenSlider = overlay.GetControl<SliderControl>(name + ".greenSlider");
            _blueSlider = overlay.GetControl<SliderControl>(name + ".blueSlider");
            _alphaSlider = overlay.GetControl<SliderControl>(name + ".alphaSlider");
            _hexText = overlay.GetControl<TextBoxControl>(name + ".hexText");
            _preview = overlay.GetControl<ImageControl>(name + ".preview");

            var redText = overlay.GetControl<TextBoxControl>(name + ".redText");
            var greenText = overlay.GetControl<TextBoxControl>(name + ".greenText");
            var blueText = overlay.GetControl<TextBoxControl>(name + ".blueText");
            var alphaText = overlay.GetControl<TextBoxControl>(name + ".alphaText");

            var preAlphaButton = overlay.GetControl<TextButtonControl>(name + ".preAlphaButton");

            SliderValueChanged(_redSlider, redText);
            SliderValueChanged(_greenSlider, greenText);
            SliderValueChanged(_blueSlider, blueText);
            SliderValueChanged(_alphaSlider, alphaText);

            SliderMouseUp(_redSlider);
            SliderMouseUp(_greenSlider);
            SliderMouseUp(_blueSlider);
            SliderMouseUp(_alphaSlider);

            ChannelTextLostFocus(_redSlider, redText);
            ChannelTextLostFocus(_greenSlider, greenText);
            ChannelTextLostFocus(_blueSlider, blueText);
            ChannelTextLostFocus(_alphaSlider, alphaText);

            _hexText.LostFocus += (sender, e) => 
            { 
                Color = ColorHelper.FromHtml(_hexText.Text);
            };

            preAlphaButton.Click += (sender, e) => 
            { 
                Color = new Color(_redSlider.Value, _greenSlider.Value, _blueSlider.Value) * _alphaSlider.NormalizedValue;
            };


        }

        public Color Color
        {
            get { return _preview.Tint; }
            set 
            {
                var oldColor = _preview.Tint;

                _preview.Tint = value;

                _redSlider.Value = value.R;
                _greenSlider.Value = value.G;
                _blueSlider.Value = value.B;
                _alphaSlider.Value = value.A;

                var isDragging = _redSlider.IsDragging || _greenSlider.IsDragging || _blueSlider.IsDragging || _alphaSlider.IsDragging;

                if (oldColor != value || _raiseColorChanged)
                    OnColorChanged(isDragging ? ColorChangedEventArgs.Dragging : ColorChangedEventArgs.Empty);
            }
        }

        void SliderValueChanged(SliderControl sliderControl, TextBoxControl channelTextBox)
        {
            sliderControl.ValueChanged += (sender, e) => 
            {
                Color = new Color(_redSlider.NormalizedValue, _greenSlider.NormalizedValue, _blueSlider.NormalizedValue, _alphaSlider.NormalizedValue);
                channelTextBox.Text = sliderControl.Value.ToString();

                _hexText.Text = Color.R.ToString("x2") + Color.G.ToString("x2") + Color.B.ToString("x2") + Color.A.ToString("x2"); 
            };
        }

        void SliderMouseUp(SliderControl sliderControl)
        {
            sliderControl.MouseUp += (sender, e) => 
            {
                _raiseColorChanged = true;
                Color = new Color(_redSlider.NormalizedValue, _greenSlider.NormalizedValue, _blueSlider.NormalizedValue, _alphaSlider.NormalizedValue);
                _raiseColorChanged = false;
            };
        }
        
        static void ChannelTextLostFocus(SliderControl sliderControl, TextBoxControl channelTextBox)
        {
            channelTextBox.LostFocus += (sender, e) => 
            { 
                byte value;
                if (byte.TryParse(channelTextBox.Text, out value))
                    sliderControl.Value = value;
                else
                    channelTextBox.ResetText();    
            };
        }

        void OnColorChanged(ColorChangedEventArgs e)
        {
            var handler = ColorChanged;
            if (handler != null)
                handler(this, e);
        }
    }
}
