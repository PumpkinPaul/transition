namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public class StandardBullet : Bullet
    {
        /// <summary>
        /// Creates a new instance of a StandardBullet.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public StandardBullet(TransitionGame game) : base(game) { }

        /// <summary>
        /// Occurs when the actor is added to the scene.
        /// </summary>
        protected override void OnAddedToScene()
        {
            _game.PlaySound("StandardBulletSpawn");

            base.OnAddedToScene();
        }
    }
}
