using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Transition.Rendering;

namespace Transition.CourseSystem
{
    public class Course 
    {
        public Vector3 CameraOffset { get; set; }
        public bool CameraLayerVisible { get; set; }
        public bool DecalLayerVisible { get; set; }
        public bool TextLayerVisible { get; set; }
        public bool TilesLayerVisible { get; set; }
        public bool ActorsLayerVisible { get; set; }

        public string FileName { get; set; }
        public string Name { get; set; }
        public Color BackgroundColor { get; set; }
        public readonly List<Tile> Tiles = new List<Tile>();
        public List<Label> Labels { get; set; }
        public CameraPath CameraPath { get; set; }

        public BitmapFont LabelFont { get { return Theme.Current.GetFont(Theme.Fonts.Default); } }

        public static Course Load(string filename)
        {
            var fileContents = File.ReadAllText(filename);
            var course = JsonConvert.DeserializeObject<Course>(fileContents);
            
            course.FileName = filename;
            return course;
        }

        public void Save(string filename = null)
        {
            var contents = JsonConvert.SerializeObject(this);
            File.WriteAllText(filename ?? FileName, contents);
        }
        
        public static Course CreateDefaultCourse()
        {
            var course = new Course
            {
                BackgroundColor = new Color(32, 32, 32, 255),
                Name = DateTime.Now.ToString("yyyyMMddHHMMss")
            };
            course.FileName = Path.Combine(Platform.ResourcesPath, "Courses", course.Name + ".course");
            course.CameraPath.Vertices = new List<Vector2>
            {
                new Vector2(0.0f, 0.0f),
                new Vector2(-2.0f, 2.0f),
                new Vector2(2.0f, 8.0f),
                new Vector2(0.0f, 10.0f)
            };

            return course;
        }

        public Course()
        {
            CameraLayerVisible = true;
            DecalLayerVisible = true;
            TextLayerVisible = true;
            TilesLayerVisible = true;

            Tiles = new List<Tile>();
            Labels = new List<Label>();
            CameraPath = new CameraPath();
        }

        public Vector2 GetCameraPosition(float t)
        {
            t = CameraPath.Clamp(t);
            var segment = (int)MathHelper.Clamp((int)t, 0, CameraPath.PathSegments - 1);
            var startPointIdx = segment * 3;
            t -= segment;
            
            return CameraPath.Bezier(t, CameraPath.Vertices[startPointIdx], CameraPath.Vertices[startPointIdx + 1], CameraPath.Vertices[startPointIdx + 2], CameraPath.Vertices[startPointIdx + 3]);
        }

        public void Draw()
        {
            //Temp attempt at draw character
            var game = TransitionGame.Instance;

            game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);

            if (TilesLayerVisible)
            {
                foreach (var tile in Tiles)
                {
                    var textureInfo = game.TextureManager.GetTextureInfo(tile.Texture);
                    var q = new Quad(tile.Position, tile.Scale, tile.Rotation);
                    game.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, tile.Color, TransparentRenderStyle.Instance);
                }
            }

            if (TextLayerVisible)
            {     
                foreach(var label in Labels)
                {
                    game.Renderer.DrawString(LabelFont, label.Position, label.FontSize, label.Rotation, label.Text, label.Alignment, label.Color, TransparentRenderStyle.Instance);
                }
            }

            if (ActorsLayerVisible)
            {

            }

            if (CameraLayerVisible)
            {
                //Debug - draw camera path
                const float stepTime = 0.025f;
                var p1 = GetCameraPosition(0.0f);
                var maxTime = (float)CameraPath.PathSegments;

                for(var t = stepTime; t <= maxTime; t += stepTime)
                {
                    var cameraPosition = GetCameraPosition(t);
                    var p2 = new Vector2(cameraPosition.X, cameraPosition.Y);

                    game.Renderer.DrawLine(p1, p2, Color.Black, TransparentRenderStyle.Instance, 0.15f);
                    p1 = p2;
                }
               
                for (var pointId = 0; pointId < CameraPath.Vertices.Count; pointId++)
                {
                    var pt = CameraPath.Vertices[pointId];
                    var textureName = "BezierControlPoint";
                    var pointSize = new Vector2(0.45f);
                    var tint = Color.Gray;
                    if (pointId % 3 == 0)
                    {
                        textureName = "BezierPoint";
                        pointSize = new Vector2(0.75f);
                        tint = Color.White;
                    }    

                    var textureInfo = game.TextureManager.GetTextureInfo(textureName);

                    var q = new Quad(pt, pointSize, 0);
                    game.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, tint, TransparentRenderStyle.Instance);

                    if (pointId % 3 == 0)
                    {
                        if (pointId > 0)
                            game.Renderer.DrawLine(CameraPath.Vertices[pointId], CameraPath.Vertices[pointId - 1], Color.Gray, TransparentRenderStyle.Instance, 0.1f);

                        if (pointId < CameraPath.Vertices.Count - 1)
                            game.Renderer.DrawLine(CameraPath.Vertices[pointId], CameraPath.Vertices[pointId + 1], Color.Gray, TransparentRenderStyle.Instance, 0.1f);
                    }
                }
            }
            game.Renderer.End();
        }
    }
}
