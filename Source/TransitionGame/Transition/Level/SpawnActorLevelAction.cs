using System;
using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// .
    /// </summary>
    public class SpawnActorLevelAction : LevelAction
    {
        public string Name;
        public int Count;
        public Vector2 Location;
        public LevelActionLocationType LocationType;

        public override void Execute(LevelManager levelManager)
        {
            levelManager.SpawnActor(this);
        }
    }
}