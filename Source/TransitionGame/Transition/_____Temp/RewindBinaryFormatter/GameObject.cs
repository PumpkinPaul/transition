﻿using System;
using Microsoft.Xna.Framework;

namespace Transition._____Temp.RewindBinaryFormatter
{
    [Serializable]
    public class Entity
    {
        public Vector2 Position;
        public Vector2 Scale = Vector2.One;

        public virtual void BeforeRewind() { }
        public virtual void StateLoaded(bool rewinding) { }

        public virtual void Update(bool wasRewinding) { }

        public virtual void Draw() { }
    }
}