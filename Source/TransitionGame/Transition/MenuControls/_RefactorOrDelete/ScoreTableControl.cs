using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control for drawing a table of scores.
    /// </summary>
    public class ScoreTableControl : MenuControl
    {		
        RepeatTemplate _repeatTemplate;

        /// <summary>The number of items per page.</summary>
        public const int PageSize = 10;

        /// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;

        /// <summary>The size of the font.</summary>
        public Vector2 FontSize;

        /// <summary>The colour of the items in the table</summary>
        public Color ItemsTint;

        /// <summary>Index of the page in the score table to draw.</summary>
        public int PageIndex;

        /// <summary>A reference to the score table to draw.</summary>
        public ScoreTable ScoreTable;
		
		/// <summary>
		/// .
		/// </summary>
        public ScoreTableControl(BaseGame game) : base(game) { }

        public override void Load(XElement element)
        {
            base.Load(element);

            ItemsTint = element.GetAttributeColor("itemsTint", ItemsTint);
            Font = ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);

            //Might be repeat (or dragons? but definitely, possibly repeat)
            ParseRepeaterTemplate(element, "repeater");
        }
			
        public void ParseRepeaterTemplate(XElement element, string templateName)
        {
            var templateElement = element.Element(templateName);
            if (templateElement == null)
                return;

            //Ok, we have an item template so might have some controls that need loading here.
            //Kinda like an asp:Repeater thing going on here. How?
            _repeatTemplate = new RepeatTemplate(templateElement, null, Game);
        }

        /// <summary>
		/// .
		/// </summary>
		protected override void OnDraw()
		{
            if (_repeatTemplate == null)
                return;

            //var tint = Tint;
            //tint *= Overlay.Opacity;

            //Required for paging the dataset
		    var startIndex = PageIndex * PageSize;
            var endIndex = startIndex + PageSize;
            var rank = (PageIndex * PageSize) + 1;

            //Start of the repeating template
            var position = Position + _repeatTemplate.Position;
            for (var i = startIndex; i < endIndex; ++i)
            {
                var scoreItem = ScoreTable.Scores[i];
                scoreItem.Rank = rank;

                ////Highlight the current score.
                //var tc = tint;
                //if (i == ScoreTable.CurrentScoreId)
                //    tc = currentScoreTint;

                foreach (var control in _repeatTemplate.Controls)
                {
                    //Need to set the values of the control here...
                    //control.DataBind(scoreItem);
                    _repeatTemplate.DataBind(control, scoreItem);
                    
                    //Temporarily diddle the position of this control in the repeater template - we'll reset it when it's been drawn in the corrct position
                    var tempPosition = control.Position;
                    control.Position += position;
                    
                    control.Overlay = Overlay;
                    control.Draw();

                    control.Position = tempPosition;
                }

                rank++;
                position += _repeatTemplate.Offset;
            }
		}
		
        public int SetPageIndex()
        {
            if (ScoreTable.CurrentScoreId != -1)
                PageIndex = ScoreTable.CurrentScoreId / PageSize;
            else
                PageIndex = 0;

            return PageIndex;
        }
    }
}
