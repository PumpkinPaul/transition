using System;

namespace Transition
{
    [Serializable]
    public sealed class CubicBezierInterpolator : InterpolatorBase
    {
	    private const float Epsilon = 1.0f / 4096.0f;
	    
	    public static readonly CubicBezierInterpolator Ease = new CubicBezierInterpolator(0.25f, 0.1f, 0.25f, 1.0f);
	    public static readonly CubicBezierInterpolator EaseIn = new CubicBezierInterpolator(0.42f, 0.0f, 1.0f, 1.0f);
	    public static readonly CubicBezierInterpolator EaseOut = new CubicBezierInterpolator(0.0f, 0.0f, 0.58f, 1.0f);
	    public static readonly CubicBezierInterpolator EaseInOut = new CubicBezierInterpolator(0.42f, 0.0f, 0.58f, 1.0f);

	    private readonly float _x0, _y0, _x1, _y1;

	    public CubicBezierInterpolator(float x0, float y0, float x1, float y1) 
        {
		    _x0 = x0;
		    _y0 = y0;
		    _x1 = x1;
		    _y1 = y1;
	    }

	    public override float Interpolate(float a, float b, float ratio) 
        {
		    if (a == b) 
			    return a;
		    
		    if (ratio < 0.0f) 
			    ratio = 0.0f;
		    else if (ratio > 1.0f) 
			    ratio = 1.0f;
		    
		    var cx = 3.0f * _x0;
		    var bx = 3.0f * (_x1 - _x0) - cx;
		    var ax = 1.0f - cx - bx;
		    var cy = 3.0f * _y0;
		    var by = 3.0f * (_y1 - _y0) - cy;
		    var ay = 1.0f - cy - by;

		    // Convert from input time to parametric value in curve, then from that to output time.
		    var r2 = Solve(ratio, ax, ay, bx, by, cx, cy);
		    return a * (1f - r2) + b * r2;
	    }

	    private static float SampleCurveX(float t, float ax, float bx, float cx) 
        {
		    return ((ax * t + bx) * t + cx) * t;
	    }

	    private static float SampleCurveY(float t, float ay, float by, float cy) 
        {
		    return ((ay * t + by) * t + cy) * t;
	    }

	    private static float SampleCurveDerivativeX(float t, float ax, float bx, float cx) 
        {
		    return (3.0f * ax * t + 2.0f * bx) * t + cx;
	    }

	    private static float Solve(float x, float ax, float ay, float bx, float by, float cx, float cy) 
        {
		    return SampleCurveY(SolveCurveX(x, ax, bx, cx), ay, by, cy);
	    }

	    // Given an x value, find a parametric value it came from.
	    private static float SolveCurveX(float x, float ax, float bx, float cx) 
        {
	        var t2 = x;

	        //First try a few iterations of Newton's method -- normally very fast.
		    for (var i = 0; i < 8; i ++) 
            {
			    var x2 = SampleCurveX(t2, ax, bx, cx) - x;

			    if (Math.Abs(x2) < Epsilon) 
				    return t2;

			    var d2 = SampleCurveDerivativeX(t2, ax, bx, cx);

			    if (Math.Abs(d2) < 1e-6f) 
				    break;
			    
			    t2 = t2 - x2 / d2;
		    }

		    // Fall back to the bisection method for reliability.
		    var t0 = 0.0f;
		    var t1 = 1.0f;
		    t2 = x;

		    if (t2 < t0) 
			    return t0;
		    
            if (t2 > t1) 
			    return t1;
		    

		    while (t0 < t1) 
            {
			    float x2 = SampleCurveX(t2, ax, bx, cx);
			    if (Math.Abs(x2 - x) < Epsilon) 
				    return t2;
			    
                if (x > x2) 
				    t0 = t2;
			    else 
				    t1 = t2;
			    
			    t2 = (t1 - t0) * 0.5f + t0;
		    }

		    return t2; // Failure.
	    }

	    public float X0 { get { return _x0; } }
		public float Y0 { get { return _y0; } }
        public float X1 { get { return _x1; } }
        public float Y1 { get { return _y1; } }
    }
}
