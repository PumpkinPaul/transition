#include "CommonVertex.inc"

struct PS_OUT
{
   float4 Color:   COLOR;
};

PS_OUT PixelShaderFunction(VS_OUT input) : COLOR0
{
	PS_OUT Out;
    float2 v = input.TextureCoords;
    
    float4 tex = tex2D(baseSampler, v);

	Out.Color.r = mul(input.Colour.r, tex.a);
    Out.Color.g = mul(input.Colour.g, tex.a);
    Out.Color.b = mul(input.Colour.b, tex.a);
    Out.Color.a = mul(input.Colour.a, tex.a);

    return Out;
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
