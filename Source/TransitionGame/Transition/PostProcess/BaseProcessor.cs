using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.PostProcess
{
    /// <summary>
    /// A base class for post processing.
    /// </summary>
    public abstract class BaseProcessor 
    {
        /// <summary>A reference to the game.</summary>
        protected readonly BaseGame Game;

        /// <summary></summary>
        protected Effect Effect;

        /// <summary>A flag indicating whether the processor is available for the application</summary>
        /// <remarks>Generally this will be set via game options</remarks>
        public bool Enabled { get; set; }

        /// <summary>A flag indicating whether the processor is available for this game tick</summary>
        /// <remarks>Generally this will be set each frame depending on certain game state</remarks>
        /// <example>Deactivate ColourShifting if menu is visible.</example>
        public bool Active { get; set; }

        /// <summary>
        /// Initialises a new instance of a <see cref="BaseProcessor">PostProcessEffect</see>.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        protected BaseProcessor(BaseGame game)
        {
            Game = game;
            Enabled = true;
        }

        /// <summary>
        /// Loads content for the post processing effect.
        /// </summary>
        public void LoadContent()
        {
            OnLoadContent();
        }

        /// <summary>
        /// Occurs when a derived effect needs to load content.
        /// </summary>
        protected abstract void OnLoadContent();

        /// <summary>
        /// Unloads content for the post processing effect.
        /// </summary>
        public void UnloadContent()
        {
            Effect.Dispose();

            OnUnloadContent();
        }

        /// <summary>
        /// Occurs when a derived effect needs to load content.
        /// </summary>
        protected abstract void OnUnloadContent();

        public virtual void ResolutionChanged() { }

        public void PreRender() 
        { 
            OnPreRender();
        }

        protected virtual void OnPreRender() { }

        /// <summary>
        /// Applies the effect to the scene.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="deltaTime">Time passed since the last frame.</param>
        /// <param name="texture">The texture to draw.</param>
        /// <param name="renderTarget">A new render target for the device, or null to set the device render target to the back buffer of the device.</param>
        public bool Draw(SpriteBatch spriteBatch, float deltaTime, Texture2D texture, RenderTarget2D renderTarget)
        {
            //Do common stuff here before deferring to processor...

            //...ok, now render the effect(s).
            return OnDraw(spriteBatch, deltaTime, texture, renderTarget);
        }

        /// <summary>
        /// Draws the post processing effect.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="deltaTime">Time passed since the last frame.</param>
        /// <param name="texture">The texture to draw.</param>
        /// <param name="renderTarget"></param>
        protected virtual bool OnDraw(SpriteBatch spriteBatch, float deltaTime, Texture2D texture, RenderTarget2D renderTarget)
        {
            Game.GraphicsDevice.SetRenderTarget(renderTarget);

            var pp = Game.GraphicsDevice.PresentationParameters;
            var destinationRect = new Rectangle(0, 0, pp.BackBufferWidth,pp.BackBufferHeight);
     
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque, null, null, null, Effect);

            foreach (var pass in Effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                spriteBatch.Draw(texture, destinationRect, Color.White);
            }

            spriteBatch.End();

            return true;
        }
    }
}
