using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Transition.GamePhases;
using Transition.Input;
using Transition.Menus;
using Transition.Rendering;

namespace Transition._____Temp.TestPhaseBlank
{
    /// <summary>
    /// Test playground
    /// </summary>
    public class TestPhaseBlank : GamePhase
    {
        bool _initialised;
        public TestPhaseBlank(BaseGame game) : base(game) { }

        protected override void OnCreate()
        {
            Game.ScreenFlashManager.ClearColor = Color.White;
            //Game.ScreenFlashManager.ClearColor = Color.Black;

            
        }

        protected override void OnUpdate()
        {
            if (!_initialised)
            {
                Game.MenuManager.AddMenu(typeof(TestMenu));
                _initialised = true;
            }

            if(KeyboardHelper.IsKeyJustReleased(Keys.T) && KeyboardHelper.IsKeyPressed(Keys.LeftControl))
                Game.MenuManager.AddMenu(typeof(TestMenu));
        }

        protected override void OnRender()
        {
            TransitionGame.Instance.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);

            //var tectureInfo = TransitionGame.Instance.TextureManager.GetTextureInfo("Pixel");

            //var q = new Quad(new Vector2(-3), new Vector2(0.5f), 0);
            //TransitionGame.Instance.Renderer.DrawSprite(ref q, tectureInfo.TextureId, tectureInfo.SourceRect, Color.Gray, TransparentRenderStyle.Instance, 0);

            //q = new Quad(new Vector2(3), new Vector2(0.5f), 0);
            //TransitionGame.Instance.Renderer.DrawSprite(ref q, tectureInfo.TextureId, tectureInfo.SourceRect, Color.LightCoral, TransparentRenderStyle.Instance, 0);

            ////Game.Renderer.DrawXXX(ref q, Game.TextureManager.GetTextureId("RingMask"), Rect.TextureRect, Color.AliceBlue, CircleGaugeRenderStyle.Instance, 0.33);

            //// 1-----3
            //// |\    |
            //// |  \  |
            //// |    \|
            //// 0-----2
            //var geometry = new Geometry<VertexPositionColorTextureFloat>
            //{
            //    Indices = Renderer.QuadIndices,
            //    Layer = 0,
            //    TextureId = Game.TextureManager.GetTextureId("RingMask"),
            //    RenderStyle = CircleGaugeRenderStyle.Instance
            //};

            //q = new Quad(new Vector2(-3, 3), new Vector2(3.0f), 0);
            //var value = 0.7f;
            //var color = Color.Red;  
            //geometry.Vertices.Add(new VertexPositionColorTextureFloat(q.BottomLeft, color, new Vector2(0,0), value));
            //geometry.Vertices.Add(new VertexPositionColorTextureFloat(q.TopLeft, color, new Vector2(0, 1), value));
            //geometry.Vertices.Add(new VertexPositionColorTextureFloat(q.BottomRight, color, new Vector2(1, 0), value));
            //geometry.Vertices.Add(new VertexPositionColorTextureFloat(q.TopRight, color, new Vector2(1, 1), value));
            
            //TransitionGame.Instance.Renderer.DrawGeometry(geometry);

            //var geometry2 = new Geometry<VertexPositionColorTextureFloat>
            //{
            //    Indices = Renderer.QuadIndices,
            //    Layer = 0,
            //    TextureId = Game.TextureManager.GetTextureId("RingMask"),
            //    RenderStyle = CircleGaugeRenderStyle.Instance
            //};

            //q = new Quad(new Vector2(-3, -3), new Vector2(3.0f), 0);
            //value = 0.3f;
            //color = Color.Blue;
            //geometry2.Vertices.Add(new VertexPositionColorTextureFloat(q.BottomLeft, color, new Vector2(0, 0), value));
            //geometry2.Vertices.Add(new VertexPositionColorTextureFloat(q.TopLeft, color, new Vector2(0, 1), value));
            //geometry2.Vertices.Add(new VertexPositionColorTextureFloat(q.BottomRight, color, new Vector2(1, 0), value));
            //geometry2.Vertices.Add(new VertexPositionColorTextureFloat(q.TopRight, color, new Vector2(1, 1), value));

            //TransitionGame.Instance.Renderer.DrawGeometry(geometry2);
            
            TransitionGame.Instance.Renderer.End();
        }
    }
}