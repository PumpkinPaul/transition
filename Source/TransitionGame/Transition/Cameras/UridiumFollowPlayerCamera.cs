using Microsoft.Xna.Framework;

namespace Transition.Cameras
{
    /// <summary>
    /// Represents a standard game camera.
    /// </summary>
    public class UridiumFollowPlayerCamera : Camera
    {
        public UridiumFollowPlayerCamera(BaseGame game) : base(game) { }

        public override void Render() 
        {
            //Track the ships's position
            if (Game.ActivePlayer.Ship == null)
                return;
            
            Position.X = Game.ActivePlayer.Ship.Position.X;
            View = Matrix.CreateTranslation(new Vector3(-Position.X, 0, Position.Z));
        }
    }
}
