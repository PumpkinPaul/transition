/*
 * Copyright 2008  Reg Whitton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace Transition.Eval
{	
	class Tokeniser 
    {
		internal static char StartNewExpression = '(';
	
		private readonly string _string;
		private int _position;
		private Operator _pushedBackOperator;
	
		internal Tokeniser(String stringx) 
        {
			_string = stringx;
			_position = 0;
		}
	
		internal int GetPosition() {
			return _position;
		}
	
		internal void SetPosition(int position) {
			_position = position;
		}
	
		internal void PushBack(Operator op) {
			_pushedBackOperator = op;
		}
	
		internal Operator GetOperator(char endOfExpressionChar) {
			/* Use any pushed back op. */
			if (_pushedBackOperator != null) {
				var op = _pushedBackOperator;
				_pushedBackOperator = null;
				return op;
			}
	
			/* Skip whitespace */
			var len = _string.Length;
			var ch = (char)0;
			while (_position < len && Char.IsWhiteSpace(ch = _string[_position])) 
				_position++;
			
			if (_position == len) 
            {
				if (endOfExpressionChar == 0) 
					return Operator.End;
				
                throw new InvalidOperationException("missing " + endOfExpressionChar);
				
			}
	
			_position++;
			if (ch == endOfExpressionChar) 
				return Operator.End;
			
	
			switch (ch) 
            {
				case '+': return Operator.Add;
				case '-': return Operator.Subtract;
				case '/': return Operator.Divide;
				case '%': return Operator.Remainder;
				case '*': return Operator.Multiply;
				case '?': return Operator.Ternary;
				
				case '>': 
                {
					if (_position < len && _string[_position] == '=') 
                    {
						_position++;
						return Operator.GreaterThanOrEqual;
					}
					return Operator.GreaterThan;
				}
				case '<': 
                {
					if (_position < len) 
                    {
						switch (_string[_position]) 
                        {
							case '=':
								_position++;
								return Operator.LessThanOrEqual;

							case '>':
								_position++;
								return Operator.NotEqual;
						}
					}
					return Operator.LessThan;
				}
				case '=': 
                {
					if (_position < len && _string[_position] == '=') 
                    {
						_position++;
						return Operator.Equal;
					}
					throw new InvalidOperationException("use == for equality at position " + _position);
				}
				case '!': 
                {
					if (_position < len && _string[_position] == '=')
                    {
						_position++;
						return Operator.NotEqual;
					}
					throw new InvalidOperationException("use != or <> for inequality at position " + _position);
				}
				case '&': 
                {
					if (_position < len && _string[_position] == '&') 
                    {
						_position++;
						return Operator.And;
					}
					throw new InvalidOperationException("use && for AND at position " + _position);
				}
				case '|':
                {
					if (_position < len && _string[_position] == '|')
                    {
						_position++;
						return Operator.Or;
					}
					throw new InvalidOperationException("use || for OR at position " + _position);
				}
				default: 
                {
					/* Is this an identifier name for an op function? */
					if (Character.IsIdentifierStart(ch)) 
                    {
						var start = _position - 1;
						while (_position < len && Character.IsIdentifierPart(_string[_position])) 
							_position++;
	
						var name = _string.Substring(start, _position - start);
						if (name.Equals("pow")) 
							return Operator.Power;
					}
					throw new InvalidOperationException("op expected at position " + _position + " instead of '" + ch + "'");
				}
			}
		}
	
		/**
		 * Called when an operand is expected next.
		 * 
		 * @return one of:
		 *         <UL>
		 *         <LI>a {@link decimal} value;</LI>
		 *         <LI>the {@link String} name of a variable;</LI>
		 *         <LI>{@link Tokeniser#START_NEW_EXPRESSION} when an opening parenthesis is found:</LI>
		 *         <LI>or {@link Operator} when a unary op is found in front of an operand</LI>
		 *         </UL>
		 * @
		 *             if the end of the string is reached unexpectedly.
		 */
		internal Object GetOperand() 
        {
			/* Skip whitespace */
			var len = _string.Length;
			var ch = (char)0;

			while (_position < len && Char.IsWhiteSpace(ch = _string[_position])) 
				_position++;
			
			if (_position == len)
				throw new InvalidOperationException("operand expected but end of string found");
	
			if (ch == '(') 
            {
				_position++;
				return StartNewExpression;
			} 

            if (ch == '-')
            {
				_position++;
				return Operator.Negate;
			} 

            if (ch == '+') 
            {
				_position++;
				return Operator.Plus;
			} 

            if (ch == '.' || Char.IsDigit(ch)) 
				return GetBigDecimal();
			
            if (Character.IsIdentifierStart(ch) || ch == '$') 
            {
				var start = _position++;
				while (_position < len && Character.IsIdentifierStart(_string[_position])) 
					_position++;
	
				var name = _string.Substring(start, _position - start);
				/* Is variable name actually a keyword unary op? */
				if (name.Equals("abs")) 
					return Operator.Absolute;

                if (name.Equals("int")) 
					return Operator.Int;
				
				/* Return variable name */
				return name;
			}
			throw new InvalidOperationException("operand expected but '" + ch + "' found");
		}
	
		private decimal GetBigDecimal() 
        {
			var len = _string.Length;
			var start = _position;
			char ch;
	
			while (_position < len && (Char.IsDigit(ch = _string[_position]) || ch == '.')) 
				_position++;
	
			/* Optional exponent part including another sign character. */
			if (_position < len && ((ch = _string[_position]) == 'E' || ch == 'e')) 
            {
				_position++;
				if (_position < len && ((ch = _string[_position]) == '+' || ch == '-')) 
					_position++;
				
				while (_position < len && Char.IsDigit(_string[_position]))
					_position++;
			}
			return decimal.Parse(_string.Substring(start, _position - start), System.Globalization.CultureInfo.InvariantCulture);
		}
	
		//@Override
		public override String ToString() 
        {
			return _string.Substring(0, _position) + ">>>" + _string.Substring(_position);
		}
	}
}

