﻿#if WINDOWS_STOREAPP
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Windows.Storage;
    using Windows.Storage.Search; 

    namespace System.IO
    {
        public class FileInfo
        {
            public FileInfo(string path)
            {
                var slashPosition = path.LastIndexOf(@"\");
                Name = slashPosition == -1 ? path : path.Substring(slashPosition + 1);

                var dotPosition = Name.LastIndexOf(@".");
                Extension = dotPosition == -1 ? string.Empty : Name.Substring(dotPosition);
            }

            public string Name { get; private set; }
            public string Extension { get; private set; }
        }
    }
#endif
