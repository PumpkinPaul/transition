namespace Transition.Achievements
{
    /// <summary>
    /// Abstract base class for achievements
    /// </summary>
    public abstract class Achievement
    {			
        public string Guid { get; private set; }			
        public string Description { get; private set; }

        public bool Completed { get; protected set; }
        public bool GainedThisSession { get; protected set; }

        public Difficulty Difficulty { get; set; }

        protected Achievement(string guid, string description)
        {
            Guid = guid;
            Description = description;
        }

        public bool IsGainedThisSession(Difficulty difficulty)
        {
            //Only interested in achievents that haven't been completed or ones that have been completed on a lower difficulty
            if (Completed && (Completed == false || (int)Difficulty >= (int)difficulty))
                return false;

            return OnIsGainedThisSession();
        }

        protected abstract bool OnIsGainedThisSession();
    }
}

