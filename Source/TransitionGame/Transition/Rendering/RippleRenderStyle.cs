using Microsoft.Xna.Framework.Graphics;
using Transition.ResourceManagers;

namespace Transition.Rendering
{
    /// <summary>
    /// A style to render 'ripple' effects.
    /// </summary>
    public class RippleRenderStyle : RenderStyle
    {
        public static RippleRenderStyle Instance;

        public override RenderStyleType RenderStyleId => RenderStyleType.Ripple;

        protected override void OnRegister(EffectManager effectManager)
        {
            Instance = this;
            Effect = effectManager.GetEffect("RippleDistortion");
        }

        public override void Setup(GraphicsDevice graphicsDevice)
        {
            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
        }
    }
}
