//using System;
//using Microsoft.Xna.Framework;
//using Transition.MenuControls;

//namespace Transition.Menus
//{
//    /// <summary>
//    /// Represents a menu for entering names for highscores.
//    /// </summary>
//    public class ScoreEntryMenu : Menu
//    {
//        readonly TextLabelControl _rankValue;
//        readonly TextBoxControl _nameTextBox;

//        public ScoreTable ScoreTable;
//        public ScoreTableTimeAttack ScoreTableTimeAttack;
//        public ScoreItem ScoreItem;
//        public ScoreItemTimeAttack ScoreItemTimeAttack;

//        bool _doneKeyboard = false;

//        /// <summary>
//        /// Initialises a new instance of a StartMenu.
//        /// </summary>
//        /// <param name="game">A reference to the game.</param>
//        /// <param name="title">The title of the menu.</param>
//        public ScoreEntryMenu(TransitionGame game, string title) : base(game, title)
//        {
//            float yPosition = 3.5f;

//            var captionLabel1 = new TextLabelControl(game);            
//            captionLabel1.Position = new Vector2(-0.5f, yPosition);
//            captionLabel1.Text = "awesome!";
//            captionLabel1.Tint = Menu.LabelValueColour;
//            captionLabel1.Alignment = Alignment.Centre;
//            captionLabel1.FontSize = Theme.FontSize * 2;

//            yPosition -= 2.25f;

//            var captionLabel2 = new TextLabelControl(game);            
//            captionLabel2.Position = new Vector2(-0.5f, yPosition);
//            captionLabel2.Text = "new high score achieved";
//            captionLabel2.Tint = Menu.LabelCaptionColour;
//            captionLabel2.Alignment = Alignment.Centre;
//            captionLabel2.FontSize = Theme.FontSize;

//            yPosition -= 1.25f;

//            var captionLabel3 = new TextLabelControl(game);            
//            captionLabel3.Position = new Vector2(-0.5f, yPosition);
//            captionLabel3.Text = "you ranked";
//            captionLabel3.Tint = Menu.LabelCaptionColour;
//            captionLabel3.Alignment = Alignment.Centre;
//            captionLabel3.FontSize = Theme.FontSize;

//            yPosition -= 2.0f;

//            _rankValue = new TextLabelControl(game);            
//            _rankValue.Position = new Vector2(-0.5f, yPosition);
//            _rankValue.Tint = Menu.LabelValueColour;
//            _rankValue.Alignment = Alignment.Centre;
//            _rankValue.FontSize = Theme.FontSize * 2;

//            yPosition -= 2.25f;

//            var nameLabel = new TextLabelControl(game);            
//            nameLabel.Position = new Vector2(-0.5f, yPosition);
//            nameLabel.Text = "name";
//            nameLabel.Tint = Menu.LabelCaptionColour;
//            nameLabel.Alignment = Alignment.CentreRight;

//            _nameTextBox = new TextBoxControl(game);            
//            _nameTextBox.Position = new Vector2(0.5f, yPosition);
//            _nameTextBox.Font = Theme.Font;
//            _nameTextBox.FontSize = Theme.FontSize;
//            _nameTextBox.Text = "";
//            _nameTextBox.MaxLength = 15;
//            _nameTextBox.Tint = Menu.StandardColour;
//            _nameTextBox.Alignment = Alignment.CentreLeft;
//            _nameTextBox.InputMode = TextBoxControl.TextInputMode.Edit;
//            _nameTextBox.Caption = "enter your name"; 
//            _nameTextBox.StartToSelect = false;
//            _nameTextBox.AToSelect = true;
//            _nameTextBox.Clicked += NameTextBoxClicked;

//            AddControl(captionLabel1, -1);
//            AddControl(captionLabel2, -1);
//            AddControl(captionLabel3, -1);
//            AddControl(_rankValue, -1);
//            AddControl(nameLabel, -1);
//            AddControl(_nameTextBox, 0);
//        }

//        /// <summary>
//        /// Occurs when a menu is added to the MenuManager.
//        /// </summary>
//        protected override void OnAddedToManager()
//        {
//            var position = ScoreTable != null ? ScoreTable.CurrentScoreId + 1 : ScoreTableTimeAttack.CurrentScoreId + 1;
//            _rankValue.Text = Ordinal(position);

//            _nameTextBox.Text = string.Empty;

//            if (MenuManager.PrimaryNavigation == ControlType.MouseAndKeys)
//            {
//                _nameTextBox.InputMode = TextBoxControl.TextInputMode.Edit;
//                _doneKeyboard = true;
//            }
//            else
//            {
//                _nameTextBox.InputMode = TextBoxControl.TextInputMode.View;
//                _doneKeyboard = false;
//            }
//        }

//        private void NameTextBoxClicked(object sender, EventArgs e)
//        {
//            if (MenuManager.PrimaryNavigation == ControlType.GamePad && _nameTextBox.InputMode == TextBoxControl.TextInputMode.Edit)
//            {
//                ShowTextEntry();
//                _nameTextBox.InputMode = TextBoxControl.TextInputMode.View;
//            }
//        }

//        private void ShowTextEntry()
//        {
//            var textEntryMenu = (TextEntryMenu)Game.MenuManager.GetMenu(MenuType.TextEntryMenu);
//            textEntryMenu.Title = "Enter Your Name";
//            textEntryMenu.InitialText = _nameTextBox.Text;
//            textEntryMenu.MaxLength = _nameTextBox.MaxLength;
//            textEntryMenu.AfterClosed += (sender, e) => {
//                _nameTextBox.Text = ((TextEntryMenu)sender).Text; 
//                _doneKeyboard = true;
//            };
//            Game.MenuManager.AddMenu(textEntryMenu);
//        }

//        protected override void OnUpdateMustCallBase()
//        {
//            base.OnUpdateMustCallBase();

//            //if (_showKeyboardInput)
//            //    _ticks++;

//            //if (_showKeyboardInput && _ticks == 120)
//            //{
//            //    _ticks = 0;
//            //    _showKeyboardInput = false;
//            //    ShowTextEntry();
//            //    _nameTextBox.InputMode = TextBoxControl.TextInputMode.View;
//            //}

//            if (AcceptsInput)
//                _nameTextBox.AToSelect = MenuManager.PrimaryNavigation == ControlType.GamePad || (MenuManager.PrimaryNavigation == ControlType.MouseAndKeys && _nameTextBox.InputMode == TextBoxControl.TextInputMode.View);

//            if (_nameTextBox.InputMode == TextBoxControl.TextInputMode.View && this.AcceptsInput && _doneKeyboard)
//            {
//                Close();

//                var name = "player 1";
//                if (_nameTextBox.Text.Length > 0)
//                    name = _nameTextBox.Text;

//                if (ScoreItem != null)
//                {
//                    ScoreItem.Name = name;
//                    ScoreTable.Save(this.Game);
//                }
//                else
//                {
//                    ScoreItemTimeAttack.Name = name;
//                    ScoreTableTimeAttack.Save(this.Game);
//                }
//            }   
//        }

//        public static string Ordinal(int number)
//        {
//            string suffix = String.Empty;

//            int ones = number % 10;
//            int tens = (int)Math.Floor(number / 10.0) % 10;

//            if (tens == 1)
//            {
//                suffix = "th";
//            }
//            else
//            {
//                switch (ones)
//                {
//                    case 1:
//                        suffix = "st";
//                        break;

//                    case 2:
//                        suffix = "nd";
//                        break;

//                    case 3:
//                        suffix = "rd";
//                        break;

//                    default:
//                        suffix = "th";
//                        break;
//                }
//            }

//            return String.Format("{0}{1}", number, suffix);
//        }
//    }
//}
