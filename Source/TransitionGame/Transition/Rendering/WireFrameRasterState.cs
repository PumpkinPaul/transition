using Microsoft.Xna.Framework.Graphics;

namespace Transition.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public class WireFrameRasterState : RasterizerState
    {
        public static WireFrameRasterState Instance = new WireFrameRasterState();

        public WireFrameRasterState()
        {
            FillMode = FillMode.WireFrame;
        }
    }
}
