using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Transition.Extensions;

namespace Transition.Input
{
    /// <summary>
    /// Input helper class, captures all the mouse input and provides some nice helper methods and properties.
    /// Will also keep track of the last frame states for comparison if a button was just pressed this frame, but not already in the last frame.
    /// </summary>
    public static class MouseHelper
    {
        //Current and previous mouse states.
        private static MouseState _mouseState, _previousMouseState;

        private static int _mouseWheelValue;

        private static float _lastXMovement, _lastYMovement;

        //----------------------------------------------------------------------------------------------------
        //Mouse queries

        /// <summary>
        /// Was a mouse detected? Returns true if the user moves the mouse.  On the Xbox 360 there will be no mouse movement and therefore we
        /// know that we don't have to display the mouse.
        /// </summary>
        public static bool IsMouseDetected { get; private set; }

        public static bool IsMouseInWindow { get; private set; }

        //----------------------------------------------------------------------------------------------------
        //Mouse movement

        /// <summary>The position of the mouse in relation to the upper left corner of the game window.</summary>
        public static Point MousePos { get { return new Point(_mouseState.X, _mouseState.Y); } }

        /// <summary>The position of the mouse in relation to the upper left corner of the game window.</summary>
        public static Vector2 MouseVector { get { return new Vector2(_mouseState.X, _mouseState.Y); } }

        //----------------------------------------------------------------------------------------------------
        //Mouse movement

        /// <summary>Mouse x movement during current tick</summary>
        public static float XMovement { get; private set; }

        /// <summary>Mouse y movement during current tick</summary>
        public static float YMovement { get; private set; }

        /// <summary>Has the mouse moved in either the X or Y direction ths tick</summary>
        /// <returns>Boolean</returns>
        public static bool HasMouseMoved { get { return XMovement.FloatEquals(0) == false || YMovement.FloatEquals(0) == false; } }

        //----------------------------------------------------------------------------------------------------
        //Buttons down?

        /// <summary>Is the left mouse button currently in a down state.</summary>
        public static bool IsLeftButtonDown { get { return _mouseState.LeftButton == ButtonState.Pressed; } }

        /// <summary>Is the middle mouse button currently in a down state.</summary>
        public static bool IsMiddleButtonDown { get { return _mouseState.MiddleButton == ButtonState.Pressed; } }

        /// <summary>Is the right mouse button currently in a down state.</summary>
        public static bool IsRightButtonDown { get { return _mouseState.RightButton == ButtonState.Pressed; } }

        //----------------------------------------------------------------------------------------------------
        //Buttons down this frame?

        /// <summary>Is the left mouse button currently in a down state after being up last tick.</summary>
        public static bool IsLeftButtonJustDown { get { return _mouseState.LeftButton == ButtonState.Pressed && _previousMouseState.LeftButton == ButtonState.Released; } }

        /// <summary>Is the middle mouse button currently in a down state after being up last tick.</summary>
        public static bool IsMiddleButtonJustDown { get { return _mouseState.MiddleButton == ButtonState.Pressed && _previousMouseState.MiddleButton == ButtonState.Released; } }

        /// <summary>Is the right mouse button currently in a down state after being up last tick.</summary>
        public static bool IsRightButtonJustDown { get { return _mouseState.RightButton == ButtonState.Pressed && _previousMouseState.RightButton == ButtonState.Released; } }

        //----------------------------------------------------------------------------------------------------
        //Buttons up?

        /// <summary>Is the left mouse button currently in an up state.</summary>
        public static bool IsLeftButtonUp { get { return _mouseState.LeftButton == ButtonState.Released; } }

        /// <summary>Is the middle mouse button currently in an up state.</summary>
        public static bool IsMiddleButtonUp { get { return _mouseState.MiddleButton == ButtonState.Released; } }

        /// <summary>Is the right mouse button currently in an up state.</summary>
        public static bool IsRightButtonUp { get { return _mouseState.RightButton == ButtonState.Released; } }

        //----------------------------------------------------------------------------------------------------
        //Buttons up this frame?

        /// <summary>Is the left mouse button currently in an up state after being down last tick.</summary>
        public static bool IsLeftButtonJustUp { get { return _mouseState.LeftButton == ButtonState.Released && _previousMouseState.LeftButton == ButtonState.Pressed; } }

        /// <summary>Is the middle mouse button currently in an up state after being down last tick.</summary>
        public static bool IsMiddleButtonJustUp { get { return _mouseState.MiddleButton == ButtonState.Released && _previousMouseState.MiddleButton == ButtonState.Pressed; } }

        /// <summary>Is the right mouse button currently in an up state after being down last tick.</summary>
        public static bool IsRightButtonJustUp { get { return _mouseState.RightButton == ButtonState.Released && _previousMouseState.RightButton == ButtonState.Pressed; } }

        /// <summary>Mouse wheel delta</summary>
        public static int MouseWheelDelta { get; private set; }

        public static bool CaptureMouse { get; set; }

        /// <summary>
        /// Update, called from BaseGame.Update().
        /// Will catch all new states for keyboard, mouse and the gamepad.
        /// </summary>
        internal static void Update()
        {
            // Handle mouse input variables
            _previousMouseState = _mouseState;
            _mouseState = Mouse.GetState();

            // Update mouseXMovement and mouseYMovement
            _lastXMovement += _mouseState.X - _previousMouseState.X;
            _lastYMovement += _mouseState.Y - _previousMouseState.Y;
            XMovement = _lastXMovement / 2.0f;
            YMovement = _lastYMovement / 2.0f;
            _lastXMovement -= _lastXMovement / 2.0f;
            _lastYMovement -= _lastYMovement / 2.0f;

            MouseWheelDelta = _mouseState.ScrollWheelValue - _mouseWheelValue;
            _mouseWheelValue = _mouseState.ScrollWheelValue;

            if (CaptureMouse)
            {
 
                if (MousePos.X < 0 || MousePos.X > TransitionGame.Instance.Window.ClientBounds.Width || MousePos.Y < 0 || MousePos.Y > TransitionGame.Instance.Window.ClientBounds.Height)
                {
                    Mouse.SetPosition(Math.Min(Math.Max(0, MousePos.X), TransitionGame.Instance.Window.ClientBounds.Width), Math.Min(Math.Max(0, MousePos.Y), TransitionGame.Instance.Window.ClientBounds.Height));
                    _mouseState = Mouse.GetState();
                }
            }

            // Check if mouse was moved this frame if it is not detected yet. This allows us to ignore the mouse even when it is captured
            // on a windows machine if just the gamepad or keyboard is used.
            if (IsMouseDetected == false)
                IsMouseDetected = _mouseState.X != _previousMouseState.X || _mouseState.Y != _previousMouseState.Y || _mouseState.LeftButton != _previousMouseState.LeftButton;

            var windowBounds = TransitionGame.Instance.Window.ClientBounds;
            IsMouseInWindow = MousePos.X >= 0 && MousePos.X <= windowBounds.Width && MousePos.Y >= 0 && MousePos.Y <= windowBounds.Height;
        }
    }
}
