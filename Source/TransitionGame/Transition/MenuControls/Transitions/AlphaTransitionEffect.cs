namespace Transition.MenuControls.Transitions
{
    /// <summary>
    /// Represents an damage limitation device.
    /// </summary>
    public class AlphaTransitionEffect : MenuTransitionEffect
    {
        readonly float _alpha;

        public AlphaTransitionEffect(MenuControl menuControl, string name, int delay, int ticks, InterpolationMethod interpolationMethod, TransitionType type, float alpha) : base(menuControl, name, delay, ticks, interpolationMethod, type)
        {
            _alpha = alpha;
        }

        protected override void OnStart()
        {
            MenuControl.AnchorAlpha = MenuControl.Alpha;
        }

        protected override void OnUpdate(float delta)
        {
            var endAlpha = Type == TransitionType.Absolute ? _alpha : MenuControl.AnchorAlpha + _alpha;
 
            MenuControl.Alpha = InterpolationMethod.Invoke(MenuControl.AnchorAlpha, endAlpha, delta); 
        }
    }
}

