using System;
using Microsoft.Xna.Framework;
using Transition.ActorDatas;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Weapon : Actor
    {
        static private readonly IWeaponState ReadyState = new WeaponReadyState();
        static private readonly IWeaponState LoadingBulletState = new WeaponLoadingBulletState();

        /// <summary>A reference to the current state machine state.</summary>
        private IWeaponState _state;

        public WeaponData WeaponData;
		
        /// <summary>Indicates whether the weapon is firing.</summary>
        protected bool Firing;

        /// <summary>Indicates whether the weapon was firing last update.</summary>
        protected bool PreviouslyFiring;

        /// <summary>The number of game ticks it takes to reload the weapon.</summary>
		private int _loadBulletTicks;

        /// <summary>The number of game ticks it takes to fire the weapon.</summary>
		private int _firingTicks;

        public int FireRateId { get; private set; }

        public int MaxFireRateId { get; private set; }

        public Type BulletClass { get; private set; }
        public string BulletName { get; private set; }

        public int BulletDamageId { get; private set; }

        public int MaxBulletDamageId { get; private set; }

        /// <summary>
        /// Creates a new instance of a Weapon
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        protected Weapon(TransitionGame game) : base(game) { }

        /// <summary>
        /// Gets the class of actor
        /// </summary>
        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.Weapon; }
        }

        protected override void OnInitialise(string name)
        {
            base.OnInitialise(name);

            WeaponData = _game.DataManager.WeaponData(name);

            _state = Weapon.ReadyState;
            Firing = false;
            PreviouslyFiring = false;
		    _loadBulletTicks = 0;
		    _firingTicks = 0;
            FireRateId = 0;
            BulletDamageId = 0;
            MaxFireRateId = WeaponData.FireRates.Count - 1;

            var bulletData = _game.DataManager.BulletData(WeaponData.BulletName);
            BulletName = bulletData.Name;
            BulletClass = bulletData.ActorClass;
            MaxBulletDamageId = bulletData.Damage.Count - 1;
        }

        public bool UpgradeFireRate()
        {
            if (FireRateId >= MaxFireRateId)
                return false;

            FireRateId++;

            return true;
        }

        public bool UpgradeBulletDamage()
        {
            if (BulletDamageId >= MaxBulletDamageId)
                return false;

            BulletDamageId++;

            return true;
        }

        /// <summary>
        /// Allows an object to update itself.
        /// </summary>
        protected override void OnUpdate(GameTime gameTime)
        {
            _direction = _owner.Direction;
            _rotation = _owner.Rotation;
    		
		    _state.OnUpdate(this);

		    if(Firing) {
			    ++_firingTicks;
		    }
		    else if (!Firing && PreviouslyFiring) {
			    OnStoppedFiring();
			    _firingTicks = 0;
		    }
    		
		    PreviouslyFiring = Firing;
		    Firing = false;
        }

        /// <summary>
        /// Occurs when an actor has expired.
        /// </summary>
        /// <remarks>Actors can be set to have a limited lifespan.</remarks>
        protected override void OnExpire()
        {
		    _owner.ExpireWeapon(this);
			Deactivate();
        }

        /// <summary>
        ///  request to fire the weapon
        /// </summary>
        /// <remarks>May not fire if the weapon isn't ready.</remarks>
        public void Fire()
        {
            Firing = true;
            _state.OnFire(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected abstract Bullet CreateBullet();

        /// <summary>
        /// Called after the weapon has created its bullet.
        /// </summary>
        /// <remarks>Standard behaviour for a wepaon - subclasses may override </remarks>
        protected virtual void OnCreateBullet()
        {
            var bullet = CreateBullet();
            if (bullet == null) 
                return;

            bullet.Tint = Tint;
            bullet.PlayerId = _playerId;
            bullet.Direction = _direction;
            bullet.Position = Position + VectorHelper.Polar(bullet.ActorData.BaseSize.X / 4.0f, _direction);
            bullet.Rotation = _rotation;
            bullet.DamageId = BulletDamageId;
            bullet.InitialVelocity = new Vector2(Owner.Velocity.X, 0.0f);

            _scene.AddActor(bullet);
        }

        /// <summary>
        /// Called after the weapon has stopped firing.
        /// </summary>
        protected virtual void OnStoppedFiring() { }

        /// <summary>
        /// Base class for the Weapon's state machine.
        /// </summary>
        public interface IWeaponState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="weapon">The weapon to update.</param>
            void OnUpdate(Weapon weapon);

            /// <summary>
            /// State specific weapon fire
            /// </summary>
            /// <param name="weapon">The weapon to fire.</param>
            void OnFire(Weapon weapon);
        }

        /// <summary>
        /// Ready behaviour for a weapon.
        /// </summary>
        public struct WeaponReadyState : IWeaponState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="weapon">The weapon to update.</param>
            public void OnUpdate(Weapon weapon) { }

            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="weapon">The weapon to fire.</param>
            public void OnFire(Weapon weapon)
            {
                //Create the Bullet for this weapon.
                weapon.OnCreateBullet();
                weapon._state = Weapon.LoadingBulletState;
            }
        }

        /// <summary>
        /// Ready behaviour for a weapon.
        /// </summary>
        public struct WeaponLoadingBulletState : IWeaponState
        {
            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="weapon">The weapon to update.</param>
            public void OnUpdate(Weapon weapon)
            {
                ++weapon._loadBulletTicks;

		        var fireRate = weapon.WeaponData.FireRates[weapon.FireRateId].FireRate;
		        if (weapon._loadBulletTicks > fireRate) 
                {
			        //Bullet loaded so ready the weapon
			        weapon._loadBulletTicks -= fireRate;
			        weapon._state = Weapon.ReadyState;
		        }	
            }

            /// <summary>
            /// State specific update
            /// </summary>
            /// <param name="weapon">The weapon to fire.</param>
            public void OnFire(Weapon weapon) { }
        }
    }
}
