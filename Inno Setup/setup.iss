#define use_msi20
#define use_msi31
#define use_msi45
#define use_dotnetfx40

#define MyConfiguration "Release"

#define MyAppSetupName "TransitionSetup"
#define MyAppVersion '0.0.3'

#define MyAppName "Transition"
#define MyAppVerName "Transition 0.0.1"
#define MyAppPublisher "Pumpkin Games"
#define MyAppURL "http://www.pumpkin-games.net"
#define MyAppExeName "Transition.exe"

[Setup]
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppCopyright=Copyright � {#MyAppPublisher} 2007-2015
VersionInfoVersion={#MyAppVersion}
VersionInfoCompany={#MyAppPublisher}
AppPublisher={#MyAppPublisher}
;AppPublisherURL=http://...
;AppSupportURL=http://...
;AppUpdatesURL=http://...
OutputBaseFilename={#MyAppSetupName}-{#MyAppVersion}
DefaultGroupName={#MyAppName}
DefaultDirName={pf}\{#MyAppName}
UninstallDisplayIcon={app}\{#MyAppName}
OutputDir=bin
SourceDir=.
AllowNoIcons=yes
SetupIconFile= ..\Source\TransitionGame\Transition\TransitionIcon.ico
SolidCompression=yes
WizardSmallImageFile=Images\WizardSmallImageFile.bmp
WizardImageFile=Images\WizardImageFile.bmp

;MinVersion default value: "0,5.0 (Windows 2000+) if Unicode Inno Setup, else 4.0,4.0 (Windows 95+)"
;MinVersion=0,5.0
PrivilegesRequired=admin
ArchitecturesAllowed=x86 x64 ia64
ArchitecturesInstallIn64BitMode=x64 ia64

[Languages]
Name: "en"; MessagesFile: "compiler:Default.isl"
Name: "de"; MessagesFile: "compiler:Languages\German.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: C:\Program Files (x86)\Microsoft XNA\XNA Game Studio\v4.0\Redist\DX Redist\*; DestDir: {tmp}
Source: C:\Program Files (x86)\Microsoft XNA\XNA Game Studio\v4.0\Redist\XNA FX Redist\xnafx40_redist.msi; DestDir: {tmp}
Source: ..\Source\TransitionGame\Transition\bin\x86\{#MyConfiguration}\Transition.exe; DestDir: {app}; Flags: ignoreversion
Source: ..\Source\TransitionGame\Transition\bin\x86\{#MyConfiguration}\AutoUpdater.config; DestDir: {app}; Flags: ignoreversion
Source: ..\Source\TransitionGame\Transition\bin\x86\{#MyConfiguration}\Content\*; DestDir: {app}\Content; Flags: ignoreversion recursesubdirs createallsubdirs
Source: .\Input\*; DestDir: {app}; Flags: ignoreversion
Source: ..\..\AutoUpdater\Compiled\*; DestDir: {app}; Flags: ignoreversion

[Icons]
Name: {group}\{#MyAppName}; Filename: {app}\{#MyAppExeName}; WorkingDir: {app}
Name: {group}\{#MyAppName} (safe mode); Filename: {app}\{#MyAppName} (safe mode).bat; WorkingDir: {app}
Name: {group}\readme; Filename: {app}\readme.txt
Name: {group}\error log; Filename: {app}\{#MyAppName}.log
Name: {group}\{cm:UninstallProgram,{#MyAppName}}; Filename: {uninstallexe}
Name: {commondesktop}\{#MyAppName}; Filename: {app}\{#MyAppExeName}; WorkingDir: {app}; Tasks: desktopicon

[Run]
Filename: {tmp}\dxsetup.exe; Parameters: /silent
Filename: msiexec.exe; Parameters: "/quiet /i ""{tmp}\xnafx40_redist.msi"""
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#MyAppName}}"; Flags: nowait postinstall skipifsilent
;Filename: {app}\readme.txt; Description: View the README file; Flags: postinstall shellexec skipifsilent

#include "scripts\products.iss"

#include "scripts\products\stringversion.iss"
#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
#include "scripts\products\dotnetfxversion.iss"

#ifdef use_msi20
#include "scripts\products\msi20.iss"
#endif
#ifdef use_msi31
#include "scripts\products\msi31.iss"
#endif
#ifdef use_msi45
#include "scripts\products\msi45.iss"
#endif

#ifdef use_dotnetfx40
#include "scripts\products\dotnetfx40client.iss"
#include "scripts\products\dotnetfx40full.iss"
#endif

[CustomMessages]
win_sp_title=Windows %1 Service Pack %2


[Code]
function InitializeSetup(): boolean;
begin
	//init windows version
	initwinversion();

#ifdef use_msi20
	msi20('2.0');
#endif
#ifdef use_msi31
	msi31('3.1');
#endif
#ifdef use_msi45
	msi45('4.5');
#endif

#ifdef use_dotnetfx11
	dotnetfx11();
#ifdef use_dotnetfx11lp
	dotnetfx11lp();
#endif
	dotnetfx11sp1();
#endif

	//install .netfx 2.0 sp2 if possible; if not sp1 if possible; if not .netfx 2.0
#ifdef use_dotnetfx20
	//check if .netfx 2.0 can be installed on this OS
	if not minwinspversion(5, 0, 3) then begin
		msgbox(fmtmessage(custommessage('depinstall_missing'), [fmtmessage(custommessage('win_sp_title'), ['2000', '3'])]), mberror, mb_ok);
		exit;
	end;
	if not minwinspversion(5, 1, 2) then begin
		msgbox(fmtmessage(custommessage('depinstall_missing'), [fmtmessage(custommessage('win_sp_title'), ['XP', '2'])]), mberror, mb_ok);
		exit;
	end;

	if minwinversion(5, 1) then begin
		dotnetfx20sp2();
#ifdef use_dotnetfx20lp
		dotnetfx20sp2lp();
#endif
	end else begin
		if minwinversion(5, 0) and minwinspversion(5, 0, 4) then begin
#ifdef use_kb835732
			kb835732();
#endif
			dotnetfx20sp1();
#ifdef use_dotnetfx20lp
			dotnetfx20sp1lp();
#endif
		end else begin
			dotnetfx20();
#ifdef use_dotnetfx20lp
			dotnetfx20lp();
#endif
		end;
	end;
#endif

#ifdef use_dotnetfx35
	//dotnetfx35();
	dotnetfx35sp1();
#ifdef use_dotnetfx35lp
	//dotnetfx35lp();
	dotnetfx35sp1lp();
#endif
#endif

	// if no .netfx 4.0 is found, install the client (smallest)
#ifdef use_dotnetfx40
	if (not netfxinstalled(NetFx40Client, '') and not netfxinstalled(NetFx40Full, '')) then
		dotnetfx40client();
#endif

	Result := true;
end;
