using ProtoBuf;

namespace Transition.ActorDatas
{
	/// <summary>
	/// Configuration data surrogate for serialization.
	/// </summary>
    [ProtoContract]
    public class ShipDataSurrogate
    {
        [ProtoMember(1)]
        public string Name;

        static readonly ShipDataSurrogate Instance = new ShipDataSurrogate();

        public static implicit operator ShipDataSurrogate(ShipData value)
        {
            Instance.Name = value.Name;
            return Instance;
        }

        public static implicit operator ShipData(ShipDataSurrogate value)
        {
            return TransitionGame.Instance.DataManager.ShipData(value.Name);
        }
    }
}
