using ProtoBuf;

namespace Transition._____Temp.StarGuard.EntityBlueprints
{
	/// <summary>
	/// Configuration data surrogate for serialization.
	/// </summary>
    [ProtoContract]
    public class EntityBlueprintSurrogate
    {
        [ProtoMember(1)]
        string _name;

        static readonly EntityBlueprintSurrogate Instance = new EntityBlueprintSurrogate();

        public static implicit operator EntityBlueprintSurrogate(EntityBlueprint value)
        {
            if (value == null)
                return null;

            Instance._name = value.Name;
            return Instance;
        }

        public static implicit operator EntityBlueprint(EntityBlueprintSurrogate value)
        {
            return value == null ? null : TransitionGame.Instance.EntityBlueprints.EntityBlueprint(value._name);
        }
    }
}
