using System;

namespace Transition._____Temp.StarGuard
{
    [Flags]
    public enum CollisionEdge
    {
        None = 0,
        Top = 1,
        Left = 2,
        Bottom = 4, 
        Right = 8,
    }
}
