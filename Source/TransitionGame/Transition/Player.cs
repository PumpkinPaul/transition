using Nuclex.Input;
using Transition.Actors;
using Transition.Input;

namespace Transition
{
    /// <summary>
    /// Represents the player
    /// </summary>
    public class Player
    {
        public const byte MaxNukes = 3;
        public const byte MaxRagePower = 5;
        public const byte MaxHealth = 10;
        public const byte MaxCityDefence = 3;
        public const byte MaxCityAttack = 3;
        private const byte InitialLives = 3;

        /// <summary>The player's lives.</summary>
        private byte _lives = Player.InitialLives;

        /// <summary>The player's time (in game ticks) for completing levels in the top world.</summary>
        private int _topTicks;

        /// <summary>The player's time (in game ticks) for completing levels in the bottom world.</summary>
        private int _bottomTicks;

        public ExtendedPlayerIndex PlayerIndex;

        /// <summary>
        /// Gets or sets a reference to the player's profile.
        /// </summary>
        public PlayerProfile Profile { get; set; }

        /// <summary>
        /// Gets or set the player's ship.
        /// </summary>
        public Difficulty Difficulty { get; set; }

        /// <summary>
        /// All input from the player this update.
        /// </summary>
        public GameInput GameInput;

        public Player()
        {
            Difficulty = Difficulty.Normal;
        }

        public int Score { get; private set; }
        
        public int Money { get; private set; }

        /// <summary>
        /// Gets or set the player's ticks for the top world.
        /// </summary>
        public int TopTicks
        {
            get { return _topTicks; }
            set 
            { 
                _topTicks = value;
                TopTime = ConversionHelper.TicksToDisplayTime(value);
            }
        }

        /// <summary>
        /// Gets or set the player's ticks for the bottom world.
        /// </summary>
        public int BottomTicks
        {
            get { return _bottomTicks; }
            set 
            { 
                _bottomTicks = value;
                BottomTime = ConversionHelper.TicksToDisplayTime(value);
            }
        }

        /// <summary>
        /// Gets the player's time for the top world.
        /// </summary>
        public string TopTime { get; private set; }

        /// <summary>
        /// Gets the player's time for the bottom world.
        /// </summary>
        public string BottomTime { get; private set; }

        /// <summary>
        /// Gets the player's lives.
        /// </summary>
        public byte Lives
        {
            get { return _lives; }
        }

        public int Nukes { get; private set; }
        public int CityDefence { get; private set; }
        public int CityAttack { get; private set; }

        /// <summary>
        /// Gets or set the player's ship.
        /// </summary>
        public Ship Ship { get; set; }
		
        /// <summary>
        /// Sets the Player to its initial state
        /// </summary>
        public void Initialise()
        {
            Score = 0;
            Money = 0;
            _topTicks = 0;
            _bottomTicks = 0;
            TopTime = "00:00";
            BottomTime = "00:00";
            _lives = Player.InitialLives;
            Nukes = 0;
            CityDefence = 0;
            CityAttack = 0;
        }

        public void AttemptNuke()
        {
            if (Nukes <= 0)
                return;

            Nukes--;
            Ship.FireNuke();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scoreToAdd"></param>
        public void AddScore(int scoreToAdd)
        {
            Score += scoreToAdd;
        }

		/// <summary>
		/// Decreases the number of lives the player has
		/// </summary>
		public void LoseLife()
		{
            if (_lives > 0)
                --_lives;
		}

        public bool BuyHealth(int cost)
        {
            if (Money < cost || Ship.Health == MaxHealth)
                return false;

            Money -= cost;
            Ship.AddHealth();

            return true;
        }

        public bool BuyBulletDamage(int cost)
        {
            if (Money < cost || Ship.Weapon.BulletDamageId == Ship.Weapon.MaxBulletDamageId)
                return false;

            Money -= cost;
            Ship.Weapon.UpgradeBulletDamage();

            return true;
        }

        public bool BuyFireRate(int cost)
        {
            if (Money < cost || Ship.Weapon.FireRateId == Ship.Weapon.MaxFireRateId)
                return false;

            Money -= cost;
            Ship.Weapon.UpgradeFireRate();

            return true;
        }

        public bool BuyNuke(int cost)
        {
            if (Money < cost || Nukes == MaxNukes)
                return false;

            Money -= cost;
            Nukes++;

            return true;
        }

        public bool BuyCityDefence(int cost)
        {
            if (Money < cost || CityDefence == MaxCityDefence)
                return false;

            Money -= cost;
            CityDefence++;

            return true;
        }

        public bool BuyCityAttack(int cost)
        {
            if (Money < cost || CityAttack == MaxCityAttack)
                return false;

            Money -= cost;
            CityAttack++;

            return true;
        }
    }
}
