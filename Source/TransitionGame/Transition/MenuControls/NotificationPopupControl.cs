using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control for displaying generic notifications in a pop up style
    /// </summary>
    public class NotificationPopupControl : MenuControl
    {
        public const int StayVisible = -1;

        InterpolationMethod _lerpInXMethod;
        InterpolationMethod _lerpInYMethod;
        InterpolationMethod _lerpOutXMethod;
        InterpolationMethod _lerpOutYMethod;

        InterpolationMode _lerpInX;
        InterpolationMode _lerpInY;
        InterpolationMode _lerpOutX;
        InterpolationMode _lerpOutY;

        public InterpolationMode LerpInX 
        {
            get { return _lerpInX; }
            set
            {
                _lerpInX = value;
                _lerpInXMethod = Interpolate.GetMethod(value);
            }
        }

        public InterpolationMode LerpInY
        {
            get { return _lerpInY; }
            set
            {
                _lerpInY = value;
                _lerpInYMethod = Interpolate.GetMethod(value);
            }
        }

        public InterpolationMode LerpOutX
        {
            get { return _lerpOutX; }
            set
            {
                _lerpOutX = value;
                _lerpOutXMethod = Interpolate.GetMethod(value);
            }
        }

        public InterpolationMode LerpOutY
        {
            get { return _lerpOutY; }
            set
            {
                _lerpOutY = value;
                _lerpOutYMethod = Interpolate.GetMethod(value);
            }
        }

        private enum Phase
        {
            Hidden,
            FadeIn,
            Normal,
            FadeOut,
        }

        const float MaxMasterAlpha = 1.0f;
        const float MinMasterAlpha = 0.0f;

        public int MaxVisibleTicks = 180;
        int _ticks;
        public int MaxFadeInTicks = 30;
        public int MaxFadeOutTicks = 30;
        
        float _masterAlpha;
        Vector2 _position;
        Vector2 _imageScale;

        readonly Queue<string> _notifications = new Queue<string>();

        Phase _phase;

        public Vector2 TargetPosition;
        public Vector2 EndPosition;

        /// <summary>Font used to render the control's text.</summary>
        public BitmapFont Font;
		
		/// <summary>The size of the font.</summary>
        public Vector2 FontSize;

        /// <summary>The id of the texture to use for drawing.</summary>
        public TextureInfo TextureInfo;

        public Vector2 ImageScale;

        public Alignment Alignment;

        public Color BackgroundTint;

        public bool DrawBorder;

        public bool Pulse;

        public float Padding = 0.2f;
        public float ImageSpacer = 0.3f;

        Rect _bounds;

        /// <summary>
        /// Initialises a new instance of an ImageControl.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        public NotificationPopupControl(BaseGame game) : base(game) 
        {
            Font = Theme.Font;
            FontSize = Theme.FontSize;
            Visible = false;
            ImageScale = Vector2.One;
            _masterAlpha = MinMasterAlpha;
            _phase = Phase.Hidden;

            LerpInX = InterpolationMode.Bounce;
            LerpInY = InterpolationMode.Bounce;

            LerpOutX = InterpolationMode.Linear;
            LerpOutY = InterpolationMode.Linear;
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            LerpInX = element.GetAttributeEnum("lerpInX", LerpInX);
            LerpInY = element.GetAttributeEnum("lerpInY", LerpInY);
            LerpOutX = element.GetAttributeEnum("lerpOutX", LerpOutX);
            LerpOutY = element.GetAttributeEnum("lerpOutY", LerpOutY);
            MaxVisibleTicks = element.GetAttributeInt32("maxVisibleTicks", MaxVisibleTicks);
            MaxFadeInTicks = element.GetAttributeInt32("maxFadeInTicks", MaxFadeInTicks);
            MaxFadeOutTicks = element.GetAttributeInt32("maxFadeOutTicks", MaxFadeOutTicks);
            TargetPosition = element.GetAttributeVector2("targetPosition", TargetPosition);
            EndPosition = element.GetAttributeVector2("endPosition", EndPosition);
            ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            TextureInfo = ParseTexture(element, "textureInfo", TextureInfo);
            ImageScale = element.GetAttributeVector2("imageScale", ImageScale);
            Alignment = element.GetAttributeEnum("alignment", Alignment);
            BackgroundTint = element.GetAttributeColor("backgroundTint", BackgroundTint);
            DrawBorder = element.GetAttributeBool("drawBorder", DrawBorder);
            Pulse = element.GetAttributeBool("pulse", Pulse);
            Padding = element.GetAttributeSingle("padding", Padding);
            ImageSpacer = element.GetAttributeSingle("imageSpacer", ImageSpacer);
        }

        /// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
		public override Rect Bounds
		{
            get { return _bounds;  }
		}

        public float MasterAlpha { get { return _masterAlpha; } }
        public float EffectiveAlpha { get { return _phase == Phase.Normal ? 1.0f : _masterAlpha; } }

        public void AddMessage(string text, bool exclusive = false)
        {
            if (exclusive && _notifications.Contains(text))
                return;

            _notifications.Enqueue(text);

            if (_phase != Phase.Hidden)
                return; //Already visible

            _phase = Phase.FadeIn;
            Visible = true;
        }

        public void FadeOut()
        {
            if (_phase == Phase.FadeOut || _phase == Phase.Hidden)
                return; //Already out or fading out

            _phase = Phase.FadeOut;
        }

        public void Reset()
        {
            _phase = Phase.Hidden;
            _notifications.Clear();
        }

        protected override void OnUpdate() 
        {           
            _imageScale = ImageScale;

            switch (_phase)
            {
                case Phase.Hidden:
                    break;

                case Phase.FadeIn:
                    _ticks++;

                    if (_ticks > MaxFadeInTicks)
                    {
                        _phase = Phase.Normal;
                        _masterAlpha = MaxMasterAlpha;
                        _ticks = 0;
                    }
                    else
                    {
                        var ratio = (float)_ticks / MaxFadeInTicks;
                        _masterAlpha = MathHelper.Lerp(MinMasterAlpha, MaxMasterAlpha, ratio);
                        _position.X = _lerpInXMethod(Position.X, TargetPosition.X, ratio);
                        _position.Y = _lerpInYMethod(Position.Y, TargetPosition.Y, ratio);
                    }
                    break;

                case Phase.Normal:
                    if (MaxVisibleTicks > StayVisible)
                    {
                        _ticks++;

                        if (_ticks > MaxVisibleTicks)
                        {
                            _ticks = 0;
                            FadeOut();
                        }

                        if (Pulse)
                        {
                            var ratio = (float)_ticks / MaxVisibleTicks;
                            var alphaZeroToOne = ((float)Math.Cos(ratio * MathHelper.TwoPi * 12) + 1.0f) / 2.0f;
                            _masterAlpha = (alphaZeroToOne * 0.85f) + 0.15f;

                        }
                    }
                    else
                    {
                        if (Pulse)
                        {
                            const int maxTicks = 360;
                            _ticks++;
                            if (_ticks > maxTicks)
                                _ticks = 0;

                            var ratio = (float)_ticks / maxTicks;
                            var alphaZeroToOne = ((float)Math.Cos(ratio * MathHelper.TwoPi * 5) + 1.0f) / 2.0f;
                            _masterAlpha = (alphaZeroToOne * 0.85f) + 0.15f;

                        }
                    }
                    
                    break;

                case Phase.FadeOut:
                    _ticks++;

                    if (_ticks > MaxFadeOutTicks)
                    {
                        _ticks = 0;
                        _notifications.Dequeue();
                        _masterAlpha = MinMasterAlpha;

                        if (_notifications.Count == 0)
                        {
                            _phase = Phase.Hidden;
                            Visible = false;
                        }
                        else 
                        {
                            _phase = Phase.FadeIn;
                        }
                    }
                    else
                    {
                        var ratio = (float)_ticks / MaxFadeOutTicks;
                        _masterAlpha = MathHelper.Lerp(MaxMasterAlpha, MinMasterAlpha, ratio);
                        _position.X = _lerpOutXMethod(TargetPosition.X, EndPosition.X, ratio);
                        _position.Y = _lerpOutYMethod(TargetPosition.Y, EndPosition.Y, ratio);
                    }
                    break;
            }
        }

        /// <summary>
        /// Allows a derived control to draw itself.
        /// </summary>
        protected override void OnDraw()
		{
            if (_notifications.Count == 0)
                return;

            var textPosition = _position;
            textPosition.Y -= Padding;

            var text = _notifications.Peek();
            var textBounds = Font.GetBounds(textPosition, FontSize, text, Alignment);

            //The height / width of the combined icon text.
            var innerHeight = MathHelper.Max(textBounds.Height, ImageScale.Y);
            var innerWidth = ImageScale.X + ImageSpacer + textBounds.Width;

            float left = 0;
            float right = 0;
            switch(Alignment)
            {
                case Alignment.TopCentre:
                case Alignment.BottomCentre:
                case Alignment.Centre:
                    textPosition.X += ((ImageScale.X + ImageSpacer) / 2.0f);// + padding;
                    left = _position.X - (innerWidth / 2) - Padding;
                    right = left + innerWidth + Padding + Padding;
                    break;

                case Alignment.TopLeft:
                case Alignment.CentreLeft:
                case Alignment.BottomLeft:
                    textPosition.X += (ImageScale.X + ImageSpacer) + Padding + Padding;
                    left = _position.X;
                    right = left + innerWidth + Padding;
                    break;

                case Alignment.TopRight:
                case Alignment.CentreRight:
                case Alignment.BottomRight:
                    textPosition.X -= (ImageScale.X + ImageSpacer);
                    right = _position.X - (ImageScale.X + ImageSpacer) + Padding + Padding;
                    left = right - innerWidth - Padding;
                    break;
            }

            //Draw background
            var pixelTextureInfo = Game.TextureManager.GetTextureInfo("Pixel");
            var bgTint = BackgroundTint;       
            bgTint *= Overlay.Opacity * _masterAlpha;  
            var q = new Quad(_position.Y, left, _position.Y - innerHeight - (Padding * 2), right);
            _bounds = q.Rect;
            Game.Renderer.DrawSprite(ref q, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, bgTint, RenderStyle, Layer);

            //Draw an icon
            var iconTint = EffectiveTint;       
            iconTint *= Overlay.Opacity * _masterAlpha;  
            var iconQuad = new Quad(new Vector2(Padding + q.Rect.Left + (ImageScale.X / 2.0f), (q.Rect.Top - ((q.Rect.Top - q.Rect.Bottom) / 2.0f))), _imageScale);
            Game.Renderer.DrawSprite(ref iconQuad, TextureInfo.TextureId, TextureInfo.SourceRect, iconTint, RenderStyle, Layer);

            //Draw  text      
            var tintColor = EffectiveTint;
            tintColor *= Overlay.Opacity * _masterAlpha;
            Game.Renderer.DrawString(Font, textPosition, FontSize, text, Alignment, tintColor, RenderStyle, Layer);

            //Draw a border
            if (DrawBorder)
            {
                var bounds = q.Rect;
                const float lineWidth = 0.04f;

                var tint = EffectiveTint;
                tint *= Overlay.Opacity * _masterAlpha * 0.5f;

                var corner = new Quad();
                corner.Rect = new Rect(bounds.Top, bounds.Left, bounds.Top - lineWidth, bounds.Right);
                Game.Renderer.DrawSprite(ref corner, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, tint, RenderStyle, Layer);

                corner.Rect = new Rect(bounds.Bottom + lineWidth, bounds.Left, bounds.Bottom, bounds.Right);
                Game.Renderer.DrawSprite(ref corner, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, tint, RenderStyle, Layer);

                corner.Rect = new Rect(bounds.Top, bounds.Left, bounds.Bottom, bounds.Left + lineWidth);
                Game.Renderer.DrawSprite(ref corner, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, tint, RenderStyle, Layer);

                corner.Rect = new Rect(bounds.Top, bounds.Right - lineWidth, bounds.Bottom, bounds.Right);
                Game.Renderer.DrawSprite(ref corner, pixelTextureInfo.TextureId, pixelTextureInfo.SourceRect, tint, RenderStyle, Layer);
            }
		}
    }
}
