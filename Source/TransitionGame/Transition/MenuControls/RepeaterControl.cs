using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Transition.MenuControls
{
    /// <summary>
    /// Represents a control for drawing repeating items.
    /// </summary>
    public class RepeaterControl : MenuControl
    {		
        public struct ItemControlDrawEventArgs 
        {
            public int Id;
            public int Rank;
        }

        public event Action<MenuControl, ItemControlDrawEventArgs> ItemControlDraw;

        RepeatTemplate _repeatTemplate;

        /// <summary>The number of items per page.</summary>
        public int PageSize = 10;

        /// <summary>Index of the page in the data to draw.</summary>
        public int PageIndex;

        public int Skip;

        /// <summary>A reference to the underlying datasource.</summary>
        public IEnumerable<object> DataSource;
		
		/// <summary>
		/// .
		/// </summary>
        public RepeaterControl(BaseGame game) : base(game) { }

        public override void Load(XElement element)
        {
            base.Load(element);

            //Might be itemTemplate (or goblins? but definitely, possibly itemTemplate, maybe)
            ParseRepeaterTemplate(element, "itemTemplate");
        }
			
        public void ParseRepeaterTemplate(XElement element, string templateName)
        {
            var templateElement = element.Element(templateName);
            if (templateElement == null)
                return;

            //Ok, we have an item template so might have some controls that need loading here.
            //Kinda like an asp:Repeater thing going on here.
            _repeatTemplate = new RepeatTemplate(templateElement, null, Game);
        }

        /// <summary>
		/// .
		/// </summary>
		protected override void OnDraw()
		{
            if (_repeatTemplate == null)
                return;

            if (DataSource == null)
                return;

            var skip = Skip + (PageIndex * PageSize);
            var dataPage = DataSource.Skip(skip).Take(PageSize);

            //Start of the repeating template
            var id = 0;
            var position = Position + _repeatTemplate.Position;
            foreach(var item in dataPage)
            {
                foreach (var control in _repeatTemplate.Controls)
                {
                    //Need to set the values of the control here...
                    _repeatTemplate.DataBind(control, item);
                    
                    //Temporarily diddle the position of this control in the repeater template - we'll reset it when it's been drawn in the corrct position
                    var tempPosition = control.Position;
                    control.Position += position;
                    
                    control.Overlay = Overlay;

                    OnItemControlDraw(control, item, id, skip + id);
                    control.Draw();

                    control.Position = tempPosition;
                }

                id++;
                position += _repeatTemplate.Offset;
            }
		}

        protected void OnItemControlDraw(MenuControl menuControl, object data, int id, int rank)
        {
            var handler = ItemControlDraw;
            handler?.Invoke(menuControl, new ItemControlDrawEventArgs { Id = id, Rank = rank });
        }
    }
}
