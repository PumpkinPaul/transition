#include "CommonVertex.inc"

float4 PixelShaderFunction(float4 color : COLOR0, float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 pixel = tex2D(baseSampler, texCoord);
	
	//Set the strength of the displacement
	//(Important to only affect the blue component here - we need to maintain the red/green offset values).
	pixel.b *= color.a;

	//Kill the alpha unless we are blue!
	pixel.a *= pixel.b;

	//Reduce intesity of red / green (displacement) by blue (strength)
	pixel.rg *= pixel.b;

	return pixel;
}

technique Refraction
{
    pass Pass1
    {
		VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
