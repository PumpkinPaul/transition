using System.Xml.Linq;

namespace Transition.Animations
{
    /// <summary>
    /// An Animation specifies a sequence of Commands which control the image displayed by an Animated thing.
    /// </summary>
    public abstract class Command : Resource.Resource
    {						
        /// <summary>
        /// Executes this command. The command may have a 0 duration, in which case the next command should be executed. In this case, execute() returns
	    /// true to indicate that the next command should execute. 
        /// </summary>
        /// <param name="target">The target Animated thing</param>
        /// <returns>If the next command should be immediately executed.</returns>
	    public abstract bool Execute(Sprite target);

        public virtual void Load(XElement element) { }
    }
}
