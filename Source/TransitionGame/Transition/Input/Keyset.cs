using System.Text;

using Microsoft.Xna.Framework.Input;

namespace Transition.Input
{
    /// <summary>
    /// A set of keys that control game actions.
    /// </summary>
    public class Keyset
    {
        public Keys Up = Keys.Up;
        public Keys Down = Keys.Down;
        public Keys Left = Keys.Left;
        public Keys Right = Keys.Right;
        public Keys Fire = Keys.X;
        public Keys Pause = Keys.P;

        public readonly string UpKeyHint;
        public readonly string DownKeyHint;
        public readonly string LeftKeyHint;
        public readonly string RightKeyHint;
        public readonly string SelectKeyHint;
        public readonly string FireKeyHint;
        public readonly string PauseKeyHint;

        public Keyset()
        {
            UpKeyHint = Up.ToString();
            DownKeyHint = Down.ToString();
            LeftKeyHint = Left.ToString();
            RightKeyHint = Right.ToString();
            SelectKeyHint = Fire.ToString();

            FireKeyHint = SelectKeyHint;
            PauseKeyHint = Pause.ToString();
        }

         /// <summary>
         /// 
         /// </summary>
         /// <returns></returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Jump: {0},", Up);
            sb.AppendFormat("Down: {0},", Down);
            sb.AppendFormat("Left: {0},", Left);
            sb.AppendFormat("Right: {0},", Right);
            sb.AppendFormat("Fire: {0},", Fire);
            sb.AppendFormat("Pause: {0}", Pause);
            return sb.ToString();
        }
    }
}