﻿using ProtoBuf;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Destructable Block enemy
    /// </summary>
    [ProtoContract]
    public class DestructibleBlock : Entity
    {
        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        protected int Health;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        public DestructibleBlock(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.DestructibleBlock;

        public override void Initialise(string name)
        {
            base.Initialise(name);
            
            Sprites[0].SetAppearance(Game.AnimationManager.Get("destructible-block.animation"));

            Health = Blueprint.InitialHealth;

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnCollisionWithEntity(Entity collider, float time)
        {
            if (collider.Classification == EntityClassification.Bullet)
            {
                //Bullet will have handled itself
                TakeDamage();
            }
        }

        void TakeDamage()
        {
            Health--;
            if (Health <= 0)
                Deactivate();
            else
            {
                HitTicks = 6;
                PlayRicochet();
            }
        }

        protected override void OnDeactivate()
        {
            base.OnDeactivate();

            var ps = Game.EntityFactory.Create<ParticleSystem>();
            if (ps != null)
            {
                ps.Position = Position;
                ps.TemplateName = "TrooperExplosion";
                Game.World.Add(ps);
            }

            var sfx = Game.EntityFactory.Create<Sound>();
            if (sfx != null)
            {
                if (string.IsNullOrEmpty(Blueprint.ExplosionSfx) == false)
                {
                    sfx.Position = Position;
                    sfx.Name = Blueprint.ExplosionSfx;
                    sfx.Play();
                    Game.World.Add(sfx);
                }
            }
        }
    }
}