using Microsoft.Xna.Framework.Audio;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public class SoundEffectInfo
    {
        public SoundEffect SoundEffect;
        public float Gain;
        public float Pitch;
        public float Pan;
        public int Priority;
        public bool Looped;
    }
}
