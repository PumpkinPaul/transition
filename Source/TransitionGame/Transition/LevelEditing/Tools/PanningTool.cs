using Transition.Input;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class PanningTool : Tool
    {
        public override string CursorName { get { return "ToolboxPan.png"; } }

        public PanningTool(TransitionGame game) : base(game) { }

        public override void Update()
        {
            if (TransitionGame.Instance.MenuManager.HoverControl != null)
                return;

            //Panning controls
            if (MouseHelper.IsLeftButtonDown)
            {
                LevelEditor.Camera.Position.X -= MouseHelper.XMovement * LevelEditor.Camera.Position.Z * -0.001f;
                LevelEditor.Camera.Position.Y += MouseHelper.YMovement * LevelEditor.Camera.Position.Z * -0.001f;
            } 
        }
    }
}
