namespace Transition
{
    public enum LevelActionType
    {
        SpawnActor,
        NoMoreAliens,
        LandNow
    }
}