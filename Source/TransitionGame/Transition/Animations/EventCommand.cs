using System.Xml.Linq;

namespace Transition.Animations
{
    /// <summary>
    /// An Animation command which sets an "event state" in the target Animated thing.
    /// </summary>
    public class EventCommand : Command
    {			
        int _event;
        bool _yield;
        			
	    public override bool Execute(Sprite target)
        {
            target.Event = _event;
			target.Sequence++;;
			return !_yield;
        }

        public override void Load(XElement element) 
        {
			_event = element.GetAttributeInt32("id");
            _yield = element.GetAttributeBool("yield", true);
        }
    }
}
