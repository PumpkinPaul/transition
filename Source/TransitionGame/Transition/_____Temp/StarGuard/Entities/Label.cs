﻿using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Rendering;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// In world notification labels
    /// </summary>
    [ProtoContract]
    public abstract class Label : Entity
    {
        const int InfiniteLife = -1;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        public int LifeTicks;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        protected Label(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.Label;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            LifeTicks = InfiniteLife;

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization()
        {
            InitialiseNonSerialized();
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            if (LifeTicks < 0)
                return;

            if (LifeTicks == 0)
            {
                Deactivate();
                return;
            }

            LifeTicks--;
        }
    }
}