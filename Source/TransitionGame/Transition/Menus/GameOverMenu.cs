using System;
using Microsoft.Xna.Framework;
using Transition.MenuControls;

namespace Transition.Menus
{
    /// <summary>
    /// Represents a menu showed after the player has died.
    /// </summary>
    public class GameOverMenu : Menu
    {
        TextLabelControl _rankValue;

        public bool ScoreAdded;
        public ScoreTable ScoreTable;
        public ScoreItem ScoreItem;
        
        TextLabelControl _leaderboardCaptionLabel1;
        TextLabelControl _leaderboardCaptionLabel2;
        TextLabelControl _leaderboardCaptionLabel3;
        TextLabelControl _leaderboardCaptionLabel4;

        TextLabelControl _unluckyCaptionLabel1;
        TextLabelControl _unluckyCaptionLabel2;
        TextLabelControl _unluckyCaptionLabel3;

		/// <summary>
        /// Initialises a new instance of a StartMenu.
        /// </summary>
        /// <param name="game">A reference to the game.</param>
        /// <param name="title">The title of the menu.</param>
		public GameOverMenu(BaseGame game, string title) : base(game, title) { }
        
        protected override void OnConfigureMenu()
        {
            var yPosition = 5.75f;

            _leaderboardCaptionLabel1 = new TextLabelControl(Game);            
            _leaderboardCaptionLabel1.Position = new Vector2(0.0f, yPosition);
            _leaderboardCaptionLabel1.Text = "score";
            _leaderboardCaptionLabel1.Tint = Theme.LabelCaptionColor;
            _leaderboardCaptionLabel1.Alignment = Alignment.Centre;
            _leaderboardCaptionLabel1.FontSize = Theme.FontSize;

            yPosition = 4.5f;

            _leaderboardCaptionLabel2 = new TextLabelControl(Game);            
            _leaderboardCaptionLabel2.Position = new Vector2(0.0f, yPosition);
            _leaderboardCaptionLabel2.Text = "awesome!";
            _leaderboardCaptionLabel2.Tint = Theme.LabelValueColor;
            _leaderboardCaptionLabel2.Alignment = Alignment.Centre;
            _leaderboardCaptionLabel2.FontSize = Theme.FontSize * 2;

            _unluckyCaptionLabel1 = new TextLabelControl(Game);            
            _unluckyCaptionLabel1.Position = new Vector2(0.0f, yPosition);
            _unluckyCaptionLabel1.Text = "unlucky";
            _unluckyCaptionLabel1.Tint = Theme.LabelValueColor;
            _unluckyCaptionLabel1.Alignment = Alignment.Centre;
            _unluckyCaptionLabel1.FontSize = Theme.FontSize * 2;

            yPosition -= 2.25f;

            _leaderboardCaptionLabel3 = new TextLabelControl(Game);            
            _leaderboardCaptionLabel3.Position = new Vector2(0.0f, yPosition);
            _leaderboardCaptionLabel3.Text = "you made the leaderboard";
            _leaderboardCaptionLabel3.Tint = Theme.LabelCaptionColor;
            _leaderboardCaptionLabel3.Alignment = Alignment.Centre;
            _leaderboardCaptionLabel3.FontSize = Theme.FontSize;

            _unluckyCaptionLabel2 = new TextLabelControl(Game);            
            _unluckyCaptionLabel2.Position = new Vector2(0.0f, yPosition);
            _unluckyCaptionLabel2.Text = "you didn't make the leaderboard";
            _unluckyCaptionLabel2.Tint = Theme.LabelCaptionColor;
            _unluckyCaptionLabel2.Alignment = Alignment.Centre;
            _unluckyCaptionLabel2.FontSize = Theme.FontSize;

            yPosition -= 1.25f;

            _leaderboardCaptionLabel4 = new TextLabelControl(Game);            
            _leaderboardCaptionLabel4.Position = new Vector2(-0.5f, yPosition);
            _leaderboardCaptionLabel4.Text = "rank:";
            _leaderboardCaptionLabel4.Tint = Theme.LabelCaptionColor;
            _leaderboardCaptionLabel4.Alignment = Alignment.CentreRight;
            _leaderboardCaptionLabel4.FontSize = Theme.FontSize;

            _unluckyCaptionLabel3 = new TextLabelControl(Game);            
            _unluckyCaptionLabel3.Position = new Vector2(-0.5f, yPosition);
            _unluckyCaptionLabel3.Text = "score:";
            _unluckyCaptionLabel3.Tint = Theme.LabelCaptionColor;
            _unluckyCaptionLabel3.Alignment = Alignment.CentreRight;
            _unluckyCaptionLabel3.FontSize = Theme.FontSize;

            _rankValue = new TextLabelControl(Game);            
            _rankValue.Position = new Vector2(0.0f, yPosition);
            _rankValue.Tint = Theme.LabelCaptionColor;
            _rankValue.Alignment = Alignment.CentreLeft;
            _rankValue.FontSize = Theme.FontSize * 1.5f;

            AddControl(_leaderboardCaptionLabel1, -1);
            AddControl(_leaderboardCaptionLabel2, -1);
            AddControl(_leaderboardCaptionLabel3, -1);
            AddControl(_leaderboardCaptionLabel4, -1);
            AddControl(_rankValue, -1);

            AddControl(_unluckyCaptionLabel1, -1);
            AddControl(_unluckyCaptionLabel2, -1);
            AddControl(_unluckyCaptionLabel3, -1);

            yPosition = -1.0f;
            var continueButton = new TextButtonControl(Game);
            continueButton.Position = new Vector2(0.0f, yPosition);
            continueButton.Text = "continue";
            continueButton.HelpText = "continue playing the current level";
            continueButton.Click += (sender, e) => Game.ContinuePlaySession();

            yPosition -= 2.0f;
            var retryButton = new TextButtonControl(Game);
			retryButton.Position = new Vector2(0.0f, yPosition);
			retryButton.Text = "play again";
            retryButton.HelpText = "begin a new Game with the current settings";
			retryButton.Click += (sender, e) => Game.BeginPlaySession();
            
            yPosition -= 2.0f;
            var mainMenuButton = new TextButtonControl(Game);
			mainMenuButton.Position = new Vector2(0.0f, yPosition);
			mainMenuButton.Text = "main menu";
			mainMenuButton.HelpText = "return to the main menu";
            mainMenuButton.Click += (sender, e) => Close();
            
            AddControl(continueButton, GetNextTabIndex());
            AddControl(retryButton, GetNextTabIndex());
            AddControl(mainMenuButton, GetNextTabIndex());
        }

        /// <summary>
        /// Occurs when a menu is added to the MenuManager.
        /// </summary>
        protected override void OnAddedToManager()
        {
            if (ScoreAdded)
            {
                var position = ScoreTable.CurrentScoreId + 1;
                _rankValue.Text = Ordinal(position);

                _leaderboardCaptionLabel2.Text = ScoreItem.DisplayScore;
                _leaderboardCaptionLabel2.Tint = Theme.LabelValueColor;

                _leaderboardCaptionLabel1.Visible = true;
                _leaderboardCaptionLabel2.Visible = true;
                _leaderboardCaptionLabel3.Visible = true;
                _leaderboardCaptionLabel4.Visible = true;

                _unluckyCaptionLabel1.Visible = false;
                _unluckyCaptionLabel2.Visible = false;
                _unluckyCaptionLabel3.Visible = false;
            }
            else
            {
                _rankValue.Text = ScoreItem.DisplayScore;

                _leaderboardCaptionLabel2.Tint = Theme.LabelValueColor;

                _leaderboardCaptionLabel1.Visible = false;
                _leaderboardCaptionLabel2.Visible = false;
                _leaderboardCaptionLabel3.Visible = false;
                _leaderboardCaptionLabel4.Visible = false;

                _unluckyCaptionLabel1.Visible = true;
                _unluckyCaptionLabel2.Visible = true;
                _unluckyCaptionLabel3.Visible = true;
            }
        }

        public static string Ordinal(int number)
        {
	        string suffix;

	        var ones = number % 10;
	        var tens = (int)Math.Floor(number / 10.0) % 10;

	        if (tens == 1)
	        {
		        suffix = "th";
	        }
	        else
	        {
		        switch (ones)
		        {
			        case 1:
				        suffix = "st";
				        break;

			        case 2:
				        suffix = "nd";
				        break;

			        case 3:
				        suffix = "rd";
				        break;

			        default:
				        suffix = "th";
				        break;
		        }
	        }

	        return $"{number}{suffix}";
        }
    }
}
