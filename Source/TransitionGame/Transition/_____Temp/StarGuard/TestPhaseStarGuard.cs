using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using Gearset;
using Krypton.Common;
using Krypton.Lights;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProtoBuf;
using ProtoBuf.Meta;
using Transition.Cameras;
using Transition.GamePhases;
using Transition.Gearset;
using Transition.Input;
using Transition.Rendering;
using Transition._____Temp.StarGuard;
using Transition._____Temp.StarGuard.Entities;
using World = Transition._____Temp.StarGuard.World;

namespace Transition
{
    public partial class TransitionGame
    {
        public World World;
        public GameMap GameMap;
        public _____Temp.StarGuard.Entities.Player Player;
        public CameraProxy CameraProxy;
        public EntityFactory EntityFactory { get; set; }
        public _____Temp.StarGuard.CollisionMapManager NewCollisionMapManager { get; set; }
    }
}

namespace Transition._____Temp.StarGuard
{
    /// <summary>
    /// Star Guard implementation
    /// </summary>
    public class TestPhaseStarGuard : GamePhase
    {
        readonly RuntimeTypeModel _runtimeTypeModel;

        readonly string _gameMapsPath = Path.Combine(Platform.ResourcesPath, "GameMaps");
        public static DynamicAudio _audio = new DynamicAudio();

        //Looks like we can get seemless rendering of tiles using the following params.
        //1) render the tiles using TransparentRenderStyle.Instance and utilise a 1 pixel border around the tile

        //2) render the tiles using PointFilterRenderStyle.Instance

        Camera _testCamera;
        
        //Platforming Action with REWINDING!
        bool _latched;
        bool _rewinding;
        private const int RewindThisManyFrames = 1;
        //World _world;
        const int MemoryStreamBufferSize = 1024 * 1024 * 450;
        readonly MemoryStream _memoryStream = new MemoryStream(MemoryStreamBufferSize); //256 MB buffer for replay system
        readonly List<long> _framePositions = new List<long>(1024);
        readonly Stack<long> _rewindPositions = new Stack<long>(1024);
        int _replayFrameId;

        public const float Gravity = -0.025f;
        
        bool _replay;

        public float LevelTimePercent => 100.0f * LevelTimeNormalized;
        public float LevelTimeNormalized => (float)_memoryStream.Position / MemoryStreamBufferSize;

        public int RewindableFrameCount => _rewindPositions.Count;

        public bool IsRewinding => _rewinding;

        public TestPhaseStarGuard(BaseGame game) : base(game) 
        {
            //StarGuardMapToTileMapConvertor.ConvertAllMaps();

            GameServices.Instance = new GameServices();
            
            TransitionGame.Instance.EntityFactory = new EntityFactory((TransitionGame)game);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Entities.Player), 2);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(CameraProxy), 2);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Weapon), 2);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Bullet), 16);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(EnemyBullet), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Zomboid), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Trooper), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Flag), 16);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(ParticleSystem), 128);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Sound), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Scientist), 8);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(MovingBlock), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Mine), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Stalactite), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Turret), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(TripleGun), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Flier), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Rhino), 16);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Octopus), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(ExplosiveBlock), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(EliteTrooper), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(DestructibleBlock), 64);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(EliteJumper), 32);
            TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(ScoreLabel), 32);
            //TransitionGame.Instance.EntityFactory.RegisterFactory(typeof(Laser), 32);

            GameServices.Instance.Add<TransitionGame>(TransitionGame.Instance);
            GameServices.Instance.Add<EntityFactory>(TransitionGame.Instance.EntityFactory);

            _runtimeTypeModel = ProtobufConfiguration.InitialiseProtobuf();

            TransitionGame.Instance.NewCollisionMapManager = new CollisionMapManager();
            TransitionGame.Instance.NewCollisionMapManager.RegisterEntityCollider(EntityClassification.Player, EntityClassification.Enemy);
            TransitionGame.Instance.NewCollisionMapManager.RegisterEntityCollider(EntityClassification.Player, EntityClassification.EnemyBullet);
            TransitionGame.Instance.NewCollisionMapManager.RegisterEntityCollider(EntityClassification.Player, EntityClassification.Flag);
            TransitionGame.Instance.NewCollisionMapManager.RegisterEntityCollider(EntityClassification.Player, EntityClassification.Scientist);
            TransitionGame.Instance.NewCollisionMapManager.RegisterEntityCollider(EntityClassification.Bullet, EntityClassification.Enemy);
            TransitionGame.Instance.NewCollisionMapManager.RegisterEntityCollider(EntityClassification.ExplosiveBlock, EntityClassification.Player);
            TransitionGame.Instance.NewCollisionMapManager.RegisterEntityCollider(EntityClassification.DestructibleBlock, EntityClassification.Bullet);
            TransitionGame.Instance.NewCollisionMapManager.RegisterEntityCollider(EntityClassification.DestructibleBlock, EntityClassification.Player);

            _audio.LoadContent();

            GS.SetFinderSearchFunction(FindEntities);
        }

        FinderResult FindEntities(string searchTerms)
        {
            var result = new FinderResult();
            var searchParams = searchTerms.Split(new [] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // Ignore Case
            for (var i = 0; i < searchParams.Length; i++)
                searchParams[i] = searchParams[i].ToUpper();

            if (TransitionGame.Instance.World?.Entities == null)
                return result;

            //var id = 0;
            foreach (var entity in TransitionGame.Instance.World.Entities)
            {
                var typeName = entity.Value.GetType().Name;
                result.Add(new ObjectDescription(entity.Value, $"{ typeName } Entity", $"{ typeName } # { entity.Value.Seed }"));
            }

            return result;
        }

        protected override void OnCreate()
        {
            Game.ScreenFlashManager.ClearColor = Color.Red;

            _testCamera = new Camera(Game);
            _testCamera.Position.Z = -11.7f;
            _testCamera.Initialise();
            Game.CameraManager.AddCamera(_testCamera);

            InitialiseReplaySystem();
        }

        void InitialiseReplaySystem()
        {
            _latched = false;
            _rewinding = false;
        
            _framePositions.Clear();
            _rewindPositions.Clear();
            _replayFrameId = 0;
            _replay = false;
        }

        public void InitialisePlaySession(IReadableCheckpoint checkpoint)
        {
            BeginLevel(Path.Combine(_gameMapsPath, $"{checkpoint.Level}.tmx"), checkpoint.ShipName);
        }

        void BeginLevel(string filename, string shipName = "Ship1")
        {
            TransitionGame.Instance.World = new World();

            TransitionGame.Instance.GameMap = GameMap.Load(filename);
            TransitionGame.Instance.GameMap.Initialise();
            GameServices.Instance.Add<GameMap>(TransitionGame.Instance.GameMap);

            TransitionGame.Instance.Player = TransitionGame.Instance.EntityFactory.Create<Entities.Player>(shipName);
            TransitionGame.Instance.CameraProxy = TransitionGame.Instance.EntityFactory.Create<CameraProxy>();

            TransitionGame.Instance.Player.Position = TransitionGame.Instance.GameMap.StartPosition;
            TransitionGame.Instance.CameraProxy.MoveToY(TransitionGame.Instance.GameMap.StartPosition.Y, 0);

            TransitionGame.Instance.World.Add(EntityKeys.Player, TransitionGame.Instance.Player);
            TransitionGame.Instance.World.Add(EntityKeys.Camera, TransitionGame.Instance.CameraProxy);
        }

        enum ReplayControl
        {
            Stop,
            Play,
            Rewind,
            StepBack,
            StepForward
        }

        ReplayControl _replayControl;

        protected override void OnUpdate()
        {
            HandleDebugKeys();

            var wasRewinding = _rewinding;

            _rewinding = false;

            var gameInput = Game.ActivePlayer.GameInput;

            if (gameInput.Rewind == false)
                _latched = false;

            if (!_latched && gameInput.Rewind)
            {
                if (_rewindPositions.Count == 0)
                {
                    _latched = true;
                }
                else
                {
                    //Rewind time!
                    _rewinding = true;
                }
            }

            if (_replay)
            {                
                if (_replayFrameId >= _framePositions.Count - 1)
                    _replayFrameId = -1;
                
                if (KeyboardHelper.IsKeyJustPressed(Keys.P)) 
                {
                    _replayControl = ReplayControl.Play;
                    
                }
                else if (KeyboardHelper.IsKeyJustPressed(Keys.O)) 
                {
                    _replayControl = ReplayControl.Stop;
                }
                else if (KeyboardHelper.IsKeyJustPressed(Keys.Left) || (KeyboardHelper.IsKeyPressed(Keys.LeftShift) && KeyboardHelper.IsKeyPressed(Keys.Left))) 
                {
                    _replayControl = ReplayControl.StepBack;
                    _replayFrameId--;
                } 
                else if (KeyboardHelper.IsKeyJustPressed(Keys.Right) || (KeyboardHelper.IsKeyPressed(Keys.LeftShift) && KeyboardHelper.IsKeyPressed(Keys.Right))) 
                {
                    _replayControl = ReplayControl.StepForward;
                    _replayFrameId++;
                }

                if (_replayControl == ReplayControl.Play)
                    _replayFrameId++;

                _replayFrameId = Math.Max(0, _replayFrameId);
                _replayFrameId = Math.Min(_framePositions.Count - 1, _replayFrameId);
                _memoryStream.Position = _framePositions[_replayFrameId];

                TransitionGame.Instance.EntityFactory.ReleaseAllEntities();
                TransitionGame.Instance.SpritePool.DeallocateAllSprites();

                var world = (World)_runtimeTypeModel.DeserializeWithLengthPrefix(_memoryStream, null, typeof(World), PrefixStyle.Base128, 1);
                TransitionGame.Instance.World = world;
                world.StateLoaded(false);

                TransitionGame.Instance.Player = (Entities.Player)world[EntityKeys.Player];
                TransitionGame.Instance.CameraProxy = (CameraProxy)world[EntityKeys.Camera];
            }
            else
            {
                if (_rewinding)
                {
                    TransitionGame.Instance.World.BeforeRewind();
                    LoadState(rewinding: true);
                    TransitionGame.Instance.World.IsRewinding = true;
                }
                else
                {
                    TransitionGame.Instance.World.IsRewinding = false;

                    TransitionGame.Instance.World.Update(wasRewinding);
                }

                SaveState();

                GS.Plot("World Size", LevelTimePercent);
            }

            //TODO - hook this to rewind and replay!
            TransitionGame.Instance.GameMap.Update();

            TransitionGame.Instance.VhsRewindProcessor.Active = TransitionGame.Instance.World.IsRewinding;

            //Some cheesey rewind effect!
            //if (TransitionGame.Instance.World.IsRewinding)
            //{
            //    TransitionGame.Instance.DistortionsProcessor.Active = true;
            //    TransitionGame.Instance.DistortionsProcessor.Enabled = true;

            //    var ce = DistortionEffects.GetReusableColorEffect(30, 90, 0, 12);
            //    var hne = DistortionEffects.GetReusableHorizontalNoiseEffect(60, 90, 0.125f);
            //    var re = DistortionEffects.GetReusableRollEffect(30, 0.03f);

            //    if (ce.IsReady)
            //        DistortionEffects.SetColorEffect(ce);

            //    if (hne.IsReady)
            //        DistortionEffects.SetHorizontalNoiseEffect(hne);

            //    if (re.IsReady)
            //        DistortionEffects.SetRollEffect(re);
            //}
            //else
            //{
            //    TransitionGame.Instance.DistortionsProcessor.Active = false;
            //    TransitionGame.Instance.DistortionsProcessor.Enabled = false;
            //}

            TransitionGame.Instance.Krypton.Lights[0].Position = TransitionGame.Instance.Player.Position;
        }
        
        private void HandleDebugKeys()
        {
            if (KeyboardHelper.IsKeyPressed(Keys.Q))
                _testCamera.Position.Z += 0.3f;
            else if (KeyboardHelper.IsKeyPressed(Keys.A))
                _testCamera.Position.Z -= 0.3f;

            //if (KeyboardHelper.IsKeyPressed(Keys.Space))
            //{
            //    TransitionGame.Instance.Player.Position = TransitionGame.Instance.GameMap.StartPosition;
            //    _replay = false;

            //    _rewindPositions.Clear();
            //    _memoryStream.Position = 0;
            //}

            //if (KeyboardHelper.IsKeyJustPressed(Keys.D0))
            //    BeginLevel(Path.Combine(_gameMapsPath, "0.tmx"));
            //if (KeyboardHelper.IsKeyJustPressed(Keys.D1))
            //    BeginLevel(Path.Combine(_gameMapsPath, "1.tmx"));
            //if (KeyboardHelper.IsKeyJustPressed(Keys.D2))
            //    BeginLevel(Path.Combine(_gameMapsPath, "2.tmx"));
            //if (KeyboardHelper.IsKeyJustPressed(Keys.D3))
            //    BeginLevel(Path.Combine(_gameMapsPath, "3.tmx"));
            //if (KeyboardHelper.IsKeyJustPressed(Keys.D4))
            //    BeginLevel(Path.Combine(_gameMapsPath, "4.tmx"));
            //if (KeyboardHelper.IsKeyJustPressed(Keys.D5))
            //    BeginLevel(Path.Combine(_gameMapsPath, "5.tmx"));
            //if (KeyboardHelper.IsKeyJustPressed(Keys.D6))
            //    BeginLevel(Path.Combine(_gameMapsPath, "6.tmx"));
            //if (KeyboardHelper.IsKeyJustPressed(Keys.D7))
            //    BeginLevel(Path.Combine(_gameMapsPath, "7.tmx"));
            //if (KeyboardHelper.IsKeyJustPressed(Keys.D8))
            //    BeginLevel(Path.Combine(_gameMapsPath, "8.tmx"));
            //if (KeyboardHelper.IsKeyJustPressed(Keys.D9))
            //    BeginLevel(Path.Combine(_gameMapsPath, "9.tmx"));

            //Save world to file
            if (KeyboardHelper.IsKeyJustPressed(Keys.S))
            {
                if (File.Exists("replay.bin"))
                    File.Delete("replay.bin");

                using (var fs = File.Create("replay.bin"))
                {
                    using (var zipStream = new GZipStream(fs, CompressionMode.Compress))
                    {
                        //Write eof for world states.
                        using (var bw = new BinaryWriter(zipStream))
                        {
                            //Write the replay header!

                            //Write the location in the stream of each frame start.
                            bw.Write(_framePositions.Count);
                            foreach(var position in _framePositions)
                                bw.Write(position);

                            //Write all the world states
                            var buffer = _memoryStream.GetBuffer();
                            zipStream.Write(buffer, 0, (int)_memoryStream.Position);
                        } 
                    }
                }

                TransitionGame.Instance.EndPlaySession();
                return;
            }

            if (KeyboardHelper.IsKeyJustPressed(Keys.L))
            {
                _replay = true;
                _replayControl = ReplayControl.Stop;

                _replayFrameId = -1;
                _framePositions.Clear();
                _rewindPositions.Clear();

                _memoryStream.Position = 0;
                using (var ms = new MemoryStream())
                {
                    using (var fs = File.OpenRead("replay.bin"))
                    {
                        using (var zipStream = new GZipStream(fs, CompressionMode.Decompress))
                        {
                            zipStream.CopyTo(ms, (int)fs.Length);
                        }
                    }

                    ms.Position = 0;                  
                    using (var br = new BinaryReader(ms))
                    {
                        //Read the replay header!

                        //Read the location in the stream of each frame start.

                        var frames = br.ReadInt32();
                        for(var i = 0; i < frames; i++)
                            _framePositions.Add(br.ReadInt64());

                        //Write all the world states
                        ms.CopyTo(_memoryStream);
                    }
                }

                _memoryStream.Position = 0;
            }
        }

        private void LoadState(bool rewinding)
        {
            var position = _memoryStream.Position;

            TransitionGame.Instance.EntityFactory.ReleaseAllEntities();
            TransitionGame.Instance.SpritePool.DeallocateAllSprites();

            for (var i = 0; i < Math.Min(_rewindPositions.Count, RewindThisManyFrames); i++)
                _memoryStream.Position = _rewindPositions.Pop();

            TransitionGame.Instance.World = (World)_runtimeTypeModel.DeserializeWithLengthPrefix(_memoryStream, null, typeof(World), PrefixStyle.Base128, 1);
            var world = TransitionGame.Instance.World;
            world.StateLoaded(rewinding);

            TransitionGame.Instance.Player = (Entities.Player)world[EntityKeys.Player];
            TransitionGame.Instance.CameraProxy = (CameraProxy)world[EntityKeys.Camera];

            _memoryStream.Position = position;
        }

        private void SaveState()
        {
            if (!_rewinding)
                _rewindPositions.Push(_memoryStream.Position);

            _framePositions.Add(_memoryStream.Position);
            _runtimeTypeModel.SerializeWithLengthPrefix(_memoryStream, TransitionGame.Instance.World, typeof(World), PrefixStyle.Base128, 1);
        }

        protected override void OnRender()
        {
            _testCamera.Position.X = TransitionGame.Instance.CameraProxy.Position.X;
            _testCamera.Position.Y = TransitionGame.Instance.CameraProxy.Position.Y;

            Game.CameraManager.Render();
            Game.CameraManager.Camera = _testCamera;

            var v = TransitionGame.Instance.CameraManager.ViewMatrix;
            GS.SetMatrices(ref TransitionGame.Instance.WorldMatrix, ref v, ref TransitionGame.Instance.ProjectionMatrix);

            if (TransitionGame.Instance.Krypton.Enabled)
            {
                var bounds = _testCamera.Bounds;
                TransitionGame.Instance.Krypton.Matrix = TransitionGame.Instance.WorldMatrix * v * TransitionGame.Instance.ProjectionMatrix;
                TransitionGame.Instance.Krypton.Bounds = new BoundingRect(bounds.Left, bounds.Bottom, bounds.Width, bounds.Height);
                TransitionGame.Instance.Krypton.Bluriness = 2;
                TransitionGame.Instance.Krypton.LightMapPrepare();
            }

            Game.ScreenFlashManager.ClearColor = TransitionGame.Instance.GameMap.BackgroundColor;
            Game.GraphicsDevice.Clear(Game.ScreenFlashManager.CurrentColor);
            TransitionGame.Instance.GameMap.Draw();

            Game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);
            TransitionGame.Instance.World.Draw();
            Game.Renderer.End();

            if (TransitionGame.Instance.Krypton.Enabled)
            {
                TransitionGame.Instance.Krypton.Draw();

                //Draw the shadow hulls as-is (no shadow stretching) in pure white on top of the shadows
                //You can omit this line if you want to see what the light-map looks like :)
                KryptonDebugDraw();
            }

            Game.CameraManager.Camera = Game.GuiCamera;
            Game.Renderer.Begin(Renderer.SortStrategy.LayerRenderStyleTextureRank);
            if (_rewinding)
            {
                TransitionGame.Instance.Renderer.DrawString(TransitionGame.Instance.FontManager.GetBitmapFont("Bebas Neue"), new Vector2(-18.0f, 9.75f), Vector2.One, "REWINDING", Alignment.CentreLeft, Color.White, TransparentRenderStyle.Instance);
            }
            
            //Rewindable Frames
            var q = new Quad(10.5f, -18.0f, 10.25f, -18 + (_rewindPositions.Count * 0.01f));
            var textureInfo = TransitionGame.Instance.TextureManager.GetTextureInfo("Pixel");
            //TransitionGame.Instance.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, Color.White, TransparentRenderStyle.Instance);
            
            if (_replay) 
            {
                TransitionGame.Instance.Renderer.DrawString(TransitionGame.Instance.FontManager.GetBitmapFont("Bebas Neue"), new Vector2(-18.0f, 9.75f), Vector2.One, "REPLAY", Alignment.CentreLeft, Color.White, TransparentRenderStyle.Instance);

                textureInfo = TransitionGame.Instance.TextureManager.GetTextureInfo("Pixel");

                q = new Quad(10.5f, -18.0f, 10.25f, -18 + (_framePositions.Count * 0.01f));
                TransitionGame.Instance.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, new Color(255, 255, 255) * 0.25f, AlphaBlendingRenderStyle.Instance);

                q = new Quad(10.5f, -18.0f, 10.25f, -18 + (_replayFrameId * 0.01f));
                TransitionGame.Instance.Renderer.DrawSprite(ref q, textureInfo.TextureId, textureInfo.SourceRect, Color.White, AlphaBlendingRenderStyle.Instance);
            }

            Game.Renderer.End();
        }

        private void KryptonDebugDraw()
        {
            TransitionGame.Instance.Krypton.RenderHelper.Effect.CurrentTechnique = TransitionGame.Instance.Krypton.RenderHelper.Effect.Techniques["DebugDraw"];
            TransitionGame.Instance.GraphicsDevice.RasterizerState = new RasterizerState()
            {
                CullMode = CullMode.None,
                FillMode = FillMode.WireFrame,
            };

            if (Keyboard.GetState().IsKeyDown(Keys.H))
            {
                // Clear the helpers vertices
                TransitionGame.Instance.Krypton.RenderHelper.ShadowHullVertices.Clear();
                TransitionGame.Instance.Krypton.RenderHelper.ShadowHullIndicies.Clear();

                foreach (var hull in TransitionGame.Instance.Krypton.Hulls)
                {
                    TransitionGame.Instance.Krypton.RenderHelper.BufferAddShadowHull(hull);
                }


                foreach (var effectPass in TransitionGame.Instance.Krypton.RenderHelper.Effect.CurrentTechnique.Passes)
                {
                    effectPass.Apply();
                    TransitionGame.Instance.Krypton.RenderHelper.BufferDraw();
                }
            }

            if (Keyboard.GetState().IsKeyDown(Keys.J))
            {
                TransitionGame.Instance.Krypton.RenderHelper.ShadowHullVertices.Clear();
                TransitionGame.Instance.Krypton.RenderHelper.ShadowHullIndicies.Clear();

                foreach (Light2D light in TransitionGame.Instance.Krypton.Lights)
                {
                    TransitionGame.Instance.Krypton.RenderHelper.BufferAddBoundOutline(light.Bounds);
                }

                foreach (var effectPass in TransitionGame.Instance.Krypton.RenderHelper.Effect.CurrentTechnique.Passes)
                {
                    effectPass.Apply();
                    TransitionGame.Instance.Krypton.RenderHelper.BufferDraw();
                }
            }

            TransitionGame.Instance.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
        }
    }
}