using System.Collections.Generic;
using Transition.Animations;

namespace Transition.Rendering
{
    /// <summary>
    /// A pool of sprites.
    /// </summary>
    public class SpritePool
    {
        readonly Stack<Sprite> _availableSprites;
        readonly List<Sprite> _allocatedSprites;

        readonly bool _allowGrowing;

        public SpritePool(bool allowGrowing, int size)
        {
            _allowGrowing = allowGrowing;

            _availableSprites = new Stack<Sprite>(size);
            _allocatedSprites = new List<Sprite>(size);

            for(var i = 0; i < size; i++)
                _availableSprites.Push(new Sprite());
        }

        public Sprite Allocate()
        {
            if (_availableSprites.Count == 0)
            {
                if (_allowGrowing)
                    _availableSprites.Push(new Sprite());
                else
                    return null;
            }

            var sprite = _availableSprites.Pop();

            _allocatedSprites.Add(sprite);

            sprite.Init();
            return sprite;
        }

        public void Deallocate(Sprite sprite)
        {
            _allocatedSprites.Remove(sprite);
            _availableSprites.Push(sprite);
        }

        public void DeallocateAllSprites()
        {
            foreach (var sprite in _allocatedSprites)
                _availableSprites.Push(sprite);

            _allocatedSprites.Clear();
        }
    }
}
