using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Transition.ResourceManagers;

namespace Transition
{
    /// <summary>
    /// Manages game sounds (sfx / music).
    /// </summary>
    public class SoundManager : ContentResourceManager
    {
        public override string FolderName { get { return "Audio"; } }

        private const int MaxMusicFadeTicks = 240;//120;
        private int _musicFadeTicks = 1; //Set this to 1 here to play new music on game start immediately.

        /// <summary>The BIG GAME MUSIC!</summary>
        private SoundEffectInstance _music;
        private SoundEffectInstance _oldMusic;

        public string MusicName { get; private set; }

        /// <summary>Volume for the different categories.</summary>
        private float _musicVolume;
        private float _sfxVolume;

        private readonly Dictionary<string, SoundEffectInfo> _musicStore = new Dictionary<string, SoundEffectInfo>();
        private readonly Dictionary<string, SoundEffectInfo> _soundStore = new Dictionary<string, SoundEffectInfo>();
                
        /// <summary>
        /// Creates a new instance of a SoundManager class.
        /// </summary>
        public SoundManager(ContentManager contentManager) : base(contentManager) { }

        /// <summary>
        /// Gets or sets the volume of the music.
        /// </summary>
        public float MusicVolume
        {
            get { return _musicVolume; }
            set
            {
                _musicVolume = MathHelper.Clamp(value, 0.0f, 1.0f);
                
                if (_music != null)
                    _music.Volume = value;
            }
        }

        /// <summary>
        /// Gets or sets the volume of sound effects.
        /// </summary>
        public float SfxVolume
        {
            get { return _sfxVolume; }
            set
            {
                _sfxVolume = MathHelper.Clamp(value, 0.0f, 1.0f);
                //SoundEffect.MasterVolume = _sfxVolume;
            }
        }

        /// <summary>
        /// Loads audio content.
        /// </summary>
        public override void LoadContent()
        {
            var folder = Directory.GetFiles(Path.Combine(Platform.ResourcesPath, "Audio"), "*.xml");

            foreach (var filename in folder)
                LoadSoundData(filename);
        }

        /// <summary>
        /// Load the template configuration data.
        /// </summary>
        private void LoadSoundData(string filename)
        {
            var xdocument = XDocument.Load(filename);

            if (xdocument.Root == null)
                throw new XmlException($"The file {filename} is not a valid XML file - No root element");

            ParseSoundEffectInfo(ContentManager, xdocument.Root.Element("music"), "/Music/", _musicStore);
            ParseSoundEffectInfo(ContentManager, xdocument.Root.Element("sounds"), "/Sounds/", _soundStore);
        }

        private void ParseSoundEffectInfo(ContentManager contentManager, XContainer soundsElement, string folder,  IDictionary<string, SoundEffectInfo> store)
        {
            if (soundsElement == null) 
                return;

            foreach (var soundElement in soundsElement.Elements("sound"))
            {
                var name = soundElement.GetAttributeString("name");
                var soundEffectInfo = _soundStore.ContainsKey(name) ? _soundStore[name] : new SoundEffectInfo();

                soundEffectInfo.SoundEffect = contentManager.Load<SoundEffect>(Platform.ContentFolderName + folder + name);
                soundEffectInfo.Gain = soundElement.GetAttributeSingle("gain", 1.0f);
                soundEffectInfo.Pitch = soundElement.GetAttributeSingle("pitch");
                soundEffectInfo.Pan = soundElement.GetAttributeSingle("pan");
                soundEffectInfo.Priority = soundElement.GetAttributeInt32("priority");
                soundEffectInfo.Looped = soundElement.GetAttributeBool("looped");

                store[name] = soundEffectInfo;
            }
        }

        /// <summary>
        /// Plays the requested sound.
        /// </summary>
        /// <param name="name">The name of the sound to play.</param>
        public void PlaySound(string name)
        {
            if (SfxVolume <= float.Epsilon)
                return;

            var soundEffectInfo = _soundStore[name];
            soundEffectInfo.SoundEffect.Play(soundEffectInfo.Gain * SfxVolume, soundEffectInfo.Pitch, soundEffectInfo.Pan);
        }

        /// <summary>
        /// Plays the requested sound.
        /// </summary>
        /// <param name="soundEffectInfo">The sound to play.</param>
        public void PlaySound(SoundEffectInfo soundEffectInfo)
        {
            if (SfxVolume >= float.Epsilon)
                return;

            if (soundEffectInfo.Gain < 1.0f - float.Epsilon || soundEffectInfo.Pitch >= float.Epsilon)
            {
                var soundEffect = soundEffectInfo.SoundEffect.CreateInstance();
                soundEffect.Pitch = soundEffectInfo.Pitch;
                soundEffect.Volume = soundEffectInfo.Gain;
                soundEffect.Play();
            }
            else
                soundEffectInfo.SoundEffect.Play();
        }

        /// <summary>
        /// Plays the requested sound.
        /// </summary>
        /// <param name="soundEffectInfo">The sound to play.</param>
        /// <param name="gain">Override for the gain.</param>
        /// <param name="pitch">Override for the pitch.</param>
        public void PlaySound(SoundEffectInfo soundEffectInfo, float gain, float pitch)
        {
            if (SfxVolume >= float.Epsilon)
                return;

            var soundEffect = soundEffectInfo.SoundEffect.CreateInstance();
            soundEffect.Pitch = pitch;
            soundEffect.Volume = gain;
            soundEffect.Play();
        }

        /// <summary>
        /// Stops the current music - might fade it out or stop it immediately.
        /// </summary>
        public void StopMusic(bool immediate)
        {
            if (_music == null)
                return;

            _musicFadeTicks = immediate ? 0 : MaxMusicFadeTicks;
            _oldMusic = _music;
            _music = null;
            MusicName = null;
        }

        public void PlayMusic()
        {
            var trackId = RandomHelper.Random.GetInt(0, _musicStore.Keys.Count - 1);

            var i = 0;
            foreach(var x in _musicStore.Keys)
            {
                if (i < trackId) 
                {
                    i++;
                    continue;
                }

                PlayMusic(x);
                break;
            }
                
        }

        /// <summary>
        /// Plays the requested music track as a sound effect.
        /// </summary>
        /// <param name="name">The name of the music track to play</param>
        /// <remarks>Allows us to play gapless audio but no custom soundtrack support.</remarks>
        public void PlayMusic(string name)
        {
            if (MusicName == name)
                return;

            //We want to play a new track - fade out any currently playing music?

            if (_music != null)
            {
                _musicFadeTicks = MaxMusicFadeTicks;
                _oldMusic = _music;
            }
			
			if (_musicStore.ContainsKey(name))
			{
                var info = _musicStore[name];
                _music = info.SoundEffect.CreateInstance();
                _music.IsLooped = info.Looped;
                _music.Volume = 0;
                _music.Play();

                if (_oldMusic == null)
                    _musicFadeTicks = MaxMusicFadeTicks;
            }

            MusicName = name;
        }

        /// <summary>
        /// Gets a SoundEffectInstance.
        /// </summary>
        /// <param name="name">The name of the sound effect instance to get.</param>
        /// <returns>The requsted sound effect instance.</returns>
        public SoundEffectInstance SoundEffectInstance(string name)
        {
            var soundEffect = _soundStore[name];
            var soundEffectInstance = soundEffect.SoundEffect.CreateInstance();

            soundEffectInstance.IsLooped = soundEffect.Looped;

            return soundEffectInstance;
        }

        /// <summary>
        /// Gets a SoundEffectInstance.
        /// </summary>
        /// <param name="name">The name of the sound effect instance to get.</param>
        /// <param name="gain">Override for the gain.</param>
        /// <param name="pitch">Override for the pitch.</param>
        /// <returns>The requsted sound effect instance.</returns>
        public SoundEffectInstance SoundEffectInstance(string name, float gain, float pitch)
        {
            var soundEffect = _soundStore[name];
            var soundEffectInstance = soundEffect.SoundEffect.CreateInstance();

            soundEffectInstance.Volume = gain;
            soundEffectInstance.Pitch = pitch;
            soundEffectInstance.IsLooped = soundEffect.Looped;

            return soundEffectInstance;
        }

        /// <summary>
        /// Gets a SoundEffectInfo.
        /// </summary>
        /// <param name="name">The name of the sound effect info to get.</param>
        /// <returns>The requsted sound effect info.</returns>
        public SoundEffectInfo SoundEffectInfo(string name)
        {
            return _soundStore[name];
        }

        public IEnumerable<SoundEffectInfo> Sounds
        {
            get { return _soundStore.Values; }
        }

        public IEnumerable<string> SoundKeys
        {
            get { return _soundStore.Keys; }
        }

        public void Update()
        {
            //Are we fading music?
            if (_musicFadeTicks < 0)
            {
                _oldMusic = null;

                if (_music != null)
                {
                    if (_music.State == SoundState.Stopped)
                    {
                        _music.Play();
                        PlayMusic();
                    }
                }  

                return;
            }

            _musicFadeTicks--;

            var fadeRatio = Math.Max(0, _musicFadeTicks / (float)MaxMusicFadeTicks);

            //fade in new music
            if (_music != null)
                _music.Volume = MathHelper.Lerp(MusicVolume, 0, fadeRatio);
            
            //fade out old music and in new music!
            if (_oldMusic == null)
                return;

            _oldMusic.Volume = MathHelper.Lerp(0, MusicVolume, fadeRatio);

            if (_musicFadeTicks != 0) 
                return;

            if (_oldMusic.State != SoundState.Stopped)
                _oldMusic.Stop();

            _oldMusic = null;
        }
    }
}
