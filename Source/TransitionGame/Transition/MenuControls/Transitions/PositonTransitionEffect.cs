using Microsoft.Xna.Framework;

namespace Transition.MenuControls.Transitions
{
    /// <summary>
    /// Represents an damage limitation device.
    /// </summary>
    public class PositionTransitionEffect : MenuTransitionEffect
    {        
        readonly Vector2 _position;

        public PositionTransitionEffect(MenuControl menuControl, string name, int delay, int ticks, InterpolationMethod interpolationMethod, TransitionType type, Vector2 position) : base(menuControl, name, delay, ticks, interpolationMethod, type)
        {
            
            _position = position;
        }

        protected override void OnStart()
        {
            MenuControl.AnchorPosition = MenuControl.Position;
        }

        protected override void OnUpdate(float delta)
        {
            var endPosition = Type == TransitionType.Absolute ? _position : MenuControl.AnchorPosition + _position;

            var x = InterpolationMethod.Invoke(MenuControl.AnchorPosition.X, endPosition.X, delta);
            var y = InterpolationMethod.Invoke(MenuControl.AnchorPosition.Y, endPosition.Y, delta);

            MenuControl.Position = new Vector2(x, y);
        }
    }
}
