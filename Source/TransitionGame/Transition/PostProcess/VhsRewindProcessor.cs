using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Transition.PostProcess
{
    /// <summary>
    /// Manages the VHS rewindeffect.
    /// </summary>
    public class VhsRewindProcessor : BaseProcessor
    {
        Texture2D _noiseTexture;

        EffectParameter _timeParameter;

        /// <summary>
        /// Initialises a new instance of a <see cref = "VhsRewindProcessor">VhsRewindProcessor</see>.
        /// </summary>
        /// <param name = "game">A reference to the game.</param>
        public VhsRewindProcessor(BaseGame game) : base(game) { }

        /// <summary>
        /// Occurs when a derived effect needs to load content.
        /// </summary>
        protected override void OnLoadContent()
        {
            Effect = Game.Content.Load<Effect>(Path.Combine(Platform.ContentPath, "Effects/VhsRewind"));

            const int resolution = 256;
            var noisyColors = new Color[resolution * resolution];
            for (var x = 0; x < resolution; x++)
                for (var y = 0; y < resolution; y++)
                    noisyColors[x + y * resolution] = new Color(new Vector3(RandomHelper.FastRandom.Next(1000) / 1000.0f, 0, 0));

            _noiseTexture = new Texture2D(Game.GraphicsDevice, resolution, resolution, false, SurfaceFormat.Color);
            _noiseTexture.SetData(noisyColors);

            Effect.Parameters["noiseTexture"].SetValue(_noiseTexture);

            _timeParameter = Effect.Parameters["time"];
        }

        protected override void OnUnloadContent()
        {
            _noiseTexture.Dispose();
        }

        /// <summary>
        /// Draws the post processing effect.
        /// </summary>
        /// <param name = "spriteBatch"></param>
        /// <param name = "deltaTime">Time passed since the last frame.</param>
        /// <param name = "texture">The texture to draw.</param>
        /// <param name = "renderTarget"></param>
        protected override bool OnDraw(SpriteBatch spriteBatch, float deltaTime, Texture2D texture, RenderTarget2D renderTarget)
        {
            if (Active == false)
                return false;

            _timeParameter.SetValue(Game.GameTimeInSeconds);

            var projection = Matrix.CreateOrthographicOffCenter(0, Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height, 0, 0, 1);
            var halfPixelOffset = Matrix.CreateTranslation(-0.5f, -0.5f, 0);
            Effect.Parameters["MatrixTransform"].SetValue(halfPixelOffset * projection);

            return base.OnDraw(spriteBatch, deltaTime, texture, renderTarget);
        }   
    }
}