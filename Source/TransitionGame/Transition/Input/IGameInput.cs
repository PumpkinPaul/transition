using System.IO;

namespace Transition.Input
{
    /// <summary>
    /// Represents an interface for reading and writing game input 
    /// </summary>
    public interface IGameInput
    {
        void Read(BinaryReader reader);
        void Write(BinaryWriter writer);
    }
}