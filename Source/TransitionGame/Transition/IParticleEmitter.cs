using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// A class to emit particles
    /// </summary>
    /// <remarks>Allows a ParticleEmitter to emit particles using a variety of shapes (e.g. circles, points, etc)</remarks>
    public interface IParticleEmitter
    {
		/// <summary>
        /// Sets the configuration data for the emitter.
        /// </summary>
		/// <param name="data">The data required for configuring the emitter.</param>
		/// <remarks>A point emitter will get passed a single point, a rect emitter passed top, left, right, bottom, etc.</remarks>
        void SetData(string data);
		
        /// <summary>
        /// Gets the position of the next particle to be emitted
        /// </summary>
        /// <returns>Returns a Vector2 defining the position of the particle.</returns>
        Vector2 GetPosition();

        float GetDirection(ref Vector2 emitterPosition, ref Vector2 particlePosition);
    }
}
