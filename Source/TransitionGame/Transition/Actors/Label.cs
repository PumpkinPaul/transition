//TODO: Transition - make all these Label subclasses data driven
using System.Text;
using Microsoft.Xna.Framework;
using Transition.Extensions;
using Transition.Rendering;

namespace Transition.Actors
{
    /// <summary>
    /// Represents scene messages
    /// </summary>
    public abstract class Label : Actor
    {
		/// <summary>Font used to display the text.</summary>
		public BitmapFont BitmapFont;
		
		/// <summary>The text to display.</summary>
		protected StringBuilder StringBuilder = new StringBuilder(64);

        public Color EndTint;

        private int _maxTicks;
		
        /// <summary>
        /// Creates a new instance of a Label
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        protected Label(TransitionGame game) : base(game) 
        { 
            _horizontalConstrainBehaviour = ConstrainBehaviour.None;
            _verticalConstrainBehaviour = ConstrainBehaviour.None;
        }

        /// <summary>
        /// Gets the type of actor.
        /// </summary>
        public override ActorClassification ActorClassification
        {
            get { return ActorClassification.Label; }
        }

        protected override void OnInitialise(string name)
        {
            base.OnInitialise(name);

            StringBuilder.Length = 0;
            EndTint = Tint;
        }
		
		/// <summary>
        // Gets or sets the text to display.
        /// </summary>
        public string Text
        {
            //get { return _text; }
			set 
            { 
                StringBuilder.Length = 0;
                StringBuilder.Append(value); 
            }
        }

         public void SetText(string value)
        {
            StringBuilder.Length = 0;
            StringBuilder.Append(value);
        }

        public void SetText(int value)
        {
            StringBuilder.Length = 0;
            StringBuilder.AppendNumber(value);
        }

        public void SetText(float value)
        {
            StringBuilder.Length = 0;
            StringBuilder.AppendNumber(value);
        }

        public void Append(string value)
        {
            StringBuilder.Append(value);
        }

        public void Append(int value)
        {
            StringBuilder.AppendNumber(value);
        }

        public void Append(float value)
        {
            StringBuilder.AppendNumber(value);
        }
		
		/// <summary>
        /// Called when the actor is added to the scene.
        /// </summary>
        protected override void OnAddedToScene() 
	    { 
			BitmapFont = BitmapFont ?? Theme.Font;
            _maxTicks = LifeTicks;
	    }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnDraw(bool isOnscreen) 
        {   
            if (isOnscreen == false)
                return;

            var tint = Color.Lerp(EndTint, Tint, _lifeTicks / (float)_maxTicks);

            _game.Renderer.DrawString(BitmapFont, Position, _scale, StringBuilder, Alignment.Centre, tint, TransparentRenderStyle.Instance, Layer);     
		}
    }
}
