using Microsoft.Xna.Framework;

namespace Transition.Actors
{
    /// <summary>
    /// 
    /// </summary>
    public class SceneLabel : Label
    {
        public Vector2 Acceleration;

        /// <summary>
        /// Creates a new instance of a ScoreBonusLabel.
        /// </summary>
        /// <param name="game">An instance of the game.</param>
        public SceneLabel(TransitionGame game) : base(game) { }
    }
}
