using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Transition.Input;
using Transition.Rendering;

namespace Transition.LevelEditing.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class SelectionTool : Tool
    {
        public enum Feature
        {
            Move,
            Drag
        }

        public override string CursorName { get { return "MousePointer.png"; } }

        public bool Dragging { get; private set; }
        public bool Moving { get; private set; }
        public Vector2 AnchorPosition { get; private set; }

        readonly TileSelector _tileSelector;
        readonly CameraNodeSelector _cameraNodeSelector;
        readonly LabelSelector _labelSelector;

        public SelectionTool(TransitionGame game) : base(game) 
        { 
            _tileSelector = new TileSelector(this);
            _cameraNodeSelector = new CameraNodeSelector(this);
            _labelSelector = new LabelSelector(this);
        }

        int SelectionCount { get { return _tileSelector.SelectionCount + _cameraNodeSelector.SelectionCount + _labelSelector.SelectionCount; } }

        public override void Update()
        {
            if (MouseHelper.IsLeftButtonJustDown && MouseHelper.IsMouseInWindow && TransitionGame.Instance.MenuManager.HoverControl == null)
            {
                var tileFeature = Feature.Drag;
                if (LevelEditor.CanSelectTiles)
                    tileFeature = _tileSelector.MouseJustDownOverCanvas();

                var nodeFeature = Feature.Drag;
                if (LevelEditor.CanSelectNodes)
                    nodeFeature = _cameraNodeSelector.MouseJustDownOverCanvas();

                var textFeature = Feature.Drag;
                if (LevelEditor.CanSelectLabels)
                    textFeature = _labelSelector.MouseJustDownOverCanvas();

                if (tileFeature == Feature.Move || nodeFeature == Feature.Move || textFeature == Feature.Move)
                {
                    Moving = true;
                    AnchorPosition = LevelEditor.ObjectPosition;
                }
                else
                {
                    Dragging = true;
                    AnchorPosition = LevelEditor.WorldPosition;
                }
            }
            else if (MouseHelper.IsLeftButtonDown && Moving)
            {
                var delta = LevelEditor.ObjectPosition - AnchorPosition;

                if (LevelEditor.CanSelectTiles)
                    _tileSelector.Moving(delta);

                if (LevelEditor.CanSelectNodes)
                    _cameraNodeSelector.Moving(delta);

                if (LevelEditor.CanSelectLabels)
                    _labelSelector.Moving(delta);

                AnchorPosition = LevelEditor.ObjectPosition;
                
            }
            else if (MouseHelper.IsLeftButtonJustUp && Moving)
            {
                Moving = false;

                if (SelectionCount > 0)
                    LevelEditor.SaveEditorState("Move selection");
            }

            else if (MouseHelper.IsLeftButtonJustUp && Dragging)
            {
                var isShiftDown = KeyboardHelper.IsKeyPressed(Keys.LeftShift);

                var selectionBox = new Rect(Math.Max(AnchorPosition.Y, LevelEditor.WorldPosition.Y), Math.Min(AnchorPosition.X, LevelEditor.WorldPosition.X), Math.Min(AnchorPosition.Y, LevelEditor.WorldPosition.Y), Math.Max(AnchorPosition.X, LevelEditor.WorldPosition.X));
                var tilesChanged = LevelEditor.CanSelectTiles && _tileSelector.DragCompleted(isShiftDown, selectionBox);
                var nodesChanged = LevelEditor.CanSelectNodes && _cameraNodeSelector.DragCompleted(isShiftDown, selectionBox);
                var textChanged = LevelEditor.CanSelectLabels && _labelSelector.DragCompleted(isShiftDown, selectionBox);

                RefreshEditorObjectColor();

                AnchorPosition = Vector2.Zero;
                Dragging = false;

                if (tilesChanged || nodesChanged || textChanged)
                {
                    switch (SelectionCount)
                    {
                        case 0:
                            LevelEditor.SaveEditorState("Clear selection");
                            break;
                        case 1:
                            LevelEditor.SaveEditorState("Select item");
                            break;
                        default:
                            LevelEditor.SaveEditorState("Select multiple items");
                            break;
                    }
                }
            }
        }

        public void RefreshEditorObjectColor()
        {
            //Set the colour picker to the aveage colour of all the selected items
            if (_tileSelector.SelectionCount + _labelSelector.SelectionCount > 0)
            {
                var v1 = _tileSelector.GetAcumulatedColorVector() + _labelSelector.GetAcumulatedColorVector();
                var v2 = v1 / (_tileSelector.SelectionCount + _labelSelector.SelectionCount);

                var color = new Color(v2);
                LevelEditor.ObjectColor = color;
            }
        }

        public override void Draw()
        {
            if (Dragging)
            {
                var top = Math.Max(LevelEditor.WorldPosition.Y, AnchorPosition.Y);
                var left = Math.Min(LevelEditor.WorldPosition.X, AnchorPosition.X);
                var bottom = Math.Min(LevelEditor.WorldPosition.Y, AnchorPosition.Y);
                var right = Math.Max(LevelEditor.WorldPosition.X, AnchorPosition.X);

                var selectionQuad = new Quad(top, left, bottom, right);

                var renderStyle = TransparentRenderStyle.Instance;
                var baseColor = new Color(0, 122, 204);
                var color = baseColor * 0.25f;
                Game.Renderer.DrawSprite(ref selectionQuad, Game.Renderer.PixelTextureInfo.TextureId, Game.Renderer.PixelTextureInfo.SourceRect, color, renderStyle);

                color = baseColor * 0.75f;
                Game.Renderer.DrawBox(top, left, bottom, right, color, renderStyle);
            }
        }
    }
}
