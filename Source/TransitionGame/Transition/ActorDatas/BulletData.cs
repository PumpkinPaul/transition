using System;
using System.Collections.Generic;

namespace Transition.ActorDatas
{
    /// <summary>
    /// Configuration data for bullets.
    /// </summary>
    public class BulletData
    {
        public Type ActorClass;
        public string Name;
        public float SpeedMin;
        public float SpeedMax;
        public float Friction;
        public int LifeTicks;
        public float Spread;
        public bool FastMoving;
        public List<BulletUpgrade> Damage;
    }

    public class BulletUpgrade
    {
        public int Damage;
        public int Cost;
    }
}
