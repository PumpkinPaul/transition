﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using ProtoBuf;
using Transition.Rendering;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Sound effects.
    /// </summary>
    [ProtoContract]
    public class Sound : Entity
    {
        public static void Play(TransitionGame game, string explosionSfx, Vector2 position)
        {
            var sfx = game.EntityFactory.Create<Sound>();
            if (sfx == null)
                return;

            if (string.IsNullOrEmpty(explosionSfx))
                return;

            sfx.Position = position;
            sfx.Name = explosionSfx;
            sfx.Play();
            game.World.Add(sfx);
        }

        static readonly Pool<DynamicSoundEffectInstance> DynamicSoundPool = new Pool<DynamicSoundEffectInstance>(false, 32, ()=> new DynamicSoundEffectInstance(44100, (AudioChannels)1), null);

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]
        [ProtoMember(1)]
        public string Name;

        [ProtoMember(2)]
        int _bufferPosition;

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here
        DynamicSoundEffectInstance _dynamicSound;
        //----------------------------------------------------------------------------------------------------

        public Sound(TransitionGame game) : base(game) { }

        public override EntityClassification Classification => EntityClassification.Sound;

        public override void Initialise(string name)
        {
            base.Initialise(name);

            Name = null;
            _bufferPosition = 0;

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization()
        {
            InitialiseNonSerialized();
        }

        void KillSound(bool immediate)
        {
            if (_dynamicSound == null)
                return;

            _dynamicSound.Stop(immediate);
            DynamicSoundPool.Deallocate(_dynamicSound);
            _dynamicSound = null;

            Deactivate();
        }

        public override void BeforeRewind()
        {
            KillSound(immediate:true);
        }

        public override void StateLoaded(World world, bool rewinding) 
        {
            base.StateLoaded(world, rewinding);

            if (!rewinding)
                Play();
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            //Should I play the sound?
            if (wasRewinding)
                Play();

            if (_dynamicSound == null)
                return;

            while (_dynamicSound?.PendingBufferCount < 3)
                SubmitBuffer();
        }

        public void Play()
        {
            EnsureSource();

            _dynamicSound?.Play();
        }

        private void EnsureSource()
        {
            if (_dynamicSound != null)
                return;

            _dynamicSound = DynamicSoundPool.Allocate();
        }

        void SubmitBuffer()
        {
            if (_dynamicSound == null)
                return;

            var audioInfo = TestPhaseStarGuard._audio.GetAudioInfo(Name);

            if (_bufferPosition >= audioInfo.SoundData.Length)
            {
                KillSound(immediate: true);
                return;
            }

            var remainingByteCount = audioInfo.SoundData.Length - _bufferPosition;
            var byteCount = remainingByteCount > audioInfo.SampleSizeInBytes ? audioInfo.SampleSizeInBytes : remainingByteCount;

            _dynamicSound.SubmitBuffer(audioInfo.SoundData, _bufferPosition, byteCount);
            _bufferPosition += byteCount;
        }

    //    #if USE_GEARSET
    //        protected override void OnDraw()
    //        {
    //            base.OnDraw();

    //            Game.Renderer.DrawString(Theme.Current.GetFont(Theme.Fonts.Default), Position, new Vector2(0.75f), $"Sound: {_dynamicSound?.State}", Alignment.Centre, new Color(64,0,0,64), AlphaBlendingRenderStyle.Instance);
    //        }
    //    #endif
    }    
}