using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Transition.Rendering;

namespace Transition
{
    /// <summary>
    /// Represents the stars
    /// </summary>
    public class StarField
    {
        /// <summary>An instance of the game.</summary>
        private readonly TransitionGame _game;

        public bool Active;

        /// <summary>Universe bounds.</summary>
        private readonly Rect _bounds;

        /// <summary>A list of star field layers - allows paralax scrolling.</summary>
        readonly List<StarFieldLayer> _layers = new List<StarFieldLayer>();

        /// <summary>
        /// 
        /// </summary>
        public StarField(TransitionGame game, Rect bounds, float bottomGutter, float topGutter)
        {
            this._game = game;
            this._bounds = bounds;
            //TODO:hacky!
            this._bounds.Left = -20.0f;
            this._bounds.Right = 20.0f;

            //Create layers for the stars to live on - may delegate this to 'scripting'.
            for (var i = 0; i < 5; ++i)
                this._layers.Add(new StarFieldLayer(game, this._bounds, 8, 0.4f - (0.05f * i), bottomGutter, topGutter));
        }

        public Color Color
        {
            set 
            {
                foreach(var layer in _layers)
                    layer.Color = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="force"></param>
        public void Update(float force)
        {
            if (Active == false)
                return;

            var forceDelta = force / this._layers.Count;
            foreach (var layer in this._layers)
            {
                layer.Update(force);
                force -= forceDelta;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Draw()
        {
            if (Active == false)
                return;

            //Gearset.GS.BeginMark(2, "Scene:_starField.Draw();", Color.Pink);
            _game.Renderer.Begin(Renderer.SortStrategy.None);
            
            foreach (var layer in _layers)
                layer.Draw();

            _game.Renderer.End();
            //Gearset.GS.EndMark(2, "Scene:_starField.Draw();");
        }
    }
}
