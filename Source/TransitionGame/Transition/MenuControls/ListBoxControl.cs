using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Transition.MenuControls
{
    /// <summary>
    /// A control that displays a list of options in a box.
    /// </summary>
    public class ListBoxControl : MenuControl
    {		
        enum MouseOverScrollBarType
        {
            None,
            Top,
            Thumb,
            Bottom
        }

        public event EventHandler TextButtonClicked;	

		/// <summary></summary>
		readonly List<string> _items = new List<string>();

        /// <summary>
        /// Gets or sets the font to use for text rendering.
        /// </summary>
        public BitmapFont Font { get; set; }

        /// <summary>
        /// Gets or sets the font to use for text rendering.
        /// </summary>
        public Alignment Alignment { get; set; }

        /// <summary>
        /// Gets or sets the size of text rendering.
        /// </summary>
        public Vector2 FontSize { get; set; }

        /// <summary>
        /// Gets or sets the index of the selected item.
        /// </summary>
        int _selectedIndex;
        public int SelectedIndex 
        {
            get { return _selectedIndex; }
            set
            {
                _selectedIndex = value;

                BringSelectedItemIntoView();
                GetHoverItem(Bounds);
            }
        }

        private void BringSelectedItemIntoView()
        {
            if (_selectedIndex == -1)
                return;

            if (_selectedIndex < _scrollIndex)
            {
                _scrollIndex = _selectedIndex;
            }
            else if (_selectedIndex > _scrollIndex + VisibleRows - 1)
            {
                _scrollIndex = _selectedIndex - VisibleRows + 1;
            }
        }

        public float MinWidth { get; set; }

        public int Rows { get; set; }
        int VisibleRows { get { return  Math.Min(_items.Count, Rows == -1 ? _items.Count : Rows); } }

        public float ScrollWidth { get; set; }

        public Color SelectedItemTint { get; set; }

        int _hoverIndex = -1;

        int _scrollIndex ;
        bool _scrollDrag;
        Vector2 _dragAnchor;
        int _dragAnchorId;

        public override bool AutoTabIndex { get { return true; } }

        /// <summary>
		/// .
		/// </summary>
        public ListBoxControl(BaseGame game) : base(game) 
        {
		    SelectedIndex = -1;
		    AButtonText = null;
            Alignment = Alignment.TopLeft;
			Font = Theme.Font;
            FontSize = Theme.FontSize;
            Rows = -1;
            ScrollWidth = 0.5f;
            SelectedItemTint = Tint;

            CapturesPreviousNextControlRequest = true;
        }

        public override void Load(XElement element)
        {
            base.Load(element);

            Alignment = element.GetAttributeEnum("alignment", Alignment);
            Font = ParseFont(element, Font);
            FontSize = element.GetAttributeVector2("fontSize", FontSize);
            MinWidth = element.GetAttributeSingle("minWidth", MinWidth);
            Rows = element.GetAttributeInt32("rows", Rows);
            ScrollWidth = element.GetAttributeSingle("scrollWidth", ScrollWidth);
            SelectedItemTint = element.GetAttributeColor("selectedItemTint", SelectedItemTint);

            //Content! Might be items
            var selectedIndex = -1;
            foreach (var itemElement in element.Elements("item"))
            {
                selectedIndex++;
                var text = itemElement.GetAttributeString("text");
                if (text == null)
                    continue;

                AddOption(text);

                var selected = itemElement.GetAttributeBool("selected", false);
                if (selected)
                    SelectedIndex = selectedIndex;
            }
        }
		
		/// <summary>
		/// Gets the control bounds (a rectangle defining the control's limits).
		/// </summary>
		public override Rect Bounds
		{
			get 
			{
                var bounds = new Rect(float.MinValue, float.MaxValue, float.MaxValue, float.MinValue);
                
                switch (Alignment)
                {
                    case Alignment.TopLeft:
                        break;

                    default:
                        throw new InvalidOperationException("Only Alignment.TopLeft is currently supported");
                }

                var rows = 0;
                var location = Position;
                foreach(var item in _items)
                {
                    if (rows == Rows)
                        break;
                        
                    var itemRect = Font.GetBounds(location, FontSize, item, Alignment);
                    
                    if (bounds.Top < itemRect.Top)
                        bounds.Top = itemRect.Top;

                    if (bounds.Bottom > itemRect.Bottom)
                        bounds.Bottom = itemRect.Bottom;

                    if (bounds.Right < itemRect.Right)
                        bounds.Right = itemRect.Right;

                    if (bounds.Left > itemRect.Left)
                        bounds.Left = itemRect.Left;

                    location.Y -= FontSize.Y;
               
                    rows++;
                }

                if (bounds.Width < MinWidth)
                    bounds.Right = bounds.Left + MinWidth;
        
                return bounds;
			}
		}

        protected override bool OnCapturePreviousControl()
        {
            if (SelectedIndex > 0)
                SelectedIndex--;

            return true;
        }

        protected override bool OnCaptureNextControl()
        {
            if (SelectedIndex < _items.Count - 1)
                SelectedIndex++;

            return true;
        }

        /// <summary>
        /// .
        /// </summary>
        protected override void OnGotFocus()
        {
            _scrollDrag = false;
        }

        /// <summary>
        /// Gets the selected text.
        /// </summary>
        public string SelectedText
        {
            get 
            {
                if (SelectedIndex != -1)
                    return _items[SelectedIndex];
                
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the number of options in the list.
        /// </summary>
        public int Count
        {
            get { return _items.Count; }
        }

        public override string LeftRightButtonText
        {
            get { return "change value"; }
        }
				
		/// <summary>
		/// .
		/// </summary>
		protected override bool OnIncreaseValue()
		{
            if (SelectedIndex >= _items.Count - 1)
                return false;

            ++SelectedIndex;
            return true;
		}
		
		/// <summary>
		/// .
		/// </summary>
		protected override bool OnDecreaseValue()
		{
            if (SelectedIndex <= 0)
                return false;

            --SelectedIndex;
            return true;
		}

        protected override void OnMouseDown() 
        {
            if (_scrollDrag)
                return;

            var bounds = Bounds;
            var mouseRelative = new Vector2(Game.MenuManager.MouseWorldPosition.X - bounds.Left, bounds.Top - Game.MenuManager.MouseWorldPosition.Y);

            var most = IsMouseOverScrollbar(mouseRelative);
            switch (most)
            {
                case MouseOverScrollBarType.None:
                    SelectedIndex = _hoverIndex;
                    break;

                case MouseOverScrollBarType.Top:
                case MouseOverScrollBarType.Bottom:
                    break;
               
               case MouseOverScrollBarType.Thumb:
                    _scrollDrag = true;
                    _dragAnchor = Game.MenuManager.MouseWorldPosition;
                    _dragAnchorId = _scrollIndex;
                    break;
            }
        }

        protected override void OnMouseMove() 
        {
            var bounds = Bounds;
            
            if (_scrollDrag)
            {
                //+ve = dragging up -ve is dragging down
                var delta = Game.MenuManager.MouseWorldPosition - _dragAnchor;

                //The height of each item in the list
                var itemHeight = bounds.Height / _items.Count;

                var itemsDelta = _dragAnchorId - (int)(delta.Y / itemHeight);
                var maxScrollIndex = _items.Count - VisibleRows;
                _scrollIndex = Math.Min(Math.Max(0, itemsDelta), maxScrollIndex); 
            }
            else
            {
                GetHoverItem(bounds);    
            }
        }

        /// <summary>
		/// .
		/// </summary>
		protected override void OnMouseUp()
		{
            if (_scrollDrag)
            {
                _scrollDrag = false;
                return;
            }
            
            var bounds = Bounds;
            var mouseRelative = new Vector2(Game.MenuManager.MouseWorldPosition.X - bounds.Left, bounds.Top - Game.MenuManager.MouseWorldPosition.Y);

            var most = IsMouseOverScrollbar(mouseRelative);
            switch (most)
            {
                case MouseOverScrollBarType.None:
                case MouseOverScrollBarType.Thumb:
                    return;
            }

            var itemHeight = bounds.Height / _items.Count;
            var item = (int)(mouseRelative.Y / itemHeight);

            switch (most)
            {
                case MouseOverScrollBarType.Top:    
                    _scrollIndex = Math.Max(0, item);
                    break;

                case MouseOverScrollBarType.Bottom:
                    _scrollIndex = 1 + item - VisibleRows;
                    break;
            }
		}

        MouseOverScrollBarType IsMouseOverScrollbar(Vector2 mouseRelative)
        {
            var bounds = Bounds;
            bounds.Left = bounds.Right - ScrollWidth;
            if (bounds.Contains(Game.MenuManager.MouseWorldPosition) == false)
                return MouseOverScrollBarType.None;
               
            var itemHeight = bounds.Height / _items.Count;

            if (mouseRelative.Y < _scrollIndex * itemHeight)
                return MouseOverScrollBarType.Top;

            bounds.Top -= _scrollIndex * itemHeight;
            bounds.Bottom = bounds.Top - (VisibleRows * itemHeight);

            if (bounds.Contains(Game.MenuManager.MouseWorldPosition))
                return MouseOverScrollBarType.Thumb;

            return MouseOverScrollBarType.Bottom;
        }

        void GetHoverItem(Rect bounds)
        {
            if(bounds.Contains(Game.MenuManager.MouseWorldPosition) == false)   
            {
                _hoverIndex = -1;
                return;
            }

            var mouseRelative = new Vector2(Game.MenuManager.MouseWorldPosition.X - bounds.Left, bounds.Top - Game.MenuManager.MouseWorldPosition.Y);

            if (IsMouseOverScrollbar(mouseRelative) != MouseOverScrollBarType.None)
                return;

            _hoverIndex = _scrollIndex + (int)(mouseRelative.Y / FontSize.Y);
        }

        protected override void OnUpdate()
        {
            if (Game.InputMethodManager.InputMethod.SupportsMouse == false)
            {

            }
            else
            { 
                AButtonText = CachedAButtonText;
                HelpText = CachedHelpText;
            }
        }
		
		/// <summary>
		/// .
		/// </summary>
        protected override void OnDraw()
		{
            var location = Position;

            //Determine the colour of each item!

            //Normal items can be either their default color or the focused color
            Color normalTint;
            if (IsFocusControl)
                normalTint = Theme.Current.GetColor(Theme.Colors.Focus) * FocusValue;
            else
                normalTint = Tint;

            //Selected items are alwqays the selected color
            var selectedTint = SelectedItemTint;

            //Hover items are either the hover color or their default color
            Color hoverTint;
            if (IsHoverControl)
                hoverTint = Theme.Current.GetColor(Theme.Colors.Hover);
            else
                hoverTint = normalTint;

            normalTint *= Overlay.Opacity;
            selectedTint *= Overlay.Opacity;
            hoverTint *= Overlay.Opacity;

            //Draw items
            
            var rows = 0;
            for (var index = _scrollIndex; index <= _items.Count; index++)
		    {
                if (rows >= Rows)
                    break;

                if (index >= _items.Count)
                    break;

		        var item = _items[index];

                //Pick the correct color (determined above)
                var tint = index == _selectedIndex ? selectedTint : index == _hoverIndex ? hoverTint : normalTint;

                Game.Renderer.DrawString(Font, location, FontSize, item, Alignment, tint, RenderStyle, Layer);

		        location.Y -= FontSize.Y;

                rows++;
		    }

            //Draw scrollbar
            if (_items.Count > VisibleRows)
            {
                var tint = Tint * Overlay.Opacity;

                var bounds = Bounds;
                bounds.Left = Bounds.Right - ScrollWidth;
                var scrollbarQuad = new Quad(bounds);

                //Scrollbar 
                Game.Renderer.DrawSprite(ref scrollbarQuad, Game.Renderer.PixelTextureInfo.TextureId, Game.Renderer.PixelTextureInfo.SourceRect, tint, RenderStyle, Layer);

                //Thumb thing
                var itemHeight = bounds.Height / _items.Count;

                bounds.Top -= _scrollIndex * itemHeight;
                bounds.Bottom = bounds.Top - (VisibleRows * itemHeight);
                scrollbarQuad = new Quad(bounds);

                Game.Renderer.DrawSprite(ref scrollbarQuad, Game.Renderer.PixelTextureInfo.TextureId, Game.Renderer.PixelTextureInfo.SourceRect, SelectedItemTint, RenderStyle, Layer);
            }
		}
		
		/// <summary>
		/// Adds an option to the list.
		/// </summary>
		/// <param name="value">The value to add.</param>
		public void AddOption(string value)
		{
			_items.Add(value);
		}
		
		/// <summary>
		/// Adds an option to the list.
		/// </summary>
		/// <param name="value">The value to add.</param>
		public void AddOption(int value)
		{
			_items.Add(value.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Clears all the options from the list.
		/// </summary>
		public void ClearOptions()
		{
			SelectedIndex = -1;
			_items.Clear();
		}
		
		/// <summary>
		/// Sets the selected item in the list.
		/// </summary>
		/// <param name="textToFind">The text used to perform the match.</param>
		public void SetSelectedItemByText(string textToFind)
		{			
			var index = _items.FindIndex(optionText => (optionText == textToFind));
			if (index > -1)
				SelectedIndex = index;
		}

        /// <summary>
        /// Sets the selected item in the list.
        /// </summary>
        /// <param name="value">A value indicating the item in the list to set.</param>
        public void SetSelectedItemByBoolean(bool value)
        {
            SelectedIndex = value == false ? 0 : 1;
        }

        public virtual void RaiseTextButtonClicked(EventArgs e)
		{		
			//Now raise the event to any subscribers
			//Note the lock to prevent race condition (needed in XNA?)
            var handler = TextButtonClicked;

            if (handler != null)
                handler(this, e);
		}
    }
}
