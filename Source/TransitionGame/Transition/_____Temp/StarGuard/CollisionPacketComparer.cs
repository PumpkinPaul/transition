using System.Collections.Generic;

namespace Transition._____Temp.StarGuard
{
    public class CollisionPacketComparer : IComparer<CollisionPacket>
    {
        public static  readonly CollisionPacketComparer Default = new CollisionPacketComparer();

        public int Compare(CollisionPacket a, CollisionPacket b)
        {
            if (a.Time > b.Time)
                return 1;

            if (a.Time < b.Time)
                return -1;

            return 0;
        }
    }
}
