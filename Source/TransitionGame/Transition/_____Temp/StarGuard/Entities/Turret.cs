﻿using Microsoft.Xna.Framework;
using ProtoBuf;
using Transition.Extensions;

namespace Transition._____Temp.StarGuard.Entities
{
    /// <summary>
    /// Turret enemy
    /// </summary>
    [ProtoContract]
    public class Turret : Enemy
    {
        const float DirectionDown = MathHelper.Pi + MathHelper.PiOver2;
        const float DirectionUp= MathHelper.PiOver2;
        const float DirectionLeft = MathHelper.Pi;
        const float DirectionRight = 0.0f;

        //----------------------------------------------------------------------------------------------------
        //Serialized fields go here [ProtoMember(x)]

        //----------------------------------------------------------------------------------------------------
        //Not Serialized fields go here

        //----------------------------------------------------------------------------------------------------

        bool IsPointingDown => Direction.FloatEquals(DirectionDown);
        bool IsPointingUp => Direction.FloatEquals(DirectionUp);
        bool IsPointingLeft => Direction.FloatEquals(DirectionLeft);
        bool IsPointingRight => Direction.FloatEquals(DirectionRight);
        
        public Turret(TransitionGame game) : base(game) { }

        public override void Initialise(string name)
        {
            base.Initialise(name);

            InitialiseNonSerialized();
        }

        void InitialiseNonSerialized() { }

        [ProtoAfterDeserialization]
        // ReSharper disable once UnusedMember.Local
        void AfterDeserialization() 
        { 
            InitialiseNonSerialized();
        }

        protected override void OnAddedToWorld(World world)
        {
            //Check tiles to left and right to determine which way to face
            var coords = Game.GameMap.GetTileCoordinates(Position);

            if (Game.GameMap.GetTileCollision(new Point(coords.X - 1, coords.Y)) == GameMap.TileCollision.Impassable)
            {
                //Solid tile to left so pointing right
                Direction = DirectionRight;
                Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-07.face-right.animation"));
            }
            else if (Game.GameMap.GetTileCollision(new Point(coords.X + 1, coords.Y)) == GameMap.TileCollision.Impassable)
            {
                //Solid tile to right so pointing left
                Direction = DirectionLeft;
                Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-07.face-left.animation"));
            }
            else if (Game.GameMap.GetTileCollision(new Point(coords.X, coords.Y + 1)) == GameMap.TileCollision.Impassable)
            {
                //Solid tile above so pointing down
                Direction = DirectionDown;
                Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-07.face-down.animation"));
            }
            else if (Game.GameMap.GetTileCollision(new Point(coords.X, coords.Y - 1)) == GameMap.TileCollision.Impassable)
            {
                //Solid tile below so pointing up
                Direction = DirectionUp;
                Sprites[0].SetAppearance(Game.AnimationManager.Get("enemy-07.face-up.animation"));
            }

            Collidable = false;
        }

        protected override void OnUpdate(bool wasRewinding)
        {
            if (IsPlayerInRange == false)
                return;

            if (Sprites[0].Event == 1)
                CreateBullet();
        }

        void CreateBullet()
        {
            var bullet = Game.EntityFactory.Create(typeof(EnemyBullet), "TurretBullet");
            if (bullet != null)
            {
                bullet.Position = Position;
                bullet.Velocity = VectorHelper.Polar(0.15f, Direction);
                Game.World.Add(bullet);
            }
        }
    }
}