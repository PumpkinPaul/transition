using Microsoft.Xna.Framework;

namespace Transition
{
    /// <summary>
    /// 
    /// </summary>
    public class GodRaysTemplate
	{	
        public int RayCount;

        public TextureInfo TextureInfo;

        public float MasterAlphaMin;
        public float MasterAlphaMax = 1.0f;
        
        public int FadeInTicks = 5;
        public int VisibleTicks = 15;
        public int FadeOutTicks = 15;

        public int TotalTicks { get { return FadeInTicks + VisibleTicks + FadeOutTicks; } }

        public Color InnerColor = Color.White;
        public Color OuterColor = Color.White * 0.0f;

        public float InnerRadiusStart;
        public float InnerRadiusEnd;

        public float OuterRadiusStart;
        public float OuterRadiusEnd;
        public float OuterRadiusModMin; //modifier for ray length;
        public float OuterRadiusModMax; //modifier for ray length;

        public float WidthStart = 10.0f; //angle difference in degrees at inner radius (start)
        public float WidthEnd = 10.0f; //angle difference in degrees at outer radius (tip)

        public float RotationMin;
        public float RotationMax;
	}
}
