using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Nuclex.Input;
using Nuclex.Input.Devices;

namespace Transition.Input
{
    /// <summary>
    /// Input helper class, captures XBox 360 (XInput) and Direct Input controller input and provides some nice helper methods and properties.
    /// Will also keep track of the last frame states for comparison if a button was just pressed this frame, but not already in the last frame.
    /// </summary>
    public static class GamePadHelper
    {
        public const float AnalogueToDigitalThreshold = 0.5f;
        public const int MaxXInputGamePads = 4;
        public const int MaxDirectInputGamePads = 4;
        public const int MaxGamePads = MaxXInputGamePads + MaxDirectInputGamePads;

        private static readonly ExtendedGamePadState[] ExtendedGamePadState = new ExtendedGamePadState[MaxGamePads];
        private static readonly ExtendedGamePadState[] ExtendedGamePadStateLastFrame = new ExtendedGamePadState[MaxGamePads];

        private static readonly List<GamePadMapping> GamePadMappings = new List<GamePadMapping>(MaxGamePads);
        
        private static readonly Vector2[] LeftSticks = new Vector2[MaxGamePads];
        private static readonly Vector2[] LeftSticksLastFrame = new Vector2[MaxGamePads];

        private static readonly Vector2[] RightSticks = new Vector2[MaxGamePads];
        private static readonly Vector2[] RightSticksLastFrame = new Vector2[MaxGamePads];

        public static InputManager InputManager { get; set; }

        public static void LoadContent()
        {
            LoadGamePadMappings();
        }

        private static void LoadGamePadMappings()
        {
            //Add default Xbox 360 / XINPUT mappings first
            for (var i = 0; i < MaxXInputGamePads; i++)
            {
                GamePadMappings.Add(new GamePadMapping());
            }

            //Now load in any custom mappings the player may have specified.
            var mappingsFolder = Path.Combine(Platform.ResourcesPath, "GamePadMappings");
            foreach (var filename in Directory.GetFiles(mappingsFolder, "*.mapping"))
            {
                var x = new XmlSerializer(typeof(GamePadMapping));
                using (var stream = TitleContainer.OpenStream(filename))
                {
                    var mapping = (GamePadMapping)x.Deserialize(stream);
                    GamePadMappings.Add(mapping);
                }
            }

            //Finally, pad the collection so we always have a minimum of (currently) 8 gamepads; 4 XInput and 4 Direct Input.
            while (GamePadMappings.Count < MaxGamePads)
            {
                GamePadMappings.Add(new GamePadMapping());
            }
        }

        /// <summary>
        /// Update, called from BaseGame.Update().
        /// Will catch all new states for keyboard, mouse and the gamepad.
        /// </summary>
        internal static void Update()
        {
            for (var i = 0; i < MaxGamePads; ++i)
            {
                var playerIndex = (ExtendedPlayerIndex)i;
                var gamePad = InputManager.GetGamePad(playerIndex);

                ExtendedGamePadStateLastFrame[i] = ExtendedGamePadState[i];
                ExtendedGamePadState[i] = gamePad.GetExtendedState();

                //Cache the Thumbsticks as we may have to apply dead zones and best to just do it once now.
                LeftSticksLastFrame[i] = LeftSticks[i];
                RightSticksLastFrame[i] = RightSticks[i];

                //Apply dead zones to analogue sticks for Direct Input pads only - we'll just use the defaults for the XInput pads as it's a load of work to implement properly for those.
                if (i < MaxXInputGamePads)
                {
                    LeftSticks[i] = new Vector2(GetRawLeftStickXAxis(playerIndex), GetRawLeftStickYAxis(playerIndex));
                    RightSticks[i] = new Vector2(GetRawRightStickXAxis(playerIndex), GetRawRightStickYAxis(playerIndex));
                }
                else if (i >= MaxXInputGamePads)
                {
                    LeftSticks[i] = GamePadDeadZoneUtils.ApplyLeftStickDeadZone(GetRawLeftStickXAxis(playerIndex), GetRawLeftStickYAxis(playerIndex), GamePadMappings[i].LeftStickDeadZone);
                    RightSticks[i] = GamePadDeadZoneUtils.ApplyRightStickDeadZone(GetRawRightStickXAxis(playerIndex), GetRawRightStickYAxis(playerIndex), GamePadMappings[i].RightStickDeadZone);
                }
            }
        }
    
        public static bool IsGamePadConnected(ExtendedPlayerIndex playerIndex)
        {
            return InputManager.GetGamePad(playerIndex).IsAttached;
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //These routines test whether buttons pressed (ie is the button down? we don't care when the press actually started)
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>Gets whether the A button is pressed this update.</summary>
        public static bool IsAPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonPressed(playerIndex, GamePadMappings[(int)playerIndex].A);
        }

        /// <summary>Gets whether the B button is pressed this update.</summary>
        public static bool IsBPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonPressed(playerIndex, GamePadMappings[(int)playerIndex].B);
        }

        /// <summary>Gets whether the X button is pressed this update.</summary>
        public static bool IsXPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonPressed(playerIndex, GamePadMappings[(int)playerIndex].X);
        }

        /// <summary>Gets whether the Y button is pressed this update.</summary>
        public static bool IsYPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonPressed(playerIndex, GamePadMappings[(int)playerIndex].Y);
        }

        /// <summary>Gets whether the Start button is pressed this update.</summary>
        public static bool IsStartPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonPressed(playerIndex, GamePadMappings[(int)playerIndex].Start);
        }

        /// <summary>Gets whether the LeftShoulder button is pressed this update.</summary>
        public static bool IsLeftShoulderPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonPressed(playerIndex, GamePadMappings[(int)playerIndex].LeftShoulder);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //These routines test for buttons pressed this frame (ie the buttons were not pressed the previous frame)
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>Gets whether the A button was pressed this update.</summary>
        public static bool IsAJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonJustPressed(playerIndex, GamePadMappings[(int)playerIndex].A);
        }

        /// <summary>Gets whether the B button was pressed this update.</summary>
        public static bool IsBJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonJustPressed(playerIndex, GamePadMappings[(int)playerIndex].B);
        }

        /// <summary>Gets whether the X button was pressed this update.</summary>
        public static bool IsXJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonJustPressed(playerIndex, GamePadMappings[(int)playerIndex].X);
        }

        /// <summary>Gets whether the Y button was pressed this update.</summary>
        public static bool IsYJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonJustPressed(playerIndex, GamePadMappings[(int)playerIndex].Y);
        }

        /// <summary>Gets whether the Back button was pressed this update.</summary>
        public static bool IsBackJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonJustPressed(playerIndex, GamePadMappings[(int)playerIndex].Back);
        }

        /// <summary>Gets whether the Start button was pressed this update.</summary>
        public static bool IsStartJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonJustPressed(playerIndex, GamePadMappings[(int)playerIndex].Start);
        }

        /// <summary>Gets whether the Left Shoulder button was pressed this update.</summary>
        public static bool IsLeftShoulderJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonJustPressed(playerIndex, GamePadMappings[(int)playerIndex].LeftShoulder);
        }

        /// <summary>Gets whether the Right Shoulder button was pressed this update.</summary>
        public static bool IsRightShoulderJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsButtonJustPressed(playerIndex, GamePadMappings[(int)playerIndex].RightShoulder);
        }

        ///// <summary>Gets whether the Big button was pressed this update.</summary>
        //public static bool IsBigButtonJustPressed(ExtendedPlayerIndex playerIndex)
        //{
        //    return IsButtonJustPressed(playerIndex, GamePadMappings[(int)playerIndex].BigButton);
        //}

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //These routines test whether dpads are pressed (ie is the dpad pushed in a direction? we don't care when the press actually started)
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>Gets whether the DPad up button is pressed this update.</summary>
        public static bool IsDPadUpPressed(ExtendedPlayerIndex playerIndex)
        {
            GamePadDPad dpadThisFrame;

            GetGamePadDPads(playerIndex, out dpadThisFrame);
                
            return dpadThisFrame.Up == ButtonState.Pressed;
        }

        /// <summary>Gets whether the DPad down button is pressed this update.</summary>
        public static bool IsDPadDownPressed(ExtendedPlayerIndex playerIndex)
        {
            GamePadDPad dpadThisFrame;

            GetGamePadDPads(playerIndex, out dpadThisFrame);
                
            return dpadThisFrame.Down == ButtonState.Pressed;
        }

        /// <summary>Gets whether the DPad left button is pressed this update.</summary>
        public static bool IsDPadLeftPressed(ExtendedPlayerIndex playerIndex)
        {
            GamePadDPad dpadThisFrame;

            GetGamePadDPads(playerIndex, out dpadThisFrame);
                
            return dpadThisFrame.Left == ButtonState.Pressed;
        }

        /// <summary>Gets whether the DPad right button is pressed this update.</summary>
        public static bool IsDPadRightPressed(ExtendedPlayerIndex playerIndex)
        {
            GamePadDPad dpadThisFrame;

            GetGamePadDPads(playerIndex, out dpadThisFrame);
                
            return dpadThisFrame.Right == ButtonState.Pressed;
        }

        /// <summary>Gets whether the DPad Right button was pressed this update.</summary>
        private static void GetGamePadDPads(ExtendedPlayerIndex playerIndex, out GamePadDPad dpadThisFrame)
        {
            var povThisFrame = ExtendedGamePadState[(int)playerIndex].Pov1;

            dpadThisFrame = Nuclex.Input.Devices.ExtendedGamePadState.DpadFromPov(povThisFrame);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //These routines test for dpads pressed this frame (ie the dpads were not pressed the previous frame)
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>Gets whether the DPad Up button was pressed this update.</summary>
        public static bool IsDPadUpJustPressed(ExtendedPlayerIndex playerIndex)
        {
            GamePadDPad dpadThisFrame;
            GamePadDPad dpadLastFrame;

            GetGamePadDPads(playerIndex, out dpadThisFrame, out dpadLastFrame);
                
            return dpadThisFrame.Up == ButtonState.Pressed && dpadLastFrame.Up == ButtonState.Released;
        }

        /// <summary>Gets whether the DPad Down button was pressed this update.</summary>
        public static bool IsDPadDownJustPressed(ExtendedPlayerIndex playerIndex)
        {
            GamePadDPad dpadThisFrame;
            GamePadDPad dpadLastFrame;

            GetGamePadDPads(playerIndex, out dpadThisFrame, out dpadLastFrame);
                
            return dpadThisFrame.Down == ButtonState.Pressed && dpadLastFrame.Down == ButtonState.Released;
        }

        /// <summary>Gets whether the DPad Left button was pressed this update.</summary>
        public static bool IsDPadLeftJustPressed(ExtendedPlayerIndex playerIndex)
        {
            GamePadDPad dpadThisFrame;
            GamePadDPad dpadLastFrame;

            GetGamePadDPads(playerIndex, out dpadThisFrame, out dpadLastFrame);
                
            return dpadThisFrame.Left == ButtonState.Pressed && dpadLastFrame.Left == ButtonState.Released;
        }

        /// <summary>Gets whether the DPad Right button was pressed this update.</summary>
        public static bool IsDPadRightJustPressed(ExtendedPlayerIndex playerIndex)
        {
            GamePadDPad dpadThisFrame;
            GamePadDPad dpadLastFrame;

            GetGamePadDPads(playerIndex, out dpadThisFrame, out dpadLastFrame);
                
            return dpadThisFrame.Right == ButtonState.Pressed && dpadLastFrame.Right == ButtonState.Released;
        }

        /// <summary>Gets whether the DPad Right button was pressed this update.</summary>
        private static void GetGamePadDPads(ExtendedPlayerIndex playerIndex, out GamePadDPad dpadThisFrame, out GamePadDPad dpadLastFrame)
        {
            var povThisFrame = ExtendedGamePadState[(int)playerIndex].Pov1;
            var povLastFrame = ExtendedGamePadStateLastFrame[(int)playerIndex].Pov1;

            dpadThisFrame = Nuclex.Input.Devices.ExtendedGamePadState.DpadFromPov(povThisFrame);
            dpadLastFrame = Nuclex.Input.Devices.ExtendedGamePadState.DpadFromPov(povLastFrame);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //These routines test whether compound directions (dpads AND left analogue stick) are pressed.
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>Gets whether 'up' was released this update (either dpad left or left thumstick pushed left passed a certain threshold).</summary>
        public static bool IsUpPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsDPadUpPressed(playerIndex) || IsLeftStickUpPressed(playerIndex);
        }

        /// <summary>Gets whether 'down' was released this update (either dpad down or left thumstick pushed down passed a certain threshold).</summary>
        public static bool IsDownPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsDPadDownPressed(playerIndex) || IsLeftStickDownPressed(playerIndex);
        }

        /// <summary>Gets whether 'left' was pressed this update (either dpad left or left thumstick pushed left passed a certain threshold).</summary>
        public static bool IsLeftPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsDPadLeftPressed(playerIndex) || IsLeftStickLeftPressed(playerIndex);
        }

        /// <summary>Gets whether 'right' was pressed this update (either dpad right or left thumstick pushed right passed a certain threshold).</summary>
        public static bool IsRightPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsDPadRightPressed(playerIndex) || IsLeftStickRightPressed(playerIndex);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //These routines test for compound directions (dpads AND left analogue stick) pressed this frame (ie the directions were not pressed the previous frame)
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>Gets whether 'up' was released this update (either dpad left or left thumstick pushed left passed a certain threshold).</summary>
        public static bool IsUpJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsDPadUpJustPressed(playerIndex) || IsLeftStickUpJustPressed(playerIndex);
        }

        /// <summary>Gets whether 'down' was released this update (either dpad down or left thumstick pushed down passed a certain threshold).</summary>
        public static bool IsDownJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsDPadDownJustPressed(playerIndex) || IsLeftStickDownJustPressed(playerIndex);
        }

        /// <summary>Gets whether 'left' was pressed this update (either dpad left or left thumstick pushed left passed a certain threshold).</summary>
        public static bool IsLeftJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsDPadLeftJustPressed(playerIndex) || IsLeftStickLeftJustPressed(playerIndex);
        }

        /// <summary>Gets whether 'right' was pressed this update (either dpad right or left thumstick pushed right passed a certain threshold).</summary>
        public static bool IsRightJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return IsDPadRightJustPressed(playerIndex) || IsLeftStickRightJustPressed(playerIndex);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //These routines get analogue stick valuess
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        public static Vector2 GetLeftStick(ExtendedPlayerIndex playerIndex)
        {
            return LeftSticks[(int)playerIndex];
        }

        public static Vector2 GetRightStick(ExtendedPlayerIndex playerIndex)
        {
            return RightSticks[(int)playerIndex];
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //These routines test for analogue sticks movement
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>Gets whether the Left Analogue stick was pushed up this update.</summary>
        public static bool IsLeftStickUpPressed(ExtendedPlayerIndex playerIndex)
        {
            return (LeftSticks[(int)playerIndex].Y > AnalogueToDigitalThreshold);
        }

        /// <summary>Gets whether the Left Analogue stick was pushed down this update.</summary>
        public static bool IsLeftStickDownPressed(ExtendedPlayerIndex playerIndex)
        {
            return (LeftSticks[(int)playerIndex].Y < -AnalogueToDigitalThreshold);
        }

        /// <summary>Gets whether the Left Analogue stick was pushed left this update.</summary>
        public static bool IsLeftStickLeftPressed(ExtendedPlayerIndex playerIndex)
        {
            return (LeftSticks[(int)playerIndex].X < -AnalogueToDigitalThreshold);
        }

        /// <summary>Gets whether the Left Analogue stick was pushed right this update.</summary>
        public static bool IsLeftStickRightPressed(ExtendedPlayerIndex playerIndex)
        {
            return (LeftSticks[(int)playerIndex].X > AnalogueToDigitalThreshold);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //These routines test for analogue sticks moved this frame (ie the sticks were neutral the previous frame)
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>Gets whether the Left Analogue stick was pushed up this update.</summary>
        public static bool IsLeftStickUpJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return (LeftSticks[(int)playerIndex].Y > AnalogueToDigitalThreshold && LeftSticksLastFrame[(int)playerIndex].Y < AnalogueToDigitalThreshold);
        }

        /// <summary>Gets whether the Left Analogue stick was pushed down this update.</summary>
        public static bool IsLeftStickDownJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return (LeftSticks[(int)playerIndex].Y < -AnalogueToDigitalThreshold && LeftSticksLastFrame[(int)playerIndex].Y > -AnalogueToDigitalThreshold);
        }

        /// <summary>Gets whether the Left Analogue stick was pushed left this update.</summary>
        public static bool IsLeftStickLeftJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return (LeftSticks[(int)playerIndex].X < -AnalogueToDigitalThreshold && LeftSticksLastFrame[(int)playerIndex].X > -AnalogueToDigitalThreshold);
        }

        /// <summary>Gets whether the Left Analogue stick was pushed right this update.</summary>
        public static bool IsLeftStickRightJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return (LeftSticks[(int)playerIndex].X > AnalogueToDigitalThreshold && LeftSticksLastFrame[(int)playerIndex].X < AnalogueToDigitalThreshold);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //Helper routines get raw input from analogue sticks
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        private static float GetRawLeftStickXAxis(ExtendedPlayerIndex playerIndex)
        {
            var axis =  GamePadMappings[(int)playerIndex].LeftStickXAxis;
            return ExtendedGamePadState[(int)playerIndex].GetAxis((ExtendedAxes)axis) * (GamePadMappings[(int)playerIndex].LeftStickXAxisInvert ? -1: 1);
        }

        private static float GetRawLeftStickYAxis(ExtendedPlayerIndex playerIndex)
        {
            var axis =  GamePadMappings[(int)playerIndex].LeftStickYAxis;
            return ExtendedGamePadState[(int)playerIndex].GetAxis((ExtendedAxes)axis) * (GamePadMappings[(int)playerIndex].LeftStickXAxisInvert ? -1: 1);
        }

        private static float GetRawRightStickXAxis(ExtendedPlayerIndex playerIndex)
        {
            var axis =  GamePadMappings[(int)playerIndex].RightStickXAxis;
            return ExtendedGamePadState[(int)playerIndex].GetAxis((ExtendedAxes)axis) * (GamePadMappings[(int)playerIndex].RightStickXAxisInvert ? -1: 1);
        }

        private static float GetRawRightStickYAxis(ExtendedPlayerIndex playerIndex)
        {
            var axis =  GamePadMappings[(int)playerIndex].RightStickYAxis;
            return ExtendedGamePadState[(int)playerIndex].GetAxis((ExtendedAxes)axis) * (GamePadMappings[(int)playerIndex].RightStickYAxisInvert ? -1: 1);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //Helper routines for the above methods.
        //------------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>Gets whether a generic button is currently pressed (ie button is down).</summary>
        public static bool IsButtonPressed(ExtendedPlayerIndex playerIndex, int buttonId)
        {
            return ExtendedGamePadState[(int)playerIndex].IsButtonDown(buttonId);
        }

        /// <summary>Gets whether a generic button was pressed this update.</summary>
        public static bool IsButtonJustPressed(ExtendedPlayerIndex playerIndex, int buttonId)
        {
            return ExtendedGamePadState[(int)playerIndex].IsButtonDown(buttonId) && ExtendedGamePadStateLastFrame[(int)playerIndex].IsButtonUp(buttonId);
        }

        /// <summary>Gets whether a generic button was released this update.</summary>
        public static bool IsButtonJustReleased(ExtendedPlayerIndex playerIndex, int buttonId)
        {
            return ExtendedGamePadState[(int)playerIndex].IsButtonUp(buttonId) && ExtendedGamePadStateLastFrame[(int)playerIndex].IsButtonDown(buttonId);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //Helper routines get raw input from analogue sticks
        //------------------------------------------------------------------------------------------------------------------------------------------------------
        public static bool IsRightTriggerPressed(ExtendedPlayerIndex playerIndex)
        {
            return ExtendedGamePadState[(int) playerIndex].Slider2 > 0.1f;
        }

        public static bool IsRightTriggerJustPressed(ExtendedPlayerIndex playerIndex)
        {
            return ExtendedGamePadState[(int)playerIndex].Slider2 > 0.1f && ExtendedGamePadStateLastFrame[(int)playerIndex].Slider2 <= 0.1f;
        }
    }
}
