﻿#if WINDOWS_STOREAPP
    namespace System.IO
    {
        public class FileStream : Stream
        {
            public float readSingle()
            {
                return 0.0f;
            }

            public override void Flush()
            {
                
            }

            public override int Read(byte[] buffer, int offset, int count)
            {
                return 1;
            }

            public override long Seek(long offset, SeekOrigin origin)
            {
                return 1;
            }

            public override void SetLength(long value)
            {
                
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                
            }

            public override bool CanRead
            {
                get { return false; }
            }

            public override bool CanSeek
            {
                get { return false; }
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override long Length
            {
                get { return 0; }
            }

            public override long Position { get; set; }
        }
    }
#endif
